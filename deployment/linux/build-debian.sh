#!/bin/sh
echo Création de la préparation pour la version 6.00.00
echo Paramètres:
echo "- sb.version : " 6.00.00
echo "- basedir : " /ext/oStorybook/oStorybook6
echo "- sb.debian.distdir : " /ext/oStorybook/oStorybook6/distrib/debian-package
echo "- sb.debian.app : " /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/ostorybook
echo "- sb.debian.deployment.dir : " /ext/oStorybook/oStorybook6/deployment/linux/debian
mkdir /ext/oStorybook/oStorybook6/distrib/debian-package
echo Copie debian-package
cp -R /ext/oStorybook/oStorybook6/deployment/linux/debian/DEBIAN /ext/oStorybook/oStorybook6/distrib/debian-package
cp -R /ext/oStorybook/oStorybook6/deployment/linux/debian/usr /ext/oStorybook/oStorybook6/distrib/debian-package
sed -i -e "s/@version@/6.00.00/g" /ext/oStorybook/oStorybook6/distrib/debian-package/DEBIAN/control
sed -i -e "s/@version@/6.00.00/g" /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/applications/ostorybook.desktop
sed -i -e "s/@version@/6.00.00/g" /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/doc/ostorybook/changelog
gzip -9 /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/doc/ostorybook/changelog
echo Copie application
mkdir /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/ostorybook
cp -R /ext/oStorybook/oStorybook6/dist/* /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/ostorybook
rm /ext/oStorybook/oStorybook6/distrib/debian-package/usr/share/ostorybook/LICENSE.txt
echo Modification des droits
find /ext/oStorybook/oStorybook6/distrib/debian-package/usr -type d -exec chmod 755 {} +
chmod -R 755 /ext/oStorybook/oStorybook6/distrib/debian-package/DEBIAN
echo Construction du paquet DEBIAN
dpkg-deb --build /ext/oStorybook/oStorybook6/distrib/debian-package /ext/oStorybook/oStorybook6/distrib/oStorybook-6.00.00.deb
echo Nettoyage...
rm -r -f /ext/oStorybook/oStorybook6/distrib/debian-package
echo Construction du paquet RPM
cd /ext/oStorybook/oStorybook6/distrib
alien -r /ext/oStorybook/oStorybook6/distrib/oStorybook-6.00.00.deb