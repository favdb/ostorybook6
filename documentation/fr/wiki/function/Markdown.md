# Markdown

La syntaxe proposée permet de se concentrer sur la rédaction du texte tout en permettant une mise en forme normalisée. Le détail de cette syntaxe est le suivant:

- les espaces multiples en début de ligne sont ignorés.
- titre: les titres commencent en début de ligne par le caractère dièse &#35;. Le nombre de dièses détermine le niveau du titre pouvant varier de 1 à 6. Exemple: &#35; Titre de niveau 1
- paragraphe: le changement de paragraphe se fait en doublant le retour chariot.
- gras: le début et la fin de la mise en gras est signalé par deux astérisques. Exemples: **&#42;&#42;ceci est en gras&#42;&#42;**
- italique: le début et la fin de la mise en italique est signalé par une astérisque. Exemple: *&#42;ceci est en italique&#42;*
- souligné: le début et la fin du souligné est indiqué par un tiret bas (underscore en anglais). Exemple: _&#95;ceci est souligné&#95;_
- une réplique dans un dialogue est indiqué par une barre de fraction en début de ligne, suivi du nom du personnage (de son abréviation si elle existe) suivi d'une autre barre de fraction. Aucun contrôle n'est fait sur le nom du personnage. Le reste de la ligne peut contenir le texte de la réplique, mais il sera restitué, après la mise en forme, comme un nouveau paragraphe, voir plus loin la mise en forme. Notez que vous pouvez forcer la barre de fraction, et donc ignorer le code "dialogue" en utilisant la double barre de fraction.
- une indication scénique, autrement dit une didascalie, est introduite par une parenthèse ouvrante en début de ligne et la fin de cette annotation est signalée par une parenthèse fermante. La parenthèse fermante n'est pas 
obligatoire si elle coincide avec la fin de la ligne.

## Mise en forme

Plutôt qu'un long discours en exemple sera beaucoup plus instructif.

_**Syntaxe Markdown:**_

-------

&#35;&#35;&#35; titre de niveau 3

Texte normal sans mise en forme particulière. Puis en texte comportant un mot 
en &#42;&#42;gras&#42;&#42;, puis un autre en &#42;italique&#42; et enfin un dernier qui est &#95;souligné&#95;.

Ce qui suit est une réplique issue du "Médecin Malgré Lui" de Molière :

/SGANARELLE/ (Il prend un bâton, et lui en donne.) Ah ! vous en voulez, donc.

//Et voici l'exemple d'utilisation de la barre de fraction.

--------

**Rendu après mise en forme:**

---------

### titre de niveau 3
Texte normal sans mise en forme particulière. Puis en texte comportant un mot 
en **gras**, puis un autre en *italique* et enfin un dernier qui est _souligné_.

Ce qui suit est une réplique issue du "Médecin Malgré Lui" de Molière:

................... **SGANARELLE**<br>
................... *Il prend un bâton, et lui en donne.*<br>
................... Ah ! vous en voulez, donc.

/Et voici l'exemple d'utilisation de la barre de fraction.

-------

Notez que dans le cas de la réplique les points de la ligne du nom du personnage ainsi que ceux des suivantes, jusqu'à un double retour à la ligne ou une autre réplique, représentent une mise en forme ayant les caractéristiques suivantes:

- retrait de paragraphe à gauche et à droite de 4cm environ
- centrage des lignes
- le nom du personnage est en gras
- la didascalie est en italique

Vous remarquerz que la mise en forme permet d'enchaîner, sans séparation particulière, le nom du personnage, l'indication scénique, le texte de la réplique.

----
retour à l'[index des fonctions](Index)