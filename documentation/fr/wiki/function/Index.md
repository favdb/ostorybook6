# Fonctions particulières et astuces

Liste des fonction particulières:

* [Astuces](./Astuces.md)
* [Assistant](./Assistant.md)
* [Correcteur orthographique](./Correcteur.md)
* [Editeur](./Editor.md)
* [Traduction](./Translation.md)
* [Markdown](./Markdown.md)
* [Interface](./Interface.md)

----
Retour au [somaire général](../Index.md)