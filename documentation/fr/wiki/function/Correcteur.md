# Correcteur orthographique

*(FAQ) Je trouve que le correcteur est bien simpliste!*

oStorybook n'a pas vocation à se substituer à des logiciels très spécialisés. 
C'est pourquoi le correcteur intégré peut paraître "primaire", il s'agit d'un 
simple moteur de détection d'anomalies orthographiques dont le fonctionnement 
repose sur un dictionnaire rudimentaire. Ce dictionnaire est une simple 
collection de mots. Lorsque plusieurs déclinaisons d'un même mot existe il est 
nécessaire d'enrichir le dictionnaire de toutes ces déclinaisons. Ainsi, si on 
souhaite l'exhaustivité de la vérification du verbe être il faudra intégrer 
dans le dictionnaire toutes les formes possibles du verbe être, donc à tous les 
temps.

En plus du dictionnaire de base il est possible de demander la mémorisation, 
dans un dictionnaire annexe, des variantes orthographiques souhaitées.

Mise en oeuvre technique

C'est le module JOrtho qui est utilisé. Pour des raisons de performance et de 
cohérence le code source du module a été intégré au code source de oStorybook, 
voir le paquetage "lib". JOrtho est sous licence GPL et a été développé par 
i-net software (https://www.inetsoftware.de).

----
retour à l'[index des fonctions](Index)