Éditeur de texte
================

Pour le mode [Scénario](../view/Scenario) se reporter à la partie [Markdown](Markdown).

oStorybook dispose d'un éditeur de texte intégré pour la saisie du texte d'une 
scène. Cet éditeur autorise un minimum de mise en forme. Une barre d'outil 
permet de sélectionner la mise en forme voulue et des racourcis claviers sont 
accessibles. Ces mises en forme sont (entre parenthèses le racourcis associés):

Pour les caractères:

- gras (Ctrl+B)
- italique (Ctrl+I)
- souligné (Ctrl+U)
- barré (Ctrl+K)
- indice
- exposant

Pour les paragraphes:

- alignement à gauche (Ctrl+L)
- centré
- alignement à droite (Ctrl+R)
- justifié (Ctrl+J)

Puis vous disposez d'autres boutons pour insérer des éléments particuliers:

- lien interne
- lien externe (vers Internet)
- image
- tableau
- forcer un changement de ligne
- insertion d'un caractère spécial

Et enfin quatre boutons utiles pour la mise en forme de dialogues:

- tiret cadratin
- guillement français ouvrant et fermant
- guillemet anglo-saxon ouvrant et fermant

----
retour à l'[index des fonctions](Index)