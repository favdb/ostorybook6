# Trucs et Astuces

## Informations complètes sur votre ouvrage:

Allez dans la vue Arbre et sélectionnez la racine, concrétisé par le titre du
livre. La vue Info est mise à jour avec toutes les données relatives au livre, 
y compris les différents paramètres que vous avez choisi.

[Index des fonctions](Index)
