Traduction de l'interface utilisateur

# Préalable

Si vous créez une nouvelle traduction dans une langue déjà identifiée vous serez
peut-être confronté au message:

    Missing resource /msg/messages_XX.properties

You have to contact the development team.

En effet, les langues identifiées sont en principe déjà traduitent et intégrées
à oStorybook. Si ce n'était pas le cas il vous faut donc contacter l'équipe de
développeent pour recevoir une version spécifique intégrant la langue choisie.

# Principes de base

Le fichier des messages est intégré au logiciel. Mais vous pouvez choisir 
d'utiliser un fichier externe personnalisé. Pour ce faire il faut lancer 
l'exécution de oStorybook avec le paramètre "--msg" (voir le document [Starting](../user/Strating).

----
retour à l'[index des fonctions](Index)