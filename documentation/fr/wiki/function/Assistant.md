# Assistant

En panne d'inspiration? oStorybook peut vous fournir quelques outils pour y
remédier.
Concrètement vous répondez à un certain nombre de questions et vos réponses sont
enregistrées dans la base de données. Par la suite l'onglet "Assistant" vous
permet de visualiser vos choix à tout instant. Les informations "Assistant" 
vous sont aussi restituées dans les fiches HTML exportées. 

## L'action

Une première formule vous permet de déterminer une action-type, inspirée de 
l'ouvrage de G. POLTI "les 36 situations dramatiques". Cet assistant est 
directement utilisable depuis:
  
* le livre
* le chapitre
* la scène

De plus une liste déroulante vous permet de sélectionner, pour les chapitres et
les scènes, l'une des douze étapes décrites par J. CAMPBELL dans son ouvrage 
"Le Héros au mille visages", repris par Christopher Vogler dans son célèbre 
mémo de 7 pages "Un Guide pratique du Héros aux mille visages".

## Description d'un élément

Les fiches de description des Personnages, Lieux et Objets vous paraissent
limitées? Qu'à cela ne tienne, l'Assistant vous proposera dans chaque cas de 
repondre à un petit questionnaire adapté au type d'élément. 

## Adaptation - Personnalisation - Traduction

La liste des questions proposées par l'Assistant ne vous convient pas? Elle 
n'est pas disponible dans votre langue? Il est possible d'adapter, ou traduire, 
le, ou les, questionnaires. Pour cela vous devrez réutiliser le fichier:
Assistant.properties en respectant les principes suivants:

### Avant toute chose:
Faites une sauvegarde du fichier original. En cas d'erreur vous pourrez ainsi 
revenir en arrière facilement.

### Structure du fichier Assistant
Le fichier est de type texte UTF8. Chaque ligne correspond à une information à 
afficher et éventuellement à renseigner. L'assistant se présente sous forme 
d'une boîte de dialogue comportant des onglets. La première ligne a paramétrer 
concerne donc chaque onglet. Les différentes composantes disponible sont:

* book: pour ce qui est des notes relatives aux données générales de 
l'ouvrage.
* polti: concerne les éléments généraux décrivants une situation tirée des 
"36 situations dramatiques" de Georges Polti. Ces situation peuvent être 
invoquées au niveau général de l'ouvrage, ou au niveau de chaque chapitre ou 
scène.
* person, location, item: concerne les notes relatives aux éléments 
correspondants. **Note** une ligne de définition ne peut contenir un retour à la ligne. En 
effet, un changement de ligne sera interprété comme la définition d'une 
nouvelle ligne de paramétrage.

### Onglets
Il est possible de paramétrer jusqu'à 9 onglets numérotés de 1 à 9. Exemple:

    book.1.tab=Méthodologie

Le mot-clé "tab" indique qu'il s'agit de l'identification d'un onglet. Utilisez un libellé assez court que vous pouvez compléter par une description 
  plus détaillées qui sera introduite par un point-virgule. Exemple complet:

    book.1.tab=Méthodologie;D'après "36 situations dramatiques" de Georges Polti.

### Ligne de définition
Après la ligne d'onglet on définit les différentes rubriques. Celles-ci sont 
numérotés de 01 à 99 (attention à bien utiliser le zéro non significatif). Puis 
on indique le type de rubrique, et enfin, si nécessaire, le numéro de l'option 
sur deux chiffres (là aussi veillez à utiliser le zéro non significatif). 
Exemple d'une clé de description d'une rubrique:

    person.2.03.radio.05

Cette clé concerne le personnage. Elle appartient à l'onglet numéro 2 et est 
la 3ème rubrique de cet onglet. La rubrique est de type radio et c'est la 
cinquième option de cette rubrique.

Une rubrique ou une option peut contenir un détail d'information qui sera 
affiché dans une bulle d'aide. Ce complément est à ajoutez en le séparant du 
reste par un point-vigule. Voir l'exemple "book" situé à la fin.

### Types de rubrique

- **text** : Une rubique de type "text" se matérialise sous la forme d'un champ de texte 
simple. La longueur maximum du libellé qu'on peut y saisir est limitée à 
64 caractères.

- **textarea** : Similaire à une rubrique de type "text" mais c'est un champ de saisie pouvant
compter plusieurs lignes.

- **int** : Une rubrique "int" est similaire à une rubrique text mais son contenu ne doit 
comporter que des chiffres. La taille visualisée est réduite.

- **radio** : Une rubrique "radio" se traduit par des boutons radio. On peut définir cette 
rubrique sous deux formes:

   - une ligne unique comportant le titre suivi d'une double barre de fraction, 
puis les différentes options possibles séparées par des point-virgules.
   - sur plusieurs lignes, la première comporte le titre, les suivantes detaillent
chacune des options. Dans ce type de description on peut ajouter un commentaire 
en le séparant du titre par un point-virgule. Le commentaire du titre sera 
affiché en italique, les commentaires des options seront affichés dans une bulle
d'aide.

- **check** : Une rubrique "check" est similaire à une rubrique "radio" dans sa construction. 
En revanche on peut cocher plusieurs cases.

* **list** : Une rubrique "list" est une liste à choix unique. Comme pour la rubrique 
"radio" on peut la décrire sous la forme d'une ligne simple ou en plusieurs 
lignes.

* **mlist** : La rubrique "mlist" et similaire à la rubrique "list". La différence est qu'on 
peut sélectionner plusieurs éléments.

## Exemples:
Dans un assistant pour les personnages on peut définir une rubrique se 
rapportant à la couleur des yeux de deux manières: radio ou list. Exemple:

* **radio** en une seule ligne:

    person.1.01.radio=Yeux// Noir; Brun; Marron; Vert; Bleu

* **radio** en plusieurs lignes:

    person.1.01.radio=Yeux

    person.1.01.radio.01=Noir

    person.1.01.radio.02=Brun

    person.1.01.radio.03=Marron

    person.1.01.radio.04=Vert

    person.1.01.radio.05=Bleu

* **list** en une seule ligne:

    person.1.01.list=Yeux// Noir; Brun; Marron; Vert; Bleu

* **list** en plusieurs lignes:

    person.1.01.list=Yeux

    person.1.01.list.01=Noir

    person.1.01.list.02=Brun

    person.1.01.list.03=Marron

    person.1.01.list.04=Vert

    person.1.01.list.05=Bleu`

À noter que les espaces qui précèdent ou suivent les double barre de fraction 
ou les point-virgules sont ignorés, ce qui permet de rendre plus intelligible 
le fichier de l'assistant. De la même manière le signe dièse placé au début 
d'une ligne indique au système qu'il s'agit d'une ligne de commentaire qui 
sera ignorée.

## Récapitulatif des caractères à usage réservé:

**`#`** (dièse) en premier caractère d'une ligne, indique que la ligne est un commentaire

**//** (double slash, ou double barre de fraction)

**;** (point-virgule) permet de séparer et identifier les différents options
   d'un choix.

## Remarque:

Si vous concevez votre propre assistant et que vous pensez qu'il pourrait en
intéresser d'autres vous pouvez le faire parvenir à l'équipe de développement 
qui se fera un plaisir de le publier. N'oubliez pas d'insérer au tout début, 
dans un commentaire, vos éléments d'identifications, en tout cas ceux que vous 
voulez faire apparaître. Exemple:

    # Fichier Assistant réalisé par FaVdB en 2018
    # licence GPL V3.0
    # contact : ostorybook@ostorybook.eu

----
retour à l'[index des fonctions](Index)