# Vue Scenario

Le choix de travailler en mode scénario se fait lors de la création du projet. 
À partir de ce moment tous les texte des scènes seront à rédiger en mode 
[Markdown](F05_Markdown). Ce mode privilégie en mise en forme du texte conforme 
aux standards en usage pour ce contexte. Notez toutefois qu'il ne s'agit pas 
d'un mode Markdown véritable, celui-ci n'étant pas normalisé, mais s'inspire 
fortement de la syntaxe élaborée par John Gruber consultable sur le site:
[Daring Fireball](https://daringfireball.net/projects/markdown/)

## Principes

La vue dispose d'une barre d'outils où on retrouve les fonctons de navigation 
dans les scènes, et à gauche un bouton permettant de passer la vue en plein 
écran et le traditionnel bouton de création d'une idée.

La reste est divisée en 3 parties:

- à gauche les caractéristiques de la scène
- au centre le texte sur lequel travailler avec trois éléments distincts:
    - en haut les indications scénaristiques d'enchainement, de lieu, de temps
    - au milieu le texte de la scène
    - en bas les indications scénaristiques de fin de séquence
- à droite la description et les notes

----
retour à l'[index des vues](Index)
