# Paperclip

La vue **Trombone** est disponible pour ceux qui veulent suivre les principes de la méthode éponyme.

La vue présente:

- à droite: la liste des idées
- au centre: un affichage du type [Info](V04_Info) qui se modifie en fonction de l'élément sélectionné soit dans la liste de gauche soit dans la liste de droite.
- à droite: la liste des scènes.

Pour chaque liste trois boutons sont disponibles pour créer, modifier ou supprimer un élément. De plus, dans la liste des idées un clic droit ouvre un menu "popup" dans lequel figure la fonction "transformer en scène".

## Pourquoi une vue spéciale?

Bien entendu il est possible d'arranger les autres vues de manière similaire à ce que propose Paperclip. La différence est qu'ainsi vous vous privez de disposer d'autres vues. Ceci étant le Paperclip existe est n'est nullement obligatoire. À vous de voir si ça vous est utile et éventuellement si on pourrait l'améliorer. Sinon voici les autres vues à utiliser pour obtenir un résultat similaire:

- à gauche : la table des idées.
- au centre: la vue [Info](V04_Info).
- à droite : la table des scènes.

**Notez** toutefois qu'à la différence de la vue [Info](V04_Info) le texte de la scène n'est pas affiché, en lieu et place vous avez le nombre de mots et le nombre de caractères.

----
retour à l'[index des vues](Index)
