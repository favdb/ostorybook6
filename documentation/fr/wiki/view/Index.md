Vues
====

Liste des vues disponibles, par ordre alphabétique du nom original:

* [Charts](./Charts.md): différents rapports
* * [Person Occurrence](./Charts_OccPerson.md): fréquence d'apparition des personnages.
* * [Location Occurrence](./Charts_OccLocation.md): fréquence d'apparition des lieux.
* * [Item Occurrence](./Charts_OccItem.md): fréquence d'apparition des objets.
* * [WiWW](./Charts_Wiww.md): littéralement Who is Where and When, Qui est Où et Quand.
* [Chrono](./Chrono.md): chronologie des scènes.
* [Gallery](./Gallery.md): gallerie d'images.
* [Info](./Info.md): informations de l'éléments actuellement ciblé.
* [Manage](./Manage.md): gestion de l'organisation des scènes.
* [Memoria](./Memoria.md): visualisation du type "mind mapping"
* [Memos](./Memos.md): visualisation des mémos.
* [Navigation](./Navigation.md): pour rechercher rapidement certains éléments.
* [Paperclip](./Paperclip.md): vue dédiée à la méthode [Trombone](./user/Methode_Paperclip.md)
* [Planning](./Planning.md): pour la planification du travail d'écriture.
* [Reading](./Reading.md): visualisation dy type "aperçu avant impression".
* [Scenario](./Scenario.md): rédaction d'un scénario.
* [Storyboard](./Storyboard.md): visualisation synthétique sous forme d'un storyboard.
* [Table](./Table.md): visualisation des éléments sous forme de tableaux.
* [Tree](./Tree.md): arborescence des différents éléments.
* [Typist](./Typist.md): vue de type "dactylographique" pour se concentrer sur la rédaction du texte.
* [Work](./Work.md): vue générale de travail.

À noter que les vues suivantes permettent de travailler directement sur le 
texte:

* [Chrono](./Chrono.md)
* [Scenario](./Scenario.md)
* [Typist](./Typist.md)
* [Work](./Work.md)

-----------------------
retour au [sommaire général](../Index.md)
