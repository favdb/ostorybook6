# Gallerie

La vue Gallerie permet de visualiser les photos et images définies dans les 
éléments Photo. Vous disposez de deux paramètres:
- zoom : qui permet d'ajuster la taille visualisée, sachant que celle-ci est 
ajustée au maximum à la dimension de votre écran, et au minimum à un dixième de 
celui-ci.
- direction : précise si les images sont à afficher sous forme d'une colonne 
unique ou sous forme d'une ligne.

Ces deux paramètres combinés permettent, en lien avec la disposition des autres 
vues, la visualisation des images avec toute autre vue on combinaisons de 
celles-ci.

----
retour à l'[index des vues](Index)
