# Vue "Gérer les chapitres et les scènes"
======================================

## Principes

La vue "Manage" permet de gérer l'organisation des scènes dans les chapitres. 
Son utilisation fait largement appelle au glisser/déposer. En effet, chaque 
scène est matérialisée par un petit encadré dont l'apparence rapelle fortement 
celui d'un post-it. On peut manipuler à la souris ces éléments pour les 
répartir, ou les ré-organiser, au sein des différents chapitres.

## Aspect

L'aspect général est tout à fait similaire à une présentation en tableau où les 
colonnes correspondent aux scènes, et les lignes aux chapitres.

## Mode opératoire

Il s'agit de déplacer une scène d'un endroit à un autre. Il faut cliquer sur le 
titre et ne relacher le bouton que lorsque la souris est à l'endroit souhaité. 
Lorsque vous survolez une localisation utilisable un trait vert apparaît. 
Celui-ci se situera soit entre deux scènes déjà positionnées, soit au début du 
chapitre, soit à la fin. Lorsque vous relachez le bouton toutes les scènes du 
chapitre concerné sont renumérotées.

## Paramétrage

Vous pouvez paramétrer le "pas" de numérotation des scènes. La premère scène du 
chapitre sera celui que vous indiquerez, les autres scènes recevront un numéro 
conforme au pas que vous indiquerez. Par exemple, si vous choisissez les 
paramètres suivants: début = 5, incrément = 2, la première scène recevra, lors 
de la renumérotation le numéro 5, puis les scènes suivantes recevront dans 
l'ordre les numéros: 7, 9, 11, 13, etc...
Par défaut le début est fixé à 1 et l'incrément est 1.

## Remarque

À tout moment vous pouvez renuméroter toutes les scènes via le menu:
Outils>Renuméroter les scènes.

----
retour à l'[index des vues](Index)
