# Chrono

La vue **Chrono** présente les différentes sur le mode chronologique. Cette 
chronologie est calculée à partir des données d'enchainement des scènes dont:
- la date de la scène
- l'enchainement relatif à une autre scène (nom de la scène précédente, durée 
avant le début de la scène)

Bien entendu l'utilisation de cette vue n'est pertinente que dans le cas où 
ces informations sont renseignées. Sinon la scène n'apparaîtra pas.

La vue **Chrono** permet de travailler sur les différents éléments comme le 
titre (nom de la scène) et le texte lui-même. Notez toutefois que vous ne 
disposerez pas des fonctions de mise en forme comme avec l'éditeur. Ce sera 
donc surtout un mode de travail de type "brouillon" ou "ébauche".

Vous disposez, en haut, dans la mini barre d'outils de la possibilité de 
dimensionner l'affichage des scènes, de choisir le sens d'affichage (horizontal 
ou vertical), et d'afficher les différence de dates.

_Rappel_ : si vos scènes ne sont pas renseignées en ce qui concerne la date, en 
valeur absolue ou relative, vous ne pourrez pas voir les voir.

----
retour à l'[index des vues](Index)
