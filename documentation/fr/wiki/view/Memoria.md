# Vue Memoria

Cette vue permet de mettre en évidence les relations entre différents éléments.
Ces relations sont de deux niveaux:
- relations primaires : celles qui sont définis dans l'élément
- relations secondaires : celles qui resultent d'un lieu relatif qui peut être 
calculé.

## Exemple

Dans l'exemple d'un élément de type Scène. Il est possible de définir des 
relations primaires comme :
- des personnages
- des lieux
- des objets
- des étiquettes
- des points de vue
- la scène qui précède et celle qui suit

Un personnage, un lieu ou un objet peut avoir une relation avec d'autres 
éléments. Dans ce cas il s'agit d'une relation secondaire qui sera visualisée à 
travers un élément Relation qui a été défini. 

Le vue Memoria est construite sous forme d'un réseau entre ces différents 
éléments qui a comme point de départ l'éléments central sélectionné dans la 
barre d'outils de la vue.

----
retour à l'[index des vues](Index)
