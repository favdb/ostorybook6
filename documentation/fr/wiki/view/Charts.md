# Les Rapports

Liste des rapports que peut produire oStorybook:
- Apparition d'un personnage, d'un objet, d'un lieu
- Répartition des personnages par date
- Répartition des personnages par scène
- Répartition des Points de Vue par date
- Qui,Quand, Où?

----
retour à l'[index des vues](Index)
