Vue Memoria
===========

Cette vue est une représentation très synthétique du projet en cours. Chaque 
scène est représentée avec des informations minimisées:

- le nom de la scène est réduit à un maximum de 16 caractères environ, si le 
nom fait plus de 16 caractères des points de suite sont affichés. Le survol du 
nom permet de faire apparaître un popup qui détail le nom complet, le numéro 
de la scène ainsi que les notes évetuelles.
- l'icône représentative de l'état d'écriture (voir la partie Status).
- une icône dans le cas où la scène a été marquée "informative" (voir la 
descritpion de l'élément Scene).
- les personnages, sous forme d'icône, avec un maximum de 5 icônes. Si il y a 
plus de 5 personnages, une sizième icône, représentant un plus, appraît. Le 
survole de chaque icône permet de faire apparaître un popup qui donne les 
informations essentiels du-dit personnage. Le popup du plus donnera quant à lui 
les informations essentielles des autres personnages. À noter que le survol 
d'une zone autre que ces icônes fera apparaître un popup donnant simplement les 
noms des personnages.
- le lieu principal où se déroule la scènes. Si la scène est liée à plusieurs 
lieux le survol de la zone à droite du lieu indiqué donnera la liste des lieux.
- les objets. Si la liste des objets n'apparaît pas en entier le survol de 
cette zone fera apparaître cette liste dans un popup.
- la date et l'heure de la scène.

De plus à droite il y a trois boutons permettant d'accéder directement à:

- l'éditeur de la scène
- sa suppression
- la création d'une nouvelle scène

La barre d'outils de cette vue permet:

- de sélectionner une partie, s'il y en a plusieurs;
- de changer le sens d'affichage entre hrizontal et vertical, ce qui permet 
d'arranger, par la suite, cette vue de manière à correspondre à ses 
besoins.

NOTA: seules les scènes affectées à un chapitre apparaissent dans la vue.

----
retour à l'[index des vues](Index)
