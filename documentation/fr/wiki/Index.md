# Documentation générale

- [Introduction](./Introduction.md)
- [Description des données](./data/Index.md) (plutôt destinée aux développeurs, mais utile aussi pour l'utilisateur)
- [Les fonctions générales](./function/Index.md)
- [Les différentes vues](./view/Index.md)
- [Le guide de l'utilisateur](./user/Index.md)

