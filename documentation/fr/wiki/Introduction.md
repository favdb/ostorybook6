# Introduction
*_source_ [OSJournal](https://osjournal.com/open-source-novel-writing-software-free-the-author-within-285/)*

*oStorybook a probablement une interface insolite pour les auteurs de romans et de nouvelles. En effet, elle ressemble plus à un IDE (Integrated Development Environment) pour programmeur qu'à un outil pour écrivain. Mais comme un IDE, 
oStorybook est en mesure de suivre et de présenter presque tous les détails concernant les personnages, les lieux et les objets. Il comporte également une fonction spéciale permettant de noter les idées et les inspirations soudaines.*

Donc ne vous fiez pas aux apparences, apprenez à vous servir de oStorybook qui aspire avant tout à être un outil au service de l'auteur.

Voyez le [guide de l'utilisateur](./user/Index.md).

## Pour les développeurs:

+  la description des [données](./data/Index.md).
+  la description des [fonctions](./function/Index.md).
+  la description des différentes [vues](./view/Index.md) et [interfaces](./ui/Index.md)
+  les règles d'[écriture du Wiki](./Rules.md).

----
[Index général](./Index.md)
