# Méthode du Trombone

Le principe veut que l'auteur ne s'embarrasse pas d'une méthodologie descriptive
à l'instar de la méthode du flocon. Il commence à écrire immédiatement sous 
forme de notes décrivant une scène ou une simple idée. Puis il organise ses 
notes pour constituer des chapitres et des scènes.

Avec oStorybook cette méthode s'applique simplement via la fonction "idées". En 
effet il suffit qu'à terme l'utilisateur puisse transformer ses idées, 
préalablement sommairement décrites, en scènes non assignées dans un premier 
temps. Ensuite, il pourra assigner une scène à l'un ou l'autre des chapitres, 
en particulier grace à la vue "Gérer les chapitres et les scènes" appelée plus 
simplement la vue "Gestion". Pour la transformation il est aussi possible de 
s'aider de la vue "Trombone (Paperclip en anglais).

Aucune règle n'est imposée pour la notation d'une idée. Toutefois il convient 
de se rappeler que la description d'une idée ne permet pas l'usage d'une mise 
en forme particulière. Il conviendra donc de faire une éventuelle mise en forme 
une fois la transformation en scène effectuée.

Pour de plus amples détails sur cette méthode se référer au site [écrire un roman](https://www.ecrire-un-roman.org/2018/06/ecrire-roman-sans-plan.html)

NOTA: dans l'entité Idea (idée) un champ "thème" est disponible pour compléter, 
en tant que de besoin, l'information Catégorie. Le champ "Thème" est un simple 
champ de texte libre avec lequel il est possible de filtrer la liste des idées.

## Mécanique

La création de la scène se fait via l'éditeur de l'idée avec le bouton 
"Créer une scène". Le nom de la scène est la concaténation de "Idée:" suivi du 
nom de l'idée. On contrôle qu'une scène n'est pas existante avec ce nom, si 
c'est le cas une message d'avertissement est affiché et le processus est 
abandonné. 
La scène est créée en copiant le champ "Description" dans le champ "Texte" de 
la scène, et en recopiant les éventuelles notes. Le champ "Description" de la 
scène est laissé vide. Le chapitre n'est pas renseigné, ce qui fait que la 
scène sera "non assignée". Le statut est laissé à "Dummy". Puis la scène est 
ajoutée à la liste des scènes, avec raffraichissement des vues nécessaires. Le 
statut de l'idée est incrémenté de 1. À l'issue de la création, si tout s'est 
bien passé, l'éditeur de l'idée est refermé avec la mise à jour qui concerne 
le statut.

## Présentation à l'écran

Pour plus de confort et d'efficacité il est recommandé de disposer à 
l'affichage les vues suivantes:

- à gauche: liste des [Idées](../view/Table.md)
- au centre: la vue [Gestion](../view/Manage.md)
- à droite: les [Mémos](../view/Memos.md)

De cette manière on peut accéder à la création de:

- nouvelles idées: via le bouton "Nouveau" de la liste des Idées, ou via le bouton habituellement présent sur presque toutes les vues en haut à droite
- nouvelles scène: via le menu contextuel "Créer une scène" dans la vue [Gestion](../view/Manage.md). Ce menu est appelé par un clic droit dans la zone des scènes non assignées, ou dans l'une des zones de chapitre, si ceux-ci existent. Attention dans ce dernier cas le champ [Chapitre](../data/Chapter.md) de la nouvelle scène sera pré-renseigné avec le chapitre dans lequel on a fait le clic droit.

Il est possible d'utiliser la vue [Trombone](../view/Paperclip.md) comme aide supplémentaire.

----
retour au [Sommaire du guide](./Index.md)
