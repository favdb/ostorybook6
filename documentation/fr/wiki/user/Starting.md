# Mise en oeuvre

Le présent document concerne la mise en oeuvre du logiciel.

## Via interface graphique

Normalement, à l'installation du logiciel une icône a été créée dans votre menu de démarrage ou sur votre bureau. Il suffit de double-cliquer sur cette icône pour lancer l'exécution de oStorybook.

## Lancement manuel

Dans certains cas particulier il peut s'avérer nécessaire de lancer l'exécution avec des paramètres spéciaux. Ceux-ci sont introduits par un double tiret de manière à les distinguer des paramètres Java. Voici les paramètres en question:

- help : qui vous donnera la syntaxe des paramètres.
- msg filename : pour utiliser une lanque personnalisée de l'interface 
                 utilisateur. Le fichier "filename" doit exister.
- dev : pour utiliser des fonctionnalités en cours de développement.
- trace : pour tracer l'activité du logiciel, c'est utile en particulier pour
              diagnostiquer une anomalie.

Nota: chaque paramètre est introduit par un double-tiret, c'est nécessaire pour éviter toute confusion avec les paramètres Java. Exemple

    java -jar ~/oStorybook/oStorybook.jar --trace

## Première utilisation

Avant de commencer à utiliser oStorybook vous serez invité à choisir votre langue. En effet, oStorybook est parfois utilisé pour faire de la traduction, il est donc nécessaire de distinguer l'environnement propre de oStorybook par rapport à celui de votre système. Attention toutefois, la liste présente de nombreuses langues mais toutes ne disposent pas d'une traduction de l'interface du logiciel. (voir à ce propos la partie consacrée à la [traduction de l'interface](../function/Translation.md)

----
retour au [Sommaire du guide](Index)
