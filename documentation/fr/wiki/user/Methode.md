Méthode
=======

Chaque auteur a sa propre méthode de travail, toutefois certaines ont été plus
ou moins théorisées. Parmi ces méthodes de travail oStorybook, grâce à la 
flexibilté de son interface, propose une mise en oeuvre simple de 3 méthodes.

*  [Gestionnaire](./Methode_Manage.md) (aussi appelé méthode Architecte)
*  [Flocon](./Methode_Snowflakes.md) (que certains appellent aussi méthode du Jardinier)
*  [Trombonne](./Methode_Paperclip.md)

----
retour au [Sommaire du guide](./Index.md)
