Méthode Gestionnaire
====================

C'est la méthode de base de oStorybook. Elle consiste à travailler 
essentiellement avec les différents tableaux d'éléments. La vue [Manage](./Manage.md) 
intervient dans un second temps pour organiser les scènes et chapitres.

*à développer*

----
retour au [Sommaire du guide](./Index.md)
