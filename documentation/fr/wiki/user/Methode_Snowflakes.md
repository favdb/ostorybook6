Méthode du flocon
=================

Pour en savoir plus sur cette méthode on peut se référer au site:
https://www.mecanismes-dhistoires.fr/la-methode-flocon/
qui décrit cette méthode progressive par étapes successives:

1. résumer de l'histoire en une phrase unique
2. décliner la phrase en un paragraphe de 5 phrases
3. rédiger un paragraphe par personnage principal et secondaire, par lieu ou encore par concept
4.  rédiger un court synopsis de l'histoire en 4 paragraphes
5. développer chaque personnage à travers un synopsis particulier à chacun d'eux
6. rédiger le synopsis d'ensemble en une à deux pages
7. faire une fiche détaillée pour chaque personnage, lieu et concept important de l'histoire
8. faire un plan scénique détaillé, scène par scène
9. écrire chaque scène

La mise en ouvre de cette méthode avec oStorybook consiste à faire appel à 
l'[assistant](../function/Assistant.md) qui vous proposera, au niveau général des propriétés du livre, de 
choisir l'étape que vous abordez, dans la liste suivante:

1. Résumer en une seule phrase: résumez en une simple phrase le sujet général du roman.
2. Résumer en un paragraphe: résumez l'histoire en un paragraphe de quatre à six phrases.
3. Présenter les personnages principaux: présentez chaque personnage principal.
* Résumer les personnages: résumez en une seule phrase simple chaque personnage.
* Détailler des personnages: résumez en 5 ou 6 phrases chaque personnage.
4. Détailler le synopsis: détaillez chaque phrase de l'étape 2 en un paragraphe complet, soit l'équivalent d'une page.
5. Décrire l'intrigue: décrivez et résumez l’intrigue, sur une page pour les personnages principaux, plus basique pour les personnages secondaires.
6. Développer le synopsis: développez le synopsis de l’étape 4 sur quatre ou cinq pages.
7. Détailler les personnages: détaillez chaque personnage, avec background et synopsis. Utilisez au besoin l'assistant de personnage.
8. Écrire la liste des scènes: résumez chaque scène en une phrase.
9. Écrire les scènes: détaillez chaque scène.

Cet assistant peut aussi être utilisé au niveau de chaque chapitre si on est 
déjà plus avancé dans son projet.

----
retour au [Sommaire du guide](./Index.md)
