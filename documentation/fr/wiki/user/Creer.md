# Créer un projet

On entendra ici qu'un projet correspond à la création d'un fichier qui recueillera vos données. Ce fichier est de type XML (voir par exemple l'article Wikipedia consacré à ce que c'est que le XML).

Pour créer un projet une information minimale est nécessaire, il s'égit du titre de votre livre. La deuxième information nécessaire sera tout simplement le nom du fichier qui sera à créer ainsi que le dossier où il sera enregistré.

Le processus commence en cliquant le menu Fichier > Nouveau. Une boîte de dialogue va vous permettre de saisir les informations essentielles sur votre (futur) ouvrage, comme:
- le titre;
- le sous-titre éventuel;
- le nom de l'auteur, ou des auteurs, donc votre nom ou votre pseudonyme;
- l'indication du droit d'auteur (le copyright)
- un court résumé de l'histoire
- d'éventuelles notes

Par ailleurs vous avez la possibilité d'indiquer si votre projet est un scénario ou pas.

Enfin vous pourrez aussi choisir d'utiliser un calendrier spécifique et les assistants disponible.

Rassurez-vous, à tout moment vous pourrez revenir à ces informations pour les modifier.

Ensuite vous serez invité à choisir un dossier pour enregistre le fichier dont vous pourrez aussi changer le nom.

_Reommandation_ : une bonne règle d'organisation est de dédier un dossier pour chaque projet. Vous pourrez ainsi y ranger, vos différents fichiers que vous collecterez. Vous pourrez même y créer des sous-dossiers, ce qui somme toute est le B-A-BA d'une bonne organisation. Mais, comme en toute chose vous avez le choix de vous organiser comme vous voulez.

----
retour au [Sommaire du guide](./Index.md)
