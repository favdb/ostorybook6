# Chapitre

Traditionnellement un livre est structuré en chapitre. Pour une pièce de 
théâtre les chapitres sont des Actes.

## Informations

- nom du chapitre.
- numéro: numéro d'ordre du chapitre.
- partie: lien à la partie à laquelle le chapitre est rattaché.
- objectif de date: pour les utilisateurs qui souhaitent se fixer des objectifs 
de réalisation par rapport à un calendrier.
- objectif de longeur: pour les utilisateurs qui souhaitent se fixer des 
objectifs de réalisation en terme de longueur du texte.
- date de réalisation: pour les utilisateurs qui souhaitent se fixer des 
objectifs de réalisation, date d'atteinte de l'objectif.

## Informations obligatoires

- nom
- numéro
- partie

## Contraintes

- nom: doit être unique dans la liste des chapitres.
- numéro: doit être unique dans la liste des chapitres, pour obtenir le 
prochain numéro automatiquement mettre un ?.
- partie: à sélectionner dans la liste des parties existantes.

----
Retour à l'[Index des données](./Index.md)