# Informations communes

Toutes les entités ont une structure d'informations communes, que l'on peut 
décrire sous forme d'un en-tête, et deux informations de "conclusion".

## En-tête

- **id** : type Long, c'est le numéro d'identification de l'entité dans la base de données. Cette information ne peut être modifiée par l'utilisateur.
- **creation** : type [SbDate](./SbDate.md), date et heure de création de l'élément dans la base de données.
- **maj** : type [SbDate](./SbDate.md), date et heure de la dernière modification apportée à l'entité.
- **name** : type String, nom attribué par l'utilisateur pour identifier l'entité.

## Conclusion
- **description** : texte de description de l'entité.
- **notes** : notes d'information sur l'entité.

## Remarques
Voir [Date](./SbDate.md) pour la description des dates.

## Informations obligatoires
- **id**
- **creation** : est renseignée automatiquement.
- **maj** : est renseignée automatiquement.
- **name**

## Contraintes
- **id** : doit être unique dans la base, est calculée par le logiciel.
- **creation** : est calculée par le logiciel.
- **maj** : est calculée par le logiciel.
- **name** : doit être unique dans le type d'entité

----
[Index des données](./Index.md)