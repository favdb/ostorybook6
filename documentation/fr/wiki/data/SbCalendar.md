# SbCalendar : type de donnée pour le calendrier

oStorybook offre la possibilité de paramétrer un calendrier spécifique. Ce calendrier permet de déterminer:

- le nombre de mois, leurs noms et abréviations, limité à un maximum de 99 mois
- le nombre de jours dans une semaine, leurs noms et abréviations, limité à un maximum de 9 jours
- le nombre d'heures dans une journée
- le nombre de minutes dans une heure
- le nombre de secondes dans une minute
- le jour de la semaine correspondant à la date du 01/01/0000, soit le premier jour de l'année 0. Limité par le nombre de jours dans une semaine.

Le nombre d'heures, de minutes et de secondes est limité à un maximum de 99.

## Informations :

- **months**: liste des mois sous la forme d'une structure comprenant:
     - **name**: nom du mois (String).
     - **abbr**: abréviation du nom du mois (String limité à 3 caractères).
     - **days**: nombre de jours dans le mois.
- **days**: liste des jours de la semaine sous la forme d'une sructure comprenant:
    - **name**: nom du jour (String)
    - **abbr**: abréviation du nom du jour (String limité à 2 caractères)
- **first_day**: numéro du jour dans la semaine pour la première date possible, donc le 01/01/0000 (Integer ne pouvant être supérieur au nombre de jours)

## Contraintes:

Si le calendrier comporte 12 mois, une semaine de 7 jours, 24 heures de 6 minutes de 60 secondes et que le **first_day** est égal à 5, il sera considéré que le calendrier de référence est le calendrier standard.

retour à l'[Index des données](Index)