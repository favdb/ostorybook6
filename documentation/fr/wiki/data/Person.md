# Fiche Personnage

Le champ Abréviation permet de donner un nom alternatif au personnage. Ce
nom est censé être une concaténation des 2 premiers caractères du
Prénom et des 3 premiers caractères du Nom. Si on saisit autre chose dans ce
champ alors l'automatisme est interrompu et tout retour pour modification du
Prénom ou du Nom n'aura ensuite aucun impacte sur l'abréviation.

----
Retour à l'[Index des données](./Index.md)