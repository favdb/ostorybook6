# BookInfo : informations générales du Livre

La structure **BookInfo** détaille les informations générales du livre, à savoir :

- **title** : titre du livre (String).
- **subtitle** : sous-titre éventuel (String).
- **author** : le ou les auteurs  (String).
- **copyright** : information sur le droit d'auteur (String).
- **scenario** : pour indiquer que le livre est un scénario (Boolean).
- **poltiUse** : pour indiquer l'utilisation de l'assistant "polti" (Boolean).
- **setpUse** : pour indiquer l'utilisation de l'assistant "step" (Boolean).
- **blurb** : résumé du livre (String).
- **notes** : annotations de l'auteur (String).
- **assistant** : informations de l'[assistant](../function/AssistantBook.md) en fonction des choix **poltiUse** et **stepUse**.

## Contraintes
- **title** : est la seule information obligatoire.

## Remarque

Voir le [guide de l'utilisateur](../function/Index.md) pour plus d'informations sur l'[assistant](../function/Assistant.md), en particulier le module dédié à l'aide à la conception de l'[histoire](../function/AssistantBook.md)

----
Retour au [Livre](./Book.md)