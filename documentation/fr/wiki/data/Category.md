# Category : fiche de catégorie

L'élément **Category** permet de regrouper d'autres éléments sous une même rubrique. Certains de ces autres éléments peuvent appartenir à plusieurs catégories, par exemple un personnage aura une première catégorie qui indiquera si c'est un personnage principal ou secondaire. Dans un roman de science-fiction on pourra créer une catégorie "extra-terrestre" et ainsi regrouper logiquement tous les personnages concernés par cette catégorie.

Le nombre de catégories n'est pas limité, toutefois les deux catégories  "personnage principal" et "personnage secondaire" ne peuvent être supprimés. Il est possible de créer des sous-catégories, dans ce cas il suffit de renseigner le lien d'attachement à la catégorie de niveau supérieure.

## Informations obligatoires

- **name**
- **sort** : type Integer, ordre de classement.
- **type** : type String, type de catégorie parmi [événement](./Event.md), [idée](./Idea.md), [action](./Plot.md), [personnage](./Person.md), [objet](./Item.md), [étiquette](./Tag.md).
- **sup** : type Long, lien de rattachement à une catégroie supérieure.

## Contraintes

- **name** : ne doit pas exister à l'identique deux fois, même si le type est différent.
- **sort** : l'ordre de classement doit être unique, quelque soit le type. Il est possible d'attribuer automatiquement un ordre de classement en mettant un ?.

----
Retour à l'[Index des données](./Index.md)