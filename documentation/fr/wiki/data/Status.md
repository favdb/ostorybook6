# Status

L'état d'un élément est une caractéristique associée de description. Il s'applique aux éléments:

- [Scene](./Scene.md)
- [Idea](./Idea.md)

Ce **Status* permet de filtrer les listes de ces éléments. À noter que pour ce filtrage une option "Tout" est disponible. Il indique l'état actuel de conception de l'élément. Cette donnée est facultative et prend l'une des valeurs suivantes:

- DUMMY : vide, ou indéfini, c'est la valeur initiale.
-  OUTLINE : les grandes lignes,
- DRAFT : brouillon,
- EDIT1 : 1ère lecture,
- EDIT2 : 2ème lecture,
- DONE : terminé,
- IN_PROGRESS : en cours,
- CANCELED: annulé

Le status *CANCELED* est utilisé pour éviter de supprimer physiquement l'idée ou la scène. Dans ce cas l'effet de ce status n'aura pas de conséquence sur d'autres fonctions, comme la visualisation [READING](../view/Reading.md) d'affichage du texte.

Le stockage dans la base de données se fait sous la forme du code numérique correspondant au numéro d'ordre dans la liste des états ci-dessus. Par exemple l'état DONE sera codé 5, puisque la numérotation commence à 0.

----
Retour à l'[Index des données](./Index.md)