# Book (Livre)

Lors de la création du projet, le fichier est initialisé avec les éléments suivants:

- les [informations générales](./BookInfo.md): le titre, l'auteur, etc...
- les [catégories](./Category.md) de base : personnage principal et secondaire.
- les [genres](./Gender.md) de base : masculin, féminin.
- une [partie](./Part.md) contenant un [chapitre](./Chapter.md) auquel une [scène](./Scene.md) est rattachée.

## Informations

- **last_id**: dernier identifiant utilisé
- **info**: [informations générales](./BookInfo.md) du livre
- **categories**: liste des [catégories](./Category.md)
- **chapters**: liste des [chapitres](./Chapter.md)
- **events**: liste des [événements](./Event.md)
- **genders**: liste des [genres](./Gender.md)
- **ideas**: liste des [idées](./Idea.md)
- **items**: liste des [objets](./Item.md)
- **locations**: liste des [lieux](./Location.md)
- **memos**: liste des [mémos](./Memo.md)
- **parts**: liste des [parties](./Part.md)
- **persons**: liste des [personnages](./Person.md)
- **photos**: liste des [photos](./Photo.md)
- **plots**: liste des [actions/pitch](./Plot.md)
- **relations**: liste des [relations](./Relation.md)
- **scenes**: liste des [scènes](./Scene.md)
- **tags**: liste des [étiquettes](./Tag.md)

## Contraintes
- **last_id**: est calculé automatiquement en fonction de la création des différents éléments de la base de données.

----
Retour à l'[Index des données](./Index.md)