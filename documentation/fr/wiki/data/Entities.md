Description des différente éléments
===================================

Chaque élément contient des informations [communes](20_00_Common.md) auxquelles 
des informations spécifiques sont ajouées.

Les différents éléments sont (dans l'ordre alphabétique):

- [Category](D020_Category.md) : catégorie
- [Chapter](D030_Chapter.md) : chapitre
- [Event](D040_Event.md) : événement
- [Gender](D050_Gender.md) : genre
- [Idea](D060_Idea.md) : idée
- [Item](D070_Item.md) : objet
- [Location](D080_Location.md) : lieu
- [Memo](D090_Memo.md) : mémo
- [Part](D100_Part.md) : partie
- [Person](D110_Person.md) : personnage
- [Photo](D120_Photo.md) : photo
- [Plot](D130_Plot.md) : action
- [Pov](D140_Pov.md) : point de vue
- [Relation](D150_Relation.md) : relation
- [Scene](D160_Scene.md) : scène
- [Tag](D170_Tag.md) : étiquette

[Index des données](D000_Index.md)