# Photo : aide picturale

Certains auteurs ont besoin de s'appuyer sur une image, une photo, pour décrire un personnage, un lieu, un objet. Vous pouvez donc agrémenter votre base de données de telles informations en utilisant la fonction "Photo".

## Remarque préliminaire

Il est recommandé de regrouper dans un même dossier tous ses fichiers. La base de données bien entendu, mais aussi tous les autres documents que vous utilisez. Vous devriez même créer un dossier principal et des sous-dossiers par type de document. Par exemple pour mon livre "Mon Livre à moi" voici comment ça se présente:

    /Livre
       |-- Mon Livre à moi.osbk (c'est ma base de données)
       |-- /Images (sous dossier dans lequel je vais ranger toutes mes images)
       |-- /Documents (sous dossier pour ranger les articles de presse et autres
       |               documents)
       |-- /Export (sous-dossier qui me servira pour mes exports)

Bien entendu libre à vous de ne pas utiliser ce principe d'organisation.

Ce qui est enregistré dans la base de données ce sont les liens vers les 
images, accompagnés d'une description et de notes. Pour visualiser ces liens vous disposez de l'habituel tableau des données, mais vous n'y verrez pas, ou très mal vos images. Ce que je vais donc vous décrire vous paraîtra un peu long et compliqué, mais rassurez-vous finalement c'est très simple. Comme on dit souvent "c'est plus long à dire qu'à faire".

## La gallerie

La fonction "Gallerie" permet d'afficher les différentes images sous forme d'une succession d'éléments, chacun d'eux étant réduit à une définition standard proche de 500x500 pixels. En fait cette définition varie en fonction du paramètre que vous pouvez changer en haut du panneau. Ce paramètre permet de sélectionner le nombre d'images à mettre sur une ligne. La dimension découle aussi de la résolution de votre écran. Ainsi avec la valeur 2 et une résolution de 1024x768, les images seront redimensionnées pour s'inscrire dans un carré de:
(1024*0.85)/2, soit 435x435 pixels. Le "0.85" c'est pour indiquer qu'on ne peut utiliser que 85% de l'espace de l'écran, cet élément n'étant pas modifiable. Si on change le nombre d'images par ligne à la valeur de 4 le carré de redimensionnement, pour la même résolution d'écran, sera ramené à 217x217.

## Astuce

Le système de gestion des fenêtres dans oStorybook permet de les disposer selon ses besoins. Ainsi, si je sélectionne "4 images par ligne", ou plus, je vais pouvoir disposer mes fenêtres de manière à ce que je puisse visualiser en même temps les vues:

* [Tree](../view/Tree.md) (pour avoir sous les yeux la structure générale du livre)
* [Typist](../view/Typist.md) (pour pouvoir travailler sur mon texte)
* [Gallery](../view/Gallery.md) (pour voir les photos et images)

Sachez que, à tout moment, vous pouvez enregistrer une disposition pour pouvoir l'utiliser plus tard, ou même pour un autre livre (voir à ce propos le chapitre 
"[Interface](../view/Interface.md)").

----
Retour à l'[Index des données](./Index.md)