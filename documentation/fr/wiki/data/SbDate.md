# SbDate : type de donnée pour une date

## Présentation

Le format d'une date suit les prescriptions de la norme ISO-8601. À savoir:

- y : l'année, sans cadrage particulier
- y...y : l'année sur n chiffres, selon le nombre de y, avec les zéros non 
significatifs
- M : le mois, sans format précis, c'est le numéro du mois dans l'année.
- MM : le mois sur deux chiffres obligatoirement, donc, selon les besoins, avec un zéro non significatif.
- MMM : le nom abrégé du mois.
- MMMM : le nom complet du mois.
- d : le numéro du jour dans le mois, sans format particulier.
- dd : le numéro du jour sur deux chiffres.
- ddd : le nom du jour dans la semaine sous forme abrégée
- dddd : le nom du jour de la semaine sous forme étendue
- H et HH : la même chose que pour le jour, mais concernant l'heure.
- m et mm : idem pour les minutes.
- S et SS : idem pour les secondes.

Il n'y a pas d'élements pour les décomptes sous la seconde. Le séparateur entre la date du jour et l'heure est le T (t majuscule) ou l'espace. La représentation externe d'une date est donc yyyyMMddTHHmmSS. Il est possible d'intégrer tout autre caractère permettant de mieux lire une date, ainsi le format yyyy/MM/ddTHH:mm:SS est tout à fait autorisé. 

## Remarque:

La gestion des années réelles, avec une fraction de 0,25 jour par an, les années bisextiles, etc... n'est pas assurée.

## Astuce:

Vous remarquerez l'alternance entre majuscules et minuscules dans les mnémoniques des éléments, c'est un moyen simple permettant de s'y retrouver avec la codification.

## Informations:

- **year**: année (Integer)
- **month**: mois (Integer)
- **day**: jour (Integer)
- hour: heure (Integer)
- **minute**: minute (Integer)
- **second**: seconde (Integer)
- **calendar**: calendrier de référence ([SbCalendar](SbCalendar))

## Note pour les développeurs:

C'est la classe SbDate, couplée avec la classe SbCalendar, qui s'occupe de gérer tout ça.

Dans le fichier XML les dates sont codées sous la forme yyyyMMddTHHmmSS où:

- **yyyy** représente l'année, le nombre de chiffres est de 4 au minimum mais peut être plus grand.
- **MM** est le numéro du mois dans l'année.
- **dd** est le numéro du jour dans le mois
- **HH** est l'heure
- **mm** représente les minutes
- **SS** représente les secondes.


## Contraintes

À part l'année, tous les éléments sont obligatoirement sur deux chiffres.

retour à l'[Index des données](./Index.md)