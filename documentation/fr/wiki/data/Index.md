***Note pour les développeurs***: oStorybook est développé sous Netbeans Apache version 11.1 (février 2020). Pour les dépendances se repporter au THANKS.txt, les librairies prêtes à l'emploi sont livrées avec le logiciel. Les librairies principales sont intégrées au code source.

# Structure globale des données
Chaque élément dispose d'informations [communes](./Common.md). Par ailleurs certaines informations sont structurées de manière particulière:

*  [Date](./SbDate.md)
*  [Status](./Status.md)

Liste des éléments dans l'ordre alphabétique:

[Book](./Book.md) : le livre en général qui contient les différents éléments:

* [Category](./Category.md) : catégorie qui s'applique à différents éléments (personnage par exemple).
* [Chapter](./Chapter.md) : le chapitre.
* [Event](./Event.md) : événement marquant de l'intrigue générale.
* [Gender](./Gender.md) : genre d'un personnage.
* [Idea](./Idea.md) : idée et inspiration.
* [Item](./Item.md) : objet significatif pour le déroulement de l'action.
* [Location](./Location.md) : lieu où se déroule l'action.
* [Memo](./Memo.md) : mémo, parfois sans rapport avec le livre.
* [Partie](./Part.md) : partie, permet de subdiviser le livre en regroupant les chapitres sans pour autant changer de livre.
* [Person](./Person.md) : description de personnages.
* [Photo](./Photo.md) : image utile à l'auteur pour concrétiser son imaginaire.
* [Plot](./Plot.md) : intrigue ou action.
* [Pov](./Pov.md) : point de vue de la narration.
* [Relation](./Relation.md) : relation pouvant exister entre des personnages, des objets, des lieux.
* [Scene](./Scene.md) : la scène est l'élément "atomique" du livre.
* [Tag](./Tag.md) : indicateur banalisé utile pour la construction de l'intrigue ou de l'action.

----
retour au [Sommaire général](../Index.md)