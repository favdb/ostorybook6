# oStorybook

Ce fichier README explique sommairement ce qu'est **oStorybook**.

## Objectif

oStorybook se veut être un outil universel pour les écrivains, auteurs de romans, de nouvelles, de pièces de théâtre, de scénarios, etc... Il peut même devenir un outil pour l'enseignement et l'étude littéraire.

## Principes

oStorybook est développé et distribué dans le plus pur esprit du Logiciel Libre en respectant les principes de libertés qui sont:

- La liberté d'exécuter le logiciel, pour n'importe quel usage;
- La liberté d'étudier le fonctionnement d'un programme et de l'adapter à ses besoins, ce qui passe par l'accès aux codes sources;
- La liberté de redistribuer des copies;
- La liberté de diffuser des versions modifiées.

Ces libertés n'impliquent pas la gratuité, toutefois oStorybook est gratuit. Si par hasard on vous propose de vous le faire payer alors sachez que ce que vous achetez c'est un support technique et l'aide à la mise en oeuvre.

## Techniques

oStorybook est développé sous Java version 11. C'est donc un logiciel utilisable sur toutes les plateformes de PC (Linux, MacOsX, Windows, BSD). Le fonctionnement du logiciel implique la gestion d'une base de données, celle-ci est dans un format largement inspiré du XML, ce qui permet d'envisager sereinement de réutiliser ses données dans un autre logiciel.

oStorybook n'implique pas la collecte de données personnels. Si la fonction de détection des mises à jour nécessite un accès Internet ce n'est que pour vérifier si le projet a publié une nouvelle version. oStorybook ne nécessite pas d'enregistrement en tant qu'utilisateur, comme pour la totalité des logiciels propriétaires. Toutefois vous êtes cordialement invité à faire savoir que vous en êtes un utilisateur régulier.

## Remerciement

Merci à Martin Munstun qui fût le concepteuroriginal de ce logiciel, même s'il a a été largement réécrit. Consultez le fichier THANKS.md pour connaître les autres outils de programmation qui sont utilisés.