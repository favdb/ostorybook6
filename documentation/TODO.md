# TODO

## Book

* création du mode normal
* création du mode scénario
* création du projet (avec toutes le propriétées de base)
* documentation de la création (U010_Creation)
* check de la création

## Scenario

* définir les données complémentaires:
    * début de scène/séquence (FADE IN, CUT) 
    * type de lieu (EXT, INT) et lieu (en majuscule)
    * type de moment (AUBE, MATIN, JOUR, MIDI, APRÈS-MIDI, CREPUSCULE, NUIT, SOIRÉE, PLUS TARD)
    * type de transition (FADE OUT, CUT, DISSOLVE)
* vérifier qu'il n'y a pas doublon avec Typist, envisager éventuellement de 
fusionner Scenario et Typist (mode scenario exclusif du mode normal)
* ajout les infos dans Scenario
