/*
 * Created on Oct 30, 2007
 */
package lib.shef;

import lib.shef.ui.dialog.ExceptionDialog;
import desk.tools.swing.SwingUtil;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Window;
import java.net.URL;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * A collection of static UI helper methods.
 *
 * @author Bob Tantlinger
 *
 */
public class ShefUtils {
	public static final String X16 = "lib/shef/resources/images/x16/";
	public static final String X24 = "lib/shef/resources/images/x24/";
	public static final String X32 = "lib/shef/resources/images/x32/";

	public static ImageIcon getIconX16(String iconName) {
		return getIcon(X16,iconName);
	}

	public static ImageIcon getIconX24(String iconName) {
		return getIcon(X24,iconName);
	}

	public static ImageIcon getIconX32(String iconName) {
		return getIcon(X32,iconName);
	}

	public static ImageIcon getIcon(String _package, String iconName) {
		if (!_package.endsWith("/")) {
			_package += "/";
		}
		return getIcon(_package + iconName+".png");
	}

	public static ImageIcon getIcon(String path) {
		return createImageIcon(path);
	}

	public static ImageIcon createImageIcon(String path) {

		URL u = Thread.currentThread().getContextClassLoader().getResource(path);
		if (u == null) {
			return null;
		}
		return new ImageIcon(u);
	}

	public static void showError(String msg) {
		showError(null, msg);
	}

	public static void showError(Component c, Throwable ex) {
		Window w = SwingUtilities.getWindowAncestor(c);
		if (w instanceof Frame) {
			showError((Frame) w, ex);
		} else if (w instanceof Dialog) {
			showError((Dialog) w, ex);
		} else {
			showError(c, ex.getLocalizedMessage());
		}
	}

	public static void showError(Component owner, String msg) {
		showError(owner, "Error", msg);
	}

	public static void showError(Component owner, String title, String msg) {
		JOptionPane.showMessageDialog(owner, msg, title, JOptionPane.ERROR_MESSAGE);
	}

	public static void showError(Frame owner, String title, Throwable th) {
		JDialog d = new ExceptionDialog(owner, th);
		if (title != null) {
			d.setTitle(title);
		}
		d.setLocationRelativeTo(owner);
		d.setVisible(true);
		th.printStackTrace(System.err);
	}

	public static void showError(Frame owner, Throwable th) {
		showError(owner, null, th);
	}

	public static void showError(Dialog owner, String title, Throwable th) {
		JDialog d = new ExceptionDialog(owner, th);
		if (title != null) {
			d.setTitle(title);
		}
		d.setLocationRelativeTo(owner);
		d.setVisible(true);
		th.printStackTrace(System.err);
	}

	public static void showError(Dialog owner, Throwable th) {
		showError(owner, null, th);
	}

	public static void showWarning(Component owner, String title, String msg) {
		JOptionPane.showMessageDialog(
				owner, msg, title, JOptionPane.WARNING_MESSAGE);
	}

	public static void showWarning(Component owner, String msg) {
		showWarning(owner, "Warning", msg);
	}

	public static void showWarning(String msg) {
		showWarning(null, msg);
	}

	public static void showInfo(Component owner, String title, String msg) {
		JOptionPane.showMessageDialog(
				owner, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}

	public static void showInfo(Component owner, String msg) {
		showInfo(owner, "Information", msg);
	}

	public static void showInfo(String msg) {
		showInfo(null, msg);
	}

	public static JMenuItem addMenuItem(JMenu menu, Action action) {
		JMenuItem item = menu.add(action);
		configureMenuItem(item, action);
		return item;
	}

	public static JMenuItem addMenuItem(JPopupMenu menu, Action action) {
		JMenuItem item = menu.add(action);
		configureMenuItem(item, action);
		return item;
	}

	private static void configureMenuItem(JMenuItem item, Action action) {
		KeyStroke keystroke = (KeyStroke) action.getValue(Action.ACCELERATOR_KEY);
		if (keystroke != null) {
			item.setAccelerator(keystroke);
		}

		item.setIcon(null);
		item.setToolTipText(null);
	}
}
