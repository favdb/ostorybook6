/*
 * Created on Jan 24, 2006
 *
 */
package lib.shef.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static javax.swing.ScrollPaneConstants.*;
import javax.swing.SwingConstants;
import javax.swing.text.JTextComponent;
import lib.miginfocom.swing.MigLayout;

import lib.shef.ui.dialog.HeaderPanel;
import lib.shef.ShefUtils;
import lib.shef.ui.text.html.Entities;
import db.I18N;

public class SpecialCharDialog extends JDialog {

	private static final long serialVersionUID = 1L;

	private static Icon icon = ShefUtils.getIconX32("copyright");
	private static String title = I18N.getMsg("shef.special_character");
	private static String desc = I18N.getMsg("shef.special_character_desc");

	private final Font plainFont = new Font("Dialog", Font.PLAIN, 12);
	private final Font rollFont = new Font("Dialog", Font.BOLD, 14);

	private final MouseListener mouseHandler = new MouseHandler();
	private final ActionListener buttonHandler = new ButtonHandler();

	private boolean insertEntity;

	private JTextComponent editor;
	private JPanel charPanel;
	private JScrollPane scroll;
	private final String block[][] = {
		{"Basic Latin & Latin-1 Supplement", "33", "255"},
		{"Latin Extended-A", "256", "383"},
		{"Latin Extended-B", "384", "591"},
		{"IPA Extensions", "592", "687"},
		{"Spacing Modifier Letters", "688", "767"},
		{"Combining Diacritical Marks", "768", "879"},
		{"Greek and Coptic", "880", "1023"},
		{"Cyrillic", "1024", "1279"},
		{"Cyrillic Supplement", "1280", "1327"},
		{"Armenian", "1328", "1423"},
		{"Hebrew", "1424", "1535"},
		{"Arabic", "1536", "1791"},
		{"Syriac", "1792", "1871"},
		{"Thaana", "1920", "1983"},
		{"Devanagari", "2304", "2431"},
		{"Bengali", "2432", "2559"},
		{"Gurmukhi", "2560", "2687"},
		{"Gujarati", "2688", "2815"},
		{"Oriya", "2816", "2943"},
		{"Tamil", "2944", "3071"},
		{"Telugu", "3072", "3199"},
		{"Kannada", "3200", "3327"},
		{"Malayalam", "3328", "3455"},
		{"Sinhala", "3456", "3583"},
		{"Thai", "3584", "3711"},
		{"Lao", "3712", "3839"},
		{"Tibetan", "3840", "4095"},
		{"Myanmar", "4096", "4255"},
		{"Georgian", "4256", "4351"},
		{"Hanjul Jamo", "4352", "4607"},
		{"Ethiopic", "4608", "4991"},
		{"Cherokee", "5024", "5119"},
		{"Unified Canadian Aboriginal Syllabics", "5120", "5759"},
		{"Ogham", "5760", "5791"},
		{"Runic", "5792", "5887"},
		{"Tagalog", "5888", "5919"},
		{"Hanunoo", "5920", "5951"},
		{"Buhid", "5952", "5983"},
		{"Tagbanwa", "5984", "6015"},
		{"Khmer", "6016", "6143"},
		{"Mongolian", "6144", "6319"},
		{"Limbu", "6400", "6479"},
		{"Tai Le", "6480", "6527"},
		{"Khmer Symbols", "6624", "6655"},
		{"Phonetic Extensions", "7424", "7551"},
		{"Latin Extended Additional", "7680", "7935"},
		{"Greek Extended", "7936", "8191"},
		{"Punctuation / Scripts / Currency / Diacriticals", "8192", "8447"},
		{"Letterlike Symbols / Number Forms / Arrows", "8448", "8703"},
		{"Mathematical Operators", "8704", "8959"},
		{"Miscellaneous Technical", "8960", "9215"},
		{"Control Pictures", "9216", "9279"},
		{"Optical Character Recognition", "9280", "9311"},
		{"Enclosed Alphanumerics", "9312", "9471"},
		{"Box Drawing / Block Elements / Geometric Shapes", "9472", "9727"},
		{"Miscellaneous Symbols", "9728", "9983"},
		{"Dingbats / Math-A / Arrows-A", "9984", "10239"},
		{"Braille Patterns", "10240", "10495"},
		{"Arrows-B / Math-B", "10496", "10751"},
		{"Supplemental Mathematical Operators", "10752", "11007"},
		{"Miscellaneous Symbols and Arrows", "11008", "11263"},
		{"CJK Radicals Supplement", "11904", "12031"},
		{"Kangxi Radicals", "12032", "12255"},
		{"Ideographic Description Characters", "12272", "12287"},
		{"CJK Symbols and Punctuation", "12288", "12351"},
		{"Hiragana", "12352", "12447"},
		{"Katakana", "12448", "12543"},
		{"Bopomofo", "12544", "12591"},
		{"Hangul Compatibility Jamo", "12592", "12687"},
		{"Kanbun", "12688", "12703"},
		{"Bopomofo Extended", "12704", "12735"},
		{"Katakana Phonetic Extensions", "12784", "12799"},
		{"Enclosed CJK Letters and Months", "12800", "13055"},
		{"CJK Compatibility", "13056", "13311"},
		{"CJK Unified Ideographs Extension A", "13312", "19903"},
		{"Yijing Hexagram Symbols", "19904", "19967"},
		{"CJK Unified Ideographs", "19968", "40879"},
		{"Yi Syllables", "40960", "42127"},
		{"Yi Radicals", "42128", "42191"},
		{"Hangul Symbols", "44032", "55215"},
		{"RESERVED AREA: High Surrogates", "55296", "56319"},
		{"RESERVED AREA: Low Surrogates", "56320", "57343"},
		{"RESERVED AREA: Private Use", "57344", "63743"},
		{"CJK Compatibility Ideographs", "63744", "64255"},
		{"Alphabetic Presentation Forms", "64256", "64335"},
		{"Arabic Presentation Forms-A", "64336", "65023"},
		{"Variation Selectors", "65024", "65039"},
		{"Combining Half Marks", "65056", "65071"},
		{"CJK Compatibility Forms", "65072", "65103"},
		{"Small Form Variants", "65104", "65135"},
		{"Arabic Presentation Forms-B", "65136", "65279"},
		{"Halfwidth and Fullwidth Forms", "65280", "65519"},
		{"Specials", "65520", "65535"}
	};
	private JComboBox blockSel;
	private MigLayout charLayout;
	private Font buttonFont;
	private final int COLUMNS = 24;

	public SpecialCharDialog(Frame parent, JTextComponent ed) {
		super(parent, title);
		editor = ed;
		init();
	}

	public SpecialCharDialog(Dialog parent, JTextComponent ed) {
		super(parent, title);
		editor = ed;
		init();
	}

	@SuppressWarnings("unchecked")
	private void init() {
		JPanel headerPanel = new HeaderPanel(title, desc, icon);
		buttonFont = new Font("Monospaced", Font.BOLD, 12);
		JPanel central = new JPanel(new MigLayout());
		central.add(new JLabel(I18N.getMsg("shef.unicode_block")));
		String x = "";
		for (int i = 0; i < COLUMNS; i++) {
			x += "[]1";
		}
		charLayout = new MigLayout("wrap, inset 1 1", x);
		charPanel = new JPanel(charLayout);
		charPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		blockSel = new JComboBox(getBlocks().toArray());
		blockSel.addActionListener((ActionEvent arg0) -> {
			initBlock(blockSel.getSelectedIndex());
		});
		central.add(blockSel, "wrap");
		scroll = new JScrollPane(charPanel);
		scroll.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
		central.add(scroll, "span");
		blockSel.setSelectedIndex(0);

		JButton close = new JButton(I18N.getMsg("shef.close"));
		close.addActionListener((ActionEvent e) -> {
			setVisible(false);
			dispose();
		});
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonPanel.add(close);
		this.getRootPane().setDefaultButton(close);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(headerPanel, BorderLayout.NORTH);
		getContentPane().add(central, BorderLayout.CENTER);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		pack();
		setResizable(false);
	}

	private void initBlock(int index) {
		charPanel.removeAll();
		String x = "";
		for (int i = 0; i < COLUMNS; i++) {
			x += "[]1";
		}
		int start = Integer.parseInt(block[index][1]);
		int end = Integer.parseInt(block[index][2]);
		initButtons(start, end);
		charPanel.validate();
		scroll.repaint();
	}

	public void initButtons(int start, int end) {
		for (int i = start; i <= end; i++) {
			if (buttonFont.canDisplay((char) i)) {
				JButton btn = createButton(i);
				charPanel.add(btn);
			}
		}
	}

	public void setJTextComponent(JTextComponent ed) {
		editor = ed;
	}

	public JTextComponent getJTextComponent() {
		return editor;
	}

	private JButton createButton(int i) {
		String ent = "&#" + i + ";";
		JButton btn = new JButton(Entities.HTML32.unescape(ent));
		btn.setMinimumSize(new Dimension(24, 24));
		btn.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		btn.setFont(buttonFont);
		btn.setOpaque(true);
		btn.setToolTipText(ent);
		btn.setBackground(Color.LIGHT_GRAY);
		btn.setHorizontalAlignment(SwingConstants.CENTER);
		btn.setVerticalAlignment(SwingConstants.CENTER);
		btn.addActionListener(buttonHandler);
		btn.addMouseListener(mouseHandler);
		btn.setMargin(new Insets(0, 0, 0, 0));
		return (btn);
	}

	private List<String> getBlocks() {
		List<String> xr = new ArrayList<>();
		for (int i = 0; i < block.length; i++) {
			xr.add(block[i][0]);
		}
		return (xr);
	}

	private class MouseHandler extends MouseAdapter {

		@Override
		public void mouseEntered(MouseEvent e) {
			JButton l = (JButton) e.getComponent();
			l.setFont(rollFont);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			JButton l = (JButton) e.getComponent();
			l.setFont(plainFont);

		}
	}

	private class ButtonHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton l = (JButton) e.getSource();
			if (editor != null) {
				if (!editor.hasFocus()) {
					editor.requestFocusInWindow();
				}
				if (insertEntity) {
					editor.replaceSelection(l.getToolTipText());
				} else {
					editor.replaceSelection(l.getText());
				}
			}
		}

	}

	public boolean isInsertEntity() {
		return insertEntity;
	}

	public void setInsertEntity(boolean insertEntity) {
		this.insertEntity = insertEntity;
	}

}
