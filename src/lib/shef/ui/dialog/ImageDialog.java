/*
 * Created on Jan 14, 2006
 *
 */
package lib.shef.ui.dialog;

import java.awt.Dialog;
import java.awt.Frame;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Icon;

import lib.shef.ShefUtils;
import db.I18N;

public class ImageDialog extends HTMLOptionDialog {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

//    private static final I18n i18n = I18n.getInstance("shef.ui.text.dialogs");
	private static Icon icon = ShefUtils.getIconX32("image");
	private static String title = I18N.getMsg("shef.image");
	private static String desc = I18N.getMsg("shef.image_desc");

	private ImagePanel imagePanel;

	public ImageDialog(Frame parent) {
		super(parent, title, desc, icon);
		init();
	}

	public ImageDialog(Dialog parent) {
		super(parent, title, desc, icon);
		init();
	}

	private void init() {
		imagePanel = new ImagePanel();
		setContentPane(imagePanel);
		setSize(300, 345);
		setResizable(false);
	}

	public void setImageAttributes(Map attr) {
		imagePanel.setAttributes(attr);
	}

	public Map getImageAttributes() {
		return imagePanel.getAttributes();
	}

	private String createImgAttributes(Map ht) {
		String html = "";
		for (Iterator e = ht.keySet().iterator(); e.hasNext();) {
			Object k = e.next();
			if (k.toString().equals("a") || k.toString().equals("name")) //$NON-NLS-2$
			{
				continue;
			}
			html += " " + k + "=" + "\"" + ht.get(k) + "\""; //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		}

		return html;
	}

	@Override
	public String getHTML() {
		Map imgAttr = imagePanel.getAttributes();
		boolean hasLink = imgAttr.containsKey("a");
		String html = "";
		if (hasLink) {
			html = "<a " + imgAttr.get("a") + ">";
		}
		html += "<img" + createImgAttributes(imgAttr) + " />";

		if (hasLink) {
			html += "</a>";
		}

		return html;
	}

}
