package lib.shef.ui.dialog;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import db.I18N;

public class ExceptionDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private static final int PREFERRED_WIDTH = 450;
	private static final String DEFAULT_TITLE = I18N.getMsg("shef.error");

	private JPanel jContentPane = null;
	private JLabel iconLabel = null;
	private JLabel titleLabel = null;
	private JLabel msgLabel = null;
	private JPanel buttonPanel = null;
	private JButton okButton = null;
	private JButton detailsButton = null;
	private JScrollPane scrollPane = null;
	private JTextArea textArea = null;
	private JSeparator separator = null;

	public ExceptionDialog() {
		super();
		init(new Exception());

	}

	public ExceptionDialog(Frame owner, Throwable th) {
		super(owner, DEFAULT_TITLE);
		init(th);

	}

	public ExceptionDialog(Dialog owner, Throwable th) {
		super(owner, DEFAULT_TITLE);
		init(th);
	}

	private void init(Throwable th) {
		setModal(true);
		initialize();
		setThrowable(th);
		showDetails(false);
	}

	private void initialize() {
		this.setContentPane(getJContentPane());
		getRootPane().setDefaultButton(getOkButton());
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			GridBagConstraints gbc5 = new GridBagConstraints();
			gbc5.gridx = 0;
			gbc5.gridy = 3;
			gbc5.gridwidth = 3;
			gbc5.fill = GridBagConstraints.HORIZONTAL;
			gbc5.anchor = GridBagConstraints.WEST;
			gbc5.insets = new Insets(0, 0, 0, 0);
			GridBagConstraints gbc4 = new GridBagConstraints();
			gbc4.fill = GridBagConstraints.BOTH;
			gbc4.gridx = 0;
			gbc4.gridy = 4;
			gbc4.gridwidth = 3;
			gbc4.weightx = 1.0;
			gbc4.weighty = 1.0;
			gbc4.insets = new Insets(0, 0, 0, 0);
			GridBagConstraints gbc3 = new GridBagConstraints();
			gbc3.gridx = 2;
			gbc3.gridy = 0;
			gbc3.fill = GridBagConstraints.VERTICAL;
			gbc3.insets = new Insets(0, 0, 10, 0);
			gbc3.gridheight = 3;
			gbc3.anchor = GridBagConstraints.WEST;
			GridBagConstraints gbc2 = new GridBagConstraints();
			gbc2.gridx = 1;
			gbc2.gridy = 1;
			gbc2.insets = new Insets(0, 20, 5, 5);
			gbc2.anchor = GridBagConstraints.WEST;
			msgLabel = new JLabel();
			msgLabel.setText(""); //$NON-NLS-1$
			GridBagConstraints gbc1 = new GridBagConstraints();
			gbc1.gridx = 1;
			gbc1.gridy = 0;
			gbc1.weightx = 1.0;
			gbc1.insets = new Insets(0, 10, 5, 5);
			gbc1.anchor = GridBagConstraints.WEST;
			gbc1.fill = GridBagConstraints.NONE;
			titleLabel = new JLabel();
			titleLabel.setText(I18N.getMsg("shef.error_prompt")); //$NON-NLS-1$
			GridBagConstraints gbc0 = new GridBagConstraints();
			gbc0.gridx = 0;
			gbc0.gridy = 0;
			gbc0.gridheight = 3;
			gbc0.weighty = 0.0;
			gbc0.insets = new Insets(0, 0, 0, 0);
			gbc0.anchor = GridBagConstraints.NORTH;
			iconLabel = new JLabel();
			iconLabel.setText(""); //$NON-NLS-1$
			iconLabel.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
			jContentPane = new JPanel();
			jContentPane.setLayout(new GridBagLayout());
			jContentPane.setBorder(BorderFactory.createEmptyBorder(12, 5, 10, 5));
			jContentPane.add(iconLabel, gbc0);
			jContentPane.add(titleLabel, gbc1);
			jContentPane.add(msgLabel, gbc2);
			jContentPane.add(getButtonPanel(), gbc3);
			jContentPane.add(getScrollPane(), gbc4);
			jContentPane.add(getSeparator(), gbc5);
		}
		return jContentPane;
	}

	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(2);
			gridLayout.setHgap(0);
			gridLayout.setVgap(5);
			gridLayout.setColumns(1);
			buttonPanel = new JPanel();
			buttonPanel.setLayout(gridLayout);
			buttonPanel.add(getOkButton(), null);
			buttonPanel.add(getDetailsButton(), null);
		}
		return buttonPanel;
	}

	private JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton();
			okButton.setText(I18N.getMsg("z.ok"));
			okButton.addActionListener((java.awt.event.ActionEvent e) -> {
				dispose();
			});
		}
		return okButton;
	}

	private JButton getDetailsButton() {
		if (detailsButton == null) {
			detailsButton = new JButton();
			detailsButton.setText(I18N.getMsg("shef.details")); //$NON-NLS-1$
			detailsButton.addActionListener((java.awt.event.ActionEvent e) -> {
				toggleDetails();
			});
		}
		return detailsButton;
	}

	private void toggleDetails() {
		showDetails(!isDetailsVisible());
	}

	public void showDetails(boolean b) {
		if (b) {
			detailsButton.setText(I18N.getMsg("shef.details_"));
			scrollPane.setVisible(true);
			separator.setVisible(false);
		} else {
			detailsButton.setText(I18N.getMsg("shef.details"));
			scrollPane.setVisible(false);
			separator.setVisible(true);
		}

		setResizable(true);
		pack();
		setResizable(false);
	}

	public boolean isDetailsVisible() {
		return scrollPane.isVisible();
	}

	public void setThrowable(Throwable th) {
		String msg = I18N.getMsg("shef.no_message_given");
		if (th.getLocalizedMessage() != null && !th.getLocalizedMessage().equals("")) {
			msg = th.getLocalizedMessage();
		}
		msgLabel.setText(msg);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		th.printStackTrace(new PrintStream(bout));

		String stackTrace = new String(bout.toByteArray());
		textArea.setText(stackTrace);
		textArea.setCaretPosition(0);
	}

	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setPreferredSize(new Dimension(PREFERRED_WIDTH, 3));
		}

		return separator;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setPreferredSize(new Dimension(PREFERRED_WIDTH, 200));
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setTabSize(2);
			textArea.setEditable(false);
			textArea.setOpaque(false);
		}
		return textArea;
	}
}
