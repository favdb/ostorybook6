/*
 * Created on Nov 2, 2007
 */
package lib.shef.ui.actions;

import java.awt.event.ActionEvent;

import javax.swing.JEditorPane;

/**
 * Action suitable for when wysiwyg or source context does not matter.
 *
 * @author Bob Tantlinger
 *
 */
public abstract class AbstractEditAction extends HTMLTextEditAction {

	/**
	 * @param name
	 */
	public AbstractEditAction(String name) {
		super(name);
	}

	/* (non-Javadoc)
     * @see shef.ui.text.actions.HTMLTextEditAction#sourceEditPerformed(java.awt.event.ActionEvent, javax.swing.JEditorPane)
	 */
	@Override
	protected final void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		doEdit(e, editor);
	}

	/* (non-Javadoc)
     * @see shef.ui.text.actions.HTMLTextEditAction#wysiwygEditPerformed(java.awt.event.ActionEvent, javax.swing.JEditorPane)
	 */
	@Override
	protected final void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		doEdit(e, editor);
	}

	protected abstract void doEdit(ActionEvent e, JEditorPane editor);

	protected void updateContextState(JEditorPane editor) {

	}

	@Override
	protected final void updateWysiwygContextState(JEditorPane wysEditor) {
		updateContextState(wysEditor);
	}

	@Override
	protected final void updateSourceContextState(JEditorPane srcEditor) {
		updateContextState(srcEditor);
	}
}
