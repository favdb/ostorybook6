/*
 * Created on Mar 3, 2005
 *
 */
package lib.shef.ui.actions;

import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.Element;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import lib.shef.ShefUtils;
import db.I18N;

/**
 * Action which inserts a horizontal rule
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLCadratinAction extends HTMLTextEditAction {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private int type_char;

	public HTMLCadratinAction() {
		super(I18N.getMsg("shef.cadratin"));
		putValue(MNEMONIC_KEY, (int) I18N.getMnemonic("shef.cadratin"));
		putValue(SMALL_ICON, ShefUtils.getIconX16("cadratin"));
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
		type_char = 0;
	}

	public HTMLCadratinAction(int c) {
		super(I18N.getMsg("shef.dialog"));
		switch (c) {
			case 0:
				putValue(MNEMONIC_KEY, (int) I18N.getMnemonic("shef.cadratin"));
				putValue(SMALL_ICON, ShefUtils.getIconX16("cadratin"));
				break;
			case 1:
				putValue(MNEMONIC_KEY, (int) I18N.getMnemonic("shef.dialog_open"));
				putValue(SMALL_ICON, ShefUtils.getIconX16("dialog_open"));
				break;
			case 2:
				putValue(MNEMONIC_KEY, (int) I18N.getMnemonic("shef.dialog_close"));
				putValue(SMALL_ICON, ShefUtils.getIconX16("dialog_close"));
				break;
		}
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
		type_char = c;
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		switch(type_char) {
			case 0:
				editor.replaceSelection("&#8212; ");
				break;
			case 1:
				editor.replaceSelection("&#171;&nbsp;");
				break;
			case 2:
				editor.replaceSelection("&nbsp;&#187;");
				break;
			case 3:
				editor.replaceSelection("&#8223;&nbsp;");
				break;
			case 4:
				editor.replaceSelection("&nbsp;&#8221;");
				break;
		}
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		int caret = editor.getCaretPosition();
		String c="— ";
		switch(type_char) {
			case 0: c="— "; break;
			case 1: c="« "; break;
			case 2: c=" »";break;
			case 3: c="“ "; break;
			case 4: c="” ";break;
			default: return;
		}
		if (caret < 1) {
			return;
		}
		String x = editor.getSelectedText();
		if (x == null || x.isEmpty()) {
			editor.setSelectionStart(caret);
			editor.setSelectionEnd(caret);
		}
		editor.replaceSelection(c);
	}
}
