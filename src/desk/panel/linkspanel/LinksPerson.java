package desk.panel.linkspanel;

import db.entity.Person;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.tools.swing.label.CleverLabel;
import desk.tools.ToolTip;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.ListUtil;

@SuppressWarnings("serial")
public class LinksPerson extends AbstractPanel {

	private Scene scene;
	private boolean vertical = false;
	private List<String> names;
	private boolean bicon=false;

	public LinksPerson(MainFrame mainFrame, Scene scene) {
		this(mainFrame, scene, "00");
	}

	public LinksPerson(MainFrame mainFrame, Scene scene, String forme) {
		super(mainFrame);
		this.scene = scene;
		if (forme.length()>0){
			if (forme.charAt(0)=='1') this.vertical = true;
			if (forme.charAt(1)=='1') this.bicon = true;
		}
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("LinksPerson.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch (propName) {
			case "scene_Update":
				if (((Scene) newValue).id.equals(scene.id)) {
					refresh();
				}
				return;

			case "person_Update":
				refresh();
		}
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		int max=5;
		if (vertical) {
			setLayout(new MigLayout("wrap 2,insets 0"));
		} else {
			setLayout(new MigLayout("flowx,insets 0"));
			setMaximumSize(new Dimension(170, 50));
		}
		setOpaque(false);
		if (book == null) {
			System.err.println("LinksPanelPerson book is null");
		}
		if (scene == null) {
			System.err.println("LinksPanelPerson scene is null");
		}
		List<Person> list = scene.persons;
		names=new ArrayList<>();
		if (list != null) {
			Collections.sort(list);
		}
		if (list != null && !list.isEmpty()) {
			int i=0;
			for (Person person:list) {
				Color color = person.getJColor();
				JLabel lbName = new JLabel(person.getFullName());
				CleverLabel lbAbbr = new CleverLabel();
				if (bicon) {
					lbAbbr.setIcon(person.getIcon());
				} else {
					lbAbbr.setText(person.abbreviation);
				}
				lbAbbr.setToolTipText(ToolTip.getToolTip(person, scene.date));
				names.add(person.name);
				if (color != null) {
					lbAbbr.setBackground(color);
				} else {
					lbAbbr.setOpaque(false);
				}
				if (vertical) {
					add(lbAbbr);
					add(lbName);
				} else {
					if (i<max) add(lbAbbr, "gap 0");
					i++;
				}
			}
			if (i>max && bicon) {
				JLabel lb=new JLabel(IconUtil.getIcon("small/plus"));
				StringBuilder buf=new StringBuilder("<html>");
				i=0;
				for (Person person:list) {
					i++;
					if (i<=max) continue;
					buf.append("<p><b>").append(person.name).append("</b><br>");
					buf.append(ToolTip.toolTipAppendPerson(person, null)).append("<br>");
					buf.append("</p>");
				}
				buf.append("</html>");
				lb.setToolTipText(buf.toString());
				add(lb);
				setToolTipText(getNames());
			}
		} else {
			add(new JLabel(IconUtil.getIcon("small/empty")));
		}
	}

	public boolean isVertical() {
		return vertical;
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene s) {
		scene = s;
		refresh();
	}
	
	public String getNames() {
		return(ListUtil.join(names, ", "));
	}

}
