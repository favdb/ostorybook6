package desk.panel.linkspanel;

import db.entity.Location;
import db.entity.Scene;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.util.Collections;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class LinksLocation extends AbstractPanel {

	private final Scene scene;
	private final boolean setSize;

	public LinksLocation(MainFrame mainFrame, Scene scene, boolean setSize) {
		super(mainFrame);
		this.scene = scene;
		this.setSize = setSize;
		initAll();
	}

	public LinksLocation(MainFrame mainFrame, Scene scene) {
		this(mainFrame, scene, true);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("LinksLocation.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();

		if ("scene_Update".equals(propName)) {
			if (!((Scene) newValue).id.equals(scene.id)) {
				// not this scene
				return;
			}
			refresh();
			return;
		}

		if ("location_Update".equals(propName)) {
			refresh();
		}
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("insets 0", "grow"));
		JTextArea ta = new JTextArea();
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setEditable(false);
		List<Location> locations = scene.locations;
		if (locations != null) {
			Collections.sort(locations);
		}
		int c = 0;
		if (locations!=null) for (Location location : locations) {
			ta.append(location.toString());
			if (c < locations.size() - 1) {
				ta.append(", ");
			}
			++c;
		}
		if (locations==null || locations.isEmpty()) ta.append(" ");

		if (setSize) {
			JScrollPane scroller = new JScrollPane();
			scroller.setViewportView(ta);
			scroller.setBorder(null);
			if (setSize) {
				scroller.setMinimumSize(new Dimension(100, 30));
				scroller.setPreferredSize(new Dimension(170, 30));
			}
			SwingUtil.setUnitIncrement(scroller);
			scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			ta.setCaretPosition(0);
			add(scroller, "grow");
		} else {
			add(ta, "grow");
		}
	}
}
