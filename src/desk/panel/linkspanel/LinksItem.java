package desk.panel.linkspanel;

import db.entity.Item;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.tools.swing.label.CleverLabel;
import desk.tools.ToolTip;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;
import tools.ListUtil;

@SuppressWarnings("serial")
public class LinksItem extends AbstractPanel {

	private Scene scene;
	private boolean vertical = false;
	private List<String> names;

	public LinksItem(MainFrame mainFrame, Scene scene) {
		this(mainFrame, scene, false);
	}

	public LinksItem(MainFrame mainFrame, Scene scene, boolean vertical) {
		super(mainFrame);
		this.scene = scene;
		this.vertical = vertical;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("LinksItem.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();

		if ("scene_Update".equals(propName)) {
			if (!((Scene) newValue).id.equals(scene.id)) {
				// not this scene
				return;
			}
			refresh();
			return;
		}

		if ("person_Update".equals(propName)) {
			refresh();
		}
	}

	@Override
	public void init() {
	}

	@Override
	@SuppressWarnings("null")
	public void initUi() {
		if (vertical) {
			setLayout(new MigLayout("wrap 2,insets 0"));
		} else {
			setLayout(new MigLayout("flowx,insets 0"));
			setMaximumSize(new Dimension(170, 50));
		}
		setOpaque(false);
		List<Item> list = scene.items;
		names=new ArrayList<>();
		if (list != null) {
			Collections.sort(list);
		}
		list.forEach((item)-> {
			JLabel lbName = new JLabel(item.name);
			CleverLabel lbAbbr = new CleverLabel(item.getAbbr());
			lbAbbr.setToolTipText(ToolTip.getToolTip(item, scene.date));
			lbAbbr.setOpaque(false);
			if (vertical) {
				add(lbAbbr);
				add(lbName);
			} else {
				add(lbAbbr, "gap 0");
			}
			names.add(item.name);
		});
		if (list.isEmpty()) add(new JLabel(" "));
		else setToolTipText(getNames());
	}

	public boolean isVertical() {
		return vertical;
	}

	public Scene getScene() {
		return scene;
	}

	public String getNames() {
		return(ListUtil.join(names, ", "));
	}

}
