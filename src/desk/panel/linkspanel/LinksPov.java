/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008-2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.linkspanel;

import db.entity.Pov;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.tools.swing.label.CleverLabel;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class LinksPov extends AbstractPanel {

	private final Scene scene;
	private final boolean opaque;

	public LinksPov(MainFrame mainFrame, Scene scene) {
		this(mainFrame, scene, false);
	}
	
	public LinksPov(MainFrame mainFrame, Scene scene, boolean opaque) {
		super(mainFrame);
		this.opaque = opaque;
		this.scene = scene;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("LinksPov.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();

		if ("scene_Update".equals(propName)) {
			if (!((Scene) newValue).id.equals(scene.id)) {
				// not this scene
				return;
			}
			refresh();
			return;
		}

		if ("pov_Update".equals(propName)) {
			refresh();
		}
	}

	@Override
	public void init() {
		//App.trace("LinksPov.init()");
	}

	@Override
	public void initUi() {
		//App.trace("LinksPov.initUi()");
		if (scene.pov==null) return;
		Pov pov=scene.pov;
		setLayout(new MigLayout("insets 2"));
		if (opaque) {
			setOpaque(true);
			setBackground(pov.getJColor());
		} else {
			setOpaque(false);
		}
		CleverLabel lb = new CleverLabel(pov.getAbbreviation(), JLabel.CENTER);
		lb.setToolTipText(pov.name);
		lb.setBackground(pov.getJColor());
		add(lb, "w 30");
	}
}
