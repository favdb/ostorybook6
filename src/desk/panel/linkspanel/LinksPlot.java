package desk.panel.linkspanel;

import db.entity.Plot;
import db.entity.Scene;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;


@SuppressWarnings("serial")
public class LinksPlot extends AbstractPanel {

	private Scene scene;
	private boolean vertical = false;

	public LinksPlot(MainFrame mainFrame, Scene scene) {
		this(mainFrame, scene, false);
	}

	public LinksPlot(MainFrame mainFrame, Scene scene, boolean vertical) {
		super(mainFrame);
		this.scene = scene;
		this.vertical = vertical;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("LinksPlot.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch(propName) {
			case "scene_update":
				if (((Scene) newValue).id.equals(scene.id)) {
					refresh();
				}
				break;
			case "plot_update":
				refresh();
				break;
		}
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		if (vertical) {
			setLayout(new MigLayout("wrap 2,insets 0"));
		} else {
			setLayout(new MigLayout("flowx,insets 0"));
			setMaximumSize(new Dimension(170, 50));
		}
		setOpaque(false);
		List<Plot> list = scene.plots;
		if (list!=null) for (Plot plot : list) {
			JLabel lbName = new JLabel(plot.name);
			if (vertical) {
				add(lbName);
			} else {
				add(lbName, "gap 0");
			}
		}
	}

	public boolean isVertical() {
		return vertical;
	}

	public Scene getScene() {
		return scene;
	}
	public void setScene(Scene s) {
		scene=s;
		refresh();
	}
}
