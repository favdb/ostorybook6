/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel;

import db.entity.Book;
import db.entity.Part;
import desk.app.MainFrame;
import desk.tools.combobox.ComboLoad;
import desk.interfaces.IPaintable;
import desk.interfaces.IRefreshable;
import db.I18N;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public abstract class AbstractFrame extends JFrame implements ChangeListener, IRefreshable, IPaintable {
	public MainFrame mainFrame;
	public Book book;
	public JComboBox cbListPart;
	public Part selPart;
	public JToolBar toolbar;

	public AbstractFrame(MainFrame mainframe) {
		mainFrame = mainframe;
		book=mainFrame.book;
		this.setName(this.getClass().getSimpleName());
	}

	@Override
	public abstract void init();

	@Override
	public abstract void initUi();

	public abstract void modelPropertyChange(PropertyChangeEvent evt);

	public final void initAll() {
		init();
		initUi();
	}
	
	public JToolBar initToolbar() {
		toolbar=new JToolBar();
		toolbar.setBorder(BorderFactory.createEmptyBorder());
		toolbar.setOpaque(false);
		toolbar.setLayout(new MigLayout("ins 0"));
		toolbar.setFloatable(false);
		JLabel lb = new JLabel(I18N.getColonMsg("part"));
		cbListPart = new JComboBox();
		ComboLoad.loadCbParts(mainFrame, cbListPart, mainFrame.getCurrentPart(), true);
		cbListPart.addActionListener((ActionEvent evt) -> {
			changePart();
		});
		toolbar.add(lb);
		toolbar.add(cbListPart);
		return(toolbar);
	}
	
	public void changePart() {
		if (cbListPart.getSelectedIndex() == 0) {
			selPart = null;
		} else {
			selPart = (Part) cbListPart.getSelectedItem();
		}
		refresh();
	}
	
	@Override
	public void stateChanged(ChangeEvent ce) {
	}

	@Override
	public void refresh() {
		removeAll();
		initAll();
		invalidate();
		validate();
		repaint();
	}

}
