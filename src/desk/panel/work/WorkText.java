/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.work;

import db.entity.Scene;
import db.entity.Status;
import desk.app.App;
import desk.app.MainFrame;
import desk.action.EntityAction;
import desk.panel.AbstractPanel;
import desk.tools.FontUtil;
import desk.tools.swing.SwingUtil;
import desk.tools.swing.undo.UndoableTextArea;
import db.I18N;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.JTextComponent;
import lib.miginfocom.swing.MigLayout;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class WorkText extends AbstractPanel implements FocusListener {

	private final Scene scene;
	private JLabel lbSceneNumber;
	private JLabel lbStatus;
	private UndoableTextArea taTitle;
	private JTextComponent tcText;

	private int size;
	private final PrefViewWork param;
	private String oText;
	private String oTitle;

	public WorkText(MainFrame mainFrame, Scene scene) {
		super(mainFrame);
		this.scene = scene;
		param = App.getInstance().getPreferences().book;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("WorkText.modelPropertyChange(evt=" + evt.toString() + ")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch (Work.getAction(propName)) {
			case WIDTH:
				setZoomedSize((Integer) newValue);
				refresh();
				break;
			case HEIGHT:
				param.height = (Integer) newValue;
				refresh();
				break;
			default:
				if (newValue instanceof Scene) {
					if (((Scene) newValue).id.equals(scene.id)) {
						switch (propName) {
							case "scene_update":
								Scene newScene = (Scene) newValue;
								if (!newScene.id.equals(scene.id)) {
									return;
								}
								lbSceneNumber.setText(scene.getNumberStr());
								lbStatus.setText(I18N.getMsg("status.not_defined"));
								if (newScene.status != null) {
									lbStatus.setText(Status.getMsg(newScene.status));
								}
								taTitle.setText(newScene.name);
								taTitle.setCaretPosition(0);
								tcText.setText(newScene.writing);
								tcText.setCaretPosition(0);
								break;
						}
					}
				}
		}
	}

	private void setZoomedSize(int zoomValue) {
		size = zoomValue * 12;
	}

	@Override
	public final void init() {
		size = param.width * 12;
	}

	@Override
	public final void initUi() {
		refresh();
	}

	@Override
	public void refresh() {
		//App.trace("WortText.refresh()");
		setLayout(new MigLayout("wrap,fill", "", "[][grow][]"));
		setBorder(SwingUtil.getBorderDefault());

		removeAll();

		// scene number
		lbSceneNumber = new JLabel(scene.getFullNumber());
		lbSceneNumber.setFont(FontUtil.getBold());

		// informational
		JLabel lbInformational = new JLabel("");
		// scene status
		lbStatus = new JLabel(I18N.getMsg("status.not_defined"));
		if (scene.status != null) {
			lbStatus.setText(Status.getMsg(scene.status));
		}

		// title
		taTitle = new UndoableTextArea();
		taTitle.setName("taTitle");
		taTitle.setLineWrap(true);
		taTitle.setWrapStyleWord(true);
		taTitle.setText(scene.name);
		oTitle = taTitle.getText();
		taTitle.setCaretPosition(0);
		taTitle.setDragEnabled(true);
		taTitle.getUndoManager().discardAllEdits();
		JScrollPane titleScroller = new JScrollPane(taTitle);
		titleScroller.setPreferredSize(new Dimension(size, 40));
		taTitle.addFocusListener(this);
		SwingUtil.addCtrlEnterAction(taTitle, EntityAction.edit(mainFrame, scene, false));

		// text
		tcText = SwingUtil.createTextComponent();
		tcText.setName("tcText");
		tcText.setText(scene.writing);
		oText = tcText.getText();
		tcText.setCaretPosition(0);
		tcText.setDragEnabled(true);
		JScrollPane textScroller = new JScrollPane(tcText);
		textScroller.setPreferredSize(new Dimension(size, calculateHeight()));
		textScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		tcText.addFocusListener(this);
		SwingUtil.addCtrlEnterAction(tcText, EntityAction.edit(mainFrame, scene, false));

		add(lbSceneNumber, "split 3,growx");
		add(lbInformational, "growx,al center");
		add(lbStatus);
		add(titleScroller, "grow");
		add(textScroller, "grow");

		revalidate();
		repaint();

		taTitle.setCaretPosition(0);
		tcText.setCaretPosition(0);
	}

	private int calculateHeight() {
		return (int) (100 + size * 0.4) * param.height / 10;
	}

	@Override
	public void focusGained(FocusEvent evt) {
		//App.trace("WorkText.focusLost(e=" + evt.toString() + ")");
		mainFrame.showInfo(scene);
	}

	@Override
	public void focusLost(FocusEvent e) {
		//App.trace("WorkText.focusLost(e=" + e.toString() + ")");
		if (e.getSource() instanceof JTextComponent) {
			boolean modify = false;
			JTextComponent tc = (JTextComponent) e.getSource();
			switch (tc.getName()) {
				case "taTitle":
					if (!oTitle.equals(tc.getText())) {
						scene.setName(tc.getText());
						modify = true;
					}
					break;
				case "tcText":
					if (!oText.equals(tc.getText())) {
						scene.writing=(tc.getText());
						modify = true;
					}
					break;
			}
			if (modify) {
				mainFrame.getBookController().updateEntity(scene);
			}
		}
	}
}
