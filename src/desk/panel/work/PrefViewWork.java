/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.work;

import desk.app.App;
import desk.app.pref.AppPref;
import db.I18N;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PrefViewWork {

	private final AppPref pref;
	public final int WIDTH_DEFAULT=32, WIDTH_MIN=10, WIDTH_MAX=80;
	public final int HEIGHT_DEFAULT=11, HEIGHT_MIN=6, HEIGHT_MAX=50;
	public int height = HEIGHT_DEFAULT;
	public int width=WIDTH_DEFAULT;

	public PrefViewWork(AppPref pref) {
		this.pref=pref;
		load();
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.book", 3));
		b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("view.book.width", width));
		b.append(HtmlUtil.getAttribute("view.book.height", height));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.book")).append("\n");
		b.append(TextUtil.getAttribute("view.book.width", width));
		b.append(TextUtil.getAttribute("view.book.height", height));
		return (b.toString());
	}

	public void save() {
		pref.setString(AppPref.Key.VIEW_BOOK, width+","+height);
	}
	
	public final void load() {
		String v=pref.getString(AppPref.Key.VIEW_BOOK);
		if (v!=null && !v.isEmpty()) {
			String z[]=v.split(",");
			if (z.length>0) width=Integer.parseInt(z[0]);
			if (z.length>1) height=Integer.parseInt(z[1]);
		}
	}

	void setHeight(int val) {
		height=val;
		save();
	}

	void setWidth(int val) {
		width=val;
		save();
	}

}
