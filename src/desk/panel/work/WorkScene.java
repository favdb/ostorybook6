/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package desk.panel.work;

import java.beans.PropertyChangeEvent;

import javax.swing.JPanel;

import db.entity.Scene;
import desk.panel.AbstractScenePanel;
import desk.app.MainFrame;
import desk.tools.EntityUtil;
import desk.tools.swing.SwingUtil;

import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class WorkScene extends AbstractScenePanel {

	private JPanel cmdPanel;
	private WorkText textPanel;
	private WorkInfo infoPanel;

	public WorkScene(MainFrame mainFrame, Scene scene) {
		super(mainFrame, scene);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
		this.setName("WorkPanelcene:"+scene.name);
	}

	@Override
	public void initUi() {
		refresh();
	}

	@Override
	public void refresh() {
		MigLayout layout = new MigLayout("wrap 3,fill","[]","[top]"
		);
		setLayout(layout);
		setOpaque(false);
		setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, scene));

		removeAll();

		// info panel
		infoPanel = new WorkInfo(mainFrame, scene);

		// text panel
		textPanel = new WorkText(mainFrame, scene);

		// command panel
		cmdPanel = createCommandPanel();

		// layout
		add(infoPanel, "w 250,h 250");
		add(textPanel, "growx,gap 10");
		add(cmdPanel);

		revalidate();
		repaint();
	}

	private JPanel createCommandPanel() {
		JPanel panel = new JPanel(new MigLayout("flowy,insets 0"));
		panel.setOpaque(false);

		// layout
		panel.add(getEditButton());
		panel.add(getDeleteButton());
		panel.add(getNewButton());

		return panel;
	}

	protected WorkScene getThis() {
		return this;
	}
}
