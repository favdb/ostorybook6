/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.work;

import db.entity.AbstractEntity;
import db.entity.Scene;
import db.entity.Pov;
import desk.label.LabelPovDate;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.linkspanel.LinksItem;
import desk.panel.linkspanel.LinksLocation;
import desk.panel.linkspanel.LinksPerson;
import desk.panel.linkspanel.LinksPov;
import desk.tools.swing.SwingUtil;
import desk.tools.swing.label.CleverLabel;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class WorkInfo extends AbstractPanel {

	private final Scene scene;
	private CleverLabel lbPov;
	private LinksPerson personLinksPanel;
	private LinksItem itemLinksPanel;
	private LinksLocation locationLinksPanel;

	private LinksPov povLinksPanel;
	private LabelPovDate lbDate;

	public WorkInfo(MainFrame mainFrame, Scene scene) {
		super(mainFrame);
		this.scene = scene;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("WorkInfo.modelPropertyChange(evt=" + evt.toString() + ")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		if (newValue instanceof AbstractEntity) {
			switch (propName) {
				case "pov_Update":
					if (newValue instanceof Pov) {
						Pov newPov = (Pov) newValue;
						if (newPov.id.equals(scene.pov.id)) {
							lbPov.setText(newPov.toString());
							lbPov.setBackground(newPov.getJColor());
						}
						povLinksPanel.refresh();
					}
					return;

				case "scene_Update":
					Scene newScene = (Scene) newValue;
					if (!(newValue instanceof Scene)) {
						return;
					}
					if (!newScene.id.equals(scene.id)) {
						return;
					}
					lbDate.setDate(newScene.date);
					lbDate.refresh();
					if (newScene.pov != null) {
						lbPov.setText(newScene.pov.toString());
						lbPov.setBackground(newScene.pov.getJColor());
					}
					personLinksPanel.refresh();
					itemLinksPanel.refresh();
					locationLinksPanel.refresh();
					povLinksPanel.refresh();
			}
		}
	}

	@Override
	public void init() {
		//App.trace("WorkInfo.init()");
	}

	@Override
	public void initUi() {
		//App.trace("WorkInfo.initUi()");
		setLayout(new MigLayout("fillx,wrap,gapy 15", "", ""));
		setOpaque(false);
		setBorder(SwingUtil.getBorderDefault());

		Pov pov = scene.pov;

		// pov
		if (pov == null) {
			pov = book.povs.get(0);
		}
		lbPov = new CleverLabel(pov.toString(), JLabel.CENTER);
		lbPov.setBackground(pov.getJColor());

		// date
		lbDate = new LabelPovDate(pov, scene.date, mainFrame.bookPref.date_long);
		lbDate.setOpaque(false);

		// person links
		personLinksPanel = new LinksPerson(mainFrame, scene, "10");

		// location links
		locationLinksPanel = new LinksLocation(mainFrame, scene, false);

		// item links
		itemLinksPanel = new LinksItem(mainFrame, scene, false);

		// pov links
		JLabel lbPovLinks = new JLabel(I18N.getColonMsg("pov"));
		povLinksPanel = new LinksPov(mainFrame, scene, false);

		// layout
		add(lbPov, "growx");
		add(lbDate, "");
		Icon personIcon = (ImageIcon) IconUtil.getIcon("small/person");
		add(new JLabel(personIcon), "aligny top,split 2");
		add(personLinksPanel);
		Icon itemIcon = (ImageIcon) IconUtil.getIcon("small/item");
		add(new JLabel(itemIcon), "aligny top,split 2");
		add(itemLinksPanel);
		Icon locationIcon = (ImageIcon) IconUtil.getIcon("small/location");
		add(new JLabel(locationIcon), "aligny top,split 2");
		add(locationLinksPanel, "growx");
		Icon povIcon = (ImageIcon) IconUtil.getIcon("small/pov");
		add(new JLabel(povIcon), "split 2");
		add(povLinksPanel);
	}
}
