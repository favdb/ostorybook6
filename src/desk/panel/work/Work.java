/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.work;

import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import desk.app.App;
import desk.options.OptionsDlg;
import desk.panel.AbstractScrollPanel;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.view.SbView;
import desk.view.SbView.VIEWID;
import desk.view.ViewUtil;
import db.I18N;
import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class Work extends AbstractScrollPanel {

	public enum ACTION {

		WIDTH("Work_Width"),
		HEIGHT("Work_Height"),
		OPTIONS_CHANGE("Work_OptionsChange"),
		OPTIONS_SHOW("Work_OptionsShow"),
		SHOW_ENTITY("Work_ShowEntity"),
		PRINT("Work_Print"),
		REFRESH("Work_Refresh"),
		ZOOM("Work_Zoom"),
		NONE("none");

		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	PrefViewWork param;

	public Work(MainFrame m) {
		super(m);
		param = App.getInstance().getPreferences().book;
		initAll();
	}

	@Override
	public void init() {
		//App.trace("Work.init()");
		setZoomValues(param.width, 10, 200);
	}

	@Override
	public void initUi() {
		//App.trace("Work.initUi()");
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("flowy, ins 0"));
		add(initToolbar(WITH_PART), "span,wrap");
		toolbar.add(initSlider("view.work.width", param.WIDTH_MIN, param.WIDTH_MAX, param.width));
		toolbar.add(initSlider("view.work.height", param.HEIGHT_MIN, param.HEIGHT_MAX, param.height));
		panel = new JPanel(new MigLayout("flowy"));
		panel.setBackground(SwingUtil.getBackgroundColor());
		scroller = new JScrollPane(panel);

		SwingUtil.setUnitIncrement(scroller);
		SwingUtil.setMaxPreferredSize(scroller);
		add(scroller, "growx");

		refresh();
		ViewUtil.scrollToTop(scroller, 800);

		registerKeyboardAction();
		panel.addMouseWheelListener(this);

		revalidate();
		repaint();
	}

	public void setWidth(int val) {
		param.setWidth(val);
		refresh();
	}

	public void setHeight(int val) {
		param.setHeight(val);
		refresh();
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider slider = (JSlider) e.getSource();
		if (slider.getName().equals("view.work.width")) {
			if (!slider.getValueIsAdjusting()) {
				int val = slider.getValue();
				setWidth(val);
			}
		}
		if (slider.getName().equals("view.work.height")) {
			if (!slider.getValueIsAdjusting()) {
				int val = slider.getValue();
				setHeight(val);
			}
		}
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Work.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		Object newValue = evt.getNewValue();
		Object oldValue = evt.getOldValue();

		if ("scene_Init".equals(propName)) {
			refresh();
			return;
		}
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH: {
				SbView newView = (SbView) evt.getNewValue();
				SbView view = (SbView) getParent().getParent();
				if (view == newView) {
					refresh();
				}
				return;
			}
		}
		switch (getAction(propName)) {
			case OPTIONS_SHOW: {
				View view = (View) evt.getNewValue();
				if (!view.getName().equals(VIEWID.VIEW_WORK.toString())) {
					return;
				}
				OptionsDlg.show(mainFrame, view.getName());
				refresh();
				ViewUtil.scrollToTop(scroller);
				return;
			}
			case OPTIONS_CHANGE:
				refresh();
				return;
			case SHOW_ENTITY: {
				if (newValue instanceof Scene) {
					Scene scene = (Scene) newValue;
					ViewUtil.scrollToScene(this, panel, scene);
					return;
				}
				if (newValue instanceof Chapter) {
					Chapter chapter = (Chapter) newValue;
					ViewUtil.scrollToChapter(this, panel, chapter);
					return;
				}
			}
		}
		switch (propName) {
			case "scene_Update": {
				Scene oldScene = (Scene) oldValue;
				Scene newScene = (Scene) newValue;
				if (oldScene == null || newScene == null) {
					break;
				}
				if (!oldScene.id.equals(newScene.id)) {
					return;
				}
				if (!oldScene.getChapterSceneNo(false).equals(newScene.getChapterSceneNo(false))) {
					refresh();
					return;
				}
			}
			case "pov_Delete":
			case "pov_DeleteMulti":
				refresh();
				return;
			case "part_Change":
				refresh();
				ViewUtil.scrollToTop(scroller);
				return;
		}

		dispatchToWorkkInfoPanels(this, evt);
		dispatchToWorkTextPanels(this, evt);
	}

	@Override
	public void refresh() {
		Part part = mainFrame.getCurrentPart();
		if (book == null) {
			return;
		}
		List<Chapter> chapters = book.chapters;
		if (!mainFrame.showAllParts) {
			chapters = Chapter.orderByNumber(book.chapters, part);
		}
		panel.removeAll();
		chapters.forEach((chapter) -> {
			List<Scene> scenes = Scene.findByNumber(book.scenes, chapter);
			scenes.forEach((scene) -> {
				WorkScene scenePanel = new WorkScene(mainFrame, scene);
				panel.add(scenePanel, "sgx");
			});
		});
		if (panel.getComponentCount() == 0) {
			panel.add(new JLabel(I18N.getMsg("scenes.warning")));
		}
		ViewUtil.scrollToTop(scroller);
		panel.revalidate();
	}

	private static void dispatchToWorkkInfoPanels(Container cont, PropertyChangeEvent evt) {
		List<Component> ret = SwingUtil.findComponentsByClass(cont, WorkInfo.class);
		ret.stream().filter((comp) -> (comp instanceof WorkInfo)).forEachOrdered((comp) -> {
			((WorkInfo) comp).modelPropertyChange(evt);
		});
	}

	private static void dispatchToWorkTextPanels(Container cont, PropertyChangeEvent evt) {
		List<Component> ret = SwingUtil.findComponentsByClass(cont, WorkText.class);
		ret.stream().map((comp) -> (WorkText) comp).forEachOrdered((panel) -> {
			panel.modelPropertyChange(evt);
		});
	}

	public JPanel getPanel() {
		return panel;
	}

}
