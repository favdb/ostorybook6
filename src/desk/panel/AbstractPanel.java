package desk.panel;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Part;
import desk.app.MainFrame;
import desk.tools.combobox.ComboLoad;
import desk.interfaces.IPaintable;
import desk.interfaces.IRefreshable;
import db.I18N;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

@SuppressWarnings("serial")
public abstract class AbstractPanel extends JPanel implements ActionListener, ChangeListener, IRefreshable, IPaintable {
	
	public static boolean NEW=true, EMPTY=true, ALL=true;
	public static final boolean WITH_PART=false;
	public MainFrame mainFrame;
	public Book book;
	public JComboBox cbListPart;
	public Part selPart;
	public JToolBar toolbar;
	
	public AbstractPanel() {
	}
	
	@SuppressWarnings("OverridableMethodCallInConstructor")
	public AbstractPanel(MainFrame mainframe) {
		mainFrame = mainframe;
		book = mainFrame.book;
		this.setName(this.getClass().getSimpleName());
	}

	@Override
	public abstract void init();
	
	@Override
	public abstract void initUi();
	
	public abstract void modelPropertyChange(PropertyChangeEvent evt);
	
	public final void initAll() {
		init();
		initUi();
	}
	
	public JToolBar initToolbar(boolean withPart) {
		toolbar = new JToolBar();
		toolbar.setBorder(BorderFactory.createEmptyBorder());
		toolbar.setOpaque(false);
		toolbar.setLayout(new MigLayout("ins 0"));
		toolbar.setFloatable(false);
		if (book.parts.size() > 1 && withPart) {
			JLabel lb = new JLabel(I18N.getColonMsg("part"));
			cbListPart = new JComboBox();
			cbListPart.setName("cbPartList");
			ComboLoad.loadCbParts(mainFrame, cbListPart, mainFrame.getCurrentPart(), true);
			cbListPart.addActionListener((ActionEvent evt) -> {
				changePart();
			});
			toolbar.add(lb);
			toolbar.add(cbListPart);
		}
		return (toolbar);
	}
	
	public void changePart() {
		if (cbListPart.getSelectedIndex() == -1) {
			selPart = null;
		} else {
			selPart = (Part) cbListPart.getSelectedItem();
		}
		refresh();
	}
	
	public JPanel initSlider(String title, int min, int max, int val) {
		//App.trace("AbstractPanel.initSlider("+title+", min="+min+", max="+max+", val="+val+")");
		int v = val;
		if (val < min) {
			v = min;
		}
		if (val > max) {
			v = max;
		}
		JSlider slider = new JSlider(JSlider.HORIZONTAL, min, max, v);
		slider.setName(title);
		slider.setOpaque(false);
		slider.addChangeListener(this);
		JPanel p = new JPanel(new MigLayout());
		p.add(new JLabel(I18N.getColonMsg(title)));
		p.add(slider);
		return (p);
	}
	
	public JButton initButton(String name, String text, String icon, String... tooltip) {
		//App.trace("AbstractPanel.initButton(name=" + name + ", text=" + text + ", icon=" + icon + ")");
		JButton btn = new JButton();
		btn.setName(name);
		if (text != null && !text.isEmpty()) {
			btn.setText(I18N.getMsg(text));
		}
		if (icon != null && !icon.isEmpty()) {
			btn.setIcon(IconUtil.getIcon(icon));
		}
		if (tooltip.length>0) {
			btn.setToolTipText(I18N.getMsg(tooltip[0]));
		}
		btn.addActionListener(this);
		return (btn);
	}
	
	public JButton initButtonIdea() {
		return(initButton("btIdea","","small/idea","idea.new"));
	}
	
	public JCheckBox initCheckBox(String title, boolean sel) {
		//App.trace("AbstractPanel.initCheckBox("+title+", sel="+(sel?"true":"false")+")");
		JCheckBox cb = new JCheckBox();
		cb.setName(title);
		cb.setText(I18N.getMsg(title));
		cb.setOpaque(false);
		cb.setSelected(sel);
		cb.addActionListener(this);
		return (cb);
	}
	
	public JComboBox initComboBox(String title) {
		//App.trace("AbstractPanel.initComboBox("+title+")");
		JComboBox cb = new JComboBox();
		cb.setName(title);
		cb.setOpaque(false);
		cb.addActionListener(this);
		return (cb);
	}
	
	public JComboBox initComboBox(String title, List<?> list, AbstractEntity toSel) {
		return(initComboBox(title, "", list, toSel, !EMPTY, !ALL));
	}
	
	@SuppressWarnings("unchecked")
	public JComboBox initComboBox(String title, String tool, List<?> list, AbstractEntity toSel, boolean isEmpty, boolean isAll) {
		//App.trace("AbstractPanel.initComboBox(title="+title+", list="+list+", toSel"+"...")");
		JComboBox cb = new JComboBox();
		cb.setName(title);
		if (tool!=null && !tool.isEmpty()) {
			cb.setToolTipText(I18N.getMsg(tool));
		}
		cb.setOpaque(false);
		if (isEmpty) {
			cb.addItem("");
		}
		if (isAll) {
			cb.addItem(I18N.getMsg("z.all"));
		}
		if (list!=null && !list.isEmpty()) {
			list.forEach((e)-> {
				AbstractEntity entity=(AbstractEntity)e;
				cb.addItem(entity);
			});
			if (toSel!=null) {
				cb.setSelectedItem(toSel);
			} else {
				cb.setSelectedIndex(0);
			}
		}
		cb.addActionListener(this);
		return (cb);
	}
	
	@Override
	public void refresh() {
		removeAll();
		initAll();
		invalidate();
		validate();
		repaint();
	}
	
	public MainFrame getMainFrame() {
		return mainFrame;
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		if (e.getSource() instanceof JSlider) {
			JSlider slider = (JSlider) e.getSource();
			if (!slider.getValueIsAdjusting()) {
				int val = slider.getValue();
				//zoom(val);
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
	}
	
}
