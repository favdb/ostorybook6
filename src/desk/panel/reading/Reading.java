/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.reading;

import db.entity.Book;
import desk.app.App;
import desk.exporter.ExportBook;
import desk.options.OptionsDlg;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;
import tools.html.HtmlUtil;

/**
 * @author favdb
 *
 */
@SuppressWarnings("serial")
public class Reading extends AbstractPanel implements HyperlinkListener {

	public enum ACTION {
		FONTSIZE("Reading_Fontsize"),
		OPTIONS_CHANGE("Reading_OptionsChange"),
		OPTIONS_SHOW("Reading_OptionsShow"),
		REFRESH("Reading_Refresh"),
		ZOOM("Reading_Zoom"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private JTextPane tpText;
	private JScrollPane scroller;
	private ReadingPov povPanel;
	private PrefViewReading param;
	private JComboBox cbFontSize;

	public Reading(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
		param = App.getInstance().getPreferences().reading;
		povPanel = new ReadingPov(mainFrame, this);
		povPanel.init();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("flowx, ins 1", "[][fill,grow]", ""));
		add(initToolbar(WITH_PART), "wrap, span,growx");
		toolbar.add(new JLabel(I18N.getColonMsg("view.reading.font_size")));
		cbFontSize = new JComboBox();
		cbFontSize.setName("view.reading.font_size");
		int fnt[] = {8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 24, 28, 32, 36, 40};
		for (int i : fnt) {
			cbFontSize.addItem(Integer.toString(i));
		}
		cbFontSize.setSelectedItem(Integer.toString(param.fontSize));
		cbFontSize.addActionListener((ActionEvent e) -> {
			int val = Integer.parseInt((String) cbFontSize.getSelectedItem());
			param.fontSize = val;
			param.save();
			mainFrame.setModified();
			refresh();
		});
		toolbar.add(cbFontSize);
		JButton btOptions = SwingUtil.initButton("", "small/preferences", "options");
		btOptions.addActionListener((ActionEvent e) -> {
			OptionsDlg.show(mainFrame, VIEWID.VIEW_READING.toString());
			refresh();
		});
		toolbar.add(btOptions);

		povPanel.initUi();
		add(povPanel, "aligny top");

		tpText = new JTextPane();
		tpText.setEditable(false);
		tpText.setContentType("text/html");
		tpText.addHyperlinkListener(this);
		scroller = new JScrollPane(tpText);
		SwingUtil.setMaxPreferredSize(scroller);

		add(scroller, "growy");
		refresh();
	}

	@Override
	public void refresh() {
		//App.trace("Reading.refresh()");
		StringBuilder buf = new StringBuilder();
		buf.append(HtmlUtil.getHeadWithCSS(mainFrame.getFont()));
		if (param.fontSize == 0) {
			buf.append("<body>\n<p style='font-weight:bold'>");
		} else {
			buf.append("<body style=\"font-size:")
					.append(param.fontSize)
					.append("px\">\n<p style='font-weight:bold'>");
		}
		buf.append(ExportBook.toPanel(mainFrame, selPart));
		buf.append("<p>&nbsp;</p>\n</body>\n</html>\n");
		final int pos = scroller.getVerticalScrollBar().getValue();
		tpText.setText(buf.toString());
		final Action restoreAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				scroller.getVerticalScrollBar().setValue(pos);
			}
		};
		SwingUtilities.invokeLater(() -> {
			restoreAction.actionPerformed(null);
		});
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent evt) {
		if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			try {
				if (!evt.getDescription().isEmpty()) {
					tpText.scrollToReference(evt.getDescription().substring(1));
				} else {
					tpText.setPage(evt.getURL());
				}
			} catch (IOException e) {
			}
		}
	}

	private static void dispatchToPovPanels(Container cont, PropertyChangeEvent evt) {
		List<Component> list = SwingUtil.findComponentsByClass(cont, ReadingPov.class);
		list.forEach((panel) -> {
			if (panel instanceof ReadingPov) {
				((ReadingPov) panel).modelPropertyChange(evt);
			}

		});
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Reading.modelPropertyChange(evt=" + evt.toString() + ")");
		String propName = evt.getPropertyName();
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				refresh();
				return;
		}
		switch (getAction(propName)) {
			case OPTIONS_CHANGE:
			case FONTSIZE:
			case ZOOM:
				refresh();
				return;
			case OPTIONS_SHOW:
				View view = (View) evt.getNewValue();
				if (!view.getName().equals(VIEWID.VIEW_READING.toString())) {
					return;
				}
				OptionsDlg.show(mainFrame, view.getName());
				return;
		}
		switch (Ctrl.getEntity(propName)) {
			case CHAPTER:
			case SCENE:
				refresh();
				return;
		}
		dispatchToPovPanels(this, evt);
		if (Ctrl.getEntity(propName).equals(Book.TYPE.POV)) {
			refresh();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e) {
	}

}
