package desk.panel.reading;

import db.entity.Pov;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import db.I18N;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class ReadingPov extends AbstractPanel implements ItemListener {

	private final Reading bookReading;
	private List<Long> povIds;
	private List<JCheckBox> cbListPov;

	public ReadingPov(MainFrame mainFrame, Reading reading) {
		super(mainFrame);
		this.bookReading = reading;
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("ReadingPov.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		if ("pov_Update".equals(propName)) {
			refresh();
		}
	}

	@Override
	public void init() {
		povIds = new ArrayList<>();
		cbListPov = new ArrayList<>();
		addAllPovs();
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("wrap"));
		setBackground(Color.white);
		setBorder(SwingUtil.getBorderDefault());
		
		List<Pov> list = book.povs;
		list.forEach((pov)-> {
			JCheckBox cb = new JCheckBox(pov.toString());
			cbListPov.add(cb);
			long id = pov.id;
			if (povIds.contains(id)) {
				cb.setSelected(true);
			}
			cb.setName(Long.toString(id));
			cb.setOpaque(false);
			cb.addItemListener(this);
			add(new JLabel(pov.getColorIcon()), "split 2");
			add(cb);
		});

		JButton btAll = new JButton(getSelectAllAction());
		btAll.setText(I18N.getMsg("show.all"));
		btAll.setName("all");
		btAll.setOpaque(false);
		add(btAll, "sg,gapy 20, al center");

		JButton cbNone = new JButton(getSelectNoneAction());
		cbNone.setText(I18N.getMsg("show.none"));
		cbNone.setName("none");
		cbNone.setOpaque(false);
		add(cbNone, "sg,al center");
	}

	private AbstractAction getSelectAllAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addAllPovs();
				cbListPov.forEach((cb) -> {
					cb.setSelected(true);
				});
				bookReading.refresh();
			}
		};
	}

	private AbstractAction getSelectNoneAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				povIds.clear();
				cbListPov.forEach((cb) -> {
					cb.setSelected(false);
				});
				bookReading.refresh();
			}
		};
	}

	private void addAllPovs() {
		book.povs.forEach((pov) -> {
			povIds.add(pov.id);
		});
	}

	@Override
	public void itemStateChanged(ItemEvent evt) {
		try {
			JCheckBox cb = (JCheckBox) evt.getSource();
			Long id = Long.parseLong(cb.getName());
			if (cb.isSelected()) {
				povIds.add(id);
			} else {
				povIds.remove(id);
			}
			bookReading.refresh();
		} catch (NumberFormatException e) {
			System.err.println(Arrays.toString(e.getStackTrace()));
		}
	}

}
