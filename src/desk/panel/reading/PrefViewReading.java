/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.reading;

import desk.app.pref.AppPref;
import db.I18N;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PrefViewReading {

	private static final int DEFAULT_ZOOM = 40;
	private static final int DEFAULT_FONTSIZE = 12;
	private static final String DEFAULT_OPTIONS = "0000000000";

	public int zoom = DEFAULT_ZOOM;
	public int fontSize = DEFAULT_FONTSIZE;
	public boolean partTitle = false, partNumber = false, partRoman = false,
			chapterTitle = false, chapterNumber = false, chapterRoman = false, chapterDateLoc = false,
			sceneTitle = false, sceneDidascalie = false, sceneSeparator = false;
	private final AppPref pref;

	public PrefViewReading(AppPref pref) {
		this.pref = pref;
		load();
	}

	public int getZoom() {
		return (zoom);
	}

	public void setZoom(int v) {
		zoom = v;
	}

	public int getFontsize() {
		return (fontSize);
	}

	public void setFontsize(int v) {
		fontSize = v;
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.reading", 3));
		b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("view.reading.zoom", zoom));
		b.append(HtmlUtil.getAttribute("view.reading.font_size", fontSize));
		b.append(HtmlUtil.getAttribute("view.reading.options",computeOptions()));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.reading"));
		b.append("\n");
		b.append(TextUtil.getAttribute("view.reading.zoom", zoom));
		b.append(TextUtil.getAttribute("view.reading.font_size", fontSize));
		b.append(TextUtil.getAttribute("view.reading.options",computeOptions()));
		b.append("\n");
		return (b.toString());
	}

	private String computeOptions() {
		String z="";
		z+=(partTitle?"1":"0");
		z+=(partNumber?"1":"0");
		z+=(partRoman?"1":"0");
		z+=(chapterTitle?"1":"0");
		z+=(chapterNumber?"1":"0");
		z+=(chapterRoman?"1":"0");
		z+=(chapterDateLoc?"1":"0");
		z+=(sceneTitle?"1":"0");
		z+=(sceneDidascalie?"1":"0");
		z+=(sceneSeparator?"1":"0");
		return(z);
	}
	public void save() {
		StringBuilder b = new StringBuilder();
		b.append(zoom + "," + fontSize + ",");
		b.append(computeOptions());
		pref.setString(AppPref.Key.VIEW_READING, b.toString());
	}

	public void load() {
		String v = pref.getString(AppPref.Key.VIEW_READING);
		String o=DEFAULT_OPTIONS;
		if (v != null && !v.isEmpty()) {
			String z[] = v.split(",");
			if (z.length > 0) {
				zoom = Integer.parseInt(z[0]);
			}
			if (z.length > 1) {
				fontSize = Integer.parseInt(z[1]);
			}
			if (z.length > 2) {
				o=(z[2]);
			}
		}
		partTitle = (o.charAt(0)=='1');
		partNumber = (o.charAt(1)=='1');
		partRoman = (o.charAt(2)=='1');
		chapterTitle = (o.charAt(3)=='1');
		chapterNumber = (o.charAt(4)=='1');
		chapterRoman = (o.charAt(5)=='1');
		chapterDateLoc = (o.charAt(6)=='1');
		sceneTitle = (o.charAt(7)=='1');
		sceneDidascalie = (o.charAt(8)=='1');
		sceneSeparator = (o.charAt(9)=='1');
	}

}
