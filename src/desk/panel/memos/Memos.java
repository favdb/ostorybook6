/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.memos;

import db.entity.Memo;
import desk.app.App;
import desk.app.MainFrame;
import desk.dialog.ConfirmDeleteDlg;
import desk.net.NetUtil;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class Memos extends AbstractPanel implements ActionListener, ListSelectionListener, HyperlinkListener {

	private JScrollPane infoScroller;

	public enum ACTION {
		BT_DELETE("btDelete"),
		BT_EDIT("btEdit"),
		BT_NEW("btNew"),
		DIRECTION("view.memo.direction"),
		PRINT("Memo_Print"),
		REFRESH("Refresh"),
		SHOW_MEMO("Memo_Show"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private JComboBox memoCombo = null;//liste deroulante des memos
	private JList memoList = null;//liste des memos
	private JButton btNew;// bouton nouveau
	private JButton btDelete;// bouton supprimer
	private JButton btEdit;// bouton modifier
	private boolean processActionListener;// listener à ignorer
	private JTextPane infoPanel;// le panneau d'information
	private Memo current;// le memo actuellement affiché
	private PrefViewMemo param;
	private JScrollPane memoScroller;
	private JCheckBox cbLeft;


	public Memos(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
		//App.trace("Memos.init()");
		param = App.getInstance().getPreferences().memo;
		current = null;
	}

	@Override
	public void initUi() {
		//App.trace("Memos.initUi()");
		MigLayout migLayout1 = new MigLayout("flowx, ins 1", "[20%][80%]", "[][grow]");
		setLayout(migLayout1);
		memoList = new JList<>();
		memoList.addListSelectionListener(this);

		add(initToolbar(!WITH_PART), "wrap, span,growx");
		btNew=this.initButton(ACTION.BT_NEW.toString(), "", "small/new");
		toolbar.add(btNew);
		btEdit=this.initButton(ACTION.BT_EDIT.toString(), "", "small/edit");
		btEdit.setEnabled(false);
		toolbar.add(btEdit);
		btDelete=this.initButton(ACTION.BT_DELETE.toString(), "", "small/delete");
		btDelete.setEnabled(false);
		toolbar.add(btDelete);
		cbLeft=this.initCheckBox("view.memo.layoutdirection.left", !param.isLeft);
		toolbar.add(cbLeft);
		memoCombo = new JComboBox();
		toolbar.add(memoCombo);
		memoScroller = new JScrollPane(memoList);
		SwingUtil.setMaxPreferredSize(memoScroller);

		infoPanel = new JTextPane();
		infoPanel.setEditable(false);
		infoPanel.setOpaque(true);
		infoPanel.setContentType("text/html");
		infoPanel.addHyperlinkListener(this);
		infoScroller = new JScrollPane(infoPanel);
		infoScroller.setPreferredSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
		add(memoScroller);
		add(infoScroller, "span");
		if (book.memos.size()>0) current=book.memos.get(0);
		refreshList();
		refreshControl();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Memos.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch (getAction(propName)) {
			case REFRESH: {
				View newView = (View) newValue;
				View view = (View) getParent().getParent();
				if (view == newView) {
					cbLeft.setSelected(param.isLeft);
					refreshControl();
				}
				break;
			}
			case SHOW_MEMO: {
				if (newValue instanceof Memo) {
					current = (Memo) newValue;
					refreshMemo();
					break;
				}
			}
		}
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
		//App.trace("Memos.hyperlinkIpdate(...)");
		try {
			if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
				NetUtil.openBrowser(e.getURL().toString());
			}
		} catch (Exception exc) {
			System.err.println("InfoPanel.hyperlinkUpdate(" + e.toString() + ") Exception : " + exc.getMessage());
		}
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		//App.trace("Memos.actionPerformed(" + evt.toString() + ")");
		if ((evt.getSource() == null) || (!processActionListener)) {
			return;
		}
		if (evt.getSource() instanceof JCheckBox) {
			//disposition=cbDisposition.isSelected();
			refreshControl();
		}
		if (evt.getSource() instanceof JButton) {
			String btn = ((JButton) evt.getSource()).getName();
			switch (getAction(btn)) {
				case BT_EDIT: {
					if (mainFrame.showEditorAsDialog(current)) return;
					refresh();
					return;
				}
				case BT_NEW: {
					if (mainFrame.showEditorAsDialog(new Memo())) return;
					refresh();
					int i = memoList.getModel().getSize();
					if (!cbLeft.isSelected()) {
						memoList.setSelectedIndex(i - 1);
					} else {
						memoCombo.setSelectedIndex(i - 1);
					}
					return;
				}
				case BT_DELETE: {
					if (current==null) return;
					boolean act = ConfirmDeleteDlg.show(mainFrame, current,null);
					if (act) {
						mainFrame.getBookController().deleteEntity(current);
						current=null;
						refreshList();
					}
					return;
				}
			}
			if (evt.getSource() instanceof JComboBox) {
				current = (Memo) memoCombo.getSelectedItem();
				memoList.setSelectedValue(current,true);
			}
			refreshMemo();
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		//App.trace("Memos.valueChanged(e="+e.toString()+")");
		current = (Memo) memoList.getSelectedValue();
		memoCombo.setSelectedItem(current);
		refreshMemo();
	}

	private void refreshMemo() {
		//App.trace("Memos.refreshMemo(" + (current != null ? current.toString() : "null") + ")");
		if (current == null) {
			infoPanel.setText("");
			btEdit.setEnabled(false);
			btDelete.setEnabled(false);
		} else {
			infoPanel.setText(current.getNotes());
			btEdit.setEnabled(true);
			btDelete.setEnabled(true);
		}
		infoPanel.setCaretPosition(0);
	}

	@SuppressWarnings({"unchecked", "unchecked", "unchecked", "unchecked"})
	private void refreshList() {
		//App.trace("Memos.refreshList()");
		List<Memo> memos = book.memos;
		processActionListener = false;
		DefaultListModel model = new DefaultListModel();
		model.removeAllElements();
		memos.forEach((memo)-> {
			model.addElement(memo);
		});
		memoList.setModel(model);
		if (current!=null) memoList.setSelectedValue(current, true);
		memoList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		memoCombo.removeAllItems();
		memos.forEach((memo)-> {
			memoCombo.addItem(memo);
		});
		if (current!=null) memoCombo.setSelectedItem(current);
		processActionListener = true;
	}

	private void refreshControl() {
		//App.trace("Memos.refreshControl() for cbDisposition="+(cbLeft.isSelected()?"true":"false")+")");
		this.remove(infoScroller);
		if (cbLeft.isSelected()) {
			this.add(memoScroller);
			memoCombo.setVisible(false);
		} else {
			this.remove(memoScroller);
			memoCombo.setVisible(true);
		}
		this.add(infoScroller,"span");
		revalidate();
	}

}
