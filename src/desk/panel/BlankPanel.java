/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel;

import db.I18N;
import desk.app.App;
import desk.app.MainFrame;
import desk.net.NetUtil;
import desk.tools.swing.SwingUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import lib.miginfocom.swing.MigLayout;
import tools.ColorUtil;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class BlankPanel extends AbstractPanel implements HyperlinkListener {

	public BlankPanel(MainFrame mainFrame) {
		super(mainFrame);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("wrap", "[center]"));
		SwingUtil.setForcedSize(this, new Dimension(640,480));
		//setPreferredSize(new Dimension(800, 600));
		setBackground(new Color(0xb9, 0xcf, 0x70));
		setBorder(BorderFactory.createLoweredBevelBorder());

		removeAll();

		JLabel lbLogo = new JLabel(IconUtil.getIcon(App.class,"logo500_blurred"/*,new Dimension(500,168)*/));

		add(lbLogo,"north");

		JLabel lbVersion = new JLabel();
		lbVersion.setText(App.FULLNAME);
		//lbVersion.setForeground(new Color(0xb9, 0xcf, 0x70));
		add(lbVersion);

		JTextPane tp = new JTextPane();
		String gpl = "<html><body "+SwingUtil.fontToHtml(tp)+">";
		gpl +="<p>" + I18N.getMsg("about.gpl.distribution") + "</p>"
			+ "<p>" + I18N.getMsg("about.gpl.copyright") + " "+ App.COPYRIGHT_YEAR + "</p>"
			+ "<p>" + I18N.getMsg("about.gpl.homepage") + " "+ NetUtil.URL_HOME + "</p>"
			+ "<p>" + I18N.getMsg("about.gpl.gpl") + "</p>"
			+ "</body></html>";
		tp.setContentType("text/html");
		tp.setEditable(false);
		tp.setText(gpl);
		JScrollPane scroller = new JScrollPane(tp);
		scroller.setBorder(BorderFactory.createLineBorder(ColorUtil.darker(Color.green,0.5)));
		tp.setCaretPosition(0);
		add(scroller);
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent evt) {
		App.trace("BlankPanel.hyperlinkUdate(evt="+evt.toString()+")");
		if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			if (!evt.getDescription().isEmpty()) {
			} else {
				NetUtil.openBrowser(evt.getURL().toString());
			}
		}
	}
}
