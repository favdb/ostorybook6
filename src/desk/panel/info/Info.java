/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.panel.info;

import db.entity.AbstractEntity;
import db.entity.Scene;
import desk.app.App;
import desk.net.NetUtil;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;
import desk.tools.EntityUtil;
import desk.tools.IOTools;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class Info extends AbstractPanel implements HyperlinkListener {

	private boolean isEntity;

	public enum ACTION {
		REFRESH("Info_Refresh"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getACTION(String str) {
		String z = (str.contains("none_") ? str.replace("none_", "") : str);
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(z)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private JTextPane infoPane;
	private AbstractEntity entity;

	public Info(MainFrame m) {
		super(m);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("wrap,fill,ins 0"));

		infoPane = new JTextPane();
		infoPane.setEditable(false);
		infoPane.setOpaque(true);
		infoPane.setContentType("text/html");
		infoPane.addHyperlinkListener(this);
		JScrollPane scroller = new JScrollPane(infoPane);
		SwingUtil.setMaxPreferredSize(scroller);
		add(scroller);

		if (entity != null) {
			refreshInfo();
		}
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Info.modelPropertyChange("+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch (getACTION(propName)) {
			case REFRESH:
				View newView = (View) newValue;
				View view = (View) getParent().getParent();
				if (view == newView) {
					refresh();
				}
				return;
		}
		if (entity != null && newValue instanceof AbstractEntity) {
			AbstractEntity updatedEntity = (AbstractEntity) newValue;
			if (updatedEntity.id.equals(entity.id)) {
				setEntity(updatedEntity);
				refreshInfo();
			}
		}
	}

	public void setEntity(AbstractEntity entity) {
		this.entity = entity;
		isEntity=true;
		refreshInfo();
	}

	public void setBook() {
		App.trace("Info.setBook()");
		isEntity=false;
		refreshInfo();
	}

	private void refreshInfo() {
		//App.trace("Info.refeshInfo() entity="+entity.name);
		Font f = mainFrame.getFont();
		String head = "<html><body style=\""
				+ "font-family: " + f.getFamily() + ",sans-serif; font-size: 9px"
				+ "\">";
		if (isEntity) infoPane.setText(head + entity.toHtmlDetail() + "</body></html>");
		else infoPane.setText(head + book.toHtmlDetail() + "</body></html>");
		infoPane.setCaretPosition(0);
		infoPane.setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, entity));
		for (int i = 0; i < this.getComponentCount(); i++) {
			Component comp = this.getComponent(i);
			if (comp instanceof JButton) {
				if (comp.getName().equals("btExternal")) {
					this.remove(comp);
					break;
				}
			}
		}
		if (entity instanceof Scene) {
			mainFrame.setLastUsedScene((Scene) entity);
			if (mainFrame.bookPref.isUseXeditor()) {
				JButton btExternal = new JButton(I18N.getMsg("xeditor.launching"));
				btExternal.setName("btExternal");
				btExternal.addActionListener((ActionEvent evt) -> {
					IOTools.launch(mainFrame, (Scene)entity);
				});
				add(btExternal);
			}
		}
		revalidate();
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
		try {
			if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
				NetUtil.openBrowser(e.getURL().toString());
			}
		} catch (Exception exc) {
			System.err.println("Info.hyperlinkUpdate(" + e.toString() + ") Exception : " + exc.getMessage());
		}
	}

}
