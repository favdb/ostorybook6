/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage;

import db.entity.Chapter;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.action.EntityAction;
import desk.interfaces.IRefreshable;
import desk.panel.AbstractPanel;
import desk.tools.EntityUtil;
import desk.tools.swing.SwingUtil;
import desk.tools.swing.label.VerticalLabelUI;
import db.I18N;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import lib.miginfocom.swing.MigLayout;

/*
Ajout de l'ouverture de l'éditeur sur double click sur le titre du chapitre
*/

@SuppressWarnings("serial")
public class ManageChapter extends AbstractPanel implements MouseListener, IRefreshable {

	private Chapter chapter;
	private ManageSceneHandler sceneTransferHandler;
	private int prefWidth;

	public ManageChapter(MainFrame mainFrame) {
		this(mainFrame, null);
	}

	public ManageChapter(MainFrame mainFrame, Chapter chapter) {
		super(mainFrame);
		this.chapter = chapter;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("ChapterPanel.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		Object oldValue = evt.getOldValue();
		String propName = evt.getPropertyName();

		if (Manage.ACTION.ZOOM.check(propName)) {
			refresh();
			return;
		}
		switch(propName) {
			case "pov_Update":
				refresh();
				return;
			case "chapter_Update":
				if(chapter == null){
					return;
				}
				Chapter newChapter = (Chapter) newValue;
				if (!newChapter.id.equals(chapter.id)) {
					return;
				}
				chapter = newChapter;
				refresh();
				return;
			case "scene_Update":
				refresh();
				break;
		}
	}

	@Override
	public void init() {
		//App.trace("ChapterPanel.init()");
	}

	@Override
	public void initUi() {
		//App.trace("ChapterPanel.initUI()");
		MigLayout layout;
		if (isForUnassignedScene()) {
			layout = new MigLayout("flowx", "[]", "[fill]");
		} else {
			layout = new MigLayout("flowy", "[]", "[]4[]0[]");
		}
		setLayout(layout);
		setBorder(SwingUtil.getBorderDefault());
		if (!isForUnassignedScene()) {
			//setPreferredSize(new Dimension(prefWidth, 80));
		}
		setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, chapter));

		StringBuilder buf = new StringBuilder();
		if (chapter == null) {
			JLabel lbChapter = new JLabel();
			buf.append(I18N.getMsg("scene.unassigned"));
			lbChapter.setText(buf.toString());
			lbChapter.setUI(new VerticalLabelUI(false));
			lbChapter.addMouseListener(this);
			add(lbChapter);
		} else {
			JTextArea lbChapter = new JTextArea();
			buf.append(chapter.number.toString());
			buf.append(" ");
			buf.append(chapter.name);
			lbChapter.setText(buf.toString());
			lbChapter.setLineWrap(true);
			lbChapter.setWrapStyleWord(true);
			lbChapter.setToolTipText(chapter.name);
			add(lbChapter, "growx");
			this.setToolTipText(chapter.name);
		}

		sceneTransferHandler = new ManageSceneHandler(mainFrame);

		List<Scene> scenes = Scene.find(book.scenes, chapter);
		scenes=Scene.orderByNumber(scenes);
		if (chapter == null) {
			// show all unassigned scenes
			for (Scene scene : scenes) {
				ManageSceneDT dtScene = new ManageSceneDT(mainFrame, scene);
				dtScene.setTransferHandler(sceneTransferHandler);
				add(dtScene, "growy");
			}
			// to make a scene unassigned
			ManageSceneDT makeUnassigned = new ManageSceneDT(mainFrame, ManageScene.MAKE_UNASSIGNED);
			makeUnassigned.setTransferHandler(sceneTransferHandler);
			add(makeUnassigned);
		} else {
			ManageSceneDT begin = new ManageSceneDT(mainFrame, ManageScene.BEGIN);
			begin.setTransferHandler(sceneTransferHandler);
			if (scenes.isEmpty()) {
				SwingUtil.setMaxPreferredSize(begin);
			} else {
				begin.setPreferredSize(new Dimension(Short.MAX_VALUE, 15));
			}
			add(begin);

			int i = 0;
			for (Scene scene : scenes) {
				// scene
				ManageSceneDT dtScene = new ManageSceneDT(mainFrame, scene);
				dtScene.setTransferHandler(sceneTransferHandler);
				add(dtScene, "growx");

				// move next
				ManageSceneDT next = new ManageSceneDT(mainFrame, ManageScene.NEXT);
				if (scene.number != null) {
					next.setPreviousNumber(scene.number);
				}
				next.setTransferHandler(sceneTransferHandler);
				if (i < scenes.size() - 1) {
					next.setPreferredSize(new Dimension(Short.MAX_VALUE, 15));
				} else {
					SwingUtil.setMaxPreferredSize(next);
				}
				add(next);
				++i;
			}
		}
	}

	public boolean isForUnassignedScene() {
		return chapter == null;
	}

	protected ManageChapter getThis() {
		return this;
	}

	public Chapter getChapter() {
		return chapter;
	}

	/**
	 * Gets all {@link ManageSceneDT} that have a scene assigned.
	 *
	 * @return a list of all {@link ManageSceneDT}
	 * @see ManageSceneDT
	 */
	public List<ManageSceneDT> getDTScenePanels() {
		List<ManageSceneDT> list = new ArrayList<>();
		for (Component comp : getComponents()) {
			if (comp instanceof ManageSceneDT && ((ManageSceneDT) comp).getScene() != null) {
				list.add((ManageSceneDT) comp);
			}
		}
		return list;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (getChapter() == null) {
			return;
		}
		requestFocusInWindow();
		if (e.getClickCount() == 2) {
			Action act = EntityAction.edit(mainFrame, chapter,false);
			act.actionPerformed(null);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}
}
