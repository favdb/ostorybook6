/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage;

import db.entity.Chapter;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.options.OptionsDlg;
import desk.panel.AbstractScrollPanel;
import desk.tools.swing.SwingUtil;
import desk.view.SbView.VIEWID;
import desk.view.ViewUtil;
import db.I18N;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class Manage extends AbstractScrollPanel {

	public enum ACTION {

		OPTIONS_CHANGE("Manage_OptionsChange"),
		OPTIONS_SHOW("Manage_OptionsShow"),
		ZOOM("Manage_Zoom"),
		COLUMNS("Manage_Columns"),
		HIDE_UNASSIGNED("Manage_HideunAssigned"),
		REFRESH("Manage_Refresh"),
		SHOW_ENTITY("Manage_ShowEntity"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a:ACTION.values()) {
			if (a.toString().equals(str)) return(a);
		}
		return(ACTION.NONE);
	}

	private int cols;
	public static final int DEFAULT_COLUMNS = 5;
	private PrefViewManage param;
	private JScrollPane scrollerUnassigned;

	public Manage(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Manage.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		Object newValue = evt.getNewValue();
		switch(Ctrl.whichACTION(propName)) {
			case REFRESH: {
				View newView = (View) evt.getNewValue();
				View view = (View) getParent().getParent();
				if (view == newView || newView.toString().contains("Manage")) {
					init();
					refresh();
				}
				return;
			}
		}
		switch(getAction(propName)) {
			case OPTIONS_SHOW: {
				View view = (View) evt.getNewValue();
				if (!view.getName().equals(VIEWID.VIEW_MANAGE.toString())) {
					return;
				}
				OptionsDlg.show(mainFrame, view.getName());
				return;
			}

			case SHOW_ENTITY: {
				if (newValue instanceof Scene) {
					Scene scene = (Scene) newValue;
					ViewUtil.scrollToScene(this, panel, scene);
					return;
				}
				if (newValue instanceof Chapter) {
					Chapter chapter = (Chapter) newValue;
					ViewUtil.scrollToChapter(this, panel, chapter);
					return;
				}
			}

			case COLUMNS:
			case OPTIONS_CHANGE: {
				init();
				refresh();
				return;
			}

			case HIDE_UNASSIGNED: {
				init();
				refresh();
				return;
			}
		}
		switch (propName.toLowerCase()) {
			case "part_change":
				refresh();
				ViewUtil.scrollToTop(scroller);
				return;
			case "chapter_delete":
			case "chapter_new":
				refresh();
				return;
			case "pov_delete":
			case "pov_update":
				refresh();
				return;
		}
		dispatchToChapterPanels(this, evt);
	}

	@Override
	public void init() {
		//App.trace("ManagePanel.init()");
		param = App.getInstance().getPreferences().manage;
		selPart = mainFrame.getCurrentPart();
		try {
			cols = param.col;
		} catch (Exception e) {
			e.printStackTrace(System.err);
			cols = DEFAULT_COLUMNS;
		}
	}

	@Override
	public void initUi() {
		//App.trace("ManagePanel.initUI()");
		setLayout(new MigLayout("flowy,fill,ins 0"));
		add(initToolbar(WITH_PART), "wrap, span,growx");
		toolbar.add(new JLabel(I18N.getColonMsg("view.manage.col")));
		JSpinner spinner = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
		spinner.addChangeListener((ChangeEvent e) -> {
			cols = (Integer) (spinner.getValue());
			param.col = cols;
			refresh();
		});
		toolbar.add(spinner);
		JCheckBox ckHide = new JCheckBox(I18N.getMsg("view.manage.unassigned"));
		ckHide.setName("HideUnassigned");
		ckHide.setSelected(param.unassigned);
		ckHide.addChangeListener((ChangeEvent e) -> {
			param.unassigned = ckHide.isSelected();
			refresh();
		});
		toolbar.add(ckHide);

		panel = new JPanel();
		panel.setBackground(SwingUtil.getBackgroundColor());
		scroller = new JScrollPane(panel);
		SwingUtil.setUnitIncrement(scroller);
		SwingUtil.setMaxPreferredSize(scroller);
		scrollerUnassigned = new JScrollPane();

		refresh();

		registerKeyboardAction();
		panel.addMouseWheelListener(this);
	}

	@Override
	public void refresh() {
		//App.trace("ManagePanel.refresh()");
		List<Chapter> chapters = book.chapters;
		if (!mainFrame.showAllParts) {
			chapters = Chapter.find(book.chapters, selPart);
		}

		this.remove(scrollerUnassigned);
		this.remove(scroller);

		// "chapter" for unassigned scenes
		if (!param.unassigned) {
			scrollerUnassigned = new JScrollPane(new ManageChapter(mainFrame));
			Image img = IconUtil.getIconImage("large/post-it");
			scrollerUnassigned.setMinimumSize(new Dimension(img.getWidth(null), img.getHeight(null) + 32));
			add(scrollerUnassigned, "growx");
		}

		add(scroller, "grow");

		// chapters
		panel.setLayout(new MigLayout("wrap " + cols, "", "[top]"));
		panel.removeAll();
		for (Chapter chapter : chapters) {
			panel.add(new ManageChapter(mainFrame, chapter)/*, "grow"*/);
		}
		if (chapters.isEmpty()) {
			panel.add(new JLabel(I18N.getMsg("chapters.warning")));
		}
		revalidate();
		repaint();
	}

	private static void dispatchToChapterPanels(Container cont, PropertyChangeEvent evt) {
		List<Component> ret = SwingUtil.findComponentsByClass(cont, ManageChapter.class);
		ret.stream().map((comp) -> (ManageChapter) comp).forEachOrdered((panel) -> {
			panel.modelPropertyChange(evt);
		});
	}

}
