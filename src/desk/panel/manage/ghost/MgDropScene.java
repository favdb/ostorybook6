package desk.panel.manage.ghost;

import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

public class MgDropScene extends AbstractGhostDropManager {

    public MgDropScene(JComponent target) {
        super(target);
    }

	@Override
	public void ghostDropped(GhostDropEvent e) {
	   String action = e.getAction();
	   Point p = getTranslatedPoint(e.getDropLocation());

	   if (isInTarget(p)) {
	       JOptionPane.showMessageDialog(this.component, "component="+component.getName()+", Action: " + action);
	   }
	}
}