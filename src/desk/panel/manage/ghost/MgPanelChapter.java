/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.manage.ghost;

import db.entity.Chapter;
import db.entity.Scene;
import desk.dialog.edit.Editor;
import desk.interfaces.IRefreshable;
import desk.panel.AbstractPanel;
import desk.tools.EntityUtil;
import desk.tools.swing.SwingUtil;
import desk.tools.swing.label.VerticalLabelUI;
import db.I18N;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;

/*
Ajout de l'ouverture de l'éditeur sur double click sur le titre du chapitre
*/

@SuppressWarnings("serial")
public class MgPanelChapter extends AbstractPanel implements MouseListener, IRefreshable {

	private Chapter chapter;
	private int prefWidth;
	public MgPanel mgPanel;
	private GhostDropManager listener;

	public MgPanelChapter(MgPanel mgPanel) {
		this(mgPanel, null);
		this.mgPanel=mgPanel;
		this.chapter = null;
		initAll();
	}

	public MgPanelChapter(MgPanel mgPanel, Chapter chapter) {
		super(mgPanel.mainFrame);
		this.mgPanel=mgPanel;
		this.chapter = chapter;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		Object newValue = evt.getNewValue();
		Object oldValue = evt.getOldValue();
		String propName = evt.getPropertyName();

		if (MgPanel.ActionManage.ZOOM.check(propName)) {
			refresh();
			return;
		}

		if ("pov_Update".equals(propName)) {
			refresh();
			return;
		}

		if ("chapter_Update".equals(propName)) {
			if(chapter == null){
				return;
			}
			Chapter newChapter = (Chapter) newValue;
			if (!newChapter.id.equals(chapter.id)) {
				return;
			}
			chapter = newChapter;
			refresh();
			return;
		}

		if ("scene_Update".equals(propName)) {
			Chapter newSceneChapter = ((Scene) newValue).chapter;
			Chapter oldSceneChapter = ((Scene) oldValue).chapter;
			if (newSceneChapter == null && chapter == null) {
				refresh();
				return;
			}
			if (chapter == null || newSceneChapter == null
					|| oldSceneChapter == null) {
				refresh();
				return;
			}
			if (!newSceneChapter.id.equals(chapter.id)
					&& !oldSceneChapter.id.equals(chapter.id)) {
				return;
			}
			refresh();
		}
	}

	private void setZoomedSize(int zoomValue) {
		prefWidth = 50 + zoomValue * 10;
		prefWidth=50+(zoomValue*20);
	}

	@Override
	public void init() {
		//App.trace("ChapterPanel.init()");
		listener=new GhostDropManager(this, chapter);
	}

	@Override
	public void initUi() {
		//App.trace("ChapterPanel.initUI()");
		setBorder(SwingUtil.getBorderDefault());

		JLabel lbChapter = new JLabel();
		if (chapter == null) {
			setLayout(new MigLayout("flowx", "[]", "[fill]"));
			setPreferredSize(new Dimension(prefWidth, 80));
			lbChapter.setText(I18N.getMsg("scene.unassigned"));
			lbChapter.setUI(new VerticalLabelUI(false));
			add(lbChapter);
		} else {
			setLayout(new MigLayout("flowy", "[]", "[]4[]0[]"));
			lbChapter.setText(chapter.number.toString()+" "+chapter.name);
			setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, chapter));
			add(lbChapter, "growx");
		}
		lbChapter.setToolTipText(lbChapter.getText());
		lbChapter.addMouseListener(this);

		List<Scene> scenes = Scene.orderByNumber(Scene.find(book.scenes, chapter));
		if (chapter == null) {
			for (Scene scene : scenes) {
				this.add(new MgPanelScene(this,scene,MgPanelScene.UNASSIGNED),"growy");
			}
			this.add(new MgPanelScene(this,null,MgPanelScene.MAKE_UNASSIGNED),"growy");
		} else {
			MgPanelScene begin = new MgPanelScene(this, null, MgPanelScene.BEGIN);
			//TODO ghostTransfer
			if (scenes.isEmpty()) {
				SwingUtil.setMaxPreferredSize(begin);
			} else {
				begin.setPreferredSize(new Dimension(Short.MAX_VALUE, 15));
			}
			add(begin);
			int i = 0;
			for (Scene scene : scenes) {
				MgPanelScene panelScene = new MgPanelScene(this, scene, MgPanelScene.ASSIGNED);
				this.add(panelScene, "growx");
				// move next
				MgPanelScene next = new MgPanelScene(this, null, MgPanelScene.NEXT);
				if (scene.number != null) {
					next.setPreviousNumber(scene.number);
				}
				//TODO ghost transfer
				if (i < scenes.size() - 1) {
					next.setPreferredSize(new Dimension(Short.MAX_VALUE, 15));
				} else {
					SwingUtil.setMaxPreferredSize(next);
				}
				add(next);
				++i;
			}
		}
	}

	public boolean isForUnassignedScene() {
		return chapter == null;
	}

	protected MgPanelChapter getThis() {
		return this;
	}

	public Chapter getChapter() {
		return chapter;
	}

	/**
	 * Gets all {@link DTScenePanel} that have a scene assigned.
	 *
	 * @return a list of all {@link DTScenePanel}
	 * @see DTScenePanel
	 */
	public List<MgPanelScene> getMgPanelScenes() {
		List<MgPanelScene> list = new ArrayList<>();
		for (Component comp : getComponents()) {
			if (comp instanceof MgPanelScene && ((MgPanelScene) comp).getScene() != null) {
				list.add((MgPanelScene) comp);
			}
		}
		return list;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (getChapter() == null) {
			return;
		}
		requestFocusInWindow();
		if (e.getClickCount() == 2) {
			Editor.show(mainFrame, getChapter(),null);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		
	}
}
