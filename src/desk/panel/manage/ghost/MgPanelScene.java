/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun, 2019 FaVdb

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage.ghost;

import db.entity.Scene;
import db.entity.Status;
import desk.dialog.edit.Editor;
import desk.interfaces.IRefreshable;
import desk.panel.AbstractScenePanel;
import desk.tools.EntityUtil;
import desk.tools.swing.SwingUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;

import javax.accessibility.Accessible;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.TransferHandler;
import lib.miginfocom.swing.MigLayout;
import tools.ColorUtil;
import tools.IconUtil;

@SuppressWarnings("serial")
public class MgPanelScene extends AbstractScenePanel implements MouseListener, MouseMotionListener,
		Accessible, IRefreshable, FocusListener {

	public final static int ASSIGNED = 0;
	public final static int UNASSIGNED = 1;
	public final static int MAKE_UNASSIGNED = 2;
	public final static int BEGIN = 3;
	public final static int NEXT = 4;
	public String postIt = "large/post-it3";

	private int type = ASSIGNED;
	//private int textLength;
	private final MgPanelChapter mgChapter;
	private Integer previousNumber;
	private Image img;
	private GhostDropManager listener;

	private MouseEvent firstMouseEvent = null;

	public String getTypeName(int t) {
		switch (t) {
			case ASSIGNED:
				return ("ASSIGNED");
			case UNASSIGNED:
				return ("UNASSIGNED");
			case MAKE_UNASSIGNED:
				return ("MAKE_UNASSIGNED");
			case BEGIN:
				return ("BEGIN");
			case NEXT:
				return ("NEXT");
		}
		return ("UNKNOWN TYPE");
	}

	public MgPanelScene(MgPanelChapter mgChapter, Scene scene, int type) {
		super(mgChapter.mainFrame, scene);
		/*App.trace("ManagePanelScene(" + mainFrame.name + ", " +
				((scene != null) ? scene.getFullTitle() : "null") +
				", " + getTypeName(type) + ")");*/
		this.mgChapter = mgChapter;
		this.scene = scene;
		this.type = type;
		img = IconUtil.getIconImage(postIt);
		initAll();
		setFocusable(true);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (type == ASSIGNED) {
			g.drawImage(img, 0, 0, null);
		}
	}

	@Override
	public void init() {
		//App.trace("MgPanelScene.init()");
		addMouseListener(this);
	}

	@Override
	public void initUi() {
		//App.trace("ManagePanelScene.initUI() type=" + getTypeName(type));
		//setOpaque(true);
		
		if (type == MAKE_UNASSIGNED) {
			setBackground(ColorUtil.getNiceDarkGray());
			setMinimumSize(new Dimension(30, 25));
			return;
		}

		if (scene == null) {
			setBackground(ColorUtil.getNiceDarkGray());
			setMinimumSize(new Dimension(30, 15));
			return;
		}
		
		addMouseMotionListener(this);

		setLayout(new MigLayout("ins 32 16 32 24"));
		setBorder(SwingUtil.getBorderDefault());

		if (scene.informative) {
			setBackground(Color.white);
		} else {
			Color clr = ColorUtil.getPastel2(scene.pov.getJColor());
			setBackground(clr);
		}
		setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, scene));

		JLabel lbState = new JLabel(Status.getIcon(scene.status));
		JLabel lbInformational = new JLabel("");
		if (scene.informative) {
			lbInformational.setIcon(IconUtil.getIcon("small/info"));
		}

		JLabel lbTitle = new JLabel(scene.getChapterSceneNo(true));
		JLabel lbTexte = new JLabel("<html>" +scene.getWriting(107) + "</u></i></p></html>");

		// layout
		add(lbState);
		add(lbInformational);
		add(lbTitle,"wrap");
		add(lbTexte,"span");
		this.addFocusListener(this);
		SwingUtil.setForcedSize(this, new Dimension(img.getWidth(null),img.getHeight(null)));

		//ghost drop'n drag
		this.addMouseMotionListener(new GhostMotionAdapter(mgChapter.mgPanel.ghostGlassPane));
		String libScene = "type=" + type;
		if (scene != null) {
			libScene = "scene=" + scene.id;
		}
		GhostAdapterComponent componentAdapter = new GhostAdapterComponent(mgChapter.mgPanel.ghostGlassPane, libScene);
		this.addMouseListener(componentAdapter);
		componentAdapter.addGhostDropListener(new GhostDropManager(this, scene));
	}

	public Dimension getMinimumSceneSize() {
		Image image = IconUtil.getIconImage(postIt);
		return (new Dimension(image.getWidth(null), image.getHeight(null)));
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		if (scene == null) {
			return;
		}
		requestFocusInWindow();
		if (evt.getClickCount() == 2) {
			Editor.show(mainFrame, scene, null);
		}
	}

	protected MgPanelScene getThis() {
		return this;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		firstMouseEvent = null;
	}

	public int getType() {
		return type;
	}

	@Override
	public void focusGained(FocusEvent e) {
		//App.trace("MgPanelScene.focusGained(e=" + e.toString() + ")");
		this.setBorder(BorderFactory.createLineBorder(Color.red));
		if (scene != null) {
			mainFrame.showInfo(scene);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		//App.trace("MgPanelScene.focusLost(e=" + e.toString() + ")");
		this.setBorder(BorderFactory.createEmptyBorder());
	}

	public int getPreviousNumber() {
		return previousNumber;
	}

	void setPreviousNumber(Integer number) {
		this.previousNumber = number;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (getScene() == null) {
			return;
		}

		if (firstMouseEvent != null) {
			e.consume();

			// if they are holding down the control key, COPY rather than MOVE
			// int ctrlMask = InputEvent.CTRL_DOWN_MASK;
			// int action = ((e.getModifiersEx() & ctrlMask) == ctrlMask) ?
			// TransferHandler.COPY
			// : TransferHandler.MOVE;
			int action = TransferHandler.MOVE;

			int dx = Math.abs(e.getX() - firstMouseEvent.getX());
			int dy = Math.abs(e.getY() - firstMouseEvent.getY());
			// arbitrarily define a 5-pixel shift as the
			// official beginning of a drag
			if (dx > 5 || dy > 5) {
				// this is a drag, not a click
				JComponent comp = (JComponent) e.getSource();
				TransferHandler handler = comp.getTransferHandler();
				// tell the transfer handler to initiate the drag
				handler.exportAsDrag(comp, firstMouseEvent, action);
				firstMouseEvent = null;
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent me) {
	}

}
