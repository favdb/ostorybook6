package desk.panel.manage.ghost;


public interface GhostDropListener {

	public void ghostDropped(GhostDropEvent e);
}
