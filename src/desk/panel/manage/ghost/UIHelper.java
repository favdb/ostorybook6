package desk.panel.manage.ghost;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import tools.IconUtil;

public final class UIHelper {

	public static JButton createButton(String text) {
		JButton button = new JButton(text);
		button.setFocusPainted(true);
		button.setBorderPainted(true);
		button.setContentAreaFilled(true);
		return button;
	}

	public static JButton createButton(String text, String icon) {
		return createButton(text, icon, false);
	}

	public static JButton createButton(String text, String icon, boolean flat) {
		ImageIcon iconNormal = readImageIcon(icon);
		ImageIcon iconHighlight = readImageIcon(icon + "_highlight");
		ImageIcon iconPressed = readImageIcon(icon + "_pressed");

		JButton button = new JButton(text, iconNormal);
		button.setFocusPainted(!flat);
		button.setBorderPainted(!flat);
		button.setContentAreaFilled(!flat);
		if (iconHighlight != null) {
			button.setRolloverEnabled(true);
			button.setRolloverIcon(iconHighlight);
		}
		if (iconPressed != null) {
			button.setPressedIcon(iconPressed);
		}
		return button;
	}

	public static JLabel createLabel(String text, String icon) {
		ImageIcon iconNormal = readImageIcon(icon);
		JLabel label = new JLabel(text, iconNormal, JLabel.LEADING);
		return label;
	}

	public static ImageIcon readImageIcon(String filename) {
		return new ImageIcon(IconUtil.getIconImage("test/"+filename));
	}
}
