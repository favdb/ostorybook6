/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage.ghost;

import db.entity.Chapter;
import db.entity.Scene;
import java.awt.Point;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author favdb
 */
public class GhostDropManager extends AbstractGhostDropManager {
    private Chapter chapter;
	private Scene scene;

    public GhostDropManager(JComponent target, Chapter chapter) {
        super(target);
		this.chapter=chapter;
    }

    public GhostDropManager(JComponent target, Scene scene) {
        super(target);
		this.scene=scene;
    }

    public GhostDropManager(JFrame target) {
        super(target.getRootPane());
    }

	@Override
	public void ghostDropped(GhostDropEvent e) {
	   String action = e.getAction();
	   Point p = getTranslatedPoint(e.getDropLocation());

	   if (isInTarget(p)) {
	       JOptionPane.showMessageDialog(this.component, "Action: " + action);
	   }
	}
	
}
