/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage.ghost;

import db.entity.Chapter;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.options.OptionsDlg;
import desk.panel.AbstractFrame;
import desk.panel.manage.PrefViewManage;
import desk.tools.swing.SwingUtil;
import desk.view.SbView;
import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class MgPanel extends AbstractFrame implements PropertyChangeListener {

	public enum ActionManage {

		CHANGEOPTIONS("Manage_ChangeOptions"),
		SHOWOPTIONS("Manage_ShowOptions"),
		ZOOM("Manage_Zoom"),
		COLUMNS("Manage_Columns"),
		HIDEUNASSIGNED("Manage_HideunAssigned"),
		REFRESH("Manage_Refresh"),
		SHOWENTITY("Manage_ShowEntity");
		final private String text;

		private ActionManage(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ActionManage getManageAction(String str) {
		for (ActionManage a:ActionManage.values()) {
			if (a.toString().equals(str)) return(a);
		}
		return(null);
	}

	public PrefViewManage param;
	private JPanel panel;
	private JScrollPane scroller;
	public GhostGlassPane ghostGlassPane;
	public MgDropScene ghostListener;
	public GhostAdapterComponent componentAdapter;
	
	public MgPanel(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
		//DragnGhostDemo();
	}
	
	@Override
	public void init() {
		param=App.getInstance().getPreferences().manage;
		ghostListener = new MgDropScene(this.getRootPane());
	}
	
	@Override
	public void initUi() {
		//App.trace("MPanel.initUI()");
		this.removeAll();
		setLayout(new MigLayout("flowy,fill,ins 0"));
		ghostGlassPane = new GhostGlassPane();
		setGlassPane(ghostGlassPane);
		
		initToolbar();
		add(toolbar);

		//panel for chapters
		panel = new JPanel();
		panel.setLayout(new MigLayout("wrap " + param.col, "", "[top]"));
		panel.setBackground(SwingUtil.getBackgroundColor());
		scroller = new JScrollPane(panel);
		SwingUtil.setUnitIncrement(scroller);
		SwingUtil.setMaxPreferredSize(scroller);
		add(scroller, "grow");
		
		initChapters();
	}

	private void initChapters() {
		panel.removeAll();
		if (!param.unassigned) {
			panel.add(new MgPanelChapter(this,null),"grow");
		}
		book.chapters.forEach((chapter) -> {
			panel.add(new MgPanelChapter(this, chapter), "grow");
		});
	}
	
	public MgDropScene getListener() {
		return(ghostListener);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		//App.trace("MPanel.propertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		Object newValue = evt.getNewValue();
		switch(getManageAction(propName)) {
			case CHANGEOPTIONS:
			case REFRESH:
				init();
				refresh();
				return;
			case SHOWOPTIONS:
				View view = (View) evt.getNewValue();
				if (!view.getName().equals(SbView.VIEWID.VIEW_MANAGE.toString())) {
					return;
				}
				OptionsDlg.show(mainFrame, view.getName());
				return;
			case SHOWENTITY:
				if (newValue instanceof Scene) {
					Scene scene = (Scene) newValue;
	//				ViewUtil.scrollToScene(this, panel, scene);
					return;
				}
				if (newValue instanceof Chapter) {
					Chapter chapter = (Chapter) newValue;
	//				ViewUtil.scrollToChapter(this, panel, chapter);
					return;
				}
			case COLUMNS:
			case HIDEUNASSIGNED:
				initUi();
				refresh();
				return;
		}
		String z[]=propName.split("_");
		switch(z[0]) {
			case "part":
			case "pov":
			case "chapter":
			case "scene":
				initUi();
				refresh();
				return;
		}

		dispatchToChapterPanels(this, evt);
	}

	private static void dispatchToChapterPanels(Container cont, PropertyChangeEvent evt) {
		List<Component> ret = SwingUtil.findComponentsByClass(cont, MgPanelChapter.class);
		ret.stream().map((comp) -> (MgPanelChapter) comp).forEachOrdered((panel) -> {
			panel.modelPropertyChange(evt);
		});
	}
	public void DragnGhostDemo() {
		//super("Drag n' Ghost Demo");
		//setDefaultCloseOperation(EXIT_ON_CLOSE);

		ghostGlassPane = new GhostGlassPane();
		mainFrame.setGlassPane(ghostGlassPane);

		JPanel expanel=new JPanel(new MigLayout());
		expanel.add(new JLabel("exemple de contenu du Panel"),"wrap");
		expanel.add(new JLabel("autre JLabel"));

		GhostDropListener listener = new GhostDropManagerDemo(expanel);
		GhostAdapterPicture pictureAdapter;

		JLabel label;
		Box box = Box.createVerticalBox();
		box.setBorder(new EmptyBorder(0, 0, 0, 20));

		box.add(label = UIHelper.createLabel("New Sale", "new_sale"));
		label.addMouseListener(pictureAdapter = new GhostAdapterPicture(ghostGlassPane, "new_sale", "resources/icons/test/new_sale.png"));
		pictureAdapter.addGhostDropListener(listener);
		label.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));

		box.add(label = UIHelper.createLabel("View Sale", "view_sale"));
		label.addMouseListener(pictureAdapter = new GhostAdapterPicture(ghostGlassPane, "view_sale", "resources/icons/test/view_sale.png"));
		pictureAdapter.addGhostDropListener(listener);
		label.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));

		box.add(label = UIHelper.createLabel("Quit", "quit"));
		label.addMouseListener(pictureAdapter = new GhostAdapterPicture(ghostGlassPane, "quit", "resources/icons/test/quit.png"));
		pictureAdapter.addGhostDropListener(listener);
		label.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));

		Container c = getContentPane();
		c.setLayout(new MigLayout());
		//c.add(BorderLayout.WEST, box);

		JPanel exp=new JPanel(new MigLayout());
		exp.add(new JLabel("dragable panel"),"wrap");
		exp.add(new JLabel("autre JLabel"));
		exp.addMouseListener(componentAdapter = new GhostAdapterComponent(ghostGlassPane, "exp"));
		componentAdapter.addGhostDropListener(listener);
		exp.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));
		add(exp,"wrap");

		JPanel buttons = new JPanel();
		JButton button;

		buttons.add(button = new JButton("Drag n' Drop"));
		button.addMouseListener(componentAdapter = new GhostAdapterComponent(ghostGlassPane, "button1"));
		componentAdapter.addGhostDropListener(listener);
		button.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));

		buttons.add(button = new JButton("Ghost Alike"));
		button.addMouseListener(componentAdapter = new GhostAdapterComponent(ghostGlassPane, "button2"));
		componentAdapter.addGhostDropListener(listener);
		button.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));


		JScrollPane scrollPane = new JScrollPane(expanel);
		scrollPane.addMouseListener(new GhostAdapterComponent(ghostGlassPane, "table"));
		scrollPane.addMouseMotionListener(new GhostMotionAdapter(ghostGlassPane));

		c.add(scrollPane,"wrap");
		c.add(buttons);

		pack();
		setResizable(false);
		setLocationRelativeTo(null);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
}
