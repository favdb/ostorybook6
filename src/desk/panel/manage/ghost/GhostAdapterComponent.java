package desk.panel.manage.ghost;


import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.SwingUtilities;

public class GhostAdapterComponent extends GhostDropAdapter {

	public GhostAdapterComponent(GhostGlassPane glassPane, String action) {
		super(glassPane, action);
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		System.out.println("GhostAdapterComponent.mousePressed(evt="+evt.toString()+")");
		Component c = evt.getComponent();

		BufferedImage image = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.getGraphics();
		c.paint(g);

		glassPane.setVisible(true);

		Point p = (Point) evt.getPoint().clone();
		SwingUtilities.convertPointToScreen(p, c);
		SwingUtilities.convertPointFromScreen(p, glassPane);

		glassPane.setPoint(p);
		glassPane.setImage(image);
		glassPane.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Component c = e.getComponent();

		Point p = (Point) e.getPoint().clone();
		SwingUtilities.convertPointToScreen(p, c);

		Point eventPoint = (Point) p.clone();
		SwingUtilities.convertPointFromScreen(p, glassPane);

		glassPane.setPoint(p);
		glassPane.setVisible(false);
		glassPane.setImage(null);

		fireGhostDropEvent(new GhostDropEvent(action, eventPoint));
	}
}
