/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage;

import static db.entity.AbstractEntity.getClean;
import desk.app.pref.AppPref;
import db.I18N;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PrefViewManage {
	public static int DEFAULT_COL=1;
	public static boolean DEFAULT_UNASSIGNED=false;

	public int col = DEFAULT_COL;
	public boolean unassigned = DEFAULT_UNASSIGNED;
	private final AppPref pref;
	
	public PrefViewManage(AppPref pref) {
		this.pref=pref;
		load();
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.manage", 3));
		b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("view.manage.col", col));
		b.append(HtmlUtil.getAttribute("view.manage.unassigned", unassigned));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.manage"));
		b.append("\n");
		b.append(TextUtil.getAttribute("view.manage.col", col));
		b.append(TextUtil.getAttribute("view.manage.unassigned", unassigned));
		return (b.toString());
	}
	
	public void save() {
		pref.setString(AppPref.Key.VIEW_MANAGE, getClean(col)+","+(unassigned?"1":"0"));
	}
	
	public void load() {
		String v=pref.getString(AppPref.Key.VIEW_MANAGE);
		if (v!=null && !v.isEmpty()) {
			String z[]=v.split(",");
			if (z.length>0) col=Integer.parseInt(z[0]);
			if (z.length>1) unassigned=("1".equals(z[1]));
		}
	}

	public int getMinZoom() {
		return(1);
	}

	public int getMaxZoom() {
		return(12);
	}

	public int getDefaultCols() {
		return(1);
	}

}
