/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.manage;

import db.entity.Scene;
import db.entity.Status;
import desk.app.MainFrame;
import desk.action.EntityAction;
import desk.interfaces.IRefreshable;
import desk.panel.AbstractScenePanel;
import desk.tools.EntityUtil;
import desk.tools.swing.SwingUtil;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import javax.accessibility.Accessible;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import lib.miginfocom.swing.MigLayout;
import tools.ColorUtil;
import tools.IconUtil;

@SuppressWarnings("serial")
public class ManageScene extends AbstractScenePanel implements MouseListener, Accessible, IRefreshable, FocusListener {

	public final static int ASSIGNED = 0,
			UNASSIGNED = 1,
			MAKE_UNASSIGNED = 2,
			BEGIN = 3,
			NEXT = 4;
	private final String postIt = "large/post-it";

	private int type = ASSIGNED;
	private BufferedImage bimage;
	private JPanel scenePanel;

	public ManageScene(MainFrame mainFrame, Scene scene) {
		this(mainFrame, scene, ASSIGNED);
		initAll();
		//App.trace("ScenePanel("+mainFrame.name+", "+scene.getFullTitle()+", "+type);
	}

	public ManageScene(MainFrame mainFrame, Scene scene, int type) {
		super(mainFrame, scene);
		//App.trace("ScenePanel("+mainFrame.name+", "+((scene!=null)?scene.getFullTitle():"null")+", "+type+")");
		this.scene = scene;
		this.type = type;
		if (scene != null && !scene.hasChapter()) {
			this.type = UNASSIGNED;
		}
		initAll();
		setFocusable(true);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
		//App.trace("ScenePanel.init()");
		Image img = IconUtil.getIconImage(postIt);
		bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Color color = book.povs.get(0).getJColor();
		if (scene != null && scene.pov != null) {
			color = scene.pov.getJColor();
		}
		//transformation image du post-it en fonction de la couleur du POV
		if (scene != null) {
			Graphics2D bGr = bimage.createGraphics();
			bGr.drawImage(img, 0, 0, null);
			bGr.dispose();
			color = new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) (.35f * 255));
			bimage = colorized(bimage, color);
		}
		addMouseListener(this);
	}

	// contribution de Joel Drigo via www.developpez.net
	private BufferedImage colorized(BufferedImage src, Color color) {
		int w = src.getWidth();
		int h = src.getHeight();
		BufferedImage dest = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = dest.createGraphics();
		g.drawImage(src, 0, 0, null);
		g.setComposite(AlphaComposite.SrcAtop);
		g.setColor(color);
		g.fillRect(0, 0, w, h);
		g.dispose();
		return dest;
	}

	@Override
	public void initUi() {
		//App.trace("ScenePanel.initUI()");
		setOpaque(true);

		if (type == MAKE_UNASSIGNED) {
			setPreferredSize(new Dimension(bimage.getWidth(), bimage.getHeight()));
			return;
		}

		if (scene == null) {
			setMaximumSize(new Dimension(bimage.getWidth(), 16));
			return;
		}

		setLayout(new MigLayout("ins 32 16 32 16"));

		setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, scene));

		JLabel lbState = new JLabel(Status.getIcon(scene.status));

		JLabel lbInformational = new JLabel("");
		if (scene.informative) {
			lbInformational.setIcon(IconUtil.getIcon("small/info"));
		}

		scenePanel = new JPanel(new MigLayout());
		scenePanel.setOpaque(false);
		scenePanel.setMaximumSize(new Dimension(bimage.getWidth() - 28, bimage.getHeight() - 48));

		JLabel lbNumber = new JLabel(scene.getChapterSceneNo(true));
		JLabel lbTexte = new JLabel("<html>" + scene.name + "<br><br>" + scene.getWriting(107) + "</u></i></p></html>");

		// layout
		scenePanel.add(lbState);
		scenePanel.add(lbInformational);
		scenePanel.add(lbNumber, "wrap");
		scenePanel.add(lbTexte, "span");
		add(scenePanel);
		this.addFocusListener(this);
		SwingUtil.setForcedSize(this, new Dimension(bimage.getWidth(null), bimage.getHeight(null)));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (scene != null) {
			g.drawImage(bimage, 0, 0, null);
		}
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
		if (scene == null) {
			return;
		}
		requestFocusInWindow();
		if (evt.getClickCount() == 2) {
			Action act = EntityAction.edit(mainFrame, scene, false);
			act.actionPerformed(null);
		}
	}

	protected ManageScene getThis() {
		return this;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if (scene==null) {
			setBackground(ColorUtil.getPastel(ColorUtil.getPastel(Color.yellow)));
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if (scene==null) {
			setBackground(null);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	public int getType() {
		return type;
	}

	@Override
	public void focusGained(FocusEvent e) {
		//App.trace("ScenePanel.focusGained");
		this.setBorder(BorderFactory.createLineBorder(Color.red));
		if (scene != null) {
			mainFrame.showInfo(scene);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		//App.trace("ScenePanel.focusLost");
		this.setBorder(null);
	}
}
