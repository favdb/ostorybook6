/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package desk.panel.manage;

import db.entity.Book;
import db.entity.Chapter;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.model.Ctrl;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

@SuppressWarnings("serial")
public class ManageSceneHandler extends TransferHandler {

	private final MainFrame mainFrame;
	private final Book book;
	private final DataFlavor sceneFlavor = DataFlavor.stringFlavor;
	private ManageSceneDT sourceScene;

	public ManageSceneHandler(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		book=mainFrame.book;
	}

	@Override
	public boolean importData(JComponent comp, Transferable t) {
		if (canImport(comp, t.getTransferDataFlavors())) {
			ManageSceneDT destDtScene = (ManageSceneDT) comp;
			// don't drop on myself
			if (sourceScene == destDtScene) {
				return true;
			}
			try {
				String sourceSceneIdStr = (String) t.getTransferData(sceneFlavor);
				long sourceSceneId = Long.parseLong(sourceSceneIdStr);
				switch (destDtScene.getType()) {
				case ManageScene.ASSIGNED:
					return swapScenes(sourceSceneId, destDtScene);
				case ManageScene.BEGIN:
					return moveSceneToBegin(sourceSceneId, destDtScene);
				case ManageScene.NEXT:
					return moveScene(sourceSceneId, destDtScene);
				case ManageScene.MAKE_UNASSIGNED:
					return unassignScene(sourceSceneId);
				default:
					break;
				}
			} catch (UnsupportedFlavorException | IOException | NumberFormatException e) {
			}
		}
		return false;
	}

	private boolean unassignScene(long sourceSceneId) {
		try {
			Scene scene = book.getScene(sourceSceneId);
			// detach scene from session
			scene.chapter=null;
			mainFrame.getBookController().updateEntity(scene);
		} catch (Exception e) {
			System.err.println(Arrays.toString(e.getStackTrace()));
		}
		return true;
	}

	private boolean moveScene(long sourceSceneId, ManageSceneDT destDtScene) {
		try {
			ManageChapter destChapterPanel = (ManageChapter) destDtScene.getParent();
			Chapter destChapter = destChapterPanel.getChapter();
			int sceneNo = destDtScene.getPreviousNumber() + 1;
			Scene scene = book.getScene(sourceSceneId);
			// detach scene from session

			// set destination chapter
			scene.number=(-99);
			scene.chapter=(destChapter);
			mainFrame.getBookController().updateEntity(scene);
			int counter = 1;
			List<Scene> scenes = Scene.find(book.scenes, destChapter);
			//App.trace("SceneTransferHandler.moveScene(): scenes:"+scenes);
			for (Scene s : scenes) {
				if (s.number != null && s.number == -99) {
					// insert scene
					s.number=(sceneNo);
					mainFrame.getBookController().updateEntity(s);
					++counter;
					continue;
				}
				if (s.number != null && s.number < sceneNo) {
					++counter;
					continue;
				}
				s.number=(counter + 1);
				mainFrame.getBookController().updateEntity(s);
				++counter;
			}
			Scene.renumber(book, destChapter,1,1);
		} catch (Exception e) {
			System.err.println(Arrays.toString(e.getStackTrace()));
		}
		return true;
	}

	private boolean moveSceneToBegin(long sourceSceneId, ManageSceneDT destDtScene){
		ManageChapter destChapterPanel = (ManageChapter) destDtScene.getParent();
		Chapter destChapter = destChapterPanel.getChapter();

		Ctrl ctrl = mainFrame.getBookController();
		Scene scene = book.getScene(sourceSceneId);
		if (destChapter != null) {
			scene.chapter=destChapter;
		}
		scene.number=-1;
		ctrl.updateEntity(scene);
		Scene.renumber(book, destChapter,1,1);
		return true;
	}

	private boolean swapScenes(long sourceSceneId, ManageSceneDT destDtScene) {
		Ctrl ctrl = mainFrame.getBookController();
		Scene srcScene = book.getScene(sourceSceneId);
		Scene destScene = destDtScene.getScene();
		Chapter srcChapter = srcScene.chapter;
		int srcNumber = srcScene.number;
		srcScene.chapter=(destScene.chapter);
		srcScene.number=(destScene.number);
		destScene.chapter=(srcChapter);
		destScene.number=(srcNumber);
		ctrl.updateEntity(srcScene);
		ctrl.updateEntity(destScene);
		return true;
	}

	@Override
	protected Transferable createTransferable(JComponent comp) {
		sourceScene = (ManageSceneDT) comp;
		return new SceneTransferable(sourceScene);
	}

	@Override
	public int getSourceActions(JComponent comp) {
		return COPY_OR_MOVE;
	}

	@Override
	protected void exportDone(JComponent comp, Transferable data, int action) {

	}

	@Override
	public boolean canImport(JComponent comp, DataFlavor[] flavors) {
		for (DataFlavor flavor : flavors) {
			if (sceneFlavor.equals(flavor)) {
				return true;
			}
		}
		return false;
	}

	class SceneTransferable implements Transferable {
		private final String sceneId;

		SceneTransferable(ManageSceneDT pic) {
			sceneId = Long.toString(pic.getScene().id);
		}

		@Override
		public Object getTransferData(DataFlavor flavor)
				throws UnsupportedFlavorException {
			if (!isDataFlavorSupported(flavor)) {
				throw new UnsupportedFlavorException(flavor);
			}
			return sceneId;
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return new DataFlavor[] { sceneFlavor };
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor flavor) {
			return sceneFlavor.equals(flavor);
		}
	}
}
