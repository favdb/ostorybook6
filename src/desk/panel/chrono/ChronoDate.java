package desk.panel.chrono;

import db.entity.SbDate;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import tools.IconUtil;

@SuppressWarnings("serial")
public class ChronoDate extends JLabel {

	private SbDate chapter;

	public ChronoDate(SbDate chapter) {
		super();
		this.chapter = chapter;
		init();
	}

	public final String getDateText() {
		if (chapter == null) {
			return "";
		}
		return(chapter.getDateTime());
	}

	public SbDate getDate() {
		return chapter;
	}

	public void setDate(SbDate date) {
		this.chapter = date;
	}

	private void init() {
		setText(getDateText());
		setToolTipText(getDateText());
		setIcon(IconUtil.getIcon("small/chrono"));
		setBackground(new Color(240, 240, 240));
		setOpaque(true);
		setHorizontalAlignment(SwingConstants.CENTER);
	}
}
