/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.chrono;

import db.entity.Scene;
import db.entity.Pov;
import db.entity.SbDate;
import desk.app.MainFrame;
import java.util.List;
import javax.swing.JPanel;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class ChronoAffRow extends ChronoAffAbstract {

	public ChronoAffRow(MainFrame mainFrame, Pov pov, SbDate date) {
		super(mainFrame, pov, date);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		try {
			setLayout(new MigLayout("insets 1", "[fill,grow]", "[top][fill,grow]"));
			setOpaque(false);

			// date
			ChronoPovDate lbDate = new ChronoPovDate(pov, date);
			add(lbDate, "wrap");

			// scenes by pov and date
			List<Scene> sceneList = Scene.findByPovAndDate(book.scenes,pov, date);
			if (sceneList.isEmpty()) {
				ChronoSpace spacePanel = new ChronoSpace(mainFrame, pov, date);
				add(spacePanel, "grow");
			} else {
				JPanel colPanel = new JPanel(new MigLayout("insets 0", "[]", "[top]"));
				colPanel.setOpaque(false);
				for (Scene scene : sceneList) {
					ChronoScene csp = new ChronoScene(mainFrame, scene);
					colPanel.add(csp, "grow");
				}
				add(colPanel, "grow");
			}
		} catch (Exception e) {
		}
	}
}
