/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.chrono;

import static db.entity.AbstractEntity.getClean;
import desk.app.pref.AppPref;
import db.I18N;
import java.awt.Toolkit;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PrefViewChrono {
	public final static boolean DIRECTION_DEFAULT=true;
	public final static boolean DEFAULT_SHOWDATEDIFF=false;
	public final static int		ZOOM_DEFAULT=300, ZOOM_MIN=200,
			ZOOM_MAX=(int) Toolkit.getDefaultToolkit().getScreenSize().getWidth()-80;
	
	private final AppPref pref;

	public boolean direction = DIRECTION_DEFAULT,
			showDateDiff = DEFAULT_SHOWDATEDIFF;
	public int zoom=ZOOM_DEFAULT;

	public PrefViewChrono(AppPref pref) {
		this.pref=pref;
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.chrono", 3));
		b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("view.chrono.direction", direction));
		b.append(HtmlUtil.getAttribute("view.chrono.show_date_diff", showDateDiff));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.chrono"));
		b.append("\n");
		b.append(TextUtil.getAttribute("view.chrono.direction", direction));
		b.append(TextUtil.getAttribute("view.chrono.show_date_diff", showDateDiff));
		b.append(TextUtil.getAttribute("view.chrono.zoom", zoom));
		return (b.toString());
	}
	
	public void save() {
		String v=(direction?"1":"0")+","+(showDateDiff?"1":"0")+","+getClean(zoom);
		pref.setString(AppPref.Key.VIEW_CHRONO, v);
	}
	
	public void load() {
		String v=pref.getString(AppPref.Key.VIEW_CHRONO);
		if (v!=null && !v.isEmpty()) {
			String z[]=v.split(",");
			if (z.length>0) direction=("1".equals(z[0]));
			if (z.length>1) showDateDiff=("1".equals(z[1]));
			if (z.length>2) zoom=Integer.parseInt(z[2]);
		}
	}

}
