/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.chrono;

import java.beans.PropertyChangeEvent;

import db.entity.Scene;
import db.entity.Pov;
import db.entity.SbDate;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;

@SuppressWarnings("serial")
public abstract class ChronoAffAbstract extends AbstractPanel {

	protected Pov pov;
	protected SbDate date;

	public ChronoAffAbstract(MainFrame mainFrame, Pov pov, SbDate date) {
		super(mainFrame);
		this.pov = pov;
		this.date = date;
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("AbstractPovDatePanel.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		if ("scene_New".equals(propName) || "scene_Delete".equals(propName)) {
			refresh();
			if (getParent() != null) {
				getParent().validate();
			} else {
				validate();
			}
		}
	}

	public Pov getPov() {
		return pov;
	}

	public SbDate getDate() {
		return date;
	}
	
	public Scene getScene(Long id) {
		Scene scene = book.getScene(id);
		return(scene);
	}
}
