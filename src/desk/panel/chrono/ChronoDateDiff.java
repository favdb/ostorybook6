/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.chrono;

import db.entity.SbDate;

import javax.swing.JLabel;

import db.I18N;
import tools.IconUtil;

@SuppressWarnings("serial")
public class ChronoDateDiff extends JLabel {

	private SbDate date1;
	private SbDate date2;

	public ChronoDateDiff(SbDate date1, SbDate date2) {
		this(date1, date2, false);
	}

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public ChronoDateDiff(SbDate date1, SbDate date2, boolean isVertical) {
		super("", JLabel.CENTER);
		this.date1 = date1;
		this.date2 = date2;
		String text = I18N.getColonMsg("preferences.datediff") + " " + getDays();
		long diff = date2.toLong() - date1.toLong();
		String text2 = "(" + date1.getDateTime() + " - " + date2.getDateTime() + ")";
		setText(getDays() + " " + text2);
		setToolTipText("<html>" + text + "<br>" + text2);
		setIcon(IconUtil.getIcon("small/datediff"));
	}

	public final int getDays() {
		return(date1.toDays()-date2.toDays());
	}
}
