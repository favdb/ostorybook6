/*
 Storybook: Open Source software for novelists and authors.
 Copyright (C) 2008 - 2012 Martin Mustun

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.chrono;

import db.entity.Book;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import db.entity.Pov;
import db.entity.SbDate;
import desk.app.App;
import desk.panel.AbstractScrollPanel;
import desk.app.MainFrame;
import desk.options.OptionsDlg;
import desk.tools.swing.SwingUtil;
import desk.tools.swing.label.VerticalLabelUI;
import desk.view.SbView;
import desk.view.ViewUtil;
import db.I18N;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;

import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;
import tools.ColorUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class Chrono extends AbstractScrollPanel implements Printable, MouseWheelListener {

	public enum ACTION {

		DIRECTION("view.chrono.direction"),
		SHOW_DATEDIFF("view.chrono.showdatediff"),
		CHANGEOPTIONS("Chrono_ChangeOptions"),
		LAYOUTDIRECTION("Chrono_LayoutDirection"),
		PRINT("Chrono_Print"),
		REFRESH("Chrono_Refresh"),
		SHOW_DATEDIFFERENCE("Chrono_ShowDateDifference"),
		SHOW_ENTITY("Chrono_ShowEntity"),
		SHOW_OPTIONS("Chrono_ShowOptions"),
		ZOOM("Chrono_Zoom"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}
	
	public static ACTION getAction(String str) {
		for (ACTION a:ACTION.values()) {
			if (a.toString().equals(str)) return(a);
		}
		return(ACTION.NONE);
	}

	public final static int ZOOM_MIN = 20;
	public final static int ZOOM_MAX = 100;

	private static final String CLIENT_PROPERTY_POV_ID = "pov_id";
	private final List<JLabel> povLabels;
	private PrefViewChrono param;

	public Chrono(MainFrame mainFrame) {
		super(mainFrame);
		povLabels = new ArrayList<>();
		param = App.getInstance().getPreferences().chrono;
		initAll();
	}

	@Override
	public void init() {
		//App.trace("Chrono.init()");
		param = App.getInstance().getPreferences().chrono;
		setZoomValues(PrefViewChrono.ZOOM_DEFAULT, ZOOM_MIN, ZOOM_MAX);
	}

	@Override
	public void initUi() {
		//App.trace("Chrono.initUi()");
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("flowy, ins 0"));

		add(initToolbar(WITH_PART), "span,wrap");
		toolbar.add(initSlider("view.chrono.zoom", PrefViewChrono.ZOOM_MIN, PrefViewChrono.ZOOM_MAX, param.zoom));
		toolbar.add(initCheckBox("view.chrono.direction", param.direction));
		toolbar.add(initCheckBox("view.chrono.showdatediff", param.showDateDiff));

		panel = new JPanel(new MigLayout("", "", "[top]"));
		panel.setBackground(SwingUtil.getBackgroundColor());
		scroller = new JScrollPane(panel);
		SwingUtil.setDefaultScroller(scroller);
		add(scroller, "grow");

		refresh();
		registerKeyboardAction();
		panel.addMouseWheelListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		App.trace("Storyboard.actionPerformed(evt=" + evt.toString() + ")");
		if (evt.getSource() instanceof JCheckBox) {
			JCheckBox cb = (JCheckBox)evt.getSource();
			if (cb.getName().equals(ACTION.DIRECTION.toString())) {
				setDirection(cb.isSelected());
			}
			if (cb.getName().equals(ACTION.SHOW_DATEDIFF.toString())) {
				setShowDateDiff(cb.isSelected());
			}
		}
	}
	
	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Chrono.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		SbView sbView, newSbView;
		View view, newView;
		String z[] = propName.split("_");
		if (z.length > 1 && propName.startsWith("Chrono_")) {
			switch (getAction(propName)) {
				case REFRESH:
					newSbView = (SbView) newValue;
					sbView = (SbView) getParent().getParent();
					if (sbView == newSbView) {
						refresh();
					}
					return;
				case PRINT:
					newView = (View) newValue;
					view = (View) getParent().getParent();
					if (view == newView) {
						// PrintUtil.printComponent(this);
						PrinterJob pj = PrinterJob.getPrinterJob();
						pj.setPrintable((Printable) this);
						pj.printDialog();
						try {
							pj.print();
						} catch (PrinterException e) {
							e.printStackTrace(System.err);
						}
					}
					return;
				case SHOW_OPTIONS:
					view = (View) evt.getNewValue();
					if (view.getName().equals(SbView.VIEWID.VIEW_CHRONO.toString())) {
						OptionsDlg.show(mainFrame, view.getName());
						initUi();
					}
					return;
				case LAYOUTDIRECTION:
					setDirection((Boolean) evt.getNewValue());
					return;
				case SHOW_DATEDIFFERENCE:
					setShowDateDiff((Boolean) evt.getNewValue());
					return;
				case SHOW_ENTITY:
					if (newValue instanceof Scene) {
						Scene scene = (Scene) newValue;
						ViewUtil.scrollToScene(this, panel, scene);
						return;
					} else if (newValue instanceof Chapter) {
						Chapter chapter = (Chapter) newValue;
						ViewUtil.scrollToChapter(this, panel, chapter);
						return;
					}
					break;
			}
		}
		switch(Book.getTYPE(z[0])) {
			case CHAPTER:
			case ITEM:
			case LOCATION:
			case PART:
			case PERSON:
			case PLOT:
			case POV:
			case SCENE:
				ViewUtil.scrollToTop(scroller);
				refresh();
		}
	}

	@Override
	public void refresh() {
		//App.trace("Chrono.refresh()");
		// must be done before the session is opened
		if (book == null) {
			return;
		}
		Part part = mainFrame.getCurrentPart();

		List<SbDate> dates;
		if (mainFrame.showAllParts) {
			dates = Scene.findDistinctDates(book.scenes);
		} else {
			dates = Scene.findDistinctDates(book, book.scenes, part);
		}
		List<Pov> povs = Pov.sort(book.povs);

		panel.removeAll();
		if (dates.isEmpty()) {
			panel.add(new JLabel(I18N.getMsg("scenes.warning")));
			panel.revalidate();
			panel.repaint();
			return;
		}
		if (param.direction) {
			// vertical layout
			SbDate lastDate = null;
			for (SbDate date : dates) {
				int i = 0;
				if (param.showDateDiff && lastDate != null) {
					for (int j = 0; j < povs.size(); ++j) {
						ChronoDateDiff lbDiff = new ChronoDateDiff(lastDate, date);
						if (lbDiff.getDays() > 1) {
							String wrap = "";
							if (j == povs.size() - 1) {
								wrap = "wrap,";
							}
							panel.add(lbDiff, wrap + "growx,al center");
						}
					}
				}
				for (Pov pov : povs) {
					JLabel lbPov = new JLabel(" " + pov.toString() + " ");
					lbPov.setMinimumSize(new Dimension(20, lbPov.getHeight()));
					lbPov.setBackground(ColorUtil.getPastel(pov.getJColor()));
					lbPov.setOpaque(true);
					lbPov.putClientProperty(CLIENT_PROPERTY_POV_ID, pov.id);
					povLabels.add(lbPov);
					String wrap = "";
					if (i == povs.size() - 1) {
						wrap = "wrap,";
					}
					panel.add(lbPov, wrap + "grow");
					++i;
				}
				i = 0;
				for (Pov pov : povs) {
					String wrap = "";
					if (i == povs.size() - 1) {
						wrap = "wrap";
					}
					ChronoAffCol colPanel = new ChronoAffCol(mainFrame, pov, date);
					panel.add(colPanel, wrap + ",grow");
					++i;
				}
				lastDate = date;
			}
		} else {
			povs.forEach((pov) -> {
				int i = 0;
				SbDate lastDate = null;
				for (SbDate date : dates) {
					if (param.showDateDiff && lastDate != null) {
						ChronoDateDiff lbDiff = new ChronoDateDiff(lastDate, date, true);
						lbDiff.setUI(new VerticalLabelUI(false));
						if (lbDiff.getDays() > 1) {
							panel.add(lbDiff, "growy");
						}
					}

					JLabel lbPov = new JLabel(" " + pov.toString() + " ");
					lbPov.setMinimumSize(new Dimension(20, lbPov.getHeight()));
					lbPov.setUI(new VerticalLabelUI(false));
					lbPov.setBackground(ColorUtil.getPastel(pov.getJColor()));
					lbPov.setOpaque(true);
					lbPov.putClientProperty(CLIENT_PROPERTY_POV_ID, pov.id);
					povLabels.add(lbPov);
					panel.add(lbPov, "grow");
					String wrap = "";
					if (i == dates.size() - 1) {
						wrap = ",wrap";
					}
					ChronoAffRow rowPanel = new ChronoAffRow(mainFrame, pov, date);
					panel.add(rowPanel, "grow" + wrap);
					++i;

					lastDate = date;
				}
			});
		}
		panel.revalidate();
		revalidate();
		repaint();
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		//App.trace("Chrono.stateChanged("+e.toString()+")");
		if (e.getSource() instanceof JSlider) {
			JSlider slider = (JSlider) e.getSource();
			if (slider.getName().equals("view.chrono.zoom")) {
				if (!slider.getValueIsAdjusting()) {
					int val = slider.getValue();
					setZoom(val);
				}
			}
		}
	}

	private void setZoom(int val) {
		param.zoom = val;
		param.save();
		refresh();
	}

	private void setDirection(boolean b) {
		param.direction = b;
		param.save();
		refresh();
	}

	private void setShowDateDiff(boolean b) {
		param.showDateDiff = b;
		param.save();
		refresh();
	}

	@Override
	public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.black);

		int fontHeight = g2.getFontMetrics().getHeight();
		int fontDescent = g2.getFontMetrics().getDescent();

		double pageHeight = pageFormat.getImageableHeight();
		double pageWidth = pageFormat.getImageableWidth();

		g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
		// bottom center
		g2.drawString("Page: " + (pageIndex + 1), (int) pageWidth / 2 - 35, (int) (pageHeight + fontHeight - fontDescent));
		this.paint(g2);

		if (pageIndex < 4) {
			return Printable.PAGE_EXISTS;
		}
		return Printable.NO_SUCH_PAGE;
	}

	public JPanel getPanel() {
		return panel;
	}
}
