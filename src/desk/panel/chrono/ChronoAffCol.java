/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.chrono;

import java.util.List;

import javax.swing.JPanel;

import lib.miginfocom.swing.MigLayout;
import db.entity.Scene;
import db.entity.Pov;
import db.entity.SbDate;
import desk.app.MainFrame;
import desk.tools.swing.label.VerticalLabelUI;

@SuppressWarnings("serial")
public class ChronoAffCol extends ChronoAffAbstract {

	public ChronoAffCol(MainFrame mainFrame, Pov pov, SbDate date) {
		super(mainFrame, pov, date);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		try {
			setLayout(new MigLayout("insets 1,fill", "[][grow]", "[top]"));
			setOpaque(false);

			// date
			ChronoPovDate lbDate = new ChronoPovDate(pov, date);
			lbDate.setUI(new VerticalLabelUI(false));
			add(lbDate, "cell 0 0,growy");

			// scenes by pov and date
			List<Scene> sceneList = Scene.findByPovAndDate(book.scenes,pov, date);
			if (sceneList.isEmpty()) {
				ChronoSpace spacePanel = new ChronoSpace(mainFrame, pov, date);
				add(spacePanel, "grow");
			} else {
				MigLayout layout2 = new MigLayout("wrap,insets 0", "[]", "[top]");
				JPanel rowPanel = new JPanel(layout2);
				rowPanel.setOpaque(false);
				sceneList.stream().map((scene) -> new ChronoScene(mainFrame, scene)).forEachOrdered((csp) -> {
					rowPanel.add(csp, "grow");
				});
				add(rowPanel, "grow");
			}
		} catch (Exception e) {
		}
	}
}
