/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import desk.tools.EntityUtil;
import edu.uci.ics.jung.algorithms.layout.GraphElementAccessor;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractPopupGraphMousePlugin;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPopupMenu;

public class MemoriaGraphMouse extends AbstractPopupGraphMousePlugin implements MouseListener {

	public static final String ACTION_KEY_DB_OBECT = "DbObject";
	private Memoria parent;

	public MemoriaGraphMouse(Memoria parent) {
		this.parent = parent;
	}

	public MemoriaGraphMouse() {
		this(4);
	}

	public MemoriaGraphMouse(int i) {
		super(i);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void handlePopup(MouseEvent evt) {
		VisualizationViewer vv = (VisualizationViewer) evt.getSource();
		Point point = evt.getPoint();
		GraphElementAccessor accel = vv.getPickSupport();
		if (accel != null) {
			AbstractEntity entity = (AbstractEntity) accel.getVertex(vv.getGraphLayout(), point.getX(), point.getY());
			if (entity != null) {
				JPopupMenu pop = EntityUtil.createPopupMenu(this.parent.getMainFrame(), entity);
				pop.show(vv, evt.getX(), evt.getY());
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void mouseClicked(MouseEvent event) {
		if (event.getClickCount() == 2) {
			VisualizationViewer locVV = (VisualizationViewer) event.getSource();
			Point point = event.getPoint();
			GraphElementAccessor gea = locVV.getPickSupport();
			if (gea != null) {
				AbstractEntity entity =
						(AbstractEntity) gea.getVertex(locVV.getGraphLayout(), point.getX(), point.getY());
				if (entity != null) {
					this.parent.refresh(entity);
				}
			}
		}
		super.mouseClicked(event);
	}

	public Memoria getParent() {
		return this.parent;
	}
}