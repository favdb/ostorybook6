/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Relation;
import db.entity.SbPeriod;
import db.I18N;
import java.util.List;

/**
 *
 * @author favdb
 */
public class GraphRelation {
	
	static void init(Memoria m,AbstractEntity entity) {
		//App.trace("GraphRelation.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.relationVertex = new Relation();
		m.relationVertex.setDescription(I18N.getMsg("relations"));
		m.graph.addVertex(m.relationVertex);
		m.labelMap.put(m.relationVertex, m.relationVertex.toString());
		m.iconMap.put(m.relationVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.relationVertex);
	}

	static void add(Memoria m, Relation relation) {
		//App.trace("GraphRelation.addToVertexRelation(m, entity="+(relation==null?"null":relation.name)+")");
		boolean b=false;
		if (relation==null) return;
		if (relation.hasPeriod() && (m.chosenDate != null)) {
			SbPeriod period = relation.getPeriod();
			if ((period != null) && (!period.isInside(m.chosenDate))) {
				b=true;
			}
		} else {
			b=true;
		}
		if (b) {
			m.graph.addVertex(relation);
			m.labelMap.put(relation, relation.getDescription());
			m.iconMap.put(relation, relation.getIcon("medium"));
			m.graph.addEdge(m.graphIndex++, m.relationVertex, relation);
		}
	}

	static void add(Memoria m, List<Relation> relations, AbstractEntity entity) {
		/*App.trace("GraphRelation.add(m, relations="
				+(relations==null||relations.isEmpty()?"empty":relations.toString())
				+", entity="+(entity==null?"null":entity.name)+")");*/
		if (!relations.isEmpty()) {
			relations.forEach((relation)-> {
				//TODO
			});
		}
	}

}
