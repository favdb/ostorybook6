/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Relation;
import db.entity.Scene;
import db.I18N;
import java.util.List;
import java.util.Set;

/**
 *
 * @author favdb
 */
public class GraphLocation {
	
	static void init(Memoria m,AbstractEntity entity) {
		//App.trace("GraphLocation.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.locationVertex = new Location();
		m.locationVertex.setName(I18N.getMsg("locations"));
		m.graph.addVertex(m.locationVertex);
		m.labelMap.put(m.locationVertex, m.locationVertex.toString());
		m.iconMap.put(m.locationVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.locationVertex);
	}
	
	static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphLocation.create(m, entity="+(entity==null?"null":entity.name)+")");
		Location location = (Location)entity;
		if (location == null) {
			return;
		}
		List<Scene> scenes = m.book.scenes;
		if ((scenes == null) || (scenes.isEmpty())) {
			return;
		}
		m.graphIndex = 0L;
		m.graph.addVertex(location);
		m.labelMap.put(location, location.toString());
		m.iconMap.put(location, location.getIcon("large"));
		m.locationVertexTitle = I18N.getMsg("graph.involved.locations");
		m.initVertices(location);
		scenes.forEach((scene)-> {
			boolean c = false;
			if (m.chosenDate == null) {
				c = true;
			} else if ((scene.hasDate()) && (m.chosenDate.compareTo(scene.date) == 0)) {
				c = true;
			}
			if (c && scene.locations.contains(location)) {
				List<Location> locations = scene.locations;
				locations.forEach((loc)-> {
					if (loc.equals(location)) {
						GraphScene.add(m,scene);
						m.sceneIds.add(scene.id);
						List<Person> persons = scene.persons;
						if (!persons.isEmpty()) {
							persons.forEach((person) -> {
								GraphPerson.add(m,person);
							});
						}
						List<Plot> plots = scene.plots;
						if (!plots.isEmpty()) {
							plots.forEach((plot) -> {
								GraphPlot.addInvolved(m,plot);
							});
						}
						List<Item> items = scene.items;
						if (!items.isEmpty()) {
							items.forEach((item) -> {
								GraphItem.addInvolved(m,item);
							});
						}
					}
				});
			}
		});
	}

	static void add(Memoria m, Location location) {
		//App.trace("GraphLocation.add(m, location="+(location==null?"null":location.name)+")");
		if (location==null) return;
		m.graph.addVertex(location);
		m.labelMap.put(location, location.toString());
		m.iconMap.put(location, location.getIcon("medium"));
		m.graph.addEdge(m.graphIndex++, m.locationVertex, location);
		List<Relation> relations = Relation.findByLocation(m.book.relations,location);
		GraphRelation.add(m, relations, location);
	}

	static void addSet(Memoria m, Set<Location> paramSet) {
		//App.trace("GraphLocation.init(m, paramSet)");
		if (!paramSet.isEmpty()) {
			for (Location location : paramSet) {
				add(m,location);
			}
		}
	}

}
