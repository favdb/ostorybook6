/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.SbDate;
import db.entity.Scene;
import db.entity.Tag;
import db.entity.SbPeriod;
import desk.app.App;
import desk.app.Language;
import desk.app.MainFrame;
import desk.tools.combobox.ComboUtil;
import desk.model.Ctrl;
import desk.interfaces.IRefreshable;
import desk.listcell.LCREntity;
import desk.listcell.LCREntityType;
import desk.options.OptionsDlg;
import desk.panel.AbstractPanel;
import desk.tools.FileFilter;
import desk.tools.IconButton;
import desk.tools.IOTools;
import desk.tools.swing.ScreenImage;
import desk.tools.swing.SwingUtil;
import desk.view.SbView.VIEWID;
import edu.uci.ics.jung.algorithms.layout.BalloonLayout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.DefaultVertexIconTransformer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.EllipseVertexShapeTransformer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.decorators.VertexIconShapeTransformer;
import edu.uci.ics.jung.visualization.layout.LayoutTransition;
import edu.uci.ics.jung.visualization.util.Animator;
import db.I18N;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.icon.EmptyIcon;

public class Memoria extends AbstractPanel implements ActionListener, IRefreshable {

	public enum ACTION {
		BT_FIRST("btFirst"),
		BT_LAST("btLast"),
		BT_NEXT("btNext"),
		BT_PREVIOUS("btPrevious"),
		BALLOON("Memoria_Balloon"),
		EXPORT("Memoria_Export"),
		OPTIONS_CHANGE("Memoria_OptionsChange"),
		OPTIONS_SHOW("Memoria_OptionsShow"),
		REFRESH("Memoria_Refresh"),
		SHOW_MEMORIA("Memoria_ShowMemoria"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	DelegateForest<AbstractEntity, Long> graph;
	private VisualizationViewer<AbstractEntity, Long> vv;
	private TreeLayout<AbstractEntity, Long> treeLayout;
	private BalloonLayout<AbstractEntity, Long> balloonLayout;
	private GraphZoomScrollPane graphPanel;

	Map<AbstractEntity, String> labelMap;
	Map<AbstractEntity, Icon> iconMap;
	public long graphIndex;
	private ScalingControl scaler;

	//entity variables
	//private String src;
	public AbstractEntity shownEntity;

	//scenes
	Scene sceneVertex;
	String sceneVertexTitle;
	List<Long> sceneIds;
	//actions
	Plot plotVertex;
	String plotVertexTitle;
	//persons
	Person personVertex;
	//relationships
	Relation relationVertex;
	private String relationshipVertexTitle;
	//locations
	Location locationVertex;
	String locationVertexTitle;
	//tag
	boolean showTagVertex = true;
	Tag tagVertex;
	Tag involvedTagVertex;
	Set<Tag> involvedTags;
	//items
	Item itemVertex;
	Item involvedItemVertex;
	Set<Item> involvedItems;
	//povs
	Pov povVertex;
	private String povVertexTitle;
	//panels and combos
	//private JPanel controlPanel;
	private JPanel datePanel;
	public SbDate chosenDate;
	private JComboBox dateCombo;
	private JCheckBox cbAutoRefresh;
	//icons
	public final Icon emptyIcon = new EmptyIcon();

	//for toolbar
	private JCheckBox ckBalloon;
	private JComboBox cbType;
	private JComboBox cbEntity;

	public Memoria(MainFrame paramMainFrame) {
		super(paramMainFrame);
		initAll();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		//App.trace("Memoria.init()");
		try {
			chosenDate = new SbDate(0L);
			sceneIds = new ArrayList<>();
			involvedTags = new HashSet<>();
			involvedItems = new HashSet<>();
			scaler = new CrossoverScalingControl();
		} catch (Exception exc2) {
			App.error("Memoria.init()", exc2);
		}
	}

	@Override
	public void initUi() {
		//App.trace("Memoria.initUi()");
		try {
			setLayout(new MigLayout("wrap,fill", "[]", "[][grow]"));
			initGraph();
			initToolbar(!WITH_PART);

			//cb types of entity
			toolbar.add(initCbType());

			//cb for entities must hidden at init
			toolbar.add(initCbEntity());

			//checkbox for showing balloon
			ckBalloon = new JCheckBox();
			ckBalloon.setName("ckBalloon");
			ckBalloon.setText(I18N.getMsg("view.memoria.balloon"));
			ckBalloon.setSelected(App.getInstance().getPreferences().memoria.balloon);
			ckBalloon.addActionListener((ActionEvent evt) -> {
				ckBalloonChange(evt);
			});
			toolbar.add(ckBalloon);

			add(toolbar, "wrap");
			add(graphPanel, "grow");
		} catch (Exception exc) {
			App.error("Memoria.initUi()", exc);
		}
	}

	@SuppressWarnings("unchecked")
	private JComboBox initCbType() {
		//App.trace("Memoria.initCbType()");
		cbType = new JComboBox();
		cbType.setName("cbType");
		cbType.setRenderer(new LCREntityType());
		cbType.addItem(Book.TYPE.NONE);
		cbType.addItem(Book.TYPE.SCENE);
		cbType.addItem(Book.TYPE.PERSON);
		cbType.addItem(Book.TYPE.LOCATION);
		cbType.addItem(Book.TYPE.ITEM);
		cbType.addItem(Book.TYPE.TAG);
		cbType.addItem(Book.TYPE.PLOT);
		cbType.addItem(Book.TYPE.POV);
		cbType.addActionListener(this);
		return (cbType);
	}

	public Book.TYPE getCbTypeSelected() {
		if (cbType == null) {
			return (null);
		}
		if (cbType.getSelectedItem() instanceof Book.TYPE) {
			return ((Book.TYPE) cbType.getSelectedItem());
		}
		return (null);
	}

	@SuppressWarnings("unchecked")
	private JComboBox initCbEntity() {
		//App.trace("Memoria.initCbEntity()");
		cbEntity = new JComboBox();
		cbEntity.setName("cbEntity");
		cbEntity.setVisible(false);
		cbEntity.setRenderer(new LCREntity(book, Book.TYPE.NONE));
		cbEntity.addItemListener((ItemEvent e) -> {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				cbEntityChange(e);
			}
		});
		return (cbEntity);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Memoria.modelPropertyChange(" + evt.toString() + ")");
		try {
			String propName = evt.getPropertyName();
			if (propName == null) {
				return;
			}
			switch (Ctrl.whichACTION(propName)) {
				case REFRESH:
					View nV = (View) evt.getNewValue();
					View oV = (View) getParent().getParent();
					if (oV == nV) {
						refresh();
					}
					return;
				case EXPORT:
					View newView = (View) evt.getNewValue();
					View oldView = (View) getParent().getParent();
					if (newView == oldView) {
						export();
					}
					return;
			}
			switch (getAction(propName)) {
				case SHOW_MEMORIA:
					AbstractEntity entity = (AbstractEntity) evt.getNewValue();
					refresh(entity);
					return;
				case OPTIONS_SHOW:
					View nv = (View) evt.getNewValue();
					if (nv.getName().equals(VIEWID.VIEW_MEMORIA.toString())) {
						OptionsDlg.show(mainFrame, nv.getName());
					}
					return;
				case OPTIONS_CHANGE:
					makeLayoutTransition();
					return;
			}
			switch (Ctrl.whichACTION(propName)) {
				case UPDATE:
				case DELETE:
				case NEW:
					refresh();
					break;
			}
		} catch (Exception exc) {
			App.error("Memoria.modelPropertyChange(" + evt.toString() + ")", exc);
		}
	}

	public void cbEntityChange(ItemEvent evt) {
		//App.trace("Memoria.cbEntityChange(evt=" + evt.toString() + ")");
		if (cbEntity.getSelectedItem() instanceof Long) {
			refreshGraph((Long) cbEntity.getSelectedItem());
		}
	}

	public void ckBalloonChange(ActionEvent evt) {
		//App.trace("Memoria.ckBalloonChange(evt=" + evt.toString() + ")");
		App.getInstance().getPreferences().memoria.balloon = ckBalloon.isSelected();
		App.getInstance().getPreferences().memoria.save();
		makeLayoutTransition();
	}

	private void refreshCbEntity(Book.TYPE type) {
		//App.trace("Memoria.refreshCbEntity(type=" + type.toString() + ")");
		List<? extends AbstractEntity> list;
		switch (type) {
			case SCENE:
				list = book.scenes;
				break;
			case PERSON:
				list = book.persons;
				break;
			case LOCATION:
				list = book.locations;
				break;
			case ITEM:
				list = book.items;
				break;
			case TAG:
				list = book.tags;
				break;
			case PLOT:
				list = book.plots;
				break;
			case POV:
				list = book.povs;
				break;
			case RELATION:
				list = book.relations;
				break;
			default:
				cbEntity.setVisible(false);
				return;
		}
		ComboUtil.fillEntity(book, cbEntity, type);
		cbEntity.setVisible(true);
		if (cbEntity.getItemCount() > 0) {
			cbEntity.setSelectedIndex(0);
		}
	}

	void addIconButton(String icon, String btString) {
		//App.trace("Memoria.addIconButton(" + icon + "," + btString + ")");
		IconButton ib = new IconButton("small/" + icon);
		ib.setSize32x20();
		ib.setName(btString);
		ib.addActionListener(this);
		datePanel.add((Component) ib);
	}

	@SuppressWarnings("unchecked")
	private void makeLayoutTransition() {
		//App.trace("Memoria.makeLayoutTransition()");
		if (vv == null) {
			return;
		}
		LayoutTransition layout;
		if (ckBalloon.isSelected()) {
			layout = new LayoutTransition(vv, treeLayout, balloonLayout);
		} else {
			layout = new LayoutTransition(vv, balloonLayout, treeLayout);
		}
		Animator animator = new Animator(layout);
		animator.start();
		vv.repaint();
	}

	private void clearGraph() {
		//App.trace("Memoria.clearGraph()");
		try {
			if (graph == null) {
				graph = new DelegateForest<>();
				return;
			}
			Collection collections = graph.getRoots();
			Iterator iCollection = collections.iterator();
			while (iCollection.hasNext()) {
				AbstractEntity entity = (AbstractEntity) iCollection.next();
				if (entity != null) {
					graph.removeVertex(entity);
				}
			}
		} catch (Exception exc) {
			graph = new DelegateForest<>();
		}
	}

	public void zoomIn() {
		//App.trace("Memoria.zoomIn()");
		scaler.scale(vv, 1.1F, vv.getCenter());
	}

	public void zoomOut() {
		//App.trace("Memoria.zoomOut()");
		scaler.scale(vv, 0.9090909F, vv.getCenter());
	}

	public void export() {
		//App.trace("Memoria.export()");
		try {
			if (shownEntity == null) {
				return;
			}
			File file1 = new File(mainFrame.bookPref.Export.getLastFile());
			JFileChooser chooser = new JFileChooser(file1);
			FileFilter filter = new FileFilter(FileFilter.PNG, I18N.getMsg("file.type.png"));
			chooser.setFileFilter(filter);
			chooser.setApproveButtonText(I18N.getMsg("export"));
			String str = IOTools.getEntityFileNameForExport(mainFrame, "Memoria", shownEntity);
			chooser.setSelectedFile(new File(str));
			int i = chooser.showDialog(this, I18N.getMsg("export"));
			if (i == 1) {
				return;
			}
			File file2 = chooser.getSelectedFile();
			if (!file2.getName().endsWith(".png")) {
				file2 = new File(file2.getPath() + ".png");
			}
			ScreenImage.createImage(graphPanel, file2.toString());
			JOptionPane.showMessageDialog(this, I18N.getMsg("export.success") + "\n" + file2.getAbsolutePath(), I18N.getMsg("export"), 1);
		} catch (IOException exc) {
			App.error("Memoria.export()", exc);
		}
	}

	@SuppressWarnings("unchecked")
	private void initGraph() {
		//App.trace("Memoria.initGraph()");
		try {
			labelMap = new HashMap<>();
			iconMap = new HashMap<>();
			graph = new DelegateForest<>();
			treeLayout = new TreeLayout<>(graph);
			balloonLayout = new BalloonLayout<>(graph);

			vv = new VisualizationViewer<>(balloonLayout);
			vv.setSize(new Dimension(800, 800));
			refreshGraph();
			vv.setBackground(Color.white);
			vv.getRenderContext().setEdgeShapeTransformer(new EdgeShape.Line<>());
			vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<>());
			vv.setVertexToolTipTransformer(new EntityTransformer());

			graphPanel = new GraphZoomScrollPane(vv);
			graphPanel.setBackground(SwingUtil.getBackgroundColor());
			graphPanel.setBorder(BorderFactory.createLineBorder(Color.black));

			DefaultModalGraphMouse mouse = new DefaultModalGraphMouse();
			vv.setGraphMouse(mouse);
			mouse.add(new MemoriaGraphMouse(this));
			VertexStringerImpl localVSI = new VertexStringerImpl(labelMap);
			vv.getRenderContext().setVertexLabelTransformer(localVSI);
			VertexIconShapeTransformer transformer = new VertexIconShapeTransformer(new EllipseVertexShapeTransformer());
			DefaultVertexIconTransformer iconTransformer = new DefaultVertexIconTransformer();
			transformer.setIconMap(iconMap);
			iconTransformer.setIconMap(iconMap);
			vv.getRenderContext().setVertexShapeTransformer(transformer);
			vv.getRenderContext().setVertexIconTransformer(iconTransformer);
		} catch (Exception exc) {
			System.err.println(exc.getLocalizedMessage());
			System.err.println(Arrays.toString(exc.getStackTrace()));
		}
	}

	private void refreshGraph() {
		//App.trace("Memoria.refreshGraph()");
		refreshGraph(null);
	}

	@SuppressWarnings({"unchecked", "unchecked"})
	private void refreshGraph(Long id) {
		//App.trace("Memoria.refreshGraph(entity=" + (id == null ? "null" : id.toString()) + ")");
		if (id == null) {
			return;
		}
		try {
			clearGraph();
			Book.TYPE type = getCbTypeSelected();
			if (type == null) {
				App.msg("Memoria.refreshGraph no type selected");
				return;
			}
			AbstractEntity entity = book.getEntity(type, id);
			if (entity == null) {
				App.msg("Memoria.refreshGraph no entity find for type=" + type.toString() + ", id=" + id.toString());
				return;
			}
			switch (Book.getTYPE(entity)) {
				case SCENE:
					GraphScene.create(this, entity);
					break;
				case PLOT:
					GraphPlot.create(this, entity);
					break;
				case PERSON:
					GraphPerson.create(this, entity);
					break;
				case LOCATION:
					GraphLocation.create(this, entity);
					break;
				case TAG:
					GraphTag.create(this, entity);
					break;
				case ITEM:
					GraphItem.create(this, entity);
					break;
				case POV:
					GraphPov.create(this, entity);
					break;
				default:
					return;
			}
			shownEntity = entity;
			treeLayout = new TreeLayout<>(graph);
			balloonLayout = new BalloonLayout<>(graph);
			Dimension dimension = mainFrame.getSize();
			balloonLayout.setSize(new Dimension(dimension.width / 2, dimension.height / 2));
			balloonLayout.setGraph(graph);
			if (ckBalloon.isSelected()) {
				vv.setGraphLayout(balloonLayout);
			} else {
				vv.setGraphLayout(treeLayout);
			}
			vv.repaint();
		} catch (Exception exc) {
			App.error("Memoria.refreshGraph(entity=" + id.toString() + ")", exc);
		}
	}

	private boolean isNothingSelected() {
		return shownEntity.id < 0L;
	}

	private void showMessage(String str) {
		Graphics2D graphics2D = (Graphics2D) vv.getGraphics();
		if (graphics2D == null) {
			return;
		}
		Rectangle rectangle = vv.getBounds();
		int i = (int) rectangle.getCenterX();
		int j = (int) rectangle.getCenterY();
		graphics2D.setColor(Color.lightGray);
		graphics2D.fillRect(i - 200, j - 20, 400, 40);
		graphics2D.setColor(Color.black);
		graphics2D.drawString(str, i - 180, j + 5);
	}

	private void scaleToLayout(ScalingControl scalingControl) {
		Dimension dimension1 = vv.getPreferredSize();
		if (vv.isShowing()) {
			dimension1 = vv.getSize();
		}
		Dimension dimension2 = vv.getGraphLayout().getSize();
		if (!dimension1.equals(dimension2)) {
			scalingControl.scale(vv, (float) (dimension1.getWidth() / dimension2.getWidth()), new Point2D.Double());
		}
	}

	@SuppressWarnings("unchecked")
	void removeDoublesFromInvolvedTags(Set<Tag> tagSet) {
		List<Tag> tagList = new ArrayList();
		involvedTags.forEach((tag) -> {
			tagSet.forEach((atag) -> {
				if (tag.id.equals(atag.id)) {
					tagList.add(atag);
				}
			});
		});
		tagList.forEach((tag) -> {
			involvedTags.remove(tag);
		});
	}

	@SuppressWarnings("unchecked")
	void removeDoublesFromInvolvedItems(Set<Item> itemSet) {
		List<Item> items = new ArrayList();
		involvedItems.forEach((aitem) -> {
			itemSet.forEach((item) -> {
				if (item.id.equals(aitem.id)) {
					items.add(aitem);
				}
			});
		});
		items.forEach((tag) -> {
			involvedItems.remove(tag);
		});
	}

	@SuppressWarnings("unchecked")
	void initVertices(AbstractEntity entity) {
		//App.trace("Memoria.initVertices(entity=" + entity.name + ")");
		GraphScene.init(this, entity);
		GraphPov.init(this, entity);
		GraphPlot.init(this, entity);
		GraphPerson.init(this, entity);
		GraphRelation.init(this, entity);
		GraphLocation.init(this, entity);
		GraphTag.init(this, entity);
		GraphItem.init(this, entity);
		GraphTag.initInvolded(this, entity);
		GraphItem.initInvolded(this, entity);

		sceneIds = new ArrayList<>();
		involvedTags = new HashSet<>();
		involvedItems = new HashSet<>();
		sceneVertexTitle = null;
		plotVertexTitle = null;
		locationVertexTitle = null;
		showTagVertex = true;
	}

	public void refresh(AbstractEntity entity) {
		//App.trace("Memoria.refresh(" + (entity != null ? entity.name : "null") + ")");
		if (entity == null || shownEntity == null) {
			return;
		}
		if (!Objects.equals(entity.id, shownEntity.id)) {
			return;
		}
		try {
			refreshGraph(entity.id);
		} catch (Exception exc) {
			App.error("Memoria.refresh(" + entity.toString() + ")", exc);
		}
	}

	public boolean hasAutoRefresh() {
		return cbAutoRefresh.isSelected();
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		App.trace("Memoria.actionPerformed(evt=" + evt.getSource().toString() + ")");
		String src = ((JComponent) evt.getSource()).getName();
		if (evt.getSource() instanceof JButton) {
			if (dateCombo.getSize().height == 0) {
				return;
			}
			int i = dateCombo.getSelectedIndex();
			switch(getAction(src)) {
				case BT_PREVIOUS:
					i--;
					if (i < 0) {
						return;
					}
					break;
				case BT_NEXT:
					i++;
					if (i > dateCombo.getItemCount() - 1) {
						return;
					}
					break;
				case BT_FIRST:
					i = 0;
					break;
				case BT_LAST:
					i = dateCombo.getItemCount() - 1;
					break;
				default:
					return;
			}
			dateCombo.setSelectedIndex(i);
			return;
		}
		if (src.equals("cbType")) {
			Book.TYPE id = (Book.TYPE) cbType.getSelectedItem();
			refreshCbEntity(id);
			return;
		}
		chosenDate = ((SbDate) dateCombo.getSelectedItem());
		refresh((AbstractEntity) cbEntity.getSelectedItem());
	}

	@Override
	public void refresh() {
		refreshGraph();
	}

	private boolean isInPeriod(SbPeriod period, SbDate date) {
		return (period == null) || (date == null) || (period.isInside(date));
	}

}
