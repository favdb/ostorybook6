/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Pov;
import db.entity.Scene;
import db.I18N;
import java.util.List;

/**
 *
 * @author favdb
 */
public class GraphPov {
	
	public static void init(Memoria m,AbstractEntity entity) {
		//App.trace("GraphPov.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.povVertex = new Pov();
		m.povVertex.setName(I18N.getMsg("povs"));
		m.graph.addVertex(m.povVertex);
		m.labelMap.put(m.povVertex, m.povVertex.name);
		m.iconMap.put(m.povVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.povVertex);
	}

	
	@SuppressWarnings("unchecked")
	public static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphPov.create(m, entity="+(entity==null?"null":entity.name)+")");
		Pov pov = (Pov) entity;
		if (pov == null) {
			return;
		}
		m.povVertex=pov;
		m.graphIndex = 0L;
		m.graph.addVertex(pov);
		m.labelMap.put(pov, pov.toString());
		m.iconMap.put(pov, pov.getIcon("large"));
		//find all Persons appearing in pov
		//first find all Scenes for the pov
		List<Scene> scenes = Scene.findByPov(m.book.scenes,pov);
		//second for each scene find all Persons
		scenes.forEach((scene)-> {
			if (!scene.persons.isEmpty()) {
				scene.persons.forEach((person) -> {
					GraphPerson.addForPov(m,person);
				});
			}
		});
	}

}
