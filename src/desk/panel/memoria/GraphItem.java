/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Relation;
import db.entity.Scene;
import db.I18N;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author favdb
 */
public class GraphItem {
	
	public static void init(Memoria m, AbstractEntity entity) {
		//App.trace("GraphItem.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.itemVertex = new Item();
		m.itemVertex.setName(I18N.getMsg("items"));
		m.graph.addVertex(m.itemVertex);
		m.labelMap.put(m.itemVertex, m.itemVertex.name);
		m.iconMap.put(m.itemVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.itemVertex);
	}

	public static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphItem.create(m, entity="+(entity==null?"null":entity.name)+")");
		Item item = (Item)entity;
		if (item == null) {
			return;
		}
		m.graphIndex = 0L;
		m.graph.addVertex(item);
		m.labelMap.put(item, item.toString());
		m.iconMap.put(item, item.getIcon(64));
		m.showTagVertex = false;
		m.initVertices(item);
		List<Scene> scenes = m.book.scenes;
		scenes.forEach((Scene scene) -> {
			if (scene.items.contains(item.id)) {
				GraphScene.add(m,scene);
				List<Location> locations = scene.locations;
				if (!locations.isEmpty()) {
					locations.forEach((location) -> {
						GraphLocation.add(m,location);
					});
				}
				List<Person> persons = scene.persons;
				if (!persons.isEmpty()) {
					persons.forEach((person) -> {
						GraphPerson.add(m,person);
					});
				}
				List<Item> items = scene.items;
				if (!items.isEmpty()) {
					items.forEach((litem) -> {
						addInvolved(m,litem);
					});
				}
			}
		});
	}
	
	public static void add(Memoria m, Item item) {
		//App.trace("GraphItem.add(m, item="+(item==null?"null":item.name)+")");
		if (item != null) {
			m.graph.addVertex(item);
			m.labelMap.put(item, item.toString());
			m.iconMap.put(item, item.getIcon(32));
			m.graph.addEdge(m.graphIndex++, m.itemVertex, item);
			List<Relation> relationships = Relation.findByItem(m.book.relations,item);
			GraphRelation.add(m,relationships, null);
		}
	}

	public static void addSet(Memoria m,Set<Item> paramSet) {
		//App.trace("GraphItem.addSet(m, paramSet)");
		if (!paramSet.isEmpty()) {
			for (Item item : paramSet) {
				add(m,item);
			}
		}
	}

	public static void initInvolded(Memoria m,AbstractEntity entity) {
		//App.trace("GraphItem.initInvolved(m, entity="+(entity==null?"null":entity.name)+")");
		m.involvedItemVertex = new Item();
		m.involvedItemVertex.setName(I18N.getMsg("graph.involved.items"));
		m.graph.addVertex(m.involvedItemVertex);
		m.labelMap.put(m.involvedItemVertex, m.involvedItemVertex.name);
		m.iconMap.put(m.involvedItemVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.involvedItemVertex);
	}

	public static void addInvolved(Memoria m,Item item) {
		//App.trace("GraphItem.addInvolved(m, item="+(item==null?"null":item.name)+")");
		if (item != null) {
			if (!isInGraph(m,item)) {
				m.graph.addVertex(item);
				m.labelMap.put(item, item.toString());
				m.iconMap.put(item, item.getIcon(32));
				m.graph.addEdge(m.graphIndex++, m.involvedItemVertex, item);
			}
		}
	}

	public static void addSet(Memoria m) {
		//App.trace("GraphItem.addSet(m)");
		if (!m.involvedItems.isEmpty()) {
			for (Item item : m.involvedItems) {
				addInvolved(m,item);
			}
		}
	}

	/*static void searchInvolved(MemoriaPanel m,List<ItemLink> itemLinks) {
		if (!itemLinks.isEmpty()) {
			for (ItemLink itemLink : itemLinks) {
				if (itemLink.hasOnlyScene()) {
					m.involvedItems.add(itemLink.getItem());
				}
			}
		}
	}*/

	public static boolean isInGraph(Memoria m, Item item) {
		//App.trace("GraphItem.isInGraph(m, item="+(item==null?"null":item.name)+")");
		if (item == null) {
			return false;
		}
		Collection<AbstractEntity> vertices = m.graph.getVertices();
		for (AbstractEntity entity : vertices) {
			if (entity instanceof Item && entity.id.equals(item.id)) {
				return true;
			}
		}
		return false;
	}

}
