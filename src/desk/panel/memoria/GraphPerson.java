/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Relation;
import db.entity.Scene;
import db.I18N;
import java.util.List;
import java.util.Set;

/**
 *
 * @author favdb
 */
public class GraphPerson {

	static void init(Memoria m, AbstractEntity entity) {
		//App.trace("GraphPerson.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.personVertex = new Person();
		m.personVertex.firstname=(I18N.getMsg("persons"));
		m.graph.addVertex(m.personVertex);
		m.labelMap.put(m.personVertex, m.personVertex.abbreviation);
		m.iconMap.put(m.personVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.personVertex);
	}

	static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphPerson.create(m, entity="+(entity==null?"null":entity.name)+")");
		Person person = (Person) entity;
		if (person == null) {
			return;
		}
		m.graphIndex = 0L;
		m.graph.addVertex(person);
		m.labelMap.put(person, person.toString());
		m.iconMap.put(person, person.getIconPerson("large"));
		m.initVertices(person);
		List<Scene> scenes = m.book.scenes;
		scenes.forEach((scene) -> {
			boolean c = false;
			if (m.chosenDate == null) {
				c = true;
			} else if ((scene.hasDate()) && (m.chosenDate.compareTo(scene.date) == 0)) {
				c = true;
			}
			if (c && (scene.persons.contains(person))) {
				GraphScene.add(m, scene);
				m.sceneIds.add(scene.id);
				List<Location> locations = scene.locations;
				locations.forEach((location) -> {
					GraphLocation.add(m, location);
				});
				List<Person> persons = scene.persons;
				persons.forEach((lperson) -> {
					if (!lperson.equals(person)) {
						add(m, lperson);
					}
				});
				List<Item> items = scene.items;
				items.forEach((item) -> {
					GraphItem.addInvolved(m, item);
				});
				List<Plot> plots = scene.plots;
				if (!plots.isEmpty()) {
					plots.forEach((plot) -> {
						GraphPlot.addInvolved(m, plot);
					});
				}
			}
		});
	}

	static void add(Memoria m, Person person) {
		//App.trace("GraphPerson.add(m, person="+(person==null?"null":person.name)+")");
		if (person == null) {
			return;
		}
		m.graph.addVertex(person);
		m.labelMap.put(person, person.getAbbr());
		m.iconMap.put(person, person.getIconPerson("medium"));
		m.graph.addEdge(m.graphIndex++, m.personVertex, person);
		List<Relation> relations;
		relations = Relation.findByPerson(m.book.relations, person);
		if (!relations.isEmpty()) {
			GraphRelation.add(m, relations, person);
		}
	}

	static void addForPov(Memoria m, Person person) {
		//App.trace("GraphPerson.addForPov(m, entity="+(person==null?"null":person.name)+")");
		if (person == null) {
			return;
		}
		m.graph.addVertex(person);
		m.labelMap.put(person, person.abbreviation);
		m.iconMap.put(person, person.getIconPerson("medium"));
		m.graph.addEdge(m.graphIndex++, m.povVertex, person);
	}

	void addSet(Memoria m, Set<Person> paramSet) {
		//App.trace("GraphPerson.addSet(m, paramSet)");
		if (!paramSet.isEmpty()) {
			paramSet.forEach((person) -> {
				add(m, person);
			});
		}
	}

}
