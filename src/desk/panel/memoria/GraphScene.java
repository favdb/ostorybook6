/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.SbDate;
import db.entity.Scene;
import db.I18N;
import java.util.List;
import java.util.Set;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class GraphScene {
	
	static void init(Memoria m,AbstractEntity entity) {
		//App.trace("GraphScene.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.sceneVertex = new Scene();
		m.sceneVertex.setName(I18N.getMsg("scenes"));
		m.graph.addVertex(m.sceneVertex);
		m.labelMap.put(m.sceneVertex, m.sceneVertex.toString());
		m.iconMap.put(m.sceneVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.sceneVertex);
	}

	@SuppressWarnings("unchecked")
	static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphScene.create(m, entity="+(entity==null?"null":entity.name)+")");
		if (entity==null) return;
		m.graphIndex = 0L;
		Scene scene = (Scene)entity;
		m.graph.addVertex(scene);
		m.labelMap.put(scene, scene.toString());
		m.iconMap.put(scene, scene.getIcon("large"));
		m.sceneVertexTitle = I18N.getMsg("graph.scenes.same.date");
		m.initVertices(scene);
		if (scene.hasDate()) {
			//scènes liés à la scène via la même date
			SbDate date = new SbDate(scene.date.getTime());
			long sceneId = scene.id;
			List<Scene> listScenes = Scene.findByDate(m.book.scenes,date);
				listScenes.forEach((lScene) -> {
					if (!lScene.id.equals(sceneId)) {
						add(m,lScene);
					}
				});
		}
		// liste des plots liés à la scène
		scene.plots.forEach((plot) -> {
			GraphPlot.add(m,plot);
		});
		// liste des personnes liées à la scène
		scene.persons.forEach((person) -> {
			GraphPerson.add(m,person);
		});
		// liste des lieux liés à la scène
		scene.locations.forEach((location) -> {
			GraphLocation.add(m,location);
		});
		// liste des items liés directement à la scène
		scene.items.forEach((item) -> {
			GraphItem.add(m,item);
		});
	}

	static void add(Memoria m,Scene scene) {
		//App.trace("GraphScene.add(m, scene="+(scene==null?"null":scene.name)+")");
		if (scene==null) return;
		m.graph.addVertex(scene);
		m.labelMap.put(scene, scene.toString());
		m.iconMap.put(scene, IconUtil.getIcon("medium/scene"));
		m.graph.addEdge(m.graphIndex++, m.sceneVertex, scene);
	}

	void addSet(Memoria m,Set<Scene> paramSet) {
		//App.trace("GraphScene.addSet(m, paramSet)");
			paramSet.forEach((scene) -> {
				m.add(m,scene);
			});
	}

}
