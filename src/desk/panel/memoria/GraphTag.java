/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Tag;
import db.I18N;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author favdb
 */
public class GraphTag {
	
	static void init(Memoria m,AbstractEntity entity) {
		//App.trace("GraphTag.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.tagVertex = new Tag();
		m.tagVertex.setName(I18N.getMsg("tags"));
		m.graph.addVertex(m.tagVertex);
		m.labelMap.put(m.tagVertex, m.tagVertex.name);
		m.iconMap.put(m.tagVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.tagVertex);
	}

	@SuppressWarnings("unchecked")
	static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphTag.create(m, entity="+(entity==null?"null":entity.name)+")");
		Tag tag = (Tag) entity;
		if (tag == null) {
			return;
		}
		m.graphIndex = 0L;
		m.graph.addVertex(tag);
		m.labelMap.put(tag, tag.toString());
		m.iconMap.put(tag, tag.getIcon("large"));
		m.showTagVertex = false;
		m.initVertices(tag);
	}

	static void add(Memoria m,Tag tag) {
		//App.trace("GraphTag.add(m, tag="+(tag==null?"null":tag.name)+")");
		if (tag != null) {
			m.graph.addVertex(tag);
			m.labelMap.put(tag, tag.toString());
			m.iconMap.put(tag, tag.getIcon("medium"));
			m.graph.addEdge(m.graphIndex++, m.tagVertex, tag);
		}
	}

	static void addSet(Memoria m,Set<Tag> paramSet) {
		//App.trace("GraphTag.addSet(m, paramSet)");
		paramSet.forEach((tag) -> {
			add(m,tag);
		});
	}

	static void initInvolded(Memoria m,AbstractEntity entity) {
		//App.trace("GraphTag.initInvolved(m, entity="+(entity==null?"null":entity.name)+")");
		m.involvedTagVertex = new Tag();
		m.involvedTagVertex.setName(I18N.getMsg("graph.involved.tags"));
		m.graph.addVertex(m.involvedTagVertex);
		m.labelMap.put(m.involvedTagVertex, m.involvedTagVertex.name);
		m.iconMap.put(m.involvedTagVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.involvedTagVertex);
	}

	static void addInvolved(Memoria m,Tag tag) {
		//App.trace("GraphTag.addInvolved(m, tag="+(tag==null?"null":tag.name)+")");
		if (tag != null) {
			if (!isInGraph(m,tag)) {
				m.graph.addVertex(tag);
				m.labelMap.put(tag, tag.toString());
				m.iconMap.put(tag, tag.getIcon("medium"));
				m.graph.addEdge(m.graphIndex++, m.involvedTagVertex, tag);
			}
		}
	}

	public static void addInvolved(Memoria m) {
		//App.trace("GraphTag.addInvolved(m)");
		if (!m.involvedTags.isEmpty()) {
			m.involvedTags.forEach((tag) -> {
				addInvolved(m,tag);
			});
		}
	}

	public static boolean isInGraph(Memoria m, Tag tag) {
		//App.trace("GraphTag.isInGraph(m, tag="+(tag==null?"null":tag.name)+")");
		if (tag == null) {
			return false;
		}
		Collection<AbstractEntity> collection = m.graph.getVertices();
		for(AbstractEntity entity:collection) {
			if (((entity instanceof Tag)) && (entity.id.equals(tag.id))) {
				return true;
			}
		}
		return false;
	}

}
