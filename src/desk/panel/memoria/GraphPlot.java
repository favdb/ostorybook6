/*
 * Copyright (C) 2018 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.memoria;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Scene;
import db.I18N;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import tools.IconUtil;

/**
 *
 * @author FaVdB
 */
public class GraphPlot {

	public static void init(Memoria m, AbstractEntity entity) {
		//App.trace("GraphPlot.init(m, entity="+(entity==null?"null":entity.name)+")");
		m.plotVertex = new Plot();
		m.plotVertex.setName(I18N.getMsg("plots"));
		m.graph.addVertex(m.plotVertex);
		m.labelMap.put(m.plotVertex, m.plotVertex.name);
		m.iconMap.put(m.plotVertex, m.emptyIcon);
		m.graph.addEdge(m.graphIndex++, entity, m.plotVertex);
	}

	public static void create(Memoria m, AbstractEntity entity) {
		//App.trace("GraphPlot.create(m, entity="+(entity==null?"null":entity.name)+")");
		Plot localPlot = (Plot) entity;
		if (localPlot == null) {
			return;
		}
		m.graphIndex = 0L;
		m.graph.addVertex(localPlot);
		m.labelMap.put(localPlot, localPlot.toString());
		m.iconMap.put(localPlot, IconUtil.getIcon("large/plot"));
		m.showTagVertex = false;
		m.initVertices(localPlot);
		List<Scene> scenes = m.book.scenes;
		scenes.forEach((scene) -> {
			if (scene.plots.contains(localPlot)) {
				GraphScene.add(m, scene);
				List<Location> locations = scene.locations;
				locations.forEach((loc) -> {
					GraphLocation.add(m, loc);
				});
				List<Person> persons = scene.persons;
				persons.forEach((person) -> {
					GraphPerson.add(m, person);
				});
			}
			List<Item> items = scene.items;
			items.forEach((item) -> {
				GraphItem.add(m, item);
			});

		});
	}

	public static void add(Memoria m, Plot plot) {
		//App.trace("GraphPlot.add(m, plot="+(plot==null?"null":plot.name)+")");
		if (plot != null) {
			m.graph.addVertex(plot);
			m.labelMap.put(plot, plot.toString());
			m.iconMap.put(plot, IconUtil.getIcon("medium/plot"));
			m.graph.addEdge(m.graphIndex++, m.plotVertex, plot);
		}
	}

	public static void addSet(Memoria m, Set<Plot> paramSet) {
		//App.trace("GraphPlot.addSet(m, paramSet)");
		paramSet.forEach((plot) -> {
			add(m, plot);
		});
	}

	public static void initInvolded(Memoria m, AbstractEntity entity) {
		//App.trace("GraphPlot.initInvolved(m, entity="+(entity==null?"null":entity.name)+")");
	}

	public static void addInvolved(Memoria m, Plot plot) {
		//App.trace("GraphPlot.addInvolved(m, plot="+(plot==null?"null":plot.name)+")");
	}

	public static void addSet(Memoria m) {
		//App.trace("GraphPlot.addSet(m)");
	}

	public static boolean isInGraph(Memoria m, Plot plot) {
		//App.trace("GraphPlot.isInGraph(m, plot="+(plot==null?"null":plot.name)+")");
		if (plot == null) {
			return false;
		}
		Collection collection = m.graph.getVertices();
		Iterator iterator = collection.iterator();
		while (iterator.hasNext()) {
			AbstractEntity entity = (AbstractEntity) iterator.next();
			if (((entity instanceof Plot)) && (entity.id.equals(plot.id))) {
				return true;
			}
		}
		return false;
	}

}
