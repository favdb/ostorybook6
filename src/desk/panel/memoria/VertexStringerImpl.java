/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.memoria;

/**
 *
 * @author martin
 */
import db.entity.AbstractEntity;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections15.Transformer;

class VertexStringerImpl<V> implements Transformer<V, String> {
    Map<AbstractEntity, String> map;
    boolean enabled;

    public VertexStringerImpl(Map<AbstractEntity, String> labelMap) {
        this.map = new HashMap<>();
        this.enabled = true;
        this.map = labelMap;
    }

	VertexStringerImpl() {
	}

	@Override
    public String transform(V paramV) {
        if (this.isEnabled()) {
            return "<html><table width='100'><tr><td>" + this.map.get(paramV) + "</td></tr></table>";
        }
        return "";
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean paramBoolean) {
        this.enabled = paramBoolean;
    }
}