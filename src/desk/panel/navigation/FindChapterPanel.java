/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.navigation;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import db.entity.Chapter;
import db.I18N;
import db.entity.Book;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;

import lib.miginfocom.swing.MigLayout;
import desk.tools.combobox.ComboUtil;
import desk.model.Ctrl;
import desk.panel.ViewsRadioButtonPanel;
import desk.tools.IconButton;
import desk.tools.swing.SwingUtil;
import desk.view.SbView.VIEWID;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class FindChapterPanel extends AbstractPanel {

	private JComboBox chapterCombo;
	private ViewsRadioButtonPanel viewsRbPanel;

	public FindChapterPanel(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		//App.trace("FindChapterPanel.initUi()");
		setLayout(new MigLayout("fillx,wrap 2", "[]10[grow]10[]", "[]10[]10[]"));

		JLabel lbChapter = new JLabel(I18N.getColonMsg("chapter"));

		chapterCombo = new JComboBox();
		Chapter chapter = new Chapter();
		ComboUtil.fillEntity(book, chapterCombo, Book.TYPE.CHAPTER, chapter, null, false, false, false);
		SwingUtil.setMaxWidth(chapterCombo, 200);

		IconButton btPrev = new IconButton("small/nav-previous", getPreviousAction());
		btPrev.setSize20x20();

		IconButton btNext = new IconButton("small/nav-next", getNextAction());
		btNext.setSize20x20();

		JLabel lbShow = new JLabel(I18N.getColonMsg("navigation.show.in"));
		viewsRbPanel = new ViewsRadioButtonPanel(mainFrame);

		JButton btFind = new JButton();
		btFind.setAction(getFindAction());
		btFind.setText(I18N.getMsg("z.find"));
		btFind.setIcon(IconUtil.getIcon("small/search"));
		SwingUtil.addEnterAction(btFind, getFindAction());

		// layout
		add(lbChapter);
		add(chapterCombo, "growx,span 2,split 3");
		add(btPrev);
		add(btNext);
		add(lbShow, "top");
		add(viewsRbPanel);
		add(btFind, "span,right");
	}

	private AbstractAction getPreviousAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				int index = chapterCombo.getSelectedIndex();
				--index;
				if (index == -1) {
					index = 0;
				}
				chapterCombo.setSelectedIndex(index);
				scrollToChapter();
			}
		};
	}

	private AbstractAction getNextAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				int index = chapterCombo.getSelectedIndex();
				++index;
				if (index == chapterCombo.getItemCount()) {
					index = chapterCombo.getItemCount() - 1;
				}
				chapterCombo.setSelectedIndex(index);
				scrollToChapter();
			}
		};
	}

	private AbstractAction getFindAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				scrollToChapter();
			}
		};
	}

	private void scrollToChapter() {
		Chapter chapter = (Chapter) chapterCombo.getSelectedItem();
		Ctrl ctrl = mainFrame.getBookController();
		if (viewsRbPanel.isChronoSelected()) {
			mainFrame.showView(VIEWID.VIEW_CHRONO);
			ctrl.chronoShowEntity(chapter);
			return;
		}
		if (viewsRbPanel.isBookSelected()) {
			mainFrame.showView(VIEWID.VIEW_WORK);
			ctrl.bookShowEntity(chapter);
			return;
		}
		if (viewsRbPanel.isManageSelected()) {
			mainFrame.showView(VIEWID.VIEW_MANAGE);
			ctrl.showInManage(chapter);
		}
	}
}
