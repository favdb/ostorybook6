/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.navigation;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import lib.miginfocom.swing.MigLayout;

import desk.action.ActionScrollToPovDate;
import db.entity.Pov;
import db.entity.SbDate;
import db.entity.Scene;
import db.I18N;
import db.entity.Book;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;
import desk.tools.combobox.ComboUtil;
import desk.listcell.LCRDate;
import desk.panel.ViewsRadioButtonPanel;
import desk.view.SbView;
import desk.panel.work.Work;
import desk.panel.chrono.Chrono;
import desk.tools.IconButton;
import desk.tools.swing.SwingUtil;
import desk.view.SbView.VIEWID;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class FindDatePanel extends AbstractPanel implements ItemListener {

	private JComboBox povCombo;
	private JComboBox dateCombo;
	private ViewsRadioButtonPanel viewsRbPanel;
	private JLabel lbWarning;

	public FindDatePanel(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		//App.trace("FindDatePanel.initUi()");
		setLayout(new MigLayout("fillx,wrap 2", "[]10[grow]", "[][]10[]"));

		JLabel lbChapter = new JLabel(I18N.getColonMsg("pov"));
		povCombo = new JComboBox();
		ComboUtil.fillEntity(book, povCombo, Book.TYPE.POV, null,null, false, false, false);
		povCombo.addItemListener(this);

		JLabel lbDate = new JLabel(I18N.getColonMsg("date"));
		dateCombo = new JComboBox();
		dateCombo.setRenderer(new LCRDate(mainFrame));
		refreshDateCombo();

		IconButton btPrev = new IconButton("small/nav-previous", getPreviousAction());
		btPrev.setSize20x20();

		IconButton btNext = new IconButton("small/nav-next", getNextAction());
		btNext.setSize20x20();

		JLabel lbShow = new JLabel(I18N.getColonMsg("navigation.show_in"));
		viewsRbPanel = new ViewsRadioButtonPanel(mainFrame, false);

		lbWarning = new JLabel(" ");

		JButton btFind = new JButton();
		btFind.setAction(getFindAction());
		btFind.setText(I18N.getMsg("z.find"));
		btFind.setIcon(IconUtil.getIcon("small/search"));
		SwingUtil.addEnterAction(btFind, getFindAction());

		// layout
		add(lbChapter);
		add(povCombo, "growx");
		add(lbDate);
		add(dateCombo, "growx,split 3");
		add(btPrev);
		add(btNext);
		add(lbShow, "top");
		add(viewsRbPanel);
		add(lbWarning, "span,split 2,left,growx");
		add(btFind, "right");
	}

	@SuppressWarnings("unchecked")
	private void refreshDateCombo() {
		//App.trace("FindDatePanel.refreshDateCombo()");
		Long pov = (Long) povCombo.getSelectedItem();
		if (pov==null) return;
		dateCombo.removeAllItems();
		List<SbDate> dates = Scene.findDistinctDatesByPov(book.scenes,book.getPov(pov));
		dates.forEach((date)-> {
			dateCombo.addItem(date);
		});
	}

	private AbstractAction getPreviousAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				int index = dateCombo.getSelectedIndex();
				--index;
				if (index == -1) {
					index = 0;
				}
				dateCombo.setSelectedIndex(index);
				scrollToPovDate();
			}
		};
	}

	private AbstractAction getNextAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				int index = dateCombo.getSelectedIndex();
				++index;
				if (index == dateCombo.getItemCount()) {
					index = dateCombo.getItemCount() - 1;
				}
				dateCombo.setSelectedIndex(index);
				scrollToPovDate();
			}
		};
	}

	private AbstractAction getFindAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				scrollToPovDate();
			}
		};
	}

	@SuppressWarnings("null")
	private void scrollToPovDate() {
		Pov pov = (Pov) povCombo.getSelectedItem();
		SbDate date = (SbDate) dateCombo.getSelectedItem();
		SbView view = null;
		boolean ischrono = viewsRbPanel.isChronoSelected();
		boolean isbook = viewsRbPanel.isBookSelected();
		AbstractPanel container = null;
		JPanel panel = null;
		if (ischrono) {
			view = mainFrame.getView(VIEWID.VIEW_CHRONO);
			mainFrame.showView(VIEWID.VIEW_CHRONO);
			container = (Chrono) view.getComponent();
			panel = ((Chrono) container).getPanel();
		} else if (isbook) {
			view = mainFrame.getView(VIEWID.VIEW_WORK);
			mainFrame.showView(VIEWID.VIEW_WORK);
			container = (Work) view.getComponent();
			panel = ((Work) container).getPanel();
		}
		int delay = 20;
		if (!view.isLoaded()) {
			delay += 180;
		}
		if (isbook) {
			delay += 100;
		}
		ActionScrollToPovDate action = new ActionScrollToPovDate(container, panel, pov, date, lbWarning);
		Timer timer = new Timer(delay, action);
		timer.setRepeats(false);
		timer.start();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
			refreshDateCombo();
		}
	}
}
