/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.navigation;

import db.entity.AbstractEntity;
import db.entity.Book;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;
import desk.model.Ctrl;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.JTabbedPane;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class Navigation extends AbstractPanel {

	public enum ACTION {
		REFRESH("Navigation_Refresh"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getNavigationAction(String str) {
		for (ACTION a:ACTION.values()) {
			if (a.toString().equals(str)) return(a);
		}
		return(ACTION.NONE);
	}

	private JTabbedPane tabbedPane;

	public Navigation(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Navigation.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();

		if (ACTION.REFRESH.check(propName)) {
			View newView = (View) newValue;
			View view = (View) getParent().getParent();
			if (view == newView) {
				refresh();
			}
			return;
		}
		switch (Ctrl.whichACTION(propName)) {
			case EDIT:
			case INIT:
			case NONE:
				return;
		}
		if (newValue instanceof AbstractEntity) {
			switch (Book.getTYPE((AbstractEntity) newValue)) {
				case SCENE:
				case CHAPTER:
				case POV:
					refresh();
			}
		}
	}

	@Override
	public void refresh() {
		int index = tabbedPane.getSelectedIndex();
		super.refresh();
		tabbedPane.setSelectedIndex(index);
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("wrap,fill,ins 0"));

		tabbedPane = new JTabbedPane();
		tabbedPane.addTab(I18N.getMsg("navigate.goto.chapter"), new FindChapterPanel(mainFrame));
		tabbedPane.addTab(I18N.getMsg("navigate.goto.date"), new FindDatePanel(mainFrame));
		add(tabbedPane, "grow");
	}
}
