/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.tree;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Person;
import db.entity.Scene;
import db.entity.Status;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import tools.html.HtmlUtil;

@SuppressWarnings("serial")
class TreeEntityCellRenderer extends DefaultTreeCellRenderer {

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel,
			boolean expanded, boolean leaf, int row, boolean hasFocus) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
		Object object = node.getUserObject();
		if (leaf) {
			if (object instanceof AbstractEntity) {
				switch(Book.getTYPE((AbstractEntity)object)) {
					case PERSON:
						Person p=(Person)object;
						setLeafIcon(p.gender.getIcon("small"));
						setText(p.getFullName());
						break;
					case SCENE:
						Scene scene=(Scene)object;
						setLeafIcon(Status.getIcon(scene.status));
						setText(scene.getChapterSceneNo(expanded));
						break;
					default:
						Icon icon = ((AbstractEntity) object).getIcon("small");
						setLeafIcon(icon);
				}
			} else {
				setLeafIcon(null);
			}
		}
		super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
		if (!leaf && object instanceof AbstractEntity) {
			AbstractEntity abs=(AbstractEntity)object;
			Icon icon = abs.getIcon();
			setIcon(icon);
			setText(abs.name);
		}
		if (!(object instanceof AbstractEntity)) {
			setToolTipText(null);
		} else {
			AbstractEntity abs=(AbstractEntity) object;
			String texte = abs.getNotes();
			if (!HtmlUtil.htmlToText(texte).isEmpty()) {
				setToolTipText("<html>" + texte + "</html>");
			} else {
				setToolTipText(null);
			}
		}
		return this;
	}

}
