/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.tree;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Scene;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Status;
import db.entity.Tag;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.dialog.edit.Editor;
import desk.panel.AbstractPanel;
import desk.tools.EntityUtil;
import desk.tools.IconButton;
import desk.tools.swing.SwingUtil;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import lib.miginfocom.swing.MigLayout;
import lib.infonode.docking.View;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class Tree extends AbstractPanel
		implements TreeSelectionListener, MouseListener, ActionListener {

	public enum ACTION {
		BT_SHOW_ALL("btShowAll"),
		REFRESH("Tree_Refresh"),
		SHOWINFO("Tree_ShowInfo"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private TreeJTree tree;
	private JScrollPane scroller;

	private DefaultMutableTreeNode topNode;
	private TreeEntityNode personsByCategoryNode;
	private TreeEntityNode personsByGendersNode;
	private TreeEntityNode ideasNode;
	private TreeEntityNode itemsNode;
	private TreeEntityNode locationsNode;
	private TreeEntityNode memosNode;
	private TreeEntityNode scenesNode;
	private TreeEntityNode povsNode;
	private TreeEntityNode partsNode;
	private TreeEntityNode plotsNode;
	private TreeEntityNode tagsNode;
	private JCheckBoxMenuItem mnuPerson;
	private JCheckBoxMenuItem mnuLocation;
	private JCheckBoxMenuItem mnuTag;
	private JCheckBoxMenuItem mnuItem;
	private JCheckBoxMenuItem mnuPlot;
	private JCheckBoxMenuItem mnuIdea;
	private JCheckBoxMenuItem mnuPov;
	private JCheckBoxMenuItem mnuScene;
	private List<JCheckBoxMenuItem> mnuList;

	public Tree(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Tree.modelPropertyChange(evt="+evt.toString()+")");
		Object oldValue = evt.getOldValue();
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				View newView = (View) newValue;
				View view = (View) getParent().getParent();
				if (view == newView) {
					refreshAll();
				}
				return;
		}

		if (newValue instanceof AbstractEntity) {
			boolean ret = refreshNode((AbstractEntity) newValue, (AbstractEntity) oldValue);
			if (!ret) {
				refreshAll();
			}
			return;
		}
		if (oldValue instanceof AbstractEntity) {
			refreshAll();
//			return;
		}
	}

	private boolean refreshNode(AbstractEntity updEntity, AbstractEntity oldEntity) {
		TreePath[] paths = getPaths(tree, false);
		for (TreePath path : paths) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
			if (node.isLeaf()) {
				Object o = node.getUserObject();
				if (o instanceof AbstractEntity) {
					AbstractEntity entity = (AbstractEntity) o;
					if (entity.id.equals(updEntity.id)) {
						if (hasHierarchyChanged(oldEntity, updEntity)) {
							return false;
						}
						changeTreeNode(node);
						return true;
					}
				}
			}
		}
		return false;
	}

	public TreePath[] getPaths(TreeJTree tree, boolean expanded) {
		TreeNode root = (TreeNode) tree.getModel().getRoot();
		List<Object> list = new ArrayList<>();
		getPaths(tree, new TreePath(root), expanded, list);
		return (TreePath[]) list.toArray(new TreePath[list.size()]);
	}

	public void getPaths(TreeJTree tree, TreePath parent, boolean expanded, List<Object> list) {
		if (expanded && !tree.isVisible(parent)) {
			return;
		}
		list.add(parent);
		TreeNode node = (TreeNode) parent.getLastPathComponent();
		if (node.getChildCount() >= 0) {
			for (Enumeration<?> e = node.children(); e.hasMoreElements();) {
				TreeNode n = (TreeNode) e.nextElement();
				TreePath path = parent.pathByAddingChild(n);
				getPaths(tree, path, expanded, list);
			}
		}
	}

	@Override
	public void init() {
	}

	@Override
	@SuppressWarnings("ResultOfObjectAllocationIgnored")
	public void initUi() {
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("wrap,fill,ins 0"));
		setMinimumSize(new Dimension(280, 180));

		add(initToolbar(!WITH_PART), "wrap");

		// top node
		topNode = new DefaultMutableTreeNode(mainFrame.book.getTitle());
		tree = new TreeJTree(topNode, TreeSelectionModel.SINGLE_TREE_SELECTION,new TreeEntityCellRenderer());
		new TreeDefaultTransferHandler(this, DnDConstants.ACTION_MOVE);
		ToolTipManager.sharedInstance().registerComponent(tree);
		scroller = new JScrollPane(tree);
		SwingUtil.setMaxPreferredSize(scroller);
		add(scroller, "grow");

		refreshAll();
		tree.addTreeSelectionListener(this);
		tree.addMouseListener(this);
	}

	private JPopupMenu initPopup() {
		mnuList = new ArrayList<>();
		JPopupMenu popupMenu = new JPopupMenu("Title");
		//mnuPart=initMenuItem("part",false);mnuList.add(mnuPart);
		mnuScene = initMenuItem("scene", true);
		mnuList.add(mnuScene);
		//mnuChapter=initMenuItem("chapter",true);mnuList.add(mnuChapter);
		mnuPov = initMenuItem("pov", false);
		mnuList.add(mnuPov);
		mnuPerson = initMenuItem("person", true);
		mnuList.add(mnuPerson);
		mnuLocation = initMenuItem("location", true);
		mnuList.add(mnuLocation);
		mnuItem = initMenuItem("item", true);
		mnuList.add(mnuItem);
		mnuTag = initMenuItem("tag", true);
		mnuList.add(mnuTag);
		mnuPlot = initMenuItem("plot", true);
		mnuList.add(mnuPlot);
		mnuIdea = initMenuItem("idea", true);
		mnuList.add(mnuIdea);
		mnuList.forEach((m) -> {
			popupMenu.add(m);
		});

		return (popupMenu);

	}

	private JCheckBoxMenuItem initMenuItem(String title, boolean checked) {
		JCheckBoxMenuItem menu = new JCheckBoxMenuItem(I18N.getMsg(title));
		menu.setName("menu" + title);
		menu.setIcon(IconUtil.getIcon("small/" + title));
		menu.setSelected(checked);
		menu.addActionListener(this);
		return (menu);
	}

	@Override
	public JToolBar initToolbar(boolean withPart) {
		super.initToolbar(withPart);
		JPopupMenu popup = initPopup();
		JButton btTree = SwingUtil.initButton("", "small/tree", "");
		btTree.setComponentPopupMenu(popup);
		btTree.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.getButton() == 1) {
					popup.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		toolbar.add(btTree);

		// tree control buttons
		IconButton btShowAll = new IconButton("small/all", "tree.show.all", actionShowAll());
		btShowAll.setControlButton();
		toolbar.add(btShowAll/*, "top"*/);
		IconButton btShowNone = new IconButton("small/none", "tree.show.none", actionShowNone());
		btShowNone.setControlButton();
		toolbar.add(btShowNone/*, "top"*/);
		IconButton btExpand = new IconButton("small/expand", "tree.expand.all", actionExpand());
		btExpand.setControlButton();
		toolbar.add(btExpand/*, "top"*/);
		IconButton btCollapse = new IconButton("small/collapse", "tree.collapse.all", actionCollapse());
		btCollapse.setControlButton();
		toolbar.add(btCollapse/*, "top"*/);
		return (toolbar);
	}

	public void refreshAll() {
		String treeState = TreeUtil.getExpansionState(tree, 0);
		topNode.removeAllChildren();

		if (mnuScene.isSelected()) {
			scenesNode = new TreeEntityNode("scenes", new Scene());
			topNode.add(scenesNode);
			refreshScenes();
		}
		if (mnuPov.isSelected()) {
			povsNode = new TreeEntityNode(new Pov());
			topNode.add(povsNode);
			refreshPovs();
		}
		if (mnuPerson.isSelected()) {
			personsByCategoryNode = new TreeEntityNode("tree.persons.by.category", new Person());
			topNode.add(personsByCategoryNode);
			refreshPersonsByCategory();
			personsByGendersNode = new TreeEntityNode("tree.persons.by.gender", new Gender());
			topNode.add(personsByGendersNode);
			refreshPersonsByGender();
		}
		if (mnuLocation.isSelected()) {
			locationsNode = new TreeEntityNode(new Location());
			topNode.add(locationsNode);
			refreshLocations();
		}
		if (mnuItem.isSelected()) {
			itemsNode = new TreeEntityNode(new Item());
			topNode.add(itemsNode);
			refreshItems();
		}
		if (mnuTag.isSelected()) {
			tagsNode = new TreeEntityNode(new Tag());
			topNode.add(tagsNode);
			refreshTags();
		}
		if (mnuPlot.isSelected()) {
			plotsNode = new TreeEntityNode(new Plot());
			topNode.add(plotsNode);
			refreshPlots();
		}
		if (mnuIdea.isSelected()) {
			ideasNode = new TreeEntityNode(new Idea());
			topNode.add(ideasNode);
			refreshIdeas();
			memosNode = new TreeEntityNode(new Memo());
			topNode.add(memosNode);
			refreshMemos();
		}
		reloadTreeModel();
		TreeUtil.restoreExpanstionState(tree, 0, treeState);
	}

	private void changeTreeNode(TreeNode node) {
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		model.nodeChanged(node);
	}

	private void reloadTreeModel() {
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		model.reload();
	}

	private void refreshPovs() {
		book.povs.forEach((pov) -> {
			povsNode.add(new DefaultMutableTreeNode(pov));
		});
	}

	private void refreshParts() {
		book.parts.forEach((part) -> {
			partsNode.add(new DefaultMutableTreeNode(part));
		});
	}

	private DefaultMutableTreeNode createPartNode(Map<Part, DefaultMutableTreeNode> partMap, Part part, DefaultMutableTreeNode root) {
		if (part == null) {
			return (null);
		}
		DefaultMutableTreeNode node = partMap.get(part);
		if (node == null) {
			DefaultMutableTreeNode supernode = root;
			if (part.hasSup()) {
				Part superPart = part.sup;
				supernode = createPartNode(partMap, superPart, root);
			}
			node = new DefaultMutableTreeNode(part);
			supernode.add(node);
			partMap.put(part, node);
		}
		return node;
	}

	private void refreshIdeas() {
		for (Status.STATUS state : Status.STATUS.values()) {
			DefaultMutableTreeNode stateNode = new DefaultMutableTreeNode(Status.getMsg(state));
			ideasNode.add(stateNode);
			List<Idea> ideas = Idea.findByStatus(book.ideas, state);
			ideas.forEach((idea) -> {
				stateNode.add(new DefaultMutableTreeNode(idea));
			});
		}
	}

	private void refreshMemos() {
		book.memos.forEach((memo) -> {
			memosNode.add(new DefaultMutableTreeNode(memo));
		});
	}

	private void refreshPersonsByCategory() {
		Map<Category, DefaultMutableTreeNode> categoryMap = new HashMap<>();
		List<Category> categories = Category.findBeacon(book.categories, Category.BEACON.PERSON.toString());
		categories.forEach((category) -> {
			getPersonsByCategoryNodeOwner(categoryMap, category);
		});
		categories.forEach((category) -> {
			DefaultMutableTreeNode categoryNode = categoryMap.get(category);
			List<Person> persons = Person.findByCategory(book.persons, category);
			persons.forEach((person) -> {
				categoryNode.add(new DefaultMutableTreeNode(person));
			});
		});
	}

	private DefaultMutableTreeNode getPersonsByCategoryNodeOwner(
			Map<Category, DefaultMutableTreeNode> categoryMap, Category category) {
		DefaultMutableTreeNode categoryNode = categoryMap.get(category);
		if (categoryNode == null && category.hasSup()) {
			categoryNode = new DefaultMutableTreeNode(category);
			DefaultMutableTreeNode supCategoryNode = personsByCategoryNode;
			if (category.sup != null) {
				supCategoryNode = categoryMap.get(category.sup);
				if ((supCategoryNode == null) && (!category.sup.equals(category))) {
					supCategoryNode = getPersonsByCategoryNodeOwner(categoryMap, category.sup);
				}
			}
			if (supCategoryNode != null) {
				supCategoryNode.add(categoryNode);
			}
			categoryMap.put(category, categoryNode);
		}
		return categoryNode;
	}

	private void refreshPersonsByGender() {
		book.genders.forEach((gender) -> {
			DefaultMutableTreeNode genderNode = new DefaultMutableTreeNode(gender);
			personsByGendersNode.add(genderNode);
			List<Person> persons = Person.findByGender(book.persons, gender);
			persons.forEach((person) -> {
				genderNode.add(new DefaultMutableTreeNode(person));
			});
		});
	}

	private void refreshLocations() {
		Map<Location, DefaultMutableTreeNode> sites = new HashMap<>();
		Map<String, DefaultMutableTreeNode> nodes = new HashMap<>();
		List<String> countries = Location.findCountries(book);
		countries.forEach((country) -> {
			DefaultMutableTreeNode countryNode = locationsNode;
			if ((country != null && (!country.isEmpty()))) {
				if (nodes.get(country) != null) {
					countryNode = nodes.get(country);
				} else {
					StringCategory cat1 = new StringCategory(country);
					countryNode = new DefaultMutableTreeNode(cat1);
					locationsNode.add(countryNode);
					nodes.put(country, countryNode);
				}
			}
			List<String> cities = Location.findCitiesByCountry(book.locations, country);
			for (String city : cities) {
				DefaultMutableTreeNode cityNode = countryNode;
				if (city != null && (!city.isEmpty())) {
					if (nodes.get(city) != null) {
						cityNode = nodes.get(city);
					} else {
						StringCategory cat2 = new StringCategory(city);
						cityNode = new DefaultMutableTreeNode(cat2);
						countryNode.add(cityNode);
						nodes.put(city, cityNode);
					}
				}
				List<Location> locations = Location.findByCountryCity(book.locations, country, city);
				for (Location location : locations) {
					DefaultMutableTreeNode node = insertLocation(location, cityNode, sites);
					nodes.put(location.name, node);
				}
			}
		});
	}

	private DefaultMutableTreeNode insertLocation(Location location, DefaultMutableTreeNode cityNode,
			Map<Location, DefaultMutableTreeNode> sites) {
		// already inserted
		if (sites.get(location) != null) {
			return sites.get(location);
		}

		DefaultMutableTreeNode locationNode = new DefaultMutableTreeNode(location);
		if (location.hasSup()) {
			DefaultMutableTreeNode siteNode = sites.get(location.sup);
			if (siteNode == null) {
				siteNode = insertLocation(location.sup, cityNode, sites);
			}
			siteNode.add(locationNode);
		} else {
			cityNode.add(locationNode);
		}
		sites.put(location, locationNode);
		return locationNode;
	}

	private void refreshScenes() {
		// unassigned scenes
		DefaultMutableTreeNode uNode = new TreeEntityNode("scene.unassigned", new Chapter());
		scenesNode.add(uNode);
		book.scenes.forEach((scene) -> {
			if (scene.chapter == null) {
				DefaultMutableTreeNode sceneNode = new DefaultMutableTreeNode(scene);
				uNode.add(sceneNode);
			}
		});

		Map<Part, DefaultMutableTreeNode> partMap = new HashMap<>();
		book.parts.forEach((part) -> {
			DefaultMutableTreeNode partNode = new DefaultMutableTreeNode(part);
			scenesNode.add(partNode);
			book.chapters.forEach((chapter) -> {
				if (chapter.hasPart() && part.id.equals(chapter.part.id)) {
					DefaultMutableTreeNode chapterNode = new DefaultMutableTreeNode(chapter);
					partNode.add(chapterNode);
					book.scenes.forEach((scene) -> {
						if (chapter.id.equals(scene.chapter.id)) {
							DefaultMutableTreeNode sceneNode = new DefaultMutableTreeNode(scene);
							chapterNode.add(sceneNode);
						}
					});
				}
			});
		});
	}

	private void refreshTags() {
		book.tags.forEach((tag) -> {
			DefaultMutableTreeNode tagNode = new DefaultMutableTreeNode(tag);
			tagsNode.add(tagNode);
		});
	}

	private void refreshItems() {
		book.items.forEach((item) -> {
			DefaultMutableTreeNode itemNode = new DefaultMutableTreeNode(item);
			itemsNode.add(itemNode);
		});
	}

	private void refreshPlots() {
		book.plots.forEach((plot) -> {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(plot);
			plotsNode.add(node);
		});
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		//App.trace("Tree.valueChange(e="+e.toString()+")");
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		if (node == null) {
			return;
		}
		Object value = node.getUserObject();
		if (value == null) {
			return;
		}
		if (node.isRoot()) {
			mainFrame.showInfo(book);
		}
		if (!(value instanceof AbstractEntity)) {
			return;
		}
		mainFrame.getView(VIEWID.VIEW_INFO).cleverRestoreFocus();
		mainFrame.showInfo((AbstractEntity) value);
	}

	@SuppressWarnings("null")
	public static boolean hasHierarchyChanged(AbstractEntity oldEntity, AbstractEntity updEntity) {
		if (oldEntity == null || updEntity == null) {
			return false;
		}
		if (oldEntity instanceof Plot) {
			return false;
		}
		if (oldEntity instanceof Idea) {
			Idea old = (Idea) oldEntity;
			Idea upd = (Idea) updEntity;
			return(!old.status.equals(upd.status));
		}
		if (oldEntity instanceof Scene) {
			Scene old = (Scene) oldEntity;
			Scene upd = (Scene) updEntity;
			if (old.chapter == null) {
				return true;
			}
			if (old.chapter != null) {
				return true;
			}
			return old.chapter != null
					&& (!Objects.equals(old.chapter, upd.chapter));
		}
		if (oldEntity instanceof Person) {
			Person old = (Person) oldEntity;
			Person upd = (Person) updEntity;
			if (!Objects.equals(old.category, upd.category)) {
				return true;
			}
			return !Objects.equals(old.gender, upd.gender);
		}
		if (oldEntity instanceof Relation) {
			return false;
		}
		if (oldEntity instanceof Location) {
			Location old = (Location) oldEntity;
			Location upd = (Location) updEntity;
			Location oldSite = old.sup;
			Location updSite = upd.sup;
			String oldCity = old.city;
			String updCity = upd.city;
			String oldCountry = old.country;
			String updCountry = upd.country;
			if (oldSite == null && updSite != null) {
				return true;
			}
			if (oldSite != null) {
				if (updSite == null) {
					return true;
				}
				if (!oldSite.equals(updSite)) {
					return true;
				}
			}
			if (oldCity == null && updCity != null) {
				return true;
			}
			if (oldCity != null) {
				if (updCity == null) {
					return true;
				}
				if (!oldCity.equals(updCity)) {
					return true;
				}
			}
			if (oldCountry == null && updCountry != null) {
				return true;
			}
			if (oldCountry != null) {
				if (updCountry == null) {
					return true;
				}
			} else {
				return (!oldCountry.equals(updCountry));
			}
		}
		if (oldEntity instanceof Tag) {
			Tag old = (Tag) oldEntity;
			Tag upd = (Tag) updEntity;
			if (old.category == null) {
				return false;
			}
			if (old.category != null) {
				return true;
			}
			return !old.category.equals(upd.category);
		}
		if (oldEntity instanceof Item) {
			Item old = (Item) oldEntity;
			Item upd = (Item) updEntity;
			if (old.category == null) {
				return false;
			}
			if (old.category != null) {
				return true;
			}
			return(!old.category.equals(upd.category));
		}
		return false;
	}

	private AbstractAction actionShowAll() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				mnuList.forEach((button) -> {
					button.setSelected(true);
				});
				refreshAll();
			}
		};
	}

	private AbstractAction actionShowNone() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				mnuList.forEach((button) -> {
					button.setSelected(false);
				});
				refreshAll();
			}
		};
	}

	private AbstractAction actionExpand() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				for (int i = 0; i < tree.getRowCount(); i++) {
					tree.expandRow(i);
				}
			}
		};
	}

	private AbstractAction actionCollapse() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				DefaultMutableTreeNode currentNode = topNode.getNextNode();
				do {
					if (currentNode.getLevel() == 1) {
						tree.collapsePath(new TreePath(currentNode.getPath()));
					}
					currentNode = currentNode.getNextNode();
				} while (currentNode != null);
			}
		};
	}

	private void showPopupMenu(MouseEvent evt) {
		evt.consume();
		TreePath selectedPath = tree.getPathForLocation(evt.getX(), evt.getY());
		DefaultMutableTreeNode selectedNode = null;
		try {
			selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
		} catch (Exception e) {
			// ignore
		}
		if (selectedNode == null) {
			return;
		}
		Object userObj = selectedNode.getUserObject();
		if (!(userObj instanceof AbstractEntity)) {
			return;
		}
		JPopupMenu menu = null;
		if (userObj instanceof AbstractEntity) {
			AbstractEntity entity = (AbstractEntity) userObj;
			menu = EntityUtil.createPopupMenu(mainFrame, entity);
		}
		if (menu == null) {
			return;
		}
		tree.setSelectionPath(selectedPath);
		JComponent comp = (JComponent) tree.getComponentAt(evt.getPoint());
		Point p = SwingUtilities.convertPoint(comp, evt.getPoint(), this);
		menu.show(this, p.x, p.y);
	}

	private void actionEntityToEdit(MouseEvent me) {
		TreePath selectedPath = tree.getPathForLocation(me.getX(), me.getY());
		DefaultMutableTreeNode selectedNode = null;
		try {
			selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
		} catch (Exception ex) {
			// ignore
		}
		if (selectedNode == null) {
			return;
		}
		// tree.setSelectionPath(selectedPath);
		if (selectedNode.isLeaf()) {
			if (selectedNode.getUserObject() instanceof AbstractEntity) {
				Editor.show(mainFrame, (AbstractEntity) selectedNode.getUserObject(), null);
			}
		}
	}

	private void actionEntityToShow(MouseEvent me) {
		TreePath selectedPath = tree.getPathForLocation(me.getX(), me.getY());
		DefaultMutableTreeNode selectedNode = null;
		try {
			selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
		} catch (Exception ex) {
			// ignore
		}
		if (selectedNode == null) {
			return;
		}
		// tree.setSelectionPath(selectedPath);
		if (selectedNode.isLeaf()) {
			if (selectedNode.getUserObject() instanceof AbstractEntity) {
				mainFrame.showInfo((AbstractEntity) selectedNode.getUserObject());
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent me) {
	}

	@Override
	public void mousePressed(MouseEvent me) {
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		switch (me.getButton()) {
			case MouseEvent.BUTTON1: //left click
				if (me.getClickCount() == 2 && !me.isConsumed()) { // double click
					me.consume();
					actionEntityToEdit(me);
				} else {
					actionEntityToShow(me);
				}
				break;
			case MouseEvent.BUTTON2: //middle click
				me.consume();
				break;
			case MouseEvent.BUTTON3: //right click
				me.consume();
				showPopupMenu(me);
				break;
		}
	}

	@Override
	public void mouseEntered(MouseEvent me) {
	}

	@Override
	public void mouseExited(MouseEvent me) {
	}

	public TreeJTree getTree() {
		return tree;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String compName = ((Component) e.getSource()).getName();
		if (compName == null || !compName.startsWith("menu")) {
			return;
		}
		refreshAll();
	}

	public class StringCategory {

		private final String name;

		public StringCategory(String name) {
			switch (name) {
				case "nocountry":
					this.name=I18N.getMsg("location.nocountry");
					break;
				case "nocity":
					this.name=I18N.getMsg("location.nocity");
					break;
				default:
					this.name = name;
					break;
			}
		}

		@Override
		public String toString() {
			return name;
		}

		public String getName() {
			return name;
		}
	}
}
