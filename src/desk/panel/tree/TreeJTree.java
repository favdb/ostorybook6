/**
 * 
 */
package desk.panel.tree;

import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;

/**
 * @author jean
 *
 */
@SuppressWarnings("serial")
public class TreeJTree extends JTree {

	public TreeJTree() {
	}

	public TreeJTree(Object[] arg0) {
		super(arg0);
	}

	public TreeJTree(TreeNode arg0) {
		super(arg0);
	}

	public TreeJTree(TreeModel arg0) {
		super(arg0);
	}

	public TreeJTree(TreeNode arg0, boolean arg1) {
		super(arg0, arg1);
	}

	public TreeJTree(TreeNode arg0, int selModel, TreeCellRenderer renderer) {
		super(arg0);
		getSelectionModel().setSelectionMode(selModel);
		setCellRenderer(renderer);
	}

	Insets autoscrollInsets = new Insets(20, 20, 20, 20); // insets

	public void autoscroll(Point cursorLocation) {
		Insets insets = getAutoscrollInsets();
		Rectangle outer = getVisibleRect();
		Rectangle inner = new Rectangle(outer.x + insets.left, outer.y + insets.top,
				outer.width - (insets.left + insets.right), outer.height - (insets.top + insets.bottom));
		if (!inner.contains(cursorLocation)) {
			Rectangle scrollRect = new Rectangle(cursorLocation.x - insets.left, cursorLocation.y - insets.top,
					insets.left + insets.right, insets.top + insets.bottom);
			scrollRectToVisible(scrollRect);
		}
	}

	public Insets getAutoscrollInsets() {
		return (autoscrollInsets);
	}
}
