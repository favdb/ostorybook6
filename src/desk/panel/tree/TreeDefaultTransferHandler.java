package desk.panel.tree;

import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import desk.model.Ctrl;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import desk.app.MainFrame;

public class TreeDefaultTransferHandler extends TreeTransferHandlerAbstract {

	public TreeDefaultTransferHandler(Tree treePanel, int action) {
		super(treePanel, action, true);
	}

	@Override
	public boolean canPerformAction(TreeJTree target, TreeNode draggedNode, int action, Point location) {
		TreePath pathTarget = target.getPathForLocation(location.x, location.y);
		if (pathTarget == null) {
			target.setSelectionPath(null);
			return (false);
		}
		target.setSelectionPath(pathTarget);
		if (action == DnDConstants.ACTION_MOVE) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) pathTarget.getLastPathComponent();
			Object draggedObject = ((DefaultMutableTreeNode) draggedNode).getUserObject();
			if (node != null) {
				Object targetObject = node.getUserObject();
				if (targetObject != null) {
					if (draggedObject instanceof Scene) {
						return (targetObject instanceof Chapter) || (targetObject instanceof Scene);
					} else if (draggedObject instanceof Chapter) {
						return (targetObject instanceof Part) || (targetObject instanceof Chapter);
					}
				}
			}
		}
		return (false);
	}

	@Override
	public boolean executeDrop(TreeJTree targetTree, TreeNode draggedNode, TreeNode targetNode, int action) {
		if (action == DnDConstants.ACTION_MOVE) {
			Object draggedObject = ((DefaultMutableTreeNode) draggedNode).getUserObject();
			Object targetObject = ((DefaultMutableTreeNode) targetNode).getUserObject();

			if ((draggedObject instanceof Scene) && (targetObject instanceof Chapter)) {
				dropSceneInChapter((Scene) draggedObject, (Chapter) targetObject);
			} else if ((draggedObject instanceof Scene) && (targetObject instanceof Scene)) {
				dropSceneBeforeScene((Scene) draggedObject, (Scene) targetObject);
			} else if ((draggedObject instanceof Chapter) && (targetObject instanceof Part)) {
				dropChapterInPart((Chapter) draggedObject, (Part) targetObject);
			} else if ((draggedObject instanceof Chapter) && (targetObject instanceof Chapter)) {
				dropChapterBeforeChapter((Chapter) draggedObject, (Chapter) targetObject);
			}
			return (true);
		}
		return (false);
	}

	private void dropChapterBeforeChapter(Chapter dragged, Chapter target) {
		MainFrame frame = (MainFrame) SwingUtilities.getWindowAncestor(getTree());
		List<Chapter> toUpdate = new ArrayList<>();
		List<Chapter> chapters = frame.book.chapters;
		int refChapterNumber = target.number;
		for (Chapter chapter : chapters) {
			int chapterNum = chapter.number;
			if (!chapter.equals(dragged))
			{
				if (chapterNum >= refChapterNumber) {
					chapter.number=chapterNum + 1;
					toUpdate.add(chapter);
				}
			}
		}
		Part targetPart=target.part;
		dragged.part=targetPart;
		dragged.number=refChapterNumber;

		Ctrl ctrl = frame.getBookController();
		ctrl.updateEntity(dragged);
		for (Chapter chapter : toUpdate) {
			ctrl.updateEntity(chapter);
		}
		ctrl.updateEntity(target.part);

		int newNum = 1;
		List<Chapter> chapters1 = Chapter.find(frame.book.chapters,targetPart);
		for (Chapter chapter : chapters1) {
			chapter.number=newNum++;
		}
		
		for (Chapter chapter : chapters1) {
			ctrl.updateEntity(chapter);
		}
		ctrl.updateEntity(targetPart);
		getTreePanel().refreshAll();
	}

	private void dropChapterInPart(Chapter dragged, Part target) {
		MainFrame frame = (MainFrame) SwingUtilities.getWindowAncestor(getTree());
		List<Chapter> chapters = Chapter.find(frame.book.chapters,target);
		int lastChapterNumber = 0;
		for (Chapter chapter : chapters) {
			int chapterNum = chapter.number;
			if (chapterNum > lastChapterNumber) {
				lastChapterNumber = chapterNum;
			}
		}
		dragged.part=target;
		dragged.number=lastChapterNumber + 1;

		Ctrl ctrl = frame.getBookController();
		ctrl.updateEntity(dragged);
		ctrl.updateEntity(target);
		getTreePanel().refreshAll();
	}

	private void dropSceneBeforeScene(Scene dragged, Scene target) {
		MainFrame frame = (MainFrame) SwingUtilities.getWindowAncestor(getTree());
		List<Scene> toUpdate = new ArrayList<>();
		Chapter targetChapter=target.chapter;
		List<Scene> scenes = Scene.find(frame.book.scenes,targetChapter);
		int refSceneNumber = target.number;
		scenes.forEach((scene)-> {
			int sceneNum = scene.number;
			if (!scene.equals(dragged)) {
				if (sceneNum >= refSceneNumber) {
					scene.number=(sceneNum + 1);
					toUpdate.add(scene);
				}
			}
		});
		dragged.chapter=(target.chapter);
		dragged.number=(refSceneNumber);

		Ctrl ctrl = frame.getBookController();
		ctrl.updateEntity(dragged);
		toUpdate.forEach((scene)-> {
			ctrl.updateEntity(scene);
		});
		ctrl.updateEntity(targetChapter);

		int newNum = 1;
		List<Scene> scenes1 = Scene.find(frame.book.scenes,targetChapter);
		for (Scene scene : scenes1) {
			scene.number=(newNum++);
		}
		
		scenes1.forEach((scene)-> {
			ctrl.updateEntity(scene);
		});
		ctrl.updateEntity(targetChapter);
		getTreePanel().refreshAll();
	}

	private void dropSceneInChapter(Scene dragged, Chapter target) {
		MainFrame frame = (MainFrame) SwingUtilities.getWindowAncestor(getTree());
		List<Scene> scenes = Scene.find(frame.book.scenes,target);
		int lastSceneNumber = 0;
		for (Scene scene : scenes) {
			int sceneNum = scene.number;
			if (sceneNum > lastSceneNumber) {
				lastSceneNumber = sceneNum;
			}
		}
		if (target.number == -1){
		   dragged.chapter=null;
		} else {
		  dragged.chapter=target;
	    }
		dragged.number=(lastSceneNumber + 1);

		Ctrl ctrl = frame.getBookController();
		ctrl.updateEntity(dragged);
		if (target.number != -1){
			ctrl.updateEntity(target);
	    }
		getTreePanel().refreshAll();
	}

}