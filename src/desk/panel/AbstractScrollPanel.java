package desk.panel;

import desk.app.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import static java.awt.event.KeyEvent.*;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import static javax.swing.KeyStroke.getKeyStroke;

@SuppressWarnings("serial")
public abstract class AbstractScrollPanel extends AbstractPanel implements MouseWheelListener {
	private final static int KB_ALT=InputEvent.ALT_DOWN_MASK;
	private final static int KB_CTRL=InputEvent.CTRL_DOWN_MASK;

	protected JPanel panel;
	protected JScrollPane scroller;

	public AbstractScrollPanel(MainFrame mainFrame) {
		super(mainFrame);
	}

	@SuppressWarnings("deprecation")
	protected void registerKeyboardAction() {
		int when = WHEN_IN_FOCUSED_WINDOW;
		registerKeyboardAction(new ScrollRight(), getKeyStroke(VK_RIGHT, KB_ALT), when);
		registerKeyboardAction(new ScrollLeft(), getKeyStroke(VK_LEFT, KB_ALT), when);
		registerKeyboardAction(new ScrollUp(), getKeyStroke(VK_UP, KB_ALT), when);
		registerKeyboardAction(new ScrollDown(), getKeyStroke(VK_DOWN, KB_ALT), when);
		registerKeyboardAction(new ZoomIn(), getKeyStroke(VK_ADD, KB_ALT), when);
		registerKeyboardAction(new ZoomOut(), getKeyStroke(VK_SUBTRACT, KB_CTRL), when);
	}

	protected void scrollHorizontal(int amount, int rotation) {
		JScrollBar sb = scroller.getHorizontalScrollBar();
		int val = sb.getValue();
		sb.setValue(val + amount * rotation * sb.getUnitIncrement());
	}

	protected void scrollVertical(int amount, int rotation) {
		JScrollBar sb = scroller.getVerticalScrollBar();
		int val = sb.getValue();
		sb.setValue(val + amount * rotation * sb.getUnitIncrement());
	}

	@Override
	@SuppressWarnings("deprecation")
	public void mouseWheelMoved(MouseWheelEvent e) {
		if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
			int modifiers = e.getModifiersEx();
			if ((modifiers & InputEvent.CTRL_DOWN_MASK) == InputEvent.CTRL_DOWN_MASK) {
				scrollHorizontal(e.getScrollAmount(), e.getWheelRotation());
				return;
			}
			scrollVertical(e.getScrollAmount(), e.getWheelRotation());
		}
	}

	private class ScrollRight extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			scrollHorizontal(6, 1);
		}
	}

	private class ScrollLeft extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			scrollHorizontal(6, -1);
		}
	}

	private class ScrollUp extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			scrollVertical(6, -1);
		}
	}

	private class ScrollDown extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			scrollVertical(6, 1);
		}
	}

	public int zoom, zoom_min = 1, zoom_max = 30;

	public int getZoomValue() {
		return (zoom);
	}

	public void setZoomValue(int val) {
		zoom = val;
	}

	public void setZoomValues(int vzoom, int minzoom, int maxzoom) {
		zoom = vzoom;
		zoom_min = minzoom;
		zoom_max = maxzoom;
	}

	public int getMaxZoomValue() {
		return (zoom_max);
	}

	public int getMinZoomValue() {
		return (zoom_min);
	}

	private class ZoomIn extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			int val = getZoomValue();
			val += 2;
			if (val > getMaxZoomValue()) {
				val = getMaxZoomValue();
			}
			setZoomValue(val);
		}
	}

	private class ZoomOut extends AbstractAction {

		@Override
		public void actionPerformed(ActionEvent e) {
			int val = getZoomValue();
			val -= 2;
			if (val < getMinZoomValue()) {
				val = getMinZoomValue();
			}
			setZoomValue(val);
		}
	}
}
