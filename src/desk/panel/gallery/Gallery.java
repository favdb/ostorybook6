/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.gallery;

import desk.app.pref.BookPrefGallery;
import db.entity.Photo;
import desk.app.App;
import desk.app.MainFrame;
import desk.dialog.ConfirmDeleteDlg;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class Gallery extends AbstractPanel implements ActionListener, ChangeListener {

	private JButton btPlus;
	private JButton btMinus;

	public enum ACTION {
		BT_DELETE("btDelete"),
		BT_EDIT("btEdit"),
		BT_NEW("btNew"),
		BT_PLUS("btPlus"),
		BT_MINUS("btMinus"),
		CB_NB("view.gallery.zoom"),
		CB_DIRECTION("view.gallery.direction"),
		PRINT("Gallery_Print"),
		REFRESH("Refresh"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private JButton btNew;
	private JButton btDelete;
	private JButton btEdit;
	private JPanel photoPanel;
	private Photo current;
	private boolean onInitUi=false;
	private final static int ZOOM_MIN=1,ZOOM_MAX=16;
	private BookPrefGallery param;
	private JCheckBox cbDirection;
	
	public Gallery(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
		param=mainFrame.bookPref.Gallery;
	}

	@Override
	public void initUi() {
		onInitUi=true;
		if (this.getComponentCount()>0) {
			this.removeAll();
		}
		// compute number of photos in one line
		setLayout(new MigLayout("flowy,fill,ins 0"));
		add(initToolbar(!WITH_PART), "wrap, span,growx");
		if (cbListPart!=null) cbListPart.setVisible(false);
		toolbar.add(new JLabel(I18N.getColonMsg("view.gallery.zoom")));
		btMinus=initButton("btMinus","","small/minus");
		toolbar.add(btMinus);
		btPlus=initButton("btPlus","","small/plus");
		toolbar.add(btPlus);
		cbDirection=this.initCheckBox("view.gallery.direction", param.direction);
		toolbar.add(cbDirection);
		add(toolbar);
		
		photoPanel=new JPanel(new MigLayout());
		JScrollPane scroller = new JScrollPane(photoPanel);
		scroller.setPreferredSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
				
		btNew=this.initButton("btNew", "", "small/new");
		btEdit=this.initButton("btEdit", "", "small/edit");
		btDelete=this.initButton("btDelete", "", "small/delete");
		
		add(scroller, "grow");
		JPanel p=new JPanel(new MigLayout());
		p.add(btNew, "span,split 3");
		p.add(btEdit);
		p.add(btDelete);
		add(p);
		refresh();
		onInitUi=false;
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		//App.trace("Gallery.stateChanged(" + e.toString() + ")");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		App.trace("Gallery.actionPerformed(" + evt.toString() + ")");
		if (evt.getSource() instanceof JComboBox) {
			if (onInitUi) return;
			App.trace("zoom action");
			refresh();
		}
		if (evt.getSource() instanceof JCheckBox) {
			if (onInitUi) return;
			App.trace("direction action");
			refresh();
		}
		if (evt.getSource() instanceof JButton) {
			String btn = ((JButton) evt.getSource()).getName();
			switch (getAction(btn)) {
				case BT_EDIT: {
					if (mainFrame.showEditorAsDialog(current)) return;
					refresh();
					return;
				}
				case BT_NEW: {
					if (mainFrame.showEditorAsDialog(new Photo())) return;
					refresh();
					return;
				}
				case BT_DELETE: {
					if (current==null) return;
					boolean act = ConfirmDeleteDlg.show(mainFrame, current, null);
					if (act) {
						mainFrame.getBookController().deleteEntity(current);
						current=null;
						refresh();
					}
					return;
				}
				case BT_PLUS: {
					int z=param.zoom;
					if (param.zoom<ZOOM_MAX) {
						param.setZoom(--z);
						refresh();
					}
					return;
				}
				case BT_MINUS: {
					int z=param.zoom;
					if (param.zoom>ZOOM_MIN) {
						param.setZoom(++z);
						refresh();
					}
					break;
				}
			}
		}
	}
	
	@Override
	public void refresh() {
		App.trace("Gallery.refresh()");
		int defNb=param.getZoom();
		/*if (!slider.getValueIsAdjusting()) {
			defNb = slider.getValue();
			if (defNb!=param.getZoom()) {
				param.setZoom(defNb);
			}
		}*/
		String cx="[]";
		if (defNb>1) for (int i=1;i<defNb;i++) {
			cx+="5[]";
		}
		Double theHeight=((SwingUtil.getScreenSize().height*0.70)/defNb);
		Integer height = theHeight.intValue();
		photoPanel.removeAll();
		photoPanel.setLayout(new MigLayout("ins 0"+(cbDirection.isSelected()?"":", wrap")));
		param.setDirection(cbDirection.isSelected());
		int i=0;
		mainFrame.book.photos.forEach((photo)-> {
			JPanel pp=new JPanel(new MigLayout("wrap"));
			pp.setName(photo.name);
			pp.setBorder(BorderFactory.createEtchedBorder());
			JLabel p=new JLabel(photo.getImage(mainFrame.getImageDir(), height));
			p.setName(photo.name);
			String str="<html><body><p><b>"+photo.name+"</b><br>"+photo.desc+"</p></body></html>";
			p.setToolTipText(str);
			pp.add(p);
			JLabel l=new JLabel(photo.name);
			pp.add(l, "center");
			photoPanel.add(pp);
		});
		revalidate();
		repaint();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Gallery.modelPropertyChange(evt="+evt.toString()+")");
		if (evt.getSource() instanceof JSlider) {
			if (onInitUi) return;
			refresh();
			return;
		}
		String propName=evt.getPropertyName();
		switch(getAction(propName)) {
			case REFRESH:
				refresh();
				return;
		}
		switch(propName) {
			case "photo_Update":
			case "photo_New":
			case "photo_Edit":
				current=(Photo)evt.getNewValue();
				refresh();
				break;
			case "photo_Delete":
				current=null;
				refresh();
				break;
		}

	}
	
}
