/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.panel.planning;

import db.entity.Book;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.panel.AbstractPanel;
import desk.panel.tree.TreeJTree;
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.Map;
import javax.swing.JScrollPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author favdb
 */
public class PlanningTree extends AbstractPanel {

	public TreeJTree tree;
	private DefaultMutableTreeNode topNode;
	private PlanningElement topSp;
	
	public PlanningTree(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		topSp = new PlanningElement();
		topSp.setElement(mainFrame.book.getTitle());
		topNode = new DefaultMutableTreeNode(topSp);

		tree = new TreeJTree(topNode, TreeSelectionModel.SINGLE_TREE_SELECTION, new PlanningTCR());
		JScrollPane scroller = new JScrollPane(tree);

		refresh();
		tree.expandRow(0);
		add(scroller);
	}
	
	@Override
	public void refresh() {
		App.trace("PlanningTree.refresh()");
		topNode.removeAllChildren();
		Map<Object, Integer> sizes = book.getElementsSize();
		// get elements
		List<Part> parts = Part.findRoots(book.parts);
		int topsize = 0;
		int topmaxsize = 0;
		for (Part part : parts) {
			createNodePart(topNode, part, sizes);
			topmaxsize += part.size;
			topsize += sizes.get(part);
		}
		if (topmaxsize == 0) {
			topSp.setSize(100);
			topSp.setMaxSize(100);
		} else {
			topSp.setSize(topsize);
			topSp.setMaxSize(topmaxsize);
		}
	}

	private void createNodePart(DefaultMutableTreeNode father, Part part, Map<Object, Integer> sizes) {
		App.trace("PlanningTree.createNodePart()");
		PlanningElement sp = new PlanningElement();
		sp.setElement(part);
		sp.setSize(sizes.get(part));
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(sp);
		father.add(node);

		List<Part> subparts = Part.getParts(book.parts, part);
		List<Chapter> chapters = Chapter.find(book.chapters, part);

		subparts.forEach((subpart) -> {
			createNodePart(node, subpart, sizes);
		});
		chapters.forEach((chapter) -> {
			createNodeChapter(node, chapter, sizes);
		});

		// align objective chars value with the one of contained elements
		int subObjective = 0;
		subObjective = subparts.stream().map((subpart)
				-> subpart.size).reduce(subObjective, Integer::sum);
		subObjective = chapters.stream().map((chapter) -> chapter.size).reduce(subObjective, Integer::sum);
		if (subObjective > part.size) {
			part.size=(subObjective);
		}
	}

	private void createNodeChapter(DefaultMutableTreeNode father, Chapter chapter, Map<Object, Integer> sizes) {
		App.trace("PlanningTree.createNodeChapter()");
		PlanningElement sp = new PlanningElement();
		sp.setElement(chapter);
		sp.setSize(sizes.get(chapter));
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(sp);
		father.add(node);

		List<Scene> scenes = Scene.find(book.scenes, chapter);

		scenes.forEach((scene) -> {
			PlanningElement ssp = new PlanningElement();
			ssp.setElement(scene);
			Integer xzx = sizes.get(scene);
			if (xzx != null) {
				ssp.setSize(xzx);
				DefaultMutableTreeNode subnode = new DefaultMutableTreeNode(ssp);
				node.add(subnode);
			}
		});

	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		App.trace("Planning.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		if (propName.contains("_")) {
			String type[]=propName.split("_");
			switch(Book.getTYPE(type[0])) {
				case SCENE:
				case CHAPTER:
				case PART:
					refresh();
					return;
			}
		}
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				refresh();
		}
	}
	
}
