package desk.panel.planning;

import db.entity.AbstractEntity;
import desk.app.App;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.panel.AbstractPanel;
import desk.tools.EntityUtil;
import db.I18N;
import db.entity.Book;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import lib.miginfocom.swing.MigLayout;

/**
 * Panel for planfication vision.
 *
 * @author Jean
 *
 */
public class Planning extends AbstractPanel implements ActionListener, MouseListener {

	public enum ACTION {
		REFRESH("Planning_Refresh"),
		SHOWINFO("Planning_ShowInfo"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getPlanningAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private JTabbedPane tabbed;
	private PlanningTree planningTree;
	private PlanningGlobal planningGlobal;
	private PlanningTimeline planningTimeline;

	/**
	 * Constructor.
	 *
	 * @param mainframe to include panel in.
	 */
	public Planning(MainFrame mainframe) {
		super(mainframe);
		initAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see storybook.ui.panel.AbstractPanel#init()
	 */
	@Override
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see storybook.ui.panel.AbstractPanel#initUi()
	 */
	@Override
	public void initUi() {
		App.trace("Planning.initUi()");
		planningGlobal=new PlanningGlobal(mainFrame);
		planningTimeline=new PlanningTimeline(mainFrame);
		planningTree=new PlanningTree(mainFrame);
		tabbed = new JTabbedPane();
		tabbed.setTabPlacement(JTabbedPane.BOTTOM);
		setLayout(new MigLayout("fill", "[grow]", "[grow]"));

		// add all subpanels to tabbed pane
		tabbed.addTab(I18N.getMsg("planning.global"), planningGlobal);
		tabbed.add(I18N.getMsg("planning.timeline"), planningTimeline);
		tabbed.addTab(I18N.getMsg("planning.tree"), planningTree);

		// add tabbed pane to global panel
		add(tabbed, "pos 0% 0% 100% 100%");
	}
	
	@Override
	public void refresh() {
		planningGlobal.refresh();
		planningTimeline.refresh();
		planningTree.refresh();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		App.trace("Planning.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		boolean b=false;
		if (propName.contains("_")) {
			String type[]=propName.split("_");
			switch(Book.getTYPE(type[0])) {
				case SCENE:
				case CHAPTER:
				case PART:
					b=true;
					break;
			}
		}
		else switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				b=true;
		}
		if (b) refresh();
	}

	private void showPopupMenu(JTree tree, MouseEvent evt) {
		App.trace("Planning.showPopupMenu(JTree tree, MouseEvent evt)");
		TreePath selectedPath = tree.getPathForLocation(evt.getX(), evt.getY());
		DefaultMutableTreeNode selectedNode = null;
		try {
			selectedNode = (DefaultMutableTreeNode) selectedPath.getLastPathComponent();
		} catch (Exception e) {
			// ignore
		}
		if (selectedNode == null) {
			return;
		}
		Object eltObj = selectedNode.getUserObject();
		if (!(eltObj instanceof PlanningElement)) {
			return;
		}
		JPopupMenu menu = null;
		Object userObj = ((PlanningElement) eltObj).getElement();
		if (userObj instanceof AbstractEntity) {
			AbstractEntity entity = (AbstractEntity) userObj;
			menu = EntityUtil.createPopupMenu(mainFrame, entity);
		}
		if (menu == null) {
			return;
		}
		tree.setSelectionPath(selectedPath);
		JComponent comp = (JComponent) tree.getComponentAt(evt.getPoint());
		Point p = SwingUtilities.convertPoint(comp, evt.getPoint(), this);
		menu.show(this, p.x, p.y);
		evt.consume();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		App.trace("Planning.mouseReleased(MouseEvent e)");
		if (e.isPopupTrigger()) {
			showPopupMenu(planningTree.tree, e);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}
}
