/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.panel.planning;

import db.entity.Book;
import db.entity.Scene;
import db.entity.Status;
import desk.app.App;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.panel.AbstractPanel;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PlanningGlobal extends AbstractPanel {
	CircleProgressBar[] progress = new CircleProgressBar[Status.STATUS.values().length];
	
	public PlanningGlobal(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("wrap 5,fill", "[center, sizegroup]", "[][grow]"));
		for (int i = 0; i < Status.STATUS.values().length; i++) {
			progress[i] = new CircleProgressBar(0, 100);
			progress[i].setPreferredSize(new Dimension(100, 100));
			progress[i].setBorderPainted(false);
			add(progress[i], "split 2, flowy");
			add(new JLabel(Status.getMsg(i), SwingConstants.CENTER));
		}
		refresh();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		App.trace("Planning.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		if (propName.contains("_")) {
			String type[]=propName.split("_");
			switch(Book.getTYPE(type[0])) {
				case SCENE:
				case CHAPTER:
				case PART:
					refresh();
					return;
			}
		}
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				refresh();
		}
	}
	
	@Override
	public void refresh() {
		App.trace("PlanningGlobal.refresh()");
		int allScenes = book.scenes.size();
		int[] nbScenesByState = new int[6];
		for (int i = 0; i < 6; i++) {
			nbScenesByState[i] = Scene.findByState(book.scenes, i).size();
		}
		for (int i = 0; i < 5; i++) {
			progress[i].setValue((nbScenesByState[i + 1] * 100) / ((allScenes == 0) ? 1 : allScenes));
		}
	}
	
}
