/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.panel.planning;

import db.entity.Book;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.SbDate;
import desk.app.App;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.panel.AbstractPanel;
import desk.panel.planning.timeline.Dataset;
import desk.panel.planning.timeline.DatasetItem;
import desk.panel.planning.timeline.Timeline;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import lib.miginfocom.swing.MigLayout;
import tools.ColorUtil;

/**
 *
 * @author favdb
 */
public class PlanningTimeline extends AbstractPanel {
	private Timeline timeline;
	private Dataset dataset;
	
	public PlanningTimeline(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		App.trace("PlanningTimeline.initUi()");
		JPanel timePanel = new JPanel();
		timePanel.setLayout(new MigLayout("wrap 5,fill", "[center]", "[][grow]"));
		// add chart
		timeline = createTimeline();
		timePanel.add(timeline, "wrap, grow");
		JScrollPane scroller = new JScrollPane(timePanel);
		add(scroller, "grow");
	}

	private Timeline createTimeline() {
		App.trace("PlanningTimeline.createTimeline()");
		dataset = new Dataset(mainFrame);
		dataset.items = new ArrayList<>();
		List<Part> parts = Part.findRoots(book.parts);
		Color[] color = ColorUtil.getNiceColors();
		int nc = 0;
		for (Part part : parts) {
			dataset.items.add(createPartItem(part, color[nc++]));
			if (nc >= color.length) {
				nc = 0;
			}
		}
		dataset.listId = new ArrayList<>();

		Timeline localTimeline = new Timeline("date", dataset);
		return (localTimeline);
	}

	@Override
	public void refresh() {
		App.trace("PlanningTimeline.refresh()");
		for (int i = 0; i < dataset.items.size(); i++) {
			dataset.items.remove(0);
		}
		List<Part> parts = Part.findRoots(book.parts);
		Color[] color = ColorUtil.getNiceColors();
		int nc = 0;
		for (Part part : parts) {
			dataset.items.add(createPartItem(part, color[nc++]));
			if (nc >= color.length) {
				nc = 0;
			}
		}
		dataset.listId = new ArrayList<>();
		timeline.redraw();
	}

	private DatasetItem createPartItem(Part part, Color color) {
		App.trace("PlanningTimeline.createPartItem(Part part, Color color)");
		SbDate ddj = SbDate.getToDay();
		SbDate debut;
		debut = part.creation;
		SbDate fin;
		int value = 0;
		if (part.hasDone()) {
			value = 2;
			fin = part.done;
		} else {
			fin = ddj;
			if (part.hasObjective()) {
				if (part.objective.after(ddj)) {
					value = 1;
				}
			} else {
				value = 2;
			}
		}
		Color[] colors = {ColorUtil.getNiceRed(), ColorUtil.getNiceBlue(), ColorUtil.getDarkGreen()};
		DatasetItem item = new DatasetItem(part.name, debut, fin, colors[value]);
		List<DatasetItem> subItems = createChapterItems(part);
		item.setSubItem(subItems);
		return (item);
	}

	private List<DatasetItem> createChapterItems(Part part) {
		App.trace("PlanningTimeline.createChapterItems(Part part)");
		List<DatasetItem> items = new ArrayList<>();
		List<Chapter> chapters = Chapter.find(book.chapters, part);
		Color[] colors = {ColorUtil.getNiceRed(), ColorUtil.getNiceBlue(), ColorUtil.getNiceGreen()};
		SbDate ddj = SbDate.getToDay();
		chapters.forEach((chapter) -> {
			SbDate debut;
			debut = chapter.creation;
			SbDate fin;
			int value = 0;
			if (chapter.hasDone()) {
				fin = chapter.done;
				value = 2;
			} else {
				fin = ddj;
				if (chapter.hasObjective()) {
					if (chapter.objective.after(ddj)) {
						value = 1;
					}
				} else {
					value = 2;
				}
			}
			DatasetItem item = new DatasetItem(chapter.name, debut, fin, colors[value]);
			items.add(item);
		});

		return (items);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		App.trace("PlanningTimeline.modelPropertyChange(evt="+evt.toString()+")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		if (propName.contains("_")) {
			String type[]=propName.split("_");
			switch(Book.getTYPE(type[0])) {
				case SCENE:
				case CHAPTER:
				case PART:
					refresh();
					return;
			}
		}
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				refresh();
		}
	}
	
}
