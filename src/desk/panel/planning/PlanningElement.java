package desk.panel.planning;

import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import tools.html.HtmlUtil;

public class PlanningElement {
	Object element;
	int size;
	int maxSize;
	//int words;
	
	public Object getElement() {
		return element;
	}
	/**
	 * @param element
	 */
	public void setElement(Object element) {
		this.element = element;
		if (element instanceof Part) {
			Part part = (Part)element;
			maxSize = part.size;
		} else if (element instanceof Chapter) {
			Chapter chapter = (Chapter)element;
			maxSize = chapter.size;
		}
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getMaxSize() {
		return maxSize;
	}
	public void setMaxSize(int size) {
		this.maxSize = size;
	}
	
	// TODO ajouter le nombre de mots
	/*
	public int getWords() {
		return words;
	}
	public void setWords(int size) {
	this.words=size;
	*/
	@Override
	public String toString() {
		String t="";
		String notes="";
		if (element instanceof Part) {
			Part part=(Part)element;
			if (part.getNotes()!=null && !HtmlUtil.htmlToText(part.getNotes()).equals("")) notes="*";
			t=part.name + notes + "    (" + size + "/" + maxSize +")";
			maxSize = part.size;
		} else if (element instanceof Chapter) {
			Chapter chapter=(Chapter)element;
			if (chapter.getNotes()!=null && !HtmlUtil.htmlToText(chapter.getNotes()).equals("")) notes="*";
			maxSize = chapter.size;
			t=chapter.name + notes + "    (" + size + "/" + maxSize +")";
		} else if (element instanceof Scene) {
			Scene scene=((Scene)element);
			if (scene.getNotes()!=null && !HtmlUtil.htmlToText(scene.getNotes()).equals("")) notes="*";
			t= scene.name + notes + "    (" + size + ")";
		} else if (element instanceof String) {
			t= (String)element + "    (" + size + "/" + maxSize +")";
		}
		return(t);
	}
}
