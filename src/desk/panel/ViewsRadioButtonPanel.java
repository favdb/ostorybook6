/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel;

import java.beans.PropertyChangeEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import db.I18N;
import desk.app.MainFrame;

import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class ViewsRadioButtonPanel extends AbstractPanel {

	private JRadioButton rbChrono;
	private JRadioButton rbBook;
	private JRadioButton rbManage;
	private boolean showManage=false;

	public ViewsRadioButtonPanel(MainFrame mainFrame) {
		this(mainFrame, true);
		initAll();
	}

	public ViewsRadioButtonPanel(MainFrame mainFrame, boolean showManage) {
		super(mainFrame);
		this.showManage = showManage;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("ins 0,wrap 2", "[]", "[]"));

		JLabel lbChronoIcon = new JLabel(IconUtil.getIcon("small/chrono"));
		rbChrono = new JRadioButton();
		rbChrono.setText(I18N.getMsg("view.chrono"));
		rbChrono.setSelected(true);
		JLabel lbBookIcon = new JLabel(IconUtil.getIcon("small/work"));
		rbBook = new JRadioButton();
		rbBook.setText(I18N.getMsg("view.book"));
		JLabel lbManageIcon = new JLabel(IconUtil.getIcon("small/manage"));
		if (showManage) {
			rbManage = new JRadioButton();
			rbManage.setText(I18N.getMsg("view.manage"));
		}
		ButtonGroup btGroup = new ButtonGroup();
		btGroup.add(rbChrono);
		btGroup.add(rbBook);
		if (showManage) {
			btGroup.add(rbManage);
		}

		// layout
		add(lbChronoIcon);
		add(rbChrono);
		add(lbBookIcon, "");
		add(rbBook, "");
		if (showManage) {
			add(lbManageIcon, "");
			add(rbManage, "");
		}
	}

	public boolean isChronoSelected() {
		return rbChrono.isSelected();
	}

	public boolean isBookSelected() {
		return rbBook.isSelected();
	}

	public boolean isManageSelected() {
		return rbManage.isSelected();
	}
}
