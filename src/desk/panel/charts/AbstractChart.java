/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.panel.charts;

import db.entity.Book.TYPE;
import db.entity.Category;
import desk.panel.AbstractPanel;
import desk.app.MainFrame;
import desk.tools.CheckBoxUtil;
import desk.model.Ctrl;
import desk.model.Ctrl.ACTION;
import desk.tools.FileFilter;
import desk.tools.IOTools;
import desk.tools.swing.PrintUtil;
import desk.tools.swing.ScreenImage;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

public abstract class AbstractChart extends AbstractPanel implements ActionListener {

	protected JPanel panel;
	protected JPanel optionsPanel;
	protected List<JCheckBox> categoryCbList;
	protected List<Category> selectedCategories;
	protected String chartTitle;
	protected boolean partRelated = false;
	protected boolean needsFullRefresh = false;
	private JScrollPane scroller;
	private AbstractAction exportAction;

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public AbstractChart(MainFrame mainFrame, String param) {
		super(mainFrame);
		this.chartTitle = I18N.getMsg(param);
	}

	protected void initChart() {
		this.categoryCbList = CheckBoxUtil.createCategoryCheckBoxes(this.mainFrame, this);
		this.selectedCategories = new ArrayList<>();
		updateSelectedCategories();
	}

	protected abstract void initChartUi();

	protected void initOptionsUi() {
		JLabel lb = new JLabel(I18N.getColonMsg("categories"));
		this.optionsPanel.add(lb, "split " + this.categoryCbList.size() + 1);
		categoryCbList.forEach((jcb)-> {
			this.optionsPanel.add(jcb);
		});
	}

	public List<Category> getSelectedCategories() {
		return (selectedCategories);
	}

	public void updateSelectedCategories() {
		this.selectedCategories.clear();
		categoryCbList.forEach((jcb)-> {
			if (jcb.isSelected()) {
				Category cat = (Category) jcb.getClientProperty("CbCategory");
				this.selectedCategories.add(cat);
			}
		});
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		Object object = evt.getNewValue();
		String propName = evt.getPropertyName();
		View view1= (View) object;
		View view2= (View) getParent().getParent();
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH:
				if (view2 == view1) {
					refresh();
				}
				return;
			case EXPORT:
				if (view2 == view1) {
					getExportAction().actionPerformed(null);
				}
				return;
			case PRINT:
				if (view2 == view1) {
					PrintUtil.printComponent(this);
				}
				return;
		}
		if ((this.partRelated) &&
				(Ctrl.getActionEntity(TYPE.PART, ACTION.CHANGE).equals(propName))) {
			if (this.needsFullRefresh) {
				refresh();
			} else {
				refreshChart();
			}
		}
	}

	@Override
	public void init() {
		try {
			initChart();
		} catch (Exception e) {
			System.err.println("AbstractChartPanel.init() Exception" + e.getLocalizedMessage());
		}
	}

	@Override
	public void initUi() {
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("flowy,fill,ins 0", "", ""));
		this.panel = new JPanel(new MigLayout("flowy,fill,ins 2", "", "[][grow][]"));
		this.panel.setBackground(Color.white);
		initChartUi();
		this.optionsPanel = new JPanel(new MigLayout("flowx,fill"));
		this.optionsPanel.setBackground(Color.white);
		this.optionsPanel.setBorder(SwingUtil.getBorderGray());
		initOptionsUi();
		this.scroller = new JScrollPane(this.panel);
		SwingUtil.setMaxPreferredSize(this.scroller);
		add(this.scroller, "grow");
		if (this.optionsPanel.getComponentCount() > 0) {
			add(this.optionsPanel, "growx");
		} else {
			add(new JLabel());
		}
	}

	protected void refreshChart() {
		this.panel.removeAll();
		initChartUi();
		this.panel.revalidate();
		this.panel.repaint();
	}

	private AbstractChart getThis() {
		return this;
	}

	private AbstractAction getExportAction() {
		if (this.exportAction == null) {
			this.exportAction = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent paramAnonymousActionEvent) {
					try {
						String dir = mainFrame.bookPref.Export.getDirectory();
						File file1 = new File(dir);
						JFileChooser fileChooser = new JFileChooser(file1);
						FileFilter filter = new FileFilter(FileFilter.PNG, I18N.getMsg("file.type.png"));
						fileChooser.setFileFilter(filter);
						fileChooser.setApproveButtonText(I18N.getMsg("export"));
						String str = AbstractChart.this.mainFrame.getXml().name + " - " + AbstractChart.this.chartTitle;
						str = IOTools.cleanupFilename(str);
						fileChooser.setSelectedFile(new File(str));
						int i = fileChooser.showDialog(AbstractChart.this.getThis(), I18N.getMsg("export"));
						if (i == 1) {
							return;
						}
						File file2 = fileChooser.getSelectedFile();
						if (!file2.getName().endsWith(".png")) {
							file2 = new File(file2.getPath() + ".png");
						}
						ScreenImage.createImage(AbstractChart.this.panel, file2.toString());
						JOptionPane.showMessageDialog(AbstractChart.this.getThis(),
								I18N.getMsg("export.success"),
								I18N.getMsg("export"), 1);
					} catch (HeadlessException | IOException localException) {
					}
				}
			};
		}
		return this.exportAction;
	}

	@Override
	public void actionPerformed(ActionEvent paramActionEvent) {
		updateSelectedCategories();
		refreshChart();
	}

}
