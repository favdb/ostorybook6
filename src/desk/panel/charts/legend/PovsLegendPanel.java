/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.panel.charts.legend;

import java.awt.Dimension;
import java.util.Iterator;
import javax.swing.JLabel;
import desk.model.BookModel;
import db.entity.Pov;
import db.I18N;
import desk.app.MainFrame;
import desk.tools.swing.label.CleverLabel;
import tools.ColorUtil;

public class PovsLegendPanel extends AbstractLegendPanel {

	public PovsLegendPanel(MainFrame paramMainFrame) {
		super(paramMainFrame);
		initAll();
	}

	@Override
	public void initUi() {
		setOpaque(false);
		BookModel localDocumentModel = this.mainFrame.getBookModel();
		add(new JLabel(I18N.getMsg("povs")), "gapright 5");
		for (Pov pov : Pov.sort(mainFrame.book.povs)) {
			CleverLabel localCleverLabel = new CleverLabel(pov.name, 0);
			localCleverLabel.setPreferredSize(new Dimension(100, 20));
			localCleverLabel.setBackground(ColorUtil.darker(pov.getJColor(), 0.05D));
			add(localCleverLabel, "sg");
		}
	}
}
