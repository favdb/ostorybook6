/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.panel.charts.legend;

import java.awt.Dimension;
import java.util.Collection;
import java.util.Set;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;
import db.entity.Person;
import desk.app.MainFrame;
import desk.tools.swing.label.CleverLabel;
import tools.ColorUtil;

public class PersonsLegendPanel extends AbstractLegendPanel {

	private final Collection<Person> collection;

	public PersonsLegendPanel(MainFrame paramMainFrame, Set<Person> paramSet) {
		super(paramMainFrame);
		this.collection = paramSet;
		initAll();
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("wrap 10"));
		setOpaque(false);
		for (Person localPerson : this.collection) {
			CleverLabel localCleverLabel = new CleverLabel(localPerson.abbreviation, 0);
			localCleverLabel.setPreferredSize(new Dimension(50, 18));
			if (localPerson.color != null) {
				localCleverLabel.setBackground(ColorUtil.darker(localPerson.getJColor(), 0.05D));
			} else {
				localCleverLabel.setBackground(ColorUtil.getNiceDarkGray());
			}
			JLabel localJLabel = new JLabel();
			localJLabel.setText(localPerson.getFullName());
			add(localCleverLabel, "sg");
			add(localJLabel, "gap after 10");
		}
	}
}
