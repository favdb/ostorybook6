/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.charts;

import java.util.ArrayList;
import java.util.List;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import db.entity.Pov;
import desk.app.MainFrame;
import desk.panel.planning.timeline.Dataset;
import desk.panel.planning.timeline.DatasetItem;
import desk.panel.planning.timeline.TimelinePanel;

/**
 *
 * @author favdb
 */
public class PovsByDate extends AbstractChart {

	private TimelinePanel timelinePanel;
	private Dataset dataset;

	public PovsByDate(MainFrame mainFrame) {
		super(mainFrame, "chart.povs_by_date");
		this.partRelated = true;
		initAll();
	}

	@Override
	protected void initChartUi() {
		dataset=new Dataset(mainFrame);
		createPovsDate();
		timelinePanel=new TimelinePanel(mainFrame, chartTitle, "date", "povs", dataset);
		this.panel.add(this.timelinePanel, "grow");
	}
	
	@Override
	public List<Category> getSelectedCategories() {
		return selectedCategories;
	}

	private void createPovsDate() {
		dataset=new Dataset(mainFrame);
		dataset.items= new ArrayList<>();
		Part part = this.mainFrame.getCurrentPart();
		List<Pov> povs = book.povs;
		List<Chapter> chapters;
		if (mainFrame.showAllParts) chapters = book.chapters;
		else chapters = Chapter.find(book.chapters,part);
		for (Pov pov : povs) {
			List<DureeScene> dureeScene = new ArrayList<>();
			for (Chapter chapter : chapters) {
				List<Scene> scenes = Scene.find(book.scenes,chapter);
				for (Scene scene : scenes) {
					if (scene.hasDate()) {
						dureeScene.add(new DureeScene(scene.id, scene.date));
					}
				}
			}
			if (!dureeScene.isEmpty()) {
				dureeScene = DureeScene.calculDureeScene(book, dureeScene);
				for (DureeScene d : dureeScene) {
					dataset.items.add(new DatasetItem(pov.name, d.debut, d.fin, pov.getJColor()));
				}
			}
		}
	}

}
