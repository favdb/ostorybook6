/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.charts;

import java.util.ArrayList;
import java.util.List;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Person;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.panel.planning.timeline.Dataset;
import desk.panel.planning.timeline.DatasetItem;
import desk.panel.planning.timeline.TimelinePanel;

/**
 *
 * @author favdb
 */
public class PersonsByDate extends AbstractChart {

	private TimelinePanel timelinePanel;
	private Dataset dataset;

	public PersonsByDate(MainFrame mainFrame) {
		super(mainFrame, "chart.persons_by_date");
		this.partRelated = true;
		initAll();
	}

	@Override
	protected void initChartUi() {
		dataset=new Dataset(mainFrame);
		createPersonsDate();
		timelinePanel=new TimelinePanel(mainFrame, chartTitle, "date", "persons", dataset);
		this.panel.add(this.timelinePanel, "grow");
	}
	
	@Override
	public List<Category> getSelectedCategories() {
		return selectedCategories;
	}

	public void createPersonsDate() {
		dataset=new Dataset(mainFrame);
		dataset.items= new ArrayList<>();
		Part part = this.mainFrame.getCurrentPart();
		List<Person> persons = Person.findByCategories(mainFrame.book.persons,selectedCategories);
		List<Chapter> chapters;
		if (mainFrame.showAllParts) {
			chapters = mainFrame.book.chapters;
		}
		else {
			chapters = Chapter.find(mainFrame.book.chapters,part);
		}
		List<DureeScene> durees=DureeScene.initScenes(mainFrame.book);
		persons.forEach((p)-> {
			chapters.forEach((chapter)-> {
				List<Scene> scenes = Scene.find(mainFrame.book.scenes,chapter);
				scenes.forEach((scene)-> {
					List<Person> lpersons = scene.persons;
					if ((!lpersons.isEmpty()) && (lpersons.contains(p))) {
						if (scene.hasDate()) {
							durees.forEach((d)-> {
								if (d.id==scene.id)
									dataset.items.add(new DatasetItem(p.getAbbr(), d.debut, d.fin, p.getJColor()));
							});
						}
					}
				});
			});
		});
		if (!dataset.items.isEmpty()) {
			
		}
	}

}
