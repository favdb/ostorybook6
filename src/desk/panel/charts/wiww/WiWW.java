package desk.panel.charts.wiww;

import db.entity.Location;
import db.entity.Part;
import db.entity.Person;
import db.entity.SbDate;
import db.entity.Scene;
import db.I18N;
import desk.app.MainFrame;
import desk.panel.charts.AbstractChart;
import desk.panel.charts.legend.PersonsLegendPanel;
import desk.tools.FontUtil;
import desk.tools.swing.FixedColumnScrollPane;
import desk.tools.swing.ReadOnlyTable;
import desk.tools.swing.SwingUtil;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableColumn;
import tools.IconUtil;
import tools.ListUtil;

public class WiWW extends AbstractChart implements ChangeListener {

	private Set<Person> foundCharacters;
	private JTable table;
	private JSlider colSlider;
	private int colWidth = 50;

	public WiWW(MainFrame paramMainFrame) {
		super(paramMainFrame, "report.person.location.time.title");
		this.partRelated = true;
		this.needsFullRefresh = true;
	}

	@Override
	protected void initChart() {
		super.initChart();
		this.foundCharacters = new TreeSet<>();
	}

	@Override
	protected void initChartUi() {
		JLabel localJLabel = new JLabel(this.chartTitle);
		localJLabel.setFont(FontUtil.getBold());
		this.table = createTable();
		FixedColumnScrollPane localFixedColumnScrollPane = new FixedColumnScrollPane(this.table, 2);
		localFixedColumnScrollPane.getRowHeader().setPreferredSize(new Dimension(300, 20));
		this.panel.add(localJLabel, "center");
		this.panel.add(localFixedColumnScrollPane, "grow, h pref-40");
		this.panel.add(new PersonsLegendPanel(this.mainFrame, this.foundCharacters), "gap push");
	}

	@Override
	protected void initOptionsUi() {
		super.initOptionsUi();
		JLabel localJLabel = new JLabel(IconUtil.getIcon("small/size"));
		this.colSlider = SwingUtil.createSafeSlider(0, 5, 300, this.colWidth);
		this.colSlider.setMinorTickSpacing(1);
		this.colSlider.setMajorTickSpacing(2);
		this.colSlider.setSnapToTicks(false);
		this.colSlider.addChangeListener(this);
		this.colSlider.setOpaque(false);
		this.optionsPanel.add(localJLabel, "gap push,right");
		this.optionsPanel.add(this.colSlider);
	}

	@Override
	public void refresh() {
		this.colWidth = this.colSlider.getValue();
		super.refresh();
		this.colSlider.setValue(this.colWidth);
		setTableColumnWidth();
	}

	@SuppressWarnings("unchecked")
	private JTable createTable() {
		Part part = this.mainFrame.getCurrentPart();
		List<Person> persons = Person.findByCategories(book.persons,selectedCategories);
		List<SbDate> dates;
		if (mainFrame.showAllParts) dates = Scene.findDistinctDates(book.scenes);
		else  dates = Scene.findDistinctDates(book,book.scenes, part);
		List<Location> locations = book.locations;
		Object[] arrayOfObject1 = ListUtil.addAll(new Object[]{I18N.getMsg("location"), ""}, dates.toArray());
		this.foundCharacters.clear();
		ArrayList localArrayList = new ArrayList();
		Iterator<Location> locationsIterator = locations.iterator();
		while (locationsIterator.hasNext()) {
			Location location = locationsIterator.next();
			Object[] localObject2 = new Object[arrayOfObject1.length];
			int j = 0;
			localObject2[(j++)] = location.name;
			localObject2[(j++)] = location.getCountryCity();
			int m = 0;
			Iterator<SbDate> datesIterator = dates.iterator();
			while (datesIterator.hasNext()) {
				SbDate localDate = datesIterator.next();
				WiWWContainer localWiWWContainer = new WiWWContainer(this.mainFrame, localDate, location, persons);
				localObject2[j] = localWiWWContainer;
				if (localWiWWContainer.isFound()) {
					this.foundCharacters.addAll(localWiWWContainer.getPersonList());
					m = 1;
				}
				j++;
			}
			if (m != 0) {
				localArrayList.add(localObject2);
			}
		}
		ReadOnlyTable jTable = new ReadOnlyTable((Object[][]) localArrayList.toArray(new Object[0][]), arrayOfObject1);
		for (int k = 2; k < jTable.getColumnCount(); k++) {
			TableColumn localTableColumn = jTable.getColumnModel().getColumn(k);
			localTableColumn.setPreferredWidth(120);
			localTableColumn.setCellRenderer(new WiWWTableCellRenderer());
		}
		jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jTable.getTableHeader().setReorderingAllowed(false);
		return jTable;
	}

	@Override
	public void stateChanged(ChangeEvent paramChangeEvent) {
		setTableColumnWidth();
	}

	private void setTableColumnWidth() {
		this.colWidth = this.colSlider.getValue();
		for (int i = 0; i < this.table.getColumnCount(); i++) {
			TableColumn column = this.table.getColumnModel().getColumn(i);
			column.setPreferredWidth(this.colWidth);
		}
	}
}