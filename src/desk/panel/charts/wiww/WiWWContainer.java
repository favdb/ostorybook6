/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.panel.charts.wiww;

import db.entity.Location;
import db.entity.Person;
import db.entity.SbDate;
import db.entity.Scene;
import desk.app.MainFrame;
import java.util.ArrayList;
import java.util.List;

public class WiWWContainer {

	private final MainFrame mainFrame;
	private final Location location;
	private final List<Person> inPersonList;
	private final List<Person> outPersonList;
	private final SbDate date;
	private boolean found;

	public WiWWContainer(MainFrame mainFrame, SbDate date, Location location, List<Person> persons) {
		this.mainFrame = mainFrame;
		this.location = location;
		this.inPersonList = persons;
		this.date = date.getZeroTime();
		this.outPersonList = new ArrayList<>();
		init();
	}

	private void init() {
		this.inPersonList.forEach((person) -> {
			long l = Scene.countByPersonLocationDate(mainFrame.book.scenes, person, location, date);
			if (l != 0L) {
				outPersonList.add(person);
				found = true;
			}
		});
	}

	public List<Person> getPersonList() {
		return(outPersonList);
	}

	public boolean isFound() {
		return(found);
	}
}
