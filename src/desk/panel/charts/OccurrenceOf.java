/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.charts;

import db.entity.AbstractEntity;
import static db.entity.Book.TYPE;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.tools.CheckBoxUtil;
import desk.model.Ctrl;
import desk.model.Ctrl.ACTION;
import desk.panel.planning.timeline.Dataset;
import desk.panel.planning.timeline.DatasetItem;
import desk.panel.planning.timeline.TimelinePanel;
import desk.tools.FileFilter;
import desk.tools.IOTools;
import desk.tools.swing.PrintUtil;
import desk.tools.swing.ScreenImage;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;
import tools.ColorUtil;

/**
 *
 * @author favdb
 */
public class OccurrenceOf extends AbstractChart {

	private Dataset dataset;
	private TimelinePanel timelinePanel;
	private List<JCheckBox> countryCbList;
	protected List<String> selectedCountries;
	private final VIEWID view;
	private AbstractAction exportAction;
	
	public OccurrenceOf(MainFrame mainFrame, VIEWID view) {
		super(mainFrame, view.name());
		this.view=view;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("OccurrenceOf.modelPropertyChange(evt="+evt.toString()+")");
		Object localObject = evt.getNewValue();
		String propName = evt.getPropertyName();
		View view1 = (View) localObject;
		View view2 = (View) getParent().getParent();
		switch(Ctrl.whichACTION(propName)) {
			case REFRESH:
				if (view2 == view1) {
					refresh();
				}
				return;
			case EXPORT:
				if (view2 == view1) {
					getExportAction().actionPerformed(null);
				}
				return;
			case PRINT:
				if (view2 == view1) {
					PrintUtil.printComponent(this);
				}
				return;
		}
		if ((this.partRelated) && (Ctrl.getActionEntity(TYPE.PART, ACTION.CHANGE).equals(propName))) {
			if (this.needsFullRefresh) {
				refresh();
			} else {
				refreshChart();
			}
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void initChart() {
		switch(view) {
			case CHART_OCCURRENCEOFITEMS:
				this.categoryCbList = CheckBoxUtil.createItemCategoryCheckBoxes(this.mainFrame, this);
				this.selectedCategories = new ArrayList();
				updateSelectedCategories();
				break;
			case CHART_OCCURRENCEOFLOCATIONS:
				this.countryCbList = CheckBoxUtil.createCountryCheckBoxes(this.mainFrame, this);
				this.selectedCountries = new ArrayList();
				updateSelectedCountries();
				break;
			case CHART_OCCURRENCEOFPERSONS:
				createOccurenceOf();
				break;
		}
	}

	@Override
	protected void initOptionsUi() {
		JPanel px = new JPanel(new MigLayout("flowx"));
		px.setOpaque(false);
		JLabel lb=new JLabel();
		switch(view) {
			case CHART_OCCURRENCEOFITEMS:
				lb.setText(I18N.getColonMsg("category"));
				px.add(lb);
				this.categoryCbList.forEach((localJCheckBox) -> {
					px.add(localJCheckBox);
				});
				break;

			case CHART_OCCURRENCEOFLOCATIONS:
				lb.setText(I18N.getColonMsg("location.country"));
				px.add(lb);
				this.countryCbList.forEach((localJCheckBox) -> {
					px.add(localJCheckBox);
				});
				break;

			case CHART_OCCURRENCEOFPERSONS:
				return;
		}
		optionsPanel.add(px);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		switch(view) {
			case CHART_OCCURRENCEOFITEMS:
				updateSelectedCategories();
				break;
			case CHART_OCCURRENCEOFLOCATIONS:
				updateSelectedCountries();
				break;
			case CHART_OCCURRENCEOFPERSONS:
				break;
		}
		refreshChart();
	}

	private void updateSelectedCountries() {
		this.selectedCountries.clear();
		countryCbList.forEach((jcb)-> {
			if (jcb.isSelected()) {
				this.selectedCountries.add(jcb.getText());
			}
		});
		createOccurenceOf();
	}

	public List<String> getSelectedCountries() {
		return (selectedCountries);
	}

	@Override
	protected void initChartUi() {
		dataset=new Dataset(mainFrame);
		createOccurenceOf();
		timelinePanel=new TimelinePanel(mainFrame, chartTitle, "value", " ", dataset);
		this.panel.add(this.timelinePanel, "grow");
	}

	@SuppressWarnings("unchecked")
	private void createOccurenceOf() {
		dataset=new Dataset(mainFrame);
		double d = 0.0D;
		Color[] color = ColorUtil.getNiceColors();
		int ncolor = 0;
		List<?> entities=new ArrayList<>();
		switch(view) {
			case CHART_OCCURRENCEOFITEMS:
				entities = Item.orderByCategory(mainFrame.book.items);
				for (Item item : (List<Item>)entities) {
					if (selectedCategories.contains(item.category)) {
						long l = Scene.countByItem(mainFrame.book.scenes,item);
						dataset.items.add(new DatasetItem(item.getAbbr(), l, color[ncolor]));
						if (dataset.maxValue < l) {
							dataset.maxValue = l;
						}
						dataset.listId.add(item.getAbbr());
						ncolor++;
						if (ncolor >= color.length) {
							ncolor = 0;
						}
						d += l;
					}
				}
				break;
			case CHART_OCCURRENCEOFLOCATIONS:
				entities = Location.findByCountries(mainFrame.book.locations, selectedCountries);
				for (Location location : (List<Location>)entities) {
					long l = Scene.countByLocation(mainFrame.book.scenes, location);
					dataset.items.add(new DatasetItem(location.getAbbr(), l, color[ncolor]));
					if (dataset.maxValue < l) {
						dataset.maxValue = l;
					}
					dataset.listId.add(location.getAbbr());
					ncolor++;
					if (ncolor >= color.length) {
						ncolor = 0;
					}
					d += l;
				}
				break;
			case CHART_OCCURRENCEOFPERSONS:
				List<Person> persons = mainFrame.book.persons;
				for (Person person : persons) {
					long l = Scene.countByPerson(mainFrame.book.scenes,person);
					dataset.items.add(new DatasetItem(person.getAbbr(), l, person.getJColor()));
					if (dataset.maxValue < l) {
						dataset.maxValue = l;
					}
					dataset.listId.add(person.getAbbr());
					d += l;
				}
		}
		if (((List<AbstractEntity>)entities).size()>0) {
			dataset.average = (d / ((List<AbstractEntity>)entities).size());
		}
	}
	
	private AbstractAction getExportAction() {
		if (this.exportAction == null) {
			this.exportAction = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent paramAnonymousActionEvent) {
					try {
						String dir=mainFrame.bookPref.Export.getDirectory();
						File file1 = new File(dir);
						JFileChooser fileChooser = new JFileChooser(file1);
						FileFilter filter=new FileFilter(FileFilter.PNG,I18N.getMsg("file.type.png"));
						fileChooser.setFileFilter(filter);
						fileChooser.setApproveButtonText(I18N.getMsg("export"));
						String str = mainFrame.getXml().name + " - " + chartTitle;
						str = IOTools.cleanupFilename(str);
						fileChooser.setSelectedFile(new File(str));
						int i = fileChooser.showDialog(panel, I18N.getMsg("export"));
						if (i == 1) {
							return;
						}
						File file2 = fileChooser.getSelectedFile();
						if (!file2.getName().endsWith(".png")) {
							file2 = new File(file2.getPath() + ".png");
						}
						ScreenImage.createImage(panel, file2.toString());
						JOptionPane.showMessageDialog(panel,
							I18N.getMsg("export.success"),
							I18N.getMsg("export"), 1);
					} catch (HeadlessException | IOException localException) {
					}
				}
			};
		}
		return this.exportAction;
	}
}
