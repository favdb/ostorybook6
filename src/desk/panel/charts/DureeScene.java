/*
 * Copyright (C) 2016 favdb
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package desk.panel.charts;

import db.entity.Book;
import db.entity.SbDate;
import java.util.ArrayList;
import java.util.List;
import db.entity.Scene;
import db.util.EntityTool;

/**
 *
 * @author favdb
 */
public class DureeScene {

	public static List<DureeScene> initScenes(Book book) {
		List<Scene> scenes = EntityTool.listToExport(book);
		List<DureeScene> md = new ArrayList<>();
		scenes.stream().filter((scene) -> (scene.hasDate())).forEachOrdered((scene) -> {
			md.add(new DureeScene(scene.id, scene.date));
		});
		if (md.isEmpty()) return(md);
		if (md.size()>0) for (int i = 1; i < md.size() - 1; i++) {
			md.get(i - 1).fin = md.get(i).debut;
			long end = md.get(i - 1).fin.toLong();
			long beg = md.get(i - 1).debut.toLong();
			md.get(i - 1).duree = (end - beg) / (60 * 1000);
			if (md.get(i - 1).duree == 0L) {
				md.get(i - 1).duree = 1;
			}
		}
		md.get(md.size()-1).fin=md.get(md.size()-1).debut;
		return (md);
	}

	long id;
	SbDate debut;
	SbDate fin;
	long duree;

	public DureeScene(long id, SbDate debut) {
		this.id = id;
		this.debut = debut;
	}
	
	public static List<DureeScene> calculDureeScene(Book book,List<DureeScene> durees) {
		List<Scene> scenes = EntityTool.listToExport(book);
		List<DureeScene> md=new ArrayList<>();
		scenes.stream().filter((scene) -> (scene.hasDate())).forEachOrdered((scene) -> {
			md.add(new DureeScene(scene.id, scene.date));
		});
		for (int i=1; i<md.size(); i++) {
			for (Scene scene : scenes) {
				if (scene.id==md.get(i).id) {
					if (md.get(i).debut!=null) {
						md.get(i-1).fin=md.get(i).debut;
						long end=md.get(i-1).fin.toLong();
						long beg=md.get(i-1).debut.toLong();
						md.get(i-1).duree=(end-beg) / (60 * 1000);
						if (md.get(i-1).duree==0L) md.get(i-1).duree=1;
					}
					break;
				}
			}
			if (md.get(i-1).duree==0L) md.get(i-1).duree=1L;
		}
		for (DureeScene duree : durees) {
			for (DureeScene d : md) {
				if (duree.id==d.id) {
					duree.fin=d.fin;
					duree.duree=d.duree;
					break;
				}
			}
			if (duree.fin==null) {
				duree.fin=duree.debut;
				duree.duree=1L;
			}
		}
		return(durees);
	}

}
