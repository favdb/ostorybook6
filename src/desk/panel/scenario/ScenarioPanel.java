/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.scenario;

import db.entity.AbstractEntity;
import db.entity.Idea;
import db.entity.Person;
import db.entity.Scene;
import db.I18N;
import db.entity.Book;
import db.entity.Location;
import db.entity.SceneScenario;
import desk.app.MainFrame;
import desk.dialog.edit.Editor;
import desk.interfaces.IRefreshable;
import desk.listcell.LCREntity;
import desk.net.NetUtil;
import desk.panel.AbstractPanel;
import desk.dialog.ListEntitiesDlg;
import desk.tools.TempUtil;
import desk.tools.combobox.ComboLoad;
import desk.tools.combobox.ComboUtil;
import desk.tools.markdown.Markdown;
import desk.tools.swing.SwingUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class ScenarioPanel extends AbstractPanel implements ActionListener, IRefreshable {

	private int allreadySave=0;
	private JButton btSave;
	private JButton btIgnore;
	private JTextField tfPitch;
	private JComboBox cbMoment;
	private JComboBox cbStart;
	private JComboBox cbEnd;
	private JComboBox cbLocation;
	private JRadioButton rbLocInt;
	private JRadioButton rbLocExt;
	private JButton btLoc;

	public enum ACTION {
		BT_IDEA("btIdea"), // add new idea
		BT_PERSON_NEW("btPersonNew"), // add new person and add a button
		BT_SCENE_CHANGE("btScenChange"),// ignore changes and restore original current scene
		BT_SCENE_PREVIOUS("btScenePrevious"),
		BT_SCENE_NEXT("btSceneNext"),
		BT_SCENE_NEW("btSceneNew"),// add new scene
		BT_SCENE_SAVE("btScenSave"),// save changes
		BT_SCREEN("btScreen"), // change full/normal screen
		BT_STAGE("btStage"),// call browser to online help about assistant.stage
		CB_SCENES("cbScenes"),// list of scenes
		CB_STAGE("cbStage"),// list of stage
		CB_STATUS("cbStatus"),// list of status
		// scenario
		BT_ADD_LOCATION("btAddLocation"),
		CB_MOMENT("cbMoment"),
		CB_LOC("cbLoc"),
		CB_IN("cbIn"),
		CB_OUT("cbOut"),
		CB_LOCATIONS("cbLocations"),
		RB_INT("rbInt"),
		// communs
		PRINT("scenario_Print"),// printing current scene
		REFRESH("scenario_Refresh"),// refresh current scene
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	public Scene scene;
	private JButton btNext,
			btPrevious,
			btScreen;
	private JPanel left,
			pPersons;
	private JScrollPane personScroll;
	private JTextArea taDescription,
			taNotes;
	private JComboBox cbStage,
			cbScenes,
			cbStatus;
	private JTextField tfName=new JTextField();
	private Markdown textEdit=new Markdown();
	private int nScreen = 1; // 1=text, 2=html, 3=both

	public ScenarioPanel(MainFrame mainFrame) {
		super(mainFrame);
		this.scene = mainFrame.getLastUsedScene();
		initAll();
	}

	@Override
	public void init() {
		if (this.scene == null) {
			if (book.scenes.size() > 0) {
				scene = book.scenes.get(0);
			}
		}
	}

	@Override
	public void initUi() {
		//App.trace("Scenario.initUi()");
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		this.setLayout(new MigLayout("wrap, hidemode 3, top, ins 1 1 1 1", "[grow][65%][20%]"));
		initToolbar(true);
		//previous scene
		btPrevious = this.initButton(ACTION.BT_SCENE_PREVIOUS.toString(), "",
				"small/nav-previous", "navigation.scene.previous");
		btPrevious.setEnabled(false);
		toolbar.add(btPrevious);

		//combobox scene
		cbScenes = entitiesInitCb(ACTION.CB_SCENES, Book.TYPE.SCENE, scene);
		toolbar.add(cbScenes);

		toolbar.add(initButton(ACTION.BT_SCENE_NEW.toString(), "", "small/new", "scene.new"));

		btNext = this.initButton(ACTION.BT_SCENE_NEXT.toString(), "",
				"small/nav-next", "navigation.scene.next");
		if (book.scenes.size() < 1) {
			btNext.setEnabled(false);
		}
		toolbar.add(btNext);

		//ignorer les modifications		
		btSave = SwingUtil.initButton(ACTION.BT_SCENE_SAVE.toString(), "z.save", "", "");
		btSave.addActionListener(this);
		toolbar.add(btSave);
		
		btIgnore = SwingUtil.initButton(ACTION.BT_SCENE_CHANGE.toString(), "z.discard", "", "");
		btIgnore.addActionListener(this);
		toolbar.add(btIgnore);
		
		toolbar.add(new JToolBar.Separator());
		
		toolbar.add(new JLabel(" "), "push");
		
		btScreen = this.initButton(ACTION.BT_SCREEN.toString(), "", "small/screen-full", "screen.full");
		toolbar.add(btScreen,"left");

		toolbar.add(initButtonIdea());
		add(toolbar, "span, grow");

		add(initHeader(), "span,grow, wrap");
		add(initLeft(), "grow, top");
		add(initMiddle());
		add(initRight(), "growy");
		add(initFooter(),"span");

		sceneRefresh();
		enableNav(0);
		resetModified();
		mainFrame.setScenarioPanel(this);
	}

	@SuppressWarnings("unchecked")
	private JComboBox entitiesInitCb(ACTION action, Book.TYPE type, AbstractEntity entity) {
		//App.trace("Scenario.initCbScenes()");
		JComboBox cb = new JComboBox();
		cb.setName(action.toString());
		cb.setRenderer(new LCREntity(book, type));
		ComboUtil.fillEntity(book, cb, type);
		if (entity != null) {
			cb.setSelectedItem(entity);
		}
		cb.addActionListener(this);
		return (cb);
	}

	private JPanel initHeader() {
		//App.trace("ScenarioPanel.initHeader()");
		JPanel head = new JPanel(new MigLayout("fill, hidemode 3"));
		head.setBorder(BorderFactory.createRaisedBevelBorder());

		tfName = initTextField("name", 16, "");
		tfName.setEditable(false);
		head.add(new JLabel(I18N.getColonMsg("z.name")),"span, split 4");
		head.add(tfName);

		cbStatus = new JComboBox();
		cbStatus.setName(ACTION.CB_STATUS.toString());
		ComboLoad.loadCbStatus(cbStatus, null);
		head.add(new JLabel(I18N.getColonMsg("status")),"span, split 2");
		head.add(cbStatus,"wrap");

		cbStage = new JComboBox();
		cbStage.setName(ACTION.CB_STAGE.toString());
		ComboLoad.loadCbProgression(mainFrame, cbStage, "");
		head.add(new JLabel(I18N.getColonMsg("assistant.stage")),"span, split 3");
		head.add(cbStage, "split 2");
		JButton bt = SwingUtil.initButton(ACTION.BT_STAGE.toString(), "",
				"small/help", "assistant.stage.help");
		bt.addActionListener(this);
		head.add(bt);

		head.add(scenarioInit(),"span");
		return (head);
	}

	private JPanel initLeft() {
		//App.trace("ScenarioPanel.initLeft()");
		left = new JPanel(new MigLayout(""));
		left.setBorder(BorderFactory.createTitledBorder(I18N.getColonMsg("persons")));

		JButton bt = SwingUtil.initButton(ACTION.BT_PERSON_NEW.toString(),
				"", "small/new", "person.new");
		bt.addActionListener(this);
		left.add(bt, "span, growx");
		pPersons = new JPanel(new MigLayout("ins 0, wrap"));
		personScroll = new JScrollPane(pPersons);
		left.add(personScroll, "span, grow, wrap");

		return (left);
	}

	private JPanel initMiddle() {
		//App.trace("Scenario.initMiddle()");
		JPanel middle = new JPanel(new MigLayout("hidemode 3"));
		//the text editor
		textEdit = new Markdown("textEdit", "text/plain", "");
		textEdit.setCaretPosition(0);
		textEdit.setCallback(this);
		middle.add(textEdit, "grow");
		textEdit.setView(Markdown.VIEW.ALL);
		return (middle);
	}

	private JPanel initRight() {
		//App.trace("Scenario.initRght()");
		JPanel right = new JPanel(new MigLayout("wrap"));

		taDescription = initTextArea(right, "z.description", "");

		taNotes = initTextArea(right, "z.notes", "");

		return (right);
	}

	private JPanel initFooter() {
		//App.trace("Scenario.initFooter()");
		JPanel foot = new JPanel(new MigLayout());
		return (foot);
	}

	private void locationsLoad() {
		//App.trace("ScenarioPanel(locationsLoad()");
		ComboUtil.fillEntity(book, cbLocation, Book.TYPE.LOCATION);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Scenario.modelPropertyChange(evt=" + evt.toString() + ")");
		String propName = evt.getPropertyName();
		String types="scene, location, person, plot, photo";
		if (propName.contains("_")) {
			String m[]=propName.split("_");
			if (!types.contains(m[0])) {
				return;
			}
			switch(Book.getTYPE(m[0])) {
				case SCENE:
					scenesLoad();
					if (m[1].toLowerCase().equals("new")) {
						cbScenes.setSelectedIndex(cbScenes.getItemCount()-1);
					}
					break;
				case PERSON:
					personsLoad();
					break;
				case LOCATION:
					locationsLoad();
					break;
			}
		}
	}

	/** set the current scene
	 * 
	 * @param scene 
	 */
	public void setScene(Scene scene) {
		//App.trace("Scenario.setScene(scene=" + (scene == null ? "null" : scene.toString()) + ")");
		this.scene = scene;
		sceneRefresh();
	}

	public void sceneRefresh(Scene scene) {
		//App.trace("Scenario.sceneRefresh(scene=" + (scene == null ? "null" : scene.toString()) + ")");
		if (scene != null) {
			this.scene = scene;
			sceneRefresh();
		}
	}

	public void sceneRefresh() {
		//App.trace("ScenarioPanel.sceneRefresh()");
		resetListener();
		Location location=null;
		if (scene == null) {
			tfName.setText("");
			textEdit.setText("");
			cbStage.setSelectedIndex(-1);
			cbStatus.setSelectedIndex(-1);
		} else {
			tfName.setText(scene.name);
			if (scene.writing.contains("<p")) {
				textEdit.setText(Markdown.toMarkdown(scene.writing));
			} else {
				textEdit.setText(scene.writing);
			}
			cbStage.setSelectedItem(scene.stage);
			cbStatus.setSelectedIndex(scene.status);
			if (scene.locations!=null && !scene.locations.isEmpty()) {
				location=scene.locations.get(0);
			}
			List<Location> locations = scene.locations;
			Location loc=null;
			if (locations!=null && !locations.isEmpty()) loc=locations.get(0);
			if (loc!=null) {
				cbLocation.setSelectedItem(loc);
			} else {
				cbLocation.setSelectedIndex(-1);
			}
			tfPitch.setText(scene.scenario.pitch);
			cbMoment.setSelectedIndex(scene.scenario.moment);
			cbStart.setSelectedIndex(scene.scenario.start);
			cbEnd.setSelectedIndex(scene.scenario.end);
			rbLocInt.setSelected((scene.scenario.loc==1));
			rbLocExt.setSelected((scene.scenario.loc==2));
			mainFrame.setLastUsedScene(scene);
			textEdit.setHeader(location, scene.scenario);
		}
		textEdit.setCaretPosition(0);
		personsLoad();
		left.revalidate();
		setListener();
		resetModified();
		//App.trace("end Scenario.sceneRefresh()");
	}

	private void setListener() {
		//App.trace("ScenarioPanel.setListener()");
		cbScenes.addActionListener(this);
		cbStage.addActionListener(this);
		cbStatus.addActionListener(this);
		cbLocation.addActionListener(this);
		cbMoment.addActionListener(this);
		cbStart.addActionListener(this);
		cbEnd.addActionListener(this);
		rbLocInt.addActionListener(this);
		rbLocExt.addActionListener(this);
	}

	private void resetListener() {
		//App.trace("ScenarioPanel.resetListener()");
		cbScenes.removeActionListener(this);
		cbStage.removeActionListener(this);
		cbStatus.removeActionListener(this);
		cbLocation.removeActionListener(this);
		cbMoment.removeActionListener(this);
		cbStart.removeActionListener(this);
		cbEnd.removeActionListener(this);
		rbLocInt.removeActionListener(this);
		rbLocExt.removeActionListener(this);
	}

	public JTextField initTextField(String title, int len, String val) {
		//App.trace("Scenario.initTextField(title=" + title + ", len=" + len + ", val=\"" + val + "\")");
		JTextField tf = new JTextField();
		tf.setName(title);
		tf.setText(val);
		if (len > 0) {
			tf.setColumns(len);
		}
		return (tf);
	}

	public JTextArea initTextArea(JPanel p, String title, String val) {
		//App.trace("Scenario.initTextArea(title=" + title + ", val=\"" + val + "\")");
		p.add(new JLabel(I18N.getColonMsg(title)));
		JTextArea ta = new JTextArea();
		ta.setName(title);
		if (val != null) {
			ta.setText(val);
		}
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		JScrollPane scroller = new JScrollPane(ta);
		SwingUtil.setMaxPreferredSize(scroller);
		p.add(scroller);
		return (ta);
	}

	@Override
	@SuppressWarnings({"unchecked", "unchecked"})
	public void actionPerformed(ActionEvent evt) {
		//App.trace("Scenario.actionPerformed(evt=" + evt.toString() + ")");
		if (evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			int n;
			switch (getAction(btn.getName())) {
				case BT_PERSON_NEW:
					personBtAction();
					break;
				case BT_SCENE_PREVIOUS:
					checkModified(false);
					n = cbScenes.getSelectedIndex();
					if (n > 0) {
						n--;
						sceneSet(n);
					}
					break;
				case BT_SCENE_NEXT:
					checkModified(false);
					n = cbScenes.getSelectedIndex();
					if (n < 0) {
						n = 0;
					}
					if (n < cbScenes.getItemCount() - 1) {
						n++;
						sceneSet(n);
					}
					break;
				case BT_SCENE_NEW:
					if (scene != null) {
						checkModified(false);
						sceneNew();
					}
					break;
				case BT_SCENE_CHANGE:
					resetModified();
					sceneRefresh(book.getScene(scene.id));
					break;
				case BT_SCENE_SAVE:
					scene=getData();
					save();
					break;
				case BT_SCREEN:
					nScreen++;
					if (nScreen == 3) {
						nScreen = 1;
					}
					String ic=(nScreen==1?"full":"normal");
					btn.setIcon(IconUtil.getIcon("small/screen-" + ic));
					btn.setToolTipText(I18N.getMsg("screen." + ic));
					break;
				case BT_IDEA:
					Idea idea = new Idea();
					Editor didea = new Editor(mainFrame, idea, null);
					didea.setVisible(true);
					break;
				case BT_STAGE:
					NetUtil.openBrowser(I18N.getMsg("assistant.stage.web"));
					break;
				case BT_ADD_LOCATION:
					Location loc = new Location();
					Editor dloc = new Editor(mainFrame, loc, null);
					dloc.setVisible(true);
					if (!dloc.canceled) {
						cbLocation.addItem(dloc.entity.id);
						cbLocation.setSelectedItem(dloc.entity.id);
						mainFrame.setModified();
					}
					break;
			}
		} else if (evt.getSource() instanceof JComboBox) {
			JComboBox cb = (JComboBox) evt.getSource();
			switch (getAction(cb.getName())) {
				case CB_SCENES:
					checkModified(false);
					Long id = (Long) cbScenes.getSelectedItem();
					scene = book.getScene(id);
					sceneRefresh();
					enableNav(cbScenes.getSelectedIndex());
					break;
				case CB_LOCATIONS:
				case CB_MOMENT:
				case CB_IN:
				case CB_OUT:
					Location loc=book.getLocation((Long)cbLocation.getSelectedItem());
					textEdit.setHeader(loc,getScenario());
					mainFrame.setModified();
					break;
			}
		} else if (evt.getSource() instanceof JRadioButton) {
			Location loc=book.getLocation((Long)cbLocation.getSelectedItem());
			textEdit.setHeader(loc,getScenario());
			mainFrame.setModified();
		}
	}
	
	/** enable/disable previous and next buttons
	 * 
	 * @param n : current scene index in cbScenes 
	 */
	private void enableNav(int n) {
		btPrevious.setEnabled(n>0);
		btNext.setEnabled(n<book.scenes.size()-1);
	}

	/** action for adding a Person
	 * 
	 */
	private void personBtAction() {
		//App.trace("ScenarioPanel.btPersonAction()");
		ListEntitiesDlg dlg = new ListEntitiesDlg(mainFrame, scene, Book.TYPE.PERSON);
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.persons=(dlg.getPersons());
			personsLoad();
			mainFrame.setModified();
		}
	}

	/** init a Person button for adding a dialog line
	 * 
	 * @param person
	 * @return 
	 */
	private JButton personInitButton(Person person) {
		//App.trace("Scenario.initPersonButton(person=" + person.getAbbr() + ")");
		JButton bt = new JButton(person.getIcon());
		bt.setName("btDialog_" + person.id);
		bt.setText(person.getAbbr());
		bt.setToolTipText("<html>" + person.toHtmlDetail() + "</html>");
		bt.addActionListener((ActionListener) evt -> {
			addDialog(person);
		});
		return (bt);
	}

	/** add a dialog line with the selected Person
	 *  call from the action button
	 * 
	 * @param entity : type Person 
	 */
	private void addDialog(AbstractEntity entity) {
		//App.trace("Scenario.addDialog()");
		String text = textEdit.getText();
		if (entity == null) {
			return;
		}
		if (scene == null) {
			return;
		}
		if (!text.isEmpty() && !text.endsWith("\n")) {
			text += "\n";
		}
		if (!text.isEmpty() && !text.endsWith("\n\n")) {
			text += "\n";
		}
		text += "[" + entity.name.trim() + "] ";
		textEdit.setText(text);
		textEdit.setFocus();
		setModified();
	}
	
	private Scene getData() {
		//App.trace("ScenarioPanel.getData()");
		Scene s=new Scene();
		if (scene!=null) s=Scene.copy(book, scene, scene.id);
		s.id=scene.id;
		s.creation=scene.creation;
		s.maj=scene.maj;
		s.status=(cbStatus.getSelectedIndex());
		s.stage=((String)cbStage.getSelectedItem());
		List<Location> loc=new ArrayList<>();
		if (cbLocation.getSelectedItem()!=null) loc.add((Location)cbLocation.getSelectedItem());
		s.locations=(loc);
		s.scenario.setPitch(tfPitch.getText());
		s.scenario.setMoment(cbMoment.getSelectedIndex());
		s.scenario.setMoment(cbMoment.getSelectedIndex());
		s.scenario.setLoc((rbLocInt.isSelected()?1:2));
		s.scenario.setStart(cbStart.getSelectedIndex());
		s.scenario.setEnd(cbEnd.getSelectedIndex());
		s.writing=(textEdit.getText());
		s.desc=(taDescription.getText());
		s.notes=(taNotes.getText());
		return(s);
	}
	
	private SceneScenario getScenario() {
		//App.trace("ScenarioPanel.getScenario()");
		SceneScenario s=new SceneScenario();
		s.setPitch(tfPitch.getText());
		s.setMoment(cbMoment.getSelectedIndex());
		s.setMoment(cbMoment.getSelectedIndex());
		s.setLoc((rbLocInt.isSelected()?1:2));
		s.setStart(cbStart.getSelectedIndex());
		s.setEnd(cbEnd.getSelectedIndex());
		return(s);
	}

	public int checkModified(boolean force) {
		//App.trace("ScenarioPanel.checkModified(force="+(force?"true":"false")+") allreadySave="+allreadySave);
		if (scene == null) {
			return(JOptionPane.CANCEL_OPTION);
		}
		if (textEdit.getText().length() > 32767) {
			String str = I18N.getMsg("editor.text_too_large");
			JOptionPane.showMessageDialog(
					this,
					str,
					I18N.getMsg("editor"),
					JOptionPane.ERROR_MESSAGE);
			return (JOptionPane.CANCEL_OPTION);
		}
		Scene s=getData();
		if (s.hashCode()!=scene.hashCode()) {
			//if (!force) {
				scene=s;
				save();
			//	return(JOptionPane.YES_OPTION);
			//}
			/*final Object[] options = {
				I18N.getMsg("z.save"),
				I18N.getMsg("z.discard"),
				I18N.getMsg("z.cancel")
			};
			int n = JOptionPane.showOptionDialog(
					this,
					I18N.getMsg("z.save_or_discard") + "\n\n"
					+ scene.name + ": "
					+ scene.toString() + "\n\n",
					I18N.getMsg("z.changes"),
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
			switch (n) {
				case JOptionPane.CANCEL_OPTION:
					return (JOptionPane.CANCEL_OPTION);
				case JOptionPane.YES_OPTION:
						scene=s;
						save();
					break;
				case JOptionPane.NO_OPTION:
					break;
				default:
					break;
			}*/
		}
		return(JOptionPane.YES_OPTION);
	}

	private void save() {
		//App.trace("ScenarioPanel.save()");
		resetModified();
		resetListener();
		if (scene.id<0) {
			mainFrame.getBookModel().setEntityNew(scene);
		} 
		else mainFrame.getBookModel().setEntityUpdate(scene);
	}

	@SuppressWarnings("unchecked")
	private void sceneNew() {
		//App.trace("ScenarioPanel.sceneNew()");
		Scene s = new Scene();
		if (scene != null) {
			s.chapter=(scene.chapter);
		}
		s.number=(book.scenes.size()+1);
		s.name=(I18N.getMsg("scene")+" "+s.number);
		cbScenes.removeActionListener(this);
		cbScenes.addItem(s);
		mainFrame.getBookModel().setEntityNew(s);
		cbScenes.addActionListener(this);
		resetModified();
	}

	private void sceneSet(int n) {
		//App.trace("ScenarioPanel.sceneSet()");
		Long id = (Long) cbScenes.getItemAt(n);
		if (id != null) {
			cbScenes.removeActionListener(this);
			cbScenes.setSelectedIndex(n);
			resetModified();
			cbScenes.addActionListener(this);
			sceneRefresh(book.getScene(id));
		}
		enableNav(n);
	}

	private void personsLoad() {
		pPersons.removeAll();
		if (scene != null && !scene.persons.isEmpty()) {
			scene.persons.forEach((person) -> {
				if (person != null) {
					pPersons.add(personInitButton(person), "center, growx");
				}
			});
		}
	}

	private void scenesLoad() {
		//App.trace("ScenePanel.scenesLoad()");
		int n=cbScenes.getSelectedIndex();
		cbScenes.removeActionListener(this);
		ComboUtil.fillEntity(book, cbScenes, Book.TYPE.SCENE);
		cbScenes.setSelectedIndex(n);
		cbScenes.addActionListener(this);
		resetModified();
	}

	public void sceneCbChange(ItemEvent evt) {
		//App.trace("Memoria.cbEntityChange(evt=" + evt.toString() + ")");
		if (cbScenes.getSelectedItem() instanceof Long) {
			Long id=(Long) cbScenes.getSelectedItem();
			if (!id.equals(scene.id)) scene = book.getScene(id);
		}
	}

	public void tempSave() {
		//App.trace("ScenarioPanle.tempSave()");
		scene.writing=(textEdit.getText());
		TempUtil.write(mainFrame, scene);
	}
	
	public void setModified() {
		btSave.setEnabled(true);
		btIgnore.setEnabled(true);
		mainFrame.setModified();
	}
	
	public void resetModified() {
		btSave.setEnabled(false);
		btIgnore.setEnabled(false);
	}
	
	private JPanel scenarioInit() {
		//App.trace("ScenarioPanel.scenarioInit()");
		JPanel p=new JPanel(new MigLayout("hidemode 3"));
		p.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("scenario")));
		// pitch
		p.add(new JLabel(I18N.getColonMsg("scenario.pitch")), "span, split 2");
		tfPitch = new JTextField();
		tfPitch.setName("tfPitch");
		p.add(tfPitch, "growx");
		
		// liste des moments
		p.add(new JLabel(I18N.getColonMsg("scenario.moment")));
		cbMoment = initCbScenario(ACTION.CB_MOMENT);
		p.add(cbMoment);
		// radio des INT/EXT
		p.add(new JLabel(I18N.getColonMsg("location")));
		rbLocInt = new JRadioButton(I18N.getMsg("scenario.loc.int"));
		rbLocInt.setName("rbInt");

		rbLocExt = new JRadioButton(I18N.getMsg("scenario.loc.ext"));
		rbLocExt.setName("rbExt");

		ButtonGroup group = new ButtonGroup();
		group.add(rbLocInt);
		group.add(rbLocExt);
		p.add(rbLocInt);
		p.add(rbLocExt);
		
		// liste des lieux
		p.add(initCbLocation());
		btLoc = SwingUtil.initButton(ACTION.BT_ADD_LOCATION.toString(), "", "small/new", "");
		p.add(btLoc, "wrap");
		// liste de la transition du début
		p.add(new JLabel(I18N.getColonMsg("scenario.start")));
		cbStart = initCbScenario(ACTION.CB_IN);
		p.add(cbStart);
		// liste de la transition de fin
		p.add(new JLabel(I18N.getColonMsg("scenario.end")), "span, split 2, right");
		cbEnd = initCbScenario(ACTION.CB_OUT);
		p.add(cbEnd);
		return(p);
	}

	private JComboBox initCbLocation() {
		cbLocation = new JComboBox();
		cbLocation.setName(ACTION.CB_LOCATIONS.toString());
		locationsLoad();
		return (cbLocation);
	}

	@SuppressWarnings("unchecked")
	public JComboBox initCbScenario(ACTION action) {
		//App.trace("ScenarioPanel.initCbScenario("+action.toString()+")");
		String type = action.toString().toLowerCase().replaceFirst("cb", "");
		JComboBox cb = new JComboBox();
		cb.setName(action.toString());
		for (int i = 1;; i++) {
			String s = findScenarioKey(type, i);
			if (s.isEmpty()) {
				break;
			}
			cb.addItem(s);
		}
		return (cb);
	}

	public String findScenarioKey(String type, int i) {
		//App.trace("ScenarioPanel.findKey(type=" + type + ", i=" + i + ")");
		String key = "scenario." + type + "." + String.format("%02d", i);
		try {
			String msg = I18N.getMsg(key);
			if (msg.equals("!" + key + "!")) {
				return ("");
			}
			return (msg);
		} catch (Exception ex) {
			return ("");
		}
	}

}
