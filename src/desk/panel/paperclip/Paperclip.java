/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.paperclip;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Idea;
import db.entity.Scene;
import db.util.CheckUsed;
import desk.app.App;
import desk.app.MainFrame;
import desk.dialog.ConfirmDeleteDlg;
import desk.listcell.LCREntity;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import lib.miginfocom.swing.MigLayout;

/**
 * PaperclipPanel pour la visualisation en mode trombone Un JPanel principal contenant: - en haut
 * une barre d'outil sur laquelle on sélectionne les options (à définir) - outils: enregistrer les
 * modifications, basculement en mode normal - à gauche la liste des idées, si double click alors
 * edition de l'idées - à droite la liste des scènes, si double click alors édition de la scène - au
 * centre: - si on sélectionne une idée, on voit la description de l'idée, zone modifiable - si on
 * sélectionne une scène, on voit le texte de la scène, zone modifiable
 *
 * @author favdb
 */
public class Paperclip extends AbstractPanel implements ListSelectionListener {

	private JPanel sceneBar;
	private JPanel ideaBar;

	public enum ACTION {
		BT_IDEA_ADD("btIdeaAdd"),
		BT_IDEA_CONVERT("btIdeaConvert"),
		BT_IDEA_EDIT("btIdeaEdit"),
		BT_IDEA_DELETE("btIdeaDelete"),
		BT_SCENE_ADD("btSceneAdd"),
		BT_SCENE_EDIT("btSceneEdit"),
		BT_SCENE_DELETE("btSceneDelete"),
		CB_NB("view.trombone.zoom"),
		CB_DIRECTION("view.trombone.direction"),
		PRINT("Print"),
		REFRESH("Refresh"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private JPanel ideaPanel;
	private JList ideaList;
	private JPanel scenePanel;
	private JList sceneList;
	private Idea ideaCurrent;
	private Scene sceneCurrent;
	private JTextPane centerPanel;

	public Paperclip(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
		//todo init for PaperclipPanel
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("ins 0","[15%][60%][15%]"));
		ideaPanel = new JPanel(new MigLayout("fillx"));
		ideaPanel.setBorder(SwingUtil.getBorderDefault());
		SwingUtil.setMaxPreferredSize(ideaPanel);
		ideaBar=new JPanel(new MigLayout());
		ideaBar.add(new JLabel(I18N.getColonMsg("ideas")));
		JButton btIdeaAdd = initButton(ACTION.BT_IDEA_ADD.toString(),"","small/new");
		ideaBar.add(btIdeaAdd);
		JButton btIdeaEdit = initButton(ACTION.BT_IDEA_EDIT.toString(),"","small/edit");
		ideaBar.add(btIdeaEdit);
		JButton btIdeaDelete = initButton(ACTION.BT_IDEA_DELETE.toString(),"","small/delete");
		ideaBar.add(btIdeaDelete);
		JButton btIdeaConvert = initButton(ACTION.BT_IDEA_CONVERT.toString(),"","small/arrow_right");
		ideaBar.add(btIdeaConvert);

		scenePanel = new JPanel(new MigLayout("fillx"));
		scenePanel.setBorder(SwingUtil.getBorderDefault());
		SwingUtil.setMaxPreferredSize(scenePanel);
		sceneBar=new JPanel(new MigLayout());
		sceneBar.add(new JLabel(I18N.getColonMsg("scenes")));
		JButton btSceneAdd = initButton(ACTION.BT_SCENE_ADD.toString(),"","small/new");
		sceneBar.add(btSceneAdd);
		JButton btSceneEdit = initButton(ACTION.BT_SCENE_EDIT.toString(),"","small/edit");
		sceneBar.add(btSceneEdit);
		JButton btSceneDelete = initButton(ACTION.BT_SCENE_DELETE.toString(),"","small/delete");
		sceneBar.add(btSceneDelete);

		centerPanel = new JTextPane();
		centerPanel.setEditable(false);
		centerPanel.setOpaque(true);
		centerPanel.setContentType("text/html");
		JScrollPane scroller = new JScrollPane(centerPanel);
		SwingUtil.setMaxPreferredSize(scroller);
		
		refresh();

		add(ideaPanel,"aligny top");
		add(scroller,"grow");
		add(scenePanel,"aligny top");
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		App.trace("Paperclip.actionPerformed(evt=" + evt.toString() + ")");
		if (evt.getSource() instanceof JButton) {
			JButton btn=(JButton)evt.getSource();
			switch(getAction(btn.getName())) {
				case BT_IDEA_ADD:
					mainFrame.showEditorAsDialog(new Idea());
					break;
				case BT_IDEA_CONVERT:
					if (ideaList.getSelectedIndex()==-1) return;
					ideaCurrent=(Idea)ideaList.getSelectedValue();
					String newName=I18N.getColonMsg("idea")+ideaCurrent.name;
					if (Scene.findName(book.scenes, newName)!=null) {
						JOptionPane.showMessageDialog(null,
								I18N.getMsg("z.error.exists"),
								I18N.getMsg("z.error"),
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					Scene scene=new Scene();
					scene.name=newName;
					scene.number=0;
					scene.writing=(ideaCurrent.getDescription());
					scene.notes=(ideaCurrent.getNotes());
					scene.id=(book.last_id++);
					mainFrame.getBookModel().setEntityNew(scene);
					ideaCurrent.status++;
					mainFrame.getBookModel().setEntityUpdate(ideaCurrent);
					refresh();
					refreshCenter(ideaCurrent);
					break;
				case BT_IDEA_EDIT:
					if (ideaList.getSelectedIndex()==-1) return;
					ideaCurrent=(Idea)ideaList.getSelectedValue();
					mainFrame.showEditorAsDialog(ideaCurrent);
					break;
				case BT_IDEA_DELETE:
					if (ideaList.getSelectedIndex()==-1) return;
					ideaCurrent=(Idea)ideaList.getSelectedValue();
					CheckUsed idu = new CheckUsed(book, ideaCurrent);
					if (ConfirmDeleteDlg.show(mainFrame, ideaCurrent, idu)) {
						mainFrame.getBookController().deleteEntity(ideaCurrent);
					}
					break;
				case BT_SCENE_ADD:
					mainFrame.showEditorAsDialog(new Scene());
					break;
				case BT_SCENE_EDIT:
					if (sceneList.getSelectedIndex()==-1) return;
					sceneCurrent=(Scene)sceneList.getSelectedValue();
					mainFrame.showEditorAsDialog(sceneCurrent);
					break;
				case BT_SCENE_DELETE:
					if (sceneList.getSelectedIndex()==-1) return;
					sceneCurrent=(Scene)sceneList.getSelectedValue();
					CheckUsed sdu = new CheckUsed(book, sceneCurrent);
					if (ConfirmDeleteDlg.show(mainFrame, sceneCurrent, sdu)) {
						mainFrame.getBookController().deleteEntity(sceneCurrent);
					}
					break;
			}
		}
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Paperclip.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		switch (getAction(propName)) {
			case REFRESH:
				refresh();
				return;
		}
		if (propName.startsWith("idea")) refreshIdea();
		if (propName.startsWith("scene")) refreshScene();
	}

	@Override
	public void refresh() {
		refreshIdea();
		refreshScene();
	}

	@SuppressWarnings("unchecked")
	private void refreshIdea() {
		ideaPanel.removeAll();
		ideaPanel.add(ideaBar,"wrap");
		ideaList = new JList();
		SwingUtil.setMaxPreferredSize(ideaList);
		ideaList.setName("listIdea");
		ideaList.setCellRenderer(new LCREntity(book, Book.TYPE.IDEA));
		DefaultListModel lm = new DefaultListModel();
		book.ideas.forEach((idea) -> {
			lm.addElement(idea);
		});
		ideaList.setModel(lm);
		if (ideaCurrent!=null) ideaList.setSelectedValue(ideaCurrent, true);
		ideaList.addListSelectionListener(this);
		ideaPanel.add(ideaList);
	}
	
	@SuppressWarnings("unchecked")
	private void refreshScene() {
		scenePanel.removeAll();
		scenePanel.add(sceneBar,"wrap");
		sceneList = new JList();
		SwingUtil.setMaxPreferredSize(sceneList);
		sceneList.setName("listIdea");
		sceneList.setCellRenderer(new LCREntity(book, Book.TYPE.SCENE));
		DefaultListModel lm = new DefaultListModel();
		book.scenes.forEach((scene) -> {
			lm.addElement(scene);
		});
		sceneList.setModel(lm);
		if (sceneCurrent!=null) sceneList.setSelectedValue(sceneCurrent, true);
		sceneList.addListSelectionListener(this);
		scenePanel.add(sceneList);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		boolean adjust = e.getValueIsAdjusting();
		if (!adjust) {
			JList list = (JList) e.getSource();
			switch(list.getName()) {
				case "listIdea":
				case "listScene":
					refreshCenter((AbstractEntity)list.getSelectedValue());
					break;
			}
		}
	}

	private void refreshCenter(AbstractEntity entity) {
		String text="";
		switch(entity.type) {
			case IDEA:
				Idea idea=(Idea)entity;
				ideaCurrent=idea;
				text=idea.toHtmlDetail();
				break;
			case SCENE:
				Scene scene=(Scene)entity;
				sceneCurrent=scene;
				text=getSceneDetail(scene);
				break;
			default:
				break;
		}
		centerPanel.setText(text);
		centerPanel.setCaretPosition(0);
	}

	private String getSceneDetail(Scene scene) {
		StringBuilder buf=new StringBuilder();
		buf.append(scene.toHtmlDetailHeader());
		buf.append(scene.toHtmlShortDetail());
		buf.append(scene.toHtmlDetailFooter());
		return(buf.toString());
	}

}
