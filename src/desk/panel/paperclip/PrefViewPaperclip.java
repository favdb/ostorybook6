/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.paperclip;

import desk.app.pref.AppPref;
import db.I18N;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PrefViewPaperclip {
	private final AppPref pref;
	
	public PrefViewPaperclip(AppPref pref) {
		this.pref=pref;
	}
	
	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.memo", 3));
		b.append("<p>\n");
		//b.append(HtmlUtil.getAttribute("view.memo.layoutdirection.left", isLeft));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.memo")).append("\n");
		//b.append(TextUtil.getAttribute("view.memo.layoutdirection.left", isLeft));
		return (b.toString());
	}
	
	public void save() {
		//pref.setString(Pref.Key.VIEW_MEMO, (isLeft?"1":"0"));
	}
	
	public void load() {
		String v=pref.getString(AppPref.Key.VIEW_MEMO);
		if (v!=null && !v.isEmpty()) {
			String z[]=v.split(",");
			//if (z.length>0) isLeft=("1".equals(z[0]));
		}
	}

}
