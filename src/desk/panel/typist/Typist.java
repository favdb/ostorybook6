/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.typist;

import db.entity.Chapter;
import db.entity.Scene;
import db.I18N;
import desk.app.App;
import desk.app.MainFrame;
import desk.app.pref.AppPref;
import desk.dialog.ChapterSelectDlg;
import desk.dialog.edit.Editor;
import desk.dialog.edit.EditorHtml;
import desk.panel.AbstractPanel;
import desk.tools.FontUtil;
import desk.tools.TempUtil;
import desk.tools.swing.SwingUtil;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.Objects;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class Typist extends AbstractPanel {

	public enum ACTION {
		BT_CHAPTER("btChapterNew"),
		BT_IDEA("btIdea"),
		BT_PERSON_NEW("btPersonNew"),
		BT_SCENE_CHANGE("btScensChange"),
		BT_SCENE_FIRST("btSceneFirst"),
		BT_SCENE_LAST("btSceneLast"),
		BT_SCENE_NEXT("btSceneNext"),
		BT_SCENE_NEW("btSceneNew"),
		BT_SCENE_PREVIOUS("btScenePrevious"),
		BT_SCENE_SEL("btSceneSel"),
		BT_SCREEN("btScreen"),
		BT_SHOWINFO("btShowInfo"),
		BT_STAGE("btStage"),
		CB_SCENES("cbScenes"),
		PRINT("scene_Print"),
		REFRESH("scene_Refresh"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private Scene scene = null;
	private boolean
			modified,
			toRefresh,
			cbEntityInit,
			isScreenFull;
	private Dimension screenSize;
	private int defaultHeight;
	private EditorHtml panelEdit;
	private TypistInfo panelInfo;
	private JComboBox cbEntity;
	private JButton
			btNewScene,
			btFirst,
			btPrior,
			btNext,
			btLast,
			btScreenFull,
			btChapter,
			btTitle,
			btShowInfo;
	private JPanel statusbar;
	private JSplitPane split;
	private int saveSplit;

	public Typist(MainFrame mainFrame) {
		super(mainFrame);
		Scene s=mainFrame.getLastUsedScene();
		scene = (Scene) TempUtil.check(mainFrame, s);
		if (scene == null) {
			scene = getFirstScene();
		}
		initAll();
	}

	@Override
	public void init() {
		toRefresh = false;
		isScreenFull=false;
		App.getInstance().getPreferences().prefUI.setTypist(true);
	}

	@Override
	public void initUi() {
		App.trace("Typist.initUi()");
		this.setLayout(new MigLayout("wrap,fill,ins 1"));
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		defaultHeight = 32;
		add(initToolbar(!WITH_PART), "span, growx");
		panelEdit = new EditorHtml(mainFrame, true, EditorHtml.TWOLINE);
		if (scene != null) {
			panelEdit.setText(scene.writing);
			panelEdit.setCaretPosition(0);
		}
		panelInfo = new TypistInfo(this, scene);
		split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, panelEdit, panelInfo);
		split.setResizeWeight(0.85);
		add(split, "growy");
		add(initStatusbar());
		refresh();
	}

	@Override
	public JToolBar initToolbar(boolean withPart) {
		App.trace("Typist.initToolbar()");
		toolbar = new JToolBar();
		toolbar.setLayout(new MigLayout("ins 1"));
		toolbar.setFloatable(false);
		toolbar.setName("TypistToolbar");
		Dimension d = new Dimension(screenSize.width, defaultHeight);
		toolbar.setSize(d);
		//toolbar.setMinimumSize(d);
		toolbar.setMaximumSize(d);

		cbEntity = new JComboBox();
		cbEntity.setName("sceneList");
		cbEntity.setMaximumRowCount(15);
		refreshScenes(scene);
		cbEntity.setSelectedItem(scene);
		cbEntity.addActionListener((ActionEvent evt) -> {
			if (cbEntityInit) {
				return;
			}
			if (askModified() == JOptionPane.CANCEL_OPTION) {
				return;
			}
			if (((JComponent) evt.getSource()).getName().equals("sceneList")) {
				if (cbEntity.getSelectedItem() != null) {
					scene = (Scene) cbEntity.getSelectedItem();
					refresh();
				}
			}
		});
		toolbar.add(cbEntity);
		btNewScene = SwingUtil.initButton("", "small/new", "scene.new");
		btNewScene.addActionListener((ActionEvent evt) -> {
			if (newScene()) {
				refresh();
			}
		});
		toolbar.add(btNewScene);
		toolbar.add(new JToolBar.Separator());

		//navigation dans les scènes
		btFirst = SwingUtil.initButton("", "small/nav-first", "navigate.first");
		btFirst.addActionListener((ActionEvent evt) -> {
			if (askModified() == JOptionPane.CANCEL_OPTION) {
				return;
			}
			getFirstScene();
			refresh();
		});
		btFirst.setEnabled(!isSceneFirst());
		toolbar.add(btFirst);
		btPrior = SwingUtil.initButton("", "small/nav-previous", "navigate.previous");
		btPrior.addActionListener((ActionEvent evt) -> {
			if (askModified() == JOptionPane.CANCEL_OPTION) {
				return;
			}
			getPriorScene();
			refresh();
		});
		toolbar.add(btPrior);
		btNext = SwingUtil.initButton("", "small/nav-next", "navigate.next");
		btNext.addActionListener((ActionEvent evt) -> {
			if (askModified() == JOptionPane.CANCEL_OPTION) {
				return;
			}
			getNextScene();
			refresh();
		});
		toolbar.add(btNext);
		btLast = SwingUtil.initButton("", "small/nav-last", "navigate.last");
		btLast.addActionListener((ActionEvent evt) -> {
			if (askModified() == JOptionPane.CANCEL_OPTION) {
				return;
			}
			getLastScene();
			refresh();
		});
		btLast.setEnabled(!isSceneLast());
		toolbar.add(btLast);
		toolbar.add(new JToolBar.Separator());

		//ignorer les modifications		
		JButton btChange = SwingUtil.initButton("z.discard", "", "");
		btChange.addActionListener((ActionEvent evt) -> {
			refresh();
		});
		toolbar.add(btChange);
		toolbar.add(new JToolBar.Separator());

		//titre du chapitre
		toolbar.add(new JToolBar.Separator());
		JLabel lb = new JLabel(I18N.getMsg("chapter"));
		lb.setFont(FontUtil.getBold());
		toolbar.add(lb);
		btChapter = SwingUtil.initButton("", "small/chapter", "chapter.change");
		btChapter.addActionListener((ActionEvent evt) -> {
			changeChapter();
		});
		toolbar.add(btChapter, "growx");

		//titre de la scène
		toolbar.add(new JToolBar.Separator());
		lb = new JLabel(I18N.getMsg("scene"));
		lb.setFont(FontUtil.getBold());
		toolbar.add(lb);
		btTitle = SwingUtil.initButton("", "small/scene", "title");
		btTitle.addActionListener((ActionEvent evt) -> {
			changeSceneTitle();
		});
		toolbar.add(btTitle);

		toolbar.add(new JToolBar.Separator());
		btShowInfo = SwingUtil.initButton("", "small/hide_infos", "typist.show_infos");
		btShowInfo.addActionListener((ActionEvent evt) -> {
			if (panelInfo.isVisible()) {
				saveSplit = split.getDividerLocation();
			}
			panelInfo.setVisible(!panelInfo.isVisible());
			if (panelInfo.isVisible()) {
				btShowInfo.setIcon(IconUtil.getIcon("small/hide_infos"));
				split.setDividerLocation(saveSplit);
			} else {
				btShowInfo.setIcon(IconUtil.getIcon("small/info"));
			}
		});
		toolbar.add(btShowInfo);
		toolbar.add(new JToolBar.Separator());

		//full screen
		btScreenFull = SwingUtil.initButton("", "small/screen-full", "screen.full");
		btScreenFull.setEnabled(true);
		btScreenFull.addActionListener((ActionEvent evt) -> {
			if (askModified() != JOptionPane.CANCEL_OPTION) {
				close();
			}
			if (toRefresh) {
				SwingUtil.setWaitingCursor(this);
				mainFrame.refresh();
				SwingUtil.setDefaultCursor(this);
			}
			App.getInstance().getPreferences().setBoolean(AppPref.Key.TYPIST_USE, true);
		});
		toolbar.add(btScreenFull,"right");

		toolbar.add(initButtonIdea(),"right");
		return (toolbar);
	}

	private JPanel initStatusbar() {
		App.trace("Typist.initStatusbar()");
		statusbar = new JPanel();
		statusbar.setName("TypistStatusbar");
		statusbar.setLayout(new MigLayout(""));
		statusbar.setSize(screenSize.width, defaultHeight);
		return (statusbar);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Typist.modelPropertyChange(evt="+evt.toString()+")");
	}

	@Override
	public void refresh() {
		App.trace("Typist.refresh()");
		if (scene.writing != null) {
			panelEdit.setText(scene.writing);
			panelEdit.setCaretPosition(0);
		}
		if (scene.hasChapter()) {
			btChapter.setText(scene.chapter.name);
		} else {
			btChapter.setText(I18N.getMsg("scene.hasnochapter"));
		}
		if (scene.name.isEmpty()) {
			btTitle.setText(I18N.getMsg("scene.hasnotitle"));
		} else {
			btTitle.setText(scene.name);
		}
		panelInfo.refreshInfo(scene);
		modified = false;
		btFirst.setEnabled(!isSceneFirst());
		btPrior.setEnabled(!isSceneFirst());
		btNext.setEnabled(!isSceneLast());
		btLast.setEnabled(!isSceneLast());
		cbEntity.setSelectedItem(scene);
		mainFrame.setLastUsedScene(scene);
		repaint();
	}

	private Scene getFirstScene() {
		App.trace("Typist.getFirstScene()");
		if (scene != null) {
			TempUtil.remove(mainFrame, scene);
		}
		List<Scene> scenes = book.scenes;
		if (scenes.isEmpty()) {
			return (null);
		}
		scene = (Scene) TempUtil.check(mainFrame, scenes.get(0));
		return (scene);
	}

	private Scene getPriorScene() {
		App.trace("Typist.getPriorScene()");
		if (scene != null) {
			TempUtil.remove(mainFrame, scene);
		}
		List<Scene> scenes = book.scenes;
		for (int i = 1; i < scenes.size(); i++) {
			if (Objects.equals(scene.id, scenes.get(i).id)) {
				scene = (Scene) TempUtil.check(mainFrame, scenes.get(i - 1));
				break;
			}
		}
		return (scene);
	}

	private Scene getNextScene() {
		App.trace("Typist.getNextScene()");
		if (scene != null) {
			TempUtil.remove(mainFrame, scene);
		}
		List<Scene> scenes = book.scenes;
		for (int i = 0; i < scenes.size() - 1; i++) {
			if (Objects.equals(scene.id, scenes.get(i).id)) {
				scene = (Scene) TempUtil.check(mainFrame, scenes.get(i + 1));
				break;
			}
		}
		return (scene);
	}

	private Scene getLastScene() {
		App.trace("Typist.getLastScene()");
		if (scene != null) {
			TempUtil.remove(mainFrame, scene);
		}
		List<Scene> scenes = book.scenes;
		scene = (Scene) TempUtil.check(mainFrame, scenes.get(scenes.size() - 1));
		return (scene);
	}

	public int askModified() {
		App.trace("Typist.askModified()");
		if (scene != null) {
			boolean change=false;
			if (panelEdit.isTextChanged()) {
				if (panelEdit.getText().length() > 32767) {
					String str = I18N.getMsg("editor.text_too_large");
					JOptionPane.showMessageDialog(
							this,
							str,
							I18N.getMsg("editor"),
							JOptionPane.ERROR_MESSAGE);
					return (JOptionPane.CANCEL_OPTION);
				}
				modified = true;
				scene.writing=(panelEdit.getText());
			}
		}
		if (modified) {
			final Object[] options = {I18N.getMsg("z.save"),
				I18N.getMsg("z.discard"),
				I18N.getMsg("z.cancel")};
			int n = JOptionPane.showOptionDialog(
					this,
					I18N.getMsg("z.save_or_discard") + "\n\n"
					+ scene.name + ": "
					+ scene.toString() + "\n\n",
					I18N.getMsg("z.changes"),
					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
			switch (n) {
				case JOptionPane.CANCEL_OPTION:
					return (JOptionPane.CANCEL_OPTION);
				case JOptionPane.YES_OPTION:
					saveScene();
					break;
				case JOptionPane.NO_OPTION:
					break;
				default:
					break;
			}
		}
		return(JOptionPane.YES_OPTION);
	}

	private boolean isSceneFirst() {
		App.trace("Typist.isSceneFirst()");
		if (scene == null) {
			return (true);
		}
		return (false);
	}

	private boolean isSceneLast() {
		//App.trace("Typist.isSceneLast()");
		if (scene == null) {
			return (true);
		}
		return (false);
	}

	private boolean newScene() {
		//App.trace("Typist.newScene()");
		boolean rc = false;
		Scene s = new Scene();
		s.chapter=(scene.chapter);
		boolean editor = Editor.show(mainFrame, s, null);
		TempUtil.remove(mainFrame, s);
		if (!editor) {
			refreshScenes(scene);
		}
		return (rc);
	}

	@SuppressWarnings("unchecked")
	private void refreshScenes(Scene s) {
		//App.trace("Typist.refreshScenes(scene="+(scene!=null?scene.name:"null")+")");
		cbEntityInit = true;
		DefaultComboBoxModel combo = (DefaultComboBoxModel) cbEntity.getModel();
		combo.removeAllElements();
		List<Scene> scenes = book.scenes;
		scenes.forEach((entity) -> {
			combo.addElement(entity);
		});
		cbEntityInit = false;
	}

	private void changeChapter() {
		//App.trace("Typist.changeChapter()");
		ChapterSelectDlg dlg = new ChapterSelectDlg(mainFrame, scene.chapter);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			Chapter chapter = dlg.getSelectedChapter();
			scene.chapter=(chapter);
			btChapter.setText(chapter.name);
			setModified();
		}
	}

	private void changeSceneTitle() {
		//App.trace("Typist.cgangeSceneTitle()");
		String s = (String) JOptionPane.showInputDialog(
				this, "", I18N.getMsg("z.name"), JOptionPane.PLAIN_MESSAGE, null, null, scene.name);
		if ((s != null) && (s.length() > 0)) {
			scene.setName(s);
			btTitle.setText(s);
			setModified();
		}
	}

	private void saveScene() {
		//App.trace("Typist.save()");
		mainFrame.getBookController().updateEntity(scene);
		toRefresh = true;
	}

	void setModified() {
		//App.trace("Typist.setModified()");
		modified = true;
	}

	public void closeScene() {
		//App.trace("Typist.closeScene()");
		if (scene != null) {
			TempUtil.remove(mainFrame, scene);
		}
	}

	private void close() {
		//App.trace("Typist.close()");
		closeScene();
		mainFrame.activateTypist();
	}

	public void tempSave() {
		//App.trace("Typist.tempSave()");
		scene.writing=(panelEdit.getText());
		TempUtil.write(mainFrame, scene);
	}

}
