/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.typist;

import desk.dialog.NotesDlg;
import desk.dialog.ListEntitiesDlg;
import db.entity.Book;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.dialog.edit.EditorHtml;
import db.I18N;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class TypistInfo extends JPanel {

	public Scene scene;
	public boolean modified;
	private JPanel pPersons;
	private JPanel pLocations;
	private JPanel pItems;
	private JPanel pPlots;
	private JEditorPane lbNotes;
	private final MainFrame mainFrame;
	private final Typist parentPanel;
	private final Book book;

	public TypistInfo(Typist m, Scene s) {
		parentPanel=m;
		mainFrame = m.getMainFrame();
		book=m.book;
		scene = s;
		init();
		if (scene != null) {
			refreshInfo(false);
		}
		modified = false;
	}

	public void init() {
		setLayout(new MigLayout("ins 2"));
		add(new JLabel(" "), "wrap");

		JButton btEdit = initButton("person");
		btEdit.setPreferredSize(new Dimension(Integer.MAX_VALUE, Integer.MIN_VALUE));
		add(btEdit, "growx,wrap");
		pPersons = new JPanel();
		pPersons.setLayout(new MigLayout("ins 0"));
		add(pPersons, " growx, wrap");

		btEdit = initButton("location");
		add(btEdit, "growx,wrap");
		pLocations = new JPanel();
		pLocations.setLayout(new MigLayout("ins 0,wrap"));
		add(pLocations, " growx, wrap");

		btEdit = initButton("item");
		add(btEdit, "growx,wrap");
		pItems = new JPanel();
		pItems.setLayout(new MigLayout("ins 0,wrap"));
		add(pItems, " growx, wrap");

		btEdit = initButton("plot");
		add(btEdit, "growx,wrap");
		pPlots = new JPanel();
		pPlots.setLayout(new MigLayout("ins 0,wrap"));
		add(pPlots, " growx, wrap");

		btEdit = initButton("z.notes");
		add(btEdit, "growx,wrap");
		lbNotes=new JEditorPane("text/html","");
		lbNotes.setEditable(false);
		lbNotes.setBackground(null);
		//add(lbNotes, "growx, wrap");
		JScrollPane scroller=new JScrollPane(lbNotes);
		add(scroller, "growy, growx, wrap");
	}

	private JButton initButton(String entity) {
		JButton bt = new JButton(IconUtil.getIcon("small/edit"));
		bt.setText(I18N.getMsg(entity));
		bt.setToolTipText(I18N.getMsg(entity + ".edit"));
		bt.setMargin(new Insets(0, 0, 0, 0));
		bt.setHorizontalAlignment(SwingConstants.LEADING);
		bt.setHorizontalTextPosition(SwingConstants.LEADING);
		switch (entity) {
			case "person":
				bt.addActionListener((ActionEvent evt) -> {
					btPersonAction();
				});
				break;
			case "location":
				bt.addActionListener((ActionEvent evt) -> {
					btLocationAction();
				});
				break;
			case "item":
				bt.addActionListener((ActionEvent evt) -> {
					btItemAction();
				});
				break;
			case "plot":
				bt.addActionListener((ActionEvent evt) -> {
					btPlotAction();
				});
				break;
			case "notes":
				bt.addActionListener((ActionEvent evt) -> {
					btNotesAction();
				});
				break;
		}
		return (bt);
	}

	private void btPersonAction() {
		ListEntitiesDlg dlg = new ListEntitiesDlg(mainFrame, scene, Book.TYPE.PERSON);
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.persons=(dlg.getPersons());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btLocationAction() {
		ListEntitiesDlg dlg = new ListEntitiesDlg(mainFrame, scene, Book.TYPE.LOCATION);
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.locations=(dlg.getLocations());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btItemAction() {
		ListEntitiesDlg dlg = new ListEntitiesDlg(mainFrame, scene, Book.TYPE.ITEM);
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.items=(dlg.getItems());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btPlotAction() {
		ListEntitiesDlg dlg = new ListEntitiesDlg(mainFrame, scene, Book.TYPE.PLOT);
		dlg.setModal(true);
		dlg.setVisible(true);
		if (!dlg.isCanceled()) {
			scene.plots=(dlg.getPlots());
			refreshInfo(true);
			modified = true;
		}
	}

	private void btNotesAction() {
		EditorHtml panel = new EditorHtml(mainFrame, true,false,false,0,0);
		panel.setText(scene.getNotes());
		NotesDlg dlg = new NotesDlg(mainFrame, panel, "edit");
		if (!dlg.isCanceled()) {
			scene.setNotes(panel.getText());
			refreshInfo(true);
		}
	}

	public void refreshInfo(boolean toUpdate) {
		pPersons.removeAll();
		if (scene.persons.size() > 0) {
			pPersons.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Person p : scene.persons) {
				pPersons.add(new JLabel(p.getFullName()), "wrap");
			}
		} else {
			pPersons.setBorder(null);
		}

		pLocations.removeAll();
		if (scene.locations.size() > 0) {
			pLocations.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Location p : scene.locations) {
				pLocations.add(new JLabel(p.name), "wrap");
			}
		} else pLocations.setBorder(null);

		pItems.removeAll();
		if (scene.items.size() > 0) {
			pItems.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Item p : scene.items) {
				pItems.add(new JLabel(p.name), "wrap");
			}
		} else pItems.setBorder(null);

		pPlots.removeAll();
		if (scene.plots.size() > 0) {
			pPlots.setBorder(javax.swing.BorderFactory.createEtchedBorder());
			for (Plot p : scene.plots) {
				pPlots.add(new JLabel(p.name), "wrap");
			}
		} else pPlots.setBorder(null);

		String text=HtmlUtil.htmlToText(scene.getNotes());
		lbNotes.setText(scene.getNotes());
		if (!text.isEmpty()) {
			lbNotes.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		} else lbNotes.setBorder(null);
		lbNotes.setCaretPosition(0);
		this.revalidate();
		if (toUpdate) {
			parentPanel.setModified();
		}
	}

	void refreshInfo(Scene s) {
		scene = s;
		modified = false;
		refreshInfo(false);
	}

	public MainFrame getMainFrame() {
		return (((Typist)getParent()).getMainFrame());
	}

}
