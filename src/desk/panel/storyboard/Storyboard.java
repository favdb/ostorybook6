/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.storyboard;

import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.panel.AbstractScrollPanel;
import desk.tools.swing.SwingUtil;
import desk.view.SbView;
import desk.view.ViewUtil;
import db.I18N;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeEvent;
import java.util.Arrays;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ChangeEvent;
import lib.infonode.docking.View;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class Storyboard extends AbstractScrollPanel implements ActionListener, Printable, MouseWheelListener {

	public enum ACTION {

		DIRECTION("view.storyboard.direction"),
		PRINT("Storyboard_Print"),
		REFRESH("Storyboard_Refresh"),
		SHOW_ENTITY("Storyboard_ShowEntity"),
		NONE("None");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	private PrefViewStoryboard param;

	public Storyboard(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
		//App.trace("Storyboard.init()");
		param = App.getInstance().getPreferences().storyboard;
	}

	@Override
	public void initUi() {
		//App.trace("Storyboard.initUi()");
		if (this.getComponentCount() > 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("flowy, ins 0"));

		add(initToolbar(WITH_PART), "wrap, span,growx");
		toolbar.add(initCheckBox(ACTION.DIRECTION.toString(), param.direction));
		panel = new JPanel(new MigLayout("", "", "[top]"));
		panel.setBackground(SwingUtil.getBackgroundColor());
		scroller = new JScrollPane(panel);
		SwingUtil.setUnitIncrement(scroller);
		SwingUtil.setMaxPreferredSize(scroller);
		add(scroller, "grow");
		refresh();
		registerKeyboardAction();
		panel.addMouseWheelListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		//App.trace("Storyboard.actionPerformed(evt="+evt.toString()+")");
		Object obj = evt.getSource();
		if (obj instanceof JCheckBox) {
			JCheckBox ck=(JCheckBox)obj;
			setDirection(ck.isSelected());
		}
	}
	
	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("Storyboard.modelPropertyChange(evt=" + evt.toString() + ")");
		Object newValue = evt.getNewValue();
		String propName = evt.getPropertyName();
		switch (Ctrl.whichACTION(propName)) {
			case REFRESH: {
				SbView newView = (SbView) newValue;
				SbView view = (SbView) getParent().getParent();
				if (view == newView) {
					refresh();
				}
				return;
			}
			case PRINT: {
				View newView = (View) newValue;
				View view = (View) getParent().getParent();
				if (view == newView) {
					PrinterJob pj = PrinterJob.getPrinterJob();
					pj.setPrintable(this);
					pj.printDialog();
					try {
						pj.print();
					} catch (PrinterException e) {
						System.err.println(Arrays.toString(e.getStackTrace()));
					}
				}
				return;
			}
		}
		switch (getAction(propName)) {
			case DIRECTION:
				setDirection((Boolean) evt.getNewValue());
				return;
			case SHOW_ENTITY:
				if (newValue instanceof Scene) {
					Scene scene = (Scene) newValue;
					ViewUtil.scrollToScene(this, panel, scene);
					return;
				}
				if (newValue instanceof Chapter) {
					Chapter chapter = (Chapter) newValue;
					ViewUtil.scrollToChapter(this, panel, chapter);
					return;
				}
		}
		switch (Ctrl.whichENTITY(propName)) {
			case CHAPTER:
			case PART:
			case SCENE:
				refresh();
				return;
		}
	}

	@Override
	public void refresh() {
		//App.trace("Storyboard.refresh()"+" param.direction="+(param.direction?"true":"false"));
		// must be done before the session is opened
		if (book == null) {
			return;
		}
		Part currentPart = mainFrame.getCurrentPart();

		List<Chapter> chapters;
		if (mainFrame.showAllParts) {
			chapters = Chapter.orderByNumber(book.chapters);
		} else {
			chapters = Chapter.orderByNumber(book.chapters, currentPart);
		}
		panel.removeAll();
		if (chapters.isEmpty()) {
			panel.add(new JLabel(I18N.getMsg("chapters.warning")));
			panel.revalidate();
			panel.repaint();
			return;
		}
		chapters.forEach((chapter) -> {
			StoryboardChapter cp = new StoryboardChapter(mainFrame, chapter);
			panel.add(cp, (param.direction ? "" : "wrap"));
		});
		revalidate();
		repaint();
	}

	@Override
	public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.black);

		int fontHeight = g2.getFontMetrics().getHeight();
		int fontDesent = g2.getFontMetrics().getDescent();

		double pageHeight = pageFormat.getImageableHeight();
		double pageWidth = pageFormat.getImageableWidth();

		g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
		// bottom center
		g2.drawString("Page: " + (pageIndex + 1), (int) pageWidth / 2 - 35, (int) (pageHeight + fontHeight - fontDesent));
		this.paint(g2);

		if (pageIndex < 4) {
			return Printable.PAGE_EXISTS;
		}
		return Printable.NO_SUCH_PAGE;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
	}

	private void setDirection(boolean b) {
		//App.trace("Storyboard.setDirection(b="+(b?"true":"false")+")");
		if (param.direction == b) {
			return;
		}
		param.direction = b;
		param.save();
		refresh();
	}

}
