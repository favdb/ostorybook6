/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2016 FaVdB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.storyboard;

import db.entity.Chapter;
import db.entity.Pov;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.tools.IconButton;
import db.I18N;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class StoryboardSpace extends AbstractPanel implements MouseListener {

	private Chapter chapter = null;
	private AbstractAction newSceneAction;

	public StoryboardSpace(MainFrame mainFrame, Chapter chapter) {
		super(mainFrame);
		this.chapter = chapter;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		App.trace("StoryboardSpace.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		if ("pov_Update".equals(propName)) {
			refresh();
		}
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setFocusable(true);
		setMinimumSize(new Dimension(150, 150));
		addMouseListener(this);

		Color color = Color.gray;
		MigLayout layout = new MigLayout("fill", "[center]", "[center]");
		setLayout(layout);
		setBorder(BorderFactory.createLineBorder(color, 2));
		setOpaque(false);

		IconButton btNewScene = new IconButton("small/plus", getNewAction());
		btNewScene.setFlat();
		btNewScene.setToolTipText(I18N.getMsg("scene.new"));
		add(btNewScene, "ax center");
	}

	private AbstractAction getNewAction() {
		if (newSceneAction == null) {
			newSceneAction = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					int n=Scene.getSceneLastNumber(book.scenes)+1;
					Pov pov=null;
					if (!book.povs.isEmpty()) pov=book.povs.get(0);
					Scene scene = new Scene(-1L,null,chapter,pov,n);
					mainFrame.getBookController().editEntity(scene);
				}
			};
		}
		return newSceneAction;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			newSceneAction.actionPerformed(null);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	public Chapter getChapter() {
		return chapter;
	}
}
