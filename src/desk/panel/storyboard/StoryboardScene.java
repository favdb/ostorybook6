/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2016 FaVdB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.storyboard;

import db.entity.Scene;
import db.entity.Status;
import desk.action.EntityAction;
import desk.app.MainFrame;
import desk.model.Ctrl;
import desk.panel.AbstractGradientPanel;
import desk.panel.linkspanel.LinksItem;
import desk.panel.linkspanel.LinksPerson;
import desk.tools.EntityUtil;
import desk.tools.IconButton;
import db.I18N;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.ListUtil;

@SuppressWarnings("serial")
public class StoryboardScene extends AbstractGradientPanel {

	private final String CN_UPPER_PANEL = "upperPanel";

	private JPanel upperPanel;
	private JLabel lbStatus;
	private JLabel lbInformative;
	private JLabel lbSceneNo;
	private JLabel lbTime;

	protected Scene scene;
	protected AbstractAction newAction;

	protected IconButton btNew;
	protected IconButton btEdit;
	protected IconButton btDelete;

	public StoryboardScene(MainFrame mainFrame, Scene scene) {
		super(mainFrame, true, Color.white, scene.pov.getJColor());
		this.scene = scene;
		initAll();
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	protected AbstractAction getNewAction() {
		if (newAction == null) {
			newAction = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					//BookController ctrl = mainFrame.getBookController();
					Scene newScene = new Scene();
					newScene.pov=(scene.pov);
					newScene.date=(scene.date);
					if (scene.hasChapter()) {
						newScene.chapter=(scene.chapter);
					}
					mainFrame.showEditorAsDialog(newScene);
				}
			};
		}
		return newAction;
	}

	protected IconButton getEditButton() {
		if (btEdit != null) {
			return btEdit;
		}
		btEdit = new IconButton("small/edit", EntityAction.edit(mainFrame, scene, false));
		btEdit.setText("");
		btEdit.setSize32x20();
		btEdit.setToolTipText(I18N.getMsg("edit"));
		return btEdit;
	}

	protected IconButton getDeleteButton() {
		if (btDelete != null) {
			return btDelete;
		}
		btDelete = new IconButton("small/delete", EntityAction.delete(mainFrame, scene));
		btDelete.setText("");
		btDelete.setSize32x20();
		btDelete.setToolTipText(I18N.getMsg("z.delete"));
		return btDelete;
	}

	protected IconButton getNewButton() {
		if (btNew != null) {
			return btNew;
		}
		btNew = new IconButton("small/new", getNewAction());
		btNew.setSize32x20();
		btNew.setToolTipText(I18N.getMsg("z.new"));
		return btNew;
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("StoryboardScene.modelPropertyChange(propName="+evt.getPropertyName()+") for scene="+scene.id);
		String propName = evt.getPropertyName();
		switch (Ctrl.whichENTITY(propName)) {
			case ITEM:
			case PERSON:
			case POV:
			case SCENE:
				refresh();
				break;
		}

	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		refresh();
	}

	@Override
	public void refresh() {
		//App.trace("StoryboardScene.refresh() for scene="+scene.id);
		MigLayout layout = new MigLayout("fill,flowy,insets 0", "", "");
		setLayout(layout);
		//setPreferredSize(new Dimension(20, 20));
		setComponentPopupMenu(EntityUtil.createPopupMenu(mainFrame, scene));

		removeAll();

		// person links
		LinksPerson linksPerson = new LinksPerson(mainFrame, scene, "01");

		// location links
		//LinksLocation linkspanellocation = new LinksLocation(mainFrame, scene);
		JLabel lbLocation = new JLabel(" ");
		if (!scene.locations.isEmpty()) {
			lbLocation.setText(scene.locations.get(0).name);
			if (scene.locations.size()>1) {
				List<String> locs=new ArrayList<>();
				scene.locations.forEach((location)->{
					locs.add(location.name);
				});
				lbLocation.setToolTipText(ListUtil.join(locs,", "));
			}
		}

		// item links
		LinksItem linksItem = new LinksItem(mainFrame, scene);

		// button new
		btNew = getNewButton();
		btNew.setSize20x20();
		// btNew.setName(COMP_NAME_BT_NEW);

		// button remove
		btDelete = getDeleteButton();
		btDelete.setSize20x20();
		// btDelete.setName(COMP_NAME_BT_REMOVE);

		// button edit
		btEdit = getEditButton();
		btEdit.setSize20x20();
		// btEdit.setName(COMP_NAME_BT_EDIT);

		// chapter and scene number
		lbSceneNo = new JLabel("", SwingConstants.LEFT);
		lbSceneNo.setText("n°" + scene.getChapterSceneNo(false));
		lbSceneNo.setToolTipText(scene.getChapterSceneToolTip());

		// status
		lbStatus = new JLabel(Status.getIcon(scene.status));
		lbStatus.setToolTipText(Status.getMsg(scene.status));

		// informational
		lbInformative = new JLabel(IconUtil.getIcon("small/empty"));
		if (scene.informative) {
			lbInformative.setIcon(IconUtil.getIcon("small/info"));
			lbInformative.setToolTipText(I18N.getMsg("scene.informative"));
		}

		// scene time
		lbTime = new JLabel();
		if (scene.hasDate()) {
			if (scene.date != null) {
				lbTime.setText(scene.date.getDateTime());
			}
		} else {
			lbTime.setText(" ");
		}

		// title
		JLabel lbTitle = new JLabel(scene.name);
		lbTitle.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		lbTitle.setToolTipText(getToolTitle());
		lbTitle.setMinimumSize(new Dimension(16*8,16));
		lbTitle.setMaximumSize(new Dimension(16*8,16));

		// layout
		// button panel
		JPanel buttonPanel = new JPanel(new MigLayout("flowy,insets 0"));
		buttonPanel.setName("buttonpanel");
		buttonPanel.setOpaque(false);
		buttonPanel.add(btEdit);
		buttonPanel.add(btDelete);
		buttonPanel.add(btNew);

		JLabel lbTop = new JLabel(IconUtil.getIcon("stamp"));
		JLabel lbBottom = new JLabel(IconUtil.getIcon("stamp"));

		upperPanel = new JPanel(new MigLayout("insets 0 0 0 0", "[grow][][][]", ""));
		upperPanel.setName(CN_UPPER_PANEL);
		upperPanel.setOpaque(false);
		upperPanel.add(lbTop, "spanx, center, wrap");//1
		upperPanel.add(lbTitle, "grow,spanx 2");//2
		upperPanel.add(lbStatus);
		upperPanel.add(lbInformative);
		upperPanel.add(buttonPanel, "spany 5,wrap");//3
		upperPanel.add(linksPerson, "spanx,growx,wrap");
		upperPanel.add(lbLocation, "spanx,grow,wrap");//5
		upperPanel.add(linksItem, "spanx,grow,wrap");//6
		upperPanel.add(lbTime, "spanx,wrap");//7

		// main panel
		add(upperPanel);
		/*upperPanel.*/add(lbBottom, "span, center");//8

		revalidate();
		repaint();
	}

	protected StoryboardScene getThis() {
		return this;
	}

	private String getToolTitle() {
		StringBuilder b = new StringBuilder();
		b.append("<html><p>");
		b.append(I18N.getMsg("scene")).append(" n°").append(" <b>")
				.append(scene.number).append("</b><br>");
		b.append(scene.name).append("<br>");
		if (!scene.getNotes().isEmpty()) {
			b.append(I18N.getColonMsg("notes")).append(scene.getNotes()).append("<br>");
		}
		b.append("</p></html>");
		return (b.toString());
	}

}
