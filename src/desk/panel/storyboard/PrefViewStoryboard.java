/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.panel.storyboard;

import desk.app.pref.AppPref;
import db.I18N;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PrefViewStoryboard {

	public boolean direction = true;
	private final AppPref pref;

	public PrefViewStoryboard(AppPref pref) {
		this.pref=pref;
		load();
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.storyboard", 3));
		b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("view.storyboard.direction", direction));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.storyboard")).append("<p>\n");
		b.append(TextUtil.getAttribute("view.storyboard.direction", direction));
		return (b.toString());
	}
	
	private void load() {
		direction=pref.getBoolean(AppPref.Key.VIEW_STORYBOARD.toString());
	}
	
	public void save() {
		pref.setBoolean(AppPref.Key.VIEW_STORYBOARD, direction);
	}

}
