/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2016 FaVdB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel.storyboard;

import db.entity.Chapter;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

@SuppressWarnings("serial")
public class StoryboardChapter extends AbstractPanel {

	private final Chapter chapter;
	private JLabel lbChapter;
	private PrefViewStoryboard param;

	public StoryboardChapter(MainFrame mainFrame, Chapter chapter) {
		super(mainFrame);
		this.chapter = chapter;
		initAll();
	}

	@Override
	public void init() {
		param = App.getInstance().getPreferences().storyboard;
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("insets 0 0 0 0", "[]", "[]"));
		setOpaque(false);
		refresh();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("StoryboardChapter.modelPropertyChange(evt="+evt.toString()+")");
		String propName = evt.getPropertyName();
		switch (propName) {
			case "chapter_Update":
				refresh();
				break;
		}
	}

	public Chapter getChapter() {
		return chapter;
	}

	public Scene getScene(Long id) {
		return (book.getScene(id));
	}
	
	@Override
	public void refresh() {
		//App.trace("StoryboardChapter.refresh() for chapter="+chapter.id);
		this.removeAll();
		lbChapter=new JLabel(chapter.name);
		lbChapter.setToolTipText("<html>"+chapter.getDescription()+"</html>");
		lbChapter.setIcon(IconUtil.getIcon("small/chapter"));
		lbChapter.setBackground(new Color(240, 240, 240));
		lbChapter.setOpaque(true);
		lbChapter.setHorizontalAlignment(SwingConstants.LEFT);
		add(lbChapter, "wrap");
		List<Scene> scenes = book.listScenes(chapter);
		if (scenes.isEmpty()) {
			StoryboardSpace spacePanel = new StoryboardSpace(mainFrame, chapter);
			add(spacePanel, "grow");
		} else {
			JPanel colPanel = new JPanel(new MigLayout("insets 0 0 0 0", "[]", "[]"));
			colPanel.setOpaque(false);
			scenes.forEach((scene)-> {
				StoryboardScene sp = new StoryboardScene(mainFrame, scene);
				if (param.direction) colPanel.add(sp);
				else colPanel.add(sp,"wrap");
			});
			add(colPanel);
		}
		revalidate();
		repaint();
	}
}
