/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.panel;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import db.entity.Scene;
import desk.app.MainFrame;
import desk.action.EntityAction;
import desk.tools.IconButton;
import db.I18N;
import java.util.Objects;

@SuppressWarnings("serial")
abstract public class AbstractScenePanel extends AbstractGradientPanel {
	protected Scene scene;

	protected AbstractAction newAction;

	protected IconButton btNew;
	protected IconButton btEdit;
	protected IconButton btDelete;

	public AbstractScenePanel(MainFrame mainFrame, Scene scene) {
		super(mainFrame);
		this.scene = scene;
	}

	public AbstractScenePanel(MainFrame mainFrame, Scene scene,
			boolean showBgGradient, Color startBgcolor, Color endBgColor) {
		super(mainFrame, showBgGradient, startBgcolor, endBgColor);
		this.scene = scene;
	}

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	protected AbstractAction getNewAction() {
		if (newAction == null) {
			newAction = new AbstractAction() {
				@Override
				public void actionPerformed(ActionEvent e) {
					//BookController ctrl = mainFrame.getBookController();
					Scene newScene = new Scene();
					newScene.pov=(scene.pov);
					newScene.date=(scene.date);
					if (scene.hasChapter()) {
						newScene.chapter=scene.chapter;
					}
					mainFrame.showEditorAsDialog(newScene);
				}
			};
		}
		return newAction;
	}

	protected IconButton getEditButton() {
		if (btEdit != null) {
			return btEdit;
		}
		btEdit = new IconButton("small/edit", EntityAction.edit(mainFrame, scene,false));
		btEdit.setText("");
		btEdit.setSize32x20();
		btEdit.setToolTipText(I18N.getMsg("z.edit"));
		return btEdit;
	}

	protected IconButton getDeleteButton() {
		if (btDelete != null) {
			return btDelete;
		}
		btDelete = new IconButton("small/delete", EntityAction.delete(mainFrame, scene));
		btDelete.setText("");
		btDelete.setSize32x20();
		btDelete.setToolTipText(I18N.getMsg("z.delete"));
		return btDelete;
	}

	protected IconButton getNewButton() {
		if (btNew != null) {
			return btNew;
		}
		btNew = new IconButton("small/new", getNewAction());
		btNew.setSize32x20();
		btNew.setToolTipText(I18N.getMsg("z.new"));
		return btNew;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof AbstractScenePanel)) {
			return false;
		}
		AbstractScenePanel asp = (AbstractScenePanel) other;
		if (asp.getScene() == null || scene == null) {
			return false;
		}
		return asp.getScene().id.equals(scene.id);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + Objects.hashCode(this.scene);
		return hash;
	}
	
}
