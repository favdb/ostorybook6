/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.memoria.PrefViewMemoria;
import desk.view.SbView;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import lib.miginfocom.swing.MigLayout;


/**
 *
 * @author favdb
 */
public class OptMemoria extends AbstractPanel {
	private JRadioButton rbBalloon;
	private PrefViewMemoria param;
	
	public OptMemoria(MainFrame m) {
		super(m);
		initAll();
	}
	
	@Override
	public void init() {
		param=App.getInstance().getPreferences().memoria;
	}

	@Override
	public void initUi() {
		// balloon or tree layout
		JLabel lbPres = new JLabel(I18N.getColonMsg("view.memoria.presentation"));
		ButtonGroup bgPresentation = new ButtonGroup();
		rbBalloon = new JRadioButton(I18N.getMsg("view.memoria.balloon"));
		rbBalloon.setSelected(param.balloon);
		bgPresentation.add(rbBalloon);
		JRadioButton rbTree = new JRadioButton(I18N.getMsg("view.memoria.tree"));
		bgPresentation.add(rbTree);
		if (!param.balloon) {
			rbTree.setSelected(true);
		}

		// layout
		setLayout(new MigLayout("wrap"));
		add(lbPres);
		add(rbBalloon, "split 2");
		add(rbTree);
	}

	public void apply() {
		param.balloon=rbBalloon.isSelected();
		param.save();
			mainFrame.getBookModel().fireAgain(SbView.VIEWID.VIEW_MEMORIA);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
