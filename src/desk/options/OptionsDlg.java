/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import javax.swing.JTabbedPane;
import lib.miginfocom.swing.MigLayout;
import db.I18N;
import desk.app.MainFrame;
import desk.dialog.AbstractDialog;
import desk.view.SbView;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import resources.MainResources;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class OptionsDlg extends AbstractDialog {

	private final String sbView;
	OptWork workPanel;
	OptChrono chronoPanel;
	OptManage managePanel;
	OptMemo memoPanel;
	OptMemoria memoriaPanel;
	OptReading readingPanel;

	public OptionsDlg(MainFrame m) {
		super(m);
		sbView = null;
		initAll();
	}

	public OptionsDlg(MainFrame m, String v) {
		super(m);
		sbView = v;
		initAll();
	}

	public static void show(MainFrame m) {
		//App.trace("OptionsDlg.show(mainFrame)");
		OptionsDlg dlg = new OptionsDlg(m);
		dlg.setVisible(true);
	}

	public static void show(MainFrame m, String v) {
		//App.trace("OptionsDlg.show(mainFrame,"+v+")");
		OptionsDlg dlg = new OptionsDlg(m, v);
		dlg.setVisible(true);
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		//App.trace("OptionsDlg.initUi()");
		workPanel = new OptWork(mainFrame);
		chronoPanel = new OptChrono(mainFrame);
		managePanel = new OptManage(mainFrame);
		memoPanel = new OptMemo(mainFrame);
		memoriaPanel = new OptMemoria(mainFrame);
		readingPanel = new OptReading(mainFrame);

		//layout
		setLayout(new MigLayout("wrap,fill"));
		setTitle(I18N.getMsg("options"));
		setIconImage(IconUtil.getIconImage(MainResources.class, "icon"));
		if (sbView == null) {
			JTabbedPane tabbed = new JTabbedPane();
			tabbed.add(I18N.getMsg("view.work"), workPanel);
			tabbed.add(I18N.getMsg("view.chrono"), chronoPanel);
			tabbed.add(I18N.getMsg("view.manage"), managePanel);
			tabbed.add(I18N.getMsg("memo"), memoPanel);
			tabbed.add(I18N.getMsg("view.memoria"), memoriaPanel);
			tabbed.add(I18N.getMsg("view.reading"), readingPanel);
			add(tabbed, "wrap");
		} else {
			switch (SbView.getVIEWID(sbView)) {
				case VIEW_WORK:
					this.setTitle(I18N.getMsg("view.book.options"));
					add(workPanel, "wrap");
					break;
				case VIEW_CHRONO:
					this.setTitle(I18N.getMsg("view.chrono.options"));
					add(chronoPanel, "wrap");
					break;
				case VIEW_MANAGE:
					this.setTitle(I18N.getMsg("view.manage.options"));
					add(managePanel, "wrap");
					break;
				/*case VIEW_MEMO:
					this.setTitle(I18N.getMsg("view.memo.options"));
					add(memoPanel, "wrap");
					break;*/
				case VIEW_MEMORIA:
					this.setTitle(I18N.getMsg("view.memoria.options"));
					add(memoriaPanel, "wrap");
					break;
				case VIEW_READING:
					this.setTitle(I18N.getMsg("view.reading.options"));
					add(readingPanel, "wrap");
					break;
				default:
					break;
			}
		}
		this.addOkCancel();
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}

	@Override
	public AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (sbView == null) {
					workPanel.apply();
					chronoPanel.apply();
					managePanel.apply();
					memoPanel.apply();
					memoriaPanel.apply();
					readingPanel.apply();
				} else {
					switch (sbView) {
						case "view.book":
							workPanel.apply();
							break;
						case "view.chrono":
							chronoPanel.apply();
							break;
						case "view.manage":
							managePanel.apply();
							break;
						case "view.memo":
							memoPanel.apply();
							break;
						case "view.memoria":
							memoriaPanel.apply();
							break;
						case "view.reading":
							readingPanel.apply();
							break;
					}
				}
				dispose();
			}
		};
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
