/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.reading.PrefViewReading;
import desk.view.SbView;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class OptReading extends AbstractPanel {

	public JCheckBox ckPartNumber;
	public JCheckBox ckPartRoman;
	public JCheckBox ckPartTitle;
	public JCheckBox ckChapterDateLoc;
	public JCheckBox ckChapterNumber;
	public JCheckBox ckChapterRoman;
	public JCheckBox ckChapterTitle;
	public JCheckBox ckSceneDidascalie;
	public JCheckBox ckSceneSeparator;
	public JCheckBox ckSceneTitle;
	private PrefViewReading param;
	private JComboBox cbSize;

	public OptReading(MainFrame m) {
		super(m);
		initAll();
	}

	@Override
	public void init() {
		param = App.getInstance().getPreferences().reading;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		// font size
		JLabel lbFontSize = new JLabel(I18N.getColonMsg("view.reading.font_size"));
		cbSize = new JComboBox();
		cbSize.setName("view.reading.font_size");
		int fnt[] = {8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 24, 28, 32, 36, 40};
		for (int i : fnt) {
			cbSize.addItem(Integer.toString(i));
		}
		cbSize.setSelectedItem(Integer.toString(param.fontSize));

		ckPartTitle = new JCheckBox(I18N.getMsg("options.titles"));
		ckPartNumber = new JCheckBox(I18N.getMsg("options.numbers"));
		ckPartRoman = new JCheckBox(I18N.getMsg("options.roman"));
		ckChapterTitle = new JCheckBox(I18N.getMsg("options.titles"));
		ckChapterNumber = new JCheckBox(I18N.getMsg("options.numbers"));
		ckChapterRoman = new JCheckBox(I18N.getMsg("options.roman"));
		ckSceneTitle = new JCheckBox(I18N.getMsg("options.titles"));
		ckChapterDateLoc = new JCheckBox(I18N.getMsg("options.date_loc"));
		ckSceneDidascalie = new JCheckBox(I18N.getMsg("options.didascalie"));
		ckSceneSeparator = new JCheckBox(I18N.getMsg("options.separator"));

		ckPartTitle.setSelected(param.partTitle);
		ckPartNumber.setSelected(param.partNumber);
		ckPartRoman.setSelected(param.partRoman);
		ckChapterTitle.setSelected(param.chapterTitle);
		ckChapterNumber.setSelected(param.chapterNumber);
		ckChapterRoman.setSelected(param.chapterRoman);
		ckChapterDateLoc.setSelected(param.chapterDateLoc);
		ckSceneTitle.setSelected(param.sceneTitle);
		ckSceneDidascalie.setSelected(param.sceneDidascalie);
		ckSceneSeparator.setSelected(param.sceneSeparator);

		// layout
		setLayout(new MigLayout("wrap"));
		add(lbFontSize, "split 2");
		add(cbSize);
		JPanel pPart = new JPanel(new MigLayout());
		pPart.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("part")));
		pPart.add(ckPartTitle);
		pPart.add(ckPartNumber);
		pPart.add(ckPartRoman);
		JPanel pChapter = new JPanel(new MigLayout());
		pChapter.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("chapter")));
		pChapter.add(ckChapterTitle);
		pChapter.add(ckChapterNumber);
		pChapter.add(ckChapterRoman);
		pChapter.add(ckChapterDateLoc);
		JPanel pScene = new JPanel(new MigLayout());
		pScene.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("scene")));
		pScene.add(ckSceneTitle);
		pScene.add(ckSceneDidascalie);
		pScene.add(ckSceneSeparator);

		add(pPart);
		add(pChapter);
		add(pScene);

	}

	public boolean apply() {
		param.fontSize = Integer.parseInt((String) cbSize.getSelectedItem());
		param.partTitle = ckPartTitle.isSelected();
		param.partNumber = ckPartNumber.isSelected();
		param.partRoman = ckPartRoman.isSelected();
		param.chapterTitle = ckChapterTitle.isSelected();
		param.chapterNumber = ckChapterNumber.isSelected();
		param.chapterRoman = ckChapterRoman.isSelected();
		param.chapterDateLoc = ckChapterDateLoc.isSelected();
		param.sceneTitle = ckSceneTitle.isSelected();
		param.sceneDidascalie = ckSceneDidascalie.isSelected();
		param.sceneSeparator = ckSceneSeparator.isSelected();
		param.save();
		mainFrame.getBookModel().fireAgain(SbView.VIEWID.VIEW_READING);
		return (true);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
