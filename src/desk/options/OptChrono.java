/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.chrono.PrefViewChrono;
import desk.view.SbView;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.JCheckBox;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class OptChrono extends AbstractPanel {
	private JCheckBox cbDir;
	private JCheckBox cbDiff;
	private PrefViewChrono param;
	
	public OptChrono(MainFrame m) {
		super(m);
		init();
		initUi();
	}
	
	@Override
	public final void init() {
		param=App.getInstance().getPreferences().chrono;
	}

	@Override
	public final void initUi() {
		// layout direction
		cbDir = new JCheckBox();
		cbDir.setName("CbLayoutDirection");
		cbDir.setText(I18N.getMsg("view.chrono.direction"));
		cbDir.setOpaque(false);
		cbDir.setSelected(param.direction);

		// show date difference
		cbDiff = new JCheckBox();
		cbDiff.setName("CbDateDifference");
		cbDiff.setText(I18N.getMsg("view.chrono.show_date_diff"));
		cbDiff.setOpaque(false);
		cbDiff.setSelected(param.showDateDiff);

		// layout
		setLayout(new MigLayout("wrap"));
		add(cbDir);
		add(cbDiff);
	}

	public void apply() {
		param.direction=cbDir.isSelected();
		param.showDateDiff=cbDiff.isSelected();
		param.save();
			mainFrame.getBookModel().fireAgain(SbView.VIEWID.VIEW_CHRONO);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
