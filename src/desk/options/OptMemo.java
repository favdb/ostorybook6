/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.memos.PrefViewMemo;
import desk.view.SbView;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class OptMemo extends AbstractPanel {
	private JRadioButton rbLeft;
	private PrefViewMemo param;
	
	public OptMemo(MainFrame m) {
		super(m);
		initAll();
	}
	
	@Override
	public void init() {
		param=App.getInstance().getPreferences().memo;
	}

	@Override
	public void initUi() {
		JLabel label = new JLabel(I18N.getColonMsg("view.memo.layoutdirection"));
		ButtonGroup bg = new ButtonGroup();
		rbLeft = new JRadioButton(I18N.getMsg("view.memo.layoutdirection.left"));
		rbLeft.setName("left");
		if (!param.isLeft) {
			rbLeft.setSelected(true);
		}
		bg.add(rbLeft);
		JRadioButton rbTop = new JRadioButton(I18N.getMsg("view.memo.layoutdirection.top"));
		rbTop.setName("top");
		bg.add(rbTop);
		if (param.isLeft) {
			rbTop.setSelected(true);
		}

		// layout
		setLayout(new MigLayout("wrap"));
		add(label);
		add(rbLeft, "split 2");
		add(rbTop);
	}

	public void apply() {
		param.isLeft=rbLeft.isSelected();
		param.save();
			mainFrame.getBookModel().fireAgain(SbView.VIEWID.VIEW_MEMO);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
