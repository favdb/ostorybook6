/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.work.PrefViewWork;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import javax.swing.JSlider;
import lib.miginfocom.swing.MigLayout;


/**
 *
 * @author favdb
 */
public class OptWork extends AbstractPanel {
	private JSlider sliderWidth;
	private JSlider sliderHeight;
	private PrefViewWork param;

	public OptWork(MainFrame m) {
		super(m);
		initAll();
	}
	
	@Override
	public void init() {
		param=App.getInstance().getPreferences().book;
		//App.trace("param="+param.toText());
	}

	@Override
	public void initUi() {
		JLabel lbZoom = new JLabel(I18N.getColonMsg("view.work.width"));
		sliderWidth = new JSlider(JSlider.HORIZONTAL, param.WIDTH_MIN, param.WIDTH_MAX, param.width);
		sliderWidth.setName("ZoomSlider");
		sliderWidth.setMajorTickSpacing(5);
		sliderWidth.setMinorTickSpacing(1);
		sliderWidth.setOpaque(false);
		sliderWidth.setPaintTicks(true);
		// height factor
		JLabel lbHeightFactor = new JLabel(I18N.getColonMsg("view.work.height"));
		sliderHeight = new JSlider(JSlider.HORIZONTAL, param.HEIGHT_MIN, param.HEIGHT_MAX, param.height);
		sliderHeight.setName("HeightSlider");
		sliderHeight.setMajorTickSpacing(5);
		sliderHeight.setMinorTickSpacing(1);
		sliderHeight.setOpaque(false);
		sliderHeight.setPaintTicks(true);

		// layout
		setLayout(new MigLayout());
		add(lbZoom);
		add(sliderWidth, "growx,wrap");
		add(lbHeightFactor);
		add(sliderHeight, "growx");
	}

	public void apply() {
		if (!sliderWidth.getValueIsAdjusting() && !sliderHeight.getValueIsAdjusting()) {
			int val1 = sliderWidth.getValue();
			param.width=val1;
			int val2 = sliderHeight.getValue();
			param.height=val2;
			param.save();
			mainFrame.getBookModel().fireAgain(VIEWID.VIEW_WORK);
		}
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
}
