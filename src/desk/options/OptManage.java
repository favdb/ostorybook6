/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.options;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.manage.PrefViewManage;
import desk.view.SbView;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JSlider;
import lib.miginfocom.swing.MigLayout;


/**
 *
 * @author favdb
 */
public class OptManage extends AbstractPanel {
	private JCheckBox ckHide;
	private PrefViewManage param;
	private JSlider sliderColumns;
	
	public OptManage(MainFrame m) {
		super(m);
		init();
		initUi();
	}
	
	@Override
	public final void init() {
		param=App.getInstance().getPreferences().manage;
	}

	@Override
	public final void initUi() {
		//App.trace(param.toText());
		// columns
		JLabel lbColumns = new JLabel(I18N.getColonMsg("view.manage.col"));
		sliderColumns = new JSlider(JSlider.HORIZONTAL, 1, 20, param.col);
		sliderColumns.setName("ColumnSlider");
		sliderColumns.setMajorTickSpacing(10);
		sliderColumns.setMinorTickSpacing(5);
		sliderColumns.setOpaque(false);
		sliderColumns.setPaintTicks(true);
		
		ckHide=new JCheckBox(I18N.getMsg("view.manage.unassigned"));
		ckHide.setName("HideUnassigned");
		ckHide.setSelected(param.unassigned);

		// layout
		setLayout(new MigLayout("wrap"));
		add(lbColumns);
		add(sliderColumns/*, "growx"*/);
		add(ckHide);
	}

	public void apply() {
		if (!sliderColumns.getValueIsAdjusting()) {
			int val = sliderColumns.getValue();
			param.col=val;
		}
		param.unassigned=ckHide.isSelected();
		param.save();
			mainFrame.getBookModel().fireAgain(SbView.VIEWID.VIEW_MANAGE);
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
