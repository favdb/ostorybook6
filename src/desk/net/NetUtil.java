/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.net;

import db.entity.Location;
import desk.app.App;
import desk.app.pref.AppPref;
import db.I18N;

import java.awt.Desktop;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import javax.swing.JOptionPane;

public class NetUtil {

	public final static String DEFAULT_OSM_URL = "http://www.openstreetmap.org";

	public final static String URL_HOME = "http://ostorybook.eu/";
	public final static String URL_CONTACT = URL_HOME + "contact";
	public final static String URL_DOC = URL_HOME + "doc";
	public final static String URL_FAQ = URL_HOME + "faq";
	public final static String URL_BUG = URL_HOME + "bug";
	public final static String URL_VERSION = URL_HOME + "Versions.txt";
	public final static String URL_UPDATE = URL_HOME + "update";
	public final static String URL_DICTIONARY = URL_HOME + "dictionary";

	private static String mapUrl;

	public static void openBrowser(String path) {
		//App.trace("NetUtil.openBrowser("+path+")");
		try {
			Desktop.getDesktop().browse(new URI(path));
		} catch (URISyntaxException | IOException e) {
			App.error("NetUtil.openBrowser(" + path + ")", e);
		}
	}

	public static void openOSM(Location location) {
		String str = String.format("%s,%s,%s", location.city, location.address, location.country);
		if (location.gps != null && !location.gps.isEmpty()) {
			str = "#map=16/" + location.gps;
		} else if (location.hasCity()) {
			str = "search?query=" + location.city;
		} else {
			JOptionPane.showMessageDialog(null,
					I18N.getMsg("location.no_position"),
					I18N.getMsg("show.osm"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		openOSM(str);
	}

	public static void openOSM(String query) {
		//App.trace("NetUtil.openOSM("+query+")");
		try {
			String queryEnc = URLEncoder.encode(query, "UTF-8");
			String path = getOSMUrl() + "/" + queryEnc;
			openBrowser(path);
		} catch (UnsupportedEncodingException e) {
			App.error("NetUtil.openGoogleMap(" + query + ")", e);
		}
	}

	public static String getOSMUrl() {
		//App.trace("NetUtil.getOSMUrl()");
		mapUrl = App.getInstance().getPreferences().getString(AppPref.Key.OSM);
		if (mapUrl == null || mapUrl.isEmpty()) {
			return DEFAULT_OSM_URL;
		}
		return mapUrl;
	}

}
