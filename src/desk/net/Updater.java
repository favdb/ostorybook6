/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.net;

import db.entity.SbCalendar;
import db.entity.SbDate;
import desk.app.App;
import desk.app.pref.AppPref;
import desk.app.MainFrame;
import desk.dialog.BrowserDlg;
import db.I18N;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import javax.swing.SwingUtilities;

/**
 *
 * @author favdb
 */
public class Updater {
	public final static String DEFAULT_DO="0";
	
	public static boolean checkForUpdate(boolean force) {
		//App.trace("Updater.checkForUpdate(".concat(force?"force":"no force")+")");
		String doUpdater = App.getInstance().getPreferences().getString(AppPref.Key.UPDATER_DO);
		boolean toDo=false;
		switch(doUpdater) {
			case "0":
				App.getInstance().getPreferences().setString(AppPref.Key.UPDATER_DO, "2");
				setDateUpdater();
				break;
			case "1":
				break;
			case "2":
				toDo=checkDateUpdater();
				break;
			case "3":
				toDo=true;
				break;
			default:
				break;
		}
		if (toDo || force) {
			try {
				// get version
				URL url = new URL(NetUtil.URL_VERSION);
				String versionStr;
				try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
					String inputLine;
					versionStr = "";
					int c = 0,nc=-1;
					while ((inputLine = in.readLine()) != null) {
						versionStr = inputLine;
						if (inputLine.contains("Versions")) {
							nc = c + 1;
						}
						if (c == nc) {
							break;
						}
						c++;
					}
				}

				// compare version
				int remoteVersion = calculateVersion(versionStr);
				int localVersion = calculateVersion(App.VERSION);
				if (localVersion < remoteVersion) {
					SwingUtilities.invokeLater(() -> {
						String updateUrl = NetUtil.URL_VERSION;
						BrowserDlg.show((MainFrame)(java.awt.Frame)null, updateUrl, I18N.getMsg("updater.title"));
					});
					setDateUpdater();
					return false;
				}
			} catch (SocketException | UnknownHostException e) {
				return true;
			} catch (IOException e) {
				App.error("Updater.checkForUpdate() Exception:", e);
			}
		}
		return true;
	}

	private static int calculateVersion(String str) {
		//App.trace("Updater.calculateVersion("+str+")");
		String[] s = str.split("\\.");
		if (str.contains(":")) {
			String[] x = str.split(":");
			s=x[0].split("\\.");
		}
		if (s.length != 3) {
			//App.trace("Warning version text not recognized: \n" +"s.length="+ s.length+",s="+str+"\n");
			return -1;
		}
		int ret = 0;
		ret += Integer.parseInt(s[0]) * 1000000;
		ret += Integer.parseInt(s[1]) * 1000;
		ret += Integer.parseInt(s[2]);
		return ret;
	}

	private static boolean checkDateUpdater() {
		boolean rc=false;
		String dateLast=App.getInstance().getPreferences().getString(AppPref.Key.UPDATER_LAST, "");
		//App.trace("Updater.checkDateUpdater() last="+dateLast);
		SbDate ddj=SbDate.getToDay();
		if (!"".equals(dateLast)) {
			SbDate ddLast=new SbDate(dateLast);
			//si la différence entre ddLast et ddj est plus grand que 90 jours alors vérif
			long diff=SbDate.daysBetween(new SbCalendar(), ddj, ddLast);
			if (diff>90) {
				App.getInstance().getPreferences().setString(AppPref.Key.UPDATER_LAST, ddj.getDateTime());
				rc=true;
			}
		} else {
			App.getInstance().getPreferences().setString(AppPref.Key.UPDATER_LAST, ddj.getDateTime());
		}
		return(rc);
	}

	private static void setDateUpdater() {
		SbDate ddj=SbDate.getToDay();
		App.getInstance().getPreferences().setString(AppPref.Key.UPDATER_LAST, ddj.getDateTime());
	}

}
