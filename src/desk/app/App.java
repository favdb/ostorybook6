/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app;

import db.DB;
import db.entity.Book;
import db.XmlDB;
import desk.dialog.PropertiesDlg;
import desk.net.Updater;
import desk.tools.FileFilter;
import tools.FileTool;
import desk.tools.FontUtil;
import desk.tools.swing.SwingUtil;
import db.I18N;
import desk.app.pref.AppPref;
import desk.app.pref.BookPref;
import java.awt.Component;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.FontUIResource;
import desk.tools.ExceptionDlg;
import desk.tools.IOTools;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class App extends Component {

	public final static String NAME = "oStorybook",
			VERSION = "6.00.00",
			RELEASE = "21/04/2020",
			FULLNAME = NAME + " " + VERSION,
			COPYRIGHT = "The oStorybook Team",
			COPYRIGHT_YEAR = "2020",
			USER_HOME_DIR = "storybook6";

	private static App instance;
	public static AppPref preferences;
	public static boolean btrace = false;
	public static boolean btest = false;
	private static String i18nFile = "";
	private static String assistant = "internal";

	public static boolean isTrace() {
		return (btrace);
	}

	public static boolean isDev() {
		return (DB.getInstance().isDev());
	}

	private Font fontEditor;
	private Font fontDefault;
	private final List<MainFrame> mainFrames;
	public String format_Date = "yyyy-MM-dd";
	public String format_DateTime = "yyyy-MM-dd HH:mm:ss";
	public String format_Time = "HH:mm:ss";
	private Font monoFont;

	public void setTrace() {
		btrace=!btrace;
		System.out.println((btrace?"Enter trace mode":"End trace mode"));
	}

	private static void showHelp() {
		System.out.println(I18N.getMsg("app.help"));
		System.out.println(I18N.getMsg("app.help.help"));
		System.out.println(I18N.getMsg("app.help.msg"));
		System.out.println(I18N.getMsg("app.help.dev"));
		System.out.println(I18N.getMsg("app.help.tace"));
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		String tempDir = System.getProperty("java.io.tmpdir");
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				if (args[i].equalsIgnoreCase("--help")) {
					showHelp();
					return;
				}
				if (args[i].equalsIgnoreCase("--trace")) {
					App.btrace = true;
					System.out.println("Storybook execution in trace mode");
				}
				if (args[i].equalsIgnoreCase("--dev")) {
					App.btest = true;
					DB.getInstance().setDev();
					System.out.println("Development test");
				}
				if (args[i].equalsIgnoreCase("--msg")) {
					String fileI18N = args[i + 1];
					File f = new File(args[i + 1]);
					if (!f.exists()) {
						fileI18N = args[i + 1] + ".properties";
						f = new File(fileI18N);
						if (!f.exists()) {
							System.out.println("Msg file not exists : " + args[i + 1]);
							fileI18N = "";
						}
					}
					if (!fileI18N.isEmpty()) {
						App.i18nFile = fileI18N;
						System.out.println("Msg file is : " + fileI18N);
						I18N.setFileMessages(i18nFile);
					}
				}
			}
		}
		/*String fn = tempDir + File.separator + "storybook.lck";
		if (!lockInstance(fn)) {
			Object[] options = {I18N.getMsg("running.remove"), I18N.getMsg("cancel")};
			int n = JOptionPane.showOptionDialog(null,
					I18N.getMsg("running.msg"),
					I18N.getMsg("running.title"),
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
			if (n == 0) {
				File file = new File(fn);
				if (file.exists() && file.canWrite()) {
					if (!file.delete()) {
						JOptionPane.showMessageDialog(null, "Delete failed",
								"File\n" + file.getAbsolutePath() + "\ncould not be deleted.",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			return;
		}*/

		SwingUtilities.invokeLater(() -> {
			App app = App.getInstance();
			app.init();
		});
	}

	private App() {
		mainFrames = new ArrayList<>();
	}

	private void init() {
		//App.trace("App.init()");
		preferences = new AppPref();
		format_DateTime = preferences.getString(AppPref.Key.DATETIMEFORMAT);
		String d[] = format_DateTime.split(" ");
		format_Date = d[0];
		format_Time = d[1];
		initI18N();
		assistant = preferences.getString(AppPref.Key.ASSISTANT, "internal");
		try {
			if (preferences.getBoolean(AppPref.Key.FIRST_START, true)) {
				String lang = System.getProperties().getProperty("user.language");
				String country = System.getProperties().getProperty("user.country");
				preferences.setLanguage(lang+"_"+country);
				preferences.setBoolean(AppPref.Key.FIRST_START, false);
			}

			// check for updates
			Updater.checkForUpdate(false);
			// check if last file must be open
			//App.trace("check if last file must be open");
			if (preferences.getBoolean(AppPref.Key.OPEN_LAST_FILE, false)) {
				String str = preferences.getLastFile();
				System.out.println("Open last file: " + str);
				File f = new File(str);
				if (f.exists()) {
					XmlDB dbFile = new XmlDB(str);
					openFile(f, false);
					if (dbFile.isOpened()) {
						return;
					}
				}
			}
			MainFrame mainFrame = new MainFrame();
			SwingUtil.setLookAndFeel();
			fontRestoreDefault();
			addMainFrame(mainFrame);

		} catch (Exception e) {
			error("App.init()", e);
			ExceptionDlg.show("", e);
		}
	}

	private void initI18N() {
		//App.trace("App.initI18N()");
		String loc = preferences.getLanguage();
		Language.LANGUAGE lang;
		try {
			lang = Language.LANGUAGE.valueOf(loc);
		} catch (Exception ex) {
			lang = Language.LANGUAGE.valueOf("en_US");
		}
		Locale locale = lang.getLocale();
		setLocale(locale);
		I18N.initMessages(getLocale());
	}

	public static App getInstance() {
		if (instance == null) {
			instance = new App();
		}
		return instance;
	}

	public void exit() {
		//App.trace("App.exit()");
		if (mainFrames.size() > 0) {
			if (preferences.getBoolean(AppPref.Key.CONFIRM_EXIT, true)) {
				int n = JOptionPane.showConfirmDialog(null,
						I18N.getMsg("z.want.exit"),
						I18N.getMsg("z.exit"),
						JOptionPane.YES_NO_OPTION);
				if (n == JOptionPane.NO_OPTION || n == JOptionPane.CLOSED_OPTION) {
					return;
				}
			}
			saveAll();
		}
		preferences.save();
		System.exit(0);
	}

	public void saveAll() {
//		App.trace("SbApp.saveAll()");
		for (MainFrame mainFrame : mainFrames) {
		}
	}

	public static void trace(String str) {
		if (btrace) {
			System.out.println(str);
		}
	}

	public static void msg(String str) {
		if (btrace) {
			System.out.println(str);
		}
	}

	public static void error(String msg, Exception... ex) {
		System.err.println(msg);
		if (ex.length > 0) {
			ex[0].printStackTrace(System.err);
		}
	}

	public void fontResetUi() {
		if (fontDefault == null) {
			return;
		}
		SwingUtil.setUIFont(new FontUIResource(fontDefault.getName(), fontDefault.getStyle(), fontDefault.getSize()));
	}

	public void fontSetDefault(Font font) {
//		App.trace("App.setDefaultFont(Font="+(font==null?"null":font.toString())+")");
		if (font == null) {
			JEditorPane e = new JEditorPane();
			fontDefault = e.getFont();
		} else {
			fontDefault = font;
		}
		fontResetUi();
		preferences.prefUI.setFontDefault(FontUtil.fontToString(fontDefault));
	}

	public Font fontGetDefault() {
//		App.trace("App.getDefaultFont()");
		if (fontDefault == null) {
			fontRestoreDefault();
		}
		return this.fontDefault;
	}

	public void fontRestoreDefault() {
//		App.trace("App.restoreDefaultFont()");
		fontSetDefault(FontUtil.getFont(preferences.prefUI.getFontDefault()));
	}

	public void fontSetEditor(Font font) {
//		App.trace("App.setEditorFont(Font="+(font==null?"null":font.toString())+")");
		if (font == null) {
			JEditorPane e = new JEditorPane();
			fontEditor = e.getFont();
		} else {
			fontEditor = font;
		}
		preferences.prefUI.setFontEditor(FontUtil.fontToString(fontEditor));
	}

	public Font fontGetEditor() {
		if (fontEditor == null) {
			fontRestoreEditor();
		}
		return this.fontEditor;
	}

	public void fontRestoreEditor() {
//		App.trace("App.restoreEditorFont()");
		String pref[] = (preferences.prefUI.getFontEditor()).split(",");
		String name = preferences.prefUI.getFontEditor();
		int size = Integer.parseInt(pref[1]);
		int style = Integer.parseInt(pref[2]);
		fontSetEditor(new Font(name, style, size));
	}

	public void fontSetMono(Font font) {
//		App.trace("App.setEditorFont(Font="+(font==null?"null":font.toString())+")");
		if (font == null) {
			JEditorPane e = new JEditorPane();
			monoFont = e.getFont();
		} else {
			monoFont = font;
		}
		preferences.prefUI.setFontMono(FontUtil.fontToString(monoFont));
	}

	public Font fontGetMono() {
		if (monoFont == null) {
			fontRestoreMono();
		}
		return this.monoFont;
	}

	public void fontRestoreMono() {
//		App.trace("App.restoreEditorFont()");
		String pref[] = (preferences.prefUI.getFontMono()).split(",");
		String name = pref[0];
		int size = Integer.parseInt(pref[1]);
		int style = Integer.parseInt(pref[2]);
		fontSetMono(new Font(name, style, size));
	}

	public static File getHomeDir() {
		return new File(System.getProperty("user.home"));
	}

	public static File getUserDir() {
		return new File(System.getProperty("user.dir"));
	}

	public static File getPrefDir() {
		return (new File(getHomeDir().getAbsolutePath() + File.separator + ".storybook6"));
	}

	public static File getPrefFile() {
		return (new File(getHomeDir().getAbsolutePath()
				+ File.separator + ".storybook6"
				+ File.separator + "oStorybook.ini"));
	}

	public void modelPropertyChange(PropertyChangeEvent evt) {
		// works, but currently not used
		// may be used for entity copying between files
		// String propName = evt.getPropertyName();
		// Object newValue = evt.getNewValue();
		// Object oldValue = evt.getOldValue();
	}

	public void createNewFile() {
		//App.trace("App.createNewFile()");
		Book book=new Book();
		book.init("");
		BookPref bookPref=new BookPref();
		File f;
		String fname;
		PropertiesDlg pdlg=new PropertiesDlg(book, bookPref);
		while(true) {
			pdlg.setVisible(true);
			if (pdlg.isCanceled() || book.info.getTitle().isEmpty()) {
				return;
			}
			fname=StringUtil.escapeTxt(book.info.getTitle());
			String x[]=fname.split(" ");
			fname="";
			for (String z:x) {
				fname+=StringUtil.capitalize(z);
			}
			fname=getHomeDir().getPath()+File.separator+fname;
			f=IOTools.selectFile(null, fname, "osbk", "oStorybook file (*.osbk)");
			if (f!=null) break;
		}
		fname=f.getAbsolutePath();
		if (!fname.endsWith(".osbk")) fname+=".osbk";
		XmlDB xml = XmlDB.create(fname, book, true);
		openFile(new File(fname), false);
	}

	public boolean openFile() {
		//App.trace("App.openFile()");
		final JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(preferences.getLastDir()));
		FileFilter filter = new FileFilter(FileFilter.DB, I18N.getMsg("file.type.db"));
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		int ret = fc.showOpenDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			if (!file.exists()) {
				JOptionPane.showMessageDialog(null,
						I18N.getMsg("file.notexists", file.getName()),
						I18N.getMsg("file.notexists.title"),
						JOptionPane.ERROR_MESSAGE);
				return (false);
			}
			return (openFile(file, false));
		}
		return (false);
	}

	public boolean openFile(File file, boolean setCopied) {
		//App.trace("App.openFile(File="+file.getAbsolutePath()+", copied="+(setCopied?"true":"false")+")");
		if (checkOpened(file.getName())) {
			return (true);
		}
		XmlDB db = new XmlDB(file);
		if (!db.isOK()) {
			return (false);
		}
		try {
			//check if db to be copied
			if (setCopied) {
				db.open();
				Book book = new Book();
				book.xmlFrom(db);
				book.info.setTitle(book.info.getTitle() + " (" + I18N.getMsg("z.copied") + ")");
				db.reformat(book.toXml());
				db.close();
				App.preferences.recentFilesAdd(db, book.getTitle());
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
			return (false);
		}
		try {
			setWaitCursor();
			//App.trace("trying invokeLater->");
			SwingUtilities.invokeLater(() -> {
				//App.trace("invoke Later in progress...");
				try {
					setWaitCursor();
					MainFrame newMainFrame = new MainFrame(db);
					addMainFrame(newMainFrame);
					closeBlank();
					updateFilePref(newMainFrame.xml, newMainFrame.book.getTitle());
					reloadMenuBars();
					setDefaultCursor();
				} catch (Exception e) {
					ExceptionDlg.show("", e);
				}
				//App.trace("invoke Later normal ending");
			});
		} catch (Exception e) {
			App.error("SbApp.openFile(file,setCopied)", e);
		}
		return true;
	}

	public void renameFile(final MainFrame mainFrame, File outFile) {
		//App.trace("App.renameFile(MainFrame="+mainFrame.name+",File="+outFile.getAbsolutePath()+")");
		try {
			File inFile = mainFrame.xml.getFile();
			mainFrame.close(false);
			FileTool.copyFile(inFile, outFile);
			inFile.delete();
			openFile(outFile, false);
		} catch (IOException e) {
			error("App.renameFile(" + mainFrame.getName() + "," + outFile.getName() + ")", e);
		}
	}

	public AppPref getPreferences() {
		return (preferences);
	}

	public void refresh() {
		//App.trace("App.refresh()");
		mainFrames.forEach((mainFrame)-> {
			int width = mainFrame.getWidth();
			int height = mainFrame.getHeight();
			boolean maximized = mainFrame.isMaximized();
			mainFrame.menu.refresh();
			mainFrame.menu.reloadMenuToolbar();
			mainFrame.setSize(width, height);
			if (maximized) {
				mainFrame.setMaximized();
			}
			mainFrame.refresh();
		});
	}

	public void refreshViews() {
		//App.trace("App.refreshViews()");
		mainFrames.forEach((mainFrame) -> {
			mainFrame.refreshViews();
		});
	}
	
	public void reloadMenuBars() {
		mainFrames.forEach((m) -> {
			m.menu.reloadMenuToolbar();
		});
	}

	public void reloadStatusBars() {
		mainFrames.forEach((m) -> {
			m.refreshStatusBar();
		});
	}

	public void updateFilePref(XmlDB db, String title) {
		//App.trace("App.updateFilePref("+db.getFile().name+")");
		// save last open directory and file
		File file = db.getFile();
		preferences.setLastDir(file.getParent());
		preferences.setLastFile(file.getPath());
		// save recent files
		preferences.recentFilesAdd(db, title);
		reloadMenuBars();
	}

	public void clearRecentFiles() {
		//App.trace("App.clearRecentFiles()");
		preferences.recentFiles = new ArrayList<>();
		reloadMenuBars();
	}

	public List<MainFrame> getMainFrames() {
		return mainFrames;
	}

	public void addMainFrame(MainFrame mainFrame) {
		//App.trace("App.addMainFrame("+mainFrame.name+")");
		mainFrames.add(mainFrame);
	}

	public void removeMainFrame(MainFrame mainFrame) {
		//App.trace("App.removeMainFrame("+mainFrame.name+")");
		mainFrames.remove(mainFrame);
	}

	public void closeBlank() {
		//App.trace("App.closeBlank()");
		for (MainFrame m : mainFrames) {
			if (m.isBlank()) {
				mainFrames.remove(m);
				m.dispose();
			}
		}
	}

	private boolean checkOpened(String name) {
		//App.trace("App.checkOpened("+name+")");
		for (MainFrame mainFrame : mainFrames) {
			if (mainFrame.isBlank()) {
				continue;
			}
			if (mainFrame.xml.name.equals(name)) {
				mainFrame.setVisible(true);
				return true;
			}
		}
		return false;
	}

	public void setWaitCursor() {
		mainFrames.forEach((mainFrame) -> {
			SwingUtil.setWaitingCursor(mainFrame);
		});
	}

	public void setDefaultCursor() {
		mainFrames.forEach((mainFrame) -> {
			SwingUtil.setDefaultCursor(mainFrame);
		});
	}

}
