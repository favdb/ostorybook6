/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app;

import desk.app.pref.AppPref;
import db.entity.AbstractEntity;
import db.entity.Book;
import desk.app.pref.BookPref;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.SbDate;
import db.entity.Scene;
import db.XmlDB;
import desk.action.MainAction;
import desk.assistant.Assistant;
import desk.model.Ctrl;
import desk.dialog.PropertiesDlg;
import desk.dialog.chooser.FileChooserDlg;
import desk.dialog.edit.Editor;
import desk.interfaces.IPaintable;
import desk.model.BlankModel;
import desk.model.BookModel;
import desk.panel.AbstractPanel;
import desk.panel.BlankPanel;
import desk.app.pref.AppPrefUI;
import desk.panel.info.Info;
import desk.panel.manage.Manage;
import desk.panel.typist.Typist;
import desk.spell.SpellUtil;
import desk.tools.DockUtil;
import desk.tools.FileFilter;
import tools.FileTool;
import desk.tools.FontUtil;
import desk.tools.IOTools;
import desk.tools.TempUtil;
import desk.tools.swing.SwingUtil;
import desk.view.SbView;
import desk.view.SbView.VIEWID;
import desk.view.SbViewFactory;
import db.I18N;
import desk.panel.scenario.ScenarioPanel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;

import lib.miginfocom.swing.MigLayout;

import lib.infonode.docking.DockingWindow;
import lib.infonode.docking.DockingWindowAdapter;
import lib.infonode.docking.RootWindow;
import lib.infonode.docking.SplitWindow;
import lib.infonode.docking.TabWindow;
import lib.infonode.docking.View;
import lib.infonode.docking.ViewSerializer;
import lib.infonode.docking.properties.RootWindowProperties;
import lib.infonode.docking.theme.DockingWindowsTheme;
import lib.infonode.docking.theme.ShapedGradientDockingTheme;
import lib.infonode.docking.util.DockingUtil;
import lib.infonode.docking.util.MixedViewHandler;
import lib.infonode.docking.util.StringViewMap;
import lib.infonode.util.Direction;
import tools.IconUtil;

/**
 * @author favdb
 *
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame implements IPaintable {

	static MainFrame instance;

	public static MainFrame getInstance() {
		return (instance);
	}

	public Menu menu;
	public MainAction mainAction;
	private BookModel bookModel;
	public BookPref bookPref;
	private Ctrl bookController;
	private SbViewFactory viewFactory;
	private RootWindow rootWindow;
	private StatusBar statusBar;
	@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
	private final HashMap<Integer, JComponent> dynamicViews = new HashMap<>();
	public XmlDB xml;
	public Book book;
	private Part currentPart;
	public boolean showAllParts = false;
	private Chapter lastChapter;
	private Scene lastScene = null;
	private Typist typistPanel;
	public Assistant assistant;

	public Manage managePanel;
	public ScenarioPanel scenarioPanel;

	public MainFrame() {
		init();
	}

	public MainFrame(XmlDB db) {
		init(db);
		initUi();
	}

	@Override
	public final void init() {
		//App.trace("MainFrame.init()");
		FontUtil.setDefault(new Font("Arial", Font.PLAIN, 12));
		mainAction = new MainAction(this);
		menu = new Menu(this);
		instance = this;
		assistant = new Assistant(this, null);
		viewFactory = new SbViewFactory(this);
		bookController = new Ctrl(this);
		bookPref = new BookPref();
		BlankModel model = new BlankModel(this);
		bookController.attachModel(model);
		setIconImage(IconUtil.getIconImage("icon"));
		addWindowListener(new MainFrameWindowAdaptor());
		this.setJMenuBar(menu.menuBar);
		initBlankUi();
	}

	public final void init(XmlDB dbF) {
		//App.trace("MainFrame.init(" + dbF.name + ")");
		FontUtil.setDefault(App.getInstance().fontGetDefault());
		mainAction = new MainAction(this);
		instance = this;
		assistant = new Assistant(this, null);
		menu = new Menu(this);
		try {
			xml = dbF;
			xml.open();
			book = new Book(xml);
			AppPref pref = App.getInstance().getPreferences();
			pref.setLastFile(xml.getFile().getAbsolutePath());
			pref.recentFilesAdd(xml, book.info.getTitle());
			pref.save();
			viewFactory = new SbViewFactory(this);
			viewFactory.setInitialisation();
			bookController = new Ctrl(this);
			bookModel = new BookModel(this);
			bookPref = new BookPref(xml.getOnlyPath());
			mainAction.init();
			bookController.attachModel(bookModel);
			addWindowListener(new MainFrameWindowAdaptor());
			SpellUtil.registerDictionaries();
			viewFactory.resetInitialisation();
			setJMenuBar(menu.menuBar);
			menu.setDevFunction();
			menu.viewPlan.setVisible(book.isPlanning());
		} catch (Exception e) {
			App.error("MainFrame.init(dbfname=" + dbF.name + ")", e);
		}
	}

	@Override
	public final void initUi() {
		//App.trace("MainFrame.initUi()");
		setLayout(new MigLayout("flowy,fill,ins 0,gap 0", "", "[grow]"));
		setIconImage(IconUtil.getIconImage("icon"));
		setTitle();
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		App.getInstance().fontResetUi();
		menu.reloadMenuToolbar();
		add(menu.toolBar);
		initRootWindow();
		setDefaultLayout();
		add(rootWindow, "grow");
		statusBar = new StatusBar(this);
		add(statusBar, "growx");
		bookController.attachView(statusBar);
		restoreDimension();
		pack();
		restoreLocation();
		setVisible(true);
		//initAfterPack();
		bookController.detachView(menu.menuBar);
		bookController.attachView(menu.menuBar);
		menu.menuBar.setVisible(true);
		this.setJMenuBar(menu.menuBar);
		// restore last used layout
		DockUtil.layoutLoad(this, "last");
		// restore last used part
		try {
			if (book.parts.size() > 0) {
				Long num = bookPref.getCurrentPart();
				Part part = (book.parts.get(0));
				if (num != -1L) {
					if (book.getEntity(Book.TYPE.PART, num) != null) {
						part = (Part)book.getEntity(Book.TYPE.PART, num);
					}
				}
				mainAction.partChange(part);
			}
		} catch (NumberFormatException e) {
			App.error("exiting try in MainFrame.initUi()", e);
		}
		bookController.attachView(this);
		bookController.getListAttachedViews();
		
		SwingUtilities.invokeLater(() -> {
			setTypist();
		});
		//App.trace(book.getStats(this,false));
	}

	public void setTypist() {
		//App.trace("MainFrame.setTypist()");
		if (xml.isOK()) {
			if (bookPref.getTypistUse()) {
				activateTypist();
			}
		}
	}

	public Scene getLastUsedScene() {
		return (lastScene);
	}

	public void setLastUsedScene(Scene s) {
		lastScene = s;
	}

	private boolean isTypist = false;

	public void activateTypist() {
		//App.trace("MainFrame.activateTypist() isTypist=" + (isTypist ? "true" : "false"));
		if (isTypist) {
			typistPanel.closeScene();
			remove(typistPanel);
			setJMenuBar(menu.menuBar);
			add(menu.toolBar);
			add(rootWindow, "grow");
			add(statusBar, "growx");
			isTypist = false;
		} else {
			typistPanel = new Typist(this);
			menu.menuBar = this.getJMenuBar();
			setJMenuBar(null);
			remove(menu.toolBar);
			remove(rootWindow);
			remove(statusBar);
			add(typistPanel, "grow");
			isTypist = true;
			this.setState(Frame.MAXIMIZED_BOTH);
		}
		pack();
	}

	public void setModified() {
		xml.setModified();
		setTitle();
		menu.viewPlan.setVisible(book.isPlanning());
	}

	public void resetModified() {
		xml.resetModified();
		setTitle();
		menu.viewPlan.setVisible(book.isPlanning());
	}

	public void setTitle() {
		//App.trace("MainFrame.setTitle()");
		String prodFullTitle = App.FULLNAME;
		if (xml != null) {
			Part part = getCurrentPart();
			String partName = "";
			if (part != null) {
				partName = part.getNumberName();
			}
			if (book != null) {
				String title = (xml.isModified()?"*":"")+book.getTitle() +
						" [" + I18N.getMsg("part") + " " + partName + "]" +
						" - " + prodFullTitle;
				setTitle(title);
			}
		} else {
			setTitle(prodFullTitle);
		}
		menu.fileSave.setEnabled(xml.isModified());
	}

	private void initRootWindow() {
		//App.trace("MainFrame.initRootWindow()");
		StringViewMap viewMap = viewFactory.getViewMap();
		MixedViewHandler handler = new MixedViewHandler(viewMap, new ViewSerializer() {
			@Override
			public void writeView(View view, ObjectOutputStream out) throws IOException {
				out.writeInt(((DynamicView) view).getId());
			}

			@Override
			public View readView(ObjectInputStream in) throws IOException {
				return getDynamicView(in.readInt());
			}
		});
		rootWindow = DockingUtil.createRootWindow(viewMap, handler, true);
		rootWindow.setName("rootWindow");
		rootWindow.setPreferredSize(new Dimension(4096, 2048));
		DockingWindowsTheme currentTheme = new ShapedGradientDockingTheme();
		RootWindowProperties properties = new RootWindowProperties();
		properties.addSuperObject(currentTheme.getRootWindowProperties());
		rootWindow.getRootWindowProperties().addSuperObject(properties);
		rootWindow.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
	}

	@SuppressWarnings("null")
	public void setDefaultLayout() {
		//App.trace("MainFrame.setDefaultLayout()");
		SbView tCategories = getView(VIEWID.TABLE_CATEGORY);
		SbView tChapters = getView(VIEWID.TABLE_CHAPTER);
		SbView tEvents = getView(VIEWID.TABLE_EVENT);
		SbView tGenders = getView(VIEWID.TABLE_GENDER);
		SbView tIdeas = getView(VIEWID.TABLE_IDEA);
		SbView tItems = getView(VIEWID.TABLE_ITEM);
		SbView tLocations = getView(VIEWID.TABLE_LOCATION);
		SbView tMemos = getView(VIEWID.TABLE_MEMO);
		SbView tParts = getView(VIEWID.TABLE_PART);
		SbView tPersons = getView(VIEWID.TABLE_PERSON);
		SbView tPlots = getView(VIEWID.TABLE_PLOT);
		SbView tPhotos = getView(VIEWID.TABLE_PHOTO);
		SbView tPovs = getView(VIEWID.TABLE_POV);
		SbView tRelations = getView(VIEWID.TABLE_RELATION);
		SbView tScenes = getView(VIEWID.TABLE_SCENE);
		SbView tTags = getView(VIEWID.TABLE_TAG);

		SbView vChrono = getView(VIEWID.VIEW_CHRONO);
		SbView vGallery = getView(VIEWID.VIEW_GALLERY);
		SbView vInfo = getView(VIEWID.VIEW_INFO);
		SbView vManage = getView(VIEWID.VIEW_MANAGE);
		SbView vMemo = getView(VIEWID.VIEW_MEMO);
		SbView vMemoria = getView(VIEWID.VIEW_MEMORIA);
		SbView vNavigation = getView(VIEWID.VIEW_NAVIGATION);
		SbView vPlanning = getView(VIEWID.VIEW_PLANNING);
		SbView vPaperclip = getView(VIEWID.VIEW_PAPERCLIP);
		SbView vReading = getView(VIEWID.VIEW_READING);
		SbView vScenario = getView(VIEWID.VIEW_SCENARIO);
		SbView vStoryboard = getView(VIEWID.VIEW_STORYBOARD);
		SbView vTree = getView(VIEWID.VIEW_TREE);
		SbView vTypist = getView(VIEWID.VIEW_TYPIST);
		SbView vWork = getView(VIEWID.VIEW_WORK);

		SbView cGantt = getView(VIEWID.CHART_GANTT);
		SbView cOccurrenceOfPersons = getView(VIEWID.CHART_OCCURRENCEOFPERSONS);
		SbView cOccurrenceOfLocations = getView(VIEWID.CHART_OCCURRENCEOFLOCATIONS);
		SbView cOccurrenceOfItems = getView(VIEWID.CHART_OCCURRENCEOFITEMS);
		SbView cPersonsByDate = getView(VIEWID.CHART_PERSONSBYDATE);
		SbView cPersonsByScene = getView(VIEWID.CHART_PERSONSBYSCENE);
		SbView cPovsByDate = getView(VIEWID.CHART_POVSBYDATE);
		SbView cWiww = getView(VIEWID.CHART_WIWW);

		TabWindow windowLeftTop = new TabWindow();
		TabWindow windowLeftBottom = new TabWindow(new SbView[]{vInfo, vNavigation});
		windowLeftBottom.setName("tabInfoNaviWindow");
		SplitWindow windowLeft = new SplitWindow(false, 0.6f, vTree, windowLeftBottom);
		windowLeft.setName("swTreeInfo");
		TabWindow windowCenter = new TabWindow(new SbView[]{
			vChrono, vGallery, vManage, vReading, vMemo, vMemoria, vPlanning, vPaperclip,
			vScenario, vStoryboard, vTypist, vWork,
			tCategories, tChapters, tGenders, tEvents, tIdeas, tItems, tLocations,
			tParts, tPersons, tPhotos, tPlots, tPovs, tRelations, tScenes, tTags,
			cGantt, cOccurrenceOfPersons, cOccurrenceOfItems, cOccurrenceOfLocations,
			cPersonsByDate, cPersonsByScene, cPovsByDate,
			cWiww
		});
		windowCenter.setName("windowCenter");
		TabWindow windowRight = new TabWindow();
		SplitWindow swTabWinMemo = new SplitWindow(true, 0.60f, windowCenter, tMemos);
		swTabWinMemo.setName("swTabWinMemos");
		SplitWindow windowMain = new SplitWindow(true, 0.20f, windowLeft, swTabWinMemo);
		windowMain.setName("windowMain");
		rootWindow.setWindow(windowMain);

		cGantt.close();
		cOccurrenceOfItems.close();
		cOccurrenceOfLocations.close();
		cOccurrenceOfPersons.close();
		cPersonsByDate.close();
		cPersonsByScene.close();
		cPovsByDate.close();
		cWiww.close();

		tCategories.close();
		tChapters.close();
		tEvents.close();
		tGenders.close();
		tIdeas.close();
		tItems.close();
		tMemos.close();
		tParts.close();
		tPersons.close();
		tPhotos.close();
		tPlots.close();
		tPovs.close();
		tRelations.close();
		tScenes.close();
		tTags.close();

		vGallery.close();
		vManage.close();
		vMemo.close();
		vMemoria.close();
		vPaperclip.close();
		vPlanning.close();
		vReading.close();
		vScenario.close();
		vStoryboard.close();
		vTypist.close();
		vWork.close();

		vInfo.restoreFocus();
		vChrono.restoreFocus();

		rootWindow.getWindowBar(Direction.RIGHT).setEnabled(true);
		DockUtil.setRespectMinimumSize(this);
	}

	private void initAfterPack() {
		//App.trace("MainFrame.initAfterPack()");
		SbView partsView = getView(VIEWID.TABLE_PART);
		SbView chaptersView = getView(VIEWID.TABLE_CHAPTER);
		SbView scenesView = getView(VIEWID.TABLE_SCENE);
		SbView memosView = getView(VIEWID.TABLE_MEMO);
		SbView readingView = getView(VIEWID.VIEW_READING);
		SbView treeView = getView(VIEWID.VIEW_TREE);
		SbView infoView = getView(VIEWID.VIEW_INFO);

		// add docking window adapter to all views (except editor)
		MainDockingWindowAdapter dockingAdapter = new MainDockingWindowAdapter();
		for (int i = 0; i < viewFactory.getViewMap().getViewCount(); ++i) {
			View view = viewFactory.getViewMap().getViewAtIndex(i);
			view.addListener(dockingAdapter);
		}
		// load initially shown views here
		SbView[] views2 = {
			readingView,
			partsView,
			chaptersView,
			scenesView,
			memosView,
			treeView,
			infoView
		};
		for (SbView view : views2) {
			viewFactory.loadView(view);
			bookController.attachView(view.getComponent());
			bookModel.fireAgain(view);
		}

		infoView.restoreFocus();
		readingView.restoreFocus();
	}

	public SbView getView(VIEWID viewId) {
		//App.trace("MainFrame.getView(viewId="+viewId.name()+")");
		SbView v = viewFactory.getView(viewId);
		return (v);
	}

	public void showView(VIEWID viewName) {
		//App.trace("MainFrame.showView(viewName=" + viewName.name() + ")");
		setWaitingCursor();
		SbView view = getView(viewName);
		if (view.getRootWindow() != null) {
			view.restoreFocus();
		} else {
			if (!view.isLoaded()) {
				viewFactory.loadView(view);
				bookController.attachView(view.getComponent());
				bookModel.fireAgain(view);
			}
			DockingUtil.addWindow(view, rootWindow);
		}
		view.requestFocusInWindow();
		DockUtil.setRespectMinimumSize(this);
		setDefaultCursor();
	}

	public void showView(String vname) {
		App.trace("MainFrame.showView(vname=" + vname + ")");
		VIEWID vn = SbView.getViewName(vname);
		if (vn == null) {
			System.err.println("MainFrame.showView(" + vname + ") no ViewName found");
			return;
		}
		showView(vn);
	}

	public void showAndFocus(String vname) {
		App.trace("MainFrame.showAndFocus(vname=" + vname + ")");
		VIEWID vn = SbView.getViewName(vname);
		if (vn == null) {
			System.err.println("MainFrame.showAndFocus(" + vname + ") no ViewName found");
			return;
		}
		SbView sbv = viewFactory.getView(vn);
		if (sbv == null) {
			System.err.println("MainFrame.showAndFocus(" + vname + ") no SbView found");
			return;
		}
		if (!sbv.isLoaded()) {
			viewFactory.loadView(sbv);
			bookController.attachView(sbv.getComponent());
			bookModel.fireAgain(sbv);
		}
		showAndFocus(vn);
	}

	public void showAndFocus(VIEWID viewId) {
		//App.trace("MainFrame.showAndFocus(viewName=" + viewName.name() + ")");
		View view = getView(viewId);
		view.restore();
		view.restoreFocus();
	}

	public void closeView(VIEWID viewId) {
		App.trace("MainFrame.closeView(" + viewId.name() + ")");
		SbView view = getView(viewId);
		view.close();
	}

	public void refresh() {
		//App.trace("MainFrame.refresh()");
		setWaitingCursor();
		for (int i = 0; i < viewFactory.getViewMap().getViewCount(); ++i) {
			SbView view = (SbView) viewFactory.getViewMap().getViewAtIndex(i);
			getBookController().refresh(view);
		}
		setDefaultCursor();
	}

	public void refreshStatusBar() {
		if (statusBar != null) {
			statusBar.refresh();
		}
	}

	// refresh tiles of views
	public void refreshViews() {
		//App.trace("MainFrame.refreshViews()");
		for (int i = 0; i < viewFactory.getViewMap().getViewCount(); ++i) {
			SbView view = (SbView) viewFactory.getViewMap().getViewAtIndex(i);
			viewFactory.setViewTitle(view);
		}
	}

	public boolean showEditorAsDialog(AbstractEntity entity) {
		//App.trace("MainFrame.showEditorAsDialog(" + (entity == null ? "null" : entity.objtype) + ")");
		boolean b = Editor.show(this, entity, null);
		TempUtil.remove(this, entity);
		return (b);
	}

	public void initBlankUi() {
		xml = null;
		setTitle(App.FULLNAME);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		setLocation(screenSize.width / 2 - 450, screenSize.height / 2 - 320);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		App.getInstance().fontResetUi();
		menu.reloadMenuToolbar();
		BlankPanel blankPanel = new BlankPanel(this);
		blankPanel.initAll();
		add(blankPanel);
		//restoreDimension();
		pack();
		setVisible(true);
	}

	public void setDefaultCursor() {
		SwingUtil.setDefaultCursor(this);
	}

	public void setWaitingCursor() {
		SwingUtil.setWaitingCursor(this);
	}

	public XmlDB getXml() {
		return xml;
	}

	public String getXmlName() {
		return xml.name;
	}

	public boolean isBlank() {
		return xml == null;
	}

	public Ctrl getBookController() {
		return bookController;
	}

	public BookModel getBookModel() {
		return bookModel;
	}

	public RootWindow getRootWindow() {
		return rootWindow;
	}

	public SbViewFactory getViewFactory() {
		return viewFactory;
	}

	private MainFrame getThis() {
		return this;
	}

	public boolean isMaximized() {
		return (getExtendedState() & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH;
	}

	public void setMaximized() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
	}

	public void close(boolean exitIfEmpty) {
		//App.trace("MainFrame.close(exitIfEmpty="+(exitIfEmpty?"true":"false")+")");
		App app = App.getInstance();
		AppPref pref = app.getPreferences();
		AppPrefUI prefui = pref.prefUI;
		if (!isBlank()) {
			if (scenarioPanel!=null &&
					scenarioPanel.checkModified(true)!=JOptionPane.YES_OPTION) {
				return;
			}
			if (isTypist && typistPanel.askModified() != JOptionPane.YES_OPTION) {
				return;
			}
			if (isTypist) {
				typistPanel.closeScene();
			}
			if (xml.isModified()) {
				//check if data are modified
				int n = JOptionPane.showConfirmDialog(getThis(),
						I18N.getMsg("z.close.confirm"),
						I18N.getMsg("z.close"),
						JOptionPane.YES_NO_CANCEL_OPTION);
				if (n == JOptionPane.CANCEL_OPTION) {
					return;
				}
				if (n == JOptionPane.YES_OPTION) {
					xml.reformat(book.toXml());
				}
			}
			if (getPref().getBoolean(AppPref.Key.CONFIRM_EXIT, true)) {
				int n = JOptionPane.showConfirmDialog(getThis(),
						I18N.getMsg("z.close.ask"),
						I18N.getMsg("z.close"),
						JOptionPane.YES_NO_OPTION);
				if (n == JOptionPane.NO_OPTION || n == JOptionPane.CLOSED_OPTION) {
					return;
				}
			}
			// save last used part
			if (currentPart != null) {
				bookPref.setCurrentPart(currentPart);
			} else {
				bookPref.setCurrentPart(-1L);
			}
			//viewFactory.saveAllTableDesign();
			bookPref.save(xml.getOnlyPath());
			xml.save(false);
			// save dimension, location, maximized and toolbar configuration
			prefui.setSize(new Dimension(this.getWidth(), this.getHeight()));
			prefui.setLoc(this.getLocation());
			prefui.setMaximized(this.isMaximized());
			prefui.setTbOrientation(menu.toolBar);
			pref.save();
			// save actual layout as last used
			DockUtil.layoutSave(this, "last");
		}
		app.removeMainFrame(this);
		dispose();
		if (app.getMainFrames().isEmpty()) {
			if (exitIfEmpty) {
				app.exit();
			} else { //re-create blank
				MainFrame mainFrame = new MainFrame();
				app.addMainFrame(mainFrame);
			}
		}
	}

	private View getDynamicView(int id) {
		View view = (View) dynamicViews.get(id);
		if (view == null) {
			view = new DynamicView("Dynamic View " + id, null, createDummyViewComponent("Dynamic View " + id), id);
		}
		return view;
	}

	private static JComponent createDummyViewComponent(String text) {
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < 100; j++) {
			sb.append(text).append(". This is line ").append(j).append("\n");
		}
		return new JScrollPane(new JTextArea(sb.toString()));
	}

	private void restoreDimension() {
		this.setPreferredSize(App.getInstance().getPreferences().prefUI.getSize());
	}

	private void restoreLocation() {
		//App.trace("MainFrame.restoreLocation()");
		AppPrefUI prefui = App.getInstance().getPreferences().prefUI;
		int w = prefui.getSize().width;
		int h = prefui.getSize().height;
		int x = prefui.getLoc().x;
		int y = prefui.getLoc().y;
		this.setLocationRelativeTo(null);
		// Get screens rectangle
		Rectangle rect = new Rectangle();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		for (GraphicsDevice device : gs) {
			rect.add(device.getDefaultConfiguration().getBounds());
		}
		// Do not put frame out of screens
		x = (int) Math.max(rect.getMinX(), Math.min(rect.getMaxX() - w, x));
		y = (int) Math.max(rect.getMinY(), Math.min(rect.getMaxY() - h, y));
		if (x != 0 && y != 0) {
			setLocation(x, y);
		}
		if (prefui.getMaximized()) {
			setMaximized();
		}
	}

	public void saveAllTableDesign() {
		//viewFactory.saveAllTableDesign();
	}

	public AppPref getPref() {
		return (App.getInstance().getPreferences());
	}

	public void setLastChapter(Chapter c) {
		lastChapter = c;
	}

	public Chapter getLastChapter() {
		return (lastChapter);
	}

	public String getLastPhotoDir() {
		return (bookPref.Gallery.getFolder());
	}

	public void setLastPhotoDir(File fc) {
		bookPref.Gallery.setFolder(fc.getAbsolutePath());
	}

	public void changeTitle(String newTitle) {
		//App.trace("MainFrame.changeTitle(" + newTitle + ")");
		book.info.setTitle(newTitle);
		setTitle();
	}

	public void changePath(String oldPath, String newPath) {
		//App.trace("MainFrame.changePath(" + oldPath + ", " + newPath + ")");
		if (oldPath.equals(newPath)) {
			return;
		}
		List<Scene> scenes = book.scenes;
		scenes.forEach((scene) -> {
			String text = scene.writing;
			if (scene.writing.contains(oldPath)) {
				text = text.replace(oldPath, newPath);
				scene.writing=text;
				this.getBookController().updateEntity(scene);
			}
		});
	}

	public void newEntity(AbstractEntity entity) {
		//App.trace("MainMenu.newEntity(" + entity.getClass().name + ")");
		showEditorAsDialog(entity);
	}

	public String getDefaultExportDir(MainFrame mainFrame) {
		return (xml.getOnlyPath() + File.separator + "export");
	}

	public void showInfo(Book entity) {
		SbView infoView = getView(VIEWID.VIEW_INFO);
		Info ip = (Info) infoView.getComponent();
		ip.setBook();
	}

	public void showInfo(AbstractEntity entity) {
		SbView infoView = getView(VIEWID.VIEW_INFO);
		Info ip = (Info) infoView.getComponent();
		ip.setEntity(entity);
	}

	public String getImageDir() {
		return (xml.getOnlyPath() + File.separator + "Images" + File.separator);
	}

	public void setScenarioPanel(ScenarioPanel panel) {
		scenarioPanel=panel;
	}

	private static class DynamicView extends View {

		private final int id;

		DynamicView(String title, Icon icon, Component component, int id) {
			super(title, icon, component);
			this.id = id;
		}

		public int getId() {
			return id;
		}
	}

	private class MainFrameWindowAdaptor extends WindowAdapter {

		@Override
		public void windowClosing(WindowEvent evt) {
			close(true);
		}
	}

	private class MainDockingWindowAdapter extends DockingWindowAdapter {

		@Override
		@SuppressWarnings("null")
		public void windowAdded(DockingWindow addedToWindow, DockingWindow addedWindow) {
			//App.trace("MainDockingWindowAdapter.windowAdded(" + addedToWindow.name + ", " + addedWindow.name + ")");
			if (addedWindow != null && addedWindow instanceof SbView) {
				SbView view = (SbView) addedWindow;
				if (!view.isLoaded()) {
					viewFactory.loadView(view);
					bookController.attachView(view.getComponent());
					bookModel.fireAgain(view);
				}
			}
		}

		@Override
		@SuppressWarnings("null")
		public void windowClosed(DockingWindow window) {
			//App.trace("MainDockingWindowAdapter.windowClosed(" + window.name + ")");
			if (window != null && window instanceof SbView) {
				SbView view = (SbView) window;
				if (!view.isLoaded()) {
					return;
				}
				bookController.detachView((AbstractPanel) view.getComponent());
				viewFactory.unloadView(view);
			}
		}
	}

	public Part getCurrentPart() {
		if (currentPart == null && book != null && book.parts != null && book.parts.size() > 0) {
			currentPart = book.parts.get(0);
		}
		return currentPart;
	}

	public void setCurrentPart(Part currentPart) {
		if (currentPart != null) {
			this.currentPart = currentPart;
		}
	}

	public boolean hasCurrentPart() {
		return currentPart != null;
	}

	public void setMainToolBar(JToolBar toolBar) {
		if (menu.toolBar != null) {
			getContentPane().remove(menu.toolBar);
		}
		String bl = this.getPref().prefUI.getTb();
		String b[] = bl.split(",");
		if ("East".equals(b[0]) || "West".equals(b[0])) {
			menu.toolBar.setOrientation(1);
		}
		getContentPane().add(menu.toolBar, b[0]);
	}

	public void backupNew(boolean isManual) {
		//App.trace("MainFrame.backupNew(isManual="+(isManual?"true":"false")+")");
		String backup_dir = bookPref.Backup.getDirectory();
		//if backup_directory is empty ask for a directory
		if (isManual && backup_dir.isEmpty()) {
			File b = IOTools.selectDirectory(null, "");
			if (b == null) {
				return;
			}
			backup_dir = b.getAbsolutePath();
		}
		//if backup_directory is emty then it is a canceled action, just return
		if (backup_dir.isEmpty()) {
			System.err.println("no backup directory, backup failed or canceled");
			return;
		}
		String backup_filename = backup_dir + File.separator + xml.name;
		//construct backup_filename with properties backup_increment
		if (bookPref.Backup.getAuto()) {
			backup_filename += SbDate.today();
		}
		//if backup_filename exists and manual opération then ask for replacement
		File file = new File(backup_filename + ".backup");
		if (file.exists() && isManual) {
			if (JOptionPane.showConfirmDialog(this,
					I18N.getMsg("file.backup_askreplace", file.getAbsolutePath()),
					I18N.getMsg("backup"),
					JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		xml.doBackup(bookPref.Backup.getDirectory(),
				bookPref.Backup.getAuto());
	}

	public void backupRest() {
		//App.trace("MainFrame.backupRest()");
		//select the file to restore to
		File b = IOTools.selectFile(null, "", "backup", "Backup file (*.backup)");
		if (b == null) {
			return;
		}
		//select the directory to restore to
		if (xml == null) {
			File dir = IOTools.selectDirectory(null, "");
			if (dir == null) {
				return;
			}
			xml = new XmlDB(b.getName().replace(".backup", ""));
			xml.doRestore(b);
		} else {
			close(false);
			xml.doRestore(b);
		}
		App.getInstance().openFile(xml.getFile(), false);
	}

	public void fileSaveAs() {
		//App.trace("MainFrame.fileSaveAs()");
		final JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(xml.getPath()));
		FileFilter filter = new FileFilter(FileFilter.DB, I18N.getMsg("file.type.db"));
		fc.addChoosableFileFilter(filter);
		fc.setFileFilter(filter);
		File file;
		int ret = fc.showOpenDialog(null);
		if (ret == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
			if (file.exists()) {
				JOptionPane.showMessageDialog(null,
						I18N.getMsg("file.rename.failed.text", file.getName()),
						I18N.getMsg("file.rename.failed.title"),
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			setWaitingCursor();
			File outFile = file;
			File inFile = this.xml.getFile();
			this.close(false);
			try {
				FileTool.copyFile(inFile, outFile);
				App.getInstance().openFile(outFile, true);
			} catch (IOException ioe) {
				ioe.printStackTrace(System.err);
				App.getInstance().openFile(inFile, false);
			}
			setDefaultCursor();
		}
	}

	public void fileRename() {
		//App.trace("MainFrame.fileRename()");
		FileChooserDlg dlg = new FileChooserDlg(this, xml.getPath(), "osbk", false);
		dlg.setForceExt();
		dlg.setVisible(true);
		if (dlg.isCanceled()) {
			return;
		}
		File outFile = dlg.getFile();
		App.getInstance().renameFile(this, outFile);
	}

	public void fileProperties() {
		PropertiesDlg dlg = new PropertiesDlg(this);
		dlg.setVisible(true);
	}

	public void windowSaveLayoutAction() {
		String name;
		while (true) {
			name = JOptionPane.showInputDialog(this,
					I18N.getColonMsg("z.name"),
					I18N.getMsg("docking.save.layout"),
					JOptionPane.PLAIN_MESSAGE);
			File f = new File(App.getPrefDir().getAbsolutePath()
					+ File.separator + name + ".layout");
			if (!f.exists()) {
				break;
			}
			//signaler que le layout existe déjà et demander le remplacement, ou si changer de nom, ou abandonner
			int ret = JOptionPane.showConfirmDialog(this,
					I18N.getMsg("z.warning") + ":" + I18N.getMsg("docking.save.layout.exists"),
					I18N.getMsg("docking.layout"),
					JOptionPane.YES_NO_CANCEL_OPTION);
			if (ret == JOptionPane.OK_OPTION) {
				break;
			}
			if (ret == JOptionPane.CANCEL_OPTION) {
				return;
			}
			// if NO_OPTION then continue in while
		}
		if (name != null) {
			DockUtil.layoutSave(this, name);
		}
	}

}
