/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app;

import db.DB;
import db.I18N;
import db.entity.Part;
import desk.app.pref.AppPref;
import desk.model.Ctrl;
import desk.panel.AbstractPanel;
import desk.panel.MemoryPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;

/**
 * @author favdb
 *
 */
@SuppressWarnings("serial")
public class StatusBar extends AbstractPanel implements ActionListener {

	private JLabel lbParts;
	private JLabel lbStat;
	private int nbWords;
	private int nbCharacters;
	private int nbPersons;
	private int nbChapters;
	private int nbScenes;
	private MemoryPanel memPanel;

	public StatusBar(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("StatusBar.modelPropertyChange(evt="+evt.getPropertyName()+")");
		String propName = evt.getPropertyName().toLowerCase();
		if (propName.contains("None")) {
			return;
		}
		if ("part_Change".equals(propName) || "part_Update".equals(propName)) {
			refresh();
			return;
		}

		if (Ctrl.whichACTION(propName).equals(Ctrl.ACTION.REFRESH)) {
			refresh();
			return;
		}

		if (propName.contains("chapter") || propName.contains("scene")) {
			refresh();
		}

	}

	@Override
	public void init() {
		computeStatistics();
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("flowx,fill,ins 1", "[][grow][]"));

		Part p = mainFrame.getCurrentPart();
		if (p != null) {
			lbParts = new JLabel(" " + I18N.getMsg(App.class, "part.current") + ": "
					+ " " + p.toString());
		} else {
			lbParts = new JLabel(I18N.getMsg("part.current_no"));
		}
		add(lbParts, "al left");

		lbStat = new JLabel(geneLibStat());
		add(lbStat, "al center");
		memPanel = new MemoryPanel();
		add(memPanel, "al right");
		if (!mainFrame.getPref().getBoolean(AppPref.Key.MEMORY, false)) {
			memPanel.setVisible(false);
		}

		refresh();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		lbStat.setText(geneLibStat());
	}

	private void computeStatistics() {
		if (book == null) {
			return;
		}
		nbChapters = book.chapters.size();
		nbScenes = book.scenes.size();
		nbPersons = book.persons.size();
		nbWords = book.getWords();
		nbCharacters = book.getChars();
	}


	@Override
	public void refresh() {
		computeStatistics();
		lbStat.setText(geneLibStat());
		revalidate();
		repaint();
	}

	private String geneLibStat() {
		String strStat = I18N.getMsg(DB.class, "z.statistics") + ":";
		strStat += " " + I18N.getMsg(DB.class, "chapters") + "=" + nbChapters + ", ";
		strStat += " " + I18N.getMsg(DB.class, "scenes") + "=" + nbScenes + ", ";
		strStat += " " + I18N.getMsg(DB.class, "persons") + "=" + nbPersons + ", ";
		strStat += " " + I18N.getMsg(DB.class, "z.characters") + "=" + nbCharacters + ", ";
		strStat += " " + I18N.getMsg(DB.class, "z.words") + "=" + nbWords;
		return (strStat);
	}

}
