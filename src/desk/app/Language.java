/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app;

import db.I18N;
import java.util.Locale;

/**
 *
 * @author favdb
 */
public class Language {

	public static enum LANGUAGE {
		ar_, //Arabe
		bg_BG, //Bulgarian
		cs_CZ, //Czech
		da_DK, //Danish
		de_DE, //German
		el_GR, //Greek
		en_GB, //GB english
		en_US, //USA english
		eo_EO, //Esperanto
		es_ES, //Spanish
		fi_FI, //Finnish
		fr_FR, //French
		hu_HU, //Hungarian
		it_IT, //Italian
		iw_IL, //Hebrew
		ja_JP, //Japanese
		ko_KR, //South Korean
		nl_NL, //Ducth
		pl_PL, //Polish
		pt_PT, //Potuguese
		pt_BR, //Brazilian Portuguese
		ru_RU, //Russian
		sv_SE, //Swedish
		tr_TR, //Turkish
		uk_UA, //Ukrainian
		zh_CN, //Simplified Chinese
		zh_HK, //Traditional Chinese (Hong Kong)
		;

		public String getI18N() {
			return I18N.getLanguage("language." + name());
		}

		public Locale getLocale() {
			Locale locale;
			switch (this) {
				case ar_:
					locale=new Locale("ar");
					break;
				case bg_BG:
					locale = new Locale("bg", "BG");
					break;
				case cs_CZ:
					locale = new Locale("cs", "CZ");
					break;
				case da_DK:
					locale = new Locale("da", "DK");
					break;
				case de_DE:
					locale = Locale.GERMANY;
					break;
				case el_GR:
					locale = new Locale("el", "GR");
					break;
				case en_GB:
					locale = Locale.UK;
					break;
				case en_US:
					locale = Locale.US;
					break;
				case eo_EO:
					locale = new Locale("eo", "EO");
					break;
				case es_ES:
					locale = new Locale("es", "ES");
					break;
				case fi_FI:
					locale = new Locale("fi", "FI");
					break;
				case fr_FR:
					locale = new Locale("fr", "FR");
					break;
				case hu_HU:
					locale = new Locale("hu", "HU");
					break;
				case it_IT:
					locale = new Locale("it", "IT");
					break;
				case iw_IL:
					locale = new Locale("iw", "IL");
					break;
				case ja_JP:
					locale = new Locale("ja", "JP");
					break;
				case ko_KR:
					locale = new Locale("ko", "KR");
					break;
				case nl_NL:
					locale = new Locale("nl", "NL");
					break;
				case pl_PL:
					locale = new Locale("pl", "PL");
					break;
				case pt_PT:
					locale = new Locale("pt", "PT");
					break;
				case pt_BR:
					locale = new Locale("pt", "BR");
					break;
				case ru_RU:
					locale = new Locale("ru", "RU");
					break;
				case sv_SE:
					locale = new Locale("sv", "SE");
					break;
				case tr_TR:
					locale = new Locale("tr", "TR");
					break;
				case uk_UA:
					locale = new Locale("uk", "UA");
					break;
				case zh_HK:
					locale = new Locale("zh", "HK");
					break;
				case zh_CN:
					locale = new Locale("zh", "CN");
					break;
				default:
					locale = Locale.US;
			}
			return locale;
		}
	};

	public static enum SPELLING {
		none, en_GB, en_US, de_DE, es_ES, it_IT, fr_FR, ru_RU, nl_NL, pl_PL;

		public String getI18N() {
			if (this == none) {
				return I18N.getMsg("pref.spelling.no");
			}
			return I18N.getMsg("language." + name());
		}
	}

}
