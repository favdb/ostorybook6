/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app;

import db.entity.Part;
import desk.app.pref.AppPref.PrefFile;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;

import tools.IconUtil;

import desk.dialog.ToolbarDlg;
import desk.tools.DockUtil;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;

/**
 *
 * @author favdb
 */
public class Menu {

	public enum ACTION {
		EDIT,
		EDIT_COPY_BLURB, EDIT_COPY_ENTITY, EDIT_COPY_TEXT,
		EDIT_EDITOR_RESET, EDIT_PASTE, EDIT_PREFERENCES, EDIT_SPELLING,
		FILE,
		FILE_ASSISTANT, FILE_NEW, FILE_CREATE, FILE_OPEN, FILE_OPEN_RECENT,
		FILE_BACKUP, FILE_BACKUP_NEW, FILE_BACKUP_REST, FILE_RECENT_CLEAR,
		FILE_SAVE, FILE_SAVEAS, FILE_RENAME, FILE_PROPERTIES,
		FILE_PRINT, FILE_CLOSE,
		FILE_EXPORT, FILE_EXPORT_HTML, FILE_EXPORT_OTHER, FILE_EXPORT_OPTIONS,
		FILE_IMPORT, FILE_EXIT,
		HELP,
		HELP_ABOUT, HELP_BUG, HELP_DOC, HELP_FAQ, HELP_HOMEPAGE,
		HELP_TEST, HELP_TRACE, HELP_TRANSLATE, HELP_UPDATE,
		NEW,
		NEW_CATEGORY, NEW_CHAPTER, NEW_CHAPTERS,
		NEW_EVENT, NEW_FOI, NEW_GENDER, NEW_IDEA, NEW_ITEM,
		NEW_LOCATION, NEW_MEMO, NEW_PART, NEW_PERSON, NEW_PHOTO,
		NEW_PLOT, NEW_POV, NEW_RELATION, NEW_SCENE, NEW_TAG,
		PART, PART_ALL, PART_NEXT, PART_PREVIOUS,
		TABLE,
		TOOLS,
		TOOLS_CHAPTER_ORDER, TOOLS_RENAME_CITY, TOOLS_RENAME_COUNTRY,
		TOOLS_RENAME, TOOLS_REPLACE, TOOLS_SCENE_RENUMBER, TOOLS_SEARCH,
		VIEW,
		VIEW_CHRONO, VIEW_GALLERY, VIEW_INFO, VIEW_MANAGE, VIEW_MEMOS, VIEW_MEMORIA,
		VIEW_NAVIGATION, VIEW_PAPERCLIP, VIEW_PLANNING, VIEW_READING, VIEW_SCENARIO,
		VIEW_STORYBOARD, VIEW_TREE, VIEW_TYPIST, VIEW_WORK, VIEW_OPTIONS,
		WINDOW,
		WINDOW_LAYOUT_DEFAULT, WINDOW_LAYOUT_LOAD, WINDOW_LOAYOUT_ONLY_BOOK,
		WINDOW_LAYOUT_ONLY_CHRONO, WINDOW_LAYOUT_ONLY_MANAGE,
		WINDOW_LAYOUT_ONLY_READING, WINDOW_LAYOUT_PERSONS_LOCATIONS,
		WINDOW_LAYOUT_REFRESH, WINDOW_LAYOUT_RESET, WINDOW_LAYOUT_SAVE,
		WINDOW_LAYOUT_SCREENPLAY, WINDOW_LAYOUT_STORYBOARD, WINDOW_LAYOUT_TAGS_ITEMS,
		WINDOW_LAYOUT_THEATERPLAY;

		private ACTION() {
		}

		@Override
		public String toString() {
			return name().toLowerCase();
		}

		public boolean compare(String str) {
			return name().equals(str);
		}
	}

	public static ACTION getAction(String text) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(text)) {
				return (a);
			}
		}
		return (null);
	}

	MainFrame mainFrame;

	public JMenuBar menuBar;

	private JMenu chartsMenu;
	public JMenuItem chartOccurrenceOfItems,
			chartOccurrenceOfLocations,
			chartOccurrenceOfPersons,
			chartPersonsByDate,
			chartPersonsByScene,
			chartPovsByDate,
			chartWIWW;

	private JMenu editMenu;
	public JMenuItem editCopyBlurb,
			editCopyBook,
			editCopyEntity,
			editPreferences,
			editSpellChecker;

	private JMenu fileMenu;
	public JMenuItem
			fileNew,
			fileOpen,
			fileClose,
			fileExit,
			fileAssistant,
			fileProperties,
			fileRename,
			fileSave,
			fileSaveAs;
	private JMenu fileExport;
	public JMenuItem fileExportHTML,
			fileExportOptions,
			fileExportOther,
			fileExportXml,
			fileImport;
	public JMenu fileOpenRecent;
	private JMenu fileBackup;
	public JMenuItem
			fileBackupNew,
			fileBackupRest;

	private JMenu helpMenu;
	public JMenuItem
			helpAbout,
			helpCheckUpdates,
			helpDoc,
			helpFaq,
			helpHome,
			helpReportBug,
			helpTranslate,
			helpDevTest;
	public JCheckBoxMenuItem helpTrace;

	private JMenu newMenu;
	public JMenuItem
			newCategory,
			newChapter,
			newChapters,
			newEvent,
			newFOI,
			newGender,
			newIdea,
			newItem,
			newLocation,
			newMemo,
			newPart,
			newPerson,
			newPhoto,
			newPlot,
			newPov,
			newRelation,
			newScene,
			newTag;

	private JMenu partsMenu;
	private JCheckBoxMenuItem partAll;
	public JMenuItem
			partNext,
			partPrevious;
	public JCheckBoxMenuItem allParts;

	public JMenuItem
			renameCity,
			renameCountry;

	private JMenu tabMenu;
	public JMenuItem
			tabCategory,
			tabChapter,
			tabEvent,
			tabGender,
			tabIdea,
			tabItem,
			tabLocation,
			tabMemo,
			tabPart,
			tabPerson,
			tabPhoto,
			tabPlot,
			tabPov,
			tabRelation,
			tabScene,
			tabTag;

	private JMenu toolsMenu;
	private JMenu toolsRenameMenu;
	public JMenuItem
			toolsChaptersOrder,
			viewPlan,
			toolsReplace,
			toolsScenesOrder,
			toolsSearchMenu;

	private JMenu viewMenu;
	public JMenuItem viewWork,
			viewChrono,
			viewGallery,
			viewInfo,
			viewMemos,
			viewManageScene,
			viewMemoria,
			viewNavigation,
			viewOptions,
			viewPaperclip,
			viewReading,
			viewScenario,
			viewStoryboard,
			viewTree,
			viewTypist;

	private JMenu windowMenu;
	private JMenu windowLoadLayout;
	private JMenu windowPredefinedLayout;
	public JMenuItem windowRefresh,
			windowResetLayout,
			windowSaveLayout;

	public JToolBar toolBar;
	public JButton btFileNew,
			btFileOpen,
			btFileSave,
			btFileExit,
			btIdea,
			btMemo,
			btNewChapter,
			btNewEvent,
			btNewGender,
			btNewItem,
			btNewLocation,
			btNewPart,
			btNewPerson,
			btNewPhoto,
			btNewPov,
			btNewRelation,
			btNewScene,
			btNewTag,
			btPartNext,
			btPartPrevious,
			btTabChapter,
			btTabGender,
			btTabIdea,
			btTabItem,
			btTabLocation,
			btTabMemo,
			btTabPart,
			btTabPerson,
			btTabRelation,
			btTabScene,
			btTabPov,
			btTabTag;
	private JButton btViewWork,
			btViewChrono,
			btViewManage,
			btViewMemoria,
			btViewPaperclip,
			btViewReading,
			btViewScenario,
			btViewStoryboard,
			btViewTypist;

	public static boolean BLANK = false;

	private JSeparator sep1, sep2, sep3, sep4;

	public Menu(MainFrame m) {
		mainFrame = m;
		initMenuAll();
		initTbAll();
	}

	/*
	public void setMenu(boolean blank) {
		JMenu menus[] = {
			fileBackup,
			newMenu, partsMenu, tabMenu,
			chartsMenu, viewMenu, windowMenu, toolsMenu
		};
		for (Component m : fileMenu.getMenuComponents()) {
			if (m instanceof javax.swing.JPopupMenu.Separator) {
				m.setVisible(blank);
			}
		}
		for (JMenu m : menus) {
			m.setVisible(blank);
		}
		JMenuItem[] submenus = {
			editCopyBlurb, editCopyBook, editCopyEntity,
			fileClose, fileExport,
			fileProperties, fileRename, fileSave, fileSaveAs, fileExport, fileImport,
			fileBackupNew, fileAssistant
		};
		for (javax.swing.JMenuItem m : submenus) {
			if (m != null) {
				m.setVisible(blank);
			}
		}

		javax.swing.JButton button[] = {
			btFileSave,
			btViewManage,
			btNewChapter, btNewItem, btNewLocation, btNewPerson, btNewScene, btNewTag,
			btPartNext, btPartPrevious,
			btTabChapter, btTabItem, btTabLocation, btTabPerson, btTabRelation,
			btTabScene, btTabTag,
			btViewTypist, btViewWork, btViewChrono, btViewMemoria, btViewPaperclip,
			btViewReading, btIdea, btMemo
		};
		for (javax.swing.JButton bt : button) {
			bt.setVisible(blank);
		}
	}*/

	private void initMenuAll() {
		menuBar = new JMenuBar();
		menuBar.setName("MainMenuBar");
		menuBar.setFont(App.getInstance().fontGetDefault());
		initMenuFile();
		initMenuEdit();
		initMenuNew();
		initMenuTable();
		initMenuTools();
		initMenuView();
		initMenuParts();
		initMenuCharts();
		initMenuWindow();
		initMenuHelp();
	}

	private JMenu initMenu(ACTION action, String... str) {
		return (initMenu(action.toString()));
	}

	private JMenu initMenu(String text) {
		String mx = I18N.getMsg("menu." + text);
		if (text.startsWith("file_")) {
			mx=I18N.getMsg("menu."+text.replace("file_","file."));
		}
		String mxz[] = mx.split(",");
		JMenu m = new JMenu(mxz[0]);
		m.setName(text);
		if (mxz.length > 1) {
			m.setMnemonic(mxz[1].charAt(0));
		}
		return (m);
	}
	
	private void refreshMenu(JMenu m) {
		String text=m.getName();
		String mx = I18N.getMsg("menu." + text);
		if (text.startsWith("file_")) {
			mx=I18N.getMsg("menu."+text.replace("file_","file."));
		}
		String mxz[] = mx.split(",");
		m.setText(mxz[0]);
		if (mxz.length > 1) {
			m.setMnemonic(mxz[1].charAt(0));
		}
		m.setFont(App.getInstance().fontGetDefault());
	}

	private JMenuItem initMenuItem(String ident, String... icon_name) {
		/*App.trace("Menu.initMenuItem(ident="+ ident+
				", icon_name="+(icon_name.length>0?icon_name[0]:"empty"+")"));*/
		String mx = "menu." + ident;
		if (ident.startsWith("new")) {
			mx = "menu." + ident.replace("_", ".");
		}
		if (ident.startsWith("file_")) {
			mx="menu." + ident.replace("file_", "file.");
		}
		String mxz[] = I18N.getMsg(mx).split(",");
		JMenuItem m = new JMenuItem(mxz[0]);
		m.setToolTipText(mxz[0]);
		if (mxz.length > 1) {
			if (!mxz[1].isEmpty()) {
				m.setMnemonic(mxz[1].charAt(0));
			}
		}
		//m.setActionCommand(mxz[0]);
		m.addActionListener((ActionEvent evt) -> {
			mainFrame.mainAction.doAction(ident);
		});
		String icn = "small/" + ident.substring(ident.lastIndexOf('.') + 1);
		if (icon_name != null && icon_name.length > 0) {
			if (!icon_name[0].isEmpty()) {
				icn = "small/" + icon_name[0];
			} else {
				icn = "";
			}
		}
		if (!icn.isEmpty()) {
			m.setIcon(IconUtil.getIcon(icn));
		}
		return (m);
	}

	private void refreshMenuItem(JMenuItem m) {
		String text=m.getName();
		String mx = I18N.getMsg("menu." + text);
		if (text.startsWith("file_")) {
			mx=I18N.getMsg("menu."+text.replace("file_","file."));
		}
		String mxz[] = mx.split(",");
		m.setText(mxz[0]);
		if (mxz.length > 1) {
			m.setMnemonic(mxz[1].charAt(0));
		}
		m.setFont(App.getInstance().fontGetDefault());
	}

	private JMenuItem initMenuItem(VIEWID view, String... icon_name) {
		return (initMenuItem(view.toString(), icon_name));
	}

	private JMenuItem initMenuItem(ACTION action, String... icon_name) {
		return (initMenuItem(action.toString(), icon_name));
	}

	private JCheckBoxMenuItem initMenuCheck(ACTION action) {
		String mx = I18N.getMsg("menu." + action.toString());
		String mxz[] = mx.split(",");
		JCheckBoxMenuItem m = new JCheckBoxMenuItem();
		m.setText(mxz[0]);
		if (mxz.length > 1) {
			m.setMnemonic(mxz[1].charAt(0));
		}
		m.addActionListener((ActionEvent evt) -> {
			mainFrame.mainAction.doAction(action);
		});
		return (m);
	}

	private void refreshMenuCheck(JCheckBoxMenuItem m) {
		String text=m.getName();
		String mx = I18N.getMsg("menu." + text);
		if (text.startsWith("file_")) {
			mx=I18N.getMsg("menu."+text.replace("file_","file."));
		}
		String mxz[] = mx.split(",");
		m.setText(mxz[0]);
		if (mxz.length > 1) {
			m.setMnemonic(mxz[1].charAt(0));
		}
		m.setFont(App.getInstance().fontGetDefault());
	}

	public void initMenuFile() {
		fileMenu = initMenu("file");

		fileNew = initMenuItem(ACTION.FILE_NEW, "file-new");
		fileMenu.add(fileNew);

		fileOpen = initMenuItem(ACTION.FILE_OPEN, "file-open");
		fileMenu.add(fileOpen);

		fileOpenRecent = initMenu(ACTION.FILE_OPEN_RECENT, "");
		fileMenu.add(fileOpenRecent);
		reloadRecentMenu();

		fileSave = initMenuItem(ACTION.FILE_SAVE, "file-save");
		fileSave.setEnabled(false);
		fileMenu.add(fileSave);

		fileSaveAs = initMenuItem(ACTION.FILE_SAVEAS, "file-saveas");
		fileMenu.add(fileSaveAs);

		fileRename = initMenuItem(ACTION.FILE_RENAME, "");
		fileMenu.add(fileRename);

		fileClose = initMenuItem(ACTION.FILE_CLOSE, "");
		fileMenu.add(fileClose);

		sep1 = new JSeparator();
		fileMenu.add(sep1);

		fileBackup = initMenu(ACTION.FILE_BACKUP);
		fileMenu.add(fileBackup);

		fileBackupNew = initMenuItem(ACTION.FILE_BACKUP_NEW, "");
		fileBackup.add(fileBackupNew);

		fileBackupRest = initMenuItem(ACTION.FILE_BACKUP_REST, "");
		fileBackup.add(fileBackupRest);

		sep2 = new JSeparator();
		fileMenu.add(sep2);

		fileProperties = initMenuItem(ACTION.FILE_PROPERTIES, "");
		fileMenu.add(fileProperties);

		fileAssistant = initMenuItem(ACTION.FILE_ASSISTANT, "");
		fileMenu.add(fileAssistant);

		fileMenu.addSeparator();

		fileImport = initMenuItem(ACTION.FILE_IMPORT, "");
		fileImport.setEnabled(false);
		fileMenu.add(fileImport);

		fileExport = initMenu(ACTION.FILE_EXPORT);

		fileExportHTML = initMenuItem(ACTION.FILE_EXPORT_HTML, "");
		fileExport.add(fileExportHTML);

		fileExportOther = initMenuItem(ACTION.FILE_EXPORT_OTHER, "");
		fileExport.add(fileExportOther);

		sep3 = new JSeparator();
		fileMenu.add(sep3);

		fileExportOptions = initMenuItem(ACTION.FILE_EXPORT_OPTIONS, "");
		fileExport.add(fileExportOptions);

		fileMenu.add(fileExport);

		sep4 = new JSeparator();
		fileMenu.add(sep4);

		fileExit = initMenuItem(ACTION.FILE_EXIT, "file-exit");
		fileMenu.add(fileExit);

		menuBar.add(fileMenu);
	}

	private void initMenuEdit() {
		editMenu = initMenu(ACTION.EDIT);

		editCopyBook = initMenuItem(ACTION.EDIT_COPY_TEXT, "");
		editMenu.add(editCopyBook);

		editCopyBlurb = initMenuItem(ACTION.EDIT_COPY_BLURB, "");
		editMenu.add(editCopyBlurb);

		editCopyEntity = initMenuItem(ACTION.EDIT_COPY_ENTITY, "edit-copy");
		editMenu.add(editCopyEntity);

		JMenuItem reinitEditor = initMenuItem(ACTION.EDIT_EDITOR_RESET, "center");
		editMenu.add(reinitEditor);

		editMenu.addSeparator();

		editPreferences = initMenuItem(ACTION.EDIT_PREFERENCES, "preferences");
		editMenu.add(editPreferences);

		editSpellChecker = initMenuItem(ACTION.EDIT_SPELLING, "spell");
		editMenu.add(editSpellChecker);

		menuBar.add(editMenu);
	}

	private void initMenuNew() {
		newMenu = initMenu(ACTION.NEW);

		newPov = initMenuItem(ACTION.NEW_POV, "pov");
		newMenu.add(newPov);

		newPart = initMenuItem(ACTION.NEW_PART, "part");
		newMenu.add(newPart);

		newChapter = initMenuItem(ACTION.NEW_CHAPTER, "chapter");
		newMenu.add(newChapter);

		newChapters = initMenuItem(ACTION.NEW_CHAPTERS, "");
		newMenu.add(newChapters);

		newScene = initMenuItem(ACTION.NEW_SCENE, "scene");
		newMenu.add(newScene);

		newPlot = initMenuItem(ACTION.NEW_PLOT, "plot");
		newMenu.add(newPlot);

		newMenu.addSeparator();

		newPerson = initMenuItem(ACTION.NEW_PERSON, "person");
		newMenu.add(newPerson);

		newGender = initMenuItem(ACTION.NEW_GENDER, "gender");
		newMenu.add(newGender);

		newCategory = initMenuItem(ACTION.NEW_CATEGORY, "category");
		newMenu.add(newCategory);

		newRelation = initMenuItem(ACTION.NEW_RELATION, "relation");
		newMenu.add(newRelation);

		newMenu.addSeparator();

		newLocation = initMenuItem(ACTION.NEW_LOCATION, "location");
		newMenu.add(newLocation);

		newItem = initMenuItem(ACTION.NEW_ITEM, "item");
		newMenu.add(newItem);

		newTag = initMenuItem(ACTION.NEW_TAG, "tag");
		newMenu.add(newTag);

		newMenu.addSeparator();

		newEvent = initMenuItem(ACTION.NEW_EVENT, "event");
		newMenu.add(newEvent);

		newPhoto = initMenuItem(ACTION.NEW_PHOTO, "photo");
		newMenu.add(newPhoto);

		newMenu.addSeparator();

		newFOI = initMenuItem(ACTION.NEW_FOI, "");
		newMenu.add(newFOI);

		newIdea = initMenuItem(ACTION.NEW_IDEA, "idea");
		newMenu.add(newIdea);

		newMemo = initMenuItem(ACTION.NEW_MEMO, "memo");
		newMenu.add(newMemo);

		menuBar.add(newMenu);
	}

	private void initMenuTable() {
		tabMenu = initMenu(ACTION.TABLE);

		tabPov = initMenuItem(VIEWID.TABLE_POV);
		tabMenu.add(tabPov);

		tabPart = initMenuItem(VIEWID.TABLE_PART);
		tabMenu.add(tabPart);

		tabChapter = initMenuItem(VIEWID.TABLE_CHAPTER);
		tabMenu.add(tabChapter);

		tabScene = initMenuItem(VIEWID.TABLE_SCENE);
		tabMenu.add(tabScene);

		tabPlot = initMenuItem(VIEWID.TABLE_PLOT);
		tabMenu.add(tabPlot);

		tabMenu.addSeparator();

		tabPerson = initMenuItem(VIEWID.TABLE_PERSON);
		tabMenu.add(tabPerson);

		tabGender = initMenuItem(VIEWID.TABLE_GENDER);
		tabMenu.add(tabGender);

		tabCategory = initMenuItem(VIEWID.TABLE_CATEGORY);
		tabMenu.add(tabCategory);

		tabRelation = initMenuItem(VIEWID.TABLE_RELATION);
		tabMenu.add(tabRelation);

		tabMenu.addSeparator();

		tabLocation = initMenuItem(VIEWID.TABLE_LOCATION);
		tabMenu.add(tabLocation);

		tabItem = initMenuItem(VIEWID.TABLE_ITEM);
		tabMenu.add(tabItem);

		tabTag = initMenuItem(VIEWID.TABLE_TAG);
		tabMenu.add(tabTag);

		tabMenu.addSeparator();

		tabEvent = initMenuItem(VIEWID.TABLE_EVENT);
		tabMenu.add(tabEvent);

		tabPhoto = initMenuItem(VIEWID.TABLE_PHOTO);
		tabMenu.add(tabPhoto);

		tabMenu.addSeparator();

		tabMemo = initMenuItem(VIEWID.TABLE_MEMO);
		tabMenu.add(tabMemo);

		tabIdea = initMenuItem(VIEWID.TABLE_IDEA);
		tabMenu.add(tabIdea);

		menuBar.add(tabMenu);
	}

	private void initMenuTools() {
		toolsMenu = initMenu(ACTION.TOOLS);

		toolsSearchMenu = initMenuItem(ACTION.TOOLS_SEARCH, "search");
		toolsMenu.add(toolsSearchMenu);

		toolsReplace = initMenuItem(ACTION.TOOLS_REPLACE, "replace");
		toolsMenu.add(toolsReplace);

		toolsChaptersOrder = initMenuItem(ACTION.TOOLS_CHAPTER_ORDER, "sort");
		toolsMenu.add(toolsChaptersOrder);

		toolsScenesOrder = initMenuItem(ACTION.TOOLS_SCENE_RENUMBER, "");
		toolsMenu.add(toolsScenesOrder);

		toolsRenameMenu = initMenu(ACTION.TOOLS_RENAME);

		renameCity = initMenuItem(ACTION.TOOLS_RENAME_CITY, "");
		toolsRenameMenu.add(renameCity);

		renameCountry = initMenuItem(ACTION.TOOLS_RENAME_COUNTRY, "");
		toolsRenameMenu.add(renameCountry);

		toolsMenu.add(toolsRenameMenu);

		menuBar.add(toolsMenu);

	}

	private void initMenuView() {
		viewMenu = initMenu(ACTION.VIEW);

		viewTypist = initMenuItem(VIEWID.VIEW_TYPIST);
		viewMenu.add(viewTypist);

		viewMenu.addSeparator();

		viewChrono = initMenuItem(VIEWID.VIEW_CHRONO);
		viewMenu.add(viewChrono);

		viewWork = initMenuItem(VIEWID.VIEW_WORK);
		viewMenu.add(viewWork);

		viewReading = initMenuItem(VIEWID.VIEW_READING);
		viewMenu.add(viewReading);

		viewManageScene = initMenuItem(VIEWID.VIEW_MANAGE);
		viewMenu.add(viewManageScene);

		viewMemoria = initMenuItem(VIEWID.VIEW_MEMORIA);
		viewMenu.add(viewMemoria);

		viewPaperclip = initMenuItem(VIEWID.VIEW_PAPERCLIP);
		viewMenu.add(viewPaperclip);

		viewScenario = initMenuItem(VIEWID.VIEW_SCENARIO);
		viewMenu.add(viewScenario);

		viewStoryboard = initMenuItem(VIEWID.VIEW_STORYBOARD);
		viewMenu.add(viewStoryboard);

		viewMenu.addSeparator();

		viewGallery = initMenuItem(VIEWID.VIEW_GALLERY);
		viewMenu.add(viewGallery);

		viewPlan = initMenuItem(VIEWID.VIEW_PLANNING);
		viewMenu.add(viewPlan);

		viewTree = initMenuItem(VIEWID.VIEW_TREE);
		viewMenu.add(viewTree);

		viewInfo = initMenuItem(VIEWID.VIEW_INFO);
		viewMenu.add(viewInfo);

		viewNavigation = initMenuItem(VIEWID.VIEW_NAVIGATION);
		viewMenu.add(viewNavigation);

		viewMemos = initMenuItem(VIEWID.VIEW_MEMO);
		viewMenu.add(viewMemos);

		viewMenu.addSeparator();

		viewOptions = initMenuItem(VIEWID.VIEW_OPTIONS);
		viewMenu.add(viewOptions);

		menuBar.add(viewMenu);
	}

	private void initMenuCharts() {
		chartsMenu = initMenu("chart");

		chartPersonsByDate = initMenuItem(VIEWID.CHART_PERSONSBYDATE, "chart");
		chartsMenu.add(chartPersonsByDate);

		chartPersonsByScene = initMenuItem(VIEWID.CHART_PERSONSBYSCENE, "chart");
		chartsMenu.add(chartPersonsByScene);

		chartWIWW = initMenuItem(VIEWID.CHART_WIWW, "chart");
		chartsMenu.add(chartWIWW);

		chartPovsByDate = initMenuItem(VIEWID.CHART_POVSBYDATE, "chart");
		chartsMenu.add(chartPovsByDate);

		chartsMenu.addSeparator();

		chartOccurrenceOfPersons = initMenuItem(VIEWID.CHART_OCCURRENCEOFPERSONS, "chart");
		chartsMenu.add(chartOccurrenceOfPersons);

		chartOccurrenceOfLocations = initMenuItem(VIEWID.CHART_OCCURRENCEOFLOCATIONS, "chart");
		chartsMenu.add(chartOccurrenceOfLocations);

		chartOccurrenceOfItems = initMenuItem(VIEWID.CHART_OCCURRENCEOFITEMS, "chart");
		chartsMenu.add(chartOccurrenceOfItems);

		menuBar.add(chartsMenu);
	}

	private void initMenuParts() {
		partsMenu = initMenu(ACTION.PART);

		partPrevious = initMenuItem(ACTION.PART_PREVIOUS, "nav-previous");
		partsMenu.add(partPrevious);

		partNext = initMenuItem(ACTION.PART_NEXT, "nav-next");
		partsMenu.add(partNext);

		partAll = initMenuCheck(ACTION.PART_ALL);
		partsMenu.add(partAll);

		menuBar.add(partsMenu);
	}

	private void initMenuWindow() {
		windowMenu = initMenu(ACTION.WINDOW);

		windowLoadLayout = initMenu(ACTION.WINDOW_LAYOUT_LOAD);
		windowMenu.add(windowLoadLayout);
		reloadWindowMenu();

		windowSaveLayout = initMenuItem(ACTION.WINDOW_LAYOUT_SAVE, "");
		windowMenu.add(windowSaveLayout);

		windowPredefinedLayout = DockUtil.layoutGetMenu(mainFrame);
		windowMenu.add(windowPredefinedLayout);

		windowResetLayout = initMenuItem(ACTION.WINDOW_LAYOUT_RESET, "");
		windowMenu.add(windowResetLayout);

		windowRefresh = initMenuItem(ACTION.WINDOW_LAYOUT_REFRESH, "");
		windowMenu.add(windowRefresh);

		menuBar.add(windowMenu);
	}

	private void initMenuHelp() {
		helpMenu = initMenu(ACTION.HELP);

		helpDoc = initMenuItem(ACTION.HELP_DOC, "");
		helpMenu.add(helpDoc);

		helpFaq = initMenuItem(ACTION.HELP_FAQ, "");
		helpMenu.add(helpFaq);

		helpHome = initMenuItem(ACTION.HELP_HOMEPAGE, "");
		helpMenu.add(helpHome);

		helpReportBug = initMenuItem(ACTION.HELP_BUG, "");
		helpMenu.add(helpReportBug);

		helpAbout = initMenuItem(ACTION.HELP_ABOUT, "");
		helpMenu.add(helpAbout);

		helpMenu.addSeparator();

		helpCheckUpdates = initMenuItem(ACTION.HELP_UPDATE, "");
		helpMenu.add(helpCheckUpdates);

		helpTrace = initMenuCheck(ACTION.HELP_TRACE);
		helpTrace.setSelected(App.btrace);
		helpMenu.add(helpTrace);

		helpDevTest = initMenuItem(ACTION.HELP_TEST, "");
		if (App.btest) {
			helpMenu.add(helpDevTest);
		}

		helpTranslate = initMenuItem(ACTION.HELP_TRANSLATE, "");
		helpMenu.add(helpTranslate);

		menuBar.add(helpMenu);
	}

	private void refreshButton(JButton bt) {
		String z=bt.getName();
		if (z==null || z.isEmpty()) return;
		String mxz[] = I18N.getMsg(z).split(",");
		bt.setToolTipText(mxz[0]);
		bt.setFont(App.getInstance().fontGetDefault());
	}
	
	private JButton initButton(String ident, String... nic) {
		String z=ident;
		if (z.startsWith("file_")) z=ident.replace("file_", "file.");
		if (z.startsWith("new_")) z=ident.replace("new_", "new.");
		String mx = "menu." + z;
		String mxz[] = I18N.getMsg(mx).split(",");
		JButton m = new JButton();
		m.setName(mx);
		m.setToolTipText(mxz[0]);
		m.setActionCommand(mxz[0]);
		m.addActionListener((ActionEvent evt) -> {
			mainFrame.mainAction.doAction(ident);
		});
		String icn = "small/" + ident.substring(ident.lastIndexOf('.') + 1);
		if (nic != null && nic.length > 0) {
			if (!nic[0].isEmpty()) {
				icn = "small/" + nic[0];
			} else {
				icn = "";
			}
		}
		if (!icn.isEmpty()) {
			m.setIcon(IconUtil.getIcon(icn));
		}
		toolBar.add(m);
		return (m);
	}

	private JButton initButton(ACTION action, String... nic) {
		return (initButton(action.toString(), nic));
	}

	private JButton initButton(VIEWID view, String... nic) {
		return (initButton(view.toString(), nic));
	}

	private void initTbAll() {
		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setName("MainToolbar");
		toolBar.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				toolBarMouseClicked(evt);
			}
		});

		initTbFile();
		toolBar.add(new JToolBar.Separator());
		initTbNew();
		toolBar.add(new JToolBar.Separator());
		initTbTab();
		toolBar.add(new JToolBar.Separator());
		initTbView();
		toolBar.add(new JToolBar.Separator());
		initTbPart();
		toolBar.add(new JToolBar.Separator());
		initTbMemo();
		setToolbar();
	}

	private void initTbFile() {
		btFileNew = initButton(ACTION.FILE_NEW, "file-new");
		btFileOpen = initButton(ACTION.FILE_OPEN, "file-open");
		btFileSave = initButton(ACTION.FILE_SAVE, "file-save");
		btFileExit = initButton(ACTION.FILE_EXIT, "file-exit");
	}

	private void initTbNew() {
		btNewPov = initButton(ACTION.NEW_POV, "pov");
		btNewPart = initButton(ACTION.NEW_PART, "part");
		btNewChapter = initButton(ACTION.NEW_CHAPTER, "chapter");
		btNewScene = initButton(ACTION.NEW_SCENE, "scene");
		btNewPerson = initButton(ACTION.NEW_PERSON, "person");
		btNewGender = initButton(ACTION.NEW_GENDER, "gender");
		btNewRelation = initButton(ACTION.NEW_RELATION, "relation");
		btNewLocation = initButton(ACTION.NEW_LOCATION, "location");
		btNewItem = initButton(ACTION.NEW_ITEM, "item");
		btNewTag = initButton(ACTION.NEW_TAG, "tag");
		btNewEvent = initButton(ACTION.NEW_EVENT, "event");
		btNewPhoto = initButton(ACTION.NEW_PHOTO, "photo");
	}

	private void initTbTab() {
		btTabPov = initButton(VIEWID.TABLE_POV);
		btTabPart = initButton(VIEWID.TABLE_PART);
		btTabChapter = initButton(VIEWID.TABLE_CHAPTER);
		btTabScene = initButton(VIEWID.TABLE_SCENE);
		btTabPerson = initButton(VIEWID.TABLE_PERSON);
		btTabGender = initButton(VIEWID.TABLE_GENDER);
		btTabRelation = initButton(VIEWID.TABLE_RELATION);
		btTabLocation = initButton(VIEWID.TABLE_LOCATION);
		btTabItem = initButton(VIEWID.TABLE_ITEM);
		btTabTag = initButton(VIEWID.TABLE_TAG);
		btTabMemo = initButton(VIEWID.TABLE_MEMO);
		btTabIdea = initButton(VIEWID.TABLE_IDEA);
	}

	private void initTbView() {
		btViewChrono = initButton(VIEWID.VIEW_CHRONO, "chrono");
		btViewWork = initButton(VIEWID.VIEW_WORK, "work");
		btViewManage = initButton(VIEWID.VIEW_MANAGE, "manage");
		btViewReading = initButton(VIEWID.VIEW_READING, "reading");
		btViewMemoria = initButton(VIEWID.VIEW_MEMORIA, "memoria");
		btViewPaperclip = initButton(VIEWID.VIEW_PAPERCLIP, "paperclip");
		btViewStoryboard = initButton(VIEWID.VIEW_STORYBOARD, "storyboard");
		btViewTypist = initButton(VIEWID.VIEW_TYPIST, "typist");
	}

	private void initTbMemo() {
		btMemo = initButton(ACTION.NEW_MEMO, "memo");
		btIdea = initButton(ACTION.NEW_IDEA, "idea");
	}

	private void initTbPart() {
		btPartPrevious = initButton(ACTION.PART_PREVIOUS, "nav-previous");
		btPartNext = initButton(ACTION.PART_NEXT, "nav-next");
	}

	private void toolBarMouseClicked(MouseEvent evt) {
		if (SwingUtilities.isRightMouseButton(evt)) {
			ToolbarDlg dlg = new ToolbarDlg(mainFrame);
			dlg.setVisible(true);
			if (!dlg.isCanceled()) {
				setToolbar();
			}
		}
	}

	private void setToolbar() {
		JButton[] btn = {
			btFileNew, btFileOpen, btFileSave, btFileExit,//4
			btNewPov, btNewPart, btNewChapter, btNewScene,//4
			btNewPerson, btNewGender, btNewRelation,//3
			btNewLocation,//1
			btNewItem,//1
			btNewTag, //1
			btMemo, btIdea, //2
			btTabPov, btTabPart, btTabChapter, btTabScene,//4
			btTabPerson, btTabGender, btTabRelation, //3
			btTabLocation, //1
			btTabItem, //1
			btTabTag, //1
			btTabMemo, btTabIdea,//2
			btViewChrono, btViewWork, btViewReading, btViewManage, btViewMemoria,
			btViewPaperclip, btViewStoryboard, btViewTypist //7
		}; // total 34
		String param = App.getInstance().getPreferences().prefUI.getTb();
		while (param.length() < btn.length) {
			param += "1";// default allParts button are visible
		}
		if (param.length() > btn.length) {
			param = param.substring(0, btn.length);
		}
		for (int i = 0; i < param.length(); i++) {
			btn[i].setVisible((param.charAt(i) == '1'));
		}
		if (mainFrame.book!=null) {
			btPartNext.setVisible(mainFrame.book.parts.size()>1);
			btPartPrevious.setVisible(mainFrame.book.parts.size()>1);
		}
		toolBar.setFloatable(true);
	}

	public void setMenuForBlank() {
		// hide menus from MenuBar
		JMenu menus[] = {
			newMenu, partsMenu, tabMenu,
			chartsMenu, viewMenu, windowMenu, toolsMenu
		};
		for (JMenu m : menus) {
			m.setVisible(false);
		}
		JMenuItem[] submenus = {
			editCopyBlurb, editCopyBook, editCopyEntity,
			fileClose,
			fileProperties, fileRename, fileSave, fileSaveAs, fileExport, fileImport,
			fileBackupNew, fileAssistant
		};
		for (JMenuItem m : submenus) {
			if (m != null) {
				m.setVisible(false);
			}
		}
		JSeparator[] separators = {
			sep1, sep2, sep3, sep4
		};
		for (JSeparator sep : separators) {
			sep.setVisible(false);
		}
		JButton button[] = {
			btFileSave,
			btViewManage,
			btNewPov, btNewPart, btNewGender, btNewRelation,
			btNewChapter, btNewItem, btNewLocation, btNewPerson, btNewScene, btNewTag,
			btPartNext, btPartPrevious,
			btTabPov, btTabPart, btTabGender,
			btTabChapter, btTabItem, btTabLocation, btTabPerson, btTabRelation,
			btTabScene, btTabTag, btTabMemo, btTabIdea,
			btViewTypist, btViewWork, btViewChrono, btViewMemoria, btViewReading, btIdea, btMemo,
			btViewStoryboard, btViewPaperclip
		};
		for (JButton bt : button) {
			bt.setVisible(false);
		}
		if (App.isDev() == false) {
			helpDevTest.setVisible(false);
		}
	}

	private JMenuItem initRecent(PrefFile pf) {
		if (pf != null) {
			File db = new File(pf.file);
			if (db.exists()) {
				JMenuItem m = new JMenuItem(pf.title);
				m.addActionListener((ActionEvent evt) -> {
					App.getInstance().openFile(db, false);
				});
				return (m);
			}
		}
		return (null);
	}

	public void reloadRecentMenu() {
		fileOpenRecent.removeAll();
		List<PrefFile> list = App.getInstance().getPreferences().recentFiles;
		list.forEach((pf) -> {
			File db = new File(pf.file);
			if (db.exists()) {
				JMenuItem m = new JMenuItem(pf.title);
				m.addActionListener((ActionEvent evt) -> {
					App.getInstance().openFile(db, false);
				});
				fileOpenRecent.add(m);
			}
		});
		if (list.size() > 0) {
			fileOpenRecent.addSeparator();
			JMenuItem item = new JMenuItem(I18N.getMsg("menu.file.recent_clear"));
			item.addActionListener((ActionEvent evt) -> {
				App.getInstance().clearRecentFiles();
			});
			fileOpenRecent.add(item);
		}
	}

	@SuppressWarnings("deprecation")
	public void reloadPartMenu() {
		if (!mainFrame.showAllParts) {
			partsMenu.setVisible(true);
			JMenuItem miPreviousPart = partPrevious;
			JMenuItem miNextPart = partNext;
			Part currentPart = mainFrame.getCurrentPart();
			List<Part> parts = mainFrame.book.parts;
			partsMenu.removeAll();
			int pos = 0;
			ButtonGroup group = new ButtonGroup();
			for (Part part : parts) {
				JRadioButtonMenuItem rbmi = new JRadioButtonMenuItem(I18N.getMsg("part") + " " + part.getNumberName());
				rbmi.addActionListener((ActionEvent evt) -> {
					mainFrame.mainAction.partChange(part);
				});
				if (currentPart.id.equals(part.id)) {
					rbmi.setSelected(true);
				}
				group.add(rbmi);
				partsMenu.insert(rbmi, pos);
				++pos;
			}
			partsMenu.insertSeparator(pos++);
			partsMenu.insert(miPreviousPart, pos++);
			partsMenu.insert(miNextPart, pos++);
			partsMenu.insertSeparator(pos++);
			if (parts.size() > 1) {
				partsMenu.insert(partAll, pos++);
				btPartPrevious.setVisible(true);
				btPartNext.setVisible(true);
			} else {
				btPartPrevious.setVisible(false);
				btPartNext.setVisible(false);
			}
		}
	}

	private void selectPartMenu() {
		Component[] comps = partsMenu.getMenuComponents();
		String n = I18N.getMsg("part") + " " + mainFrame.getCurrentPart().getNumberName();
		for (Component comp : comps) {
			if (comp instanceof JRadioButtonMenuItem) {
				JRadioButtonMenuItem rbmi = (JRadioButtonMenuItem) comp;
				if (rbmi.getText().equals(n)) {
					rbmi.setSelected(true);
					return;
				}
			}
		}
	}

	public void reloadWindowMenu() {
		windowLoadLayout.removeAll();
		File dir = new File(App.getPrefDir().getAbsolutePath());
		File[] files = dir.listFiles();
		if (files == null || files.length < 1) {
			return;
		}
		for (File file : files) {
			if (file.isFile()) {
				String name = file.getName();
				if (name.endsWith(".layout")) {
					String str = name.substring(0, name.lastIndexOf(".layout"));
					if (str.equals("last")
							|| str.equals("LastUsedLayout")) {
						continue;
					}
					JMenuItem item = new JMenuItem(str);
					item.addActionListener((ActionEvent evt) -> {
						DockUtil.layoutLoad(mainFrame, name);
					});
					windowLoadLayout.add(item);
				}
			}
		}
	}

	public void reloadMenuToolbar() {
		if (mainFrame.isBlank()) {
			setMenuForBlank();
		} else {
			reloadRecentMenu();
		}
	}
	
	public void refresh() {
		Component[] cs=menuBar.getComponents();
		for (Component c:cs) {
			if (c instanceof JMenu) refreshMenu((JMenu)c);
			if (c instanceof JMenuItem) refreshMenuItem((JMenuItem)c);
			if (c instanceof JCheckBoxMenuItem) refreshMenuCheck((JCheckBoxMenuItem)c);
		}
		cs=toolBar.getComponents();
		for (Component c:cs) {
			if (c instanceof JButton) refreshButton((JButton)c);
		}
	}

	public void setDevFunction() {
		JMenuItem[] submenus = {
			fileImport,
			fileBackupNew, fileBackupRest,
			chartOccurrenceOfItems, chartOccurrenceOfLocations, chartOccurrenceOfPersons,
			chartPersonsByDate, chartPersonsByScene, chartPovsByDate, chartWIWW
		};
		for (JMenuItem m : submenus) {
			if (m != null) {
				m.setEnabled(false);
			}
		}

		JButton button[] = {
			btPartNext,
			btPartPrevious,
			btViewTypist
		};
		for (JButton bt : button) {
			bt.setEnabled(false);
		}

	}

}
