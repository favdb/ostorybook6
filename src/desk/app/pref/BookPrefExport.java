/*
Copyright ???? by ????
Modifications ???? by ???? 

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import static db.entity.AbstractEntity.*;
import db.XmlDB;
import static db.XmlDB.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import db.I18N;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author FaVdB
 */
public class BookPrefExport {

    private String directory = "", lastFile = "", format = "html";
    private boolean partNumber = false,
	    partRoman = false,
	    partTitle = false,
	    chapterBookTitle = false,
	    chapterBreakPage = false,
	    chapterDateLocation = false,
	    chapterNumber = false,
	    chapterRoman = false,
	    chapterTitle = false;

    private boolean sceneDidascalie = false,
	    sceneSeparator = false,
	    sceneTitle = false;

    private String csvQuotes = "\"",
	    csvComma = ";",
	    csvTab = "\t";

    private String htmlCss = "";
    private boolean htmlNav = false,
	    htmlNavImage = false,
	    htmlMultiChapter = false,
	    htmlMultiScene = false,
	    htmlCssUse = false;
    public boolean txtTab = true;
    public String txtSeparator = "";

    public BookPrefExport() {
    }

    public BookPrefExport(String d) {
	directory = d;
    }

    public String getDirectory() {
	return (directory);
    }

    public void setDirectory(String d) {
	directory = d;
    }

    public String getLastFile() {
	return (lastFile);
    }

    public void setLastFile(String d) {
	lastFile = d;
    }

    public void setFormat(String str) {
	format = str;
    }

    public String getFormat() {
	return (format);
    }

    public boolean getChapterBookTitle() {
	return (chapterBookTitle);
    }

    public void setChapterBookTitle(boolean d) {
	chapterBookTitle = d;
    }

    public boolean getChapterBreakPage() {
	return (chapterBreakPage);
    }

    public void setChapterBreakPage(boolean d) {
	chapterBreakPage = d;
    }

    public boolean getChapterDateLocation() {
	return (chapterDateLocation);
    }

    public void setChapterDateLocation(boolean d) {
	chapterBookTitle = d;
    }

    public boolean getPartNumber() {
	return (partNumber);
    }

    public void setPartNumber(boolean d) {
	partNumber = d;
    }

    public boolean getPartRoman() {
	return (partRoman);
    }

    public void setPartRoman(boolean d) {
	partRoman = d;
    }

    public boolean getChapterNumber() {
	return (chapterNumber);
    }

    public void setChapterNumber(boolean d) {
	chapterNumber = d;
    }

    public boolean getChapterRoman() {
	return (chapterRoman);
    }

    public void setChapterRoman(boolean d) {
	chapterRoman = d;
    }

    public boolean getChapterTitle() {
	return (chapterTitle);
    }

    public void setChapterTitle(boolean d) {
	chapterTitle = d;
    }

    public String getCsvComma() {
	return (csvComma);
    }

    public void setCsvComma(String d) {
	csvComma = d;
    }

    public String getCsvQuotes() {
	return (csvQuotes);
    }

    public void setCsvQuotes(String d) {
	csvQuotes = d;
    }

    public String getCsvTab() {
	return (csvTab);
    }

    public void setCsvTab(String d) {
	csvTab = d;
    }

    public String getHtmlCss() {
	return (htmlCss);
    }

    public void setHtmlCss(String d) {
	htmlCss = d;
    }

    public boolean getHtmlCssUse() {
	return (htmlCssUse);
    }

    public void setHtmlCssUse(boolean d) {
	htmlCssUse = d;
    }

    public boolean getHtmlMultiChapter() {
	return (htmlMultiChapter);
    }

    public void setHtmlMultiChapter(boolean d) {
	htmlMultiChapter = d;
    }

    public boolean getHtmlMultiScene() {
	return (htmlMultiScene);
    }

    public void setHtmlMultiScene(boolean d) {
	htmlMultiScene = d;
    }

    public boolean getHtmlNav() {
	return (htmlNav);
    }

    public void setHtmlNav(boolean d) {
	htmlNav = d;
    }

    public boolean getHtmlNavImage() {
	return (htmlNavImage);
    }

    public void setHtmlNavImage(boolean d) {
	htmlNavImage = d;
    }

    public boolean getPartTitle() {
	return (partTitle);
    }

    public void setPartTitle(boolean d) {
	partTitle = d;
    }

    public boolean getSceneDidascalie() {
	return (sceneDidascalie);
    }

    public void setSceneDidascalie(boolean d) {
	sceneDidascalie = d;
    }

    public boolean getSceneSeparator() {
	return (sceneSeparator);
    }

    public void setSceneSeparator(boolean d) {
	sceneSeparator = d;
    }

    public boolean getSceneTitle() {
	return (sceneTitle);
    }

    public void setSceneTitle(boolean d) {
	sceneTitle = d;
    }

    public static BookPrefExport xmlFrom(XmlDB xml, Node paramNode) {
	BookPrefExport p = new BookPrefExport("");
	Node exportNode = xml.findNode(paramNode, "export");
	if (exportNode == null) {
	    System.err.println("no export tag for param tag in" + xml.name);
	    return (p);
	}
	p.setDirectory(attributeGetString(exportNode, "directory"));
	p.setLastFile(attributeGetString(exportNode, "lastfile"));
	Node child = xml.findNode(exportNode, "part");
	if (child != null) {
	    p.setPartNumber(attributeGetBoolean(child, "number"));
	    p.setPartRoman(attributeGetBoolean(child, "roman"));
	    p.setPartTitle(attributeGetBoolean(child, "title"));
	}
	child = xml.findNode(exportNode, "chapter");
	if (child != null) {
	    p.setChapterBookTitle(attributeGetBoolean(child, "booktitle"));
	    p.setChapterBreakPage(attributeGetBoolean(child, "breakPage"));
	    p.setChapterDateLocation(attributeGetBoolean(child, "datelocation"));
	    p.setChapterNumber(attributeGetBoolean(child, "number"));
	    p.setChapterRoman(attributeGetBoolean(child, "roman"));
	    p.setChapterTitle(attributeGetBoolean(child, "title"));
	}
	child = xml.findNode(exportNode, "csv");
	if (child != null) {
	    String x = attributeGetString(child, "csvcomma");
	    switch (x) {
		case "space":
		    p.setCsvComma(" ");
		    break;
		case "colon":
		    p.setCsvComma(",");
		    break;
		default:
		    p.setCsvComma(";");
		    break;
	    }
	    x = attributeGetString(child, "csvquote");
	    switch (x) {
		case "single":
		    p.setCsvQuotes("'");
		    break;
		case "double":
		    p.setCsvQuotes("\"");
		    break;
		default:
		    p.setCsvQuotes("");
		    break;
	    }
	    p.setCsvTab(attributeGetString(child, "csvTab"));
	}
	child = xml.findNode(exportNode, "html");
	if (child != null) {
	    p.setHtmlCss(attributeGetString(child, "css"));
	    p.setHtmlMultiChapter(attributeGetBoolean(child, "multi"));
	    p.setHtmlNav(attributeGetBoolean(child, "nav"));
	    p.setHtmlNavImage(attributeGetBoolean(child, "navimage"));
	}
	child = xml.findNode(exportNode, "scene");
	if (child != null) {
	    p.setSceneDidascalie(attributeGetBoolean(child, "didascalie"));
	    p.setSceneSeparator(attributeGetBoolean(child, "separator"));
	    p.setSceneTitle(attributeGetBoolean(child, "title"));
	}
	return (p);
    }

    void toXml(XmlDB xml, Node node) {
	NodeList child = node.getChildNodes();
	Node e = null;
	for (int i = 0; i < child.getLength(); i++) {
	    if (child.item(i).getNodeName().equals("export")) {
		e = child.item(i);
		break;
	    }
	}
	if (e == null) {
	    e = xml.document.createElement("export");
	    node.appendChild(e);
	}
	xml.attributeSetValue(e, "directory", directory);
	xml.attributeSetValue(e, "lastFile", lastFile);
	xml.attributeSetValue(e, "partNumber", getPartNumber());
	xml.attributeSetValue(e, "partRoman", getPartRoman());
	xml.attributeSetValue(e, "partTitle", getPartTitle());
	xml.attributeSetValue(e, "chapterBookTitle", getChapterBookTitle());
	xml.attributeSetValue(e, "chapterBreakPage", getChapterBreakPage());
	xml.attributeSetValue(e, "chapterDateLocation", getChapterDateLocation());
	xml.attributeSetValue(e, "chapterNumber", getChapterNumber());
	xml.attributeSetValue(e, "chapterRoman", getChapterRoman());
	xml.attributeSetValue(e, "chapterTitle", getChapterTitle());
	switch (getCsvComma()) {
	    case " ":
		xml.attributeSetValue(e, "csvComma", "space");
		break;
	    case ",":
		xml.attributeSetValue(e, "csvComma", "colon");
		break;
	    default:
		xml.attributeSetValue(e, "csvComma", "semicolon");
		break;
	}
	switch (getCsvQuotes()) {
	    case "'":
		xml.attributeSetValue(e, "csvQuotes", "single");
		break;
	    case "\"":
		xml.attributeSetValue(e, "csvQuotes", "double");
		break;
	    default:
		xml.attributeSetValue(e, "csvQuotes", "none");
		break;
	}
	xml.attributeSetValue(e, "csvTab", getCsvTab());
	xml.attributeSetValue(e, "htmlCss", getHtmlCss());
	xml.attributeSetValue(e, "htmlMulti", getHtmlMultiChapter());
	xml.attributeSetValue(e, "htmlNav", getHtmlNav());
	xml.attributeSetValue(e, "htmlNavImage", getHtmlNavImage());
	xml.attributeSetValue(e, "sceneDidascalie", getSceneDidascalie());
	xml.attributeSetValue(e, "sceneSeparator", getSceneSeparator());
	xml.attributeSetValue(e, "sceneTitle", getSceneTitle());
    }

    public String toHtml() {
	StringBuilder b = new StringBuilder(HtmlUtil.getTitle("export", 3));
	//b.append("<p>\n");
	b.append(HtmlUtil.getAttribute("export.directory", getDirectory()));
	b.append(HtmlUtil.getAttribute("export.last_file", getLastFile()));
	b.append(HtmlUtil.getAttribute("export.part.number", getPartNumber()));
	b.append(HtmlUtil.getAttribute("export.part.roman", getPartRoman()));
	b.append(HtmlUtil.getAttribute("export.part.title", getPartTitle()));
	b.append(HtmlUtil.getAttribute("export.chapter.title", getChapterBookTitle()));
	b.append(HtmlUtil.getAttribute("export.chapter.break", getChapterBreakPage()));
	b.append(HtmlUtil.getAttribute("export.chapter.date_location", getChapterDateLocation()));
	b.append(HtmlUtil.getAttribute("export.chapter.number", getChapterNumber()));
	b.append(HtmlUtil.getAttribute("export.chapter.roman", getChapterRoman()));
	b.append(HtmlUtil.getAttribute("export.chapter.title", getChapterTitle()));
	b.append(HtmlUtil.getAttribute("export.csv.comma", getCsvComma()));
	b.append(HtmlUtil.getAttribute("export.csv.quotes", getCsvQuotes()));
	b.append(HtmlUtil.getAttribute("export.csv.tab", getCsvTab()));
	b.append(HtmlUtil.getAttribute("export.html.css", getHtmlCss()));
	b.append(HtmlUtil.getAttribute("export.html.multi", getHtmlMultiChapter()));
	b.append(HtmlUtil.getAttribute("export.html.nav", getHtmlNav()));
	b.append(HtmlUtil.getAttribute("export.html.navimage", getHtmlNavImage()));
	b.append(HtmlUtil.getAttribute("export.scene.didascalie", getSceneDidascalie()));
	b.append(HtmlUtil.getAttribute("export.scene.separator", getSceneSeparator()));
	b.append(HtmlUtil.getAttribute("export.scene.title", getSceneTitle()));
	//b.append("</p>\n");
	return (b.toString());
    }

    public String toText() {
	StringBuilder b = new StringBuilder(I18N.getColonMsg("export"));
	b.append("\n");
	b.append(TextUtil.getAttribute("export.directory", getDirectory()));
	b.append(TextUtil.getAttribute("export.last_file", getLastFile()));
	b.append(TextUtil.getAttribute("export.part.number", getPartNumber()));
	b.append(TextUtil.getAttribute("export.part.roman", getPartRoman()));
	b.append(TextUtil.getAttribute("export.part.title", getPartTitle()));
	b.append(TextUtil.getAttribute("export.chapter.title", getChapterBookTitle()));
	b.append(TextUtil.getAttribute("export.chapter.break", getChapterBreakPage()));
	b.append(TextUtil.getAttribute("export.chapter.date_location", getChapterDateLocation()));
	b.append(TextUtil.getAttribute("export.chapter.number", getChapterNumber()));
	b.append(TextUtil.getAttribute("export.chapter.roman", getChapterRoman()));
	b.append(TextUtil.getAttribute("export.chapter.title", getChapterTitle()));
	b.append(TextUtil.getAttribute("export.csv.comma", getCsvComma()));
	b.append(TextUtil.getAttribute("export.csv.quotes", getCsvQuotes()));
	b.append(TextUtil.getAttribute("export.csv.tab", getCsvTab()));
	b.append(TextUtil.getAttribute("export.html.css", getHtmlCss()));
	b.append(TextUtil.getAttribute("export.html.multi", getHtmlMultiChapter()));
	b.append(TextUtil.getAttribute("export.html.nav", getHtmlNav()));
	b.append(TextUtil.getAttribute("export.html.navimage", getHtmlNavImage()));
	b.append(TextUtil.getAttribute("export.scene.didascalie", getSceneDidascalie()));
	b.append(TextUtil.getAttribute("export.scene.separator", getSceneSeparator()));
	b.append(TextUtil.getAttribute("export.scene.title", getSceneTitle()));
	return (b.toString());
    }

    public String toXml() {
	StringBuilder b = new StringBuilder("        <export ");
	b.append(toXmlSetAttribute(0, "directory", getClean(getDirectory())));
	b.append(toXmlSetAttribute(0, "lastFile", getClean(getLastFile()))).append(">\n");

	b.append("            <part ");
	b.append(toXmlSetAttribute(0, "number", getPartNumber()));
	b.append(toXmlSetAttribute(0, "roman", getPartRoman()));
	b.append(toXmlSetAttribute(0, "title", getPartTitle())).append(" />\n");

	b.append("            <chapter ");
	b.append(toXmlSetAttribute(0, "booktitle", getChapterBookTitle()));
	b.append(toXmlSetAttribute(0, "breakpage", getChapterBreakPage()));
	b.append(toXmlSetAttribute(0, "datelocation", getChapterDateLocation())).append("\n");
	b.append(toXmlSetAttribute(4, "number", getChapterNumber()));
	b.append(toXmlSetAttribute(0, "roman", getChapterRoman()));
	b.append(toXmlSetAttribute(0, "title", getChapterTitle())).append(" />\n");

	b.append("            <csv ");
	String z;
	switch (getCsvComma()) {
	    case " ":
		z = "space";
		break;
	    case ",":
		z = "colon";
		break;
	    default:
		z = "semicolon";
		break;
	}
	b.append(toXmlSetAttribute(0, "csvComma", z));
	switch (getCsvQuotes()) {
	    case "'":
		z = "single";
		break;
	    case "\"":
		z = "double";
		break;
	    default:
		z = "none";
		break;
	}
	b.append(toXmlSetAttribute(0, "quotes", z));
	b.append(toXmlSetAttribute(0, "tab", getCsvTab())).append(" />\n");

	b.append("            <html ");
	b.append(toXmlSetAttribute(0, "css", getHtmlCss()));
	b.append(toXmlSetAttribute(0, "multi", getHtmlMultiChapter()));
	b.append(toXmlSetAttribute(0, "nav", getHtmlNav()));
	b.append(toXmlSetAttribute(0, "navimage", getHtmlNavImage())).append(" />\n");

	b.append("            <scene ");
	b.append(toXmlSetAttribute(0, "didascalie", getSceneDidascalie()));
	b.append(toXmlSetAttribute(0, "separator", getSceneSeparator()));
	b.append(toXmlSetAttribute(0, "title", getSceneTitle())).append(" />\n");

	b.append("        </export>\n");
	return (b.toString());
    }

    public boolean getTxtTab() {
	return (txtTab);
    }

    public void setTxtTab(boolean b) {
	txtTab = b;
    }

    public String getTxtSeparator() {
	return (txtSeparator);
    }

    public void setTxtSeparator(String str) {
	txtSeparator = str;
    }

}
