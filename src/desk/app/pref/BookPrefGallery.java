/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import db.XmlDB;
import static db.XmlDB.*;
import static db.entity.AbstractEntity.getClean;
import static db.entity.AbstractEntity.toXmlSetAttribute;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class BookPrefGallery {

    public static boolean HORIZONTAL = true;

    public Integer zoom = 1;
    public boolean direction = false;//true=horizontal, false=vertical
    public String folder = "";

    public BookPrefGallery() {
    }

    public void setZoom(int nb) {
	this.zoom = nb;
    }

    public Integer getZoom() {
	return (zoom);
    }

    public void setFolder(String dir) {
	this.folder = dir;
    }

    public String getFolder() {
	return (folder);
    }

    public void setDirection(boolean dir) {
	this.direction = dir;
    }

    public boolean getDirection() {
	return (direction);
    }

    public boolean isHorizontal() {
	return (direction);
    }

    public static BookPrefGallery xmlFrom(XmlDB xml, Node param) {
	BookPrefGallery p = new BookPrefGallery();
	Node child = xml.findNode(param, "gallery");
	if (child == null) {
	    System.err.println("no gallery tag for param tag in" + xml.name);
	    return (p);
	}
	p.setZoom(attributeGetInteger(child, "zoom"));
	p.setDirection(attributeGetBoolean(child, "direction"));
	p.setFolder(attributeGetString(child, "folder"));
	return (p);
    }

    public void toXml(XmlDB xml, Node node) {
	NodeList child = node.getChildNodes();
	Node e = null;
	for (int i = 0; i < child.getLength(); i++) {
	    if (child.item(i).getNodeName().equals("gallery")) {
		e = child.item(i);
		break;
	    }
	}
	if (e == null) {
	    e = xml.document.createElement("gallery");
	    node.appendChild(e);
	}
	xml.attributeSetValue(e, "zoom", zoom);
	xml.attributeSetValue(e, "direction", direction);
	xml.attributeSetValue(e, "folder", folder);
    }

    public String toHtml() {
	StringBuilder b = new StringBuilder();
	b.append(HtmlUtil.getTitle("gallery", 3));
	//b.append("<p>\n");
	b.append(HtmlUtil.getAttribute("zoom", zoom));
	b.append(HtmlUtil.getAttribute("direction", direction));
	b.append(HtmlUtil.getAttribute("folder", folder));
	//b.append("</p>\n");
	return (b.toString());
    }

    public String toXml() {
	StringBuilder b = new StringBuilder("        <gallery ");
	b.append(toXmlSetAttribute(0, "zoom", getClean(getZoom())));
	b.append(toXmlSetAttribute(0, "direction", getClean(getDirection())));
	b.append(toXmlSetAttribute(0, "folder", getClean(getFolder())));
	b.append(" />\n");
	return (b.toString());
    }

}
