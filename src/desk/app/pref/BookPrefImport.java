/*
Copyright ???? by ????
Modifications ???? by ???? 

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import static db.entity.AbstractEntity.*;
import db.XmlDB;
import static db.XmlDB.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.html.HtmlUtil;

/**
 *
 * @author FaVdB
 */
public class BookPrefImport {

    private String directory = "";
    private String file = "";

    public BookPrefImport() {
    }

    public void setDirectory(String d) {
	directory = d;
    }

    public String getDirectory() {
	return (directory);
    }

    public void setFile(String d) {
	file = d;
    }

    public String getFile() {
	return (file);
    }

    public String toHtml() {
	StringBuilder b = new StringBuilder(HtmlUtil.getTitle("import", 3));
	//b.append("<p>\n");
	b.append(HtmlUtil.getAttribute("import.directory", directory));
	b.append(HtmlUtil.getAttribute("import.file", file));
	//b.append("</p>\n");
	return (b.toString());
    }

    public String toXml() {
	StringBuilder b = new StringBuilder("        <import ");
	b.append(toXmlSetAttribute(0, "directory", getClean(directory)));
	b.append(toXmlSetAttribute(0, "file", getClean(file)));
	b.append(" />\n");
	return (b.toString());
    }

    public static BookPrefImport xmlFrom(XmlDB xml, Node param) {
	BookPrefImport p = new BookPrefImport();
	Node child = xml.findNode(param, "import");
	if (child == null) {
	    System.out.println("no import tag for param tag in" + xml.name);
	    return (p);
	}
	p.setDirectory(attributeGetString(child, "directory"));
	p.setFile(attributeGetString(child, "file"));
	return (p);
    }

    public void toXml(XmlDB xml, Node node) {
	NodeList child = node.getChildNodes();
	Node e = null;
	for (int i = 0; i < child.getLength(); i++) {
	    if (child.item(i).getNodeName().equals("import")) {
		e = child.item(i);
		break;
	    }
	}
	if (e == null) {
	    e = xml.document.createElement("import");
	    node.appendChild(e);
	}
	xml.attributeSetValue(e, "directory", directory);
	xml.attributeSetValue(e, "file", file);
    }

}
