/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import db.I18N;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.JToolBar;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class AppPrefUI {

	public final static String 
			DEF_FONTDEFAULT = "Dialog,12,0",
			DEF_FONTEDITOR = "Time,12,0",
			DEF_FONTMONO = "Monospaced,12,0",
			DEF_TOOLBAR = "North;0000001110011000110100110000101101",
			DEF_WINDOW = "800,600;0,0;0";
	public final static boolean
			DEF_TYPIST = false,
			DEF_MAXIMIZED=false;

	private Dimension size = new Dimension(800, 600);
	private Point loc = new Point(0, 0);
	private String
			toolbar,
			tbOrientation,
			tb,
			fontDefault = DEF_FONTDEFAULT,
			fontEditor = DEF_FONTEDITOR,
			fontMono=DEF_FONTMONO,
			window = DEF_WINDOW;
	private boolean
			maximized = DEF_MAXIMIZED,
			typist = DEF_TYPIST;

	public AppPrefUI(AppPref pref) {
		load(pref);
	}

	public String getToolbar() {
		return (tbOrientation+","+tb);
	}

	public void setToolbar(String s) {
		toolbar = s;
		if (s==null || s.isEmpty()) {
			s=DEF_TOOLBAR;
		}
		String x[]=s.split(";");
		setTbOrientation(x[0]);
		setTb(x[1]);
	}

	public void setTbOrientation(JToolBar tb) {
		String s = "North";
		if (tb.getOrientation() == 0) {
			if (tb.getY() != 0) {
				s = "South";
			}
		} else if (tb.getX() != 0) {
			s = "East";
		} else {
			s = "West";
		}
		tbOrientation = s;
	}

	public void setTb(String s) {
		tb=s;
	}
	
	public String getTb() {
		return(tb);
	}

	void setTbOrientation(String s) {
		if (s.isEmpty()) {
			tbOrientation = "North";
		} else {
			tbOrientation = s;
		}
	}

	public String getTbOrientation() {
		return (tbOrientation);
	}
	
	public String getWindow() {
		return (window);
	}

	public void setWindow(String s) {
		window = s;
	}

	public String getFontDefault() {
		return (fontDefault);
	}

	public void setFontDefault(String s) {
		fontDefault = s;
	}

	public String getFontEditor() {
		return (fontEditor);
	}

	public void setFontEditor(String s) {
		fontEditor = s;
	}

	public String getFontMono() {
		return (fontMono);
	}

	public void setFontMono(String s) {
		fontMono = s;
	}

	public Dimension getSize() {
		return (size);
	}

	public void setSize(Dimension s) {
		size = s;
	}

	public void setSize(String x) {
		if (x.isEmpty()) {
			String s[] = DEF_WINDOW.split(";");
			String sx[]=s[0].split(",");
			size = new Dimension(Integer.getInteger(sx[0]), Integer.getInteger(sx[1]));
		} else {
			String s[] = x.split(",");
			size = new Dimension(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
		}
	}

	public Point getLoc() {
		return (loc);
	}

	public void setLoc(Point s) {
		loc = new Point(s.x, s.y);
	}

	public void setLoc(String x) {
		if (x.isEmpty()) {
			loc = new Point(0, 0);
		} else {
			String s[] = x.split(",");
			loc = new Point(Integer.parseInt(s[0]), Integer.parseInt(s[1]));
		}
	}

	public boolean getMaximized() {
		return (maximized);
	}

	public void setMaximized(boolean s) {
		maximized = s;
	}

	public void setMaximized(String s) {
		maximized = s.equals("true");
	}

	public boolean getTypist() {
		return (typist);
	}

	public void setTypist(boolean s) {
		typist = s;
	}

	public void setTypist(String s) {
		typist = s.equals("true");
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder();
		b.append(HtmlUtil.getTitle("view.ui", 3));
		b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("view.ui.window", window));
		b.append(HtmlUtil.getAttribute("view.ui.toolbar", toolbar));
		b.append(HtmlUtil.getAttribute("view.ui.font_default", fontDefault));
		b.append(HtmlUtil.getAttribute("view.ui.font_editor", fontEditor));
		b.append(HtmlUtil.getAttribute("view.ui.font_mono", fontMono));
		b.append("</p>\n");
		return (b.toString());
	}

	public String toText() {
		StringBuilder b = new StringBuilder();
		b.append(I18N.getColonMsg("view.ui")).append("\n");
		b.append(TextUtil.getAttribute("view.ui.window", window));
		b.append(TextUtil.getAttribute("view.ui.toolbar", toolbar));
		b.append(TextUtil.getAttribute("view.ui.font_default", fontDefault));
		b.append(TextUtil.getAttribute("view.ui.font_editor", fontEditor));
		b.append(TextUtil.getAttribute("view.ui.font_mono", fontMono));
		b.append("\n");
		return (b.toString());
	}

	public void save(AppPref pref) {
		String w = String.format("%d,%d;%d,%d,%s", size.width, size.height, loc.x, loc.y, (maximized ? "1" : "0"));
		pref.setString(AppPref.Key.WINDOW, w);
		pref.setString(AppPref.Key.TOOLBAR, toolbar);
		pref.setString(AppPref.Key.TYPIST_USE, (typist ? "1" : "0"));
		pref.setString(AppPref.Key.FONT_DEFAULT, fontDefault);
		pref.setString(AppPref.Key.FONT_EDITOR, fontEditor);
		pref.setString(AppPref.Key.FONT_MONO, fontMono);
	}

	private void load(AppPref pref) {
		String v = pref.getString(AppPref.Key.WINDOW);
		if (v != null && !v.isEmpty()) {
			String z[] = v.split(";");
			if (z.length > 0) {
				setSize(z[0]);
			}
			if (z.length > 1) {
				setLoc(z[1]);
			}
			if (z.length > 2) {
				maximized = (z[2].equals("1"));
			}
		}
		v = pref.getString(AppPref.Key.TOOLBAR);
		if (v != null && !v.isEmpty()) {
			setToolbar(v);
		}
		v = pref.getString(AppPref.Key.FONT_DEFAULT);
		if (v != null && !v.isEmpty()) {
			setFontDefault(v);
		}
		v = pref.getString(AppPref.Key.FONT_EDITOR);
		if (v != null && !v.isEmpty()) {
			setFontEditor(v);
		}
		v = pref.getString(AppPref.Key.FONT_MONO);
		if (v != null && !v.isEmpty()) {
			setFontMono(v);
		}
	}

}
