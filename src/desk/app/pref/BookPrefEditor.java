/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import static db.entity.AbstractEntity.*;
import db.XmlDB;
import static db.XmlDB.*;
import static db.XmlDB.attributeGetString;
import org.w3c.dom.Node;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class BookPrefEditor {

    public boolean modless = false,
	    xUse = false;
    public String xName = "",
	    xExtend = "",
	    xTemplate = "";

    public String toHtml() {
	StringBuilder b = new StringBuilder();
	b.append(HtmlUtil.getTitle("editor", 3));
	b.append(HtmlUtil.getAttribute("editor.nonmodal", modless));
	b.append(HtmlUtil.getAttribute("editor.xuse", xUse));
	b.append(HtmlUtil.getAttribute("editor.xname", xName));
	b.append(HtmlUtil.getAttribute("editor.xextend", xExtend));
	b.append(HtmlUtil.getAttribute("editor.xtemplate", xTemplate));
	return (b.toString());
    }

    public String toXml() {
	StringBuilder b = new StringBuilder("        <editor ");
	b.append(toXmlSetAttribute(0, "modless", getClean(modless)));
	b.append(toXmlSetAttribute(0, "xUse", getClean(xUse)));
	b.append(toXmlSetAttribute(0, "xName", getClean(xName)));
	b.append(toXmlSetAttribute(0, "xExtend", getClean(xExtend)));
	b.append(toXmlSetAttribute(0, "xTemplate", getClean(xTemplate)));
	b.append(" />\n");
	return (b.toString());
    }

    public static BookPrefEditor xmlFrom(XmlDB xml, Node param) {
	BookPrefEditor p = new BookPrefEditor();
	Node child = xml.findNode(param, "editor");
	if (child == null) {
	    System.out.println("no editor tag for param tag in" + xml.name);
	    return (p);
	}
	p.modless = attributeGetBoolean(child, "modless");
	p.xUse = attributeGetBoolean(child, "xUse");
	p.xName = attributeGetString(child, "xName");
	p.xExtend = attributeGetString(child, "xExtend");
	p.xTemplate = attributeGetString(child, "xTemplate");
	return (p);
    }

    public void toXml(XmlDB xml, Node node) {
	Node n = childGetNode(node, "editor");
	xml.attributeSetValue(n, "modless", modless);
	xml.attributeSetValue(n, "xUse", xUse);
	xml.attributeSetValue(n, "xName", xName);
	xml.attributeSetValue(n, "xExtend", xExtend);
	xml.attributeSetValue(n, "xTemplate", xTemplate);
    }

}
