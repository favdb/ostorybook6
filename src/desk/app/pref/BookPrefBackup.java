/*
Copyright ???? by ????
Modifications ???? by ???? 

This file is part of oStorybook.

    oStorybook is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    oStorybook is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with oStorybook.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import static db.entity.AbstractEntity.getClean;
import static db.entity.AbstractEntity.toXmlSetAttribute;
import db.XmlDB;
import static db.XmlDB.*;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import tools.html.HtmlUtil;

/**
 *
 * @author FaVdB
 */
public class BookPrefBackup {

    private String directory = "";
    private boolean auto;
    private boolean increment;

    public BookPrefBackup() {
    }

    public void setDirectory(String d) {
	directory = d;
    }

    public String getDirectory() {
	return (directory);
    }

    public void setAuto(boolean d) {
	auto = d;
    }

    public boolean getAuto() {
	return (auto);
    }

    public void setIncrement(boolean d) {
	increment = d;
    }

    public boolean getIncrement() {
	return (increment);
    }

    public static BookPrefBackup xmlFrom(XmlDB xml, Node param) {
	BookPrefBackup p = new BookPrefBackup();
	Node child = xml.findNode(param, "backup");
	if (child == null) {
	    System.err.println("no backup tag for param tag in" + xml.name);
	    return (p);
	}
	p.setDirectory(attributeGetString(child, "directory"));
	p.setAuto(attributeGetBoolean(child, "auto"));
	p.setIncrement(attributeGetBoolean(child, "increment"));
	return (p);
    }

    void toXml(XmlDB xml, Node node) {
	Node n = childGetNode((Element) node, "backup");
	xml.attributeSetValue(n, "directory", directory);
	xml.attributeSetValue(n, "auto", auto);
	xml.attributeSetValue(n, "increment", increment);
    }

    public String toHtml() {
	StringBuilder b = new StringBuilder();
	b.append(HtmlUtil.getTitle("backup", 3));
	//b.append("<p>\n");
	b.append(HtmlUtil.getAttribute("directory", directory));
	b.append(HtmlUtil.getAttribute("backup.auto", auto));
	b.append(HtmlUtil.getAttribute("backup.increment", increment));
	//b.append("</p>\n");
	return (b.toString());
    }

    public String toXml() {
	StringBuilder b = new StringBuilder("        <backup ");
	b.append(toXmlSetAttribute(0, "directory", getClean(getDirectory())));
	b.append(toXmlSetAttribute(0, "auto", getClean(getAuto())));
	b.append(toXmlSetAttribute(0, "increment", getClean(getIncrement())));
	b.append(" />\n");
	return (b.toString());
    }

}
