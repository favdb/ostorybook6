/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import desk.app.App;
import db.XmlDB;
import desk.net.Updater;
import desk.panel.work.PrefViewWork;
import desk.panel.chrono.PrefViewChrono;
import desk.panel.manage.PrefViewManage;
import desk.panel.memos.PrefViewMemo;
import desk.panel.memoria.PrefViewMemoria;
import desk.panel.reading.PrefViewReading;
import desk.panel.storyboard.PrefViewStoryboard;
import tools.ListUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author favdb
 */
public class AppPref {

	List<PrefValue> prefValues = new ArrayList<>();
	public List<PrefFile> recentFiles = new ArrayList<>();
	private final String configFile = App.getPrefFile().getAbsolutePath();
	private String language="en_US";
	private String lastFile="", lastDir="";
	public AppPrefUI prefUI;
	public PrefViewWork book;
	public PrefViewChrono chrono;
	public PrefViewManage manage;
	public PrefViewMemo memo;
	public PrefViewMemoria memoria;
	public PrefViewReading reading;
	public PrefViewStoryboard storyboard;

	public AppPref() {
		load();
	}
	
	public String getLanguage() {
		return(language);
	}
	
	public void setLanguage(String language) {
		this.language=language;
	}
	
	public String getLastFile(){
		return(lastFile);
	}
	
	public void setLastFile(String file) {
		this.lastFile=file;
	}
	
	public String getLastDir() {
		return(lastDir);
	}
	
	public void setLastDir(String dir) {
		lastDir=dir;
	}

	public enum Key {
		VERSION("Version", "6.00"),
		DATETIMEFORMAT("FormatDateTime", "yyyy-MM-dd HH:mm:ss"),
		OPEN_LAST_DIR("LastOpenDir", System.getProperty("user.home")),//last directory opened
		OPEN_LAST_FILE("LastOpenFile", "false"),//open the last opened file at start time
		OPEN_LAST_FILENAME("LastOpenFileName", ""),//last opened file name
		RECENT_FILES("RecentFiles", ""),//list of the recent opened files
		CONFIRM_EXIT("ConfirmExit", "true"),//confirm when exit
		LANG("Language", "en_US"),//language of the UI
		SPELLING("Spelling", "en"),//language for the spelling check
		FIRST_START("FirstStart", "true"),//is first start for this version
		UPDATER_DO("UpdaterDo", Updater.DEFAULT_DO),//is update to chack
		UPDATER_LAST("UpdaterLast", ""),//date of the last check for update
		EXPORT_PREF("ExportPref", "html"),//export préférences, see the exim.Export for the format
		IMPORT_PREF("ImportPref", "xml"),//import préférences, see the exim.Import for the format
		VIEW_BOOK("ViewBook", "10,40"),// book view preferences
		VIEW_CHRONO("ViewChrono", "1,0,40"),// chrono view preferences
		VIEW_MANAGE("ViewManage", "1,0"),// manage view preferences
		VIEW_MEMO("ViewMemo", "1"),// memo view preferences
		VIEW_MEMORIA("ViewMemoria", "1"),// memoria view preferences
		VIEW_READING("ViewReading", "40,12,0000000000"),// reading view preferences
		VIEW_STORYBOARD("ViewStoryboard", "1"),// storyboard view preferences
		MEMORY("Memory", "true"),//see memory usage in the statusBar
		BOOK_LASTAUTHORNAME("LastAuthorName", ""),//last author name used
		BOOK_LASTCOPYRIGHT("LastCopyright", ""),//last copyright used
		ASSISTANT("Assistant", ""),//last assistant file used
		BACKUP("Backup", ""),//last backup parameters, see xml.Backup for the format
		LAF("LookAndFeel", "cross"),
		LAYOUT_LAST("LastLayout", ""),
		LAYOUT_LIST("Layouts", ""),
		FONT_DEFAULT("FontDefault", "Dialog,12,0"),
		FONT_EDITOR("FontEditor", "Time,12,0"),
		FONT_MONO("FontMono", "Monospaced,12,0"),
		TOOLBAR("ToolBar", "North;1110011100110101100111001101000111110"),
		TYPIST_USE("TypistUse", "0"),
		OSM("OSMurl", "http://www.openstreetmap.org"),
		WINDOW("Window", "840,600;0,0;1");
		final private String text;
		final private String def;

		private Key(String text, String def) {
			this.text = text;
			this.def = def;
		}

		public String getDef() {
			return (def);
		}

		@Override
		public String toString() {
			return text;
		}
	}

	public void dump() {
		prefValues.forEach((preference) -> {
			System.out.println(preference.toString());
		});
	}

	public void set(String key, String value) {
		setString(key, value);
	}

	public void setString(AppPref.Key key, String value) {
		setString(key.toString(), value);
	}

	public void setString(String key, String value) {
		//App.trace("setting pref value: "+key+"="+value);
		boolean find = false;
		for (PrefValue v : prefValues) {
			if (v.key.equals(key)) {
				//App.trace("replacing value \""+v.value+"\" by \""+value+"\"");
				v.set(value);
				find = true;
				break;
			}
		}
		if (!find) {
			//App.trace("adding pref");
			prefValues.add(new PrefValue(key, value));
		}
	}

	public void setInteger(Key key, Integer value) {
		setString(key, value.toString());
	}

	public void setInteger(String key, Integer value) {
		setString(key, value.toString());
	}

	public void setBoolean(String key, boolean value) {
		setString(key, (value ? "1" : "0"));
	}

	public void setBoolean(AppPref.Key key, boolean sel) {
		setBoolean(key.toString(), sel);
	}

	public void recentFilesLoad() {
		recentFiles = new ArrayList<>();
		String v = getString(Key.RECENT_FILES);
		if (!v.isEmpty()) {
			String vfiles[] = v.split(";");
			for (String vfile : vfiles) {
				String vx[] = vfile.split("\\|");
				if (vx.length == 2) {
					recentFiles.add(new PrefFile(vx[0], vx[1]));
				}
			}
		}
	}

	public void recentFilesAdd(XmlDB xml, String title) {
		for (int i = 0; i < recentFiles.size(); i++) {
			PrefFile pf = recentFiles.get(i);
			if (pf.file.equals(xml.getPath())) {
				recentFiles.remove(i);
				break;
			}
		}
		recentFiles.add(0, new PrefFile(xml.getPath(), title));
		recentFilesSave();
	}

	public void recentFilesSave() {
		StringBuilder b = new StringBuilder();
		recentFiles.forEach((pf) -> {
			if (!b.toString().isEmpty()) {
				b.append(";");
			}
			b.append(pf.toString());
		});
		setString(Key.RECENT_FILES, b.toString());
	}

	public String get(String key, String defvalue) {
		return (getString(key, defvalue));
	}

	public String getString(Key key, String defvalue) {
		return (getString(key.toString(), defvalue));
	}

	public String getString(Key key) {
		return (getString(key.toString(), key.getDef()));
	}

	public String getString(String key, String defvalue) {
		for (PrefValue v : prefValues) {
			if (v.key.equals(key)) {
				return (v.value);
			}
		}
		if (!"".equals(defvalue)) {
			prefValues.add(new PrefValue(key, defvalue));
		}
		return (defvalue);
	}

	public Integer getInteger(Key key, Integer val) {
		String r = getString(key, val.toString());
		return (Integer.parseInt(r));
	}

	public Integer getInteger(String key, Integer val) {
		String r = getString(key, val.toString());
		return (Integer.parseInt(r));
	}

	public boolean getBoolean(Key key, boolean b) {
		return (getBoolean(key.toString(), b));
	}

	public boolean getBoolean(String key, boolean val) {
		String r = getString(key, (val ? "1" : "0"));
		if (r == null) {
			return (false);
		}
		return (("1".equals(r)));
	}

	public boolean getBoolean(String key) {
		return(getBoolean(key, false));
	}

	public void removeString(String keyName) {
		for (int i = 0; i < prefValues.size(); i++) {
			if (prefValues.get(i).key.equals(keyName)) {
				prefValues.remove(i);
				save();
				return;
			}
		}
	}

	public List<String> getStringList(String key) {
		List<String> list = new ArrayList<>();
		String str = getString(key, "");
		if (str.isEmpty()) {
			return (list);
		}
		String values[] = str.split(",");
		list.addAll(Arrays.asList(values));
		return (list);
	}

	public void setStringList(Key key, List<String> list) {
		if (list.isEmpty()) {
			setString(key, "");
			return;
		}
		setString(key, ListUtil.join(list, ";"));
	}

	public List<String> getList(Key key) {
		List<String> list = new ArrayList<>();
		String v = getString(Key.LAYOUT_LIST);
		if (!v.isEmpty()) {
			String values[] = v.split("#");
			list.addAll(Arrays.asList(values));
		}
		return (list);
	}

	public void setList(Key key, List<String> values) {
		@SuppressWarnings("unchecked")
		List<String> uniqueList = (List<String>) ListUtil.setUnique(values);
		try {
			if (uniqueList.size() > 10) {
				uniqueList = uniqueList.subList(uniqueList.size() - 10, uniqueList.size());
			}
		} catch (IndexOutOfBoundsException e) {
		}
		String val = ListUtil.join(uniqueList, "#");
		setString(key, val);
	}

	public final void load() {
		File file = new File(configFile);
		if (!file.exists()) {
			create(file);
			return;
		}
		App.msg("Load Preferences from " + file.getAbsolutePath());
		try {
			InputStream ips = new FileInputStream(configFile);
			InputStreamReader ipsr = new InputStreamReader(ips);
			try ( BufferedReader br = new BufferedReader(ipsr)) {
				String ligne;
				while ((ligne = br.readLine()) != null) {
					String[] s = ligne.split("=");
					if (s.length < 1) {
						continue;
					}
					if (s.length < 2) {
						prefValues.add(new PrefValue(s[0], ""));
					} else {
						prefValues.add(new PrefValue(s[0], s[1]));
					}
					PrefValue pref = prefValues.get(prefValues.size() - 1);
				}
				br.close();
				recentFilesLoad();
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
		language=getString(Key.LANG);
		lastFile=getString(Key.OPEN_LAST_FILENAME);
		book = new PrefViewWork(this);
		chrono = new PrefViewChrono(this);
		manage = new PrefViewManage(this);
		memo = new PrefViewMemo(this);
		memoria = new PrefViewMemoria(this);
		reading = new PrefViewReading(this);
		storyboard = new PrefViewStoryboard(this);
		prefUI = new AppPrefUI(this);
	}

	private void create(File file) {
		//App.trace("Create new Preferences in " + file.getAbsolutePath());
		try {
			App.getPrefDir().mkdir();
			file.createNewFile();
		} catch (IOException e) {
			System.err.println("Unable to create new file " + file.getAbsolutePath());
		}
		prefUI = new AppPrefUI(this);
		prefValues.add(new PrefValue(Key.VERSION, db.DB.DB_VERSION));
		save();
	}

	public void save() {
		setString(Key.LANG,language);
		setString(Key.OPEN_LAST_FILENAME,lastFile);
		prefUI.save(this);
		String newline = System.getProperty("line.separator");
		prefValues.sort((PrefValue s1, PrefValue s2) -> s1.toString().compareTo(s2.toString()));
		try {
			OutputStream f = new FileOutputStream(configFile);
			for (PrefValue p : prefValues) {
				f.write((p.toString() + newline).getBytes());
			}
		} catch (FileNotFoundException ex) {
			App.error("Unable to save Preferences (file not found)", ex);
		} catch (IOException ex) {
			App.error("Unable to save Preferences", ex);
		}
	}

	private static class PrefValue {

		String key;
		String value;

		public PrefValue(String k, String v) {
			key = k;
			value = v;
		}

		public PrefValue(Key k, String v) {
			key = k.toString();
			value = v;
		}

		private PrefValue(Key key) {
			this.key = key.toString();
			this.value = key.getDef();
		}

		public void set(String v) {
			value = v;
		}

		public String get() {
			return (value);
		}

		@Override
		public String toString() {
			return (key + "=" + value);
		}
	}

	public static class PrefFile {

		public String file, title;

		public PrefFile(String file, String title) {
			this.file = file;
			this.title = title;
		}

		@Override
		public String toString() {
			return (file + "|" + title);
		}
	}

}
