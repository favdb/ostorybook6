/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.app.pref;

import db.XmlDB;
import static db.XmlDB.*;
import static db.entity.AbstractEntity.getClean;
import static db.entity.AbstractEntity.toXmlSetAttribute;
import db.entity.Part;
import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import tools.html.HtmlUtil;

/**
 * gestion des préférences associées à un projet
 *
 * @author favdb
 */
public class BookPref {

    public String date_short = "yyyy-MM-dd HH:mm:SS";
    public String date_long = "yyyy MMMM dddd dd HH:mm:SS";
    private Long last_part = -1L;
    private boolean typist_use = false, all_part=false;
    public BookPrefBackup Backup;
    public BookPrefEditor Editor;
    public BookPrefGallery Gallery;
    public BookPrefExport Export;
    public BookPrefImport Import;
    private boolean modified = false;
    private String folder = "";

    public void setModified() {
        modified = true;
    }

    public enum BK {
        DATE_SHORT("date_short", "yyyy-MM-dd HH:mm:SS"),
        DATE_LONG("date_long", "yyyy MMMM dddd dd HH:mm:SS"),
        IMAGE_LAST_DIR("image_last_dir", ""),
        PART_LAST("part_last", "1"),
        PART_SHOW_ALL("part_show_all", "false"),
        TYPIST_USE("typist_use", "false");
        final private String text;
        final private String def;

        private BK(String text, String def) {
            this.text = text;
            this.def = def;
        }

        public String getDefStr() {
            return (def);
        }

        public Boolean getDefBool() {
            return (def.equals("true"));
        }

        public Integer getDefInt() {
            return (Integer.parseInt(def));
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum Format {
        HTML("html"),
        XML("xml"),
        TXT("txt"),
        CSV("csv"),
        PDF("pdf");
        final private String text;

        private Format(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public BookPref(String folder) {
        this.folder = folder;
        Backup = new BookPrefBackup();
        Editor = new BookPrefEditor();
        Export = new BookPrefExport();
        Import = new BookPrefImport();
        Gallery = new BookPrefGallery();
        load(folder);
        modified = false;
    }

    public BookPref() {
	//Assistant=new ParamAssistant();
        Backup = new BookPrefBackup();
        Editor = new BookPrefEditor();
        Export = new BookPrefExport();
        Import = new BookPrefImport();
        Gallery = new BookPrefGallery();
        modified = false;
    }

    public void setTypistUse(boolean v) {
        typist_use=v;
    }

    public boolean getTypistUse() {
        return(typist_use);
    }

    public void setAllPart(boolean v) {
        all_part=v;
    }

    public boolean getAllPart() {
        return(all_part);
    }

    public void setCurrentPart(Part part) {
        if (part==null) last_part=-1L;
	else last_part=part.id;
    }

    public void setCurrentPart(Long id) {
        last_part=id;
    }

    public Long getCurrentPart() {
        return (last_part);
    }

    public void setDateShort(String v) {
        date_short=v;
    }

    public String getDateShort() {
        return(date_short);
    }

    public void setDateLong(String v) {
        date_long=v;
    }

    public String getDateLong() {
        return(date_long);
    }

    public BookPrefBackup getParamBackup() {
        return (Backup);
    }

    public void setParamBackup(BookPrefBackup b) {
        Backup = b;
    }

    public BookPrefExport getParamExport() {
        return (Export);
    }

    public void setParamExport(BookPrefExport b) {
        Export = b;
    }

    public BookPrefEditor getParamEditor() {
        return (Editor);
    }

    public void setParamEditor(BookPrefEditor b) {
        Editor = b;
    }

    public BookPrefImport getParamImport() {
        return (Import);
    }

    public void setParamImport(BookPrefImport b) {
        Import = b;
    }

    public BookPrefGallery getParamGallery() {
        return (Gallery);
    }

    public void setParamGallery(BookPrefGallery b) {
        Gallery = b;
    }

    public void load(XmlDB xml) {
        Node n = xml.findNode(xml.rootNode, "param");
        if (n == null) {
            System.err.println("no 'param' tag in " + xml.name);
        }
        fromXml(xml, n);
    }

    public void fromXml(XmlDB xml, Node node) {
        //App.trace("BookParam.fromXml(xml,paramNode)");
	setCurrentPart(attributeGetLong(node, "last_part"));
	setAllPart(attributeGetBoolean(node, "all_part"));
	setDateShort(attributeGetString(node, "date_short"));
	setDateLong(attributeGetString(node, "date_long"));
        setParamBackup(BookPrefBackup.xmlFrom(xml, node));
        setParamEditor(BookPrefEditor.xmlFrom(xml, node));
        setParamExport(BookPrefExport.xmlFrom(xml, node));
        setParamImport(BookPrefImport.xmlFrom(xml, node));
        setParamGallery(BookPrefGallery.xmlFrom(xml, node));
    }

    public void xmlTo(XmlDB xml) {
        //App.trace("BookParam.xmlTo(xml)");
        Node n = childGetNode(xml.rootNode, "param");
        if (n == null) {
            n = xml.document.createElement("param");
            xml.rootNode.appendChild(n);
        }
	xml.attributeSetValue(n, "last_part", last_part);
	xml.attributeSetValue(n, "all_part", all_part);
	xml.attributeSetValue(n, "typist_use", typist_use);
	xml.attributeSetValue(n, "date_short", date_short);
	xml.attributeSetValue(n, "date_long", date_long);
        Backup.toXml(xml, n);
        Editor.toXml(xml, n);
        Export.toXml(xml, n);
        Import.toXml(xml, n);
        Gallery.toXml(xml, n);
    }

    public String toHtmlDetail() {
        StringBuilder b = new StringBuilder(HtmlUtil.getTitle("book.param", 2));
	b.append(HtmlUtil.getAttribute("book.param_last_part", last_part));
	b.append(HtmlUtil.getAttribute("book.param_all_part", all_part));
	b.append(HtmlUtil.getAttribute("book.param_typist_use", typist_use));
	b.append(HtmlUtil.getAttribute("book.param_date_short", date_short));
	b.append(HtmlUtil.getAttribute("book.param_date_long", date_long));
        b.append(Backup.toHtml());
        b.append(Editor.toHtml());
        b.append(Export.toHtml());
        b.append(Import.toHtml());
        b.append(Gallery.toHtml());
        return (b.toString());
    }

    public String toXml() {
        //App.trace("BookParam.toXml()");
        StringBuilder b = new StringBuilder("    <param ");
		b.append(toXmlSetAttribute(0, "last_part", getClean(getCurrentPart())));
		b.append(toXmlSetAttribute(0, "all_part", getClean(getAllPart())));
		b.append(toXmlSetAttribute(0, "typist_use", getClean(getTypistUse()))).append("\n");
		b.append(toXmlSetAttribute(2, "date_short", getClean(getDateShort())));
		b.append(toXmlSetAttribute(0, "date_long", getClean(getDateLong()))).append("\n");
        b.append("        >\n");
        b.append(Backup.toXml());
        b.append(Editor.toXml());
        b.append(Export.toXml());
        b.append(Import.toXml());
        b.append(Gallery.toXml());
        b.append("    </param>\n");
        return (b.toString());
    }

    public boolean isEditorModless() {
        return (Editor.modless);
    }

    public boolean isUseXeditor() {
        return (Editor.xUse);
    }

    public String getXeditorTemplate() {
        return (Editor.xTemplate);
    }

    public String getXeditorExtension() {
        return (Editor.xExtend);
    }

    public void save(String folder) {
        String nf = folder + File.separator + ".preferences";
        File f = new File(nf);
        XmlDB xml = new XmlDB(f);
        xml.reformat("<ostorybook>\n" + toXml() + "</ostorybook>");
        xml.close();
    }

    public void load(String folder) {
        String nf = folder + File.separator + ".preferences";
        File f = new File(nf);
        if (f.exists()) {
            XmlDB xml = new XmlDB(f);
            xml.open();
            load(xml);
            xml.close();
        }
    }

    private void initXml(XmlDB xml) {
        if (xml.document == null) {
            try {
                xml.documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                xml.document = xml.documentBuilder.newDocument();
            } catch (ParserConfigurationException ex) {
                ex.printStackTrace(System.err);
            }
        }
        xml.rootNode = xml.document.getDocumentElement();
    }

}
