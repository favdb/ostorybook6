package desk.label;

import db.entity.SbDate;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import tools.IconUtil;

@SuppressWarnings("serial")
public class LabelDate extends JLabel {

	private SbDate chapter_date;
	private String date_format;

	public LabelDate(SbDate chapter, String fmt) {
		super();
		this.chapter_date = chapter;
		setText(getDateText());
		setToolTipText(getDateText());
		setIcon(IconUtil.getIcon("small/chrono"));
		setBackground(new Color(240, 240, 240));
		setOpaque(true);
		setHorizontalAlignment(SwingConstants.CENTER);
		date_format=fmt;
	}

	public final String getDateText() {
		if (chapter_date == null) {
			return "";
		}
		String dayFmt=chapter_date.format(date_format);
		return(dayFmt);
	}

	public SbDate getDate() {
		return chapter_date;
	}

	public void setDate(SbDate date) {
		this.chapter_date = date;
	}
}
