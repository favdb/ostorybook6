/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.label;

import db.entity.Pov;
import db.entity.SbDate;
import desk.interfaces.IRefreshable;

@SuppressWarnings("serial")
public class LabelPovDate extends LabelDate implements IRefreshable {

	private Pov pov;

	public LabelPovDate(Pov pov, SbDate date, String fmt) {
		super(date,fmt);
		this.pov = pov;
		refresh();
	}

	@Override
	public final void refresh() {
		String text = getDateText();
		setText(text);
		setToolTipText("<html>" + text + "<br>" + pov);
	}

	public Pov getPov() {
		return pov;
	}

	public void setPov(Pov pov) {
		this.pov = pov;
	}
}
