/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.template;

import desk.app.MainFrame;
import desk.panel.AbstractFrame;
import java.beans.PropertyChangeEvent;

/**
 * Template for creation of a new frame
 * 
 * Create a new subfolder in the frame folder.
 * Copy this template in this new created subfolder, and refract with the
 * desired name of the new frame.
 *
 * @author favdb
 */
public class FrameTemplate extends AbstractFrame {
	
	public FrameTemplate(MainFrame mainFrame) {
		super(mainFrame);
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
