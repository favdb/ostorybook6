/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.table;

import db.entity.Book;
import desk.table.TableCol.DataType;
import desk.table.renderer.TCRBoolean;
import desk.table.renderer.TCREntities;
import desk.table.renderer.TCREntity;
import desk.table.renderer.TCRHtml;
import desk.table.renderer.TCRSceneId;
import desk.table.renderer.TCRStatus;
import desk.view.SbView.VIEWID;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingConstants;

/**
 * @author martin
 *
 */
public class TableFactory {

	private static final boolean ASSISTANT = true;

	public static List<TableCol> getColumns(Book book, VIEWID view) {
		//App.trace("TableFactory.getColumns("+view+")");
		switch (view) {
			case TABLE_CATEGORY:
				return (getCategoryColumns(book));
			case TABLE_CHAPTER:
				return (getChapterColumns(book));
			case TABLE_EVENT:
				return (getEventColumns(book));
			case TABLE_GENDER:
				return (getGenderColumns(book));
			case TABLE_IDEA:
				return (getIdeaColumns(book));
			case TABLE_ITEM:
				return (getItemColumns(book));
			case TABLE_LOCATION:
				return (getLocationColumns(book));
			case TABLE_MEMO:
				return (getMemoColumns(book));
			case TABLE_PART:
				return (getPartColumns(book));
			case TABLE_PERSON:
				return (getPersonColumns(book));
			case TABLE_PHOTO:
				return (getPhotoColumns(book));
			case TABLE_PLOT:
				return (getPlotColumns(book));
			case TABLE_POV:
				return (getPovColumns(book));
			case TABLE_RELATION:
				return (getRelationColumns(book));
			case TABLE_SCENE:
				return (getSceneColumns(book));
			case TABLE_TAG:
				return (getTagColumns(book));
		}
		return (getAbstractColumns());
	}

	public static List<TableCol> getAbstractColumns() {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;
		getFooterColumns(columns, i, !ASSISTANT);
		return (columns);
	}

	private static void getHeaderColumns(List<TableCol> columns) {
		TableCol col = new TableCol(0, "Id", DataType.TEXTFIELD, "z.id", 5);
		col.setReadOnly();
		col.setHide();
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(1, "Creation", DataType.DATETIME, "z.date.creation", 0);
		col.setHide();
		columns.add(col);

		col = new TableCol(2, "Maj", DataType.DATETIME, "z.date.maj", 0);
		col.setHide();
		columns.add(col);

		col = new TableCol(3, "Name", DataType.TEXTFIELD, "z.name", 64);
		columns.add(col);
	}

	private static void getFooterColumns(List<TableCol> columns, int i, boolean assistant) {
		getDescriptionColumn(columns, i++);
		getNotesColumn(columns, i++);
		if (assistant) {
			getAssistantColumn(columns, i);
		}
	}

	private static void getDescriptionColumn(List<TableCol> columns, int i) {
		TableCol col = new TableCol(i, "Description", DataType.TEXTAREA, "z.description", 3768);
		col.setTableCellRenderer(new TCRHtml());
		col.setHide();
		columns.add(col);
	}
	
	private static void showDescription(List<TableCol> columns) {
		columns.forEach((c)-> {
			if (c.getMethodName().equals(("Description"))) {
				c.resetHide();
			}
		});
	}

	private static void getNotesColumn(List<TableCol> columns, int i) {
		TableCol col = new TableCol(i, "Notes", DataType.TEXTAREA, "z.notes", 3268);
		col.setTableCellRenderer(new TCRHtml());
		col.setHide();
		columns.add(col);
	}

	private static void showNotes(List<TableCol> columns) {
		columns.forEach((c)-> {
			if (c.getMethodName().equals(("Notes"))) {
				c.resetHide();
			}
		});
	}

	private static void getAssistantColumn(List<TableCol> columns, int i) {
		TableCol col = new TableCol(i, "Assistant", DataType.TEXTAREA, "assistant", 32768);
		col.setTableCellRenderer(new TCRHtml());
		col.setHide();
		columns.add(col);
	}

	public static List<TableCol> getChapterColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Number", DataType.TEXTFIELD, "z.number", 3);
		col.setAlign(SwingConstants.CENTER);
		col.setDefaultSort(true);
		columns.add(col);

		col = new TableCol(i++, "Part", DataType.ENTITY, "part", 0);
		col.setAlign(SwingConstants.CENTER);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.PART, TCREntity.TEXT));
		columns.add(col);

		col = new TableCol(i++, "NbScenes", DataType.TEXTFIELD, "chapter.nbscenes", 2);
		col.setAlign(SwingConstants.CENTER);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "ObjectiveDate", DataType.DATE, "objective.date", 0);
		columns.add(col);

		col = new TableCol(i++, "ObjectiveChars", DataType.TEXTFIELD, "objective.chars", 6);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "ObjectiveDone", DataType.DATE, "objective.done", 0);
		columns.add(col);

		getFooterColumns(columns, i, ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getPartColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Number", DataType.TEXTFIELD, "z.number", 2);
		col.setDefaultSort(true);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Sup", DataType.ENTITY, "part.sup", 0);
		columns.add(col);

		col = new TableCol(i++, "ObjectiveDate", DataType.DATE, "objective.date", 0);
		columns.add(col);

		col = new TableCol(i++, "ObjectiveChars", DataType.TEXTFIELD, "objective.chars", 6);
		columns.add(col);

		col = new TableCol(i++, "ObjectiveDone", DataType.DATE, "objective.done", 0);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getLocationColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Address", DataType.TEXTFIELD, "location.address", 255);
		columns.add(col);

		col = new TableCol(i++, "City", DataType.TEXTFIELD, "location.city", 255);
		columns.add(col);

		col = new TableCol(i++, "Country", DataType.TEXTFIELD, "location.country", 255);
		columns.add(col);

		col = new TableCol(i++, "Sup", DataType.ENTITY, "z.sup", 0);
		columns.add(col);

		col = new TableCol(i++, "Altitude", DataType.TEXTFIELD, "location.altitude", 5);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Gps", DataType.TEXTFIELD, "location.gps", 16);
		col.setHide();
		columns.add(col);

		getFooterColumns(columns, i, ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getPersonColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Firstname", DataType.TEXTFIELD, "person.firstname", 255);
		col.setDefaultSort(true);
		columns.add(col);

		col = new TableCol(i++, "Lastname", DataType.TEXTFIELD, "person.lastname", 255);
		columns.add(col);

		col = new TableCol(i++, "Abbreviation", DataType.TEXTFIELD, "z.abbreviation", 16);
		columns.add(col);

		col = new TableCol(i++, "Gender", DataType.COMBOBOX, "gender", 0);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.GENDER, TCREntity.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Category", DataType.ENTITY, "category", 0);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.CATEGORY,TCREntity.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Birthday", DataType.DATE, "person.birthday", 0);
		columns.add(col);

		col = new TableCol(i++, "Dayofdeath", DataType.DATE, "person.dayofdeath", 0);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Occupation", DataType.TEXTFIELD, "person.occupation", 255);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Color", DataType.COLOR, "z.color", 0);
		columns.add(col);

		getFooterColumns(columns, i, ASSISTANT);

		return columns;
	}

	private static List<TableCol> getPhotoColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "File", DataType.TEXTFIELD, "file.name", 255);
		columns.add(col);
/*
		col = new TableCol(i++, "Height", DataType.TEXTFIELD, "photo.height", 4);
		col.setAlign(SwingConstants.CENTER);
		col.setWidth(4);
		columns.add(col);

		col = new TableCol(i++, "Width", DataType.TEXTFIELD, "photo.width", 4);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);
*/
		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getRelationColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "StartScene", DataType.ENTITY, "scene.start", 0);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.SCENE, TCREntity.TEXT));
		columns.add(col);

		col = new TableCol(i++, "EndScene", DataType.ENTITY, "scene.end", 0);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.SCENE, TCREntity.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Persons", DataType.ENTITIES, "persons", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.PERSON, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Locations", DataType.ENTITIES, "locations", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.LOCATION, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Items", DataType.ENTITIES, "items", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.ITEM, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Tags", DataType.ENTITIES, "tags", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.TAG, TCREntities.TEXT));
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);

		return columns;
	}

	private static List<TableCol> getGenderColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Sort", DataType.TEXTFIELD, "z.sort", 0);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Childhood", DataType.TEXTFIELD, "gender.childhood", 3);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Adolescence", DataType.TEXTFIELD, "gender.adolescence", 3);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Adulthood", DataType.TEXTFIELD, "gender.adulthood", 3);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Retirement", DataType.TEXTFIELD, "gender.retirement", 3);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "IconFile", DataType.TEXTFIELD, "icon.file", 255);
		col.setHide();
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getCategoryColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Type", DataType.TEXTFIELD, "z.type", 64);
		col.setDefaultSort(true);
		columns.add(col);

		col = new TableCol(i++, "Sort", DataType.TEXTFIELD, "z.sort", 3);
		col.setDefaultSort(true);
		columns.add(col);

		col = new TableCol(i++, "Sup", DataType.ENTITY, "z.sup", 0);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getPlotColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Category", DataType.ENTITY, "category", 0);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getPovColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Abbreviation", DataType.TEXTFIELD, "z.abbr", 16);
		columns.add(col);

		col = new TableCol(i++, "JColor", DataType.COLOR, "z.color", 0);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Sort", DataType.TEXTFIELD, "z.sort", 3);
		col.setDefaultSort(true);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

	private static List<TableCol> getIdeaColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Status", DataType.STATUS, "status", 0);
		col.setDefaultSort(true);
		columns.add(col);

		col = new TableCol(i++, "Category", DataType.ENTITY, "category", 0);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showNotes(columns);

		return columns;
	}

	private static List<TableCol> getTagColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Category", DataType.ENTITY, "category", 0);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);

		showNotes(columns);

		return columns;
	}

	private static List<TableCol> getMemoColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		getFooterColumns(columns, i, !ASSISTANT);
		
		showNotes(columns);

		return columns;
	}

	private static List<TableCol> getItemColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col;
		
		col = new TableCol(i++, "Category", DataType.ENTITY, "category", 0);
		columns.add(col);

		col = new TableCol(i++, "Beacon", DataType.TEXTFIELD, "category.beacon", 255);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Icone", DataType.TEXTFIELD, "icon.file", 255);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Photos", DataType.ENTITIES, "photos", 255);
		col.setHide();
		columns.add(col);

		getFooterColumns(columns, i, ASSISTANT);
		
		showNotes(columns);

		return columns;
	}

	private static List<TableCol> getSceneColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;
		TableCol col;

		col = new TableCol(i++, "Number", DataType.TEXTFIELD, "z.number", 3);
		col.setHide();
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Informative", DataType.BOOL, "scene.informative", 0);
		col.setTableCellRenderer(new TCRBoolean());
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Chapter", DataType.ENTITY, "chapter", 0);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.CHAPTER, TCREntity.TEXT));
		col.setDefaultSort(true);
		columns.add(col);

		col = new TableCol(i++, "Status", DataType.STATUS, "status", 0);
		col.setTableCellRenderer(new TCRStatus(book));
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "NbWords", DataType.TEXTFIELD, "z.words", 6);
		col.setAlign(SwingConstants.CENTER);
		columns.add(col);

		col = new TableCol(i++, "Pov", DataType.ENTITY, "pov", 0);
		col.setTableCellRenderer(new TCREntity(book, Book.TYPE.POV, TCREntity.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Narrator", DataType.ENTITY, "scene.narrator", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.PERSON, TCREntities.TEXT));
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Date", DataType.DATETIME, "z.date", 0);
		col.setShowDateTime();
		columns.add(col);

		col = new TableCol(i++, "RelativeDate", DataType.TEXTFIELD, "scene.relative.date", 5);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "RelativeScene", DataType.ENTITY, "scene.relative.scene", 0);
		col.setTableCellRenderer(new TCRSceneId(book));
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Texte", DataType.TEXTFIELD, "scene.texte", 32768);
		col.setTableCellRenderer(new TCRHtml());
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Xfile", DataType.TEXTFIELD, "scene.xfile", 255);
		col.setHide();
		columns.add(col);

		col = new TableCol(i++, "Persons", DataType.ENTITIES, "persons", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.PERSON, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Locations", DataType.ENTITIES, "locations", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.LOCATION, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Items", DataType.ENTITIES, "items", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.ITEM, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Tags", DataType.ENTITIES, "tags", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.TAG, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Plots", DataType.ENTITIES, "plots", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.PLOT, TCREntities.TEXT));
		columns.add(col);

		col = new TableCol(i++, "Photos", DataType.ENTITIES, "photos", 0);
		col.setTableCellRenderer(new TCREntities(book, Book.TYPE.PHOTO, TCREntities.TEXT));
		columns.add(col);

		getFooterColumns(columns, i, ASSISTANT);

		return columns;
	}

	private static List<TableCol> getEventColumns(Book book) {
		List<TableCol> columns = new ArrayList<>();
		getHeaderColumns(columns);
		int i = 4;

		TableCol col = new TableCol(i++, "Date", DataType.DATETIME, "z.date", 0);
		columns.add(col);

		col = new TableCol(i++, "Category", DataType.ENTITY, "category", 0);
		columns.add(col);

		getFooterColumns(columns, i, !ASSISTANT);
		
		showDescription(columns);

		return columns;
	}

}
