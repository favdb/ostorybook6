/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.table;

import javax.swing.table.TableCellRenderer;

import db.I18N;

import javax.swing.SwingConstants;

public class TableCol {

	public enum DataType {
		TEXTFIELD, TEXTAREA, COMBOBOX, ENTITY, ENTITIES, BOOL, DATE, DATETIME, COLOR, ICON,
		ATTRIBUTES, STATUS, NONE
	}

	private final int colId;
	private final DataType inputType;
	private final String methodName;
	private final String resourceKey;
	private int width = 100;
	private boolean readOnly = false;
	private boolean hide = false;
	private TableCellRenderer tableCellRenderer = null;
	private int maxLength = -1;
	private boolean showDateTime = false;
	private boolean defaultSort = false;
	private int align=SwingConstants.LEFT;


	public TableCol(int colId, String methodName, DataType inputType, String resourceKey, int len) {
		this.colId = colId;
		this.methodName = methodName;
		this.inputType = inputType;
		this.resourceKey = resourceKey;
		this.maxLength=len;
	}

	@Override
	public String toString() {
		if ((resourceKey != null) && (!resourceKey.isEmpty())) {
		   return I18N.getMsg(resourceKey);
		} else {
			return "";
		}
	}

	public void setReadOnly() {
		this.readOnly = true;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getColId() {
		return colId;
	}

	public String getName() {
		return resourceKey;
	}

	public DataType getType() {
		return inputType;
	}

	public String getResourceKey() {
		return resourceKey;
	}

	public String getMethodName() {
		return methodName;
	}

	public int getWidth() {
		return width;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public boolean isHide() {
		return hide;
	}

	public void setHide() {
		this.hide = true;
	}

	public void resetHide() {
		this.hide = false;
	}

	public boolean hasTableCellRenderer() {
		return tableCellRenderer != null;
	}

	public TableCellRenderer getTableCellRenderer() {
		return tableCellRenderer;
	}

	public void setTableCellRenderer(TableCellRenderer renderer) {
		this.tableCellRenderer = renderer;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setShowDateTime() {
		this.showDateTime = true;
	}

	public boolean hasDateTime() {
		return showDateTime;
	}

	public boolean isDefaultSort() {
		return defaultSort;
	}

	public void setDefaultSort(boolean defaultSort) {
		this.defaultSort = defaultSort;
	}

	public int getAlign() {
		return align;
	}

	public void setAlign(int i) {
		align=i;
	}
}
