/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.table.renderer;

import db.entity.Book;
import db.entity.Scene;
import desk.app.App;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class TCRSceneId extends DefaultTableCellRenderer {

	private final Book book;

	public TCRSceneId(Book book) {
		this.book = book;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lbText = (JLabel) super.getTableCellRendererComponent(table,
				null, isSelected, hasFocus, row, column);
		JPanel panel = new JPanel(new MigLayout("insets 2"));
		panel.setOpaque(true);
		panel.setBackground(lbText.getBackground());
		if (value instanceof String) {
			panel.add(new JLabel());
			return panel;
		}
		if (value instanceof Scene) {
			try {
				Scene scene = (Scene)value;
				if (scene == null) {
					return panel;
				}
				panel.add(new JLabel(scene.name));
			} catch (Exception e) {
				App.error("TCRSceneId", e);
			}
		}
		return panel;
	}
}
