/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.table.renderer;

import db.entity.AbstractEntity;
import db.entity.Book;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author favdb
 */
public class TCREntity extends DefaultTableCellRenderer {

	public final Book book;
	public Book.TYPE type;
	private final int mode;
	public static int ICON = 1, TEXT = 10;

	public TCREntity(Book book, Book.TYPE type, int mode) {
		this.book = book;
		this.type=type;
		this.mode = mode;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lbText = (JLabel) super.getTableCellRendererComponent(table, null, isSelected, hasFocus, row, column);
		if (value == null || value instanceof String) {
			return lbText;
		}
		try {
			if (value instanceof AbstractEntity) {
				AbstractEntity entity=(AbstractEntity)value;
				if (mode >= TEXT) {
					lbText.setText(entity.toString());
				}
				if (mode == ICON || mode == ICON+TEXT) {
					lbText.setIcon(entity.getIcon(16));
				}
			}  else if (value instanceof Long) {
				Long id = (Long) value;
				AbstractEntity entity = book.getEntity(type,id);
				if (entity!=null) {
					if (mode >= TEXT) {
						lbText.setText(entity.toString());
					}
					if (mode == ICON || mode == ICON+TEXT) {
						lbText.setIcon(entity.getIcon(16));
					}
				}
			}
			lbText.setText(value.toString());
		} catch (Exception e) {
		}
		return lbText;
	}
}
