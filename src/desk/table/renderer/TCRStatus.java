/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.table.renderer;

import db.entity.Book;
import db.entity.Status;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class TCRStatus extends DefaultTableCellRenderer {

	private final Book book;
	
	public TCRStatus(Book book) {
		this.book=book;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lbText = (JLabel) super.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);
		lbText.setText(Status.getMsg((Integer)value));
		lbText.setIcon(Status.getIcon((Integer)value));
		lbText.setToolTipText(Status.getMsg((Integer)value));
		lbText.setHorizontalAlignment(CENTER);
		return lbText;
	}
}
