/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.table.renderer;

import db.entity.SbDate;
import java.util.Date;

import javax.swing.table.DefaultTableCellRenderer;

import db.I18N;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class TCRDate extends DefaultTableCellRenderer {

	private final boolean isTime;

	public TCRDate(boolean isTime) {
		super();
		this.isTime=isTime;
	}

	@Override
	public void setValue(Object value) {
		try {
			if (value instanceof Date) {
				setText(I18N.getDateTime((Date)value));
			} else if (value instanceof SbDate) {
				SbDate v=(SbDate)value;
				if (!v.isZero()) {
					if (isTime) setText(I18N.getDateTime(v));
					else setText(I18N.getDate(v));
				}
			} else {
				setText("");
			}
		} catch (Exception e) {
			setText("");
		}
	}
}
