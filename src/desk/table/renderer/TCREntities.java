/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.table.renderer;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Item;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class TCREntities extends DefaultTableCellRenderer {
	public final Book book;
	private final Book.TYPE type;
	private final int mode;
	public final static int ICON=1, TEXT=10;
	
	public TCREntities(Book book, Book.TYPE type, int mode) {
		this.book=book;
		this.type=type;
		this.mode=mode;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lbText = (JLabel) super.getTableCellRendererComponent(table,
				null, isSelected, hasFocus, row, column);
		if (value instanceof String) {
			return lbText;
		} else if (value instanceof List) {
			try {
				if (((List) value).get(0) instanceof Long) {
					@SuppressWarnings("unchecked")
					List list = book.getEntities(type, (List<Long>) value);
					lbText.setText(getListOfEntities(list));
				} else if (((List) value).get(0) instanceof AbstractEntity) {
					lbText.setText(getListOfEntities((List)value));
				}
			} catch (Exception e) {
			}
		}
		return lbText;
	}

	private String getListOfEntities(List<?> list) {
		List<String> abbrs = new ArrayList<>();
		list.forEach((p) -> {
			abbrs.add(((AbstractEntity)p).getAbbr());
		});
		return (StringUtil.join(abbrs, ", "));
	}
}
