/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.table.renderer;

import db.entity.Book;
import db.entity.Tag;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import tools.StringUtil;

@SuppressWarnings("serial")
public class TCRTags extends AbstractTCR {

	public TCRTags(Book book) {
		super(book);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JLabel lbText = (JLabel) super.getTableCellRendererComponent(table,
				null, isSelected, hasFocus, row, column);
		if (value instanceof String) {
			return lbText;
		} else if (value instanceof List) {
			try {
				if (((List) value).get(0) instanceof Long) {
					lbText.setText(getListOfEntities(book.getTags((List<Long>) value)));
				} else if (((List) value).get(0) instanceof Tag) {
					lbText.setText(getListOfEntities((List<Tag>)value));
				}
			} catch (Exception e) {
			}
		}
		return lbText;
	}

}