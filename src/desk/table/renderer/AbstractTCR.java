/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.table.renderer;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Item;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableCellRenderer;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public abstract class AbstractTCR extends DefaultTableCellRenderer {
	public final Book book;
	
	public AbstractTCR(Book book) {
		this.book=book;
	}

	public String getListOfEntities(List<?> list) {
		List<String> abbrs = new ArrayList<>();
		list.forEach((p) -> {
			abbrs.add(((AbstractEntity)p).getAbbr());
		});
		return (StringUtil.join(abbrs, ", "));
	}
	
}
