/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.table;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Person;
import db.entity.Pov;
import db.entity.Scene;
import db.entity.Status;
import db.util.CheckUsed;
import db.util.EntityTool;
import desk.app.App;
import desk.panel.AbstractPanel;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import desk.tools.combobox.ComboUtil;
import desk.model.Ctrl;
import desk.exporter.ExportTable;
import desk.dialog.ConfirmDeleteDlg;
import static desk.dialog.edit.AbstractEditorPanel.ALL;
import static desk.dialog.edit.AbstractEditorPanel.EMPTY;
import static desk.dialog.edit.AbstractEditorPanel.NEW;
import desk.table.TableCol.DataType;
import desk.table.renderer.TCRColor;
import desk.table.renderer.TCRDate;
import desk.table.renderer.TCRIcon;
import desk.table.renderer.TCRStatus;
import desk.tools.EntityUtil;
import desk.tools.IOTools;
import desk.view.SbView;
import desk.view.SbView.VIEWID;
import db.I18N;
import db.entity.Book;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListSelectionModel;
import javax.swing.DefaultRowSorter;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import lib.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.table.TableColumnExt;

/**
 *
 * @author favdb
 */
public class Table extends AbstractPanel implements ActionListener, ListSelectionListener, MouseListener {

	public enum ACTION {
		BT_NEW("btNew"),
		BT_DELETE("btDelete"),
		BT_EDIT("btEdit"),
		BT_COPY("btCopy"),
		BT_ODT("btOdt"),
		REFRESH("Table_Refresh"),
		SHOWINFO("Table_ShowInfo"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	public VIEWID view;
	protected List<TableCol> columns;
	//protected JPanel optionsPanel;
	public JXTable table = new JXTable();
	protected DefaultTableModel tableModel;
	protected boolean allowMultiDelete;
	private JButton btNew,
			btDelete,
			btEdit,
			btCopy,
			btODT;
	private JLabel totalObjectif,
			lbCount;
	private JComboBox sceneStateCombo,
			scenePovCombo,
			sceneNarratorCombo,
			typeCombo,
			partCombo;
	private String objtype = "???";

	public Table(MainFrame mainFrame, VIEWID view) {
		super(mainFrame);
		this.allowMultiDelete = true;
		this.view = view;
//		App.trace("Table(mainFrame, view=" + view.name() + ")");
		initAll();
	}

	@Override
	public void init() {
		setName(view.toString());
		columns = getColumns();
	}

	@Override
	public void initUi() {
//		App.trace("Table.initUi()");
		if (this.getComponentCount() != 0) {
			this.removeAll();
		}
		setLayout(new MigLayout("fill,wrap"));

		//optionsPanel = new JPanel(new MigLayout("flowx"));
		initOptionsPanel();

		List<String> colNames = getColumnNames();
		tableModel = new DefaultTableModel(colNames.toArray(), 0);
		table = new JXTable();
		table.setName(view.toString());
		table.setModel(tableModel);

		// renderer and comparators
		for (TableCol tabcol : columns) {
			TableColumn tcol;
			try {
				tcol = table.getColumn(tabcol.toString());
			} catch (IllegalArgumentException ex) {
				System.err.println("Table.initUi() Error tcol");
				continue;
			}
			DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
			if (tabcol.getType() == DataType.DATE) {
				renderer = new TCRDate(false);
			} else if (tabcol.getType() == DataType.DATETIME) {
				renderer = new TCRDate(true);
			} else if (tabcol.getType() == DataType.STATUS) {
				renderer = new TCRStatus(book);
			} else if ((tabcol.getType() == DataType.ICON)) {
				renderer = new TCRIcon();
			} else if (tabcol.getType() == DataType.COLOR) {
				renderer = new TCRColor();
			} else if (tabcol.getTableCellRenderer() != null) {
				renderer = (DefaultTableCellRenderer) tabcol.getTableCellRenderer();
			}

			if (tabcol.getAlign() == SwingConstants.CENTER) {
				renderer.setHorizontalAlignment(SwingConstants.CENTER);
			}
			if (renderer != null) {
				tcol.setCellRenderer(renderer);
			}
			TableColumnExt ext = table.getColumnExt(tabcol.toString());
			if (ext != null) {
				// visible on start
				if (tabcol.isHide()) {
					ext.setVisible(false);
				}
			}
			if (tabcol.isDefaultSort()) {
				table.setSortOrder(tabcol.toString(), SortOrder.ASCENDING);
			}
			if (tabcol.getType() == DataType.TEXTAREA) {
				tcol.setPreferredWidth(400);
			}
			tcol.setMaxWidth(800);
		}

		table.setColumnControlVisible(true);
		table.setShowGrid(false, false);
		table.setHorizontalScrollEnabled(true);

		table.addHighlighter(HighlighterFactory.createSimpleStriping());

		table.setEditable(false);
		table.addMouseListener(this);
		table.getSelectionModel().addListSelectionListener(this);

		// hot keys
		table.registerKeyboardAction(this, "Edit",
				SwingUtil.getKeyStrokeEnter(), JComponent.WHEN_FOCUSED);
		table.registerKeyboardAction(this, "New",
				SwingUtil.getKeyStrokeInsert(), JComponent.WHEN_FOCUSED);
		table.registerKeyboardAction(this, "Copy",
				SwingUtil.getKeyStrokeCopy(), JComponent.WHEN_FOCUSED);
		table.registerKeyboardAction(this, "Delete",
				SwingUtil.getKeyStrokeDelete(), JComponent.WHEN_FOCUSED);

		// column widths
		table.getColumns().forEach((col) -> {
			try {
				col.setMinWidth(40);
			} catch (IllegalArgumentException e) {
				System.err.println("Table unknown error for setting colsize");
			}
		});

		JScrollPane scroller = new JScrollPane(table);
		scroller.setPreferredSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));

		btNew = SwingUtil.initButton(ACTION.BT_NEW.toString(), "", "small/new", "z.new");
		btNew.addActionListener(this);

		btDelete = SwingUtil.initButton(ACTION.BT_DELETE.toString(), "", "small/delete", "z.delete");
		btDelete.addActionListener(this);
		btDelete.setEnabled(false);

		btEdit = SwingUtil.initButton(ACTION.BT_EDIT.toString(), "", "small/edit", "z.edit");
		btEdit.addActionListener(this);
		btEdit.setEnabled(false);

		btCopy = SwingUtil.initButton(ACTION.BT_COPY.toString(), "", "small/edit-copy", "z.copy");
		btCopy.addActionListener(this);
		btCopy.setEnabled(false);

		// layout
		if (toolbar.getComponentCount() > 0) {
			add(toolbar, "growx");
		}
		add(scroller, "grow");
		String split = "split 6";
		add(btNew, "span," + split);
		add(btEdit);
		add(btCopy);
		add(btDelete);

		lbCount = new JLabel(" ");
		add(lbCount);

		switch (view) {
			case TABLE_SCENE:
				if (mainFrame.bookPref.isUseXeditor()) {
					btODT = SwingUtil.initButton(ACTION.BT_ODT.toString(),
							"", "small/libreoffice", "xeditor.launch");
					btODT.addActionListener(this);
					btODT.setEnabled(false);
					add(btODT, "sg");
				}
				break;
			case TABLE_CHAPTER:
				totalObjectif = new JLabel();
				totalObjectif.setText("O");
				totalObjectif.setVisible(false);
				totalObjectif.setHorizontalAlignment(SwingConstants.RIGHT);
				add(totalObjectif, "right,growx");
				break;
		}

		reloadData();
	}

	protected void initTableModel(PropertyChangeEvent evt) {
//		App.trace("Table.initTableModel(" + evt.toString() + ")");
		//table.putClientProperty(Language.ClientPropertyName.MAIN_FRAME.toString(), mainFrame);
		reloadData();
	}

	private void reloadData() {
		// clear table
		for (int i = tableModel.getRowCount() - 1; i >= 0; i--) {
			tableModel.removeRow(i);
		}
		// fill in data
		try {
			@SuppressWarnings("unchecked")
			List<AbstractEntity> entities = (List<AbstractEntity>) getAllEntities();
			Integer nbTotalObjectif = 0;
			for (AbstractEntity entity : entities) {
				// show only scenes from current part
				if ((entity instanceof Scene) && !mainFrame.bookPref.getAllPart()) {
					Part part = mainFrame.getCurrentPart();
					Scene scene = (Scene) entity;
					Chapter chap=scene.chapter;
					if (part != null && !mainFrame.bookPref.getAllPart() && scene.hasChapter()) {
						if (chap.hasPart() && !chap.part.id.equals(part.id)) {
							continue;
						}
					}
				}
				List<Object> cols = new AbstractEntity().getRow();
				if (entity != null) {
					cols = entity.getRow();
				}
				tableModel.addRow(cols.toArray());
				if (entity instanceof Chapter) {
					nbTotalObjectif += ((Chapter) entity).size;
				}
			}
			lbCount.setText(I18N.getMsg("entity.count", table.getRowCount()));
			if (view.equals(VIEWID.TABLE_CHAPTER)) {
				if (nbTotalObjectif > 0) {
					totalObjectif.setText(I18N.getMsg("objective.total")
							+ " " + nbTotalObjectif + " " + I18N.getMsg("z.characters"));
					totalObjectif.setVisible(true);
					this.revalidate();
				} else {
					totalObjectif.setVisible(false);
				}
			}
		} catch (ClassCastException e) {
		}
	}

	protected AbstractEntity getNewEntity() {
//		App.trace("("+table.name+")"+"Table.getNewEntity() view=" + view);
		AbstractEntity abs = book.getEntityNew(Book.getTYPE(view.toString()));
		return (abs);
	}

	protected void sendSetEntityToEdit(int row) {
		//App.trace("("+table.name+")"+"Table.sendSetEntityToEdit(" + row + ")");
		if (row == -1) {
			return;
		}
		AbstractEntity entity = getEntityFromRow(row);
		if (entity == null) {
			App.error(table.getName() + " entity not find for this row=" + row);
			return;
		}
		if (mainFrame.showEditorAsDialog(entity)) {
			reloadData();
		}
		table.setRowSelectionInterval(row, row);
	}

	protected void sendSetNewEntityToEdit(AbstractEntity entity) {
//		App.trace("("+table.name+")"+"Table.sendSetNewEntityToEdit(" + entity.toString() + ")");
		if (!mainFrame.showEditorAsDialog(entity)) {
			mainFrame.refresh();
			setSelectedRow(entity);
		}
	}

	protected synchronized void sendSetDeleteEntity(int[] rows) {
//		App.trace("("+table.name+")"+"Table.sendSetDeleteEntities("+Arrays.toString(rows)+")");
		ArrayList<Long> ids = new ArrayList<>();
		AbstractEntity entity = null;
		for (int row : rows) {
			entity = getEntityFromRow(row);
			ids.add(entity.id);
		}
		if (entity != null) {
			mainFrame.getBookController().deleteMultiEntities(entity.type.toString(), ids);
		}
	}

	private void setSelectedRow(AbstractEntity entity) {
		//App.trace("("+name+")"+"Table.setSelectedRow("+entity.name+")");
		for (int i = 0; i < table.getRowCount(); i++) {
			AbstractEntity abs = getEntityFromRow(i);
			if (entity.equals(abs)) {
				table.setRowSelectionInterval(i, i);
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void initOptionsPanel() {
		//App.trace("("+name+")"+"Table.initOptionsPanel()");
		initToolbar(WITH_PART);
		switch (view) {
			case TABLE_CATEGORY:
				//optionsPanel.removeAll();
				typeCombo = new JComboBox();
				typeCombo.setName("ComboType");
				typeCombo.addItem(I18N.getMsg("z.all"));
				for (Category.BEACON t : Category.BEACON.values()) {
					typeCombo.addItem(I18N.getMsg(t.name().toLowerCase()));
				}
				typeCombo.addActionListener(this);
				toolbar.add(new JLabel(I18N.getColonMsg("z.type")));
				toolbar.add(typeCombo);
				break;
			case TABLE_SCENE:
				//optionsPanel.removeAll();
				/*if (book.parts.size()>1) {
					partCombo = new JComboBox();
					partCombo.setName("ComboPart");
					ComboUtil.fillEntity(book, partCombo, "part", null, null, !NEW, !EMPTY, ALL);
					optionsPanel.add(new JLabel(I18N.getColonMsg("part")));
					optionsPanel.add(partCombo);
				}*/
				sceneStateCombo = new JComboBox();
				sceneStateCombo.setName("ComboSceneStates");
				toolbar.add(new JLabel(I18N.getColonMsg("status")));
				sceneStateCombo.addItem(I18N.getMsg("status.all"));
				for (Status.STATUS p : Status.STATUS.values()) {
					sceneStateCombo.addItem(I18N.getMsg("status." + p.toString()));
				}
				sceneStateCombo.setSelectedIndex(0);
				sceneStateCombo.addActionListener(this);
				toolbar.add(sceneStateCombo);

				if (book.povs.size() > 1) {
					scenePovCombo = new JComboBox();
					scenePovCombo.setName("ComboScenePov");
					ComboUtil.fillEntity(book, scenePovCombo, Book.TYPE.POV, null, null, !NEW, !EMPTY, ALL);
					scenePovCombo.setSelectedIndex(0);
					scenePovCombo.addActionListener(this);
					toolbar.add(new JLabel(I18N.getColonMsg("pov")));
					toolbar.add(scenePovCombo);
				}

				toolbar.add(new JLabel(I18N.getColonMsg("scene.narrator")));
				sceneNarratorCombo = new JComboBox();
				sceneNarratorCombo.setName("ComboSceneNarrator");
				ComboUtil.fillEntity(book, sceneNarratorCombo, Book.TYPE.PERSON, null, null, !NEW, !EMPTY, ALL);
				sceneNarratorCombo.setSelectedIndex(0);
				sceneNarratorCombo.addActionListener(this);
				toolbar.add(sceneNarratorCombo);
				break;
			case TABLE_CHAPTER:
				//optionsPanel.removeAll();
				/*if (book.parts.size()>1) {
					partCombo = new JComboBox();
					partCombo.setName("ComboPart");
					ComboUtil.fillEntity(book, partCombo, "part", null, null, !NEW, !EMPTY, ALL);
					optionsPanel.add(new JLabel(I18N.getColonMsg("part")));
					optionsPanel.add(partCombo);
				}*/
				break;
		}
	}

	@SuppressWarnings("unchecked")
	public List<?> getAllEntities() {
//		App.trace("("+table.name+")"+"Table.getAllEntities() view=" + view);
		switch (view) {
			case TABLE_CATEGORY:
				objtype = "category";
				List<Category> categories = book.categories;
				if (typeCombo != null && typeCombo.getSelectedIndex() > 0) {
					categories = Category.findBeacon(book.categories,
							((String) typeCombo.getSelectedItem()).toLowerCase());
				}
				return (categories);
			case TABLE_CHAPTER:
				objtype = "chapter";
				List<Chapter> chapters = book.chapters;
				if (!mainFrame.bookPref.getAllPart()) {
					chapters = Chapter.find(chapters, mainFrame.getCurrentPart());
				}
				return (chapters);

			case TABLE_EVENT:
				objtype = "event";
				return (book.events);
			case TABLE_GENDER:
				objtype = "gender";
				return (book.genders);
			case TABLE_IDEA:
				objtype = "idea";
				return (book.ideas);
			case TABLE_ITEM:
				objtype = "item";
				return (book.items);
			case TABLE_LOCATION:
				objtype = "location";
				return (book.locations);
			case TABLE_MEMO:
				objtype = "memo";
				return (book.memos);
			case TABLE_PART:
				objtype = "part";
				return (book.parts);
			case TABLE_PERSON:
				objtype = "person";
				return (book.persons);
			case TABLE_PHOTO:
				objtype = "photo";
				return (book.photos);
			case TABLE_PLOT:
				objtype = "plot";
				return (book.plots);
			case TABLE_POV:
				objtype = "pov";
				return (book.povs);
			case TABLE_RELATION:
				objtype = "relation";
				return (book.relations);
			case TABLE_SCENE:
				objtype = "scene";
				List<Scene> scenes = book.scenes;
				if (sceneStateCombo != null && sceneStateCombo.getSelectedIndex() > 0) {
					scenes = Scene.getForStatus(scenes, sceneStateCombo.getSelectedIndex() - 1);
				}
				if (scenePovCombo != null && scenePovCombo.getSelectedIndex() > 0) {
					Pov pov = (Pov)scenePovCombo.getSelectedItem();
					if (pov != null) {
						scenes = Scene.getForPov(book, scenes, pov);
					}
				}
				if (sceneNarratorCombo != null && sceneNarratorCombo.getSelectedIndex() > 0) {
					Person person = (Person)sceneNarratorCombo.getSelectedItem();
					if (person != null) {
						scenes = Scene.getForPerson(book, scenes, person);
					}
				}
				if (partCombo != null && partCombo.getSelectedIndex() > 0) {
					Part part = (Part) partCombo.getSelectedItem();
					scenes = Scene.getForPart(book, scenes, part);
				}
				return (scenes);
			case TABLE_TAG:
				objtype = "tag";
				return (book.tags);
		}
		return new ArrayList<>();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		//App.trace("(" + table.name + ")" + ".modelPropertyChange(evt=" + evt.toString() + ")");
		String propName = evt.getPropertyName();
		if (!propName.contains(objtype)) {
			//TODO refresh this table if the entity is referenced in it
			return;
		}
		try {
			switch (Ctrl.whichACTION(propName)) {
				case DELETE:
					entityDelete(evt);
					return;
				case EXPORT:
					ExportTable.exec(mainFrame, (SbView) evt.getNewValue());
					return;
				case INIT:
					initTableModel(evt);
					return;
				case NEW:
					entityNew(evt);
					return;
				case UPDATE:
					entityUpdate(evt);
					return;
				case REFRESH:
					reloadData();
					break;
			}
		} catch (Exception e) {
		}
		//SwingUtil.forceRevalidate(this);
	}

	public JXTable getTable() {
		return table;
	}

	public String getTableName() {
		return (this.getName());
	}

	protected void sortByColumn(int col) {
		DefaultRowSorter<?, ?> sorter = ((DefaultRowSorter<?, ?>) table.getRowSorter());
		ArrayList<RowSorter.SortKey> list = new ArrayList<>();
		list.add(new RowSorter.SortKey(col, SortOrder.ASCENDING));
		sorter.setSortKeys(list);
		sorter.sort();
	}

	protected void entityNew(PropertyChangeEvent evt) {
//		App.trace("("+table.name+")"+"Table.newEntity(evt="+evt.toString()+")");
		AbstractEntity entity = (AbstractEntity) evt.getNewValue();
		List<Object> cols = entity.getRow();
		tableModel.addRow(cols.toArray());
		tableModel.fireTableDataChanged();
		lbCount.setText(I18N.getMsg("entity.count", table.getRowCount()));
	}

	protected void entityUpdate(PropertyChangeEvent evt) {
		//App.trace("(" + table.name + ")" + ".entityUpdate(" + evt.toString() + ")");
		AbstractEntity entity = (AbstractEntity) evt.getNewValue();
		for (int row = 0; row < tableModel.getRowCount(); ++row) {
			Long id = (Long) tableModel.getValueAt(row, 0);
			if (id.equals(entity.id)) {
				List<Object> rowVector = entity.getRow();
				int col = 0;
				for (Object val : rowVector) {
					tableModel.setValueAt(val, row, col);
					++col;
				}
			}
		}
		tableModel.fireTableDataChanged();
	}

	protected void entityDelete(PropertyChangeEvent evt) {
		//App.trace("("+table.name+")"+"Table.entityDelete(evt="+evt.toString()+")");
		int rows = tableModel.getRowCount();
		if (rows < 1) {
			return;
		}
		AbstractEntity entity = (AbstractEntity) evt.getOldValue();
		try {
			for (int row = 0; row < rows; row++) {
				Long id = (Long) tableModel.getValueAt(row, 0);
				if (id.equals(entity.id)) {
					tableModel.removeRow(row);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		tableModel.fireTableDataChanged();
		lbCount.setText(I18N.getMsg("entity.count", table.getRowCount()));
	}

	protected synchronized AbstractEntity getEntityFromRow(int row) {
		//App.trace("("+table.name+")"+"Table.getEntityFromRow(" + row + ")");
		if (row == -1) {
			return null;
		}
		try {
			int rowIndex = table.getRowSorter().convertRowIndexToModel(row);
			Long id = (Long) tableModel.getValueAt(rowIndex, 0);
			AbstractEntity ent = book.getEntity(table.getName(), id);
			return (ent);
		} catch (NumberFormatException ex) {
			App.error("(" + table.getName() + ")" + "Table.getEntityFromRow(" + row + ")", ex);
		}
		return null;
	}

	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		String actCmd = e.getActionCommand();
		Component comp = (Component) e.getSource();
		String compName = comp.getName();
		int row = table.getSelectedRow();
		/*App.trace("("+table.name+")"+"Table.actionPerformed(" + e.toString() + ")\n"
				+"actCmd=" + actCmd + ",compName=" + compName + ",row=" + row);*/
		if (comp instanceof JButton) {
			switch (getAction(compName)) {
				case BT_EDIT:
					sendSetEntityToEdit(row);
					break;
				case BT_NEW:
					table.clearSelection();
					sendSetNewEntityToEdit(getNewEntity());
					break;
				case BT_COPY:
					AbstractEntity entity = (AbstractEntity) getEntityFromRow(row);
					EntityTool.copy(book, entity);
					break;
				case BT_DELETE:
					if (table.getSelectedRowCount() > 0) {
						// delete multiple entities
						List<AbstractEntity> entities = new ArrayList<>();
						int[] rows = table.getSelectedRows();
						for (int row2 : rows) {
							entities.add(getEntityFromRow(row2));
						}
						CheckUsed du = new CheckUsed(book, entities);
						if (ConfirmDeleteDlg.show(mainFrame, entities, du)) {
							sendSetDeleteEntity(rows);
						}
					}
					break;
				case BT_ODT:
					sendSetLibreOffice(row);
					break;
			}
		} else if (actCmd.equals("comboBoxChanged")) {
			reloadData();
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		DefaultListSelectionModel selectionModel = (DefaultListSelectionModel) e.getSource();
		int count = selectionModel.getMaxSelectionIndex() - selectionModel.getMinSelectionIndex() + 1;
		if (count > 1) {
			btEdit.setEnabled(false);
			btCopy.setEnabled(false);
			btDelete.setEnabled(allowMultiDelete);
			return;
		}
		int row = selectionModel.getMinSelectionIndex();
		AbstractEntity entity = getEntityFromRow(row);
		boolean b = true;
		if (entity == null) {
			b = false;
		}
		btEdit.setEnabled(b);
		btCopy.setEnabled(b);
		btDelete.setEnabled(b);
		if (btODT != null) {
			btODT.setEnabled(b);
		}
	}

	private void sendSetLibreOffice(int row) {
//		App.trace("("+table.name+")"+"Table.sendSetLibreOffice(" + row + ")");
		Scene scene = (Scene) getEntityFromRow(row);
		String xfile = scene.xfile;
		if (xfile == null || xfile.isEmpty()) {
			xfile = IOTools.getDefaultFilePath(mainFrame, scene);
		}
		File file = new File(xfile);
		if (!file.exists()) {
			file = IOTools.createNewXfile(mainFrame, xfile);
			if (file == null) {
				return;
			}
			scene.xfile=(file.getAbsolutePath());
			mainFrame.getBookController().updateEntity(scene);
		} else {
			return;
		}
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			App.error("(" + table.getName() + ")" + "Table.sendSetLibreOffice(" + mainFrame + scene.name + ")", e);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		//App.trace("("+table.name+")"+"Table.mouseClicked() evt=" + e.toString());
		DefaultListSelectionModel selectionModel = (DefaultListSelectionModel) table.getSelectionModel();
		int row = selectionModel.getMinSelectionIndex();
		if (e.getClickCount() == 2) {
			int count = selectionModel.getMaxSelectionIndex() - selectionModel.getMinSelectionIndex() + 1;
			if (count > 1) {
				return;
			}
			sendSetEntityToEdit(row);
		} else {
			AbstractEntity entity = getEntityFromRow(selectionModel.getMinSelectionIndex());
			if (entity != null) {
				mainFrame.showInfo(entity);
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger()) {
			showPopup(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
			showPopup(e);
		}
	}

	@Override
	public void mouseEntered(MouseEvent me) {
	}

	@Override
	public void mouseExited(MouseEvent me) {
	}

	private void showPopup(MouseEvent e) {
		if (!(e.getSource() instanceof JXTable)) {
			return;
		}
		JXTable source = (JXTable) e.getSource();
		int row = source.rowAtPoint(e.getPoint());
		int column = source.columnAtPoint(e.getPoint());
		if (!source.isRowSelected(row)) {
			source.changeSelection(row, column, false, false);
		}
		AbstractEntity entity = getEntityFromRow(row);
		if (entity != null) {
			JPopupMenu popup = EntityUtil.createPopupMenu(mainFrame, entity);
			popup.show(e.getComponent(), e.getX(), e.getY());
		}
	}

	private List<TableCol> getColumns() {
		return (TableFactory.getColumns(book, view));
	}

	private List<String> getColumnNames() {
		List<String> cols = new ArrayList<>();
		columns.forEach((col) -> {
			cols.add(col.toString());
		});
		return (cols);
	}
	/*
	private class MyMouseAdapter extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent e) {
			DefaultListSelectionModel selectionModel = (DefaultListSelectionModel) table.getSelectionModel();
			int row = selectionModel.getMinSelectionIndex();
			if (e.getClickCount() == 2) {
				int count = selectionModel.getMaxSelectionIndex() - selectionModel.getMinSelectionIndex() + 1;
				if (count > 1) {
					return;
				}
				sendSetEntityToEdit(row);
			} else {
				mainFrame.getBookController().showInfo(getEntityFromRow(selectionModel.getMinSelectionIndex()));
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				showPopup(e);
			}
		}

		private void showPopup(MouseEvent e) {
			if (!(e.getSource() instanceof JXTable)) {
				return;
			}
			JXTable source = (JXTable) e.getSource();
			int row = source.rowAtPoint(e.getPoint());
			int column = source.columnAtPoint(e.getPoint());
			if (!source.isRowSelected(row)) {
				source.changeSelection(row, column, false, false);
			}
			AbstractEntity entity = getEntityFromRow(row);
			if (entity != null) {
				JPopupMenu popup = SwingUtil.createPopupMenu(mainFrame, entity);
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}
	 */
}
