/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.dialog.edit.CheckBoxPanel;
import desk.tools.swing.SwingUtil;
import db.I18N;
import db.entity.Book;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class ListEntitiesDlg extends AbstractDialog implements ActionListener {
	public enum ACTION {
		BT_ADD("btAdd"),
		BT_EDIT("btEdit"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}
	
	public static ACTION getAction(String str) {
		for (ACTION a:ACTION.values()) {
			if (a.toString().equals(str)) return(a);
		}
		return(ACTION.NONE);
	}


	private final Scene scene;
	private final Book.TYPE typeEntity;
	private CheckBoxPanel cbPanel;
	private JPanel entitiesPanel;

	public ListEntitiesDlg(MainFrame i, Scene s, Book.TYPE t) {
		super(i);
		scene = s;
		typeEntity = t;
		initAll();
	}

	@Override
	public void init() {
		this.setTitle(I18N.getMsg(typeEntity.toString()));
		this.setModal(true);
	}

	@Override
	public void initUi() {
		super.initUi();
		// layout
		setLayout(new MigLayout("wrap,fill", "[][]push[][]"));
		setTitle(I18N.getMsg(typeEntity.toString()));
		setIconImage(IconUtil.getIconImage("icon"));
		setPreferredSize(new Dimension(500, 600));

		entitiesPanel = new JPanel(new MigLayout());
		loadEntities();
		add(entitiesPanel, "span");

		JButton bt = new JButton();
		bt.setText(I18N.getMsg("z.edit"));
		bt.setIcon(IconUtil.getIcon("small/edit"));
		bt.setName(ACTION.BT_EDIT.toString());
		bt.addActionListener(this);
		add(bt);
		
		bt = new JButton();
		bt.setText(I18N.getMsg("z.new"));
		bt.setIcon(IconUtil.getIcon("small/new"));
		bt.setName(ACTION.BT_ADD.toString());
		bt.addActionListener(this);
		add(bt);

		addOkCancel();
		pack();
		setLocationRelativeTo(parent);
		this.setModal(true);
	}
	
	public List<Person> getPersons() {
		List<Person> sel = new ArrayList<>();
		List<AbstractEntity> entities = cbPanel.getSelectedEntities();
		entities.forEach((entity)-> {
			sel.add((Person)entity);
		});
		return (sel);
	}

	public List<Location> getLocations() {
		List<Location> sel = new ArrayList<>();
		List<AbstractEntity> entities = cbPanel.getSelectedEntities();
		entities.forEach((entity)-> {
			sel.add((Location)entity);
		});
		return (sel);
	}

	public List<Item> getItems() {
		List<Item> sel = new ArrayList<>();
		List<AbstractEntity> entities = cbPanel.getSelectedEntities();
		entities.forEach((entity)-> {
			sel.add((Item)entity);
		});
		return (sel);
	}

	public List<Plot> getPlots() {
		List<Plot> sel = new ArrayList<>();
		List<AbstractEntity> entities = cbPanel.getSelectedEntities();
		entities.forEach((entity)-> {
			sel.add((Plot)entity);
		});
		return (sel);
	}

	@SuppressWarnings("unchecked")
	private void loadEntities() {
		entitiesPanel.removeAll();
		cbPanel = new CheckBoxPanel(mainFrame);
		cbPanel.setSearch(typeEntity);
		cbPanel.setAutoSelect(false);
		JScrollPane scroller = new JScrollPane(cbPanel);
		SwingUtil.setUnitIncrement(scroller);
		SwingUtil.setMaxPreferredSize(scroller);
		entitiesPanel.add(scroller, "grow");

		List entities = book.getEntities(typeEntity);
		cbPanel.setEntityList(entities);
		cbPanel.initAll();
		switch (typeEntity) {
			case PERSON:
				scene.persons.forEach((id)-> {
					cbPanel.selectEntity(id);
				});
				break;
			case LOCATION:
				scene.locations.forEach((id)-> {
					cbPanel.selectEntity(id);
				});
				break;
			case ITEM:
				scene.items.forEach((id)-> {
					cbPanel.selectEntity(id);
				});
				break;
			case PLOT:
				scene.plots.forEach((id)-> {
					cbPanel.selectEntity(id);
				});
				break;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn=(JButton)e.getSource();
			switch(getAction(btn.getName())) {
				case BT_ADD:
					AbstractEntity entity;
					switch(typeEntity) {
						case PERSON:
							entity=new Person();
							break;
						case LOCATION:
							entity=new Location();
							break;
						case ITEM:
							entity=new Item();
							break;
						case PLOT:
							entity=new Plot();
							break;
						default:
							return;
					}
					mainFrame.showEditorAsDialog(entity);
					loadEntities();
					break;
				case BT_EDIT:
					AbstractEntity pointedEntity = cbPanel.getPointedEntity();
					if (pointedEntity != null) {
						mainFrame.showEditorAsDialog(pointedEntity);
					}
					break;
			}
		}
	}

}
