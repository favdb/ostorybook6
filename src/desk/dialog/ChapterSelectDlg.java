/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import db.entity.Chapter;
import desk.app.MainFrame;
import desk.dialog.edit.Editor;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class ChapterSelectDlg extends AbstractDialog {

	private JList lstChapters;
	private final Chapter chapter;
	
	public ChapterSelectDlg(JFrame p,MainFrame m, Chapter c) {
		super(m);
		mainFrame=m;
		book=m.book;
		chapter=c;
		init();
	}

	public ChapterSelectDlg(MainFrame m, Chapter c) {
		super(m);
		mainFrame=m;
		chapter=c;
		init();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		this.setModal(true);
		JScrollPane scroller = new JScrollPane();
		scroller.setPreferredSize(new Dimension(310,320));
        lstChapters = new JList();
		lstChapters.setModel(new DefaultListModel());
		loadList(-1);
		lstChapters.setSelectedValue(chapter, true);
        scroller.setViewportView(lstChapters);
		JButton btNew = SwingUtil.initButton("", "small/new_chapter","chapter.new");
        btNew.addActionListener((java.awt.event.ActionEvent evt) -> {
			newChapter();
		});

		JButton btOK = SwingUtil.initButton("ok", "small/ok","ok");
        btOK.addActionListener((java.awt.event.ActionEvent evt) -> {
			dispose();
		});
		
		JButton btCancel = SwingUtil.initButton("cancel", "small/cancel","cancel");
        btCancel.addActionListener((java.awt.event.ActionEvent evt) -> {
			canceled=true;
			dispose();
		});
		
		//layout
		setLayout(new MigLayout());
		setTitle(I18N.getMsg("chapter"));
		add(scroller,"split 2");
		add(btNew,"top,wrap");
		add(btCancel,"split 2,right");
		add(btOK);
		pack();
		setLocationRelativeTo(getParent());
	}

	@SuppressWarnings("unchecked")
	private void loadList(int first) {
		DefaultListModel listModel=(DefaultListModel)lstChapters.getModel();
		listModel.removeAllElements();
		List<Chapter> chapters = mainFrame.book.chapters;
		for (Chapter c : chapters) {
			listModel.addElement(c);
		}
		if (first!=-1) {
			lstChapters.setSelectedIndex(first);
		}
	}

	private void newChapter() {
		Editor.show(mainFrame, new Chapter(), this);
		loadList(-1);
		lstChapters.setSelectedValue(chapter, true);
	}

	public Chapter getSelectedChapter() {
		System.out.println("initialChapter="+chapter.toString());
		System.out.println("selectedChapterIndex="+lstChapters.getSelectedIndex());
		System.out.println("selectedChapter="+((Chapter)lstChapters.getSelectedValue()).toString());
		return((Chapter)lstChapters.getSelectedValue());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
