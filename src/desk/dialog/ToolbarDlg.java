/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import desk.app.App;
import desk.app.MainFrame;
import db.I18N;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class ToolbarDlg extends AbstractDialog {
	/* desciption des boutons
	paramètre 1 = groupe (file,new, table, view
	paramètre 2 = 
	paramètre 3 = nom de l'icone
	paramètre 4 = texte du tooltips
	*/
		String boutons[][] = {
			{"file", "0", "small/file-new", "file.new"}
			, {"file", "0", "small/file-open", "file.open"}
			, {"file", "0", "small/file-save", "file.save"}
			, {"new", "0", "small/pov", "pov"}
			, {"new", "0", "small/part", "part"}
			, {"new", "0", "small/chapter", "chapter"}
			, {"new", "0", "small/scene", "scene"}
			, {"new", "1", "small/person", "person"}
			, {"new", "1", "small/gender", "gender"}
			, {"new", "1", "small/relation", "relation"}
			, {"new", "2", "small/location", "location"}
			, {"new", "3", "small/item", "item"}
			, {"new", "4", "small/tag", "tag"}
			, {"new", "4", "small/event", "event"}
			, {"new", "4", "small/photo", "photo"}
			, {"new", "5", "small/memo", "memo"}
			, {"new", "6", "small/idea", "idea"}
			, {"table", "0", "small/povs", "povs"}
			, {"table", "0", "small/parts", "parts"}
			, {"table", "0", "small/chapters", "chapters"}
			, {"table", "0", "small/scenes", "scenes"}
			, {"table", "1", "small/persons", "persons"}
			, {"table", "1", "small/genders", "genders"}
			, {"table", "1", "small/relations", "relations"}
			, {"table", "2", "small/locations", "locations"}
			, {"table", "3", "small/items", "items"}
			, {"table", "4", "small/tags", "tags"}
			, {"table", "4", "small/events", "events"}
			, {"table", "4", "small/photos", "photos"}
			, {"table", "5", "small/memos", "memos"}
			, {"table", "6", "small/ideas", "ideas"}
			, {"view", "0", "small/chrono", "view.chrono"}
			, {"view", "1", "small/work", "view.work"}
			, {"view", "2", "small/manage", "view.manage"}
			, {"view", "3", "small/reading", "view.reading"}
			, {"view", "4", "small/memoria", "view.pov"}
			, {"view", "5", "small/storyboard", "view.storyboard"}
			, {"view", "6", "small/typist", "typist"}
				/*, {"end", "", "", ""}*/
		};
	private JPanel panelFile;
	private JPanel panelNew;
	private JPanel panelTable;
	private JPanel panelView;
	private JButton btSelectAll;
	private JButton btSelectNotAll;
	
	@SuppressWarnings("OverridableMethodCallInConstructor")
	public ToolbarDlg(MainFrame m) {
		super(m);
		initAll();
	}

	@Override
	public void init() {
	}
	
	@Override
	public void initUi() {
		String param = App.getInstance().getPreferences().prefUI.getTb();
		while (param.length() < (boutons.length-1)) {
			param += "1";// default all button are visible
		}
		if (param.length()>boutons.length) param=param.substring(0,boutons.length);
		while (param.length()<boutons.length) param=param+"1";
		panelFile = initPanel("file",param);
		panelNew = initPanel("new",param);
		panelTable = initPanel("table",param);
		panelView = initPanel("view",param);
		btSelectAll = new JButton(I18N.getMsg("toolbar.select.all"));
        btSelectAll.addActionListener((java.awt.event.ActionEvent evt) -> {
			selectAll(true);
		});
		btSelectNotAll = new JButton(I18N.getMsg("toolbar.select.notall"));
        btSelectNotAll.addActionListener((java.awt.event.ActionEvent evt) -> {
			selectAll(false);
		});
		
		//layout
		setLayout(new MigLayout("wrap 4", "[]", "[]20[]"));
		setTitle(I18N.getMsg("toolbar.select"));
		add(btSelectAll,"span 2");
		add(btSelectNotAll, "span 2");
		add(panelFile);
		add(panelNew);
		add(panelTable);
		add(panelView);
		add(buttonOk(), "sg,span,split 2,right");
		add(buttonCancel(), "sg");
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);		
	}
	
	public JPanel initPanel(String name,String param) {
		JPanel p=new JPanel();
        p.setBorder(
			BorderFactory.createTitledBorder(null,
				I18N.getMsg(name),
				TitledBorder.DEFAULT_JUSTIFICATION,
				TitledBorder.DEFAULT_POSITION,
				new Font("Dialog", 1, 12)));
		p.setLayout(new MigLayout());
		String line = "0";
		int i = 0;
		for (String b[] : boutons) {
			if (b[0].equals(name)) {
				ImageIcon icon=IconUtil.createImageIcon((b[2] + ".png"));
				JToggleButton bt = new JToggleButton(icon);
				bt.setName(b[2].substring(b[2].indexOf("/") + 1));
				bt.setToolTipText(I18N.getMsg(b[3]));
				bt.setMargin(new Insets(0, 0, 0, 0));
				bt.setSelected((param.charAt(i) == '1'));
				if (param.charAt(i) == '1') {
					bt.setBorder(BorderFactory.createLineBorder(Color.green, 3));
				} else {
					bt.setBorder(BorderFactory.createLineBorder(Color.red, 3));
				}
				if (!b[1].equals(line)) {
					p.add(new JLabel(""), "wrap");
					line = b[1];
				}
				bt.addActionListener((java.awt.event.ActionEvent evt) -> {
					JToggleButton bx = (JToggleButton)evt.getSource();
					if (bx.isSelected()) {
						bt.setBorder(BorderFactory.createLineBorder(Color.green, 3));
					} else {
						bt.setBorder(BorderFactory.createLineBorder(Color.red, 3));
					}
				});
				p.add(bt);
			}
			i++;
		}
		return(p);
	}

	@Override
	public AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String param = "";
				param+=checkSel(panelFile);
				param+=checkSel(panelNew);
				param+=checkSel(panelTable);
				param+=checkSel(panelView);
				App.getInstance().getPreferences().prefUI.setTb(param);
				canceled = false;
				dispose();
			}
		};
	}
	
	private String checkSel(JPanel panel) {
		String r="";
		Component comps[] = panel.getComponents();
		for (Component comp : comps) {
			if (!(comp instanceof JToggleButton)) continue;
			JToggleButton bt = (JToggleButton) comp;
			r+=((bt.isSelected()?"1":"0"));
		}
		return(r);
	}

	private void selectAll(boolean b) {
		setSel(panelFile,b);
		setSel(panelNew,b);
		setSel(panelTable,b);
		setSel(panelView,b);
	}

	private void setSel(JPanel panel, boolean b) {
		Component comps[] = panel.getComponents();
		for (Component comp : comps) {
			if (!(comp instanceof JToggleButton)) continue;
			JToggleButton bt = (JToggleButton) comp;
			bt.setSelected(b);
			if (bt.isSelected()) {
				bt.setBorder(BorderFactory.createLineBorder(Color.green, 3));
			} else {
				bt.setBorder(BorderFactory.createLineBorder(Color.red, 3));
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
