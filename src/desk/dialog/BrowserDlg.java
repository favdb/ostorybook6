/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.dialog;

import desk.app.MainFrame;
import db.I18N;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class BrowserDlg extends AbstractDialog {
	
	private final String url;
	private final String titleDlg;
	
	public BrowserDlg(java.awt.Frame parent, String u, String t) {
		url=u;
		titleDlg=t;
		initAll();
	}

	@Override
	public void init() {
	}
	
	@Override
	public void initUi() {
		JEditorPane jEditorPane = new JEditorPane();
        jEditorPane.setEditable(false);
		JScrollPane scroller = new JScrollPane();
        scroller.setPreferredSize(new java.awt.Dimension(640, 480));
        scroller.setViewportView(jEditorPane);
		try {
			jEditorPane.setPage(url);
		} catch (IOException ex) {
			jEditorPane.setText(I18N.getMsg("net.connection.failed", url) + "\n");
		}
		
		//layout
		setLayout(new MigLayout("wrap 9"));
		setTitle(titleDlg);
		add(scroller,"wrap");
		add(buttonCancel("close"), "right");
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}
	
	public static void show(MainFrame m, String str, String msg) {
		BrowserDlg dlg=new BrowserDlg(m,str,msg);
		dlg.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
