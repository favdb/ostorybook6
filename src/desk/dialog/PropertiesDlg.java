/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import db.entity.Book;
import db.entity.BookAssistant;
import db.entity.BookInfo;
import db.I18N;
import db.entity.SbCalendar;
import desk.app.pref.BookPref;
import desk.assistant.AssistantBookDlg;
import desk.dialog.sbcalendar.SbCalendarDlg;
import desk.tools.FontUtil;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import desk.tools.EntityUtil;
import desk.tools.markdown.Markdown;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class PropertiesDlg extends AbstractDialog implements ActionListener {

	private JPanel properties;
	private JPanel assistant;
	private JPanel editor;
	private JPanel info;

	public enum ACTION {
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	BookPref pref;

	private JButton btCalendar,
			btTemplate;
	private JCheckBox ckScenario,
			ckUseCalendar,
			ckUseNonModalEditors,
			ckUseX,
			ckStep,
			ckPolti;
	private JEditorPane edAssistant;
	private JTextArea taNotes, taBlurb;
	private JTextField tfTitle,
			tfSubtitle,
			tfAuthor,
			tfCopyright,
			tfExtension,
			tfTemplate,
			tfName;
	private JTextPane taFileInfo;
	private JLabel lbName,
			lbExtension,
			lbTemplate;
	private SbCalendar theCalendar;

	public PropertiesDlg(MainFrame m) {
		super(m);
		this.pref = m.bookPref;
		initAll();
	}

	public PropertiesDlg(Book book, BookPref pref) {
		super((MainFrame) null);
		this.book = book;
		this.pref = pref;
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		super.initUi();
		Dimension dim = new Dimension(640, 480);
		setLayout(new MigLayout("wrap,fill"));
		setTitle(I18N.getMsg("properties"));
		initProperties();
		initExternEditor();
		initAssistant();
		initFileInfo();

		JTabbedPane tbPane = new JTabbedPane();
		tbPane.addTab(I18N.getMsg("properties"), properties);
		if (mainFrame!=null) {
			tbPane.addTab(I18N.getMsg("assistant"), assistant);
		}
		else {
			properties.add(new JLabel(I18N.getColonMsg("assistant")));
			properties.add(assistant);
		}
		tbPane.addTab(I18N.getMsg("xeditor"), editor);
		if (!book.getTitle().isEmpty()) {
			tbPane.addTab(I18N.getMsg("file.info"), info);
		}
		tbPane.setMinimumSize(dim);
		if (mainFrame!=null) {
			tbPane.setPreferredSize(dim);
		}

		// layout
		add(tbPane, "wrap,grow");
		addOkCancel();
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}

	private JPanel initProperties() {
		BookInfo inf = book.info;
		properties = new JPanel(new MigLayout("wrap", "[al right][grow]"));
		properties.add(new JSeparator());//espace vide pour cadrage
		ckScenario = Ui.initCheckBox(properties, "book.scenario", inf.scenario, !Ui.MANDATORY);
		tfTitle = Ui.initTextField(properties, "book.title", 0, book.getTitle(), MANDATORY, !INFO);
		tfSubtitle = Ui.initTextField(properties, "book.subtitle", 0, inf.getSubtitle(), !MANDATORY, !INFO);
		tfAuthor = Ui.initTextField(properties, "book.author", 0, inf.getAuthor(), !MANDATORY, !INFO);
		tfCopyright = Ui.initTextField(properties, "book.copyright", 0, inf.getCopyright(), !MANDATORY, !INFO);
		taBlurb = Ui.initTextArea(properties, "book.blurb",inf.getBlurb(), 6, !MANDATORY, !INFO, null);
		taNotes = Ui.initTextArea(properties, "z.notes",inf.getNotes(), 6, !MANDATORY, !INFO, null);

		// calendar
		boolean b=false;
		properties.add(new JSeparator());//espace vide pour cadrage
		if (inf.calendar!=null) b=inf.calendar.isCalendar();
		ckUseCalendar = addCheckBox(properties, "calendar.use", b, "span, split 2", !MANDATORY);
		ckUseCalendar.addActionListener((ActionEvent evt) -> {
			if (ckUseCalendar.isSelected()) {
				btCalendar.setEnabled(true);
			} else {
				btCalendar.setEnabled(false);
			}
		});
		theCalendar = book.info.getCalendar();
		btCalendar = new JButton(I18N.getMsg("calendar"));
		btCalendar.setIcon(IconUtil.getIcon("small/chrono"));
		btCalendar.addActionListener((ActionEvent evt) -> {
			SbCalendarDlg dlg = new SbCalendarDlg(this, book.info.getCalendar());
			dlg.setVisible(true);
			theCalendar = book.info.getCalendar();
		});
		btCalendar.setEnabled(ckUseCalendar.isSelected());
		properties.add(btCalendar);

		return (properties);
	}

	private void addTitle(JPanel panel, String i18nKey) {
		JLabel lb = new JLabel(I18N.getMsg(i18nKey));
		lb.setFont(FontUtil.getBold(12));
		panel.add(lb, "span,gaptop 10");
	}

	@Override
	public MainFrame getMainFrame() {
		return (mainFrame);
	}

	private JCheckBox initCheckBox(String title, boolean value) {
		JCheckBox cb = new JCheckBox(I18N.getMsg(title));
		cb.setSelected(value);
		return (cb);
	}

	private void getAssistant() {
		BookAssistant ass = book.info.assistant;
		if (!ass.isEmpty()) {
			StringBuilder w = new StringBuilder("<html>");
			w.append("<p><b>")
					.append(I18N.getMsg("assistant.step"))
					.append("</b><br>")
					.append("<blockquote>")
					.append(ass.getStep())
					.append("<br><b>")
					.append(I18N.getMsg("z.notes")).append("</b><br>")
					.append("<blockquote>")
					.append(ass.getStepNotes())
					.append("</blockquote>")
					.append("</blockquote>")
					.append("</p>");
			w.append("<p><b>").append(I18N.getMsg("assistant.polti")).append("</b><br>")
					.append("<blockquote>")
					.append(ass.getPolti())
					.append("<br><b>").append(I18N.getMsg("z.notes")).append("</b><br>")
					.append("<blockquote>")
					.append(ass.getPoltiNotes())
					.append("</blockquote>")
					.append("</blockquote>")
					.append("</p>");
			w.append("</html>");
			edAssistant.setText(w.toString());
		}
	}

	private JPanel initAssistant() {
		edAssistant = new JEditorPane("text/html", "");
		getAssistant();

		JScrollPane tas = new JScrollPane(edAssistant);
		SwingUtil.setMaxPreferredSize(tas);

		JButton bt = new JButton(I18N.getMsg("assistant"));
		bt.setIcon(IconUtil.getIcon("small/target"));
		bt.addActionListener((ActionEvent evt) -> {
			if (ckStep.isSelected() || ckPolti.isSelected()) {
				AssistantBookDlg.show(mainFrame);
				getAssistant();
			}
		});
		ckStep = initCheckBox("assistant.step_use", book.info.isUseStep());
		ckPolti = initCheckBox("assistant.polti_use", book.info.isUsePolti());

		//layout
		assistant = new JPanel(new MigLayout("wrap", "[grow]", ""));
		assistant.add(ckStep, "split 2");
		assistant.add(ckPolti);
		if (mainFrame!=null) {
			assistant.add(tas, "growx");
			assistant.add(bt, "right");
		}
		return (assistant);
	}

	private JPanel initExternEditor() {
		//layout
		editor = new JPanel(new MigLayout("wrap 3", "[][grow][]", "[][][][]"));

		ckUseNonModalEditors = initCheckBox("editor.nonmodal", pref.Editor.modless);
		editor.add(ckUseNonModalEditors, "span");

		ckUseX = new JCheckBox(I18N.getMsg("xeditor.ask_use"));
		ckUseX.addActionListener((java.awt.event.ActionEvent evt) -> {
			changeXeditor();
		});
		editor.add(ckUseX, "span");

		lbName = new JLabel(I18N.getMsg("xeditor.name"));
		tfName = new JTextField();
		editor.add(lbName);
		editor.add(tfName,"span, growx");

		lbExtension = new JLabel(I18N.getMsg("xeditor.extension"));
		tfExtension = new JTextField();
		tfExtension.setColumns(4);
		editor.add(lbExtension);
		editor.add(tfExtension,"wrap");

		lbTemplate = new JLabel(I18N.getMsg("xeditor.template"));
		tfTemplate = new JTextField();
		btTemplate = new JButton();
		btTemplate.setIcon(IconUtil.getIcon("small/file-open"));
		btTemplate.setMargin(new Insets(0, 0, 0, 0));
		btTemplate.addActionListener((java.awt.event.ActionEvent evt) -> {
			JFileChooser chooser = new JFileChooser(tfTemplate.getText());
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int i = chooser.showOpenDialog(null);
			if (i != 0) {
				return;
			}
			File file = chooser.getSelectedFile();
			tfTemplate.setText(file.getAbsolutePath());
			tfTemplate.setCaretPosition(0);
		});
		editor.add(lbTemplate);
		editor.add(tfTemplate, "growx");
		editor.add(btTemplate);

		if (pref.Editor.xUse) {
			ckUseX.setSelected(pref.Editor.xUse);
			tfName.setText(pref.Editor.xName);
			tfExtension.setText(pref.Editor.xExtend);
			tfTemplate.setText(pref.Editor.xTemplate);
		} else {
			lbName.setEnabled(false);
			lbExtension.setEnabled(false);
			lbTemplate.setEnabled(false);
			tfName.setEnabled(false);
			tfExtension.setEnabled(false);
			tfTemplate.setEnabled(false);
			btTemplate.setEnabled(false);
		}

		return (editor);
	}

	private void changeXeditor() {
		boolean b = ckUseX.isSelected();
		ckUseNonModalEditors.setEnabled(!b);
		lbName.setEnabled(b);
		lbExtension.setEnabled(b);
		lbTemplate.setEnabled(b);
		tfName.setEnabled(b);
		tfExtension.setEnabled(b);
		tfTemplate.setEnabled(b);
		btTemplate.setEnabled(b);
	}

	private JPanel initFileInfo() {
		info = new JPanel(new MigLayout("wrap,fill", "", "[grow]"));
		if (mainFrame==null || mainFrame.xml == null) {
			info.add(new JLabel(I18N.getMsg("file.empty")));
			return (info);
		}

		taFileInfo = new JTextPane();
		taFileInfo.setEditable(false);
		taFileInfo.setContentType("text/html");
		taFileInfo.setText(EntityUtil.getStats(mainFrame, true));
		taFileInfo.setCaretPosition(0);
		JScrollPane scroller = new JScrollPane();
		scroller.setViewportView(taFileInfo);

		//layout
		info.add(scroller, "grow");
		return (info);
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				applySettings();
			}
		};
	}

	/**
	 * check()
	 *
	 * @return true if there is no error
	 */
	private boolean check() {
		String errors = "";
		if (tfTitle.getText().isEmpty()) {
			errors += I18N.getMsg("book.title.missing") + "\n";
		}
		if (mainFrame != null && ckUseX.isSelected()) {
			if (tfName.getText().isEmpty()) {
				errors += I18N.getMsg("xeditor.name.missing") + "\n";
			}
			if (tfExtension.getText().isEmpty()) {
				errors += I18N.getMsg("xeditor.extension.missing") + "\n";
			}
			if (tfTemplate.getText().isEmpty()) {
				errors += I18N.getMsg("xeditor.template.missing") + "\n";
			}
		}
		if (errors.isEmpty()) {
			return (true);
		}
		JOptionPane.showMessageDialog(this, errors,
				I18N.getMsg("z.error"), JOptionPane.ERROR_MESSAGE);
		return (false);
	}

	private void applySettings() {
		if (!check()) {
			return;
		}
		// properties
		boolean ck = ckScenario.isSelected();
		if (ck != book.info.scenario) {
			if (book.scenes.size() > 0 
					&& !HtmlUtil.htmlToText(book.scenes.get(0).writing).isEmpty()) {
				if (askForConversion() == 0) {
					return;
				} else {
					return;
				}
			}
			book.info.setScenario(ck);
		}
		book.info.setTitle(tfTitle.getText());
		book.info.setSubtitle(tfSubtitle.getText());
		book.info.setAuthor(tfAuthor.getText());
		book.info.setCopyright(tfCopyright.getText());
		book.info.setBlurb(taBlurb.getText());
		book.info.setNotes(taNotes.getText());

		if (ckUseX.isSelected()) {
			pref.Editor.xUse = ckUseX.isSelected();
			pref.Editor.xName = tfName.getText();
			pref.Editor.xTemplate = tfTemplate.getText();
			pref.Editor.xExtend = tfExtension.getText();
		} else {
			pref.Editor.xUse = false;
			pref.Editor.xName = "";
			pref.Editor.xTemplate = "";
			pref.Editor.xExtend = "";
		}

		if (mainFrame != null) {
			if (ckUseCalendar.isSelected()) {
				book.info.setCalendar(theCalendar);
			} else {
				book.info.setCalendar(null);
			}

			book.info.setStepUse(ckStep.isSelected());
			book.info.setPoltiUse(ckPolti.isSelected());

			pref.save(mainFrame.xml.getOnlyPath());

			mainFrame.refresh();
		}
		dispose();
	}

	/**
	 * change mode from/to scenario
	 *
	 * @return 0 if canceled, 1 if conversion is ok
	 */
	private int askForConversion() {
		// warning for conversion
		Object args[] = {book.scenes.size(), (book.info.scenario ? "Html" : "Markdown")};
		int reponse = JOptionPane.showConfirmDialog(null,
				I18N.getMsg("file.conversion.ask", args),
				I18N.getMsg("file.conversion",
						(book.info.scenario ? "Html" : "Markdown")),
				JOptionPane.YES_NO_OPTION);
		if (reponse == JOptionPane.CANCEL_OPTION) {
			return (0);
		}
		book.scenes.forEach((scene) -> {
			if (!HtmlUtil.htmlToText(scene.writing).isEmpty()) {
				String str;
				if (book.isScenario()) {
					str = Markdown.toHtml(scene.writing);
				} else {
					str = Markdown.toMarkdown(scene.writing);
				}
				scene.writing=str;
			}
		});
		mainFrame.setModified();
		return (1);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton bt = (JButton) e.getSource();
			switch (getAction(bt.getName())) {

			}
		}
	}

}
