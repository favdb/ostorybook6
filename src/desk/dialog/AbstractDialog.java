/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import db.entity.Book;
import tools.IconUtil;
import java.awt.event.ActionEvent;

import desk.app.App;
import desk.app.MainFrame;
import desk.tools.swing.SwingUtil;
import db.I18N;
import db.entity.AbstractEntity;
import desk.dialog.edit.Editor;
import desk.tools.FontUtil;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import desk.tools.combobox.ComboUtil;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public abstract class AbstractDialog extends JDialog implements ActionListener {

	protected MainFrame mainFrame;
	protected JComponent parent;

	public boolean canceled = false;
	public JButton btOk;
	public JButton btCancel;
	public Book book;

	public AbstractDialog() {
		this.mainFrame = null;
		this.book = null;
		this.parent = null;
	}

	public AbstractDialog(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		if (mainFrame != null) {
			this.book = mainFrame.book;
		} else {
			this.book = null;
		}
		this.parent = null;
	}

	public AbstractDialog(JComponent parent) {
		this.parent = parent;
		this.mainFrame = null;
		this.book = null;
	}

	public void init() {
	}

	public void initUi() {
		if (mainFrame != null) {
			setIconImage(mainFrame.getIconImage());
		} else {
			setIconImage(IconUtil.getIconImage(App.class, "icon"));
		}
	}

	public final void initAll() {
		init();
		initUi();
	}

	public MainFrame getMainFrame() {
		return (mainFrame);
	}

	public JButton getButton(String text, String icon, String tips) {
		return (SwingUtil.initButton(text, icon, tips));
	}

	public JButton getButton(String text, String icon) {
		return (getButton(text, icon, ""));
	}

	public JButton getButton(String text) {
		return (getButton(text, "", ""));
	}

	public JLabel getLabel(String text, String icon) {
		JLabel label = new JLabel(I18N.getMsg(text));
		if (!icon.isEmpty()) {
			label.setIcon(IconUtil.getIcon(App.class, icon));
		}
		return (label);
	}

	public JTextField getTextField(String str, int col) {
		JTextField tf = new JTextField();
		if (!str.isEmpty()) {
			tf.setText(str);
		}
		if (col != 0) {
			tf.setColumns(col);
		}
		return (tf);
	}

	public JTextArea getTextArea(String str, int col, int row) {
		JTextArea tf = new JTextArea();
		if (!str.isEmpty()) {
			tf.setText(str);
		}
		if (col != 0) {
			tf.setColumns(col);
		}
		if (row > 0) {
			tf.setRows(row);
		}
		tf.setLineWrap(true);
		return (tf);
	}

	public JButton buttonClose() {
		AbstractAction act = actionClose();
		JButton btClose = new JButton(act);
		btClose.setText(I18N.getMsg("z.close"));
		btClose.setIcon(IconUtil.getIcon("small/close"));
		SwingUtil.addEnterAction(btClose, act);
		SwingUtil.addEscAction(btClose, act);
		return btClose;
	}

	public JButton buttonOk() {
		AbstractAction act = actionOk();
		btOk = new JButton(act);
		btOk.setText(I18N.getMsg("z.ok"));
		btOk.setIcon(IconUtil.getIcon("small/ok"));
		SwingUtil.addEnterAction(btOk, act);
		return btOk;
	}

	public JButton buttonOk(String str) {
		btOk = buttonOk();
		btOk.setText(I18N.getMsg(str));
		return btOk;
	}

	public JButton buttonCancel() {
		AbstractAction act = actionCancel();
		btCancel = new JButton(act);
		btCancel.setText(I18N.getMsg("z.cancel"));
		btCancel.setIcon(IconUtil.getIcon("small/cancel"));
		SwingUtil.addEscAction(btCancel, act);
		return (btCancel);
	}

	public JButton buttonCancel(String str) {
		buttonCancel();
		btCancel.setText(I18N.getMsg(str));
		return btCancel;
	}

	public void addOkCancel() {
		add(buttonOk(), "right,split 2");
		add(buttonCancel());
	}

	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canceled = false;
				dispose();
			}
		};
	}

	protected AbstractAction actionCancel() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canceled = true;
				dispose();
			}
		};
	}

	protected AbstractAction actionClose() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//App.trace("closeAction form abstract");
				canceled = false;
				dispose();
			}
		};
	}

	public boolean isCanceled() {
		return canceled;
	}

	/**
	 * add a JLabel
	 * @param panel
	 * @param title
	 * @param isTop
	 * @param mandatory 
	 */
	public void addLabel(JComponent panel, String title, boolean isTop, boolean mandatory) {
		String strtop="";
		if (isTop) {
			strtop="top,";
		}
		JLabel lb = new JLabel(I18N.getColonMsg(title));
		if (mandatory) {
			lb.setFont(FontUtil.getBold());
		}
		panel.add(lb,strtop+"right");
	}

	/**
	 * Add a JLabel and a JComponent
	 * @param panel : dialog wher to add the JTextField
	 * @param title : text of the JLabel
	 * @param top : top alignement, may be empty, "wrap" for wrapping JLabel
	 * @param comp : JComponent to add
	 * @param grow : growing the JComponent
	 * @param mandatory : set bold font for the JLabel 
	 */
	public void addField(
			JComponent panel,
			String title,
			String top,
			JComponent comp,
			boolean grow,
			boolean mandatory) {
		JLabel lb = new JLabel(I18N.getColonMsg(title));
		if (mandatory) {
			lb.setFont(FontUtil.getBold());
		}
		String c = top;
		String strgrow = "";
		if (!top.isEmpty()) {
			c += ",right";
		} else {
			c = "right";
		}
		if (grow) {
			strgrow = "grow";
		}
		if (top.contains("wrap")) {
			if (strgrow.isEmpty()) strgrow = "wrap";
			else strgrow+=",wrap";
		}
		try {
			panel.add(lb, c);
			panel.add(comp, strgrow);
		} catch (Exception e) {
			add(lb, c);
			add(comp, strgrow);
		}
	}

	public JTextArea addTextArea(JPanel panel, String title, String val, int len,
			boolean mandatory, boolean info, JTabbedPane tab) {
		JTextArea ta = new JTextArea();
		ta.setName(title);
		if (val != null) {
			ta.setText(val);
		}
		if (info) {
			ta.setEditable(false);
		}
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		JScrollPane scroller = new JScrollPane(ta);
		scroller.setMinimumSize(MINIMUM_SIZE);
		if (tab!=null) {
			scroller.setMaximumSize(new Dimension(640, 110));
		}
		Ui.addField(panel, title, "top", scroller, GROW, mandatory, tab);
		return (ta);
	}

	/**
	 * init a JCheckBox
	 * 
	 * @param panel : where to add the JCheckBox
	 * @param title : text for the JCheckBox
	 * @param check : is JCheckBox must be selected
	 * @param constraints : layout constraints
	 * @param isMandatory : if mandatory then set bold font
	 * @return the JCheckBox element
	 */
	public JCheckBox addCheckBox(JComponent panel, String title, boolean check, String constraints, boolean isMandatory) {
		JCheckBox cb = new JCheckBox();
		cb.setName(title);
		cb.setText(I18N.getMsg(title));
		cb.setSelected(check);
		if (isMandatory) {
			cb.setFont(FontUtil.getBold());
		}
		cb.addActionListener(this);
		panel.add(cb,constraints);
		return (cb);
	}

	/**
	 * init a ComboBox
	 * 
	 * @param panel : AbstractDialog where to add the ComboBox
	 * @param title : title and name of the JPanel
	 * @param objtype : entity type
	 * @param toSel : entity to select
	 * @param toHide : entity to hide
	 * @param mandatory: if selection is mandatory
	 * @param addNew : is adding a new entity
	 * @param addEmpty : is empty item is allowed
	 * @return the JComboBox object
	 */
	public JComboBox addComboBox(
			JComponent panel,
			String title,
			Book.TYPE objtype,
			AbstractEntity toSel,
			AbstractEntity toHide,
			boolean mandatory,
			boolean addNew,
			boolean addEmpty) {
		JComboBox combo = new JComboBox();
		combo.setName(title);
		ComboUtil.fillEntity(book, combo, objtype, toSel, toHide, addNew, addEmpty, !ALL);
		if (addNew) {
			JPanel p = new JPanel(new MigLayout("inset 0"));
			JButton bt = new JButton(IconUtil.getIcon("small/new"));
			SwingUtil.setForcedSize(bt, new Dimension(22, 22));
			bt.addActionListener((ActionEvent e) -> {
				AbstractEntity newEntity = book.getEntityNew(objtype);
				boolean rc = Editor.show(mainFrame, newEntity, this);
				if (rc != true) {
					book.entityNew(newEntity);
					ComboUtil.fillEntity(book, combo, objtype, newEntity, toHide, addNew, addEmpty, ALL);
				}
			});
			p.add(combo);
			p.add(bt, "shrink");
			addLabel(panel, title, !TOP, mandatory);
			panel.add(p, "al left");
		} else {
			addField(panel, title, "", combo, !GROW, mandatory);
		}
		combo.addActionListener(this);
		return (combo);
	}

	public JTextField addTextField(JComponent p, String title, int len, String val, boolean isMandatory, boolean isInfo) {
		JTextField tf = new JTextField();
		tf.setName(title);
		tf.setText(val);
		boolean grow = !GROW;
		if (len > 0) {
			tf.setColumns(len);
		} else {
			grow=GROW;
		}
		if (isInfo) {
			tf.setEditable(false);
		}
		addField(p,title,"",tf,grow,isMandatory);
		return (tf);
	}

}
