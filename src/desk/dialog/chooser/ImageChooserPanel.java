/*
PanelIconChooser: Clever Icon Chooser

This class needs MigLayout to compile:
http://www.miglayout.com/

Copyright (C) 2016 FaVdB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.dialog.chooser;

import desk.interfaces.IRefreshable;
import desk.tools.IconButton;
import desk.tools.swing.SwingUtil;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

@SuppressWarnings("serial")
public class ImageChooserPanel extends JPanel implements IRefreshable, MouseListener {
	private IconButton btChooser;
	private IconButton btClear;
	private final Icon startIcon;
	private final String startIconFile;
	private JTextField tfFile;
	private String currentFile;
	private JLabel lbIcon;
	private final String path;
	private final boolean isIcon;

	public ImageChooserPanel(String path, Icon startIcon, String startIconFile, boolean isIcon) {
		this.path=path;
		this.startIcon = startIcon;
		this.startIconFile = startIconFile;
		currentFile=startIconFile;
		this.isIcon=isIcon;
		initGUI();
	}

	private void initGUI() {
		MigLayout layout = new MigLayout("insets 0");
		setLayout(layout);
		
		//set the lbIconFile
		lbIcon=new JLabel(startIcon);
		//the icon start
		tfFile=new JTextField();
		tfFile.setText(startIconFile);
		tfFile.setEditable(false);
		SwingUtil.setForcedSize(tfFile, new Dimension(300, 20));
		tfFile.setCaretPosition(0);
		
		// the icon chooser
		btChooser=new IconButton("small/open",getShowIconChooserAction());

		// button to clear the icon and set it to default
		btClear = new IconButton("small/clear",getClearIconAction());
		btClear.setSize20x20();

		if (!isIcon) add(lbIcon,"span, center, wrap");
		//else add(lbIcon);
		add(tfFile);
		add(btChooser);
		add(btClear);

	}

	@Override
	public void refresh() {
		removeAll();
		initGUI();
	}

	private AbstractAction getShowIconChooserAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				Component parent = getThis().getParent();
				if (parent == null) {
					parent = getThis();
				}
				ImageChooserDlg chooser=new ImageChooserDlg(false);
				if (!currentFile.isEmpty())
					chooser.setSelectedFile(new File(currentFile));
				else chooser.setCurrentDirectory(new File(path+"."));
				chooser.setUpper(path);
				int i = chooser.showOpenDialog(parent);
				if (i!=0) {
					return;
				}
				File file = chooser.getSelectedFile();
				tfFile.setText(file.getAbsolutePath());
				currentFile=file.getAbsolutePath();
				lbIcon.setIcon(IconUtil.getIconExternal(file.getAbsolutePath(),new Dimension(32,32)));
			}
		};
	}

	private AbstractAction getClearIconAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				tfFile.setText("");
				currentFile="";
				lbIcon.setIcon(startIcon);
			}
		};
	}

	/*
	 * Returns its self for use within anonymous objects that require references
	 * to this object without being able to use <code>this</code> keyword.
	 */
	protected ImageChooserPanel getThis() {
		return this;
	}

	public String getIconFile() {
		if (tfFile.getText().isEmpty()) {
			return null;
		}
		return tfFile.getText();
	}

	public void setIconFile(String str) {
		tfFile.setText(str);
		tfFile.setCaretPosition(0);
		currentFile=str;
		lbIcon.setIcon(new ImageIcon(str));
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		Object source = evt.getSource();
		if (source instanceof JLabel) {
			JComponent comp = (JComponent) source;
			JComponent parent1 = (JComponent) comp.getParent();
			JComponent parent2 = (JComponent) parent1.getParent();
			JPopupMenu menu = (JPopupMenu) parent2;
			menu.setVisible(false);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

}
