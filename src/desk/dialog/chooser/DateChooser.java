/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.dialog.chooser;

import db.entity.SbDate;
import db.util.BookUtil;
import lib.jdatechooser.calendar.JDateChooser;
import lib.jdatechooser.calendar.JTextFieldDateEditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import lib.miginfocom.swing.MigLayout;

import desk.app.App;
import desk.panel.AbstractPanel;
import desk.tools.IconButton;
import desk.app.MainFrame;
import db.I18N;
import java.util.Calendar;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class DateChooser extends AbstractPanel {

	private JDateChooser dateChooser;
	private JSpinner timeSpinner;
	private boolean showDateTime;

	public DateChooser(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	public DateChooser(MainFrame mainFrame, boolean showDateTime) {
		super(mainFrame);
		this.showDateTime = showDateTime;
		initAll();
	}

	public boolean hasError() {
		JTextFieldDateEditor tf = (JTextFieldDateEditor) dateChooser.getComponent(1);
		return tf.getForeground() == Color.red;
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {

	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		//App.trace("DateChooser.initUi()");
		setLayout(new MigLayout("flowx, ins 0"));

		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString(App.getInstance().format_Date);
		dateChooser.setMinimumSize(new Dimension(120, 20));
		//dateChooser.setDate(Calendar.getInstance().getTime());

		JLabel lbTime = new JLabel(I18N.getColonMsg("z.time"));

		IconButton btClearTime = new IconButton("small/clear", getClearTimeAction());
		btClearTime.setSize20x20();

		timeSpinner = new JSpinner(new SpinnerDateModel());
		JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, I18N.TIME_FORMAT);
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setValue(Calendar.getInstance().getTime());
		timeSpinner.setPreferredSize(new Dimension(80, 30));

		IconButton btFirstDate = new IconButton("small/nav-first", getFirstDateAction());
		btFirstDate.setSize20x20();

		IconButton btPrevDay = new IconButton("small/nav-previous", getPrevDayAction());
		btPrevDay.setSize20x20();

		IconButton btNextDay = new IconButton("small/nav-next", getNextDayAction());
		btNextDay.setSize20x20();

		IconButton btLastDate = new IconButton("small/nav-last", getLastDateAction());
		btLastDate.setSize20x20();

		// layout
		add(dateChooser, "gapafter 10");
		add(btFirstDate);
		add(btPrevDay);
		add(btNextDay);
		add(btLastDate);
		if (showDateTime) {
			add(lbTime, "aligny center,span,split 3");
			add(timeSpinner);
			add(btClearTime);
		}
	}

	public void setDate(Date date) {
		dateChooser.setDate(date);
		if (date != null) {
			timeSpinner.setValue(date);
		}
	}

	private AbstractAction getFirstDateAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SbDate date = BookUtil.findFirstDate(book);
				dateChooser.setDate(date.toDate());
			}
		};
	}

	private AbstractAction getLastDateAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date date = BookUtil.findLastDate(book).toDate();
				dateChooser.setDate(date);
			}
		};
	}

	private AbstractAction getNextDayAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date date;
				if (dateChooser.getDate() == null) {
					date = BookUtil.findLastDate(book).toDate();
				} else {
					date = SbDate.addDateDays(dateChooser.getDate(), 1);
				}
				dateChooser.setDate(date);
			}
		};
	}

	private AbstractAction getPrevDayAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Date date;
				if (dateChooser.getDate() == null) {
					date = BookUtil.findFirstDate(book).toDate();
				} else {
					date = SbDate.addDateDays(dateChooser.getDate(), -1);
				}
				dateChooser.setDate(date);
			}
		};
	}

	private AbstractAction getClearTimeAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timeSpinner.setValue(SbDate.getZeroTimeDate());
			}
		};
	}

	public SbDate getDate() {
		if (dateChooser.getDate()==null) return(null);
		return(new SbDate(dateChooser.getDate()));
	}
}
