/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.chooser;

import db.DB;
import desk.dialog.AbstractDialog;
import desk.app.MainFrame;
import db.I18N;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.event.CaretEvent;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class FileChooserDlg extends AbstractDialog {

	public static String show(MainFrame m, String path, String ext,
			boolean mustexists, boolean readonly, boolean askoverwrite, boolean askTitle) {
		FileChooserDlg dlg = new FileChooserDlg(m, path, ext, askTitle);
		if (mustexists) {
			dlg.setExists();
		}
		if (readonly) {
			dlg.setReadonly();
		}
		if (askoverwrite) {
			dlg.setAskOverWrite();
		}
		dlg.setVisible(true);
		if (dlg.isCanceled()) {
			return (null);
		}
		return (dlg.getFile().getAbsolutePath());
	}

	protected File file;
	private String defaultExt = DB.DB_FILE_EXT;
	private boolean forceExt = false;
	private boolean askForOverwrite;
	private boolean readonly;
	private boolean createFileName = true;

	private JTextField tfName = new JTextField();
	private JTextField tfDir = new JTextField();
	private JButton btChooseDir;
	private JLabel lbWarning;
	private boolean mustexists;
	private final boolean askTitle;
	private JTextField tfTitle;

	public FileChooserDlg(MainFrame m, String path, String ext, boolean askTitle) {
		super(m);
		//App.trace("FileChooserDlg("mainFrame, path=" + path + ", ext=" + ext + ")");
		this.askTitle = askTitle;
		initAll();
		setPath(path);
		this.setExt(ext);
	}

	public FileChooserDlg(String path, String ext) {
		super((MainFrame)null);
		//App.trace("FileChooserDlg("path="+path + ", ext=" + ext + ")");
		this.askTitle = false;
		initAll();
		setPath(path);
		this.setExt(ext);
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("wrap 2", "[][grow]"));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		if (askTitle) {
			JLabel lb1 = new JLabel(I18N.getColonMsg("title"));
			tfTitle = new JTextField();
			tfTitle.setColumns(32);
			tfTitle.addCaretListener((CaretEvent e) -> {
				if (createFileName) {
					tfName.setText(titleToFileName(tfTitle.getText()));
				}
			});
			add(lb1);
			add(tfTitle);
		}
		JLabel lb2 = new JLabel(I18N.getColonMsg("file"));
		tfName = new JTextField();
		tfName.setColumns(32);
		tfName.addCaretListener((CaretEvent e) -> {
			if (tfName.hasFocus()) {
				createFileName = false;
			}
		});
		JLabel lb3 = new JLabel(I18N.getColonMsg("folder"));
		tfDir = new JTextField();
		tfDir.setColumns(32);
		tfDir.setEditable(false);
		tfDir.addCaretListener((CaretEvent e) -> {
			if (tfDir.hasFocus()) {
				createFileName = false;
			}
		});
		btChooseDir = new JButton();
		btChooseDir.setMargin(new Insets(0, 0, 0, 0));
		btChooseDir.setIcon(IconUtil.getIcon("small/file-open"));
		btChooseDir.setToolTipText(I18N.getMsg("file.choose_folder"));
		btChooseDir.addActionListener((ActionEvent evt) -> {
			this.createFileName = false;
			btChooseDir();
		});
		lbWarning = new JLabel();
		lbWarning.setForeground(Color.red);

		setTitle(I18N.getMsg("app.new"));
		setModal(true);
		add(lb2);
		add(tfName);
		add(lb3);
		add(tfDir, "span, split 2");
		add(btChooseDir);
		add(buttonCancel(), "span, split 2,right");
		add(buttonOk(), "right");
		this.pack();
		setLocationRelativeTo(mainFrame);
	}

	private String titleToFileName(String t) {
		String r=t.replaceAll("\\W", "_");
		while(r.contains("__")) {
			r=r.replace("__", "_");
		}
		return (r);
	}

	private void btChooseDir() {
		final JFileChooser fc = new JFileChooser(tfDir.getText());
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int ret = fc.showOpenDialog(this);
		if (ret != JFileChooser.APPROVE_OPTION) {
			return;
		}
		File dir = fc.getSelectedFile();
		tfDir.setText(dir.getAbsolutePath());
		lbWarning.setText(" ");
	}

	@Override
	public AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (applySettings()) {
					canceled = false;
					dispose();
				}
			}
		};
	}

	public boolean applySettings() {
		lbWarning.setText("");
		File dir = new File(tfDir.getText());
		if (!dir.isDirectory() || !dir.canWrite() || !dir.canExecute()) {
			lbWarning.setText(I18N.getMsg("file.not_writable"));
		}
		String name = tfName.getText();
		if (!name.endsWith(defaultExt) && forceExt) {
			name += defaultExt;
		}
		file = new File(tfDir.getText() + File.separator + name);
		if (mustexists && !file.exists()) {
			lbWarning.setText(I18N.getMsg("file.not_exists"));
		}
		if ((file.exists()) && (readonly)) {
			return (true);
		}
		if ((file.exists()) && (askForOverwrite)) {
			int ret = JOptionPane.showConfirmDialog(this,
					I18N.getMsg("file.askoverwrite_msg", file.getName()),
					I18N.getMsg("file.askoverwrite"),
					JOptionPane.YES_NO_OPTION);
			if (ret == JOptionPane.NO_OPTION) {
				lbWarning.setText(I18N.getMsg("file.exists"));
			}
		} else if (file.exists()) {
			lbWarning.setText(I18N.getMsg("file.exists"));
		}
		if (askTitle) {
			if (tfTitle.getText().isEmpty()) {
				lbWarning.setText(I18N.getMsg("error.title"));
			}
		}
		if (!lbWarning.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this,
					lbWarning.getText(),
					I18N.getMsg("file"),
					JOptionPane.ERROR_MESSAGE);
			return (false);
		}
		return (true);
	}

	public String getBookTitle() {
		return tfTitle.getText();
	}

	public File getFile() {
		return file;
	}

	public void setAskOverWrite() {
		this.askForOverwrite = true;
	}

	public void setExt(String ext) {
		defaultExt = ext;
	}

	public void setPath(String p) {
		tfDir.setText(p);
	}

	public void setForceExt() {
		forceExt = true;
	}

	private void setReadonly() {
		readonly = true;
	}

	private void setExists() {
		mustexists = true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
