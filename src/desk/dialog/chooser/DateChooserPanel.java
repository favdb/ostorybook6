/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.chooser;

import db.entity.Book;
import db.entity.SbDate;
import db.util.BookUtil;
import desk.app.MainFrame;
import db.I18N;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import lib.jdatechooser.calendar.JDateChooser;
import lib.jdatechooser.calendar.JTextFieldDateEditor;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class DateChooserPanel extends JPanel {

	private final MainFrame mainFrame;
	private final Book book;
	private JDateChooser dateChooser;
	private JSpinner timeSpinner;
	private JButton btPrevDay;
	private JButton btNextDay;
	private JButton btLastDate;
	private JButton btFirstDate;
	private JButton btClearTime;
	
	public DateChooserPanel(MainFrame parent) {
		mainFrame=parent;
		book=mainFrame.book;
		init();
	}
	
	private void init() {
        dateChooser = new JDateChooser();
        timeSpinner = new javax.swing.JSpinner();
		JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, I18N.TIME_FORMAT);
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setValue(SbDate.getZeroTimeDate());
		timeSpinner.setPreferredSize(new Dimension(80, 30));
		btPrevDay = new javax.swing.JButton();
        btPrevDay.setIcon(IconUtil.getIcon("small/nav-previous"));
        btPrevDay.addActionListener((java.awt.event.ActionEvent evt) -> {
			Date date;
			if (dateChooser.getDate() == null) {
				date = BookUtil.findFirstDate(book).toDate();
			} else {
				date=SbDate.addDateDays(dateChooser.getDate(), -1);
			}
			dateChooser.setDate(date);
		});

		btNextDay = new javax.swing.JButton();
        btNextDay.setIcon(IconUtil.getIcon("small/nav-next"));
        btNextDay.addActionListener((java.awt.event.ActionEvent evt) -> {
			Date date;
			if (dateChooser.getDate() == null) {
				date = BookUtil.findLastDate(book).toDate();
			} else {
				date=SbDate.addDateDays(dateChooser.getDate(), 1);
			}
			dateChooser.setDate(date);
		});

		btLastDate = new javax.swing.JButton();
        btLastDate.setIcon(IconUtil.getIcon("small/nav-last"));
        btLastDate.addActionListener((java.awt.event.ActionEvent evt) -> {
			Date date = BookUtil.findLastDate(book).toDate();
			dateChooser.setDate(date);
		});

		btFirstDate = new javax.swing.JButton();
        btFirstDate.setIcon(IconUtil.getIcon("small/nav-first"));
        btFirstDate.addActionListener((java.awt.event.ActionEvent evt) -> {
			Date date = BookUtil.findFirstDate(book).toDate();
			dateChooser.setDate(date);
		});

		btClearTime = new javax.swing.JButton();
        btClearTime.setIcon(IconUtil.getIcon("small/clear"));
        btClearTime.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btClearTime.addActionListener((java.awt.event.ActionEvent evt) -> {
			timeSpinner.setValue(SbDate.getZeroTimeDate());
		});
		
		//layout
		setLayout(new MigLayout("insets 0"));
		add(dateChooser);
		add(btFirstDate);
		add(btPrevDay);
		add(btNextDay);
		add(btLastDate);
		add(timeSpinner);
		add(btClearTime);
	}
	
	public void setDate(Date date) {
		dateChooser.setDate(date);
		if (date != null) {
			if (timeSpinner.isVisible()) timeSpinner.setValue(date);
		}
	}
	
	public Date getDate() {
		return(dateChooser.getDate());
	}

	public void hideTime(){
		timeSpinner.setVisible(false);
		btClearTime.setVisible(false);
	}
	
	public void hideButtons() {
		btFirstDate.setVisible(false);
		btLastDate.setVisible(false);
		btPrevDay.setVisible(false);
		btNextDay.setVisible(false);

	}
	
	public void showOnlyDate() {
		hideTime();
		hideButtons();
	}

	public boolean hasError() {
		JTextFieldDateEditor tf = (JTextFieldDateEditor) dateChooser.getComponent(1);
		return tf.getForeground() == Color.red;
	}

	@Override
	public void setEnabled(boolean enabled) {
		dateChooser.setEnabled(enabled);
		timeSpinner.setEnabled(enabled);
		btClearTime.setEnabled(enabled);
		btFirstDate.setEnabled(enabled);
		btLastDate.setEnabled(enabled);
		btPrevDay.setEnabled(enabled);
		btNextDay.setEnabled(enabled);
	}
}
