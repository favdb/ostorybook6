/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.i18n;

import java.awt.event.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.SortOrder;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXTable;
import lib.miginfocom.swing.MigLayout;

import desk.app.App;
import desk.app.Language;
import desk.app.MainFrame;
import desk.dialog.AbstractDialog;
import desk.tools.IOTools;
import db.I18N;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author favdb
 */
public class I18NDlg extends AbstractDialog {

	private JComboBox<String> cbLanguage;
	private JButton btSave;
	private JXTable table;
	private JTextArea text;
	private JTextArea comment;
	private JTextPane infoPane1;
	private ArrayList<String> languages;
	private DefaultTableModel tableModel;
	private boolean bModified=false;
	private String curLanguage;

	public I18NDlg(MainFrame m) {
		super(m);
		initAll();
	}
	
	public static void show(MainFrame m) {
		I18NDlg dlg=new I18NDlg(m);
		dlg.setVisible(true);
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("wrap 9"));
		setTitle("UI Translation");

		JToolBar bar = new JToolBar();
		bar.setLayout(new MigLayout());
		bar.setRollover(false);
		bar.setFloatable(false);

		bar.add(new JLabel(I18N.getColonMsg("language")));
		cbLanguage = new JComboBox<>();
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
		model.addElement(I18N.getMsg("language.select"));
		languages = new ArrayList<>();
		for (Language.LANGUAGE lang : Language.LANGUAGE.values()) {
			model.addElement(lang.name().substring(0, 2) + " " + lang.getI18N());
			languages.add(lang.name().substring(0, 2));
		}
		cbLanguage.setModel(model);
		cbLanguage.addItemListener((ItemEvent evt) -> {
			if (evt.getStateChange() == ItemEvent.SELECTED) {
				ChangeLanguage();
			}
		});
		bar.add(cbLanguage);

		JButton btNew = getButton("","small/file-new","file.new");
		btNew.addActionListener((ActionEvent evt) -> {
			btNewAction();
		});
		bar.add(btNew);

		JButton btOpen = getButton("","small/file-open","file.open");
		btOpen.addActionListener((ActionEvent evt) -> {
			btOpenAction();
		});
		bar.add(btOpen);

		JButton btRefresh = getButton("","small/refresh","z.refresh");
		btRefresh.addActionListener((ActionEvent evt) -> {
			bModified = false;
			ChangeLanguage();
		});
		bar.add(btRefresh);

		btSave = getButton("","small/file-save","file.save");
		btSave.addActionListener((ActionEvent evt) -> {
			save();
		});
		bar.add(btSave);

		JButton btExit = getButton("","small/file-exit","file.exit");
		btExit.addActionListener((ActionEvent evt) -> {
			onExit();
		});
		bar.add(btExit);
		add(bar,"growx,wrap");

		add(new JLabel(I18N.getColonMsg("z.comment")),"wrap");
		comment = new javax.swing.JTextArea();
		comment.setEditable(false);
		comment.setColumns(25);
		comment.setRows(5);
		comment.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				bModified=true;
			}
		});
		JScrollPane scrollComment = new JScrollPane();
		scrollComment.setViewportView(comment);
		add(scrollComment,"sg, span, split 2, grow");

		infoPane1 = new JTextPane();
		infoPane1.setEditable(false);
		infoPane1.setText("{0}, {1}, ... are for insertion of variables data, they are mandatory.\n\n\\n stands for line feed.");
		JScrollPane scrollInfo = new JScrollPane();
		scrollInfo.setViewportView(infoPane1);
		scrollInfo.setFocusable(false);
		add(scrollInfo,"grow,wrap");

		table = new JXTable();
		initTable();
		JScrollPane scrollTable = new JScrollPane();
		scrollTable.setViewportView(table);
		add(scrollTable,"span,growx");
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				SelectRow();
			}
		});

		add(new JLabel(I18N.getColonMsg("z.text")),"span");
		text = new JTextArea();
		text.setColumns(20);
		text.setRows(5);
		text.setEditable(false);
		text.addCaretListener((javax.swing.event.CaretEvent evt) -> {
			textCaretUpdate(evt);
		});
		JScrollPane scrollInfoText = new JScrollPane();
		scrollInfoText.setViewportView(text);
		add(scrollInfoText,"span, growx");

		pack();
		setLocationRelativeTo(mainFrame);
		setModal(true);
		this.addWindowListener(new WindowAdapter() {
			private boolean bModified;
			@Override
			public void windowClosing(WindowEvent e) {
				if (bModified) {
					int confirmed = JOptionPane.showConfirmDialog(null,
						I18N.getMsg("language.confirm.msg"),
						I18N.getMsg("language.confirm"),
						JOptionPane.YES_NO_OPTION);
					if (confirmed == JOptionPane.YES_OPTION) {
						dispose();
					}
				} else {
					dispose();
				}
			}
		});
		
	}

	private void btNewAction() {
		I18NnewDlg dlg = new I18NnewDlg(this, mainFrame, languages);
		dlg.setVisible(true);
		String str=dlg.getLanguage();
		if (str.isEmpty()) return;
		cbLanguage.addItem(str);
		cbLanguage.setSelectedItem(str);
		ChangeLanguage();
	}
	
	private void btOpenAction() {
        File f=IOTools.selectFile(this,
			mainFrame.getPref().getLastDir(),
			"properties",
			"Properties file (*.properties)");
		if (f==null) return;
		curLanguage="file:"+f.getAbsolutePath();
		cbLanguage.addItem(curLanguage);
		cbLanguage.setSelectedItem(curLanguage);
		ChangeLanguage();
	}
	
	private void textCaretUpdate(javax.swing.event.CaretEvent evt) {
		String str = text.getText().replace("\n", "\\n");
		int row = table.getSelectedRow();
		if (row!=-1) table.setValueAt(str, row, 2);
	}
	
	private void initTable() {
		List<String> cols = new ArrayList<>();
		cols.add(I18N.getMsg("z.key"));
		cols.add(I18N.getMsg("z.default"));
		cols.add(" ");
		tableModel = new DefaultTableModel(cols.toArray(), 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				if (column == 2) {
					return (true);
				}
				return (false);
			}
		};
		// read all keys
		InputStream stream = App.getInstance().getClass().getResourceAsStream("/msg/messages.properties");
		BufferedReader txt = new BufferedReader(new InputStreamReader(stream));
		String line;
		try {
			line = txt.readLine();
			while (line != null) {
				if (!line.isEmpty() && !(line.startsWith("#") || line.startsWith(" ")) && line.contains("=")) {
					String[] s = line.split("=");
					s[0]=s[0].trim();
					s[1]=s[1].trim();
					if (!s[0].startsWith("language.")) tableModel.addRow(s);
				}
				line = txt.readLine();
			}
			txt.close();
		} catch (IOException ex) {
			App.error("I18NDlg error", ex);
		}
		table.setModel(tableModel);
		table.setSortOrder(0, SortOrder.ASCENDING);
		CellEditorListener ChangeNotification = new CellEditorListener() {
			@Override
			public void editingCanceled(ChangeEvent e) {
			}

			@Override
			public void editingStopped(ChangeEvent e) {
				bModified = true;
				int row = table.getSelectedRow();
				text.setText((String) table.getValueAt(row, 2));
			}
		};
		TableCellEditor z = table.getCellEditor();
		if (z==null) {
			//System.err.println("I18NDlg TableCellEditor is null");
			return;
		}
		z.addCellEditorListener(ChangeNotification);
	}

	private void ChangeLanguage() {
		if (cbLanguage.getSelectedIndex()==0) {
			return;
		}
		if (tableModel.getRowCount() > 0) {
			for (int i = table.getRowCount() - 1; i > 0; i--) {
				tableModel.setValueAt("", i, 2);
			}
		}
		curLanguage = (String) cbLanguage.getSelectedItem();
		if (curLanguage.contains("=")) return;
		String bundler = "msg.messages_" + curLanguage.substring(0, 2).toLowerCase();
		Object src;
		if (curLanguage.startsWith("file:")) {
			bundler=curLanguage.replace("file:", "");
			bundler=bundler.replace(".properties","");
			String path=bundler.substring(0, bundler.lastIndexOf("/"));
			String name=bundler.substring(bundler.lastIndexOf("/")+1);
			URL resourceURL;
			File fl = new File(path);
			try {
				resourceURL = fl.toURI().toURL();
			} catch (MalformedURLException ex) {
				App.error("ChangeLanguage MalformedURL", ex);
				return;
			}
			URLClassLoader urlLoader = new URLClassLoader(new java.net.URL[]{resourceURL});
			src= ResourceBundle.getBundle(name,java.util.Locale.getDefault(),urlLoader);
		} else {
			try {
				src = ResourceBundle.getBundle(bundler);
			} catch (java.util.MissingResourceException ex) {
				StringBuilder b=new StringBuilder();
				b.append("Missing ressource file for ").append(curLanguage).append("\n");
				b.append("Do you want to create it?\n\n");
				int n = JOptionPane.showConfirmDialog(null,
						b.toString(),
						"Language",
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (n!=JOptionPane.OK_OPTION) {
					cbLanguage.setSelectedIndex(0);
					return;
				}
				setLanguageHeader();
				bModified = false;
				text.setEditable(true);
				return;
			}
		}
		setLanguageHeader();
		bModified = false;
		for (int i = 0; i < tableModel.getRowCount(); i++) {
			String s1 = (String) tableModel.getValueAt(i, 0);
			String s2 = "";
			try {
				Object x=((ResourceBundle)src).getObject(s1);
				s2 = ((String)x).replace("\n","\\n");
			} catch (Exception e) {
			}
			if (s2.equals("")) continue;
			tableModel.setValueAt(s2.trim(), i, 2);
		}
		readComment(curLanguage.substring(0, 2).toLowerCase());
		SelectRow();
		text.setEditable(true);
	}

	private void SelectRow() {
		int row = table.getSelectedRow();
		if (row == -1) {
			return;
		}
		int col = 2;
		if (table.getValueAt(row, col)!=null) {
			String str = ((String) table.getValueAt(row, col)).replace("\\n","\n");
			text.setText(str);
			text.setCaretPosition(0);
		}
	}

	private void save() {
		//select a valid file name, if the file exists override
		File dir = IOTools.selectDirectory(this, mainFrame.getPref().getLastDir());
		if (dir == null) {
			return;
		}
		String fileName;
		if (curLanguage.startsWith("file:")) {
			fileName=curLanguage.replace("file:", "");
		} else {
			fileName = dir.getAbsolutePath() + "/messages_" + curLanguage.substring(0, 2) + ".properties";
			File file = new File(fileName);
			if (file.exists()) {
				if (JOptionPane.showConfirmDialog(null,
					I18N.getMsg("language.overwrite"),
					I18N.getMsg("language.confirm"),
					JOptionPane.YES_NO_OPTION) == JOptionPane.CANCEL_OPTION) {
					return;
				}
			}
		}
		try {
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName),"ISO-8859-1"));
			writeComment(out);
			int nb=0;
			for (int i = 0; i < table.getRowCount(); i++) {
				String val = (String) table.getValueAt(i, 2);
				if (val != null && !val.isEmpty()) {
					nb++;
					String bytes = table.getValueAt(i, 0) + "=" + val.trim();
					out.write(bytes);
					out.write("\n");
				}
			}
			out.flush();
			out.close();
			
			JOptionPane.showMessageDialog(null,
				I18N.getMsg("language.saveok", fileName),
				I18N.getMsg("language"),
				JOptionPane.INFORMATION_MESSAGE);
			System.out.println("Writing "+nb+" lines to "+fileName);
			bModified = false;
		} catch (FileNotFoundException ex) {
			System.err.println(ex.getLocalizedMessage());
		} catch (IOException ex) {
			App.error("save", ex);
		}
	}

	private void readComment(String lang) {
		String fileName="/msg/messages_" + lang + ".properties";
		InputStream stream;
		if (curLanguage.startsWith("file:")) {
			try {
				stream=new FileInputStream(curLanguage.replace("file:",""));
			} catch (FileNotFoundException ex) {
				App.error("readComment", ex);
				return;
			}
		}
		else stream = App.getInstance().getClass().getResourceAsStream(fileName);
		BufferedReader txt = new BufferedReader(new InputStreamReader(stream));
		String line;
		String str = "";
		try {
			line = txt.readLine();
			while (line != null) {
				if (!line.isEmpty() && line.startsWith("#")) {
					str += line.replaceFirst("#", "").trim() + "\n";
				} else break;
				line = txt.readLine();
			}
			txt.close();
			comment.setText(str);
			comment.setEditable(true);
			comment.setCaretPosition(0);
		} catch (IOException ex) {
			App.error("DlgT10N error", ex);
		}
	}

	private void writeComment(Writer out) {
		String str=comment.getText();
		if (str!=null) {
			String s[]=str.split("\n");
			for (String x:s) {
				String bytes = "#"+x+"\n";
				//byte[] buffer = bytes.getBytes();
				try {
					out.write(bytes);
				} catch (IOException ex) {
					App.error("writeComment", ex);
				}
			}
		}
	}

	private int onExit() {
		if (bModified) {
			if (JOptionPane.showConfirmDialog(null,
				I18N.getMsg("language.confirm.msg"),
				I18N.getMsg("language.confirm"),
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				dispose();
				return(JDialog.DISPOSE_ON_CLOSE);
			} else {
				//dispose();
				return(JDialog.DO_NOTHING_ON_CLOSE);
			}
		} else dispose();
		return(JDialog.DO_NOTHING_ON_CLOSE);
	}

	private void setLanguageHeader() {
		table.getTableHeader().getColumnModel().getColumn(2).setHeaderValue(curLanguage);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
