/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import desk.app.MainFrame;
import desk.dialog.AbstractDialog;
import desk.dialog.edit.EditorHtml;
import db.I18N;
import java.awt.event.ActionEvent;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class NotesDlg extends AbstractDialog {
	private EditorHtml panel;

	public NotesDlg(MainFrame caller, EditorHtml panel, String t) {
		super(caller);
		this.panel=panel;
		init(panel, t);
	}

	@Override
	public void init() {
	}

	public void init(EditorHtml panel, String t) {
		this.setModal(true);
		this.setTitle(I18N.getMsg(t));
		this.setIconImage(IconUtil.getIconImage("icon"));
		this.setLayout(new MigLayout("wrap,fill,ins 2"));
		this.add(panel, "wrap");
		this.addOkCancel();
		this.pack();
		this.setLocationRelativeTo(this.getParent().getParent());
		this.setVisible(true);
	}

	@Override
	public void initUi() {
		setModal(true);
		setTitle(I18N.getMsg("z.edit"));
		setIconImage(IconUtil.getIconImage("icon"));
		setLayout(new MigLayout("wrap,fill,ins 2"));
		add(panel, "wrap");
		addOkCancel();
		pack();
		setLocationRelativeTo(this.getParent().getParent());
		setVisible(true);
		//SwingUtil.getDlgSize(this, new Idea());
	}

	@Override
	public boolean isCanceled() {
		return (canceled);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
