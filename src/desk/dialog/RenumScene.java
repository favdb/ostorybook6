/*
 * Copyright (C) 2018 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import desk.app.MainFrame;
import db.I18N;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 *
 * @author FaVdB
 */
public class RenumScene extends AbstractDialog {

	private JRadioButton rbAuto;
	private JRadioButton rbInc;
	private JTextField tfInc;
	private JLabel lbInc;
	
	public RenumScene(MainFrame m) {
		super(m);
		initAll();
	}

	public static int showDialog(MainFrame m) {
		RenumScene dlg=new RenumScene(m);
		dlg.setVisible(true);
		if (dlg.canceled) return(0);
		return(dlg.getChoice());
	}

	@Override
	public void init() {
	}
	
	@Override
	public void initUi() {
		JLabel label1 = new JLabel(I18N.getMsg("scenes.renumber"));
		ButtonGroup group = new ButtonGroup();
		rbAuto = new JRadioButton(I18N.getMsg("scenes.renumber.auto"));
		rbInc = new JRadioButton(I18N.getMsg("scenes.renumber.inc"));
		group.add(rbAuto);
		group.add(rbInc);
		lbInc = new JLabel(I18N.getMsg("scenes.renumber.inc_by"));
		tfInc = new JTextField();
		tfInc.setColumns(3);
		tfInc.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (c >= '0' && c <= '9') {
				} else {
					evt.consume();
				}
			}
		});
		tfInc.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				int len = tfInc.getText().length();
				if (len >= 2) {
					evt.consume();
				}
			}
		});
		rbAuto.addActionListener((ActionEvent evt) -> {
			tfInc.setEnabled(!rbAuto.isSelected());
			lbInc.setEnabled(!rbAuto.isSelected());
		});
		rbInc.addActionListener((ActionEvent evt) -> {
			tfInc.setEnabled(rbInc.isSelected());
			lbInc.setEnabled(rbInc.isSelected());
		});
		tfInc.setEnabled(false);
		lbInc.setEnabled(false);

		setLayout(new MigLayout());
		setTitle(I18N.getMsg("scenes.renumber"));
		add(label1, "wrap");
		add(rbAuto, "wrap");
		add(rbInc,"span, split 3");add(lbInc);add(tfInc,"wrap");
		add(this.buttonCancel(), "span,split,right");
		add(this.buttonOk(), "right");
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}

	private int getChoice() {
		if (canceled) {
			return(0);
		}
		if (rbAuto.isSelected()) {
			return(-1);
		}
		if (tfInc.getText().isEmpty()) {
			return(0);
		}
		return(Integer.parseInt(tfInc.getText()));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
	}
	
	public int getInc() {
		String str=tfInc.getText();
		if (str.isEmpty() || !StringUtil.isNumeric(str)) return(0);
		return(Integer.parseInt(str));
	}
	
	public boolean getAuto() {
		return(rbAuto.isSelected());
	}

}
