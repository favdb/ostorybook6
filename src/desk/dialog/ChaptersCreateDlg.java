/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import lib.jdatechooser.calendar.JDateChooser;
import lib.miginfocom.swing.MigLayout;
import db.I18N;
import db.entity.Book;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.SbDate;
import desk.app.App;
import desk.app.MainFrame;
import static desk.tools.Ui.*;
import desk.tools.combobox.ComboUtil;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class ChaptersCreateDlg extends AbstractDialog {

	public static boolean show(MainFrame m) {
		ChaptersCreateDlg dlg=new ChaptersCreateDlg(m);
		dlg.setVisible(true);
		return(dlg.canceled);
	}

	private JDateChooser dateChooser;
	private JTextField tfQuantity;
	private JComboBox<Object> partCombo;
	private JTextField tfSize;
	private JTextField tfFormat;
	private JCheckBox rbRoman;

	public ChaptersCreateDlg(MainFrame parent) {
		super(parent);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		JLabel lb1 = new JLabel(I18N.getMsg("chapters.generate.text"));
		tfQuantity = new JTextField();
		tfQuantity.setColumns(2);

		JLabel lb2 = new JLabel(I18N.getMsg("part"));
		partCombo = new JComboBox<>();
		ComboUtil.fillEntity(book, partCombo, Book.TYPE.PART, null, null, NEW, EMPTY, !ALL);
		
		JLabel lbn=new JLabel(I18N.getMsg("chapters.generate.format"));
		tfFormat = new JTextField();
		tfFormat.setText(I18N.getMsg("chapter")+" %d");
		
		rbRoman=new JCheckBox(I18N.getMsg("chapters.generate.roman"));

		JLabel lb3 = new JLabel(I18N.getMsg("objective_size"));
		tfSize = new JTextField();
		tfSize.setColumns(8);

		JLabel lb4 = new JLabel(I18N.getMsg("objective_date"));
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString(App.getInstance().format_Date);

		//layout
		setLayout(new MigLayout("", "", ""));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle(I18N.getMsg("chapters.generate"));
		add(lb1, "split 2");
		add(tfQuantity, "wrap");
		add(lbn,"wrap");add(tfFormat,"growx,wrap");add(rbRoman,"right, wrap");
		add(lb2, "split 2");
		add(partCombo, "wrap");
		add(lb3, "split 2");
		add(tfSize, "wrap");
		add(lb4, "split 2");
		add(dateChooser, "wrap");

		add(buttonCancel(), "split 2, right");
		add(buttonOk(), "right");
		pack();
		setLocationRelativeTo(mainFrame);
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int quant = 0;
				try {
					quant = Integer.parseInt(tfQuantity.getText());
				} catch (NumberFormatException evt) {
					// ignore
				}
				if (quant < 1 || quant > 20) {
					showError(I18N.getMsg("chapters.generate.number.error"));
					return;
				}
				if (!tfFormat.getText().contains("%d")) {
					showError(I18N.getMsg("chapters.generate.format.error"));
					return;
				}
				int size = 0;
				try {
					size = Integer.parseInt(tfSize.getText());
				} catch (NumberFormatException evt) {
					// ignore
				}
				SbDate xdate = getSbDate();
				Part part = (Part) partCombo.getSelectedItem();
				for (int i = 0; i < quant; ++i) {
					Chapter ch = new Chapter();
					ch.part=part;
					if (size != 0) {
						ch.size=size;
					}
					if (xdate != null) {
						ch.objective=xdate;
					}
					if (rbRoman.isSelected()) {
						String x=StringUtil.intToRoman((int)ch.number);
						String f=tfFormat.getText().replace("%d", "%s");
						ch.setName(String.format(f, x));
					}
					mainFrame.getBookController().newEntity(ch);
				}
				canceled = false;
				dispose();
			}

		};
	}

	private void showError(String msg) {
		JOptionPane.showMessageDialog(this, msg, I18N.getMsg("z.error") , JOptionPane.ERROR_MESSAGE);
	}

	public SbDate getSbDate() {
		if (dateChooser.getDate() == null) {
			return null;
		}
		Date date = dateChooser.getDate();
		return(new SbDate(date.getTime()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
