/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Pov;
import db.util.EntityTool;
import db.XmlDB;
import desk.app.MainFrame;
import desk.dialog.edit.CheckBoxPanel;
import desk.tools.decorator.DecoratorCbPanel;
import desk.tools.decorator.DecoratorCbPanelIdea;
import desk.tools.decorator.DecoratorCbPanelItem;
import desk.tools.decorator.DecoratorCbPanelLocation;
import desk.tools.decorator.DecoratorCbPanelPerson;
import desk.tools.decorator.DecoratorCbPanelPov;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.UIDefaults;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class CopyEntityFromDlg extends AbstractDialog implements ActionListener, CaretListener {
	
	//TODO to rewrite all code

	public static boolean show(MainFrame m) {
		CopyEntityFromDlg dlg=new CopyEntityFromDlg(m);
		dlg.setVisible(true);
		return(dlg.isCopied());
	}
	
	private String fileFrom;
	private XmlDB fromXml;
	private Book fromBook;
	private AbstractEntity entity;
	private String entityType="person";
	private JTextField tfFromName;
	private JComboBox<EntityName> cbEntity;
	private CheckBoxPanel cbPanel;
	private List<AbstractEntity> entities;
	//private AbstractEntityHandler entityHandler;
	private final String[] entityAllowed = {"pov", "person", "location", "item", "idea"};
	private boolean copied=false;
	
	public CopyEntityFromDlg(MainFrame m) {
		super(m);
		initAll();
	}
	
	@Override
	public void init() {
		
	}
	
	@Override
	public void initUi() {
		super.initUi();
		setLayout(new MigLayout("wrap,fill", "", "[grow][]"));
		setTitle(I18N.getMsg("copy.title"));
		setIconImage(IconUtil.getIconImage("icon"));
		setPreferredSize(new Dimension(500, 600));

		JPanel panel = new JPanel(new MigLayout("flowy,fill"));
		panel.add(createFromPanel(), "growx");
		panel.add(createEntitiesPanel(), "growx");

		add(panel);

		add(buttonOk(), "split 2,sg,right");
		add(buttonCancel(), "sg");
		pack();
		setLocationRelativeTo(mainFrame);
	}
	
	private JPanel createFromPanel() {
		JPanel panel = new JPanel(new MigLayout("wrap 2", "", "[]10"));
		panel.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("copy.source")));
		JLabel lbFromName = new JLabel(I18N.getMsg("copy.source_name"));
		tfFromName = new JTextField();
		tfFromName.setColumns(32);
		tfFromName.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyReleased(java.awt.event.KeyEvent evt) {
				File f = new File(tfFromName.getText());
				if (f.exists() && f.isDirectory()) {
					UIDefaults defaults = javax.swing.UIManager.getDefaults();
					tfFromName.setForeground(defaults.getColor("TextField.foreground"));
				} else {
					tfFromName.setForeground(Color.red);
				}
			}
		});
		JButton bt = new JButton(IconUtil.getIcon("small/file-open"));
		bt.addActionListener((java.awt.event.ActionEvent evt) -> {
			JFileChooser chooser = new JFileChooser(tfFromName.getText());
			chooser.setFileSelectionMode(1);
			int i = chooser.showOpenDialog(null);
			if (i != 0) {
				return;
			}
			File file = chooser.getSelectedFile();
			tfFromName.setText(file.getAbsolutePath());
			tfFromName.setBackground(Color.WHITE);
			fromXml=new XmlDB(file);
			if (!fromXml.open()) {
				return;
			}
			fromBook=new Book();
			fromBook.xmlFrom(fromXml);
			tfFromName.setText(fromBook.getTitle());
			entityType="pov";
			createEntitiesPanel();
		});
		bt.setMargin(new Insets(0, 0, 0, 0));
		// layout
		panel.add(lbFromName);
		panel.add(tfFromName);
		panel.add(bt);
		return panel;
	}

	@SuppressWarnings("unchecked")
	private JPanel createEntitiesPanel() {
		JPanel panel = new JPanel(new MigLayout());
		panel.setBorder(BorderFactory.createTitledBorder(I18N.getMsg("copy.elements")));
		cbEntity = new JComboBox();
		cbEntity.setName("cbEntity");
		for (String e : entityAllowed) {
			cbEntity.addItem(new EntityName(e));
		}
		cbEntity.addActionListener(this);
		cbEntity.setSelectedItem(entityType);
		panel.add(new JLabel(I18N.getMsg("copy.elements")), "split 2");
		panel.add(cbEntity, "wrap");
		cbPanel = new CheckBoxPanel(mainFrame);
		JScrollPane scroller = new JScrollPane(cbPanel);
		SwingUtil.setUnitIncrement(scroller);
		SwingUtil.setMaxPreferredSize(scroller);
		panel.add(scroller, "grow");
		cbPanel.setAutoSelect(false);
		DecoratorCbPanel decorator = getDecorator();
		if (decorator != null) {
			decorator.setPanel(cbPanel);
		}
		cbPanel.setEntityList(getAllElements());
		cbPanel.initAll();
		return panel;
	}

	private List<AbstractEntity> getAllElements() {
		List<AbstractEntity> list=new ArrayList<>();
		switch (Book.getTYPE(entityType)) {
			case POV:
				list.addAll(book.povs);
				break;
			case PERSON:
				list.addAll(book.persons);
				break;
			case LOCATION:
				list.addAll(book.locations);
				break;
			case ITEM:
				list.addAll(book.items);
				break;
			case IDEA:
				list.addAll(book.ideas);
				break;
		}
		return (list);
	}

	private DecoratorCbPanel getDecorator() {
		switch (Book.getTYPE(entityType)) {
			case POV:
				return new DecoratorCbPanelPov();
			case PERSON:
				return new DecoratorCbPanelPerson();
			case LOCATION:
				return new DecoratorCbPanelLocation();
			case ITEM:
				return new DecoratorCbPanelItem();
			case IDEA:
				return new DecoratorCbPanelIdea();
		}
		return (null);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		String compName = ((Component) ae.getSource()).getName();
		if (compName.equals("cbEntity")) {
			String newType = ((EntityName) cbEntity.getSelectedItem()).getType();
			if (!newType.equals(entityType)) {
				entityType = newType;
				refreshType();
			}
		}
	}

	@Override
	public void caretUpdate(CaretEvent ce) {
	}

	private void refreshType() {
		cbPanel.removeAll();
		switch (Book.getTYPE(entityType)) {
			case POV:
				entity = new Pov();
				break;
			case PERSON:
				entity = new Person();
				break;
			case LOCATION:
				entity = new Location();
				break;
			case ITEM:
				entity = new Item();
				break;
			case IDEA:
				entity = new Idea();
				break;
		}
		DecoratorCbPanel decorator = getDecorator();
		if (decorator != null) {
			decorator.setPanel(cbPanel);
		}
		cbPanel.setEntity(entity);
		cbPanel.setEntityList(getAllElements());
		cbPanel.initAll();
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				entities=cbPanel.getSelectedEntities();
				entities.forEach((srce) -> {
					copyEntity(srce);
				});
				canceled = false;
				dispose();
			}

		};
	}

	private void copyEntity(AbstractEntity srce) {
		AbstractEntity dest = EntityTool.copy(book, srce);
		switch (Book.getTYPE(entityType)) {
			case POV: {
				Pov p=(Pov)dest;
				p.id=(book.nextId());
				book.povs.add(p);
				break;
			}
			case PERSON: {
				Person p=(Person)dest;
				//check category
				if (p.category==null) {
					Category nc=p.category;
					nc.id=(book.nextId());
					book.categories.add(nc);
					p.category=(nc);
				}
				//check categories
				{
					boolean toAdd=false;
					List<Category> attrsrce=p.categories;
					List<Category> sa=new ArrayList<>();
					for (Category a:attrsrce) {
						if (a==null) {
							Category na=a;
							na.id=(book.nextId());
							book.categories.add(na);
							sa.add(na);
							toAdd=true;
						} else {
							sa.add(a);
						}
					}
					if (toAdd) {
						p.categories=(sa);
					}
				}
				//check gender
				if (p.gender==null) {
					Gender nc=p.gender;
					nc.id=(book.nextId());
					book.genders.add(nc);
					p.gender=(nc);
				}
				//remove photos
				p.photos=(new ArrayList<>());
				book.persons.add(p);
				break;
			}
			case LOCATION: {
				Location p=(Location)dest;
				p.photos=(new ArrayList<>());
				//check sup
				book.locations.add((Location)dest);
				if (p.sup==null) {
					Location nc=p.sup;
					nc.id=(book.nextId());
					book.locations.add(nc);
					p.sup=(nc);
				}
				break;
			}
			case ITEM: {
				Item p=(Item)dest;
				//check category
				if (p.category==null) {
					Category nc=p.category;
					nc.id=(book.nextId());
					book.categories.add(nc);
					p.category=nc;
				}
				//check photos
				p.photos=new ArrayList<>();
				book.items.add(p);
				break;
			}
			case IDEA: {
				Idea p=(Idea)dest;
				//check category
				if (p.category==null) {
					Category nc=p.category;
					nc.id=(book.nextId());
					book.categories.add(nc);
					p.category=nc;
				}
				//check status
				book.ideas.add(p);
				break;
			}
		}
		copied=true;
	}

	private boolean isCopied() {
		return(copied);
	}

	private static class EntityName {

		String entityType;

		public EntityName(String t) {
			entityType = t;
		}

		public String getType() {
			return (entityType);
		}

		@Override
		public String toString() {
			return (I18N.getMsg(entityType));
		}
	}

	class ProjectComboRenderer extends JLabel implements ListCellRenderer<MainFrame> {

		@SuppressWarnings("OverridableMethodCallInConstructor")
		public ProjectComboRenderer() {
			super("");
			setOpaque(true);
		}

		@Override
		public Component getListCellRendererComponent(JList<? extends MainFrame> list,
			MainFrame value, int index, boolean isSelected, boolean cellHasFocus) {
			if (value != null) {
				String title = book.info.getTitle();
				if (title.isEmpty()) {
					title = value.getTitle();
				}
				setText(title);
			}
			return this;
		}
	}

	class CopyCategory {

		public CopyCategory() {
		}

		protected void copyCategoryPrepare(MainFrame destination, Category originElt, Category destElt) {
			Category sup = originElt.sup;
			if (sup != null) {
				List<Category> cats = destination.book.categories;
				boolean found = false;
				for (Category cat : cats) {
					if (cat.name.equals(sup.name)) {
						found = true;
						destElt.sup=(cat);
						break;
					}
				}
				if (!found) {
					Category destSup = copyCategory(destination, sup);
					destElt.sup=(destSup);
				}
			}
		}

		protected Category copyCategory(MainFrame destination, Category oldCategory) {
			Category newCategory=(Category)EntityTool.copy(destination.book,oldCategory);
			return ((Category) newCategory);
		}
	}
}
