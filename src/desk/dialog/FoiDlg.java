/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.dialog;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JLabel;

import lib.miginfocom.swing.MigLayout;
import db.entity.Idea;
import db.entity.Status;
import db.I18N;
import desk.app.MainFrame;
import desk.dialog.edit.EditorHtml;
import tools.IconUtil;

/**
 * @author martin
 * 
 */
@SuppressWarnings("serial")
public class FoiDlg extends AbstractDialog {

	private EditorHtml ideaEditor;

	public FoiDlg(MainFrame mainFrame) {
		super(mainFrame);
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		super.initUi();
		setLayout(new MigLayout("wrap,fill", "", ""));
		setTitle(I18N.getMsg("foi.title"));
		setIconImage(IconUtil.getIconImage("icon.small.bulb"));

		JLabel lbTitle = new JLabel(I18N.getColonMsg("foi.new"));

		ideaEditor = new EditorHtml(mainFrame, true, EditorHtml.TWOLINE);

		// layout
		add(lbTitle);
		add(ideaEditor, "grow");
		add(buttonOk(), "split 2,sg,right");
		add(buttonCancel(), "sg");
		pack();
		setLocationRelativeTo(mainFrame);
	}

	public String getText() {
		return ideaEditor.getText();
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canceled = false;
				dispose();
				Idea idea = new Idea();
				idea.status=Status.STATUS.DUMMY.ordinal();
				idea.setNotes(ideaEditor.getText());
				mainFrame.getBookController().newEntity(idea);
			}
		};
	}
	
	public static void show(MainFrame m) {
		FoiDlg dlg = new FoiDlg(m);
		dlg.setModal(true);
		dlg.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}