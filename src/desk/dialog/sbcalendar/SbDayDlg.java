/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.sbcalendar;

import db.entity.SbCalendar.DAY;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import org.jdesktop.swingx.JXTable;
import lib.miginfocom.swing.MigLayout;
import db.I18N;
import desk.dialog.AbstractDialog;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import tools.IconUtil;

/**
 *
 * @author FaVdB
 */
class SbDayDlg extends AbstractDialog {
	public List<DAY> lsDays;
	private JTable table;

	SbDayDlg(SbCalendarDlg p, List<DAY> days) {
		this.lsDays = days;
		initAll();
		setLocationRelativeTo(p);
		setModal(true);
	}

	@Override
	public void init() {
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		setLayout(new MigLayout("wrap", "", ""));
		setTitle(I18N.getMsg("calendar.day.title"));

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn(I18N.getMsg("z.name"));
		model.addColumn(I18N.getMsg("z.abbr"));
		table = new JTable(model);
		JScrollPane scroller = new JScrollPane(table);
		lsDays.forEach((ms)-> {
			model.addRow(new Object[]{ms.name, ms.abbr});
		});
		add(new JScrollPane(table), "grow");
		table.setFillsViewportHeight(true);

		JButton btNew = new JButton(I18N.getMsg("calendar.day.new"));
		btNew.setIcon(IconUtil.getIcon("small/new"));
		btNew.addActionListener((ActionEvent evt) -> {
			model.addRow(new Object[]{I18N.getMsg("calendar.day.new"), ""});
		});
		add(btNew,"split 3, al right");

		addOkCancel();
		pack();
	}

	private boolean check() {
		for (int i = 0; i < table.getRowCount(); i++) {
			String n = (String) table.getModel().getValueAt(i, 0);
			if (n == null || n.isEmpty()) {
				error("calendar.error.name");
				return (false);
			}
			n = (String) table.getModel().getValueAt(i, 1);
			if (n == null || n.isEmpty()) {
				error("calendar.error.abbr");
				return (false);
			}
		}
		return (true);
	}

	private void error(String msg) {
		JOptionPane.showMessageDialog(this,
			I18N.getMsg(msg),
			I18N.getMsg("z.error"),
			JOptionPane.YES_OPTION);
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!check()) {
					return;
				}
				canceled = false;
				for (int i=0; i<table.getRowCount(); i++) {
					lsDays.get(i).name=(String) table.getModel().getValueAt(i, 0);
					lsDays.get(i).abbr=(String) table.getModel().getValueAt(i, 1);
				}
				dispose();
			}
		};
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
