/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.sbcalendar;

import db.entity.SbCalendar.MONTH;
import desk.dialog.AbstractDialog;
import db.I18N;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author FaVdB
 */
public class SbMonthDlg extends AbstractDialog {

	public List<MONTH> lsMonths;
	private JTable table;
	private JButton btNew;

	public SbMonthDlg(SbCalendarDlg p, List<MONTH> lsMonths) {
		this.lsMonths = lsMonths;
		initAll();
		setLocationRelativeTo(p);
		setModal(true);
	}

	@Override
	public void init() {
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		setLayout(new MigLayout("wrap", "", ""));
		setTitle(I18N.getMsg("calendar.month.title"));
		Object data[][] = new Object[lsMonths.size()][3];
		for (int i = 0; i < lsMonths.size(); i++) {
			data[i][0] = lsMonths.get(i).name;
			data[i][1] = lsMonths.get(i).abbr;
			data[i][2] = lsMonths.get(i).days;
		}
		String title[] = {I18N.getMsg("z.name"), I18N.getMsg("z.abbr"), I18N.getMsg("calendar.month.nbdays")};
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn(I18N.getMsg("z.name"));
		model.addColumn(I18N.getMsg("z.abbr"));
		model.addColumn(I18N.getMsg("calendar.month.nbdays"));
		table = new JTable(model);
		lsMonths.forEach((ms)->{
			model.addRow(new Object[]{ms.name, ms.abbr, ms.days});
		});
		add(new JScrollPane(table), "grow");
		table.setFillsViewportHeight(true);

		btNew=new JButton(I18N.getMsg("calendar.month.new"));
		btNew.setIcon(IconUtil.getIcon("small/new"));
		btNew.addActionListener((ActionEvent evt) -> {
			model.addRow(new Object[]{I18N.getMsg("calendar.month.new"), "", "99"});
		});
		add(btNew,"split 3, al right");

		addOkCancel();
		pack();
	}

	private boolean check() {
		for (int i = 0; i < table.getRowCount(); i++) {
			String n = (String) table.getModel().getValueAt(i, 0);
			if (n == null || n.isEmpty()) {
				error("calendar.error.name");
				return (false);
			}
			n = (String) table.getModel().getValueAt(i, 1);
			if (n == null || n.isEmpty()) {
				error("calendar.error.abbr");
				return (false);
			}
			Object no = table.getModel().getValueAt(i, 2);
			if ((no instanceof String) || ((int) no == 0)) {
				error("calendar.error.days");
				return (false);
			}
		}
		return (true);
	}

	private void error(String msg) {
		JOptionPane.showMessageDialog(this,
			I18N.getMsg(msg),
			I18N.getMsg("z.error"),
			JOptionPane.YES_OPTION);
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!check()) {
					return;
				}
				canceled = false;
				for (int i = 0; i < table.getRowCount(); i++) {
					lsMonths.get(i).name = (String) table.getModel().getValueAt(i, 0);
					lsMonths.get(i).abbr = (String) table.getModel().getValueAt(i, 1);
					lsMonths.get(i).days = (int) table.getModel().getValueAt(i, 2);
				}
				dispose();
			}
		};
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
