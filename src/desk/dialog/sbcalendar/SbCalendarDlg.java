/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.sbcalendar;

import db.entity.SbCalendar;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.text.NumberFormatter;
import lib.miginfocom.swing.MigLayout;
import db.I18N;
import desk.dialog.AbstractDialog;
import desk.dialog.PropertiesDlg;
import tools.IconUtil;

/**
 *
 * @author FaVdB
 */
public class SbCalendarDlg extends AbstractDialog {

	public static boolean show(PropertiesDlg p, SbCalendar c) {
		SbCalendarDlg dlg=new SbCalendarDlg(p,c);
		dlg.setVisible(true);
		return(!dlg.canceled);
	}

	private JFormattedTextField tfWeekDays;
	private JFormattedTextField tfMonths;
	private JFormattedTextField tfHours;
	private JFormattedTextField tfMinutes;
	private JFormattedTextField tfSeconds;
	private JFormattedTextField tfStartDay;
	private JLabel lsDays;
	private JLabel lsMonths;
	private JButton btDays;
	private JButton btMonths;
	private SbCalendar calendar;
	
	public SbCalendarDlg(PropertiesDlg parent, SbCalendar c) {
		super(parent.getMainFrame());
		calendar=c;
		initAll();
	}

	@Override
	public void init() {
	}
	
	@Override
	public void initUi() {
		super.initUi();
		setLayout(new MigLayout("wrap","[]", ""));
		setTitle(I18N.getMsg("calendar"));
		this.setIconImage(IconUtil.getIconImage("small/chrono"));

		NumberFormatter deuxChiffres = new NumberFormatter(NumberFormat.getInstance());
		deuxChiffres.setValueClass(Integer.class);
		deuxChiffres.setMinimum(0);
		deuxChiffres.setMaximum(99);
		deuxChiffres.setAllowsInvalid(false);

		NumberFormatter troisChiffres = new NumberFormatter(NumberFormat.getInstance());
		troisChiffres.setValueClass(Integer.class);
		troisChiffres.setMinimum(0);
		troisChiffres.setMaximum(999);
		troisChiffres.setAllowsInvalid(false);

		JLabel lbWeekDays=new JLabel(I18N.getMsg("calendar.weekdays"));
		tfWeekDays = new JFormattedTextField(deuxChiffres);
		tfWeekDays.setColumns(2);
		tfWeekDays.setValue(calendar.days.size());
		lsDays=new JLabel(getListDays());
		btDays=new JButton(I18N.getMsg("calendar.day.title"));
		btDays.addActionListener((ActionEvent evt) -> {
			SbDayDlg dlg = new SbDayDlg(this, calendar.days);
			dlg.setVisible(true);
			if (!dlg.isCanceled()) {
				lsDays.setText(getListMonth());
			}
		});
		add(lbWeekDays,"split 2");add(tfWeekDays);
		add(btDays,"split 2");add(lsDays);

		JLabel lbMonths=new JLabel(I18N.getMsg("calendar.month"));
		tfMonths = new JFormattedTextField(deuxChiffres);
		tfMonths.setColumns(2);
		tfMonths.setValue(calendar.months.size());
		lsMonths=new JLabel(getListMonth());
		btMonths=new JButton(I18N.getMsg("calendar.month.title"));
		btMonths.addActionListener((ActionEvent evt) -> {
			SbMonthDlg dlg = new SbMonthDlg(this, calendar.months);
			dlg.setVisible(true);
			if (!dlg.isCanceled()) {
				lsMonths.setText(getListMonth());
			}
		});
		add(lbMonths,"split 2");add(tfMonths);
		add(btMonths,"split 2");add(lsMonths);

		JLabel lbHours=new JLabel(I18N.getMsg("calendar.hours"));
		tfHours = new JFormattedTextField(deuxChiffres);
		tfHours.setColumns(2);
		tfHours.setValue(calendar.hours);
		add(lbHours,"split 2");add(tfHours);

		JLabel lbMinutes=new JLabel(I18N.getMsg("calendar.minutes"));
		tfMinutes = new JFormattedTextField(deuxChiffres);
		tfMinutes.setColumns(2);
		tfMinutes.setValue(calendar.minutes);
		add(lbMinutes,"split 2");add(tfMinutes);

		JLabel lbSeconds=new JLabel(I18N.getMsg("calendar.seconds"));
		tfSeconds = new JFormattedTextField(deuxChiffres);
		tfSeconds.setColumns(2);
		tfSeconds.setValue(calendar.seconds);
		add(lbSeconds,"split 2");add(tfSeconds);

		JLabel lbStartDay=new JLabel(I18N.getMsg("calendar.startday"));
		tfStartDay = new JFormattedTextField(deuxChiffres);
		tfStartDay.setColumns(2);
		tfStartDay.setValue(calendar.first_day);
		add(lbStartDay,"split 2");add(tfStartDay);
		
		JButton btReinit = new JButton(I18N.getMsg("z.reinit"));
		btReinit.addActionListener((ActionEvent evt) -> {
			reinit();
			validate();
		});
		add(btReinit,"span, split 3, right");

		addOkCancel();

		pack();
		setLocationRelativeTo(mainFrame);
		setModal(true);
	}
	
	public void reinit() {
		tfWeekDays.setValue(calendar.days.size());
		lsDays.setText(getListDays());
		tfMonths.setText(calendar.getMonths());
		lsMonths.setText(getListMonth());
		tfHours.setValue(calendar.hours);
		tfMinutes.setValue(calendar.minutes);
		tfSeconds.setValue(calendar.seconds);
		tfStartDay.setValue(calendar.first_day);
	}
	
	private boolean checkOK() {
		return(true);
	}
	
	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				applySettings();
			}
		};
	}
	
	private void applySettings() {
		if (checkOK()) {
			book.info.calendar.setMonths(tfMonths.getText());
			book.info.calendar.setDays(tfWeekDays.getText());
			String s=tfHours.getText()+","+tfMinutes.getText()+","+tfSeconds.getText()+","+tfStartDay.getText();
			book.info.calendar.setCalendar(s);
			mainFrame.xml.setModified();
			canceled = false;
			dispose();
		}
	}

	public String getListDays() {
		String r="<html>";
		for (int i=0;i<calendar.days.size();i++) {
			if (!r.equals("<html>")) r+=", ";
			if (i>0 && i%5==0) r+="<br>";
			r+=calendar.getDayName(i);
		}
		return(r+"</html>");
	}

	public String getListMonth() {
		String r="<html>";
		for (int i=0;i<calendar.months.size();i++) {
			if (!r.equals("<html>")) r+=", ";
			if (i>0 && i%6==0) r+="<br>";
			r+=calendar.getMonthName(i);
		}
		return(r+"</html>");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}
