/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import java.awt.Font;
import java.awt.Insets;
import java.io.File;
import java.util.Locale;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import lib.miginfocom.swing.MigLayout;

import desk.app.App;
import desk.app.Language;
import desk.app.pref.AppPref;
import desk.dialog.chooser.FontChooserDlg;
import desk.tools.Backup;
import desk.tools.swing.SwingUtil;
import db.I18N;
import desk.app.MainFrame;
import desk.tools.FontUtil;
import desk.tools.IOTools;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JOptionPane;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class PreferencesDlg extends AbstractDialog {

	public static void show(MainFrame m) {
		PreferencesDlg dlg = new PreferencesDlg(m);
		dlg.setVisible(true);
	}

	private JComboBox<String> cbLanguage;
	private Font fontDefault, fontEditor, fontMono;
	private int inLang;
	private JCheckBox ckOnStart,
			ckOnExit,
			ckTypist,
			ckMemory,
			ckBackupOpen,
			ckBackupClose,
			ckBackupIncrement;
	private JRadioButton
			rbUpdaterNever,
			rbUpdaterYear,
			rbUpdaterAuto,
			rbTxt,
			rbCsv,
			rbXml,
			rbHtml;
	private JLabel lbOnStart,
			lbOnExit,
			lbUpdater,
			//lbAppearence,
			lbFontDefault,
			lbFontEditor,
			lbFontMono,
			lbExport,
			lbExportParam,
			lbCommon,
			lbLanguage,
			lbAutoBackup;
	private Locale currentlocale;
	private JTextField tfAssistant,
			tfBackupDir,
			tfDateTimeFormat;
	private Backup backup;
	private AppPref preferences;
	private JButton btFontDefault, btFontEditor, btFontMono;

	public PreferencesDlg(MainFrame m) {
		super(m);
		initAll();
	}

	@Override
	public void init() {
		preferences = App.getInstance().getPreferences();
		currentlocale = Locale.getDefault();
	}

	@Override
	@SuppressWarnings("static-access")
	public void initUi() {
		super.initUi();
		ckMemory = new JCheckBox(I18N.getMsg("memory"));
		ckMemory.setSelected(mainFrame.getPref().getBoolean(AppPref.Key.MEMORY, false));

		// layout
		setLayout(new MigLayout("wrap,fill"));
		setTitle(I18N.getMsg("pref"));
		setIconImage(IconUtil.getIconImage("icon"));
		JTabbedPane tbPane = new JTabbedPane();
		tbPane.addTab(I18N.getMsg("pref.general"), initCommon());
		tbPane.addTab(I18N.getMsg("pref.updater"), initUpdater());
		tbPane.addTab(I18N.getMsg("pref.appearence"), initAppearence());
		tbPane.addTab(I18N.getMsg("export"), initExport());
		tbPane.addTab(I18N.getMsg("backup"), initBackup());
		add(tbPane, "span, growx,wrap");
		add(ckMemory, "wrap");
		addOkCancel();
		pack();
		this.setLocationRelativeTo(mainFrame);
		this.setModal(true);
	}

	private JPanel initCommon() {
		lbCommon = new JLabel(I18N.getMsg("pref.general"));
		lbCommon.setFont(new java.awt.Font("Noto Sans", 1, 12));

		lbLanguage = new JLabel(I18N.getMsg("language"));
		String currentLangStr = I18N.getCountryLanguage(Locale.getDefault());
		Language.LANGUAGE lang_idx = Language.LANGUAGE.valueOf(currentLangStr);
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
		for (Language.LANGUAGE lang : Language.LANGUAGE.values()) {
			model.addElement(lang.getI18N());
		}
		cbLanguage = new javax.swing.JComboBox<>();
		cbLanguage.setModel(model);
		inLang = lang_idx.ordinal();
		cbLanguage.setSelectedIndex(lang_idx.ordinal());
		cbLanguage.addActionListener((java.awt.event.ActionEvent evt) -> {
			int i = cbLanguage.getSelectedIndex();
			Language.LANGUAGE lang = Language.LANGUAGE.values()[i];
			Locale locale = lang.getLocale();
			I18N.initMsgInternal(locale);
			refreshUi();
		});

		JLabel lbDateTimeFormat = new JLabel(I18N.getMsg("z.date.format.datetime.label"));
		tfDateTimeFormat = new JTextField(App.getInstance().format_DateTime);
		tfDateTimeFormat.setName(I18N.getMsg("z.date.format.datetime.label"));

		lbOnStart = new JLabel(I18N.getMsg("pref.start"));
		ckOnStart = new JCheckBox(I18N.getMsg("pref.start.openproject"));
		ckOnStart.setSelected(preferences.getBoolean(AppPref.Key.OPEN_LAST_FILE, false));

		lbOnExit = new JLabel(I18N.getMsg("pref.exit"));
		ckOnExit = new JCheckBox(I18N.getMsg("pref.exit.confirm"));
		ckOnExit.setSelected(preferences.getBoolean(AppPref.Key.CONFIRM_EXIT, false));

		JLabel lbAssistant = new JLabel(I18N.getMsg("assistant.file"));
		tfAssistant = new JTextField();
		tfAssistant.setText(preferences.getString(AppPref.Key.ASSISTANT, ""));
		JButton btAssistant = new JButton();
		btAssistant.setIcon(IconUtil.getIcon("small/file-open"));
		btAssistant.setMargin(new Insets(0, 0, 0, 0));
		btAssistant.addActionListener((java.awt.event.ActionEvent evt) -> {
			String str = tfAssistant.getText();
			if (str.isEmpty()) {
				str = App.getPrefDir().getAbsolutePath();
			}
			JFileChooser chooser = new JFileChooser(str);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int i = chooser.showOpenDialog(null);
			if (i != 0) {
				return;
			}
			File file = chooser.getSelectedFile();
			tfAssistant.setText(file.getAbsolutePath());
			tfAssistant.setCaretPosition(0);
		});

		//layout
		JPanel p = new JPanel();
		p.setLayout(new MigLayout());
		p.add(lbCommon, "wrap");
		p.add(lbLanguage, "split 2");
		p.add(cbLanguage, "wrap");
		p.add(lbDateTimeFormat, "split 2");
		p.add(tfDateTimeFormat, "wrap");
		p.add(lbOnStart, "split 2");
		p.add(ckOnStart, "wrap");
		p.add(lbOnExit, "split 2");
		p.add(ckOnExit, "wrap");
		p.add(lbAssistant, "split 3");
		p.add(tfAssistant, "growx");
		p.add(btAssistant, "wrap");

		return (p);
	}

	private JPanel initUpdater() {
		lbUpdater = new JLabel(I18N.getMsg("pref.updater"));
		lbUpdater.setFont(new java.awt.Font("Noto Sans", 1, 12));
		rbUpdaterNever = new JRadioButton(I18N.getMsg("pref.updater.never"));
		rbUpdaterYear = new JRadioButton(I18N.getMsg("pref.updater.year"));
		rbUpdaterAuto = new JRadioButton(I18N.getMsg("pref.updater.auto"));
		switch (preferences.getString(AppPref.Key.UPDATER_DO, "")) {
			case "1":
				rbUpdaterNever.setSelected(true);
				break;
			case "2":
				rbUpdaterYear.setSelected(true);
				break;
			case "3":
				rbUpdaterAuto.setSelected(true);
				break;
			default:
				rbUpdaterNever.setSelected(true);
		}
		ButtonGroup bgUpdater = new ButtonGroup();
		bgUpdater.add(rbUpdaterNever);
		bgUpdater.add(rbUpdaterYear);
		bgUpdater.add(rbUpdaterAuto);

		JPanel p = new JPanel();
		p.setLayout(new MigLayout());
		p.add(lbUpdater, "span, wrap");
		p.add(new JLabel("     "));
		p.add(rbUpdaterNever, "wrap");
		p.add(new JLabel("     "));
		p.add(rbUpdaterYear, "wrap");
		p.add(new JLabel("     "));
		p.add(rbUpdaterAuto, "wrap");
		return (p);
	}

	private JPanel fontPanel(String name, Font font) {
		JPanel panel = new JPanel(new MigLayout());
		panel.setName("pFont_" + name);
		JLabel lb = new JLabel(I18N.getMsg("pref.font." + name));
		JButton btFont = new JButton(IconUtil.getIcon("small/preferences"));
		btFont.setName("btFont_" + name);
		btFont.setText(SwingUtil.getNiceFontName(font));
		btFont.setMargin(new Insets(0, 0, 0, 0));
		btFont.addActionListener((java.awt.event.ActionEvent evt) -> {
			FontChooserDlg dlg = new FontChooserDlg(this, font);
			dlg.setVisible(true);
			if (dlg.isCanceled) {
				return;
			}
			if (dlg.getSelectedFont() == null) {
				return;
			}
			Font lfont = dlg.getSelectedFont();
			switch (name) {
				case "general":
					fontDefault = lfont;
					break;
				case "edit":
					fontEditor = lfont;
					break;
				default:
					fontMono = lfont;
					break;
			}
			btFont.setText(SwingUtil.getNiceFontName(lfont));
		});
		panel.add(lb);
		panel.add(btFont);
		return (panel);
	}

	private Font fontChooser(Font font) {
		FontChooserDlg dlg = new FontChooserDlg(this, font);
		dlg.setVisible(true);
		if (dlg.isCanceled) {
			return (null);
		}
		return (dlg.getSelectedFont());
	}

	private JPanel initAppearence() {
		JPanel p = new JPanel(new MigLayout("wrap, ins 0", "[][fill]"));
		//p.add(new JLabel(I18N.getMsg("appearence")),"span,wrap");

		fontDefault = App.getInstance().fontGetDefault();
		lbFontDefault=new JLabel(I18N.getColonMsg("pref.font.standard"));
		p.add(lbFontDefault);
		btFontDefault = new JButton(IconUtil.getIcon("small/preferences"));
		btFontDefault.setFont(fontDefault);
		btFontDefault.setHorizontalTextPosition(AbstractButton.LEADING);
		btFontDefault.setText(SwingUtil.getNiceFontName(fontDefault));
		btFontDefault.addActionListener((java.awt.event.ActionEvent evt) -> {
			Font lfont = fontChooser(fontDefault);
			if (lfont != null) {
				fontDefault = lfont;
				btFontDefault = (JButton) evt.getSource();
				btFontDefault.setText(SwingUtil.getNiceFontName(fontDefault));
				btFontDefault.setFont(lfont);
			}
		});
		p.add(btFontDefault);

		fontEditor = App.getInstance().fontGetEditor();
		lbFontEditor=new JLabel(I18N.getColonMsg("pref.font.editor"));
		p.add(lbFontEditor);
		btFontEditor = new JButton(IconUtil.getIcon("small/preferences"));
		btFontEditor.setFont(fontEditor);
		btFontEditor.setText(SwingUtil.getNiceFontName(fontEditor));
		btFontEditor.setHorizontalTextPosition(AbstractButton.LEADING);
		btFontEditor.addActionListener((java.awt.event.ActionEvent evt) -> {
			Font lfont = fontChooser(fontEditor);
			if (lfont != null) {
				fontEditor = lfont;
				btFontEditor = (JButton) evt.getSource();
				btFontEditor.setText(SwingUtil.getNiceFontName(fontEditor));
				btFontEditor.setFont(lfont);
			}
		});
		p.add(btFontEditor);

		fontMono = App.getInstance().fontGetMono();
		lbFontMono=new JLabel(I18N.getColonMsg("pref.font.mono"));
		p.add(lbFontMono);
		btFontMono = new JButton(IconUtil.getIcon("small/preferences"));
		btFontMono.setFont(fontMono);
		btFontMono.setText(SwingUtil.getNiceFontName(fontMono));
		btFontMono.setHorizontalTextPosition(AbstractButton.LEADING);
		btFontMono.addActionListener((java.awt.event.ActionEvent evt) -> {
			Font lfont = fontChooser(fontMono);
			if (lfont != null) {
				fontMono = lfont;
				btFontMono = (JButton) evt.getSource();
				btFontMono.setText(SwingUtil.getNiceFontName(fontMono));
				btFontMono.setFont(lfont);
			}
		});
		p.add(btFontMono);

		ckTypist = new JCheckBox(I18N.getMsg("pref.typist"));
		ckTypist.setSelected(preferences.getBoolean(AppPref.Key.TYPIST_USE, false));
		p.add(ckTypist, "span");

		return (p);
	}

	private JPanel initExport() {
		lbExport = new JLabel(I18N.getMsg("pref.export"));
		lbExport.setFont(FontUtil.getBold());
		lbExportParam = new JLabel(I18N.getMsg("pref.export.parameters"));
		lbExportParam.setFont(FontUtil.getItalic());
		rbTxt = new JRadioButton(I18N.getMsg("pref.export.txt"));
		rbCsv = new JRadioButton(I18N.getMsg("pref.export.csv"));
		rbXml = new JRadioButton(I18N.getMsg("pref.export.xml"));
		rbHtml = new JRadioButton(I18N.getMsg("pref.export.html"));
		switch (preferences.getString(AppPref.Key.EXPORT_PREF, "xml")) {
			case "txt":
				rbTxt.setSelected(true);
				break;
			case "csv":
				rbCsv.setSelected(true);
				break;
			case "html":
				rbCsv.setSelected(true);
				break;
			default:
				rbXml.setSelected(true);
				break;
		}
		ButtonGroup bgExport = new ButtonGroup();
		bgExport.add(rbTxt);
		bgExport.add(rbCsv);
		bgExport.add(rbXml);
		bgExport.add(rbHtml);

		//layout
		JPanel p = new JPanel();
		p.setLayout(new MigLayout());
		p.add(lbExport, "span,wrap");
		p.add(rbTxt);
		p.add(rbCsv);
		p.add(rbXml);
		p.add(rbHtml, "wrap");
		p.add(lbExportParam, "span");
		return (p);
	}

	private JPanel initBackup() {
		JLabel lb1 = new JLabel(I18N.getMsg("backup"));
		lb1.setFont(new Font("Noto Sans", Font.BOLD, 12));
		JLabel lbDir = new JLabel(I18N.getMsg("z.directory"));
		JButton btBackupDir = new JButton();
		btBackupDir.setIcon(IconUtil.getIcon("small/file-open"));
		btBackupDir.setMargin(new Insets(0, 0, 0, 0));
		btBackupDir.addActionListener((java.awt.event.ActionEvent evt) -> {
			String str = tfBackupDir.getText();
			if (str.isEmpty()) {
				str = App.getPrefDir().getAbsolutePath();
			}
			File dir = IOTools.selectDirectory(null, str);
			if (dir == null) {
				return;
			}
			tfBackupDir.setText(dir.getAbsolutePath());
			tfBackupDir.setCaretPosition(0);
		});

		getPrefBackup();

		JPanel p = new JPanel();
		p.setLayout(new MigLayout("", "[][]"));
		p.add(lb1, "span");
		p.add(lbDir, "span");
		p.add(tfBackupDir, "span, split 2,growx");
		p.add(btBackupDir);
		p.add(ckBackupIncrement, "span, wrap");
		p.add(lbAutoBackup, "span, split 3");
		p.add(ckBackupOpen);
		p.add(ckBackupClose);
		return (p);
	}

	@Override
	public AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (applySettings()) {
					canceled = false;
					dispose();
				}
			}
		};
	}

	private boolean checkOneDateFormat(JTextField tf) {
		if (tf.getText().isEmpty()) {
			return (false);
		}
		return (true);
	}

	private boolean checkDates() {
		String err = "";
		if (!checkOneDateFormat(tfDateTimeFormat)) {
			err += I18N.getMsg("z.date.format.error", tfDateTimeFormat.getName()) + "\n";
		}
		return (err.isEmpty());
	}

	public boolean applySettings() {
		//App.trace("Apply settings");
		if (!checkDates()) {
			return (false);
		}
		App app = App.getInstance();
		SwingUtil.setWaitingCursor(this);
		String updater = "1";
		if (rbUpdaterYear.isSelected()) {
			updater = "2";
		} else if (rbUpdaterAuto.isSelected()) {
			updater = "3";
		}
		preferences.setString(AppPref.Key.UPDATER_DO, updater);

		preferences.setBoolean(AppPref.Key.OPEN_LAST_FILE, ckOnStart.isSelected());
		preferences.setBoolean(AppPref.Key.CONFIRM_EXIT, ckOnExit.isSelected());

		String export = "xml";
		if (rbTxt.isSelected()) {
			export = "txt";
		} else if (rbCsv.isSelected()) {
			export = "csv";
		} else if (rbHtml.isSelected()) {
			export = "html";
		}
		preferences.setString(AppPref.Key.EXPORT_PREF, export);
		// language
		int i = cbLanguage.getSelectedIndex();
		Language.LANGUAGE lang = Language.LANGUAGE.values()[i];
		Locale locale = lang.getLocale();
		boolean b=false;
		if (!preferences.getLanguage().equals(I18N.getCountryLanguage(locale))) b=true;
		preferences.setLanguage(I18N.getCountryLanguage(locale));
		preferences.setString(AppPref.Key.DATETIMEFORMAT, tfDateTimeFormat.getText());
		app.format_DateTime = tfDateTimeFormat.getText();
		I18N.initMsgInternal(locale);
		app.fontSetDefault(fontDefault);
		app.fontSetEditor(fontEditor);
		app.fontSetMono(fontMono);
		//default use Typist
		preferences.setBoolean(AppPref.Key.TYPIST_USE, ckTypist.isSelected());
		preferences.setBoolean(AppPref.Key.MEMORY, ckMemory.isSelected());
		if (tfAssistant.getText().isEmpty()) {
			preferences.setString(AppPref.Key.ASSISTANT, "");
		} else {
			preferences.setString(AppPref.Key.ASSISTANT, tfAssistant.getText());
		}
		//backup
		setPrefBackup();
		app.refresh();
		if (b) {
			JOptionPane.showMessageDialog(this,
					I18N.getMsg("language.change"),
					I18N.getMsg("language"),
					JOptionPane.WARNING_MESSAGE);
		}
		if (inLang != lang.ordinal()) {
			app.refreshViews();
		}
		SwingUtil.setDefaultCursor(this);
		return (true);
	}

	private void refreshUi() {
		ckMemory.setText(I18N.getMsg("pref.memory"));
		lbCommon.setText(I18N.getMsg("pref.general"));
		lbLanguage.setText(I18N.getMsg("language"));
		lbOnStart.setText(I18N.getMsg("pref.start"));
		ckOnStart.setText(I18N.getMsg("pref.start.openproject"));
		lbOnExit.setText(I18N.getMsg("pref.exit"));
		ckOnExit.setText(I18N.getMsg("pref.exit.confirm"));
		lbUpdater.setText(I18N.getMsg("pref.updater"));
		rbUpdaterNever.setText(I18N.getMsg("pref.updater.never"));
		rbUpdaterYear.setText(I18N.getMsg("pref.updater.year"));
		rbUpdaterAuto.setText(I18N.getMsg("pref.updater.auto"));
		//lbAppearence.setText(I18N.getMsg("pref.appearence"));
		lbFontDefault.setText(I18N.getMsg("pref.font.standard"));
		lbFontEditor.setText(I18N.getMsg("pref.font.editor"));
		lbFontMono.setText(I18N.getMsg("pref.font.mono"));
		ckTypist.setText(I18N.getMsg("pref.typist"));
		lbExport.setText(I18N.getMsg("export"));
		lbExportParam.setText(I18N.getMsg("pref.export.parameters"));
		rbTxt.setText(I18N.getMsg("pref.export.txt"));
		rbCsv.setText(I18N.getMsg("pref.export.csv"));
		rbXml.setText(I18N.getMsg("pref.export.xml"));
		rbHtml.setText(I18N.getMsg("pref.export.html"));
	}

	private void getPrefBackup() {
		backup = new Backup(mainFrame);
		tfBackupDir = new JTextField(backup.getDir());
		lbAutoBackup = new JLabel(I18N.getMsg("backup.auto"));
		ckBackupOpen = new JCheckBox(I18N.getMsg("backup.auto.open"));
		ckBackupOpen.setSelected(backup.getOpen());
		ckBackupClose = new JCheckBox(I18N.getMsg("backup.auto.close"));
		ckBackupClose.setSelected(backup.getClose());
		ckBackupIncrement = new JCheckBox(I18N.getMsg("backup.increment"));
		ckBackupIncrement.setSelected(backup.getIncrement());
	}

	private void setPrefBackup() {
		backup.setDir(tfBackupDir.getText());
		backup.setOpen(ckBackupOpen.isSelected());
		backup.setClose(ckBackupClose.isSelected());
		backup.setIncrement(ckBackupIncrement.isSelected());
		backup.saveParam();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
