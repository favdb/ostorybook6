/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import db.entity.Location;
import desk.app.MainFrame;
import db.I18N;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class RenameDlg extends AbstractDialog {

	private final String which;
	JComboBox<String> combo;
	JTextField tfNewName;

	public RenameDlg(MainFrame parent, String which) {
		super(parent);
		this.which=which;
		initAll();
	}

	@Override
	public void init() {
	}
	
	@Override
	public void initUi() {
		JLabel lbRename=new JLabel(I18N.getMsg("rename.rename"));
        combo = new JComboBox<>();
		JLabel lbTo = new JLabel(I18N.getMsg("rename.to"));
        tfNewName = new JTextField(20);
		switch(which) {
			case "country":
				this.setTitle(I18N.getMsg("location.rename.country"));
				createListCombo(countryGetList());
				break;
			case "city":
				this.setTitle(I18N.getMsg("location.rename.city"));
				createListCombo(cityGetList());
				break;
		}
		
		//layout
		setLayout(new MigLayout("wrap 4", "[]", "[]20[]"));
		setTitle(I18N.getMsg("rename"));
		add(lbRename);add(combo);add(lbTo);add(tfNewName);
		add(buttonOk(), "sg,span,split 2,right");
		add(buttonCancel(), "sg");
		pack();
		setLocationRelativeTo(mainFrame);
		this.setModal(true);		
	}
	
	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String oldValue = (String) combo.getSelectedItem();
				String newValue = tfNewName.getText();
				if (!newValue.isEmpty()) {
					rename(oldValue, newValue);
					dispose();
				}
			}
		};
	}
	
	@SuppressWarnings("unchecked")
	private void createListCombo(List<String> list) {
		DefaultComboBoxModel model = new DefaultComboBoxModel();
		list.forEach((category)-> {
			model.addElement(category);
		});
		combo.setModel(model);
	}

	private void rename(String oldValue, String newValue) {
		switch(which) {
			case "country":
				countryRename(oldValue, newValue);
				break;
			case "city":
				cityRename(oldValue, newValue);
				break;
		}
	}
	
	protected List<String> countryGetList() {
		List<String> ret = Location.findCountries(book);
		return ret;
	}

	protected void countryRename(String oldValue, String newValue) {
		List<Location> locations = Location.findByCountry(mainFrame.book.locations,oldValue);
		locations.forEach((location)-> {
			location.country=(newValue);
			mainFrame.getBookController().updateEntity(location);
		});
	}
	
	protected List<String> cityGetList() {
		List<String> ret = Location.findCities(book);
		return ret;
	}

	protected void cityRename(String oldValue, String newValue) {
		List<Location> locations = Location.findByCity(mainFrame.book.locations,oldValue);
		locations.forEach((location)-> {
			location.city=(newValue);
			mainFrame.getBookController().updateEntity(location);
		});
	}

	public static void show(MainFrame m, String t) {
		RenameDlg dlg=new RenameDlg(m, t);
		dlg.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
