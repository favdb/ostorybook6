/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import lib.miginfocom.swing.MigLayout;
import db.I18N;
import db.entity.AbstractEntity;
import db.util.CheckUsed;
import desk.app.MainFrame;
import java.awt.event.ActionEvent;

/**
 *
 * @author favdb
 */
public class ConfirmDeleteDlg extends AbstractDialog {

	public static boolean show(MainFrame mainFrame, AbstractEntity entity, CheckUsed d) {
		List<AbstractEntity> en=new ArrayList<>();
		en.add(entity);
		return(show(mainFrame,en,d));
	}

	public static boolean show(MainFrame mainFrame, List<AbstractEntity> en, CheckUsed d) {
		ConfirmDeleteDlg dlg=new ConfirmDeleteDlg(mainFrame, en, d);
		dlg.setVisible(true);
		return(!dlg.canceled);
	}

	private JTextPane tpEntity;
	private final List<AbstractEntity> entities;
	private final CheckUsed displayUsed;

	public ConfirmDeleteDlg(MainFrame parent, List<AbstractEntity> entities, CheckUsed d) {
		super(parent);
		mainFrame = parent;
		this.entities=entities;
		this.displayUsed=d;
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		this.setModal(true);
		JLabel lb = new JLabel(I18N.getMsg("z.delete.confirm"));
		//scroller.setMinimumSize(new Dimension(334, 348));
        tpEntity = new JTextPane();
        tpEntity.setEditable(false);
        tpEntity.setContentType("text/html");
		StringBuilder buf = new StringBuilder();
		if (displayUsed!=null && !displayUsed.rc.isEmpty()) {
			buf.append(displayUsed.rc).append("<hr>\n");
		} else {
			entities.forEach((e)-> {
				buf.append(e.toHtmlDetail()).append("<hr style='margin:10px'>\n");
			});
		}
		tpEntity.setText(buf.toString());
		tpEntity.setCaretPosition(0);
		JScrollPane scroller = new JScrollPane();
        scroller.setViewportView(tpEntity);
		
		//layout
		setLayout(new MigLayout());
        setTitle(I18N.getMsg("z.warning"));
		add(lb,"wrap");add(scroller,"growx,wrap");
		addOkCancel();
		pack();
		setLocationRelativeTo(null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
