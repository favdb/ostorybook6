/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Category;
import desk.tools.FontUtil;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import static desk.tools.Ui.initTextField;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import org.jsoup.helper.StringUtil;

/**
 *
 * @author favdb
 */
public class PanelCategory extends AbstractEditorPanel {

	private final Category category;
	private JTextField tfSort;
	private JComboBox cbSup;
	JComboBox cbBeacon;

	public PanelCategory(Editor m, AbstractEntity e) {
		super(m,e);
		category=(Category)entity;
		initAll();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p);
		JLabel lb=new JLabel(I18N.getColonMsg("type"));
		lb.setFont(FontUtil.getBold());
		p.add(lb,"right");
		cbBeacon=new JComboBox();
		cbBeacon.setName("category.beacon");
		int i=0;
		String ztype=category.beacon;
		if (ztype==null) ztype="";
		cbBeacon.addItem("");
		for (Category.BEACON t:Category.BEACON.values()) {
			cbBeacon.addItem(t);
			if (ztype.equals(t.name().toLowerCase())) {
				cbBeacon.setSelectedIndex(i);
			}
			i++;
		}
		p.add(cbBeacon);
		tfSort=initTextField(p,"sort",5,category.sort,Ui.MANDATORY,!Ui.INFO);
		if (category.sort<1) tfSort.setText("");
		cbSup=Ui.initAutoCB(p,mainFrame,"category.sup",
				Book.TYPE.CATEGORY, category.sup,category,!MANDATORY,!NEW,EMPTY);
		initDescription(p,null);
		initNotes(p,null);
	}
	
	@Override
	public boolean verifier() {
		JTextField tf=new JTextField();
		resetError();
		cbBeacon.setBackground(tf.getBackground());
		tfSort.setBackground(tf.getBackground());
		if (cbBeacon.getSelectedIndex()<1) {
			errorMsg(cbBeacon, "error.missing");
		}
		if (tfSort.getText().isEmpty()) {
			errorMsg(tfSort, "error.missing");
		} else if (tfSort.getText().equals("+")) {
		} else if (!StringUtil.isNumeric(tfSort.getText())) {
			errorMsg(tfSort, "error.notnumeric");
		} else {
			int n=Integer.parseInt(tfSort.getText());
			if (cbBeacon.getSelectedIndex()!=-1) {
				List<Category> lst=Category.findBeacon(book.categories, (String)cbBeacon.getSelectedItem());
				if (Category.findSort(lst,n)!=null) {
					errorMsg(tfSort, "error.exists");
				}
			}
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		category.setName(tfName.getText());
		if (tfSort.getText().equals("+")) {
			category.setSort(Category.getSortNext(book.categories));
		} else category.setSort(Integer.parseInt(tfSort.getText()));
		category.beacon=(((Category.BEACON)cbBeacon.getSelectedItem()).toString());
		if (cbSup.getSelectedIndex()>0) category.sup=((Category)cbSup.getSelectedItem());
		else category.sup=null;
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
