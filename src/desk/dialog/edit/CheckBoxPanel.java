package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import desk.app.App;
import desk.app.MainFrame;
import desk.interfaces.IRefreshable;
import desk.panel.AbstractPanel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;

@SuppressWarnings("serial")
public class CheckBoxPanel extends AbstractPanel implements IRefreshable {

	public List<EntityCB> entityCB;
	private AbstractEntity entity, pointedEntity;
	public List<AbstractEntity> entities;
	public boolean autoSelect = true;
	private Book.TYPE search=Book.TYPE.NONE;
	
	public CheckBoxPanel(MainFrame mainFrame) {
		super(mainFrame);
	}
	
	public CheckBoxPanel(MainFrame mainFrame, Book.TYPE search, AbstractEntity toSel) {
		super(mainFrame);
		this.search=search;
		initAll();
		if (toSel != null) {
			selectEntity(toSel);
		}
	}
	
	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
		//App.trace("CheckBoxPanel.init() for search="+search);
		entityCB=new ArrayList<>();
		List<?> ls;
		switch(search) {
			case CATEGORY:
			case CATEGORIES:
				ls=book.categories;
				break;
			case ITEM:
			case ITEMS:
				ls=book.items;
				break;
			case LOCATION:
			case LOCATIONS:
				ls=book.locations;
				break;
			case PERSON:
			case PERSONS:
				ls=book.persons;
				break;
			case PHOTO:
			case PHOTOS:
				ls=book.photos;
				break;
			case PLOT:
			case PLOTS:
				ls=book.plots;
				break;
			case POV:
			case POVS:
				ls=book.povs;
				break;
			case TAG:
			case TAGS:
				ls=book.tags;
				break;
			default: return;
		}
		ls.sort((o1, o2) -> ((AbstractEntity)o1).name.compareTo(((AbstractEntity)o2).name));
		ls.forEach((obj)-> {
			AbstractEntity entity=(AbstractEntity)obj;
			addEntity(entity);
		});
	}

	public void addEntity(AbstractEntity entity) {
		//App.trace("CheckBoxPanel.addEntity(entity="+entity.toString()+")");
		JCheckBox ck = new JCheckBox();
		ck.setOpaque(false);
		ck.addActionListener((ActionEvent evt) -> {
			pointedEntity=entity;
		});
		entityCB.add(new EntityCB(entity,ck));
		ck.setText(entity.toString());
		//ck.setIcon(entity.getIcon());
		ck.setToolTipText("<html>"+entity.toHtmlShort()+"</html");
		add(ck);
	}
	
	@Override
	public void initUi() {
		//App.trace("CheckBoxPanel.initUi()");
		setLayout(new MigLayout("wrap"));
		setBackground(Color.white);
		refresh();
	}

	@Override
	public void refresh() {
		//App.trace("CheckBoxPanel.refresh()");
		removeAll();
		entityCB.forEach((ecb)-> {
			add(ecb.cb, "split 2");
			add(new JLabel(ecb.entity.getIcon()));
		});

		revalidate();
		repaint();
	}

	public void selectEntity(AbstractEntity ent) {
		JCheckBox cb = findCB(ent);
		if (cb != null) {
			cb.setSelected(true);
		}
	}
	
	private JCheckBox findCB(AbstractEntity ent) {
		for (EntityCB ecb:entityCB) {
			if (ecb.entity.equals(ent)) {
				return(ecb.cb);
			}
		}
		return(null);
	}
	
	public void selectEntities(List<AbstractEntity> entities) {
		entities.forEach((e)-> selectEntity(e));
	}

	public List<AbstractEntity> getSelectedEntities() {
		ArrayList<AbstractEntity> ret = new ArrayList<>();
		entityCB.forEach((ecb)-> {
			if (ecb.cb.isSelected()) {
				ret.add(ecb.entity);
			}
		});
		return ret;
	}

	public AbstractEntity getPointedEntity() {
		return(pointedEntity);
	}

	public AbstractEntity getEntity() {
		return entity;
	}

	public void setEntity(AbstractEntity entity) {
		this.entity = entity;
	}

	public void setEntityList(List<AbstractEntity> entities) {
		this.entities = entities;
	}

	public Book.TYPE getSearch() {
		return search;
	}

	public void setSearch(Book.TYPE search) {
		this.search = search;
	}

	public boolean getAutoSelect() {
		return autoSelect;
	}

	public void setAutoSelect(boolean flag) {
		this.autoSelect = flag;
	}

	public static class EntityCB {
		AbstractEntity entity;
		JCheckBox cb;

		public EntityCB(AbstractEntity entity, JCheckBox cb) {
			this.entity=entity;
			this.cb=cb;
		}
		
		public JCheckBox getCB() {
			return(cb);
		}
	}

}
