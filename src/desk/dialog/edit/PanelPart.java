/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Part;
import db.entity.SbDate;
import desk.dialog.chooser.DateChooser;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.util.Objects;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class PanelPart extends AbstractEditorPanel {

	private final Part part;
	private JTextField tfNumber;
	private JComboBox cbSup;
	private JTextField tfSize;
	private DateChooser dObjective;
	private DateChooser dDone;
	private JTextField tfDoneSize;

	public PanelPart(Editor m, AbstractEntity e) {
		super(m,e);
		part=(Part)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p);
		tfNumber=Ui.initTextField(p,"number",5,part.number.toString(),MANDATORY,!INFO);
		if (part.number<0) tfNumber.setText("");
		cbSup=Ui.initAutoCB(p,mainFrame,"part.sup",Book.TYPE.PART,part.sup,part,!MANDATORY,!NEW,EMPTY);
		JPanel group=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		group.setBorder(javax.swing.BorderFactory.createTitledBorder(I18N.getMsg("objective")));
		tfSize=initTextField(group,"objective.chars",5,part.size,!MANDATORY,!INFO);
		if (part.size<1) tfSize.setText("");
		dObjective=initSbDate(group,"objective.date",part.objective, !MANDATORY,!TIME);
		tfDoneSize=Ui.initTextField(group,"objective.donechars",4,part.getChars(book),!MANDATORY,INFO);
		dDone=initSbDate(group,"objective.done",part.done, !MANDATORY,!TIME);
		p.add(group,"span,wrap,right");
		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (tfNumber.getText().isEmpty()) {
			errorMsg(tfNumber, "error.missing");
		} else if (tfNumber.getText().equals("+")) {
		} else if (!StringUtil.isNumeric(tfNumber.getText())) {
			errorMsg(tfNumber, "error.notNumeric");
		} else {
			int n=Integer.parseInt(tfNumber.getText());
			Part b=Part.findNumber(mainFrame.book.parts,n);
			if (b!=null && (b.type.equals(part.type) && !Objects.equals(b.id, part.id))) {
				errorMsg(tfNumber, "error.exists");
				tfNumber.setBackground(Color.red);
			}
		}
		if (!tfSize.getText().isEmpty()) {
			if (!StringUtil.isNumeric(tfSize.getText())) {
				errorMsg(tfSize,"error.notnumeric");
			}
			if (dObjective.getDate()!=null && dDone.getDate()!=null) {
				if (dObjective.getDate().after(dDone.getDate())) {
					errorMsg(dObjective,"error.date_incorrect");
				}
			}
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		part.setName(tfName.getText());
		if (tfNumber.getText().equals("+")) {
			part.number=(Part.getNextNumber(book.parts));
		} else {
			part.number=(Integer.parseInt(tfNumber.getText()));
		}
		if (cbSup.getSelectedIndex()>0) part.sup=((Part)cbSup.getSelectedItem());
		else part.sup=(null);
		if (tfSize.getText().isEmpty()) {
			part.size=(0);
			part.objective=(null);
			part.done=(null);
		} else {
			part.size=(Integer.parseInt(tfSize.getText()));
			part.objective=(new SbDate(dObjective.getDate().toString()));
			part.done=(new SbDate(dDone.getDate().toString()));
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
