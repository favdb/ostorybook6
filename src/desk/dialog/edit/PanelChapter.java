/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Chapter;
import desk.dialog.chooser.DateChooser;
import db.I18N;
import db.entity.Book;
import db.entity.Part;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import java.util.Objects;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class PanelChapter extends AbstractEditorPanel {
	
	Chapter chapter;
	private JComboBox cbPart;
	private JTextField tfNumber;
	private JTextField tfSize;
	private DateChooser dObjective;
	private DateChooser dDone;
	private JTextField tfDoneSize;
	
	public PanelChapter(Editor m, AbstractEntity e) {
		super(m,e);
		chapter=(Chapter)entity;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg("chapter"));
		initHeader(p);
		tabbed.add(p);
		cbPart=Ui.initComboBox(p,mainFrame,"part",Book.TYPE.CHAPTER, chapter.part,null,!MANDATORY,NEW,EMPTY);
		tfNumber=Ui.initTextField(p,"number",5,chapter.number,MANDATORY,!INFO);
		if (chapter.number<1) tfNumber.setText("");
		JPanel group=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		group.setBorder(javax.swing.BorderFactory.createTitledBorder(I18N.getMsg("objective")));
		tfSize=Ui.initTextField(group,"objective.size",4,chapter.size,!MANDATORY,!INFO);
		if (chapter.size<1) tfSize.setText("");
		dObjective=initSbDate(group,"objective.date",chapter.objective, !MANDATORY, !TIME);
		tfDoneSize=initTextField(group,"objective.donesize",4,chapter.getChars(book),!MANDATORY,INFO);
		dDone=initSbDate(group,"objective.done",chapter.done, !MANDATORY, !TIME);
		p.add(group,"span,wrap,right");
		initDescription(p,null);
		initNotes(p,null);
		initAssistant(null,tabbed);
	}
	
	@Override
	public boolean verifier() {
		JTextField tf=new JTextField();
		resetError();
		tfNumber.setBorder(tf.getBorder());
		tfSize.setBorder(tf.getBorder());
		dObjective.setBorder(tf.getBorder());
		dDone.setBorder(tf.getBorder());
		if (cbPart.getSelectedIndex()<0) {
			errorMsg(cbPart,"error.missing");
		}
		if (tfNumber.getText().equals("+")) {
		} else if (tfNumber.getText().isEmpty()) {
			errorMsg(cbPart,"error.missing");
		} else if (!StringUtil.isNumeric(tfNumber.getText())) {
			errorMsg(tfNumber, "error.notnumeric");
		} else {
			int n=Integer.parseInt(tfNumber.getText());
			Chapter c=Chapter.findNumber(mainFrame.book.chapters,n);
			if (c!=null && !Objects.equals(c.id, chapter.id)) {
				errorMsg(tfNumber, "error.exists");
			}
		}
		if (!tfSize.getText().isEmpty()) {
			if (!StringUtil.isNumeric(tfSize.getText())) {
				errorMsg(tfSize,"error.notnumeric");
			}
			if (dObjective.getDate()==null && dDone.getDate()!=null) {
				dObjective.setDate(dDone.getDate().toDate());
			}
			if (dObjective.getDate()==null) {
				errorMsg(dObjective,"error.missing");
			}
		}
		return(msgError.isEmpty());
	}
	
	@Override
	public void apply() {
		chapter.setName(tfName.getText());
		if (cbPart.getSelectedIndex()>=1) chapter.part=(Part)cbPart.getSelectedItem();
		else chapter.part=null;
		if (tfNumber.getText().equals("+")) {
			chapter.number=Chapter.getNextNumber(book.chapters);
		} else {
			chapter.number=Integer.parseInt(tfNumber.getText());
		}
		if (tfSize.getText().isEmpty()) {
			chapter.size=0;
			chapter.objective=null;
			chapter.done=null;
		} else {
			chapter.size=Integer.parseInt(tfSize.getText());
			chapter.objective=dObjective.getDate();
			if (dDone.getDate()==null) chapter.done=null;
			else chapter.done=dDone.getDate();
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
