/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Idea;
import db.entity.Scene;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelIdea extends AbstractEditorPanel {

	private final Idea idea;
	private JComboBox cbCategory;
	private JComboBox cbStatus;
	private JTextField tfTheme;
	
	public PanelIdea(Editor m, AbstractEntity e) {
		super(m,e);
		idea=(Idea)e;
		initAll();
		
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p);
		cbCategory=Ui.initAutoCB(p,mainFrame, "category",Book.TYPE.CATEGORY, idea.category,NEW);
		cbStatus=initStatus(p,idea.status);
		tfTheme=Ui.initTextField(p,"theme",32,idea.theme, !MANDATORY,!INFO);
		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (cbStatus.getSelectedIndex()<0) {
			errorMsg(cbStatus,"error.missing");
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		idea.setName(tfName.getText());
		idea.status=cbStatus.getSelectedIndex();
		if (cbCategory.getSelectedIndex()>0) idea.category=(Category)cbCategory.getSelectedItem();
		else idea.category=null;
		idea.theme=tfTheme.getText();
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@SuppressWarnings("null")
	void createScene() {
		if (taDescription.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					I18N.getMsg("z.error.empty.description"),
					I18N.getMsg("z.error"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		String newName=I18N.getColonMsg("idea")+idea.name;
		if (Scene.findName(book.scenes, newName)!=null) {
			JOptionPane.showMessageDialog(null,
					I18N.getMsg("z.error.exists"),
					I18N.getMsg("z.error"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		Scene scene=new Scene();
		scene.name=newName;
		scene.number=0;
		scene.writing=(taDescription.getText());
		scene.notes=(taNotes.getText());
		scene.id=(book.last_id++);
		scene.pov=(book.povs.get(0));
		cbStatus.setSelectedIndex(cbStatus.getSelectedIndex()+1);
		Container comp=this.getParent();
		while (!(comp instanceof Editor)) {
			comp=comp.getParent();
		}
		((Editor)comp).btOk.doClick();
		book.scenes.add(scene);
		mainFrame.getBookModel().setEntityNew(scene);
	}
	
}
