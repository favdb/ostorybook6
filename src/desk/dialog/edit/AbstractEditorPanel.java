/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.SbDate;
import db.entity.Scene;
import db.entity.Tag;
import db.I18N;
import db.util.EntityTool;
import desk.dialog.chooser.ColorChooserPanel;
import desk.dialog.chooser.DateChooser;
import desk.dialog.chooser.ImageChooserPanel;
import desk.panel.AbstractPanel;
import desk.tools.combobox.ComboLoad;
import desk.tools.FontUtil;
import static desk.tools.Ui.*;
import desk.tools.XeditorUtil;
import desk.tools.swing.SwingUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Objects;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public abstract class AbstractEditorPanel extends AbstractPanel {

	public AbstractEntity entity;
	public JTextField tfName;
	public JTabbedPane tabbed;
	public JTextArea taDescription;
	public JTextArea taNotes;
	public String msgError;
	public JTextArea taAssistant;
	private final Editor editor;
	public boolean modified=false;

	public AbstractEditorPanel(Editor m, AbstractEntity e) {
		super(m.getMainFrame());
		this.editor=m;
		this.entity=e;
		//App.trace("AbstractEditor("+entity.objtype+") name="+entity.name);
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
	}
	
	@SuppressWarnings("unchecked")
	public static JComboBox initStatus(JPanel panel, int sel) {
		JComboBox cb=new JComboBox();
		cb.setName("status");
		ComboLoad.loadCbStatus(cb, sel);
		addField(panel,"status","",cb,!GROW,!MANDATORY,null);
		return (cb);
	}

	public void initHeader(JPanel p) {
		//JPanel h = new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		tfName = initTextField(p, "z.name", 0, entity.name, MANDATORY, !INFO);
		JLabel lb=new JLabel(I18N.getColonMsg("z.date.creation") + entity.creation.getDate());
		lb.setFont(FontUtil.getItalic());
		p.add(lb,"span,split 2,al right");
		JLabel lb2=new JLabel(I18N.getColonMsg("z.date.maj") + entity.maj.getDate());
		lb2.setFont(FontUtil.getItalic());
		p.add(lb2);
		//p.add(h,"span,growx");
	}

	public void initMultiTab() {
		tabbed = new JTabbedPane();
		if (entity instanceof Scene) {
			SwingUtil.setForcedSize(tabbed, MAXIMUM_SIZE);
		}
		add(tabbed, "span,grow");
	}
	
	public EditorHtml initEditorHtml(JPanel panel, String title, String text, boolean toolBar, JTabbedPane tab) {
		EditorHtml ed = new EditorHtml(this, entity.getDescription(), toolBar,32768);
		SwingUtil.setForcedSize(ed, MAXIMUM_SIZE);
		addField(panel,title,"top",ed,GROW,!MANDATORY,tab);
		return(ed);
	}

	public void initDescription(JPanel panel, JTabbedPane tab) {
		taDescription=initTextArea(panel, "z.description", entity.getDescription(), 32768, !MANDATORY, !INFO, tab);
	}

	public void initNotes(JPanel panel, JTabbedPane tab) {
		taNotes=initTextArea(panel, "z.notes", entity.getNotes(), 32768, !MANDATORY, !INFO, tab);
	}

	public void initAssistant(JPanel panel, JTabbedPane tab) {
		taAssistant=initTextArea(panel, "assistant", entity.getAssistant(), 32768, !MANDATORY, INFO, tab);
	}

	public DateChooser initSbDate(JPanel p, String title, SbDate date, boolean isMandatory, boolean time) {
		DateChooser d = new DateChooser(mainFrame, time);
		d.setName(title);
		if (date!=null && date.toLong()!=0) d.setDate(date.toDate());
		addField(p,title,"top",d,!GROW,isMandatory,null);
		return (d);
	}

	public ImageChooserPanel initIconChooser(JPanel p, String title, String val) {
		Icon icon = IconUtil.getIcon("small/unknown");
		String name="";
		if (val != null && !val.isEmpty()) {
			icon = IconUtil.getIconExternal(val, new Dimension(16, 16));
		}
		ImageChooserPanel ic = new ImageChooserPanel(name, icon, val, true);
		if (p!=null) addField(p,title,"top",ic,!GROW,MANDATORY,null);
		return (ic);
	}

	public ImageChooserPanel initImageChooser(JPanel p, String title, String val) {
		Icon icon = IconUtil.getIcon(IconUtil.class,"large/photo", new Dimension(400, 300));
		String name=mainFrame.getImageDir();
		if (val != null && !val.isEmpty()) {
			icon = IconUtil.getIconExternal(val, new Dimension(400, 300));
		}
		ImageChooserPanel ic = new ImageChooserPanel(name, icon, val, false);
		addField(p,title,"top",ic,!GROW,MANDATORY,null);
		return (ic);
	}

	public ColorChooserPanel initColorChooser(JPanel p, String title, Integer val) {
		ColorChooserPanel ic = new ColorChooserPanel(I18N.getMsg("z.color.choose"), new Color(val));
		addField(p,title,"",ic,!GROW,!MANDATORY,null);
		return (ic);
	}

	public JTextField initFileChooser(JPanel panel, String title, String val) {
		JPanel p=new JPanel(new MigLayout("", "[][grow][][]", ""));
		JLabel l = new JLabel(I18N.getMsg(title));
		p.add(l);
		JTextField tfFile = new JTextField(30);
		tfFile.setName(title);
		tfFile.setText(val);
		p.add(tfFile);
		JButton btChooseFile = new JButton();
		btChooseFile.addActionListener((ActionEvent arg0) -> {
			JFileChooser fc = new JFileChooser(tfFile.getText());
			if (tfFile.getText().isEmpty()) {
				fc = new JFileChooser(mainFrame.xml.getPath());
			}
			fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int ret = fc.showOpenDialog(mainFrame);
			if (ret != JFileChooser.APPROVE_OPTION) {
				return;
			}
			File dir = fc.getSelectedFile();
			tfFile.setText(dir.getAbsolutePath());
		});
		//btChooseFile.setText(I18N.getMsg("libreoffice.open"));
		btChooseFile.setIcon(IconUtil.getIcon("small/open"));
		btChooseFile.setToolTipText(I18N.getMsg("file.open"));
		p.add(btChooseFile);
		JButton btResetFile = new JButton();
		btResetFile.addActionListener((ActionEvent arg0) -> {
			tfFile.setText(XeditorUtil.getDefaultFilePath(mainFrame, (Scene) entity));
		});
		if (tfFile.getText().isEmpty()) {
			btResetFile.setText(I18N.getMsg("file.create"));
		} else {
			btResetFile.setText(I18N.getMsg("file.reset"));
		}
		p.add(btResetFile);
		panel.add(p,"span");
		return (tfFile);
	}

	public abstract boolean verifier();

	public void verifierNumeric(JTextField tf, boolean isMandatory) {
		if (isMandatory && tf.getText().isEmpty()) {
			errorMsg(tf, "error.missing");
		} else if (!StringUtil.isNumeric(tf.getText())) {
			errorMsg(tf, "error.notNumeric");
		}
	}

	public void apply() {
		entity.maj=(SbDate.getToDay());
		if (taDescription!=null) entity.setDescription(taDescription.getText());
		else entity.setDescription("");
		if (taNotes!=null) entity.setNotes(taNotes.getText());
		else entity.setNotes("");
		if (taAssistant!=null) entity.setAssistant(taAssistant.getText());
		else entity.setAssistant("");
		if (entity.id==-1L) {
			entity.id=(++book.last_id);
			mainFrame.getBookModel().setEntityNew(entity);
		} else {
			mainFrame.getBookModel().setEntityUpdate(entity);
		}
		refresh();
	}

	public void resetError() {
		msgError = "";
		if (tfName.getText().isEmpty()) {
			errorMsg(tfName, "z.error.missing");
		}
		AbstractEntity s = EntityTool.findName(book,entity.type,tfName.getText());
		if (s!=null && s.type.toString().equals(entity.type.toString())) {
			if (entity.id!=null && !Objects.equals(s.id, entity.id)) {
				errorMsg(tfName, "z.error.exists");
			}
		}
	}

	public void errorMsg(JComponent comp, String msg) {
		msgError += I18N.getColonMsg(comp.getName()) + I18N.getMsg(msg) + "\n";
		comp.setBorder(new LineBorder(Color.red,1));
	}

	public void actionEntity(String compName, CheckBoxPanel cb) {
		//App.trace("AbstractEditor.actionEntity(" + compName + "," + cb.name + ")");
		if (compName == null) {
			return;
		}
		if (compName.startsWith("BtAdd")) {
			switch (cb.getName()) {
				case "categories":
					mainFrame.showEditorAsDialog(new Category());
					break;
				case "items":
					mainFrame.showEditorAsDialog(new Item());
					break;
				case "locations":
					mainFrame.showEditorAsDialog(new Location());
					break;
				case "persons":
					mainFrame.showEditorAsDialog(new Person());
					break;
				case "plots":
					mainFrame.showEditorAsDialog(new Plot());
					break;
				case "povs":
					mainFrame.showEditorAsDialog(new Pov());
					break;
				case "tags":
					mainFrame.showEditorAsDialog(new Tag());
					break;
			}
			cb.initAll();
		} else if (compName.startsWith("BtEdit")) {
			if (cb.getPointedEntity() == null) {
				return;
			}
			mainFrame.showEditorAsDialog(cb.getPointedEntity());
			cb.initAll();
		}
	}

	void tempSave() {
		//TODO temp save
	}
}
