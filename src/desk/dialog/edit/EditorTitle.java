/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Idea;
import desk.panel.AbstractPanel;
import desk.tools.FontUtil;
import desk.tools.swing.SwingUtil;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class EditorTitle extends AbstractPanel {
	private final Editor caller;

	public EditorTitle(Editor m, AbstractEntity entity) {
		super(m.getMainFrame());
		this.caller = m;
		initAll();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout("flowx, ins 2"));
		setBackground(new java.awt.Color(128, 228, 227));
		setOpaque(true);
		JLabel lbIcon = new JLabel(caller.entity.getIcon());
		add(lbIcon);
		String str=StringUtil.capitalize(caller.entity.type.toString());
		JLabel lbTitle = new JLabel(str);
		lbTitle.setFont(FontUtil.getBold());
		add(lbTitle,"growx");
		JButton center = SwingUtil.initButton("", "small/center", "center");
		center.setName("btCenter");
		center.setMnemonic(KeyEvent.VK_C);
		center.addActionListener((ActionEvent arg0) -> {
			caller.setLocationRelativeTo(null);
		});
		add(center, "align right");
		JButton ideaButton = SwingUtil.initButton("", "small/idea", "foi.new");
		ideaButton.addActionListener((ActionEvent arg0) -> {
			mainFrame.showEditorAsDialog(new Idea());
		});
		add(ideaButton, "align right");
		if (caller.entity instanceof Idea) {
			ideaButton.setVisible(false);
		}

	}

}
