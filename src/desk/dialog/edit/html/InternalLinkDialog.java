/*
 * Copyright (C) 2018 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit.html;

import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import lib.shef.ShefUtils;
import lib.shef.ui.dialog.HTMLOptionDialog;
import db.I18N;
import desk.app.MainFrame;

/**
 *
 * @author FaVdB
 */
public class InternalLinkDialog extends HTMLOptionDialog  {
	private static Icon icon = ShefUtils.getIconX32("linkinternal");
	private static String title = I18N.getMsg("shef.linkinternal");
	private static String desc = I18N.getMsg("shef.linkinternal_desc");
	private InternalLinkPanel linkPanel;
	private MainFrame mainFrame;

	public InternalLinkDialog(MainFrame m, Frame parent) {
		this(m, parent, title, desc, icon);
	}

	public InternalLinkDialog(MainFrame m, Dialog parent) {
		this(m, parent, title, desc, icon);
	}

	public InternalLinkDialog(MainFrame m, Dialog parent, String title, String desc, Icon ico) {
		super(parent, title, desc, ico);
		mainFrame=m;
		init();
	}

	public InternalLinkDialog(MainFrame m,Frame parent, String title, String desc, Icon ico) {
		super(parent, title, desc, ico);
		mainFrame=m;
		init();
	}

	void setLinkText(String linkText) {
		linkPanel.setLinkText(linkText);
	}

	public String getLinkText() {
		return linkPanel.getLinkText();
	}

	@Override
	public String getHTML() {
		if ("".equals(linkPanel.getUrl())) return(getLinkText());
		String html = "<a href=\"#chapscene";
		html += linkPanel.getUrl();
		html += "\">" + getLinkText() + "</a>";
		return html;
	}

	private void init() {
		linkPanel = new InternalLinkPanel(mainFrame);
		linkPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(linkPanel);
		setSize(315, 370);
		setResizable(false);
		this.pack();
	}
	
}
