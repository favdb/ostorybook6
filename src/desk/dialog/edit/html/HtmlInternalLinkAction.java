/*
 * Copyright (C) 2018 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit.html;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import static javax.swing.Action.MNEMONIC_KEY;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTML;
import lib.shef.ShefUtils;
import lib.shef.ui.text.html.HTMLUtils;
import lib.shef.ui.actions.HTMLTextEditAction;
import db.I18N;
import desk.app.MainFrame;

/**
 *
 * @author FaVdB
 */
public class HtmlInternalLinkAction extends HTMLTextEditAction {
	
	private static final long serialVersionUID = 1L;
	private final MainFrame mainFrame;

	public HtmlInternalLinkAction(MainFrame m) {
		super(I18N.getMsg("shef.linkinternal_"));
		mainFrame=m;
		putValue(MNEMONIC_KEY, (int) I18N.getMnemonic("shef.linkinternal_"));
		putValue(SMALL_ICON, ShefUtils.getIconX16("linkinternal"));
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		InternalLinkDialog dlg = createDialog(editor);
		if (dlg == null) {
			return;
		}

		if (editor.getSelectedText() != null) {
			dlg.setLinkText(editor.getSelectedText());
		}
		dlg.setLocationRelativeTo(dlg.getParent());
		dlg.setVisible(true);
		if (dlg.hasUserCancelled()) {
			return;
		}

		String tagText = dlg.getHTML();
		if (editor.getSelectedText() == null) {
			tagText += "&nbsp;";
		}

		editor.replaceSelection("");
		HTMLUtils.insertHTML(tagText, HTML.Tag.A, editor);
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
	}

	protected InternalLinkDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		InternalLinkDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new InternalLinkDialog(mainFrame, (Frame) w);
		} else if (w != null && w instanceof Dialog) {
			d = new InternalLinkDialog(mainFrame, (Dialog) w);
		}

		return d;
	}
}
