package desk.dialog.edit.html;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import static javax.swing.Action.SMALL_ICON;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTML;

import lib.shef.ShefUtils;
import lib.shef.ui.text.html.HTMLUtils;
import lib.shef.ui.actions.HTMLTextEditAction;
import lib.shef.ui.dialog.ImageDialog;

import db.I18N;
import desk.app.MainFrame;

@SuppressWarnings("serial")
public class HtmlImageAction extends HTMLTextEditAction {

	private final MainFrame mainFrame;

	public HtmlImageAction(MainFrame m) {
		super(I18N.getMsg("shef.image_"));
		mainFrame=m;
		putValue(SMALL_ICON, ShefUtils.getIconX16("image"));
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		ImageDialog d = createDialog(editor);
		d.setLocationRelativeTo(d.getParent());
		d.setVisible(true);
		if (d.hasUserCancelled()) return;
		editor.requestFocusInWindow();
		editor.replaceSelection(d.getHTML());
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		ImgDialog dlg = new ImgDialog(mainFrame,editor);
		dlg.setVisible(true);
		if (dlg.canceled) {
			return;
		}
		String tagText = dlg.getHTML();
		if (editor.getCaretPosition() == editor.getDocument().getLength()) tagText += "&nbsp;";

		editor.replaceSelection("");
		HTML.Tag tag = HTML.Tag.IMG;
		if (tagText.startsWith("<a")) tag = HTML.Tag.A;

		HTMLUtils.insertHTML(tagText, tag, editor);
	}

	protected ImageDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		ImageDialog d = null;
		if (w != null && w instanceof Frame)
			d = new ImageDialog((Frame) w);
		else if (w != null && w instanceof Dialog)
			d = new ImageDialog((Dialog) w);

		return d;
	}
}
