/*
 * Created on April 29, 2018
 *
 */
package desk.dialog.edit.html;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lib.shef.ui.text.html.TextEditPopupManager;
import db.I18N;
import db.entity.Chapter;
import db.entity.Scene;
import desk.app.MainFrame;
import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;

public class InternalLinkPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JPanel hlinkPanel = null;
	private JLabel urlLabel = null;
	private JLabel textLabel = null;
	private JComboBox urlField = null;
	private JTextField textField = null;
	private final MainFrame mainFrame;

	public InternalLinkPanel(MainFrame m) {
		super();
		mainFrame=m;
		initialize();
	}

	public void setLinkText(String text) {
		textField.setText(text);
	}

	public String getLinkText() {
		return textField.getText();
	}

	private void initialize() {
		this.setLayout(new BorderLayout(5, 5));
		this.setSize(328, 218);
		this.add(getHlinkPanel(), java.awt.BorderLayout.NORTH);

		TextEditPopupManager popupMan = TextEditPopupManager.getInstance();
		popupMan.registerJComboBox(urlField);
		popupMan.registerJTextComponent(textField);
	}

	private JPanel getHlinkPanel() {
		if (hlinkPanel == null) {
			GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
			gridBagConstraints3.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints3.gridy = 1;
			gridBagConstraints3.weightx = 1.0;
			gridBagConstraints3.gridx = 1;
			GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
			gridBagConstraints2.fill = java.awt.GridBagConstraints.HORIZONTAL;
			gridBagConstraints2.gridy = 0;
			gridBagConstraints2.weightx = 1.0;
			gridBagConstraints2.insets = new java.awt.Insets(0, 0, 5, 0);
			gridBagConstraints2.gridx = 1;
			GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
			gridBagConstraints1.gridx = 0;
			gridBagConstraints1.anchor = java.awt.GridBagConstraints.WEST;
			gridBagConstraints1.insets = new java.awt.Insets(0, 0, 0, 5);
			gridBagConstraints1.gridy = 1;
			textLabel = new JLabel();
			textLabel.setText(I18N.getMsg("shef.text"));
			GridBagConstraints gridBagConstraints = new GridBagConstraints();
			gridBagConstraints.gridx = 0;
			gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
			gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
			gridBagConstraints.gridy = 0;
			urlLabel = new JLabel();
			urlLabel.setText(I18N.getMsg("chapter")+"+"+I18N.getMsg("scene"));
			hlinkPanel = new JPanel();
			hlinkPanel.setBorder(javax.swing.
					BorderFactory.createCompoundBorder(
							BorderFactory.createTitledBorder(null, I18N.getMsg("shef.link"),
									TitledBorder.DEFAULT_JUSTIFICATION,
									TitledBorder.DEFAULT_POSITION, null, null),
							BorderFactory.createEmptyBorder(5, 5, 5, 5)));
			hlinkPanel.setLayout(new GridBagLayout());
			hlinkPanel.add(urlLabel, gridBagConstraints);
			hlinkPanel.add(textLabel, gridBagConstraints1);
			hlinkPanel.add(getUrlField(), gridBagConstraints2);
			hlinkPanel.add(getTextField(), gridBagConstraints3);
		}
		return hlinkPanel;
	}

	@SuppressWarnings("unchecked")
	private JComboBox getUrlField() {
		if (urlField == null) {
			List<StringChapScene> list=new ArrayList<>();
			List<Chapter> chapters = Chapter.orderByNumber(mainFrame.book.chapters);
			List<Scene> scenes = mainFrame.book.scenes;
			for (Chapter chapter:chapters) {
				list.add(new StringChapScene(chapter.id,0,chapter.number+":"+chapter.name));
				for (Scene scene : scenes) {
					if (scene.chapter == null) {
						continue;
					}
					if (scene.chapter.id.equals(chapter.id)) {
						list.add(new StringChapScene(scene.chapter.id,
							scene.id,"    "+(scene.getChapterSceneNo(true)+scene.name)));
					}
				}
			}
			urlField = new JComboBox(list.toArray());
			urlField.setMaximumRowCount(15);
		}
		return urlField;
	}

	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
		}
		return textField;
	}
	
	public String getUrl() {
		StringChapScene link=(StringChapScene)urlField.getModel().getSelectedItem();
		if (link==null) return("");
		return(link.getLink());
	}

	private static class StringChapScene {
		long chapterId, sceneId;
		String text;

		public StringChapScene(long c, long s, String t) {
			chapterId=c;
			sceneId=s;
			text=t;
		}
		
		public String getLink() {
			if (sceneId==0L) {
				return(Long.toString(chapterId));
			}
			return(Long.toString(chapterId)+"."+Long.toString(sceneId));
		}
		
		@Override
		public String toString() {
			return(text);
		}
	}

}
