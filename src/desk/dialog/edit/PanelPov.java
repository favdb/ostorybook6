/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Pov;
import desk.dialog.chooser.ColorChooserPanel;
import db.I18N;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import java.util.Objects;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class PanelPov extends AbstractEditorPanel {

	private final Pov pov;
	private JComboBox cbCategory;
	private JTextField tfBeacon;
	private JTextField tfAbbr;
	private ColorChooserPanel tfColor;
	private JTextField tfSort;

	public PanelPov(Editor m, AbstractEntity e) {
		super(m,e);
		pov=(Pov)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p,I18N.getMsg("pov"));
		tfAbbr=Ui.initTextField(p,"abbreviation",6,pov.getAbbreviation(),MANDATORY,!INFO);
		tfSort=Ui.initTextField(p,"sort",4,pov.getSort().toString(),MANDATORY,!INFO);
		tfColor=initColorChooser(p, "color", pov.getColor());
		initDescription(p,null);
		initNotes(p,null);
		//initAssistant(p,NEWTAB);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (tfAbbr.getText().isEmpty()) {
			errorMsg(tfAbbr,"error.missing");
		}
		if (tfSort.getText().equals("+")) {
		} else if (tfSort.getText().isEmpty()) {
			errorMsg(tfAbbr,"error.missing");
		} else if (!StringUtil.isNumeric(tfSort.getText())) {
			errorMsg(tfSort, "error.notnumeric");
		} else {
			int n=Integer.parseInt(tfSort.getText());
			Pov c=Pov.findSort(mainFrame.book.povs,n);
			if (c!=null && !Objects.equals(c.id, pov.id)) {
				errorMsg(tfSort, "error.exists");
			}
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		pov.setName(tfName.getText());
		pov.setAbbreviation(tfAbbr.getText());
		if (tfSort.getText().equals("+")) {
			pov.setSort(Pov.getNextSort(book.povs));
		} else pov.setSort(Integer.parseInt(tfSort.getText()));
		pov.setColor(tfColor.getColorToInteger());
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
