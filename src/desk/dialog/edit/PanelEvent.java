/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Event;
import desk.dialog.chooser.DateChooser;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelEvent extends AbstractEditorPanel {

	private final Event event;
	private JComboBox cbCategory;
	private DateChooser dDate;
	
	public PanelEvent(Editor m, AbstractEntity e) {
		super(m,e);
		event=(Event)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		cbCategory=Ui.initAutoCB(p,mainFrame,"category",Book.TYPE.CATEGORY, event.category,null,MANDATORY,NEW,EMPTY);
		dDate=initSbDate(p,"date",event.date, MANDATORY,TIME);
		add(p);
		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		JTextField tf=new JTextField();
		resetError();
		dDate.setBorder(tf.getBorder());
		if (dDate.hasError() || dDate.getDate()==null) {
			errorMsg(dDate,"error.date");
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		event.setName(tfName.getText());
		if (cbCategory.getSelectedIndex()==0) event.category=null;
		else event.category=((Category)cbCategory.getSelectedItem());
		event.date=dDate.getDate();
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
