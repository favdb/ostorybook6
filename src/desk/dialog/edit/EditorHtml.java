/*
 Storybook: Open Source software for novelists and authors.
 Copyright (C) 2008 - 2012 Martin Mustun

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import desk.app.Language.SPELLING;
import desk.app.pref.AppPref;
import desk.app.MainFrame;
import desk.dialog.edit.html.HtmlInternalLinkAction;
import desk.dialog.edit.html.HtmlImageAction;
import desk.panel.AbstractPanel;
import desk.panel.typist.Typist;
import desk.tools.FontUtil;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.BadLocationException;
import javax.swing.undo.UndoManager;
import lib.jortho.SpellChecker;
import lib.miginfocom.swing.MigLayout;
import lib.shef.ui.actions.DefaultAction;
import lib.shef.ShefUtils;
import lib.shef.ui.text.html.CompoundUndoManager;
import lib.shef.ui.text.html.Entities;
import lib.shef.ui.text.html.HTMLUtils;
import lib.shef.ui.text.html.IndentationFilter;
import lib.shef.ui.text.html.SourceCodeEditor;
import lib.shef.ui.text.html.WysiwygHTMLEditorKit;
import lib.shef.ui.actions.HTMLFontColorAction;
import lib.shef.ui.actions.HTMLInlineAction;
import lib.shef.ui.actions.HTMLLineBreakAction;
import lib.shef.ui.actions.HTMLLinkAction;
import lib.shef.ui.actions.HTMLTableAction;
import lib.shef.ui.actions.HTMLTextEditAction;
import lib.shef.ui.actions.SpecialCharAction;
import static java.awt.event.ActionEvent.CTRL_MASK;
import javax.swing.JToggleButton;
import lib.shef.ui.actions.HTMLAlignAction;
import lib.shef.ui.actions.HTMLBlockAction;
import lib.shef.ui.actions.HTMLCadratinAction;
import lib.shef.ui.actions.HTMLEditorActionFactory;
import novaworx.syntax.SyntaxFactory;
import novaworx.textpane.SyntaxDocument;
import novaworx.textpane.SyntaxGutter;
import novaworx.textpane.SyntaxGutterBase;
import org.bushe.swing.action.ActionList;
import org.bushe.swing.action.ActionManager;
import org.bushe.swing.action.ActionUIFactory;
import desk.tools.ExceptionDlg;
import tools.IconUtil;
import tools.TextUtil;
import tools.html.HtmlUtil;

@SuppressWarnings("serial")
public class EditorHtml extends JPanel {

	public static boolean TWOLINE = true, NO_TOOLBAR=false;

	private static final String INVALID_TAGS[] = {"html", "head", "body", "title", "style"};

	private int maxLength = 32768;
	private boolean showFullToolbar = true;

	private JEditorPane wysEditor;
	private SourceCodeEditor srcEditor;
	private JEditorPane focusedEditor;
	private JComboBox cbFontFamily;
	private JComboBox cbParagraph;
	private JToolBar formatToolBar;
	private JLabel lbMessage;

	private JPopupMenu wysPopupMenu, srcPopupMenu;

	private ActionList actionList;

	private final FocusListener focusHandler = new FocusHandler();
	private final DocumentListener textChangedHandler = new TextChangedHandler();
	private final ActionListener cbFontFamilyHandler = new FontChangeHandler();
	private final ActionListener cbParagraphHandler = new ParagraphComboHandler();
	private final CaretListener caretHandler = new CaretHandler();
	private final MouseListener popupHandler = new PopupHandler();

	private boolean isWysTextChanged;
	private final MainFrame mainFrame;
	private final boolean twoLineBar;
	private JPanel panel;
	private int objsize=0;
	private int inisize=0;
	private JProgressBar progress;
	private JToggleButton viewSrce;
	private JScrollPane psWys;
	private JScrollPane psSrc;

	public EditorHtml(MainFrame m) {
		this.showFullToolbar = false;
		this.twoLineBar = false;
		mainFrame = m;
		initUI();
	}

	public EditorHtml(MainFrame m, String text) {
		this.showFullToolbar = false;
		this.twoLineBar = false;
		mainFrame = m;
		initUI();
		setText(text);
	}

	public EditorHtml(MainFrame m, boolean fullToolbar, boolean twoLine) {
		this.showFullToolbar = fullToolbar;
		this.twoLineBar = twoLine;
		mainFrame = m;
		initUI();
	}

	public EditorHtml(MainFrame m, boolean showFullToolbar, boolean showHtml, boolean twoLineBar, int objsize, int inisize) {
		this.showFullToolbar = showFullToolbar;
		this.twoLineBar = twoLineBar;
		this.objsize=objsize;
		this.inisize=inisize;
		mainFrame = m;
		initUI();
	}

	public EditorHtml(AbstractEditorPanel p, String texte) {
		this.showFullToolbar = true;
		this.twoLineBar = true;
		mainFrame = p.mainFrame;
		initUI();
		this.setText(texte);
	}

	public EditorHtml(AbstractEditorPanel p, String texte, boolean fullToolBar, int len) {
		this.showFullToolbar = fullToolBar;
		this.twoLineBar = fullToolBar;
		this.mainFrame = p.mainFrame;
		this.objsize=len;
		initUI();
		this.setText(texte);
	}

	public EditorHtml(MainFrame p, String texte, boolean fullToolBar, int len) {
		this.showFullToolbar = fullToolBar;
		this.twoLineBar = fullToolBar;
		this.mainFrame = p;
		this.objsize=len;
		initUI();
		this.setText(texte);
	}

	public boolean getShowSimpleToolbar() {
		return showFullToolbar;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setCaretPosition(int pos) {
		if (!viewSrce.isSelected()) {
			wysEditor.setCaretPosition(pos);
			wysEditor.requestFocusInWindow();
		} else {
			srcEditor.setCaretPosition(pos);
			srcEditor.requestFocusInWindow();
		}
	}

	private void initUI() {
		initEditorPanel();
		initToolbar();
		setLayout(new MigLayout("fill,wrap", "", "[][grow][]"));
		if (this.showFullToolbar) add(formatToolBar,"span,growx");
		lbMessage = new JLabel("", JLabel.RIGHT);
		add(lbMessage, "shrink, pos null null 100% 100%");
		add(panel, "grow, id tabs");

		SwingUtilities.invokeLater(() -> {
			isWysTextChanged = false;
			wysEditor.requestFocusInWindow();
		});
	}

	private JButton initButton(Character accel, String name, ActionListener al) {
		String tips = I18N.getMsg("shef." + name);
		JButton bt = new JButton(ShefUtils.getIconX16(name));
		bt.setName(name);
		bt.setText(null);
		bt.setMnemonic(0);
		bt.setToolTipText(tips);
		bt.setMargin(new Insets(1, 1, 1, 1));
		bt.setMaximumSize(new Dimension(22, 22));
		bt.setMinimumSize(new Dimension(22, 22));
		bt.setPreferredSize(new Dimension(22, 22));
		bt.setFocusable(false);
		bt.setFocusPainted(false);
		if (accel != ' ') {
			bt.registerKeyboardAction(al,
				KeyStroke.getKeyStroke(accel, CTRL_MASK, false), JComponent.WHEN_IN_FOCUSED_WINDOW);
		}
		bt.addActionListener(al);
		return (bt);
	}

	/**
	 * Creation de la barre d'outil. La barre est composée de deux éléments:
	 * 1) une barre de base comprenant : le type de paragraphe, la famille de
	 * la police de caractères la taille de la police, le choix de la couleur.
	 * 2) si la barre complète est à afficher : gras, italique, souligné,
	 * liste à puces, liste ordonnée, cadre à gauche, cadrage au centre,
	 * cadrage à droite, justifié, insertion d'un lien, insertion d'une image
	 * insertion d'un tableau.
	 *
	 * @param blockActs
	 * @param fontSizeActs
	 */
	@SuppressWarnings({"unchecked"})
	private void initToolbar() {
		actionList = new ActionList("editor-actions");
		ActionList fontSizeActs = HTMLEditorActionFactory.createFontSizeActionList();
		formatToolBar = new JToolBar();
		formatToolBar.setFloatable(false);
		formatToolBar.setFocusable(false);
		formatToolBar.setLayout(new MigLayout("ins 0"));
		formatToolBar.setOpaque(false);
		
		// paragraphs
		ActionList paraActions = new ActionList("paraActions");
		ActionList lst = HTMLEditorActionFactory.createBlockElementActionList();
		actionList.addAll(lst);
		paraActions.addAll(lst);
		lst = HTMLEditorActionFactory.createListElementActionList();
		actionList.addAll(lst);
		paraActions.addAll(lst);
		formatToolBar.add(initParagraphCombo(paraActions));
		formatToolBar.addSeparator();
		
		// fonts family
		formatToolBar.add(initFontFamilyCombo());
		// font size
		final JButton fontSizeButton = new JButton(ShefUtils.getIconX16("fontsize"));
		fontSizeButton.setToolTipText(I18N.getMsg("shef.size"));
		final JPopupMenu sizePopup = ActionUIFactory.getInstance().createPopupMenu(fontSizeActs);
		ActionListener al = (ActionEvent e) -> {
			sizePopup.show(fontSizeButton, 0, fontSizeButton.getHeight());
		};
		fontSizeButton.addActionListener(al);
		configToolbarButton(fontSizeButton);
		formatToolBar.add(fontSizeButton);
		
		// font color
		Action actColor = new HTMLFontColorAction();
		actionList.add(actColor);
		formatToolBar.add(initButton(' ', "color", actColor));

		// view HTML code if wiew Wysiwyg only
		formatToolBar.addSeparator();
		viewSrce=new JToggleButton(IconUtil.getIcon("small/source"));
		viewSrce.setToolTipText(I18N.getMsg("shef.source"));
		viewSrce.addActionListener((ActionEvent evt) -> {
			if (viewSrce.isSelected()) {
				panel.removeAll();
				panel.add(psSrc);
			} else {
				panel.removeAll();
				panel.add(psWys);				
			}
			updateEditView();
			repaint();
		});
		formatToolBar.add(viewSrce,"span, al right");

		if (twoLineBar) {
			formatToolBar.add(new JLabel(""),"wrap");
		}
		
		// bold
		Action actBold = new HTMLInlineAction(HTMLInlineAction.BOLD);
		actBold.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actBold);
		formatToolBar.add(initButton('B', "bold", actBold),"span, split 30");

		// italic
		Action actItalic = new HTMLInlineAction(HTMLInlineAction.ITALIC);
		actItalic.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actItalic);
		formatToolBar.add(initButton('I', "italic", actItalic));

		// underline
		Action actUnderline = new HTMLInlineAction(HTMLInlineAction.UNDERLINE);
		actUnderline.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actUnderline);
		formatToolBar.add(initButton('U', "underline", actUnderline));

		// strike
		Action actStrike = new HTMLInlineAction(HTMLInlineAction.STRIKE);
		actStrike.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actStrike);
		formatToolBar.add(initButton('K', "strike", actStrike));

		// subscript
		Action actSubscript = new HTMLInlineAction(HTMLInlineAction.SUB);
		actSubscript.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actSubscript);
		formatToolBar.add(initButton(' ', "subscript", actSubscript));

		// superscript
		Action actSuperscript = new HTMLInlineAction(HTMLInlineAction.SUP);
		actSuperscript.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actSuperscript);
		formatToolBar.add(initButton('S', "superscript", actSuperscript));

		formatToolBar.addSeparator();

		// ul and ol
		Action actUL=new HTMLBlockAction(HTMLBlockAction.UL);
		actUL.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actUL);
		formatToolBar.add(initButton(' ', "listunordered", actUL));
		actUL=new HTMLBlockAction(HTMLBlockAction.OL);
		actUL.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actUL);
		formatToolBar.add(initButton(' ', "listordered", actUL));

		formatToolBar.addSeparator();

		// alignement (left, center, right, justify)
		Action actAlign=new HTMLAlignAction(HTMLAlignAction.LEFT);
		actAlign.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actAlign);
		formatToolBar.add(initButton('L', "al_left", actAlign));
		actAlign=new HTMLAlignAction(HTMLAlignAction.CENTER);
		actAlign.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actAlign);
		formatToolBar.add(initButton(' ', "al_center", actAlign));
		actAlign=new HTMLAlignAction(HTMLAlignAction.RIGHT);
		actAlign.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actAlign);
		formatToolBar.add(initButton('R', "al_right", actAlign));
		actAlign=new HTMLAlignAction(HTMLAlignAction.JUSTIFY);
		actAlign.putValue(ActionManager.BUTTON_TYPE, ActionManager.BUTTON_TYPE_VALUE_TOGGLE);
		actionList.add(actAlign);
		formatToolBar.add(initButton('J', "al_justify", actAlign));

		formatToolBar.addSeparator();

		// link, image, table
		Action actLinkInternal = new HtmlInternalLinkAction(mainFrame);
		actionList.add(actLinkInternal);
		formatToolBar.add(initButton(' ', "linkinternal", actLinkInternal));

		Action actLink = new HTMLLinkAction();
		actionList.add(actLink);
		formatToolBar.add(initButton(' ', "link", actLink));

		Action actImage = new HtmlImageAction(mainFrame);
		actionList.add(actImage);
		formatToolBar.add(initButton(' ', "image", actImage));

		Action actTable = new HTMLTableAction();
		actionList.add(actTable);
		formatToolBar.add(initButton(' ', "table", actTable));

		//line break
		Action actLineBreak = new HTMLLineBreakAction();
		actionList.add(actLineBreak);
		formatToolBar.add(initButton(' ', "line_break", actLineBreak));

		formatToolBar.addSeparator();

		//unicode
		Action actSpecChar = new SpecialCharAction();
		actionList.add(actSpecChar);
		formatToolBar.add(initButton(' ', "unicode", actSpecChar));

		//cadratin
		Action actCadratin = new HTMLCadratinAction(0);
		actionList.add(actCadratin);
		formatToolBar.add(initButton(' ', "cadratin", actCadratin));

		//french double-quote open
		actCadratin = new HTMLCadratinAction(1);
		actionList.add(actCadratin);
		formatToolBar.add(initButton(' ', "dialog_open_fr", actCadratin));

		//french double-quote close
		actCadratin = new HTMLCadratinAction(2);
		actionList.add(actCadratin);
		formatToolBar.add(initButton(' ', "dialog_close_fr", actCadratin));

		//english double-quote open
		actCadratin = new HTMLCadratinAction(3);
		actionList.add(actCadratin);
		formatToolBar.add(initButton(' ', "dialog_open", actCadratin));

		//english double-quote close
		actCadratin = new HTMLCadratinAction(4);
		actionList.add(actCadratin);
		formatToolBar.add(initButton(' ', "dialog_close", actCadratin));

	}

	@SuppressWarnings("unchecked")
	private JComboBox initParagraphCombo(ActionList blockActs) {
		PropertyChangeListener propLst = (PropertyChangeEvent evt) -> {
			if (evt.getPropertyName().equals("selected")) {
				if (evt.getNewValue().equals(Boolean.TRUE)) {
					cbParagraph.removeActionListener(cbParagraphHandler);
					cbParagraph.setSelectedItem(evt.getSource());
					cbParagraph.addActionListener(cbParagraphHandler);
				}
			}
		};
		for (Object o: blockActs) {
			if (o instanceof DefaultAction) {
				((DefaultAction) o).addPropertyChangeListener(propLst);
			}
		}
		cbParagraph = new JComboBox(toArray(blockActs));
		cbParagraph.setFont(new Font("Dialog", Font.PLAIN, 11));
		cbParagraph.addActionListener(cbParagraphHandler);
		cbParagraph.setRenderer(new ParagraphComboRenderer());
		cbParagraph.setToolTipText(I18N.getMsg("shef.style"));
		return(cbParagraph);
	}

	@SuppressWarnings("unchecked")
	private JComboBox initFontFamilyCombo() {
		List<String> fonts = new ArrayList<>();
		fonts.add("Default");
		fonts.add("serif");
		fonts.add("sans-serif");
		fonts.add("monospaced");
		GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		fonts.addAll(Arrays.asList(gEnv.getAvailableFontFamilyNames()));

		cbFontFamily = new JComboBox(fonts.toArray());
		cbFontFamily.setFont(new Font("Dialog", Font.PLAIN, 11));
		cbFontFamily.addActionListener(cbFontFamilyHandler);
		cbFontFamily.setToolTipText(I18N.getMsg("shef.font"));
		return(cbFontFamily);
	}

	/**
	 * Converts an action list to an array. Any of the null "separators" or
	 * sub ActionLists are omitted from the array.
	 *
	 * @param lst
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Action[] toArray(ActionList lst) {
		List acts = new ArrayList();
		for (Iterator it = lst.iterator(); it.hasNext();) {
			Object v = it.next();
			if (v != null && v instanceof Action) {
				acts.add(v);
			}
		}

		return (Action[]) acts.toArray(new Action[acts.size()]);
	}

	private void configToolbarButton(AbstractButton button) {
		button.setText(null);
		button.setMnemonic(0);
		button.setMargin(new Insets(1, 1, 1, 1));
		button.setMaximumSize(new Dimension(22, 22));
		button.setMinimumSize(new Dimension(22, 22));
		button.setPreferredSize(new Dimension(22, 22));
		button.setFocusable(false);
		button.setFocusPainted(false);
		Action a = button.getAction();
		if (a != null) {
			button.setToolTipText(a.getValue(Action.NAME).toString());
		}
	}

	private void initEditorPanel() {
		wysEditor = createWysiwygEditor();
		psWys=new JScrollPane(wysEditor);
		
		srcEditor = createSourceEditor();
		srcEditor.setBackground((new JEditorPane().getBackground()));
		srcEditor.setForeground(Color.black);
		psSrc = new JScrollPane(srcEditor);
		SyntaxGutter gutter = new SyntaxGutter(srcEditor);
		SyntaxGutterBase gutterBase = new SyntaxGutterBase(gutter);
		psSrc.setRowHeaderView(gutter);
		psSrc.setCorner(ScrollPaneConstants.LOWER_LEFT_CORNER, gutterBase);

		panel = new JPanel(new MigLayout());
		panel.add(psWys, "span, grow");
	}

	private SourceCodeEditor createSourceEditor() {
		SourceCodeEditor ed = new SourceCodeEditor();
		SwingUtil.setForcedSize(ed, SwingUtil.getScreenSize());
		SyntaxDocument doc = new SyntaxDocument();
		doc.setSyntax(SyntaxFactory.getSyntax("html"));
		CompoundUndoManager cuh = new CompoundUndoManager(doc, new UndoManager());

		doc.addUndoableEditListener(cuh);
		doc.setDocumentFilter(new IndentationFilter());
		doc.addDocumentListener(textChangedHandler);
		ed.setDocument(doc);
		ed.addFocusListener(focusHandler);
		ed.addCaretListener(caretHandler);
		ed.addMouseListener(popupHandler);

		return ed;
	}

	private JEditorPane createWysiwygEditor() {
		JEditorPane ed = new JEditorPane();

		ed.setEditorKitForContentType("text/html", new WysiwygHTMLEditorKit());
		ed.setContentType("text/html");
		SwingUtil.setForcedSize(ed, SwingUtil.getScreenSize());
		insertHTML(ed, "<p></p>", 0);
		
		ed.addCaretListener(caretHandler);
		ed.addFocusListener(focusHandler);
		// spell checker, must be before the popup handler
		String spelling = mainFrame.getPref().getString(AppPref.Key.SPELLING, SPELLING.none.name());
		if (!SPELLING.none.name().equals(spelling)) {
			SpellChecker.register(ed);
		}

		ed.addMouseListener(popupHandler);

		HTMLDocument document = (HTMLDocument) ed.getDocument();
		CompoundUndoManager cuh = new CompoundUndoManager(document, new UndoManager());
		document.addUndoableEditListener(cuh);
		document.addDocumentListener(textChangedHandler);

		isWysTextChanged = false;
		
		return ed;
	}

	// inserts html into the wysiwyg editor
	private void insertHTML(JEditorPane editor, String html, int location) {
		String topText = "<html><body "
			+ "style=\"f"
			+ "font-family: " +  FontUtil.getFontName(mainFrame.getPref().getString(AppPref.Key.FONT_EDITOR)) + "; "
			+ "font-size: " + FontUtil.getFontSize(mainFrame.getPref().getString(AppPref.Key.FONT_EDITOR)) + "pt;"
			+ "\">"
			+ removeInvalidTags(html)+"</body></html>";
		try {
			HTMLEditorKit kit = (HTMLEditorKit) editor.getEditorKit();
			Document doc = editor.getDocument();
			StringReader reader = new StringReader(HTMLUtils.jEditorPaneizeHTML(topText));
			kit.read(reader, doc, location);
		} catch (IOException | BadLocationException e) {
			ExceptionDlg.show("", e);
		}
	}

	// called when changing tabs
	private void updateEditView() {
		if (!viewSrce.isSelected()) {
			String topText = removeInvalidTags(srcEditor.getText());
			wysEditor.setText("");
			insertHTML(wysEditor, topText, 0);
			CompoundUndoManager.discardAllEdits(wysEditor.getDocument());
		} else {
			String topText = removeInvalidTags(wysEditor.getText());
			if (isWysTextChanged || srcEditor.getText().equals("")) {
				String t = deIndent(removeInvalidTags(topText));
				t = Entities.HTML40.unescapeUnknownEntities(t);
				srcEditor.setText(t);
			}
			CompoundUndoManager.discardAllEdits(srcEditor.getDocument());
		}
		isWysTextChanged = false;
		cbParagraph.setEnabled(!viewSrce.isSelected());
		cbFontFamily.setEnabled(!viewSrce.isSelected());
		updateState();
	}

	public void setText(String text) {
		String topText = removeInvalidTags(text);
		wysEditor.setText("");
		insertHTML(wysEditor, topText, 0);
		CompoundUndoManager.discardAllEdits(wysEditor.getDocument());

		String t = deIndent(removeInvalidTags(topText));
		t = Entities.HTML40.unescapeUnknownEntities(t);
		srcEditor.setText(t);
		CompoundUndoManager.discardAllEdits(srcEditor.getDocument());

		isWysTextChanged = false;
	}

	public String getText() {
		String topText = "";
		// return only body content
		try {
			if (!viewSrce.isSelected()) {
				HTMLDocument doc = (HTMLDocument) wysEditor.getDocument();
				topText = HtmlUtil.getContent(doc);
				topText = removeInvalidTags(topText);
			} else {
				topText = removeInvalidTags(srcEditor.getText());
				topText = deIndent(removeInvalidTags(topText));
				topText = Entities.HTML40.unescapeUnknownEntities(topText);
			}
		} catch (Exception e) {
			ExceptionDlg.show("", e);
		}
		return topText;
	}

	private String deIndent(String html) {
		String ws = "\n    ";
		StringBuilder sb = new StringBuilder(html);
		while (sb.indexOf(ws) != -1) {
			int s = sb.indexOf(ws);
			int e = s + ws.length();
			sb.delete(s, e);
			sb.insert(s, "\n");
		}
		return sb.toString();
	}

	private String removeInvalidTags(String html) {
		for (String invalid_tag : INVALID_TAGS) {
			html = deleteOccurance(html, '<' + invalid_tag + '>');
			html = deleteOccurance(html, "</" + invalid_tag + '>');
		}
		return html.trim();
	}

	private String deleteOccurance(String text, String word) {
		StringBuilder sb = new StringBuilder(text);
		int p;
		while ((p = sb.toString().toLowerCase().indexOf(word.toLowerCase())) != -1) {
			sb.delete(p, p + word.length());
		}
		return sb.toString();
	}

	private void updateState() {
		if (focusedEditor == wysEditor) {
			cbFontFamily.removeActionListener(cbFontFamilyHandler);
			String fontName = HTMLUtils.getFontFamily(wysEditor);
			if (fontName == null) {
				cbFontFamily.setSelectedIndex(0);
			} else {
				cbFontFamily.setSelectedItem(fontName);
			}
			cbFontFamily.addActionListener(cbFontFamilyHandler);
		}
		actionList.putContextValueForAll(HTMLTextEditAction.EDITOR, focusedEditor);
		actionList.updateEnabledForAll();
	}

	public boolean isTextChanged() {
		return (isWysTextChanged);
	}

	AbstractPanel callback=null;
	public void setCallback(AbstractPanel panel) {
		callback=panel;
	}
	
	int nbChange=0;
	public void checkCallback() {
		if (nbChange>20) {
			Container p = this.getParent();
			if (callback instanceof Typist) {
				((Typist)callback).tempSave();
				nbChange=0;
			}
			if (callback instanceof AbstractEditorPanel) {
				((AbstractEditorPanel)callback).tempSave();
				nbChange=0;
			}
		}
		nbChange++;
	}

	private class CaretHandler implements CaretListener {

		@Override
		public void caretUpdate(CaretEvent e) {
			if (maxLength > 0) {
				String t=getText();
				int len = maxLength - t.length() - 1;
				if (len < 0) {
					lbMessage.setForeground(Color.red);
				} else if (len < 100) {
					lbMessage.setForeground(Color.orange);
				} else {
					lbMessage.setForeground(Color.black);
				}
				String fmt=String.format("%s %d/%d, %s %d", I18N.getColonMsg("editor.letters"),
						t.length(),maxLength,I18N.getColonMsg("editor.words"),
						TextUtil.countWords(t));
				lbMessage.setText(fmt);
				if (objsize>0) {
					int x=(inisize+HtmlUtil.htmlToText(getText()).length());
					if (x>0 && progress!=null) progress.setValue(x);
				}
			}
			updateState();
		}
	}

	private class PopupHandler extends MouseAdapter {

		@Override
		public void mousePressed(MouseEvent e) {
			checkForPopupTrigger(e);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			checkForPopupTrigger(e);
		}

		private void checkForPopupTrigger(MouseEvent e) {
			if (e.isPopupTrigger()) {
				JPopupMenu p;
				if (e.getSource() == wysEditor) {
					p = wysPopupMenu;
				} else if (e.getSource() == srcEditor) {
					p = srcPopupMenu;
				} else {
					return;
				}
				p.show(e.getComponent(), e.getX(), e.getY());
			}
		}
	}

	private class FocusHandler implements FocusListener {

		@Override
		public void focusGained(FocusEvent e) {
			if (e.getComponent() instanceof JEditorPane) {
				JEditorPane ed = (JEditorPane) e.getComponent();
				CompoundUndoManager.updateUndo(ed.getDocument());
				focusedEditor = ed;
				updateState();
			}
		}

		@Override
		public void focusLost(FocusEvent e) {

			if (e.getComponent() instanceof JEditorPane) {
			}
		}
	}

	private class TextChangedHandler implements DocumentListener {

		@Override
		public void insertUpdate(DocumentEvent e) {
			textChanged();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			textChanged();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			textChanged();
		}

		private void textChanged() {
			isWysTextChanged = true;
			checkCallback();
		}
	}

	private class ParagraphComboHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == cbParagraph) {
				Action a = (Action) (cbParagraph.getSelectedItem());
				a.actionPerformed(e);
			}
		}
	}

	private class ParagraphComboRenderer extends DefaultListCellRenderer {

		private static final long serialVersionUID = 1L;

		@Override
		public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
			if (value instanceof Action) {
				value = ((Action) value).getValue(Action.NAME);
			}

			return super.getListCellRendererComponent(list, value, index,
				isSelected, cellHasFocus);
		}
	}

	private class FontChangeHandler implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == cbFontFamily && focusedEditor == wysEditor) {
				// MutableAttributeSet tagAttrs = new SimpleAttributeSet();
				HTMLDocument document = (HTMLDocument) focusedEditor.getDocument();
				CompoundUndoManager.beginCompoundEdit(document);

				if (cbFontFamily.getSelectedIndex() != 0) {
					HTMLUtils.setFontFamily(wysEditor, cbFontFamily.getSelectedItem().toString());
				} else {
					HTMLUtils.setFontFamily(wysEditor, null);
				}
				CompoundUndoManager.endCompoundEdit(document);
			}
		}

		public void itemStateChanged(ItemEvent e) {
		}
	}
}
