/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Location;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelLocation extends AbstractEditorPanel {

	private final Location location;
	private JTextField tfAddress;
	private JTextField tfAltitude;
	private JTextField tfGps;
	private JComboBox cbSup;
	private JComboBox cbCity;
	private JComboBox cbCountry;
	private CheckBoxPanel lsPhotos;

	public PanelLocation(Editor m, AbstractEntity e) {
		super(m,e);
		location=(Location)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		tabbed.add(p);
		initHeader(p);
		cbSup=Ui.initAutoCB(p,mainFrame,"z.sup",Book.TYPE.LOCATION, location.sup,location,!MANDATORY,!NEW,EMPTY);
		tfAddress = Ui.initTextField(p, "location.address", 32, location.address, !MANDATORY,!INFO);
		cbCity=Ui.initAutoCB(p, "location.city", Location.findCities(mainFrame.book), location.city,!MANDATORY,EMPTY);
		cbCountry=Ui.initAutoCB(p,"location.country",Location.findCountries(mainFrame.book), location.country,!MANDATORY,EMPTY);
		tfAltitude = Ui.initTextField(p, "location.altitude", 5, location.address, !MANDATORY,!INFO);
		tfGps = Ui.initTextField(p, "location.gps", 10, location.address, !MANDATORY,!INFO);
		lsPhotos = Ui.initListBox(null,mainFrame, "photos", null, location.photos,!MANDATORY,tabbed);

		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		location.setName(tfName.getText());
		if (cbSup.getSelectedItem()==null || cbSup.getSelectedIndex()==0) {
			location.sup=(null);
		} else {
			location.sup=((Location)cbSup.getSelectedItem());
		}
		location.address=(tfAddress.getText());
		location.city=((String)cbCity.getSelectedItem());
		location.country=((String)cbCountry.getSelectedItem());
		location.altitude=(Integer.getInteger(tfAltitude.getText()));
		location.gps=(tfGps.getText());
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
