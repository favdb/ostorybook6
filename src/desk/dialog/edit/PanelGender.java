/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Gender;
import desk.dialog.chooser.ImageChooserPanel;
import db.I18N;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class PanelGender extends AbstractEditorPanel {

	private final Gender gender;
	private JTextField tfChildhood;
	private JTextField tfAdolescence;
	private JTextField tfAdulthood;
	private JTextField tfRetirement;
	private JTextField tfSort;
	private ImageChooserPanel tfIcon;

	public PanelGender(Editor m, AbstractEntity e) {
		super(m,e);
		gender=(Gender)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p);
		tfSort=Ui.initTextField(p,"sort",5,gender.sort,MANDATORY,!INFO);
		tfChildhood=initTextField(p,"gender.childhood",5,gender.childhood, !MANDATORY,!INFO);
		tfAdolescence=initTextField(p,"gender.adolescence",5,gender.adolescence, !MANDATORY,!INFO);
		tfAdulthood=initTextField(p,"gender.adulthood",5,gender.adulthood, !MANDATORY,!INFO);
		tfRetirement=initTextField(p,"gender.retirement",5,gender.retirement, !MANDATORY,!INFO);
		tfIcon=initIconChooser(p,"icon.file",gender.iconFile);

		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (tfSort.getText().isEmpty()) {
			errorMsg(tfSort, "error.missing");
		} else if (tfSort.getText().equals("+")) {
		} else if (!StringUtil.isNumeric(tfSort.getText())) {
			errorMsg(tfSort, "error.notnumeric");
		}
		if (!StringUtil.isNumeric(tfChildhood.getText())) {
			errorMsg(tfChildhood, "error.notNumeric");
		}
		if (!StringUtil.isNumeric(tfAdolescence.getText())) {
			errorMsg(tfAdolescence, "error.notNumeric");
		}
		if (!StringUtil.isNumeric(tfAdulthood.getText())) {
			errorMsg(tfAdulthood, "error.notNumeric");
		}
		if (!StringUtil.isNumeric(tfRetirement.getText())) {
			errorMsg(tfRetirement, "error.notNumeric");
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		gender.setName(tfName.getText());
		if (tfSort.getText().equals("+")) {
			gender.sort=(Gender.getNextSort(book.genders));
		} else gender.sort=(Integer.parseInt(tfSort.getText()));
		gender.childhood=(Integer.parseInt(tfChildhood.getText()));
		gender.adolescence=(Integer.parseInt(tfAdolescence.getText()));
		gender.adulthood=(Integer.parseInt(tfAdulthood.getText()));
		gender.retirement=(Integer.parseInt(tfRetirement.getText()));
		gender.iconFile=(tfIcon.getIconFile());
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
