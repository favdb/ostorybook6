/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Chapter;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import desk.assistant.AssistantDlg;
import desk.dialog.AbstractDialog;
import desk.tools.FontUtil;
import desk.tools.swing.SwingUtil;
import db.I18N;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class Editor extends AbstractDialog {

	public static boolean show(MainFrame frame, AbstractEntity entity, AbstractDialog dlg) {
		Editor d = new Editor(frame, entity, dlg);
		d.setVisible(true);
		return (d.canceled);
	}

	public AbstractEntity entity;
	public AbstractEditorPanel panel;
	private final AbstractDialog dlg;

	public Editor(MainFrame frame, AbstractEntity entity, AbstractDialog dlg) {
		super(frame);
		this.entity = entity;
		this.dlg = dlg;
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		App.trace("Editor.initUi()");
		boolean modless = mainFrame.bookPref.isEditorModless();
		if (modless) {
			setModalityType(Dialog.ModalityType.MODELESS);
			setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		} else {
			this.setModal(true);
		}
		/*Dimension size = SwingUtil.getDlgSize(mainFrame,entity);
		if (size != null) {
			setSize(size.height, size.width);
		} else {
			setSize(mainFrame.getWidth() / 2, 680);
		}*/
		//setSize(new Dimension(800,600));
		setTitle(I18N.getMsg("editor") + " - " + I18N.getMsg(entity.type.toString()));
		setLayout(new MigLayout("flowx, ins 2"));
		setBackground(new java.awt.Color(128, 228, 227));
		JPanel t = new JPanel(new MigLayout("", "[][grow][][]", ""));
		t.setBackground(new java.awt.Color(128, 228, 227));
		JLabel lbIcon = new JLabel(entity.getIcon());
		t.add(lbIcon);
		String str =/*"<html><body><h3>"+*/ StringUtil.capitalize(entity.type.toString())/*+"</h3></body></html>"*/;
		JLabel lbTitle = new JLabel(str);
		lbTitle.setFont(FontUtil.getBold());
		t.add(lbTitle, "growx");
		JButton center = SwingUtil.initButton("", "small/center", "center");
		center.setName("btCenter");
		center.setMnemonic(KeyEvent.VK_C);
		center.addActionListener((ActionEvent arg0) -> {
			setLocationRelativeTo(null);
		});
		t.add(center, "align right");
		JButton ideaButton = SwingUtil.initButton("", "small/idea", "foi.new");
		ideaButton.addActionListener((ActionEvent arg0) -> {
			mainFrame.showEditorAsDialog(new Idea());
		});
		if (entity instanceof Idea) {
			ideaButton.setVisible(false);
		}
		t.add(ideaButton, "align right");
		add(t, "align center, growx, wrap");
		initPanel();
		if (panel==null) {
			App.error("Editor.initUi panel is nul, no entity type found");
			return;
		} else {
			add(panel, "grow,wrap");
		}
		initFooter();
		pack();
		setLocationRelativeTo(null);
	}

	public void initPanel() {
		App.trace("Editor.initPanel()");
		switch (Book.getTYPE(entity)) {
			case CATEGORY:
				panel = new PanelCategory(this, entity);
				break;
			case CHAPTER:
				panel = new PanelChapter(this, entity);
				break;
			case EVENT:
				panel = new PanelEvent(this, entity);
				break;
			case GENDER:
				panel = new PanelGender(this, entity);
				break;
			case IDEA:
				panel = new PanelIdea(this, entity);
				break;
			case ITEM:
				panel = new PanelItem(this, entity);
				break;
			case LOCATION:
				panel = new PanelLocation(this, entity);
				break;
			case MEMO:
				panel = new PanelMemo(this, entity);
				break;
			case PART:
				panel = new PanelPart(this, entity);
				break;
			case PERSON:
				panel = new PanelPerson(this, entity);
				break;
			case PHOTO:
				panel = new PanelPhoto(this, entity);
				break;
			case PLOT:
				panel = new PanelPlot(this, entity);
				break;
			case POV:
				panel = new PanelPov(this, entity);
				break;
			case RELATION:
				panel = new PanelRelation(this, entity);
				break;
			case SCENE:
				panel = new PanelScene(this, entity);
				break;
			case TAG:
				panel = new PanelTag(this, entity);
				break;
		}
	}

	public void initFooter() {
		App.trace("Editor.initFooter()");
		JPanel p = new JPanel(new MigLayout());
		if ((entity instanceof Chapter
				|| entity instanceof Scene
				|| entity instanceof Plot
				|| entity instanceof Person
				|| entity instanceof Location
				|| entity instanceof Item)) {
			JButton btAssistant = new JButton(I18N.getMsg("assistant"));
			btAssistant.setIcon(IconUtil.getIcon("small/target"));
			btAssistant.addActionListener((ActionEvent evt) -> {
				String origin = entity.getAssistant();
				String r = AssistantDlg.show(this, entity, panel.taAssistant.getText());
				if (origin.equals(r)) {
					return;
				}
				panel.taAssistant.setText(r);
				panel.modified=true;
				revalidate();
			});
			p.add(btAssistant);
		}
		if (entity instanceof Idea) {
			JButton btIdea = new JButton();
			btIdea.setText(I18N.getMsg("idea.to_scene"));
			btIdea.setIcon(IconUtil.getIcon("small/scene"));
			btIdea.addActionListener((ActionEvent evt) -> {
				PanelIdea pidea = (PanelIdea) panel;
				pidea.createScene();
			});
			add(btIdea, "span, split 4, right");
		}
		JButton btUpdate = new JButton();
		btUpdate.setText(I18N.getMsg("update"));
		btUpdate.setIcon(IconUtil.getIcon("small/refresh"));
		btUpdate.addActionListener((ActionEvent evt) -> {
			if (!panel.verifier()) {
				showError();
				return;
			}
			panel.apply();
			canceled = false;
			SwingUtil.saveDlgPosition(getThis(), entity);
			//dispose();
			mainFrame.getBookController().updateEntity(entity);
			mainFrame.setTitle();
		});
		p.add(btUpdate);
		p.add(buttonOk());
		p.add(buttonCancel());
		add(p, "right");
	}

	public Editor getThis() {
		return (this);
	}

	@Override
	public AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!panel.verifier()) {
					showError();
					return;
				}
				panel.apply();
				canceled = false;
				SwingUtil.saveDlgPosition(getThis(), entity);
				dispose();
				mainFrame.getBookController().updateEntity(entity);
				mainFrame.setTitle();
			}
		};
	}

	public void showError() {
		JOptionPane.showMessageDialog(this, panel.msgError,
				I18N.getMsg(entity.type.toString()) + " " + I18N.getMsg("z.error"),
				JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
