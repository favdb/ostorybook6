/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Photo;
import db.entity.Plot;
import db.entity.Scene;
import db.entity.Tag;
import desk.dialog.chooser.DateChooser;
import desk.tools.swing.SwingUtil;
import db.I18N;
import db.entity.Book;
import db.entity.Chapter;
import db.entity.Pov;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import desk.tools.markdown.Markdown;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class PanelScene extends AbstractEditorPanel {

	private final Scene scene;
	private JTextField tfNumber;
	private JComboBox cbChapter;
	private JComboBox cbPov;
	private DateChooser dDate;
	private JComboBox cbRelScene;
	private JTextField tfRel;
	private JComboBox cbStatus;
	private JComboBox cbNarrator;
	private JTextField xfile;
	private CheckBoxPanel lsLocations;
	private CheckBoxPanel lsItems;
	private CheckBoxPanel lsTags;
	private CheckBoxPanel lsPersons;
	private CheckBoxPanel lsPlots;
	private CheckBoxPanel lsPhotos;
	private JCheckBox cbInformative;
	private EditorHtml taTexte;
	private JCheckBox ckDate;
	private JPanel groupDate;
	private Markdown tmTexte;

	public PanelScene(Editor m, AbstractEntity e) {
		super(m,e);
		scene = (Scene) e;
		if (e==null) {
			System.err.println("PanelScene : entity is null");
			return;
		}
		initAll();
	}

	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p = new JPanel(new MigLayout("wrap 2, hidemode 3", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p,I18N.getMsg("scene"));
		cbInformative=Ui.initCheckBox(p,"informative",scene.informative, !MANDATORY);
		p.add(new JLabel());
		tfNumber = initTextField(p, "z.number", 5, scene.number, MANDATORY,!INFO);
		cbChapter=Ui.initComboBox(p,mainFrame,"chapter",Book.TYPE.CHAPTER, scene.chapter,null,!MANDATORY,NEW,EMPTY);
		cbPov=Ui.initComboBox(p,mainFrame, "pov",Book.TYPE.POV, scene.pov,null,!MANDATORY,NEW,EMPTY);
		if (scene.pov==null) cbPov.setSelectedIndex(1);
		cbStatus = initStatus(p, scene.status);
		cbNarrator=Ui.initComboBox(p,mainFrame, "scene.narrator",Book.TYPE.PERSON, scene.narrator,null,!MANDATORY,NEW,EMPTY);
		if (mainFrame.bookPref.isUseXeditor()) {
			xfile = initFileChooser(p, "scene.xfile", scene.xfile);
		}
		
		JPanel cx=new JPanel(new MigLayout("hidemode 3"));
		ckDate=Ui.initCheckBox(cx, "z.date", scene.hasDate(), !MANDATORY,"top, span, split 2");
		ckDate.addActionListener((ActionEvent arg0) -> {
			setGroupDate(ckDate.isSelected());
		});
		groupDate = new JPanel(new MigLayout("hidemode 3", "[][grow]", ""));
		groupDate.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
		dDate = initSbDate(groupDate, "z.date", scene.date, !MANDATORY,TIME);
		groupDate.add(new JSeparator(),"newline,span,split 5");
		cbRelScene = Ui.initAutoCB(groupDate, mainFrame, "scene.relative",Book.TYPE.SCENE,
				scene.relativeScene,scene, !MANDATORY, !NEW, EMPTY);
		tfRel = Ui.initTextField(groupDate, "scene.relative.date", 5, scene.relativeDate, !MANDATORY,!INFO);
		cx.add(groupDate,"span");
		p.add(cx,"span");
		setGroupDate(scene.hasDate());
		
		if (book.isScenario()) {
			tmTexte=new Markdown("tmTexte","text/plain",scene.writing);
			tmTexte.hideToolbar();
			tmTexte.setView(Markdown.VIEW.TEXT_ONLY);
			tmTexte.setDimension();
			tabbed.add(tmTexte,I18N.getMsg("scene.summary"));
		} else {
			taTexte=new EditorHtml(this, scene.writing);
			taTexte.setPreferredSize(MINIMUM_SIZE);
			taTexte.setCaretPosition(0);
			JScrollPane scroller = new JScrollPane(taTexte);
			SwingUtil.setUnitIncrement(scroller);
			tabbed.add(scroller,I18N.getMsg("scene.summary"));
		}
		JPanel nt=new JPanel(new MigLayout("fill, wrap","[grow][grow][grow]"));
		lsPersons = Ui.initListBox(nt,mainFrame, "persons", null, scene.persons,!MANDATORY,null);
		lsLocations = Ui.initListBox(nt,mainFrame, "locations", null, scene.locations,!MANDATORY,null);
		lsItems = Ui.initListBox(nt,mainFrame, "items", null, scene.items,!MANDATORY,null);
		lsPlots = Ui.initListBox(nt,mainFrame, "plots", null, scene.plots,!MANDATORY,null);
		lsPhotos = Ui.initListBox(nt,mainFrame, "photos", null, scene.photos,!MANDATORY,null);
		lsTags = Ui.initListBox(nt,mainFrame, "tags", null, scene.tags,!MANDATORY,null);
		JScrollPane scroller = new JScrollPane(nt);
		tabbed.add(scroller,I18N.getMsg("linked"));
		initDescription(p, null);
		initNotes(p,null);
		initAssistant(p, tabbed);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (tfNumber.getText().equals("+")) {
		} else if (tfNumber.getText().isEmpty()) {
			errorMsg(tfNumber,"error.missing");
		} else if (!StringUtil.isNumeric(tfNumber.getText())) {
			errorMsg(tfNumber, "error.notnumeric");
		} else {
			int n=Integer.parseInt(tfNumber.getText());
			Scene c=Scene.findNumber(mainFrame.book.scenes,n);
			if (c!=null
					&& c.chapter.equals(cbChapter.getSelectedItem())
					&& !Objects.equals(c.id, scene.id)) {
				errorMsg(tfNumber, "error.exists");
			}
		}
		if (cbStatus.getSelectedIndex()<0) {
			errorMsg(cbStatus,"error.missing");
		}
		if (!tfRel.getText().isEmpty()) {
			if (!StringUtil.isNumeric(tfRel.getText())) {
				errorMsg(tfNumber, "error.notNumeric");
			}
			if (cbRelScene.getSelectedItem()==null) {
				errorMsg(cbRelScene,"error.missing");
			}
		}
		return (msgError.isEmpty());
	}

	@Override
	public void apply() {
		scene.name=(tfName.getText());
		scene.informative=(cbInformative.isSelected());
		if (cbChapter.getSelectedIndex()>0) scene.chapter=(Chapter)cbChapter.getSelectedItem();
		else scene.chapter=(null);
		if (tfNumber.getText().equals("+")) {
			scene.number=(Scene.getNextNumber(book.scenes, (Chapter)cbChapter.getSelectedItem()));
		} else scene.number=(Integer.parseInt(tfNumber.getText()));
		Pov pov = (Pov)cbPov.getSelectedItem();
		if (cbPov.getSelectedIndex()>0) scene.pov=(pov);
		else scene.pov=(null);
		scene.status=(cbStatus.getSelectedIndex());
		if (cbNarrator.getSelectedIndex()>0) {
			Person person=(Person)cbNarrator.getSelectedItem();
			scene.narrator=(person);
		}
		else scene.narrator=(null);
		if (mainFrame.bookPref.isUseXeditor()) {
			scene.xfile=(xfile.getText());
		} else {
			scene.xfile="";
		}
		scene.date=(dDate.getDate());
		if (tfRel.getText().isEmpty() || tfRel.getText().equals("0")) {
			scene.relativeDate=0;
			scene.relativeScene=null;
		} else {
			scene.relativeDate=(Integer.parseInt(tfRel.getText()));
			scene.relativeScene=(Scene)cbRelScene.getSelectedItem();
		}
		if (book.isScenario()) {
			scene.writing=(tmTexte.getText());
		} else {
			scene.writing=(taTexte.getText());
		}
		{
			List<Person> ls=new ArrayList<>();
			lsPersons.getSelectedEntities().forEach((e) -> {
				ls.add((Person)e);
			});
			scene.persons=ls;
		}
		{
			List<Location> ls=new ArrayList<>();
			lsLocations.getSelectedEntities().forEach((e) -> {
				ls.add((Location)e);
			});
			scene.locations=ls;
		}
		{
			List<Item> ls=new ArrayList<>();
			lsItems.getSelectedEntities().forEach((e) -> {
				ls.add((Item)e);
			});
			scene.items=ls;
		}
		{
			List<Plot> ls=new ArrayList<>();
			lsPlots.getSelectedEntities().forEach((e) -> {
				ls.add((Plot)e);
			});
			scene.plots=ls;
		}
		{
			List<Photo> ls=new ArrayList<>();
			lsPhotos.getSelectedEntities().forEach((e) -> {
				ls.add((Photo)e);
			});
			scene.photos=ls;
		}
		{
			List<Tag> ls=new ArrayList<>();
			lsTags.getSelectedEntities().forEach((e) -> {
				ls.add((Tag)e);
			});
			scene.tags=ls;
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

	private void setGroupDate(boolean ck) {
		for (Component c:groupDate.getComponents()) {
			c.setEnabled(ck);
		}
		groupDate.setVisible(ck);
	}

}
