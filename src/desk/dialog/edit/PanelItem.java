/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Item;
import db.entity.Photo;
import desk.dialog.chooser.ImageChooserPanel;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelItem extends AbstractEditorPanel {

	private final Item item;
	private JComboBox cbCategory;
	private ImageChooserPanel tfIcon;
	private CheckBoxPanel lsPhoto;
	private JTextField tfBeacon;

	public PanelItem(Editor m, AbstractEntity e) {
		super(m,e);
		item=(Item)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		tabbed.add(p);
		initHeader(p);
		cbCategory=Ui.initAutoCB(p,mainFrame,"category",Book.TYPE.CATEGORY,item.category,NEW);
		tfBeacon=Ui.initTextField(p,"category.beacon",32,item.beacon, !MANDATORY,!INFO);
		tfIcon=initIconChooser(p,"icon.file",item.iconFile);
		lsPhoto=Ui.initListBox(p,mainFrame,"photos",null,item.photos,!MANDATORY,tabbed);

		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		item.setName(tfName.getText());
		if (cbCategory.getSelectedIndex()==0) item.category=null;
		else item.category=(Category)cbCategory.getSelectedItem();
		item.beacon=tfBeacon.getText();
		item.iconFile=tfIcon.getIconFile();
		List<Photo> ls=new ArrayList<>();
		lsPhoto.getSelectedEntities().forEach((e) -> {
			ls.add((Photo)e);
		});
		item.photos=ls;
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
