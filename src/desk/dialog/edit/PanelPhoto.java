/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Photo;
import desk.dialog.chooser.ImageChooserPanel;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.JPanel;
import lib.miginfocom.swing.MigLayout;
import tools.FileTool;

/**
 *
 * @author favdb
 */
public class PanelPhoto extends AbstractEditorPanel {

	private final Photo photo;
	private ImageChooserPanel tfPhoto;
	
	public PanelPhoto(Editor m, AbstractEntity e) {
		super(m,e);
		this.photo=(Photo)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		tabbed.add(p);
		initHeader(p);
		String image="";
		if (photo.file!=null && !photo.file.isEmpty()) {
			image=mainFrame.getImageDir()+photo.file;
		}
		tfPhoto=initImageChooser(p,"image",image);
		add(p);
		initDescription(p,null);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (tfPhoto.getIconFile().isEmpty()) {
			errorMsg(tfPhoto,"error.missing");
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		photo.setName(tfName.getText());
		String path=FileTool.getOnlyPath(tfPhoto.getIconFile());
		mainFrame.bookPref.Gallery.setFolder(path);
		String name=tfPhoto.getIconFile().replace(mainFrame.getImageDir(), "");
		photo.file=name;
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
}
