/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import db.I18N;
import db.entity.Book;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelRelation extends AbstractEditorPanel {

	private final Relation relation;
	private JComboBox cbSceneStart;
	private JComboBox cbSceneEnd;
	private CheckBoxPanel lsPersons;
	private CheckBoxPanel lsLocations;
	private CheckBoxPanel lsItems;
	private CheckBoxPanel lsTags;

	public PanelRelation(Editor m, AbstractEntity e) {
		super(m,e);
		relation = (Relation) e;
		initAll();
	}

	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p = new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		initHeader(p);
		tabbed.add(p);
		Scene d=relation.startScene;
		cbSceneStart = Ui.initAutoCB(p,mainFrame, "scene.start",Book.TYPE.SCENE, d, null, !MANDATORY, !NEW, EMPTY);
		Scene f=relation.endScene;
		cbSceneEnd = Ui.initAutoCB(p, mainFrame, "scene.end",Book.TYPE.SCENE, f, null, !MANDATORY, !NEW, EMPTY);
		lsPersons = Ui.initListBox(p, mainFrame, "persons", null, relation.persons,!MANDATORY,tabbed);
		lsLocations = Ui.initListBox(p, mainFrame, "locations", null, relation.locations,!MANDATORY,tabbed);
		lsItems = Ui.initListBox(p, mainFrame, "items", null, relation.items,!MANDATORY,tabbed);
		lsTags = Ui.initListBox(p, mainFrame, "tags", null, relation.tags,!MANDATORY,tabbed);
		initDescription(p, null);
		initNotes(p, null);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (cbSceneStart.getSelectedItem() != null
				&& cbSceneEnd.getSelectedItem() == null) {
			errorMsg(cbSceneEnd, "error.missing");
		}
		if (lsPersons.getSelectedEntities().isEmpty()
				&& lsLocations.getSelectedEntities().isEmpty()
				&& lsItems.getSelectedEntities().isEmpty()
				&& lsTags.getSelectedEntities().isEmpty()) {
			errorMsg(lsPersons, "error.must_select");
		}
		return (msgError.isEmpty());
	}

	@Override
	public void apply() {
		relation.setName(tfName.getText());
		Object scene=(Object)cbSceneStart.getSelectedItem();
		if (cbSceneStart.getSelectedItem()!=null) relation.startScene=((Scene)cbSceneStart.getSelectedItem());
		else relation.startScene=null;
		if (cbSceneEnd.getSelectedItem()!=null) relation.endScene=((Scene)cbSceneEnd.getSelectedItem());
		else relation.endScene=null;
		List<AbstractEntity> ls;
		{
			ls = lsPersons.getSelectedEntities();
			List<Person> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Person) e);
			}
			relation.persons=(lsc);
		}
		{
			ls = lsLocations.getSelectedEntities();
			List<Location> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Location) e);
			}
			relation.locations=(lsc);
		}
		{
			ls = lsItems.getSelectedEntities();
			List<Item> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Item) e);
			}
			relation.items=(lsc);
		}
		{
			ls = lsTags.getSelectedEntities();
			List<Tag> lsc = new ArrayList<>();
			for (AbstractEntity e : ls) {
				lsc.add((Tag) e);
			}
			relation.tags=(lsc);
		}
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
