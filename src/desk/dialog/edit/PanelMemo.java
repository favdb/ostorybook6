/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Memo;
import desk.dialog.chooser.ImageChooserPanel;
import db.I18N;
import java.beans.PropertyChangeEvent;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelMemo extends AbstractEditorPanel {

	private final Memo memo;
	private JComboBox cbCategory;
	private ImageChooserPanel tfIcon;
	private CheckBoxPanel lsPhoto;
	private JTextField tfBeacon;

	public PanelMemo(Editor m, AbstractEntity e) {
		super(m,e);
		memo=(Memo)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		tabbed.add(p);
		initHeader(p);

		//initDescription(p,!NEWTAB);
		initNotes(p,null);
	}

	@Override
	public boolean verifier() {
		resetError();
		return(!msgError.isEmpty());
	}

	@Override
	public void apply() {
		memo.setName(tfName.getText());
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
}
