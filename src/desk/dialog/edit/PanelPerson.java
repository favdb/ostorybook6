/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.dialog.edit;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Person;
import db.entity.SbDate;
import desk.dialog.chooser.ColorChooserPanel;
import desk.dialog.chooser.DateChooser;
import db.I18N;
import db.entity.Book;
import db.entity.Gender;
import desk.tools.Ui;
import static desk.tools.Ui.*;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class PanelPerson extends AbstractEditorPanel {

	private final Person person;
	private JTextField tfFirstName;
	private JTextField tfLastName;
	private JTextField tfAbbr;
	private JComboBox cbGender;
	private JComboBox cbCategory;
	private CheckBoxPanel lsCategory;
	private DateChooser dBirth;
	private DateChooser dDeath;
	private JTextField tfOccupation;
	private ColorChooserPanel color;
	private JTextField tfBeacon;
	private boolean autoAbbr=false;

	public PanelPerson(Editor m, AbstractEntity e) {
		super(m,e);
		person=(Person)e;
		initAll();
	}
	
	@Override
	public void initUi() {
		super.initUi();
		initMultiTab();
		JPanel p=new JPanel(new MigLayout("wrap 2", "[][grow]", ""));
		p.setName(I18N.getMsg(entity.type.toString()));
		tabbed.add(p);
		initHeader(p);
		tfName.setEditable(false);
		tfFirstName=Ui.initTextField(p,"person.firstname",32,person.firstname,MANDATORY,!INFO);
		tfLastName=Ui.initTextField(p,"person.lastname",32,person.lastname, !MANDATORY,!INFO);
		tfAbbr=initTextField(p,"z.abbreviation",8,person.abbreviation, !MANDATORY,!INFO);
		if (tfAbbr.getText().isEmpty()) {
			autoAbbr=true;
			tfFirstName.addCaretListener((CaretEvent e) -> {
				doAutoAbbr();
			});
			tfLastName.addCaretListener((CaretEvent e) -> {
				doAutoAbbr();
			});
			tfAbbr.addCaretListener((CaretEvent e) -> {
				if (tfAbbr.hasFocus()) {
					autoAbbr = false;
				}
			});
		}
		cbGender=Ui.initAutoCB(p,mainFrame,"gender",Book.TYPE.GENDER,
				person.gender,null, MANDATORY,NEW,!EMPTY);
		cbCategory=Ui.initAutoCB(p,mainFrame,"category",Book.TYPE.CATEGORY,
				person.category,null,MANDATORY,NEW,!EMPTY);
		dBirth=initSbDate(p,"person.birthday",person.birthday, !MANDATORY,!TIME);
		dDeath=initSbDate(p,"person.dayofdeath",person.dayofdeath, !MANDATORY,!TIME);
		tfOccupation=Ui.initTextField(p,"person.occupation",20,person.occupation,!MANDATORY,!INFO);
		color=initColorChooser(p,"z.color",person.color);
		lsCategory=Ui.initListBox(p,mainFrame,"categories",
				person.category, person.categories, !MANDATORY,tabbed);
		
		initDescription(p,null);
		initNotes(p,null);
		initAssistant(p,tabbed);
	}
	
	private void doAutoAbbr() {
		if (!autoAbbr) return;
		String str1=tfFirstName.getText();
		if (str1.length()>2) {
			str1=str1.substring(0, 2);
		}
		String str2=tfLastName.getText();
		if (str2.length()>2) {
			str2=str2.substring(0, 3);
		}
		tfAbbr.setText(str1+str2);
	}

	@Override
	public boolean verifier() {
		resetError();
		if (tfFirstName.getText().isEmpty()) {
			errorMsg(tfFirstName,"error.missing");
		}
		//check if this name is allready use
		String s=tfFirstName.getText()+" "+tfLastName.getText();
		Person p=(Person)AbstractEntity.findName(mainFrame.book.persons, s);
		if (p!=null && !Objects.equals(p.id, entity.id)){
			errorMsg(tfName,"error.exists");
		}
		tfName.setText(s);
		if (tfAbbr.getText().isEmpty()) {
			errorMsg(tfAbbr,"error.missing");
		}
		SbDate db=dBirth.getDate();
		if (db!=null) {
			SbDate df=dDeath.getDate();
			if (db.after(df)) {
				errorMsg(dBirth,"error.date");
			}
		}
		return(msgError.isEmpty());
	}

	@Override
	public void apply() {
		person.name=(tfName.getText());
		person.firstname=(tfFirstName.getText());
		person.lastname=(tfLastName.getText());
		person.abbreviation=(tfAbbr.getText());
		person.gender=(((Gender)cbGender.getSelectedItem()));
		if (cbCategory.getSelectedIndex()>0) person.category=(((Category)cbCategory.getSelectedItem()));
		else person.category=(book.getSecondaryCharacter());
		List<AbstractEntity> ls = lsCategory.getSelectedEntities();
		List<Category> lsc=new ArrayList<>();
		ls.forEach((e) -> {
			lsc.add((Category)e);
		});
		person.categories=(lsc);
		person.birthday=(dBirth.getDate());
		person.dayofdeath=(dBirth.getDate());
		person.occupation=(tfOccupation.getText());
		person.color=(color.getColor().getRGB());
		super.apply();
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}

}
