/*
 * Copyright (C) 2018 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.exporter;

import db.DB;
import db.entity.Book;
import db.I18N;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import desk.tools.ExceptionDlg;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class Export {
	public enum FORMAT {
		BOOK, CSV, HTML, TXT, PDF, XML, NONE;
		
		@Override
		public String toString() {
			return(name().toLowerCase());
		}
	}
	
	public static FORMAT getFormat(String str) {
		for (FORMAT f:FORMAT.values()) {
			if (f.toString().equals(str)) {
				return(f);
			}
		}
		return(FORMAT.NONE);
	}

	private final Book book;
	private boolean opened=false;
	private final String fileName;
	private String format="html";//alternative may be text
    private BufferedWriter outStream;
	
	public Export(String filename, Book book, String format) {
		this.fileName=filename;
		this.book=book;
		this.format=format;
	}
	
	public boolean isOpened() {
		return(opened);
	}
	
	public boolean isHtml() {
		return(format.equals("html"));
	}
	
	public boolean isText() {
		return(format.equals("text"));
	}
	
	public static void dbToHtml(Book book, String f, boolean statistics) {
		Export ex=new Export(f,book,"html");
		if (ex.htmlOpen()) {
			ex.htmlWrite(book.toHtmlDetail());
			if (statistics) {
				ex.htmlWrite(book.statistics(true));
			}
			ex.htmlClose();
		}
	}
	
	public static void dbToText(Book book, String f, boolean statistics) {
		Export ex=new Export(f,book,"text");
		/*if (ex.textOpen()) {
			ex.textWrite(book.toText());
			if (statistics) {
				String str=book.statistics();
				ex.textWrite(HtmlUtil.htmlToText(str));
			}
			ex.textClose();
		}*/
	}
	
    public boolean htmlOpen() {
        opened = false;
        File file = new File(fileName);
        if ((file.exists() && file.isDirectory())) {
            JOptionPane.showMessageDialog(null,
                    I18N.getMsg(DB.class,"export.error.dir"),
                    I18N.getMsg(DB.class,"export"), 1);
            return (false);
        }
        try {
            outStream = new BufferedWriter(new FileWriter(fileName));
        } catch (IOException ex) {
            error("Export", ex);
            return (false);
        }
        htmlWriteHeader();
        opened = true;
        return (true);
    }

	private void error(String msg, IOException ex) {
        ExceptionDlg.show("Export", ex);
	}

    private boolean htmlWriteHeader() {
        StringBuilder str = new StringBuilder();
        str.append("<!DOCTYPE html>\n");
        str.append("<html>\n");
        str.append("<head>\n");
        str.append("    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
        str.append("    <title>").append(book.info.getTitle()).append("</title>\n");
        str.append(HtmlUtil.getCSSDefault());
        str.append("</head>\n");
        str.append("<body>\n");
        try {
            outStream.write(str.toString(), 0, str.length());
            outStream.flush();
        } catch (IOException ex) {
            ExceptionDlg.show("Export", ex);
            return (false);
        }
        return (true);
    }

	private boolean htmlWrite(String str) {
        try {
            outStream.write(str, 0, str.length());
            outStream.flush();
        } catch (IOException ex) {
            ExceptionDlg.show("Export", ex);
            return (false);
        }
		return(true);
	}
	
	private void htmlClose() {
		try {
			htmlWrite("</body>\n</html>");
			outStream.flush();
			outStream.close();
		} catch (IOException ex) {
            ExceptionDlg.show("Export", ex);
		}
		opened=false;
	}

}
