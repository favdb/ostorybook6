/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.exporter;

import desk.dialog.AbstractDialog;
import desk.exporter.options.OptionsTxt;
import desk.exporter.options.OptionsHtml;
import desk.exporter.options.OptionsBook;
import desk.exporter.options.OptionsCsv;
import desk.app.MainFrame;
import db.I18N;
import db.entity.Book;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIDefaults;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class ExportDlg extends AbstractDialog implements ActionListener, CaretListener {

	private JTextField txFolder;
	private JTabbedPane options;
	private JComboBox cbReport;
	public List<ExportType> exports;
	public ExportParam param;
	private OptionsHtml HTML;
	private OptionsCsv CSV;
	private OptionsTxt TXT;
	private OptionsBook BOOK;
	private JRadioButton rbHtml, rbTxt, rbCsv, rbXml, rbSql;
	private JPanel panelFormat;

	public ExportDlg(MainFrame m) {
		super(m);
		book=mainFrame.book;
		initAll();
	}

	public static void show(MainFrame m) {
		ExportDlg dlg=new ExportDlg(m);
		dlg.setVisible(true);
	}

	@Override
	public void init() {
		super.initUi();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initUi() {
		//App.trace("ExportDlg.initUi()");
		super.initUi();
		param = new ExportParam(mainFrame);

		JLabel lbFolder = new JLabel(I18N.getMsg("export.directory"));
		txFolder = new JTextField();
		txFolder.setText(mainFrame.bookPref.Export.getDirectory());
		txFolder.setColumns(32);
		txFolder.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyReleased(java.awt.event.KeyEvent evt) {
				File f = new File(txFolder.getText());
				if (f.exists() && f.isDirectory()) {
					UIDefaults defaults = javax.swing.UIManager.getDefaults();
					txFolder.setForeground(defaults.getColor("TextField.foreground"));
				} else {
					txFolder.setForeground(Color.red);
				}
			}
		});
		JButton bt = new JButton(IconUtil.getIcon("small/file-open"));
		bt.addActionListener((java.awt.event.ActionEvent evt) -> {
			JFileChooser chooser = new JFileChooser(txFolder.getText());
			chooser.setFileSelectionMode(1);
			int i = chooser.showOpenDialog(null);
			if (i != 0) {
				return;
			}
			File file = chooser.getSelectedFile();
			txFolder.setText(file.getAbsolutePath());
			txFolder.setBackground(Color.WHITE);
		});
		bt.setMargin(new Insets(0, 0, 0, 0));
		initExports();

		JLabel lbReport = new JLabel(I18N.getMsg("export.type"));
		initReport();
		initFormat();

		BOOK = new OptionsBook(mainFrame);
		CSV = new OptionsCsv(mainFrame);
		TXT = new OptionsTxt(mainFrame);
		HTML = new OptionsHtml(mainFrame);
		initOptions();

		//layout
		setLayout(new MigLayout("", "", ""));
		setBackground(Color.white);
		setTitle(I18N.getMsg("export"));
		add(lbFolder, "split 3");
		add(txFolder);
		add(bt, "wrap");
		add(lbReport, "split 2");
		add(cbReport, "wrap");
		add(panelFormat, "span, wrap");
		add(options, "span, left, wrap");
		add(buttonOk(), "span, split 2,sg,right");
		add(buttonCancel(), "sg");
		pack();
		setLocationRelativeTo(mainFrame);
	}

	private void initExports() {
		exports = new ArrayList<>();
		exports.add(new ExportType("book", "export.book.text"));
		exports.add(new ExportType("summary", "export.book.summary"));
		exports.add(new ExportType(Book.TYPE.PART, "export.part.list"));
		exports.add(new ExportType(Book.TYPE.CHAPTER, "export.chapter.list"));
		exports.add(new ExportType(Book.TYPE.SCENE, "export.scene.list"));
		exports.add(new ExportType(Book.TYPE.PERSON, "export.person.list"));
		exports.add(new ExportType(Book.TYPE.LOCATION, "export.location.list"));
		exports.add(new ExportType(Book.TYPE.TAG, "export.tag.list"));
		exports.add(new ExportType(Book.TYPE.ITEM, "export.item.list"));
		exports.add(new ExportType(Book.TYPE.IDEA, "export.idea.list"));
		exports.add(new ExportType(Book.TYPE.ALL, "export.all.list"));
	}

	private void initOptions() {
		options = new JTabbedPane();
		//options.setLayout(new MigLayout("wrap","[]","[][][][]"));
		//options.setMinimumSize(new Dimension(600, 300));
		options.setBorder(javax.swing.BorderFactory.createTitledBorder(I18N.getMsg("options")));
		options.add(BOOK, I18N.getMsg("export.book.text"));
		options.add(HTML, "HTML");
		options.add(TXT, "TXT");
		options.add(CSV, "CSV");
	}

	@SuppressWarnings("unchecked")
	private void initReport() {
		cbReport = new JComboBox();
		exports.forEach((export) -> {
			cbReport.addItem(export);
		});
		cbReport.setSelectedIndex(0);
		cbReport.addItemListener((java.awt.event.ItemEvent evt) -> {
			if (evt.getStateChange() == ItemEvent.SELECTED) {
				initFormat();
				if (((ExportType) cbReport.getSelectedItem()).name.equals("book")) {
					options.setSelectedComponent(BOOK);
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void initFormat() {
		String m = "";
		if (panelFormat == null) {
			ButtonGroup group1 = new ButtonGroup();
			rbHtml = new JRadioButton("html");
			rbTxt = new JRadioButton("txt");
			rbCsv = new JRadioButton("csv");
			rbXml = new JRadioButton("xml");
			rbSql = new JRadioButton("sql");
			group1.add(rbHtml);
			group1.add(rbTxt);
			group1.add(rbCsv);
			group1.add(rbXml);
			group1.add(rbSql);
			panelFormat = new JPanel();
			panelFormat.setLayout(new MigLayout("", "", ""));
			JLabel lb3 = new JLabel(I18N.getMsg("export.format") + ":");
			panelFormat.add(lb3);
			panelFormat.add(rbHtml);
			panelFormat.add(rbCsv);
			panelFormat.add(rbTxt);
			panelFormat.add(rbXml);
			panelFormat.add(rbSql);
		} else {
			m = getFormat();
		}

		switch (Export.getFormat(getReportName())) {
			case BOOK:
				setAllowedFormat("html,txt,xml");
				if (m.equals("sql") || m.equals("csv")) {
					m = "html";
				}
				break;
			case CSV:
			case HTML:
			case TXT:
			case PDF:
			case XML:
				setAllowedFormat("csv,txt,html,xml");
				if (m.equals("sql")) {
					m = "html";
				}
				break;
		}
		setFormat(m);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void caretUpdate(CaretEvent e) {
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (doExport()) {
					dispose();
				}
			}
		};
	}

	private String getReportName() {
		return (((ExportType) cbReport.getSelectedItem()).name.toLowerCase());
	}

	private boolean doExport() {
		//App.trace("ExportDlg.doExport()");
		boolean b = true;
		String title = book.getTitle();
		if (title.isEmpty()) {
			JOptionPane.showMessageDialog(this,
				I18N.getMsg("export.missing.title"),
				I18N.getMsg("export"), 1);
			return (false);
		}
		// check if the folder exists
		String dir = txFolder.getText();
		if (dir.isEmpty()) {
			JOptionPane.showMessageDialog(this,
				I18N.getMsg("export.dir.missing"),
				I18N.getMsg("export"), 1);
			return (false);
		}
		//check if dir is a directory
		File f = new File(dir);
		if (!(f.exists() && f.isDirectory())) {
			JOptionPane.showMessageDialog(this,
				I18N.getMsg("export.dir.error"),
				I18N.getMsg("export"), 1);
			return (false);
		}
		saveParam();
		mainFrame.bookPref.Export.setDirectory(dir);
		String exportName = ((ExportType) cbReport.getSelectedItem()).name.toLowerCase();
		String exportTitle = ((ExportType) cbReport.getSelectedItem()).getTitle();
		String format = getFormat();
		String ret = "";
		ExportTable exporter=new ExportTable(mainFrame,format);
		if (Book.TYPE.ALL.toString().equals(exportName)) {
			for (ExportType t:exports) {
				if (t.isList && (exporter.askFileExists(t.getTitle()))) {
					ret += I18N.getMsg(t.title) + "\n";
				}
			}
		} else if (exporter.askFileExists(exportName)) {
			ret += exportTitle + "\n";
		}
		if (!ret.replace("\n", "").isEmpty()) {
			if (JOptionPane.showConfirmDialog(this.getParent(),
				I18N.getMsg("export.replace", ret),
				I18N.getMsg("export"),
				JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
				return (false);
			}
		}
		mainFrame.setWaitingCursor();
		switch (Book.getTYPE(exportName)) {
			case ALL:
				exports.forEach((t)-> {
					if (t.isList) exporter.doExport(t.name);
				});
				break;
			case BOOK:
				ExportBook.toFile(mainFrame);
				break;
			default:
				exporter.doExport(exportName);
				break;
		}
		mainFrame.setDefaultCursor();
		if (!ret.isEmpty()) {
			return (false);
		}
		return (true);
	}

	private void saveParam() {
		BOOK.save();
		CSV.save();
		TXT.save();
		HTML.save();
		param.save();
	}

	private String getFormat() {
		if (rbSql.isSelected()) {
			return ("sql");
		}
		if (rbTxt.isSelected()) {
			return ("txt");
		}
		if (rbCsv.isSelected()) {
			return ("csv");
		}
		if (rbXml.isSelected()) {
			return ("xml");
		}
		return ("html");
	}

	private void setFormat(String m) {
		switch (m) {
			case "txt":
				rbTxt.setSelected(true);
				return;
			case "csv":
				rbCsv.setSelected(true);
				return;
			case "xml":
				rbXml.setSelected(true);
				return;
			case "sql":
				rbSql.setSelected(true);
				return;
		}
		rbHtml.setSelected(true);
	}

	private void setAllowedFormat(String str) {
		rbHtml.setEnabled(str.contains("html"));
		rbCsv.setEnabled(str.contains("csv"));
		rbTxt.setEnabled(str.contains("txt"));
		rbXml.setEnabled(str.contains("xml"));
	}

	public class ExportType {

		private String name;
		private String title;
		private boolean isList;

		public ExportType(String name, String key) {
			this.name = name;
			this.title = I18N.getMsg(key);
			this.isList = this.name.contains("list");
		}

		public ExportType(Book.TYPE type, String key) {
			this.name = type.toString();
			this.title = I18N.getMsg(key);
			this.isList = this.name.contains("list");
		}

		public void setExportName(String name) {
			this.name = name;
		}

		public void setKey(String title) {
			this.title = title;
		}

		@Override
		public String toString() {
			return getTitle();
		}

		public String getName() {
			return name;
		}

		public String getTitle() {
			return (title);
		}

	}
}
