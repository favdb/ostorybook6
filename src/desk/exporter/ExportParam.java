/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.exporter;

import db.entity.Book;
import desk.app.pref.BookPrefExport;
import desk.app.MainFrame;

/**
 *
 * @author favdb
 */
public class ExportParam {

	public MainFrame mainFrame;
	public Book book;
	public String format = "xml";
	public String dir;
	public String fileName;
	public String csvQuote = "\"";
	public String csvComma=";";
	public boolean txtTab=true;
	public String txtSeparator="";
	public boolean htmlCssUse=false;
	public String htmlCssFile="";
	public boolean isExportChapterBooktitle=false;
	public boolean isExportChapterBreakPage=false;
	public boolean isExportChapterNumbers=false;
	public boolean isExportChapterNumbersRoman=false;
	public boolean isExportChapterTitles=false;
	public boolean isExportChapterDatesLocs=false;
	public boolean isExportSceneDidascalie=false;
	public boolean isExportSceneTitles=false;
	public boolean isExportSceneSeparator=false;
	public boolean isExportPartTitles = false;
	public boolean htmlMultiChapter=false; // one file by chapter
	public boolean htmlMultiScene=false; // one file by Scene
	public boolean htmlNav = false;
	public boolean htmlNavImage = false;
	/* TODO PDF export
	public String pdfPageSize;
	public boolean pdfLandscape;
	*/

	public ExportParam(MainFrame m) {
		mainFrame = m;
		book=m.book;
		init();
	}

	public void init() {
		BookPrefExport param = mainFrame.bookPref.Export;
		format = param.getFormat();
		csvQuote = param.getCsvQuotes();
		csvComma = param.getCsvComma();
		txtSeparator = param.getCsvTab();
		htmlCssUse=param.getHtmlCssUse();
		htmlCssFile = param.getHtmlCss();
		htmlNav = param.getHtmlNav();
		htmlNavImage = param.getHtmlNavImage();
		htmlMultiChapter = param.getHtmlMultiChapter();
		htmlMultiScene = param.getHtmlMultiScene();
		isExportChapterBooktitle = param.getChapterBookTitle();
		isExportChapterBreakPage = param.getChapterBreakPage();
		isExportChapterNumbers = param.getChapterNumber();
		isExportChapterNumbersRoman = param.getChapterRoman();
		isExportChapterTitles = param.getChapterTitle();
		isExportChapterDatesLocs = param.getChapterDateLocation();
		isExportSceneDidascalie = param.getSceneDidascalie();
		isExportSceneTitles = param.getSceneTitle();
		isExportSceneSeparator = param.getSceneSeparator();
		isExportPartTitles = param.getPartTitle();
		/*TODO PDF export
		pdfPageSize = book.param.getString(BK.PDF_PAGE_SIZE);
		pdfLandscape = book.param.getBoolean(BK.PDF_LANDSCAPE);
		*/
	}

	public void save() {
		BookPrefExport param = mainFrame.bookPref.Export;
		param.setCsvQuotes(csvQuote);
		param.setCsvComma(csvComma);
		param.setCsvTab(txtSeparator);
		param.setHtmlCssUse(htmlCssUse);
		param.setHtmlCss(htmlCssFile);
		param.setHtmlNav(htmlNav);
		param.setHtmlNavImage(htmlNavImage);
		param.setHtmlMultiChapter(htmlMultiChapter);
		param.setHtmlMultiScene(htmlMultiScene);
		param.setChapterBookTitle(isExportChapterBooktitle);
		param.setChapterBreakPage(isExportChapterBreakPage);
		param.setChapterNumber(isExportChapterNumbers);
		param.setChapterRoman(isExportChapterNumbersRoman);
		param.setChapterTitle(isExportChapterTitles);
		param.setChapterDateLocation(isExportChapterDatesLocs);
		param.setSceneDidascalie(isExportSceneDidascalie);
		param.setSceneTitle(isExportSceneTitles);
		param.setSceneSeparator(isExportSceneSeparator);
		/* TODO PDF export
		book.param.setParamValue(BK.PDF_PAGE_SIZE.toString(), pdfPageSize);
		book.param.setParamValue(BK.PDF_LANDSCAPE.toString(), pdfLandscape);
		*/
	}

}
