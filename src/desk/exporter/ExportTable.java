/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.exporter;

import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Event;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Photo;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import desk.app.MainFrame;
import db.I18N;
import java.util.List;
import javax.swing.JOptionPane;
import lib.infonode.docking.View;

/**
 *
 * @author favdb
 */
public class ExportTable extends AbstractExporter {

	public static void exec(MainFrame m, View v, String... fmt) {
		ExportTable exp = new ExportTable(m, v);
		if (fmt.length > 0 && !fmt[0].equals("html")) {
			exp.setFormat(fmt[0]);
		}
		exp.doExport();
	}

	public String error = "";
	public String trace = "";

	public ExportTable(MainFrame m, String fmt) {
		super(m, "", "html");
		setFormat(fmt);
		book = mainFrame.book;
	}

	public ExportTable(MainFrame m, View v) {
		super(m, v.getName(), "html");
		book = mainFrame.book;
		init();
	}

	public final void setFormat(String fmt) {
		param.format = fmt;
	}

	public final void init() {
	}

	public void doExport(String table) {
		name = table;
		doExport();
	}

	public void doExport() {
		if (askFileExists(name)) {
			if (JOptionPane.showConfirmDialog(mainFrame,
					I18N.getMsg("export.replace", param.fileName),
					I18N.getMsg("export"),
					JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		if (!openFile(name)) {
			return;
		}
		if (!isOpened) {
			return;
		}
		switch (Book.getTYPE(name)) {
			case CATEGORY:
				writeCategory();
				break;
			case CHAPTER:
				writeChapter();
				break;
			case EVENT:
				writeEvent();
				break;
			case GENDER:
				writeGender();
				break;
			case IDEA:
				writeIdea();
				break;
			case ITEM:
				writeItem();
				break;
			case LOCATION:
				writeLocation();
				break;
			case MEMO:
				writeMemo();
				break;
			case PART:
				writePart();
				break;
			case PERSON:
				writePerson();
				break;
			case PHOTO:
				writePhoto();
				break;
			case PLOT:
				writePlot();
				break;
			case POV:
				writePov();
				break;
			case RELATION:
				writeRelation();
				break;
			case SCENE:
				writeScene();
				break;
			case TAG:
				writeTag();
				break;
			default:
				break;
		}
		if ("html".equals(param.format)) {
			writeText("    </table>\n");
		}
		closeFile(true);
	}

	private void writeCategory() {
		writeHeaderColumn(Category.getColumns());
		List<Category> entities = book.categories;
		for (Category e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeChapter() {
		writeHeaderColumn(Chapter.getColumns());
		List<Chapter> entities = book.chapters;
		for (Chapter e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeGender() {
		writeHeaderColumn(Gender.getColumns());
		List<Gender> entities = book.genders;
		for (Gender e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeIdea() {
		writeHeaderColumn(Idea.getColumns());
		List<Idea> entities = book.ideas;
		for (Idea e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeItem() {
		writeHeaderColumn(Item.getColumns());
		List<Item> entities = book.items;
		for (Item e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeLocation() {
		writeHeaderColumn(Location.getColumns());
		List<Location> entities = book.locations;
		for (Location e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeMemo() {
		writeHeaderColumn(Memo.getColumns());
		List<Memo> entities = book.memos;
		for (Memo e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writePart() {
		writeHeaderColumn(Part.getColumns());
		List<Part> entities = book.parts;
		for (Part e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writePerson() {
		writeHeaderColumn(Person.getColumns());
		List<Person> entities = book.persons;
		for (Person e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writePhoto() {
		writeHeaderColumn(Photo.getColumns());
		List<Photo> entities = book.photos;
		for (Photo e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writePlot() {
		writeHeaderColumn(Plot.getColumns());
		List<Plot> entities = book.plots;
		for (Plot e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeRelation() {
		writeHeaderColumn(Relation.getColumns());
		List<Relation> entities = book.relations;
		for (Relation e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeScene() {
		writeHeaderColumn(Scene.getColumns());
		List<Scene> entities = book.scenes;
		for (Scene e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writePov() {
		writeHeaderColumn(Pov.getColumns());
		List<Pov> entities = book.povs;
		for (Pov e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeTag() {
		writeHeaderColumn(Tag.getColumns());
		List<Tag> entities = book.tags;
		for (Tag e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeEvent() {
		writeHeaderColumn(Event.getColumns());
		List<Event> entities = book.events;
		for (Event e : entities) {
			switch (param.format) {
				case "html":
					writeHtml(e.toHtml());
					break;
				case "csv":
					writeText(e.toCsv(param.csvQuote, param.csvQuote, param.csvComma));
					break;
				case "txt":
					writeText(e.toText());
					break;
			}
		}
	}

	private void writeHeaderColumn(String[] cols) {
		if (cols == null || cols.length < 1) {
			return;
		}
		switch (param.format) {
			case "html":
				writeText("<table border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\n<tr>\n");
				for (String col : cols) {
					writeColumn(I18N.getMsg(col));
				}
				writeText("</tr>\n");
				break;
			case "xml": //no headers
				break;
			case "csv":
				for (String col : cols) {
					writeText(param.csvQuote + col + param.csvQuote + param.csvComma);
				}
				writeText("\n");
			case "txt":
				for (String col : cols) {
					writeText(col + param.txtSeparator);
				}
				writeText("\n");
		}
	}

	private void writeColumn(String name) {
		switch (param.format) {
			case "html":
				writeText("    <td>" + name + "</td>\n");
				break;
			case "csv":
				writeText("\"" + name + "\";");
				break;
			case "text":
				writeText("\t" + name + "");
				break;
		}
	}

}
