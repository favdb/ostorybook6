/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.exporter;

import desk.app.App;
import desk.app.MainFrame;
import desk.panel.reading.PrefViewReading;

/**
 *
 * @author favdb
 */
public class ParamExport {

	public MainFrame mainFrame;
	public String format = "xml";
	public String dir;
	public String fileName;
	public String csvQuote = "\"";
	public String csvComma = ";";
	public boolean txtTab = false;
	public String txtSeparator = "\t";
	public boolean htmlCssUse;
	public String htmlCssFile;
	public boolean htmlMultiChapter; // one file by chapter
	public boolean htmlMultiScene; // one file by Scene
	public String pdfPageSize;
	public boolean pdfLandscape;
	public boolean htmlNav = false;
	public boolean htmlNavImage = false;
	public PrefViewReading reading;

	public ParamExport(MainFrame m) {
		mainFrame = m;
		init();
	}

	public void init() {
		format = mainFrame.bookPref.Export.getFormat();
		dir = mainFrame.bookPref.Export.getDirectory();
		fileName = mainFrame.bookPref.Export.getLastFile();
		csvQuote = mainFrame.bookPref.Export.getCsvQuotes();
		csvComma = mainFrame.bookPref.Export.getCsvComma();
		txtSeparator = mainFrame.bookPref.Export.getCsvTab();
		txtTab = !txtSeparator.isEmpty();
		htmlCssUse = mainFrame.bookPref.Export.getHtmlCssUse();
		htmlNav = mainFrame.bookPref.Export.getHtmlNav();
		htmlNavImage = mainFrame.bookPref.Export.getHtmlNavImage();
		htmlMultiChapter = mainFrame.bookPref.Export.getHtmlMultiChapter();
		htmlMultiScene = mainFrame.bookPref.Export.getHtmlMultiScene();
		if (htmlCssUse) {
			htmlCssFile = mainFrame.bookPref.Export.getHtmlCss();
		} else {
			htmlCssFile = "";
		}
		reading=mainFrame.getPref().reading;
	}

	public void save() {
		mainFrame.bookPref.Export.setFormat(format);
		mainFrame.bookPref.Export.setDirectory(dir);
		mainFrame.bookPref.Export.setLastFile(fileName);
		mainFrame.bookPref.Export.setCsvComma(csvComma);
		mainFrame.bookPref.Export.setCsvQuotes(csvQuote);
		mainFrame.bookPref.Export.setCsvComma(txtSeparator);
		mainFrame.bookPref.Export.setHtmlCssUse(htmlCssUse);
		mainFrame.bookPref.Export.setHtmlNav(htmlNav);
		mainFrame.bookPref.Export.setHtmlNavImage(htmlNavImage);
		mainFrame.bookPref.Export.setHtmlMultiChapter(htmlMultiChapter);
		mainFrame.bookPref.Export.setHtmlMultiScene(htmlMultiScene);
		mainFrame.bookPref.Export.setHtmlCss(htmlCssFile);
		mainFrame.bookPref.xmlTo(mainFrame.xml);
	}

}
