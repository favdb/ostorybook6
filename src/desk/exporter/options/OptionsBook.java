/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.exporter.options;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import lib.miginfocom.swing.MigLayout;
import db.I18N;
import desk.app.MainFrame;
import desk.app.pref.BookPrefExport;
import desk.tools.FontUtil;
import javax.swing.JSeparator;

/**
 *
 * @author favdb
 */
public class OptionsBook extends JPanel {
	private final ButtonGroup group1;
    public JCheckBox ckExportChapterBooktitle,
			ckExportChapterDatesLocations,
			ckExportChapterNumbers,
			ckExportChapterNumbersRoman,
			ckExportChapterTitles,
			ckExportSceneDidascalie,
			ckExportSceneSeparator,
			ckExportSceneTitles;
    public JRadioButton htmlChapterMulti,
			htmlBookMultiScene,
			htmlBookOneFile;
	private final BookPrefExport param;
	private final MainFrame mainFrame;

	public OptionsBook(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
		this.param=mainFrame.bookPref.Export;
		setLayout(new MigLayout("","",""));

		JLabel lb=new JLabel(I18N.getMsg("export.book.htmloption"));
		lb.setFont(FontUtil.getBold());
		add(lb,"span");

		group1 = new ButtonGroup();
		htmlBookOneFile = new JRadioButton(I18N.getMsg("export.book.htmloption.onefile"));
        htmlBookOneFile.setText(I18N.getMsg("export.book.htmloption.onefile"));
        add(htmlBookOneFile,"span,split 3");
        group1.add(htmlBookOneFile);

        htmlChapterMulti = new JRadioButton(I18N.getMsg("export.book.htmloption.multifile"));
		htmlBookOneFile.addActionListener((java.awt.event.ActionEvent evt) -> {
			ckExportChapterBooktitle.setEnabled(htmlChapterMulti.isSelected());
		});
		add(htmlChapterMulti);
		group1.add(htmlChapterMulti);
		
		htmlBookMultiScene = new JRadioButton(I18N.getMsg("export.book.htmloption.multiscene"));
		htmlBookMultiScene.addActionListener((java.awt.event.ActionEvent evt) -> {
			ckExportChapterBooktitle.setEnabled(htmlBookMultiScene.isSelected());
		});
		add(htmlBookMultiScene);
		group1.add(htmlBookMultiScene);
		
        ckExportChapterBooktitle = new JCheckBox(I18N.getMsg("export.chapter.booktitle"));
		ckExportChapterBooktitle.setSelected(param.getChapterBookTitle());
		add(ckExportChapterBooktitle,"wrap");
		
        ckExportChapterNumbers = new JCheckBox(I18N.getMsg("export.chapter.numbers"));
		ckExportChapterNumbers.setSelected(param.getChapterNumber());
		ckExportChapterNumbers.addActionListener((java.awt.event.ActionEvent evt) -> {
			if (ckExportChapterNumbers.isSelected()) {
				ckExportChapterNumbersRoman.setEnabled(true);
			} else {
				ckExportChapterNumbersRoman.setEnabled(false);
			}
			ckExportChapterBooktitle.setEnabled(htmlChapterMulti.isSelected());
		});
		add(ckExportChapterNumbers,"wrap");

        ckExportChapterNumbersRoman = new JCheckBox(I18N.getMsg("export.chapter.roman"));
		ckExportChapterNumbersRoman.setSelected(param.getChapterRoman());
		add(ckExportChapterNumbersRoman,"wrap");

        ckExportChapterTitles = new JCheckBox(I18N.getMsg("export.chapter.titles"));
		ckExportChapterTitles.setSelected(param.getChapterTitle());
		add(ckExportChapterTitles,"wrap");

        ckExportChapterDatesLocations = new JCheckBox(I18N.getMsg("export.chapter.date_location"));
		ckExportChapterDatesLocations.setSelected(param.getChapterDateLocation());
		add(ckExportChapterDatesLocations,"wrap");

        ckExportSceneDidascalie = new JCheckBox(I18N.getMsg("export.scene.didascalie"));
		ckExportSceneDidascalie.setSelected(param.getSceneDidascalie());
		add(ckExportSceneDidascalie,"wrap");

        ckExportSceneTitles = new JCheckBox(I18N.getMsg("export.scene.title"));
		ckExportSceneTitles.setSelected(param.getSceneTitle());
		add(ckExportSceneTitles,"wrap");

        ckExportSceneSeparator = new JCheckBox(I18N.getMsg("export.scene.separator"));
		ckExportSceneSeparator.setSelected(param.getSceneSeparator());
		add(ckExportSceneSeparator,"wrap");

		if (param.getHtmlMultiChapter()) {
			htmlBookOneFile.setSelected(false);
			htmlChapterMulti.setSelected(true);
			htmlBookMultiScene.setSelected(false);
		} else if (param.getHtmlMultiScene()) {
			htmlBookOneFile.setSelected(false);
			htmlChapterMulti.setSelected(false);
			htmlBookMultiScene.setSelected(true);
		} else {
			htmlBookOneFile.setSelected(true);
			htmlChapterMulti.setSelected(false);
			htmlBookMultiScene.setSelected(false);
		}
		ckExportChapterBooktitle.setEnabled(htmlChapterMulti.isSelected());
		
	}

	public void save() {
		param.setChapterBookTitle(ckExportChapterBooktitle.isSelected());
		param.setChapterDateLocation(ckExportChapterDatesLocations.isSelected());
		param.setChapterNumber(ckExportChapterNumbers.isSelected());
		param.setChapterRoman(ckExportChapterNumbersRoman.isSelected());
		param.setChapterTitle(ckExportChapterTitles.isSelected());
		param.setSceneDidascalie(ckExportSceneDidascalie.isSelected());
		param.setSceneSeparator(ckExportSceneSeparator.isSelected());
		param.setSceneTitle(ckExportSceneTitles.isSelected());
		if (htmlBookOneFile.isSelected()) {
			param.setHtmlMultiChapter(false);
			param.setHtmlMultiScene(false);
		} else {
			param.setHtmlMultiChapter(htmlChapterMulti.isSelected());
			param.setHtmlMultiScene(htmlBookMultiScene.isSelected());	
		}
	}
}
