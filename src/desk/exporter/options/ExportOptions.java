/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.exporter.options;

import db.I18N;
import desk.app.MainFrame;
import desk.app.pref.BookPrefExport;
import desk.panel.AbstractPanel;
import java.beans.PropertyChangeEvent;
import javax.swing.JTabbedPane;
import lib.miginfocom.swing.MigLayout;

/**
 *
 * @author favdb
 */
public class ExportOptions extends AbstractPanel {
	private JTabbedPane tabbed;
	private OptionsBook BOOK;
	private OptionsCsv CSV;
	private OptionsTxt TXT;
	private OptionsHtml HTML;
	private final BookPrefExport param;
	
	public ExportOptions(MainFrame mainFrame) {
		super(mainFrame);
		param=mainFrame.bookPref.Export;
		initAll();
	}

	@Override
	public void init() {
	}

	@Override
	public void initUi() {
		setLayout(new MigLayout(""));
        tabbed = new JTabbedPane();
		BOOK=new OptionsBook(mainFrame);
		tabbed.add(BOOK,I18N.getMsg("export.book.text"));
		CSV=new OptionsCsv(mainFrame);
		tabbed.add(CSV,"CSV");
		TXT=new OptionsTxt(mainFrame);
		tabbed.add(TXT,"TXT");
		HTML=new OptionsHtml(mainFrame);
		tabbed.add(HTML,"HTML");
		add(tabbed,"wrap, grow");
	}

	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
	}
	
	public void save() {
		BOOK.save();
		CSV.save();
		HTML.save();
		TXT.save();
		mainFrame.bookPref.save(mainFrame.xml.getOnlyPath());
	}
	
}
