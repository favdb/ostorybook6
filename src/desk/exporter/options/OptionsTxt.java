/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.exporter.options;

import java.awt.event.ItemEvent;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import desk.exporter.ExportParam;
import db.I18N;
import desk.app.MainFrame;
import desk.app.pref.BookPrefExport;

/**
 *
 * @author favdb
 */
public class OptionsTxt extends JPanel {
    public JRadioButton rbOther;
    public JTextField txSeparator;
    public JRadioButton rbTab;
	private final MainFrame mainFrame;
	private final BookPrefExport param;
	
	public OptionsTxt(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
		this.param=mainFrame.bookPref.Export;
		ButtonGroup group1 = new ButtonGroup();
        rbTab = new JRadioButton(I18N.getMsg("export.options.txt.separator.tab"));
        rbOther = new JRadioButton(I18N.getMsg("export.options.txt.separator.other"));
        rbOther.addItemListener((java.awt.event.ItemEvent evt) -> {
			if (evt.getStateChange()==ItemEvent.SELECTED) {
				txSeparator.setVisible(true);
			} else {
				txSeparator.setVisible(false);
			}
		});
        txSeparator = new JTextField();
		if (!param.getTxtTab()) {
			txSeparator.setText(param.getTxtSeparator());
			txSeparator.setVisible(true);
		} else {
			txSeparator.setVisible(false);
		}
		txSeparator.setColumns(2);

		rbTab.setSelected(param.getTxtTab());
		if (!rbOther.isSelected()) txSeparator.setVisible(false);
		group1.add(rbTab); group1.add(rbOther);

		//layout
		setLayout(new MigLayout("", "", ""));
		add(new JLabel("TXT "+I18N.getMsg("export.options.csv.separate")),"split 4");
		add(rbTab);
		add(rbOther);
		add(txSeparator);
	}

	public void save() {
		param.setTxtTab(rbTab.isSelected());
		param.setTxtSeparator(txSeparator.getText());
	}
	
}
