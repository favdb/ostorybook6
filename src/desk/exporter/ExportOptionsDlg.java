/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.exporter;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JTabbedPane;

import lib.miginfocom.swing.MigLayout;
import desk.app.App;

import desk.exporter.options.OptionsBook;
import desk.exporter.options.OptionsCsv;
import desk.exporter.options.OptionsHtml;
import desk.exporter.options.OptionsTxt;
import db.I18N;
import desk.app.MainFrame;
import desk.dialog.AbstractDialog;

/**
 *
 * @author favdb
 */
public class ExportOptionsDlg extends AbstractDialog {

	private final ExportParam param;
	private JTabbedPane tabbed;
	private OptionsBook BOOK;
	private OptionsCsv CSV;
	private OptionsTxt TXT;
	private OptionsHtml HTML;
	
	public ExportOptionsDlg(MainFrame parent) {
		super(parent);
		this.param=new ExportParam(mainFrame);
		initAll();
	}


	@Override
	public void init() {
	}
	
	@Override
	public void initUi() {
        tabbed = new JTabbedPane();
		BOOK=new OptionsBook(mainFrame);
		tabbed.add(BOOK,I18N.getMsg("export.book.text"));
		CSV=new OptionsCsv(mainFrame);
		tabbed.add(CSV,"CSV");
		TXT=new OptionsTxt(mainFrame);
		tabbed.add(TXT,"TXT");
		HTML=new OptionsHtml(mainFrame);
		tabbed.add(HTML,"HTML");

        //layout
		setLayout(new MigLayout(""));
        setTitle(I18N.getMsg("export.options"));
		add(tabbed,"wrap");
		add(buttonCancel(), "split 2,sg,right");
		add(buttonOk(), "sg, right");
		pack();
		setLocationRelativeTo(mainFrame);
		setModal(true);
	}
	
	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BOOK.save();
				CSV.save();
				HTML.save();
				TXT.save();
				param.save();
				dispose();
				App.getInstance().refresh();
			}
		};
	}

	public static void show(MainFrame m) {
		ExportOptionsDlg dlg=new ExportOptionsDlg(m);
		dlg.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
