/*
 * Copyright (C) 2018 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.exporter;

import db.entity.BookInfo;
import db.entity.Chapter;
import db.entity.Location;
import db.entity.Part;
import db.entity.SbDate;
import db.entity.Scene;
import db.util.BookUtil;
import desk.app.App;
import desk.app.MainFrame;
import desk.panel.reading.PrefViewReading;
import db.I18N;
import db.entity.Book;
import desk.tools.markdown.Markdown;
import java.awt.Font;
import java.awt.HeadlessException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.swing.JOptionPane;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import tools.IconUtil;
import tools.ListUtil;
import tools.StringUtil;
import tools.html.HtmlUtil;

/**
 * Export book to HTML (file or String) filename : - "index.html" : book general index - "bookTitle-Cnn.html" : chapter
 * index or content, nn=chapter - "bookTitle-Scc.ss.html" : scene content cc=chapter, ss=scene
 *
 * @author FaVdB
 */
public class ExportBook extends AbstractExporter {

	private Chapter firstChapter;
	private Chapter lastChapter;
	private final String indent = "&nbsp;&nbsp;&nbsp;&nbsp;";
	public boolean exportOnlyCurrentPart = false;
	public static boolean ONLY_CURRENT_PART = true, ALL_PARTS = false;
	public Part partToDo;
	public boolean tocLink = false;
	public static boolean TOC_LINKS = true, NO_TOC_LINKS = false;

	public ExportBook(MainFrame m, boolean bFile) {
		super(m, "Book", "html");
		book = mainFrame.book;
		if (bFile && (param.htmlMultiChapter || param.htmlMultiScene)) {
			String dir = param.dir + File.separator + "img";
			copyImage("empty", dir);
			copyImage("arrowleft", dir);
			copyImage("arrowright", dir);
			copyImage("arrowup", dir);
			copyImage("first", dir);
			copyImage("previous", dir);
			copyImage("summary", dir);
			copyImage("next", dir);
			copyImage("last", dir);
			copyImage("grayedfirst", dir);
			copyImage("grayedprevious", dir);
			copyImage("grayednext", dir);
			copyImage("grayedlast", dir);
		}
		init();
	}

	private void init() {
		firstChapter = book.chapters.get(0);
		lastChapter = book.chapters.get(book.chapters.size() - 1);
	}

	public static void toFile(MainFrame m) {
		//App.trace("ExportBook.toFile(mainFrame)");
		ExportBook export = new ExportBook(m, true);
		export.getFile();
		JOptionPane.showMessageDialog(m,
				I18N.getMsg("export.success"),
				I18N.getMsg("export"), JOptionPane.INFORMATION_MESSAGE);
	}

	public static String toString(MainFrame m) {
		//App.trace("ExportBookHtml.toString(mainFrame)");
		ExportBook export = new ExportBook(m, false);
		return (export.getString());
	}

	public static void toClipboard(MainFrame m, boolean onlyPart) {
		//App.trace("BookExporter.toClipboarb(mainFrame," + (onlyPart ? "current part" : "all part") + ")");
		ExportBook export = new ExportBook(m, false);
		export.exportOnlyCurrentPart = onlyPart;
		export.param.format = "html";
		export.param.htmlMultiChapter = false;
		export.param.htmlMultiScene = false;
		try {
			TextTransfer tf = new TextTransfer();
			tf.setClipboardContents(export.getString());
			JOptionPane.showMessageDialog(m,
					I18N.getMsg("clipboard.copy.book.confirmation"),
					I18N.getMsg("clipboard.copy.title"), 1);
		} catch (HeadlessException exc) {
		}
	}

	public static String toPanel(MainFrame m, Part part) {
		//App.trace("BookExporter.toPanel(mainFrame," + (onlyCurrentPart ? "current part" : "all part") + ")");
		ExportBook exp = new ExportBook(m, false);
		exp.param.format = "html";
		PrefViewReading reading = App.getInstance().getPreferences().reading;
		exp.param.reading=reading;
		exp.tocLink = true;
		exp.exportOnlyCurrentPart = (part != null);
		exp.partToDo = part;
		StringBuilder buf = new StringBuilder("<html>");
		buf.append("<head>\n<style type='text/css'>\n");
		buf.append(HtmlUtil.getScenarioCSS(true));
		buf.append("</style></head>\n");
		Font font = App.getInstance().fontGetMono();
		buf.append("<body>");
		buf.append("<a name=\"toc\"></a>");
		buf.append(exp.getTitle());
		buf.append(exp.getTocBook());
		buf.append(exp.getBody());
		buf.append("</body></html>");
		return (buf.toString());
	}

	public void getFile() {
		param.dir = mainFrame.getDefaultExportDir(mainFrame);
		String file=StringUtil.escapeTxt(book.getTitle()).replaceAll(" ", "_")+".html";
		if ((param.htmlMultiChapter || param.htmlMultiScene)) {
			file = "index.html";
		}
		String fileName=param.dir+File.separator+file;
		File f=new File(param.dir);
		if (!f.exists()) {
			f.mkdir();
		}
		if (askFileExists(fileName)) {
			if (JOptionPane.showConfirmDialog(mainFrame,
					I18N.getMsg("export.replace", fileName),
					I18N.getMsg("export"),
					JOptionPane.OK_CANCEL_OPTION) != JOptionPane.OK_OPTION) {
				return;
			}
		}
		param.fileName="";
		if (!openFile(file.replace(".html", ""))) {
			return;
		}
		getTitle();
		getTocBook();
		getBody();
		closeFile(!SILENT);
	}

	public String getString() {
		StringBuilder buf = new StringBuilder();
		buf.append(getTitle());
		buf.append(getTocBook());
		buf.append(getBody());
		return (buf.toString());
	}

	private String getBookTitle() {
		return (book.getTitle());
	}

	private String getTitle() {
		BookInfo p = book.info;
		//App.trace("ExportBookHtml.getTitle()");
		String t = "<h1>" + p.getTitle() + "</h1>\n";
		t += "<p style=\"text-align: center;\"><b>" + p.getSubtitle() + "</b><br>\n";
		t += I18N.getMsg("book.author") + " " + p.getAuthor() + "<br>\n";
		t += "<small><i>" + p.getCopyright() + "</i></small><br>\n";
		t += "<small><i>"
				+ I18N.getMsg("export.by") + " " + App.FULLNAME
				+ "</i></small></p>\n";
		t += "<p>" + p.getBlurb() + "</p>\n";
		writeText(t);
		return (t);
	}

	private String getTocBook() {
		//App.trace("ExportBook.getBookTOC()");
		StringBuilder buf = new StringBuilder();
		if (isOpened) {
			buf.append("<h2>").append(I18N.getMsg("export.toc")).append("</h2>\n");
		}
		if (book.parts == null) {
			return ("");
		}
		book.parts.forEach((part) -> {
			if ((exportOnlyCurrentPart && part.equals(mainFrame.getCurrentPart()))
					|| (partToDo != null && part.equals(partToDo))) {
				if (book.parts.size() > 1) {
					buf.append("<font size=\"5\">");
					buf.append(part.name);
					buf.append("</font><br>\n");
				}
			}
			if (book.chapters.size()>1) book.chapters.forEach((chapter) -> {
				if (chapter.hasPart() && chapter.part.id.equals(part.id)) {
					buf.append(indent).append(linkToChapter(chapter, "")).append("<br>\n");
					if (isOpened && param.htmlMultiScene) {
						book.scenes.forEach((scene) -> {
							if (scene.chapter.id.equals(chapter.id) && !scene.informative) {
								buf.append(indent).append(indent).append(linkToScene(scene, null)).append("<br>\n");
							}
						});
					}
				}
			});
		});
		if (isOpened && (param.htmlMultiChapter || param.htmlMultiScene)) {
			buf.append(getNavBar(null, null, firstChapter));
			writeText(buf.toString());
			closeFile(SILENT);
			return ("");
		} else {
			writeText(buf.toString());
		}
		return (buf.toString());
	}

	private String linkToScene(Scene scene, String img) {
		//App.trace("ExportBookHtml.linkToScene("+
		//	(scene!=null?scene.name:"null")+
		//	","+(img==null?"null":img)+")");
		if (scene == null) {
			return ("scene empty and img empty");
		}
		String txt = (img != null ? img : scene.name);
		if (isOpened && param.htmlMultiScene) {
			return (String.format("<a href=\"%s\">%s</a>", getFilenameScene(scene), txt));
		}
		return (String.format("<a href=\"#S%d.%d\">%s</a>", scene.chapter.id, scene.id, txt));
	}

	private String linkToChapter(Chapter chapter, String img) {
		//App.trace("ExportBookHtml.linkToChapter("+
		//	(chapter!=null?chapter.name:"null")+
		//	","+(img==null?"null":img)+")");
		if (chapter == null) {
			return (String.format("<img src=\"img/%s.png\">", img));
		}
		String title = chapter.name;
		if (param.reading.chapterNumber) {
			if (param.reading.chapterRoman) {
				title = (String) StringUtil.intToRoman(chapter.number) + " " + title;
			} else {
				title = chapter.number + " " + title;
			}
		}
		if (img != null && !img.isEmpty()) {
			title = String.format("<img src=\"img/%s.png\" title=\"%s\" alt=\"%s\">", img, title, title);
			if (!param.htmlNavImage) {
				title = I18N.getMsg("nav." + img);
			}
		}
		if (isOpened && (param.htmlMultiChapter || param.htmlMultiScene)) {
			return (String.format("<a href=\"%s\">%s</a>", getFilenameChapter(chapter), title));
		}
		return (String.format("<a href=\"#%d\">%s</a>", chapter.id, title));
	}

	private String getBody() {
		//App.trace("ExportBook.getBody()");
		StringBuilder buf = new StringBuilder();
		Chapter priorChapter = null, chapter, nextChapter = book.chapters.get(0);
		Scene priorScene, nextScene;
		for (Part part : book.parts) {
			if (exportOnlyCurrentPart) {
				if (!mainFrame.getCurrentPart().equals(part)) {
					continue;
				}
			} else {
				if (partToDo != null && !part.equals(partToDo)) {
					continue;
				}
			}
			if (!book.parts.isEmpty()) buf.append(getPartTitle(part));
			List<Chapter> chapters = Chapter.find(book.chapters, part);
			for (int i = 0; i < chapters.size(); i++) {
				chapter = chapters.get(i);
				if (param.htmlMultiChapter || param.htmlMultiScene) {
					buf.append(getNavBar(priorChapter, chapter, nextChapter));
					writeText(buf.toString());
					closeFile(SILENT);
					openFile(getFilenameChapter(chapter).replace(".html", ""));
					buf = new StringBuilder();
				}
				if (i < chapters.size() - 1) {
					nextChapter = chapters.get(i + 1);
				}
				if (i > 0) {
					priorChapter = chapters.get(i - 1);
				}
				if (chapter.hasPart() && chapter.part.id.equals(part.id)) {
					buf.append(getChapterTitle(chapter));
					List<Scene> list_scene = book.listScenes(chapter);
					if (param.htmlMultiScene) {
						buf.append("<p>");
						for (Scene scene : list_scene) {
							buf.append(linkToScene(scene, null)).append("<br>\n");
						}
						buf.append("</p>");
						buf.append(getNavBarScene(null, null, list_scene.get(0)));
						buf.append(getNavBar(priorChapter, chapter, nextChapter));
						writeText(buf.toString());
						closeFile(SILENT);
						buf = new StringBuilder();
					}
					for (int s = 0; s < list_scene.size(); s++) {
						Scene scene = list_scene.get(s);
						if (scene.informative) {
							continue;
						}
						priorScene = (s > 0 ? list_scene.get(s - 1) : null);
						nextScene = (s < list_scene.size() - 1 ? list_scene.get(s + 1) : null);
						String txtScene = getScene(scene);
						if (!HtmlUtil.htmlToText(txtScene).isEmpty()) {
							if (param.htmlMultiScene) {
								openFile(getFilenameScene(scene).replace(".html", ""));
								buf = new StringBuilder();
								buf.append(txtScene);
								buf.append(getNavBarScene(priorScene, chapter, nextScene));
								buf.append(getNavBar(priorChapter, chapter, nextChapter));
								writeText(buf.toString());
								closeFile(SILENT);
								buf = new StringBuilder();
							} else {
								buf.append(txtScene);
							}
							if (tocLink) {
								buf.append("<p style='text-align:left;'><a href=\"#toc\">")
										.append(I18N.getMsg("toc"))
										.append("</a></p>\n");
							}
						}
					}
				}
			}
		}
		if (isOpened && (param.htmlMultiChapter || param.htmlMultiScene)) {
			int i = book.chapters.size();
			if (i > 1) {
				i -= 2;
			} else {
				i = -1;
			}
			buf.append(getNavBar(book.chapters.get(i), null, null));
		}
		writeText(buf.toString());
		return (buf.toString());
	}

	private String getScene(Scene scene) {
		//App.trace("ExportBook.getScene("+scene.name+")");
		StringBuilder buf = new StringBuilder("<a name=\"" + scene.chapter.id + "." + scene.id + "\"></a>");
		if (param.reading.sceneTitle) {
			String t = scene.name;
			if (!t.isEmpty()) {
				buf.append(HtmlUtil.getHtag("." + t));
			}
		}
		if (param.reading.sceneDidascalie) {
			buf.append(getDidascalie(scene));
		}
		String x = scene.writing;
		if (book.isScenario()) {
			StringBuilder b = new StringBuilder();
			b.append(scene.scenario.getHeader(scene.getFirstLocation(), scene.scenario));
			b.append(Markdown.toHtml(scene.writing));
			b.append(scene.scenario.getFooter());
			x = b.toString();
		}
		if (x.isEmpty()) {
			return ("");
		}
		if (x.contains("#chapscene")) {
			x = replaceLinkInternal(x);
		}
		buf.append(x).append("\n");
		if (param.reading.sceneSeparator) {
			buf.append("<p style=\"text-align:center\">.oOo.</p>\n");
		}
		return (buf.toString());
	}

	public String getDidascalie(Scene scene) {
		//App.trace("ExportBookHtml.getDidascalie("+scene.name+")");
		StringBuilder rc =new StringBuilder();
		if (param.reading.sceneDidascalie) {
			if (!scene.persons.isEmpty()) {
				rc.append("<i><b>").append(I18N.getMsg("persons")).append("</b> : ");
				rc.append(Book.getNames(scene.persons));
				rc.append("</i><br>\n");
			}
			if (!scene.locations.isEmpty()) {
				rc.append("<i><b>").append(I18N.getMsg("locations")).append("</b> : ");
				rc.append(Book.getNames(scene.locations));
				rc.append("</i><br>\n");
			}
			if (!scene.items.isEmpty()) {
				rc.append("<i><b>").append(I18N.getMsg("items")).append("</b> : ");
				rc.append(Book.getNames(scene.items));
				rc.append("</i><br>\n");
			}
			if (!rc.toString().isEmpty()) {
				rc.append("<p style=\"text-align:right\">").append(rc).append("</p>");
			}
		}
		return (rc.toString());
	}

	private String replaceLinkInternal(String x) {
		//App.trace("ExportBookHtml.replaceLinkInternal("+TextUtil.ellipsize(x, 32)+")");
		if (!x.contains("#chapscene")) {
			return (x);
		}
		Document doc = Jsoup.parse(x);
		Elements links = doc.select("a");
		for (Element e : links) {
			if (e.attr("href").contains("#chapscene")) {
				String r = e.attr("href").replace("#chapscene", "");
				String[] y = r.split("\\.");
				if (y.length < 1) {
					continue;
				}
				String chap = y[0];
				String scene = "";
				if (y.length > 1) {
					scene = chap + "." + y[1];
				}
				if ("".equals(chap)) {
					continue;
				}
				if (chap.length() < 2) {
					chap = "0" + chap;
				}
				if (param.htmlMultiChapter) {
					r = getBookTitle() + "-C" + chap + ".html";
					if (!"".equals(scene)) {
						r += "#" + scene;
					}
				} else if (param.htmlMultiScene) {
					r = getBookTitle() + "-S" + r + ".html";
				} else {
					r = "#" + r;
				}
				e.attr("href", r);
			}
		}
		String z = (doc.select("body").get(0).children().toString());
		return (z);
	}

	private String getNavBarScene(Scene priorScene, Chapter chapter, Scene nextScene) {
		StringBuilder buf = new StringBuilder("<hr><p style=\"text-align: right;\">");
		String img;
		String link = String.format("<img src=\"img/empty.png\">");
		if (priorScene != null) {
			String x = I18N.getMsg("export.nav.previous");
			img = String.format("<img src=\"img/arrowleft.png\" title=\"%s\" alt=\"%s\">", x, x);
			link = linkToScene(priorScene, img);
		}
		buf.append(link).append(" &nbsp; ");
		if (chapter != null) {
			buf.append(linkToChapter(chapter, "arrowup")).append(" &nbsp; ");
		}
		link = String.format("<img src=\"img/empty.png\">");
		if (nextScene != null) {
			String x = I18N.getMsg("export.nav.next");
			img = String.format("<img src=\"img/arrowright.png\" title=\"%s\" alt=\"%s\">", x, x);
			link = linkToScene(nextScene, img);
		}
		buf.append(link).append(" &nbsp; ");
		return (buf.toString());
	}

	private String getNavBar(Chapter prior, Chapter current, Chapter next) {
		if (!param.htmlNav) {
			return ("");
		}
		String imgToc = I18N.getMsg("export.nav.summary");
		String toc = String.format("<img src=\"img/summary.png\" title=\"%s\" alt=\"%s\">", imgToc, imgToc);
		if (!param.htmlNavImage) {
			toc = imgToc;
		}
		if (current != null) {
			toc = "<a href=\"index.html\">" + toc + "</a> | ";
		} else {
			toc = toc + " | ";
		}
		String firstLink = linkToChapter(null, "grayedfirst") + " | ";
		if (prior != null) {
			firstLink = linkToChapter(firstChapter, "first") + " | ";
		}
		String priorLink = linkToChapter(null, "grayedprevious") + " | ";
		if (prior != null) {
			priorLink = linkToChapter(prior, "previous") + " | ";
		}
		String nextLink = linkToChapter(null, "grayednext") + " | ";
		if (next != null) {
			nextLink = linkToChapter(next, "next") + " | ";
		}
		String lastLink = linkToChapter(null, "grayedlast") + "</p>\n";
		if (next != null) {
			lastLink = linkToChapter(lastChapter, "last") + "</p>\n";
		}
		return ("<p></p><p style=\"background-color: lightgrey;\">"
				+ firstLink + priorLink + toc + nextLink + lastLink + "</p>");
	}

	public String getPartTitle(Part part) {
		//App.trace("ExportBookHtml.getPartTitle("+part.name+")");
		StringBuilder buf = new StringBuilder();
		if (book.parts.size() > 1) {
			if (param.reading.partTitle) {
				buf.append("<h1>");
				if (param.reading.partNumber) {
					if (param.reading.partRoman) {
						buf.append(StringUtil.intToRoman(part.number));
					} else {
						buf.append(Integer.toString(part.number));
					}
					buf.append(" : ");
				}
				buf.append(part.name);
				buf.append("</h1>\n");
			}
		}
		return (buf.toString());
	}

	@SuppressWarnings("unchecked")
	public String getChapterTitle(Chapter chapter) {
		//App.trace("ExportBookHtml.getChapterTitle(...)");
		StringBuilder buf = new StringBuilder("<a name=\"").append(chapter.id).append("\"></a><h2>");
		if (param.reading.chapterNumber) {
			if (param.reading.chapterRoman) {
				buf.append(StringUtil.intToRoman(chapter.number));
			} else {
				buf.append(Integer.toString(chapter.number));
			}
			if (param.reading.chapterTitle) {
				buf.append(": ").append(chapter.name);
			}
		} else if (param.reading.chapterTitle) {
			buf.append(chapter.name);
		}
		buf.append("</h2>\n");
		if (param.reading.chapterDateLoc) {
			buf.append("<p><i>");
			buf.append(SbDate.getNiceDates(BookUtil.findDistinctDates(book, chapter)));
			if (!Location.findByChapter(book.scenes, chapter).isEmpty()) {
				buf.append(": ").append(ListUtil.join(Location.findByChapter(book.scenes, chapter), ", "));
			}
			buf.append("</i></p>");
		}
		return (buf.toString());
	}

	private void copyImage(String img, String dir) {
		//App.trace("ExportBookHtml.copyImage(" + img + "," + dir + ")");
		File fdir = new File(dir);
		fdir.mkdirs();
		File out = new File(dir + File.separator + img + ".png");
		if (out.exists()) {
			return;
		}
		InputStream resource = IconUtil.getResourceAsStream("small/" + img);
		if (resource == null) {
			System.err.println("ExportBookHtml copyImage InputStream error on " + img + ".png");
			return;
		}
		try {
			InputStream in = resource;
			try (OutputStream writer = new BufferedOutputStream(new FileOutputStream(out))) {
				byte[] buffer = new byte[1024];
				int length;
				while ((length = in.read(buffer)) >= 0) {
					writer.write(buffer, 0, length);
				}
			}
		} catch (IOException ex) {
			System.err.println("ExportBookHtml.copyImage " + ex.getLocalizedMessage());
		}
	}

	private String getFilenameChapter(Chapter chapter) {
		return (String.format("%s-C%02d.html", getBookTitle(), chapter.id));
	}

	private String getFilenameScene(Scene scene) {
		return (String.format("%s-S%d.%d.html", getBookTitle(), scene.chapter.id, scene.id));
	}

}
