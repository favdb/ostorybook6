package desk.model;

import db.entity.AbstractEntity;
import db.entity.Book;
import desk.app.MainFrame;
import desk.view.SbView;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public abstract class AbstractModel {

	public PropertyChangeSupport support;
	MainFrame mainFrame;

	public AbstractModel(MainFrame m) {
		mainFrame=m;
		support = new PropertyChangeSupport(this);
	}

	public abstract void fireAgain();

	public void initDefault() {
		fireAgain();
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		//App.trace("AbstractModel.addPropertyChangeListener("+l.toString()+")");
		support.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		support.removePropertyChangeListener(l);
	}

	protected void firePropertyChange(Ctrl.ACTION action, Object oldValue, Object newValue) {
		Book.TYPE type = Book.TYPE.NONE;
		if (oldValue!=null && oldValue instanceof AbstractEntity) {
			type=Book.getTYPE((AbstractEntity) oldValue);
		} else if (newValue!=null && newValue instanceof AbstractEntity) {
			type=Book.getTYPE((AbstractEntity) newValue);
		} else if (newValue instanceof SbView) {
			firePropertyChange(action.toString(), oldValue, newValue);
		}
		else return;
		firePropertyChange(type, action, oldValue, newValue);
	}
	protected void firePropertyChange(Book.TYPE type, Ctrl.ACTION action, Object oldValue, Object newValue) {
		String name=type.toString();
		String prop=type.toString()+"_"+action.toString();
		/*App.trace("AbstractModel.firePropertyChange("+
			"propertyName="+propertyName+
			", oldValue="+(oldValue!=null?oldValue.toString():"null")+
			", newValue="+(newValue!=null?newValue.toString():"null")+")");*/
		support.firePropertyChange(prop, oldValue, newValue);
	}

	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		/*App.trace("AbstractModel.firePropertyChange("+
			"propertyName="+propertyName+
			", oldValue="+(oldValue!=null?oldValue.toString():"null")+
			", newValue="+(newValue!=null?newValue.toString():"null")+")");*/
		support.firePropertyChange(propertyName, oldValue, newValue);
	}

}
