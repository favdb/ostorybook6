/*
 Storybook: Open Source software for novelists and authors.
 Copyright (C) 2008 - 2012 Martin Mustun

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.model;

import db.entity.AbstractEntity;
import db.entity.Book;
import desk.app.App;
import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.chrono.Chrono;
import desk.panel.manage.Manage;
import desk.panel.memoria.Memoria;
import desk.panel.reading.Reading;
import desk.panel.storyboard.Storyboard;
import desk.panel.work.Work;
import desk.view.SbView;
import desk.view.SbView.VIEWID;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.JMenuBar;

/**
 * Cette classe controle les opérations sur la base de donnéeset pilote les différentes
 * interfaces en faisant appel à la classe BookModel
 *
 */
public class Ctrl implements PropertyChangeListener {

	public MainFrame mainFrame;
	private List<Component> attachedViews = new ArrayList<>();
	private List<AbstractModel> attachedModels = new ArrayList<>();

	public Ctrl(MainFrame m) {
		super();
		//App.trace("Controller(mainFrame)");
		attachedViews = new CopyOnWriteArrayList<>();
		attachedModels = new ArrayList<>();
		mainFrame = m;
	}

	public void attachModel(AbstractModel model) {
		//App.trace("Ctrl.attachModel(" + model.toString() + ")");
		attachedModels.add(model);
		model.addPropertyChangeListener(this);
		printAttachedModels();
	}

	public void detachModel(AbstractModel model) {
		//App.trace("Ctrl.detachModel(" + model.toString() + ")");
		attachedModels.remove(model);
		model.removePropertyChangeListener(this);
		printAttachedModels();
	}

	@SuppressWarnings("SynchronizeOnNonFinalField")
	public void attachView(Component view) {
		//App.trace("Ctrl.attachView(" + view.toString() + ")");
		if (attachedViews.contains(view)) {
			return;
		}
		synchronized (attachedViews) {
			attachedViews.add(view);
		}
		//App.trace(getListAttachedViews());
	}

	@SuppressWarnings("SynchronizeOnNonFinalField")
	public void detachView(Component view) {
		//App.trace("Ctrl.detachView(view=" + view.toString() + ")");
		synchronized (attachedViews) {
			attachedViews.remove(view);
		}
		/*if (App.getTrace()) {
			printAttachedViews();
		}*/
	}

	public void printNumberOfAttachedViews() {
		//App.trace("number of attached views=" + attachedViews.size());
	}

	@SuppressWarnings({"EmptySynchronizedStatement", "SynchronizeOnNonFinalField"})
	public void printAttachedViews() {
		synchronized (attachedViews) {
			//App.trace(getInfoAttachedViews());
		}
	}

	public String getInfoAttachedViews() {
		return getInfoAttachedViews(false);
	}

	public String getInfoAttachedViews(boolean html) {
		StringBuilder buf = new StringBuilder();
		int i = 0;
		int size = attachedViews.size();
		for (Component view : attachedViews) {
			buf.append("view ")
					.append(i).append("/")
					.append(size).append(" : ")
					.append(view.getClass().getSimpleName());
			if (view.getName() != null) {
				buf.append("/")
						.append(view.getName());
			}
			if (html) {
				buf.append("\n<br>");
			} else {
				buf.append("\n");
			}
			++i;
		}
		return buf.toString();
	}

	public void printAttachedModels() {
		//App.trace("Ctrl.printAttachedModels()");
		for (AbstractModel model : attachedModels) {
			//App.trace("model ->" + model.toString());
		}
	}

	@Override
	@SuppressWarnings("SynchronizeOnNonFinalField")
	public void propertyChange(PropertyChangeEvent evt) {
		//App.trace("Ctrl.propertyChange(" + evt.toString() + ")");
		synchronized (attachedViews) {
			// must be in synchronized block
			attachedViews.forEach((comp) -> {
				if (comp instanceof AbstractPanel) {
					((AbstractPanel) comp).modelPropertyChange(evt);
				} else if (comp instanceof JMenuBar) {
					JMenuBar mb = (JMenuBar) comp;
					PropertyChangeListener[] pcls = mb.getPropertyChangeListeners();
					for (PropertyChangeListener pcl : pcls) {
						pcl.propertyChange(evt);
					}
				} else if (comp instanceof App) {
					((App) comp).modelPropertyChange(evt);
				}
			});
		}
	}

	public synchronized void fireAgain() {
		//App.trace("Ctrl.fireAgain()");
		attachedModels.forEach((model) -> {
			model.fireAgain();
		});
	}

	protected synchronized void setModelProperty(Ctrl.ACTION action, Object value) {
		if (value instanceof AbstractEntity) {
			setModelProperty(action.toString(), value);
		}
	}

	protected synchronized void setModelProperty(String propertyName, Object value) {
		/*App.trace("Ctrl.setModelProperty("
				+ propertyName + "," + (newValue == null ? "null" : newValue.toString()) + ")");*/
		if (value != null) {
			attachedModels.forEach((model) -> {
				String cpt="", cpl="";
				String pName="";
				Method method = null;
				Class<?>[] classes = null;
				try {
					if (value instanceof AbstractEntity) {
						classes = new Class[]{AbstractEntity.class};
						AbstractEntity entity=(AbstractEntity)value;
						cpt=entity.type.toString()+": "+entity.name;
						cpl="Entity";
					} else {
						classes = new Class[]{value.getClass()};
					}
					pName="set"+ cpl + propertyName;
					System.out.println("cpl="+cpl);
					method = model.getClass().getMethod(pName, classes);
					method.invoke(model, value);
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException e) {
					if (e.getCause() != null) {
						@SuppressWarnings("ThrowableResultIgnored")
						String emsg = (e.getCause() == null ? "null" : e.getCause().toString());
						String emethod = (method == null ? "null" : method.getName());
						System.err.println("*** : Ctrl.setModelProperty : "+cpt+"\n"
								+ "emsg=" + emsg
								+ "\nmodel=" + model.getClass().getName()
								+ "\nmethod=" + emethod
								+ "\nclasse=" + classes[0].getName());
					} else {
						System.err.println("*** : Ctrl.setModelProperty() "+pName);
						System.err.println("*** : e.getCause()==null");
					}
					e.printStackTrace(System.err);
				}
			});
		}
	}

	protected synchronized void setModelProperty(String propertyName) {
		//App.trace("Ctrl.setModelProperty(" + propertyName + ")");
		attachedModels.forEach((model) -> {
			try {
				Method method = model.getClass().getMethod("set" + propertyName);
				method.invoke(model);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException ex) {
			}
		});
	}

	public int getNumberOfAttachedViews() {
		return (attachedViews.size());
	}

	public List<Component> getAttachedViews() {
		return (attachedViews);
	}

	public List<AbstractModel> getAttachedModels() {
		return attachedModels;
	}

	public String getListAttachedViews() {
		StringBuilder buf = new StringBuilder();
		attachedViews.forEach((view) -> {
			buf.append("view ").append(view.getClass().getSimpleName());
			if (view.getName() != null) {
				buf.append(", name=").append(view.getName());
			}
			buf.append("\n");
		});
		return buf.toString();
	}

	public enum ACTION {

		// views action
		EXPORT("Export"),
		PRINT("Print"),
		REFRESH("Refresh"),
		SHOW_OPTIONS("ShowOptions"),
		SHOW_INFO("ShowInfo"),
		SHOW_MANAGE("ShowManage"),
		SHOW_MEMO("ShowMemo"),
		SHOW_MEMORIA("ShowMemoria"),
		UNLOAD_EDITOR("UnloadEditor"),
		// entities actions
		INIT("Init"),
		EDIT("Edit"),
		DELETE("Delete"),
		DELETE_MULTI("DeleteMulti"),
		NEW("New"),
		ORDER_UP("OrderUp"),
		ORDER_DOWN("OrderDown"),
		CHANGE("Change"),
		UPDATE("Update"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION whichACTION(String str) {
		for (ACTION v : ACTION.values()) {
			if (v.check(str)) {
				return (v);
			}
			if (str.contains(v.text)) {
				return (v);
			}
		}
		return (ACTION.NONE);
	}

	public static Book.TYPE whichENTITY(String propName) {
		String s[]=propName.split("_");
		return(Book.getTYPE(s[0]));
	}
//****************
//*** Views    ***
//****************
	public enum CHRONO {

		ZOOM("Chrono_Zoom"),
		LAYOUT_DIRECTION("Chrono_LayoutDirection"),
		SHOW_DATE_DIFFERENCE("Chrono_ShowDateDifference"),
		SHOW_ENTITY("Chrono_ShowEntity"),
		NONE("none");
		final private String text;

		private CHRONO(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static CHRONO whichCHRONO(String str) {
		for (CHRONO v : CHRONO.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (CHRONO.NONE);
	}

	public enum MANAGE {

		COLUMNS("Manage_Columns"),
		HIDE_UNASSIGNED("Manage_HideUnassigned"),
		SHOW_ENTITY("Manage_ShowEntity"),
		ZOOM("Manage_Zoom"),
		NONE("none");
		final private String text;

		private MANAGE(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static MANAGE whichMANAGE(String str) {
		for (MANAGE v : MANAGE.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (MANAGE.NONE);
	}

	public enum MEMORIA {

		BALLOON("Memoria_Balloon"),
		NONE("none");
		final private String text;

		private MEMORIA(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static MEMORIA whichMEMORIA(String str) {
		for (MEMORIA v : MEMORIA.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (MEMORIA.NONE);
	}

	public enum READING {

		ZOOM("Reading_Zoom"),
		FONT_SIZE("Reading_FontSize"),
		NONE("none");
		final private String text;

		private READING(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static READING whichREADING(String str) {
		for (READING v : READING.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (READING.NONE);
	}

	public enum STORYBOARD {

		LAYOUT_DIRECTION("Storyboard_LayoutDirection"),
		SHOW_DATEDIFF("Storyboard_ShowDateDiff"),
		SHOW_ENTITY("Storyboard_ShowEntity"),
		ZOOM("Storyboard_Zoom"),
		NONE("none");
		final private String text;

		private STORYBOARD(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static STORYBOARD whichSTORYBOARD(String str) {
		for (STORYBOARD v : STORYBOARD.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (STORYBOARD.NONE);
	}

	public enum TREE {
		REFRESH("Tree_Refresh"),
		SHOW_INFO("Work_ShowInfo"),
		NONE("none");
		final private String text;

		private TREE(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static TREE whichTREE(String str) {
		for (TREE v : TREE.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (TREE.NONE);
	}

	public enum WORK {
		ZOOM("Work_Zoom"),
		HEIGHT_FACTOR("Work_HeightFactor"),
		SHOW_ENTITY("Work_ShowEntity"),
		NONE("none");
		final private String text;

		private WORK(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static WORK whichWORK(String str) {
		for (WORK v : WORK.values()) {
			if (v.check(str)) {
				return (v);
			}
		}
		return (WORK.NONE);
	}

//****************
//*** Entities ***
//****************
	public static String getAction(AbstractEntity entity, ACTION action) {
		return (entity.type.toString() + "_" + action.text);
	}

	public static String getActionEntity(Book.TYPE entity, ACTION action) {
		return (entity.toString() + "_" + action.text);
	}

	public static Book.TYPE getEntity(String str) {
		for (Book.TYPE t : Book.TYPE.values()) {
			if (str.toLowerCase().contains(t.toString())) {
				return (t);
			}
		}
		return (Book.TYPE.NONE);
	}

	public void updateEntity(AbstractEntity entity) {
		//App.trace("Ctrl.updateEntity("+entity.getClass().name+")");
		setModelProperty(ACTION.UPDATE, entity);
	}

	public void deleteEntity(AbstractEntity entity) {
		//App.trace("Controller.deleteEntity("+entity.getClass().name+")");
		if (entity==null) return;
		try {
			setModelProperty(ACTION.DELETE, entity);
		} catch (Exception e) {
			App.error("Ctrl.deleteEntity(" + entity.getClass().getName() + ")\nException:", e);
		}
	}

	public void deleteMultiEntities(String name, List<Long> ids) {
		//App.trace("Ctrl.deleteMultiEntity("+name+",ids nb="+ids.size()+")");
		try {
			ids.forEach((id) -> {
				AbstractEntity entity = mainFrame.book.getEntity(id);
				if (entity != null) {
					deleteEntity(entity);
				}
			});
		} catch (Exception e) {
			App.error("Ctrl.deleteMultiEntities(name=" + name + ")\nException:", e);
		}
	}

	public void newEntity(AbstractEntity entity) {
		//App.trace("Ctrl.newEntity("+entity.getClass().name+")");
		if (entity==null) return;
		try {
			setModelProperty(ACTION.NEW, entity);
		} catch (Exception e) {
			App.error("Ctrl.newEntity(" + entity.type.toString() + ")\nException:", e);
		}
	}

	public void changeEntity(AbstractEntity entity) {
		//App.trace("Ctrl.changeEntity("+entity.getClass().name+")");
		if (entity==null) return;
		try {
			setModelProperty(ACTION.CHANGE, entity);
		} catch (Exception e) {
			App.error("Ctrl.changeEntity(" + entity.type.toString() + ")\nException:", e);
		}
	}

	public void editEntity(AbstractEntity entity) {
		//App.trace("Ctrl.editEntity("+entity.getClass().name+")");
		try {
			setModelProperty(ACTION.EDIT.toString(), entity);
		} catch (Exception e) {
			App.error("Ctrl.setEntityToEdit(" + entity.getClass().getName() + ")\nException:", e);
		}
	}

	// common
	public void refresh(SbView view) {
		setModelProperty(ACTION.REFRESH, view);
	}

	public void refresh(VIEWID view) {
		refresh(mainFrame.getView(view));
	}

	public void showOptions(SbView view) {
		setModelProperty(ACTION.SHOW_OPTIONS, view);
	}

	public void print(SbView view) {
		setModelProperty(ACTION.PRINT, view);
	}

	public void export(SbView view) {
		setModelProperty(ACTION.EXPORT, view);
	}

	public void showMemo(AbstractEntity entity) {
		setModelProperty(ACTION.SHOW_MEMO, entity);
	}

	public void showInfo(AbstractEntity entity) {
		setModelProperty(ACTION.SHOW_INFO, entity);
	}

	public void showInManage(AbstractEntity entity) {
		setModelProperty(ACTION.SHOW_MANAGE, entity);
	}

	public void showInMemoria(AbstractEntity entity) {
		setModelProperty(ACTION.SHOW_MEMORIA, entity);
	}

	public void unloadEditor() {
		setModelProperty(ACTION.UNLOAD_EDITOR, null);
	}

	// tools
	public void showTaskList() {
		setModelProperty(ACTION.REFRESH.toString(), null);
	}

	// chrono view
	public void chronoSetZoom(Integer val) {
		setModelProperty(Chrono.ACTION.ZOOM.toString(), val);
	}

	public void chronoSetLayoutDirection(Boolean val) {
		setModelProperty(Chrono.ACTION.LAYOUTDIRECTION.toString(), val);
	}

	public void chronoSetShowDateDifference(Boolean val) {
		setModelProperty(Chrono.ACTION.SHOW_DATEDIFFERENCE.toString(), val);
	}

	public void chronoShowEntity(AbstractEntity entity) {
		setModelProperty(Chrono.ACTION.SHOW_ENTITY.toString(), entity);
	}

	// storyboard view
	public void storyboardShowEntity(AbstractEntity entity) {
		setModelProperty(Storyboard.ACTION.SHOW_ENTITY.toString(), entity);
	}

	// book view
	public void bookSetZoom(Integer val) {
		setModelProperty(Work.ACTION.ZOOM.toString(), val);
	}

	public void bookSetHeightFactor(Integer val) {
		setModelProperty(Work.ACTION.HEIGHT.toString(), val);
	}

	public void bookShowEntity(AbstractEntity entity) {
		setModelProperty(Work.ACTION.SHOW_ENTITY.toString(), entity);
	}

	// manage view
	public void manageSetZoom(Integer val) {
		setModelProperty(Manage.ACTION.ZOOM.toString(), val);
	}

	public void manageSetColumns(Integer val) {
		setModelProperty(Manage.ACTION.COLUMNS.toString(), val);
	}

	public void manageSetHideUnassigned(Boolean val) {
		setModelProperty(Manage.ACTION.HIDE_UNASSIGNED.toString(), val);
	}

	public void manageShowEntity(AbstractEntity entity) {
		setModelProperty(Manage.ACTION.SHOW_ENTITY.toString(), entity);
	}

	// reading view
	public void readingSetZoom(Integer val) {
		setModelProperty(Reading.ACTION.ZOOM.toString(), val);
	}

	public void readingSetFontSize(Integer val) {
		setModelProperty(Reading.ACTION.FONTSIZE.toString(), val);
	}

	// memoria view
	public void memoriaSetBalloonLayout(Boolean val) {
		setModelProperty(Memoria.ACTION.BALLOON.toString(), val);
	}

	// entites
	public void orderUpPov(AbstractEntity entity) {
		setModelProperty(Ctrl.ACTION.ORDER_UP.toString(), entity);
	}

	public void orderDownPov(AbstractEntity entity) {
		setModelProperty(Ctrl.ACTION.ORDER_DOWN.toString(), entity);
	}

}
