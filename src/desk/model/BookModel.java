/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun, 2015 FaVdB

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.model;

import java.util.List;
import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Pov;
import db.entity.Tag;
import db.entity.Event;
import db.entity.Photo;
import db.entity.Plot;
import desk.app.App;
import desk.app.MainFrame;
import desk.view.SbView;
import desk.panel.work.Work;
import desk.panel.chrono.Chrono;
import desk.panel.manage.Manage;
import desk.panel.memos.Memos;
import desk.panel.memoria.Memoria;
import desk.panel.reading.Reading;
import desk.view.SbView.VIEWID;

/**
 * @author martin
 *
 */
public class BookModel extends AbstractModel {

	private final Book book;

	public BookModel(MainFrame m) {
		super(m);
		book = m.book;
	}

	@Override
	public void fireAgain() {
//		App.trace("BookModel.fireAgain()");
		for (Book.TYPE type : Book.TYPE.values()) {
			firePropertyChange(type, Ctrl.ACTION.INIT, null, book.scenes);
		}
		fireAgainPlanning();
	}

	public void fireAgain(SbView view) {
		//App.trace("BookModel.fireAgain(SbView=" + view.name + ")");
		fireAgain(SbView.getVIEWID(view.getName()));
	}

	public void fireAgain(VIEWID view) {
		//App.trace("BookModel.fireAgain(ViewID=" + view.name() + ")");
		switch(view) {
			case VIEW_MEMO:
				setRefresh(mainFrame.getView(SbView.VIEWID.VIEW_MEMO));
				break;
			case VIEW_CHRONO:
			case VIEW_STORYBOARD:
			case VIEW_WORK:
				fireAgainEntities(mainFrame.book.scenes);
				break;
			case VIEW_READING:
			case VIEW_MANAGE:
				fireAgainEntities(mainFrame.book.chapters);
				break;

			case TABLE_CATEGORY:
				fireAgainEntities(mainFrame.book.categories);
				break;
			case TABLE_CHAPTER:
				fireAgainEntities(mainFrame.book.chapters);
				break;
			case TABLE_EVENT:
				fireAgainEntities(mainFrame.book.events);
				break;
			case TABLE_GENDER:
				fireAgainEntities(mainFrame.book.genders);
				break;
			case TABLE_IDEA:
				fireAgainEntities(mainFrame.book.ideas);
				break;
			case TABLE_ITEM:
				fireAgainEntities(mainFrame.book.items);
				break;
			case TABLE_LOCATION:
				fireAgainEntities(mainFrame.book.locations);
				break;
			case TABLE_MEMO:
				fireAgainEntities(mainFrame.book.memos);
				break;
			case TABLE_PART:
				fireAgainEntities(mainFrame.book.parts);
				break;
			case TABLE_PERSON:
				fireAgainEntities(mainFrame.book.persons);
				break;
			case TABLE_POV:
				fireAgainEntities(mainFrame.book.povs);
				break;
			case TABLE_RELATION:
				fireAgainEntities(mainFrame.book.relations);
				break;
			case TABLE_SCENE:
				fireAgainEntities(mainFrame.book.scenes);
				break;
			case TABLE_TAG:
				fireAgainEntities(mainFrame.book.tags);
				break;

			case VIEW_PLANNING: fireAgainPlanning(); break;
		}
	}

	private void fireAgainPlanning() {
		//TODO
	}

	private void fireAgainEntities(List<?> entities) {
		//App.trace("BookModel.fireAgainEntities(type="+((AbstractEntity)entities.get(0)).objtype+")");
		firePropertyChange(Ctrl.ACTION.INIT, null, entities);
	}

	// common
	public void setRefresh(SbView view) {
		//App.trace("BookModel.setRefresh(" + view.name + ")");
		try {
			if (view.getComponentCount() == 0) {
				return;
			}
			firePropertyChange(Ctrl.ACTION.REFRESH, null, view);
		} catch (Exception e) {
			App.msg("setRefresh exception "+e.getLocalizedMessage());
		}
	}

	public void setShowOptions(SbView view) {
		firePropertyChange(Ctrl.ACTION.SHOW_OPTIONS, null, view);
	}

	public void setShowInfo(AbstractEntity entity) {
		firePropertyChange(Ctrl.ACTION.SHOW_INFO, null, entity);
	}

	public void setShowInMemoria(AbstractEntity entity) {
		firePropertyChange(Ctrl.ACTION.SHOW_MEMORIA, null, entity);
	}

	public void setUnloadEditor() {
		firePropertyChange(Ctrl.ACTION.UNLOAD_EDITOR, null, null);
	}

	public void setPrint(SbView view) {
		firePropertyChange(Ctrl.ACTION.PRINT, null, view);
	}

	public void setExport(SbView view) {
		firePropertyChange(Ctrl.ACTION.EXPORT, null, view);
	}

	// chrono view
	public void setChronoZoom(Integer val) {
		firePropertyChange(Chrono.ACTION.ZOOM.toString(), null, val);
	}

	public void setChronoLayoutDirection(Boolean val) {
		firePropertyChange(Chrono.ACTION.LAYOUTDIRECTION.toString(), null, val);
	}

	public void setChronoShowDateDifference(Boolean val) {
		firePropertyChange(Chrono.ACTION.SHOW_DATEDIFFERENCE.toString(), null, val);
	}

	public void setChronoShowEntity(Scene scene) {
		firePropertyChange(Chrono.ACTION.SHOW_ENTITY.toString(), null, scene);
	}

	public void setChronoShowEntity(Chapter chapter) {
		firePropertyChange(Chrono.ACTION.SHOW_ENTITY.toString(), null, chapter);
	}

	// book view
	public void setBookZoom(Integer val) {
		firePropertyChange(Work.ACTION.ZOOM.toString(), null, val);
	}

	public void setBookHeightFactor(Integer val) {
		firePropertyChange(Work.ACTION.HEIGHT.toString(), null, val);
	}

	public void setBookShowEntity(Scene scene) {
		firePropertyChange(Work.ACTION.SHOW_ENTITY.toString(), null, scene);
	}

	public void setBookShowEntity(Chapter chapter) {
		firePropertyChange(Work.ACTION.SHOW_ENTITY.toString(), null, chapter);
	}

	// manage view
	public void setManageZoom(Integer val) {
		firePropertyChange(Manage.ACTION.ZOOM.toString(), null, val);
	}

	public void setManageColumns(Integer val) {
		firePropertyChange(Manage.ACTION.COLUMNS.toString(), null, val);
	}

	public void setManageHideUnassigned(Boolean val) {
		firePropertyChange(Manage.ACTION.HIDE_UNASSIGNED.toString(), null, val);
	}

	public void setManageShowEntity(Scene scene) {
		firePropertyChange(Manage.ACTION.SHOW_ENTITY.toString(), null, scene);
	}

	public void setManageShowEntity(Chapter chapter) {
		firePropertyChange(Manage.ACTION.SHOW_ENTITY.toString(), null, chapter);
	}

	// reading view
	public void setReadingZoom(Integer val) {
		firePropertyChange(Reading.ACTION.ZOOM.toString(), null, val);
	}

	public void setReadingFontSize(Integer val) {
		firePropertyChange(Reading.ACTION.FONTSIZE.toString(), null, val);
	}

	// memoria view
	public void setMemoriaBalloon(Boolean val) {
		firePropertyChange(Memoria.ACTION.BALLOON.toString(), null, val);
	}

	// standard entity
	public void setEntityEdit(AbstractEntity entity) {
		firePropertyChange(Ctrl.ACTION.EDIT, entity, entity);
	}
	
	public void setEntityUpdate(AbstractEntity entity) {
		//App.trace("BookModel.setEntityUpdate("+entity.name+")");
		book.entityUpdate(entity);
		mainFrame.setModified();
		firePropertyChange(Ctrl.ACTION.UPDATE, null, entity);
	}
	
	public void setEntityNew(AbstractEntity entity) {
		book.entityNew(entity);
		mainFrame.setModified();
		firePropertyChange(Ctrl.ACTION.NEW, null, entity);
	}
	
	public void setEntityDelete(AbstractEntity entity) {
		if (entity==null) {
			App.error("BookModel.setEntityDelete(...) the entity is null)");
			return;
		}
		//App.trace("BookModel.setEntityDelete("+entity.name+")");
		switch(Book.getTYPE(entity)) {
			case CATEGORY: setDeleteCategory((Category)entity); break;
			case CHAPTER: setDeleteChapter((Chapter)entity); break;
			case EVENT: setDeleteEvent((Event)entity); break;
			case GENDER: setDeleteGender((Gender)entity); break;
			case IDEA: setDeleteIdea((Idea)entity); break;
			case ITEM: setDeleteItem((Item)entity); break;
			case LOCATION: setDeleteLocation((Location)entity); break;
			case MEMO: setDeleteMemo((Memo)entity); break;
			case PART: setDeletePart((Part)entity); break;
			case PERSON: setDeletePerson((Person)entity); break;
			case PHOTO: setDeletePhoto((Photo)entity); break;
			case PLOT: setDeletePlot((Plot)entity); break;
			case POV: setDeletePov((Pov)entity); break;
			case RELATION: setDeleteRelation((Relation)entity); break;
			case SCENE: setDeleteScene((Scene)entity); break;
			case TAG: setDeleteTag((Tag)entity); break;
		}
		book.entityDelete(entity);
		mainFrame.setModified();
		firePropertyChange(Ctrl.ACTION.DELETE, entity, null);
	}
	
	public synchronized void setDeleteCategory(Category entity) {
		//App.trace("BookModel.setDeleteCategory("+entity.name+")");
		if (entity.id == null) {
			return;
		}
		//remove links to this category for category sup
		List<Category> list=Category.findSup(book.categories, entity);
		list.forEach((c)-> {
			c.sup=null;
			setEntityUpdate(c);
		});
		// set category of affected persons to "minor"
		Category minor = book.categories.get(0);
		List<Person> persons = Person.findByCategory(book.persons, entity);
		persons.forEach((person)-> {
			person.category=(minor);
			setEntityUpdate(person);
		});
	}

	public synchronized void setDeleteChapter(Chapter entity) {
		List<Scene> scenes = Scene.find(book.scenes, entity);
		scenes.forEach((scene)-> {
			scene.chapter=null;
			setEntityUpdate(scene);
		});
	}

	public synchronized void setDeleteEvent(Event entity) {
	}

	public synchronized void setDeleteGender(Gender entity) {
		if (entity.id == null) {
			return;
		}
		// set gender of affected persons to "male"
		Gender male = book.genders.get(0);
		List<Person> persons = Person.findByGender(book.persons, entity);
		persons.forEach((person)-> {
			person.gender=(male);
			setEntityUpdate(person);
		});
	}

	public synchronized void setDeleteIdea(Idea entity) {
	}

	public synchronized void setDeleteItem(Item entity) {
		List<Relation> relations = Relation.findByItem(book.relations,entity);
		relations.forEach((relation)-> {
			List<Item> items=relation.items;
			if (items.contains(entity)) {
				items.remove(entity);
				relation.items=(items);
				setEntityUpdate(relation);
			}
		});
	}

	public synchronized void setDeleteLocation(Location entity) {
		// delete scene links
		List<Scene> scenes = Scene.findByLocation(book.scenes, entity);
		scenes.forEach((scene)-> {
			scene.locations.remove(entity);
			setEntityUpdate(scene);
		});
		// delete relationship
		List<Relation> relations = Relation.findByLocation(book.relations, entity);
		relations.forEach((relation)-> {
			relation.locations.remove(entity);
			setEntityUpdate(relation);
		});
	}

	public synchronized void setDeleteMemo(Memo entity) {
	}

	public synchronized void setDeletePart(Part entity) {
		List<Chapter> chapters = Chapter.find(book.chapters, entity);
		chapters.forEach((chapter)-> {
			setDeleteChapter(chapter);
		});
	}

	public synchronized void setDeletePerson(Person entity) {
		// delete scene links
		List<Scene> scenes = Scene.findByPerson(book.scenes, entity);
		scenes.forEach((scene)-> {
			scene.persons.remove(entity);
			setEntityUpdate(scene);
		});
		// delete links in relation
		List<Relation> relations = Relation.findByPerson(book.relations, entity);
		relations.forEach((relation)-> {
			relation.persons.remove(entity);
			setEntityUpdate(relation);
		});
	}

	public synchronized void setDeletePhoto(Photo entity) {
		List<Scene> scenes = Scene.findByPhoto(book.scenes, entity);
		scenes.forEach((scene)-> {
			scene.photos.remove(entity);
			setEntityUpdate(scene);
		});
	}

	public synchronized void setDeletePlot(Plot entity) {
		List<Scene> scenes = Scene.findByPlot(book.scenes, entity);
		scenes.forEach((scene)-> {
			scene.plots.remove(entity);
			setEntityUpdate(scene);
		});
	}

	public synchronized void setDeletePov(Pov entity) {
		List<Scene> scenes = Scene.findByPov(book.scenes,entity);
		scenes.forEach((scene)-> {
			scene.pov=null;
			setEntityUpdate(scene);
		});
	}

	public synchronized void setDeleteRelation(Relation entity) {
	}

	public synchronized void setDeleteScene(Scene entity) {
		// delete in relations
		List<Relation> relations=Relation.findByScene(book.relations, entity);
		relations.forEach((relation)-> {
			if (relation.hasStartScene() && relation.startScene.equals(entity)) {
				relation.startScene=(null);
			}
			if (relation.hasEndScene() && relation.endScene.equals(entity)) {
				relation.endScene=(null);
			}
		});
	}

	public synchronized void setDeleteTag(Tag entity) {
		List<Relation> relations=Relation.findByTag(book.relations, entity);
		relations.forEach((relation)-> {
			List<Tag> tags=relation.tags;
			if (tags.contains(entity)) {
				tags.remove(entity);
				setEntityUpdate(relation);
			}
		});
	}

	public void setMemoShowEntity(Memo memo) {
		firePropertyChange(Memos.ACTION.SHOW_MEMO.toString(), null, memo);
	}

}
