/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.verifier;

import desk.dialog.edit.EditorHtml;
import desk.dialog.edit.EditorPlainText;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;

import db.I18N;

public class VerifierLength extends VerifierAbstractInput {

	private final Integer length;

	public VerifierLength(int length) {
		super(true);
		this.length = length;
	}

	public VerifierLength(int length, boolean acceptEmpty) {
		super(acceptEmpty);
		this.length = length;
	}

	@Override
	public boolean verify(JComponent comp) {
		String errorMsg = I18N.getMsg("verifier.too.long", length.toString());
		if (comp instanceof JTextComponent) {
			JTextComponent tc = (JTextComponent) comp;
			if (tc.getText().length() < length) {
				return true;
			}
			setErrorText(errorMsg);
			return false;
		}
		if (comp instanceof JComboBox) {
			JComboBox combo = (JComboBox) comp;
			Object item = combo.getSelectedItem();
			if (item == null
					|| item.toString().length() < length) {
				return true;
			}
			setErrorText(errorMsg);
			return false;
		}
		if (comp instanceof EditorHtml) {
			EditorHtml editor = (EditorHtml) comp;
			if (editor.getText().length() < length) {
				return true;
			}
			setErrorText(errorMsg);
			return false;
		}
		if (comp instanceof EditorPlainText) {
			EditorPlainText editor = (EditorPlainText) comp;
			if (editor.getText().length() < length) {
				return true;
			}
			setErrorText(errorMsg);
			return false;
		}
		return true;
	}
}
