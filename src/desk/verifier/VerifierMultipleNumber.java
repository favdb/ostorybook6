/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2011 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.verifier;

import db.util.BookUtil;
import desk.app.MainFrame;
import javax.swing.JComponent;
import javax.swing.text.JTextComponent;
import desk.tools.ExceptionDlg;

public class VerifierMultipleNumber extends VerifierAbstractInput {

	private final String type;
	private final String elem;

	public VerifierMultipleNumber(String c, String e) {
		super(false);
		type = c;
		elem = e;
		setCheckOnlyOnNewEntities(true);
	}

	@Override
	public boolean isNumber() {
		return true;
	}

	@Override
	public boolean verify(JComponent comp) {
		if (comp instanceof JTextComponent) {
			try {
				JTextComponent tc = (JTextComponent) comp;
				int n = Integer.parseInt(tc.getText());
				if (type.isEmpty()) {
					return (false);
				}
				MainFrame m=MainFrame.getInstance();
				if (m==null) return(false);
				return(BookUtil.checkIfNumberExists(m.book, type, elem, n));
			} catch (NullPointerException e) {
				ExceptionDlg.show("", e);
				return false;
			}
		}
		return false;
	}
}
