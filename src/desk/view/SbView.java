/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.view;

import java.awt.Component;
import java.util.Objects;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JFrame;

import lib.infonode.docking.View;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class SbView extends View {
	
	public static VIEWID getViewName(String v) {
		for (VIEWID n:VIEWID.values()) {
			if (n.toString().equals(v)) return(n);
		}
		return(null);
	}
	
	public enum VIEWID {
		CHART_GANTT("chart.gantt"),
		CHART_OCCURRENCEOFITEMS("chart.occurrence_of_items"),
		CHART_OCCURRENCEOFLOCATIONS("chart.occurrence_of_locations"),
		CHART_OCCURRENCEOFPERSONS("chart.occurrence_of_persons"),
		CHART_PERSONSBYDATE("chart.persons_by_date"),
		CHART_PERSONSBYSCENE("chart.persons_by_scene"),
		CHART_POVSBYDATE("chart.povs_by_date"),
		CHART_WIWW("chart.wiww"),
		OSM("net.osm"),
		
		TABLE_CATEGORY("table.category"),
		TABLE_CHAPTER("table.chapter"),
		TABLE_EVENT("table.event"),
		TABLE_GENDER("table.gender"),
		TABLE_IDEA("table.idea"),
		TABLE_ITEM("table.item"),
		TABLE_LOCATION("table.location"),
		TABLE_MEMO("table.memo"),
		TABLE_PART("table.part"),
		TABLE_PERSON("table.person"),
		TABLE_PHOTO("table.photo"),
		TABLE_PLOT("table.plot"),
		TABLE_POV("table.pov"),
		TABLE_RELATION("table.relation"),
		TABLE_SCENE("table.scene"),
		TABLE_TAG("table.tag"),
		
		VIEW_WORK("view.work"),
		VIEW_CHRONO("view.chrono"),
		VIEW_GALLERY("view.gallery"),
		VIEW_INFO("view.info"),
		VIEW_MANAGE("view.manage"),
		VIEW_MEMO("view.memo"),
		VIEW_MEMORIA("view.memoria"),
		VIEW_NAVIGATION("view.navigation"),
		VIEW_OPTIONS("view.options"),
		VIEW_PLANNING("view.planning"),
		VIEW_PAPERCLIP("view.paperclip"),
		VIEW_READING("view.reading"),
		VIEW_SCENARIO("view.scenario"),
		VIEW_STORYBOARD("view.storyboard"),
		VIEW_TREE("view.tree"),
		VIEW_TYPIST("view.typist"),
		
		NONE("none");

		final private String text;

		private VIEWID(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}
		
		public String toCode() {
			return(text.replace("view.", "").replace("chart.", "").replace("table.", ""));
		}

		public boolean compare(View view) {
			return text.equals(view.getName());
		}
	}
	
	public static VIEWID getVIEWID(String view) {
		for (VIEWID v:VIEWID.values()) {
			if (v.toString().equals(view)) return(v);
		}
		return(VIEWID.NONE);
	}
	

	private static int counter = -1;
	private boolean loaded;
	private Integer number;
	private VIEWID viewName;

	public SbView(String title, VIEWID viewName) {
		this(title, viewName, null, null);
	}

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public SbView(String title, VIEWID viewName, Icon icon, Component comp) {
		super(title, icon, comp);
		loaded = comp != null;
		number = counter++;
		this.viewName=viewName;
		setName(title);
	}

	public void load(JFrame comp) {
		super.setComponent(comp.getContentPane());
		loaded=true;
	}
	
	public void load(JComponent comp) {
		super.setComponent(comp);
		loaded = true;
	}

	public void unload() {
		super.setComponent(null);
		loaded = false;
	}

	public boolean isLoaded() {
		return loaded;
	}
	
	public boolean isTable() {
		return(viewName.toString().startsWith("table"));
	}

	public boolean isWindowShowing() {
		return getRootWindow() != null;
	}

	public void cleverRestoreFocus() {
		setVisible(true);
		if (!isMinimized()) {
			restore();
		}
		restoreFocus();
	}

	@Override
	public String toString() {
		return "View " + number + ": " + getTitle();
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 31 + number.hashCode();
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if ((obj == null) || (obj.getClass() != this.getClass())) {
			return false;
		}
		SbView test = (SbView) obj;
		return Objects.equals(number, test.number);
	}

}
