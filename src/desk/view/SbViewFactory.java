/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.view;

import desk.app.MainFrame;
import desk.panel.AbstractPanel;
import desk.panel.BlankPanel;
import desk.panel.work.Work;
import desk.panel.charts.OccurrenceOf;
import desk.panel.charts.PersonsByDate;
import desk.panel.charts.PersonsByScene;
import desk.panel.charts.PovsByDate;
import desk.panel.charts.wiww.WiWW;
import desk.panel.chrono.Chrono;
import desk.panel.gallery.Gallery;
import desk.panel.info.Info;
import desk.panel.manage.Manage;
import desk.panel.memos.Memos;
import desk.panel.memoria.Memoria;
import desk.panel.navigation.Navigation;
import desk.panel.paperclip.Paperclip;
import desk.panel.planning.Planning;
import desk.panel.reading.Reading;
import desk.panel.scenario.ScenarioPanel;
import desk.panel.storyboard.Storyboard;
import desk.panel.tree.Tree;
import desk.panel.typist.Typist;
import desk.table.Table;
import desk.view.SbView.VIEWID;
import static desk.view.SbView.VIEWID.*;
import db.I18N;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import lib.infonode.docking.View;
import lib.infonode.docking.util.StringViewMap;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.table.TableColumnExt;
import tools.IconUtil;

public class SbViewFactory {

	private int NONE = 0, EXPORT = 1, OPTIONS = 10;
	private final MainFrame mainFrame;
	private final StringViewMap viewMap;
	private boolean initialisation;

	public SbViewFactory(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		viewMap = new StringViewMap();
	}

	public void setInitialisation() {
		initialisation = true;
	}

	public void resetInitialisation() {
		initialisation = false;
	}

	@SuppressWarnings("null")
	public void setViewTitle(View view) {
		if (view == null) {
			return;
		}
		String title = I18N.getMsg(view.toString());
		view.getViewProperties().setTitle(title);
	}

	public SbView getView(VIEWID viewName) {
		//App.trace("SbViewFactory.getView(viewName="+viewName.name()+")");
		SbView view = (SbView) viewMap.getView(viewName.toString());
		if (view != null) {
			//App.trace("find view: "+view.toString());
			return view;
		}
		int opt = EXPORT;
		if (!viewName.toString().toLowerCase().startsWith("table")) {
			switch (viewName) {
				case VIEW_WORK:
				case VIEW_GALLERY:
				case VIEW_INFO:
				case VIEW_MEMO:
				case VIEW_NAVIGATION:
				case VIEW_READING:
				case VIEW_SCENARIO:
				case VIEW_STORYBOARD:
				case VIEW_TREE:
					opt = NONE;
					break;
				case VIEW_MANAGE:
				case VIEW_MEMORIA:
					opt = OPTIONS;
					break;
				case VIEW_CHRONO:
					opt = OPTIONS + EXPORT;
					break;
			}
		}
		return (getOneView(viewName, opt));
	}

	private SbView getOneView(VIEWID viewId, int opt) {
		//App.trace("SbViewFactory.getOneView(viewId="+viewId.toString()+", opt="+opt+")");
		if (isViewInitialized(viewId)) {
			SbView view = new SbView(I18N.getMsg(viewId.toString()), viewId);
			view.setName(viewId.toString());
			addButtons(view, opt);
			viewMap.addView(view.getName(), view);
		}
		return (SbView) viewMap.getView(viewId.toString());
	}

	public void loadView(SbView view) {
		//App.trace("SbViewFactory.loadView(view="+(view!=null?view.name:"null")+")");
		if (view == null) {
			return;
		}
		AbstractPanel comp = new BlankPanel(mainFrame);
		boolean isTable = false;
		VIEWID vname = SbView.getViewName(view.getName());
		switch (vname) {
			case VIEW_CHRONO:
				comp = new Chrono(mainFrame);
				break;
			case VIEW_GALLERY:
				comp = new Gallery(mainFrame);
				break;
			case VIEW_INFO:
				comp = new Info(mainFrame);
				break;
			case VIEW_MANAGE:
				comp = new Manage(mainFrame);
				break;
			case VIEW_MEMO:
				comp = new Memos(mainFrame);
				break;
			case VIEW_MEMORIA:
				comp = new Memoria(mainFrame);
				break;
			case VIEW_NAVIGATION:
				comp = new Navigation(mainFrame);
				break;
			case VIEW_PAPERCLIP:
				comp = new Paperclip(mainFrame);
				break;
			case VIEW_PLANNING:
				comp = new Planning(mainFrame);
				break;
			case VIEW_READING:
				comp = new Reading(mainFrame);
				break;
			case VIEW_SCENARIO:
				comp = new ScenarioPanel(mainFrame);
				break;
			case VIEW_STORYBOARD:
				comp = new Storyboard(mainFrame);
				break;
			case VIEW_TREE:
				comp = new Tree(mainFrame);
				break;
			case VIEW_TYPIST:
				comp = new Typist(mainFrame);
				break;
			case VIEW_WORK:
				comp = new Work(mainFrame);
				break;

			case CHART_PERSONSBYDATE:
				comp = new PersonsByDate(mainFrame);
				break;
			case CHART_PERSONSBYSCENE:
				comp = new PersonsByScene(mainFrame);
				break;
			case CHART_WIWW:
				comp = new WiWW(mainFrame);
				break;
			case CHART_POVSBYDATE:
				comp = new PovsByDate(mainFrame);
				break;
			case CHART_OCCURRENCEOFPERSONS:
			case CHART_OCCURRENCEOFLOCATIONS:
			case CHART_OCCURRENCEOFITEMS:
				comp = new OccurrenceOf(mainFrame, vname);
				break;

			case TABLE_CATEGORY:
			case TABLE_CHAPTER:
			case TABLE_EVENT:
			case TABLE_GENDER:
			case TABLE_IDEA:
			case TABLE_ITEM:
			case TABLE_LOCATION:
			case TABLE_MEMO:
			case TABLE_PART:
			case TABLE_PERSON:
			case TABLE_PHOTO:
			case TABLE_PLOT:
			case TABLE_POV:
			case TABLE_RELATION:
			case TABLE_SCENE:
			case TABLE_TAG:
				comp = new Table(mainFrame, vname);
				break;
		}
		//comp.initAll();
		view.load(comp);
		if (isTable && !initialisation) {
			loadTableDesign(view);
		}
	}

	public void unloadView(SbView view) {
		if (view.getName().toLowerCase().startsWith("table")) {
			saveTableDesign(view);
		}
		view.unload();
	}

	private void addButtons(SbView view, int bt) {
		addRefreshButton(view);
		if (bt == 10 || bt == 11) {
			addOptionsButton(view);
		}
		//addPrintButton(view);
		if (bt == 1 || bt == 11) {
			addExportButton(view);
		}
		addSeparator(view);
	}

	@SuppressWarnings("unchecked")
	private void addRefreshButton(SbView view) {
		JButton bt = createMiniButton("refresh");
		bt.addActionListener((ActionEvent e) -> {
			mainFrame.setWaitingCursor();
			mainFrame.getBookController().refresh(view);
			mainFrame.setDefaultCursor();
		});
		view.getCustomTabComponents().add(bt);
	}

	@SuppressWarnings("unchecked")
	private void addOptionsButton(final SbView view) {
		JButton bt = createMiniButton("options");
		bt.addActionListener((ActionEvent e) -> {
			mainFrame.getBookController().showOptions(view);
		});
		view.getCustomTabComponents().add(bt);
	}

	@SuppressWarnings("unchecked")
	private void addExportButton(final SbView view) {
		JButton bt = createMiniButton("export");
		bt.addActionListener((ActionEvent e) -> {
			mainFrame.getBookController().export(view);
		});
		view.getCustomTabComponents().add(bt);
	}

	@SuppressWarnings({"unchecked", "unused"})
	private void addPrintButton(final SbView view) {
		JButton bt = createMiniButton("print");
		bt.addActionListener((ActionEvent e) -> {
			mainFrame.getBookController().print(view);
		});
		view.getCustomTabComponents().add(bt);
	}

	private JButton createMiniButton(String key) {
		final JButton bt = new JButton(IconUtil.getIcon("mini/" + key));
		bt.setOpaque(false);
		bt.setBorder(null);
		bt.setBorderPainted(false);
		bt.setContentAreaFilled(false);
		bt.setToolTipText(I18N.getMsg(key));
		return bt;
	}

	@SuppressWarnings("unchecked")
	private void addSeparator(View view) {
		if (view != null) {
			view.getCustomTabComponents().add(new JLabel("  "));
		}
	}

	public StringViewMap getViewMap() {
		return viewMap;
	}

	private boolean isViewInitialized(VIEWID viewName) {
		return viewMap.getView(viewName.toString()) == null;
	}

	public void saveAllTableDesign() {
		if (viewMap.getViewCount() == 0) {
			return;
		}
		for (int i = 0; i < viewMap.getViewCount(); i++) {
			saveTableDesign((SbView) viewMap.getViewAtIndex(i));
		}
	}

	private void saveTableDesign(SbView view) {
		if (mainFrame.isBlank()) {
			return;
		}
		if (!TABLE_CATEGORY.compare(view)
				&& !TABLE_CHAPTER.compare(view)
				&& !TABLE_EVENT.compare(view)
				&& !TABLE_GENDER.compare(view)
				&& !TABLE_IDEA.compare(view)
				&& !TABLE_ITEM.compare(view)
				&& !TABLE_LOCATION.compare(view)
				&& !TABLE_MEMO.compare(view)
				&& !TABLE_PART.compare(view)
				&& !TABLE_PERSON.compare(view)
				&& !TABLE_POV.compare(view)
				&& !TABLE_RELATION.compare(view)
				&& !TABLE_SCENE.compare(view)
				&& !TABLE_TAG.compare(view)) {
			return;
		}
		if (!view.isLoaded()) {
			return;
		}
		try {
			Table comp = (Table) view.getComponent();
			if (comp == null) {
				return;
			}
			JXTable table = comp.table;
			if (table == null) {
				return;
			}
			if (table.getColumns(true).isEmpty()) {
				return;
			}
			StringBuilder x = new StringBuilder();
			table.getColumns(true).forEach((col) -> {
				String l1 = (String) col.getHeaderValue();
				TableColumnExt ext = table.getColumnExt(col.getHeaderValue().toString());
				int modelIdx = col.getModelIndex();
				if (modelIdx != -1) {
					int ix = table.convertColumnIndexToView(modelIdx) + 1;
					if (ix != -1) {
						String strVal = col.getPreferredWidth() + "," + ix + (ext.isVisible() ? "" : ",hide");
						x.append("(").append(l1).append("=").append(strVal).append(")");
					}
				}
			});
			/*TODO
			mainFrame.bookPref.setParamValue("Table." + comp.table.name, x);*/
		} catch (Exception e) {
			System.err.println("saveTableDesign(" + view.getName() + ")\n" + " err=" + e.getLocalizedMessage());
		}
	}

	private void loadTableDesign(SbView view) {
		/* TODO
	if (null == view) {
	    return;
	}
	Table comp = (Table) view.getComponent();
	JXTable table = comp.table;
	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	String tableKey = "Table." + comp.table.name;
	if (!mainFrame.bookPref.isKeyExist(tableKey)) {
	    return;
	}
	String z = mainFrame.bookPref.getString(tableKey);
	String[] xlist = z.substring(1).split("\\(");
	List<TABCOL> tcol = new ArrayList<>();
	for (String x2 : xlist) {
	    if ("".equals(x2)) {
		continue;
	    }
	    int zz = 0;
	    if (x2.startsWith("=")) {
		zz = 1;
	    }
	    String[] xname = x2.substring(zz, x2.length() - 1).split("=");
	    if (xname.length < 2) {
		continue;
	    }
	    String[] x3 = xname[1].split(",");
	    boolean bhide = false;
	    for (String x4 : x3) {
		if ("hide".equals(x4)) {
		    bhide = true;
		}
	    }
	    Integer lng = Integer.parseInt(x3[0]);
	    Integer idx = Integer.parseInt(x3[1]);
	    TABCOL tabcol = new TABCOL(xname[0], lng, idx, bhide);
	    tcol.add(tabcol);
	}
	for (TableColumn col : table.getColumns(true)) {
	    String colName = (String) col.getHeaderValue();
	    for (TABCOL c : tcol) {
		if (c.name.equals(colName)) {
		    TableColumnExt ext = table.getColumnExt(colName);
		    if (c.hide) {
			ext.setVisible(false);
		    } else {
			ext.setVisible(true);
			col.setPreferredWidth(c.lng);
		    }
		    break;
		}
	    }
	}*/
	}

	private static class TABCOL {

		public String name;
		public Integer lng;
		public Integer idx;
		public boolean hide;

		public TABCOL() {
		}

		public TABCOL(String n, Integer l, Integer i, boolean h) {
			name = n;
			lng = l;
			idx = i;
			hide = h;
		}
	}
}
