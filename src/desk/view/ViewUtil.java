/*
 * oStorybook: Libre Software for novelists and authors.
 * original idea Martin Mustun
 * (c) 2013-2020 The oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.view;

import db.entity.Chapter;
import db.entity.Part;
import db.entity.Pov;
import db.entity.SbDate;
import db.entity.Scene;
import db.util.EntityTool;
import desk.action.ActionScrollToEntity;
import desk.panel.AbstractPanel;
import desk.panel.AbstractScenePanel;
import desk.panel.work.Work;
import desk.panel.work.WorkScene;
import desk.label.LabelPovDate;
import desk.panel.manage.ghost.MgPanelChapter;
import desk.panel.manage.ghost.MgPanelScene;
import desk.tools.swing.SwingUtil;
import desk.app.MainFrame;
import desk.panel.chrono.Chrono;
import desk.panel.chrono.ChronoScene;
import desk.panel.manage.Manage;
import desk.panel.manage.ghost.MgPanel;
import static desk.view.SbView.VIEWID.*;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 *
 * @author favdb
 */
public class ViewUtil {

	public static void scrollToTop(JScrollPane scroller) {
		if (scroller==null) return;
		scrollToTop(scroller, 100);
	}

	public static void scrollToTop(JScrollPane scroller, int i) {
		SwingUtilities.invokeLater(() -> {
			JViewport viewport = scroller.getViewport();
			JComponent comp = (JComponent) viewport.getView();
			if (comp instanceof JTextPane) {
				JTextPane textPane = (JTextPane) comp;
				textPane.setCaretPosition(0);
			} else {
				viewport.setViewPosition(new Point(0, 0));
			}
		});
	}

	public static boolean scrollToPovDate(AbstractPanel container, JPanel panel, Pov pov, SbDate date) {
		if (pov == null || date == null) {
			return false;
		}
		return doScrolling(container, panel, pov, date);
	}

	public static boolean scrollToScene(AbstractPanel container, JPanel panel, Scene scene) {
		if (scene == null) {
			return false;
		}
		boolean partChanged = changeCurrentPart(container, panel, scene);
		final ActionScrollToEntity action = new ActionScrollToEntity(container, panel, scene);
		int delay = 1;
		if (partChanged) {
			delay = 200;
		}
		Timer timer = new Timer(delay, action);
		timer.setRepeats(false);
		timer.start();
		return action.isFound();
	}

	public static boolean scrollToChapter(AbstractPanel container, JPanel panel, Chapter chapter) {
		if (container instanceof MgPanelChapter) {
			if (chapter == null || !chapter.hasPart()) {
				return(false);
			}
			boolean partChanged = changeCurrentPart(container, panel, chapter.part);
			final ActionScrollToEntity action = new ActionScrollToEntity(container, panel, chapter);
			int delay = 1;
			if (partChanged) {
				delay = 200;
			}
			Timer timer = new Timer(delay, action);
			timer.setRepeats(false);
			timer.start();
			return action.isFound();
		}

		@SuppressWarnings("null")
		Scene scene = EntityTool.findFirstScene(container.mainFrame.book,chapter);
		return scrollToScene(container, panel, scene);
	}

	public static boolean scrollToChapter(Manage container, JPanel panel, Chapter chapter) {
		//TODO
		if (container instanceof Manage) {
			if (chapter == null) {
				return false;
			}
			//todo
			/*boolean partChanged = changeCurrentPart(container, panel, chapter.book.getPart(chapter.getPartId()));
			final ActionScrollToEntity action = new ActionScrollToEntity(container, panel, chapter);
			int delay = 1;
			if (partChanged) {
				delay = 200;
			}
			Timer timer = new Timer(delay, action);
			timer.setRepeats(false);
			timer.start();
			return action.isFound();
			*/
			return(false);
		}

		/*Scene scene = container.mainFrame.book.findFirstScene(chapter);
		return scrollToScene(container, panel, scene);
		*/
		return(false);
	}

	public static boolean changeCurrentPart(AbstractPanel container, JPanel panel, Scene scene) {
		if (scene == null) {
			return false;
		}
		boolean changed = false;
		Chapter chap=scene.chapter;
		if (chap != null) {
			MainFrame mainFrame = container.getMainFrame();
			Part part = chap.part;
			if (part!=null && !mainFrame.getCurrentPart().id.equals(chap.part.id)) {
				mainFrame.getBookController().changeEntity(part);
				changed = true;
			}
		}
		return changed;
	}

	public static boolean changeCurrentPart(AbstractPanel container, JPanel panel, Part part) {
		if (part == null) {
			return false;
		}
		boolean changed = false;
		MainFrame mainFrame = container.getMainFrame();
		if (!mainFrame.getCurrentPart().id.equals(part.id)) {
			mainFrame.getBookController().changeEntity(part);
			changed = true;
		}
		return changed;
	}

	public static boolean doScrolling(AbstractPanel container, JPanel panel, Scene scene) {
		boolean found = false;
		List<AbstractScenePanel> panels = findScenePanels(container);
		for (AbstractScenePanel scenePanel : panels) {
			Scene sc = scenePanel.getScene();
			if (sc == null) {
				continue;
			}
			if (scene.id.equals(sc.id)) {
				Rectangle rect = scenePanel.getBounds();
				/*if (container instanceof ChronoPanel) {
					rect = SwingUtilities.convertRectangle(scenePanel.getParent(), rect, panel);
				}*/
				/*if (container instanceof MgPanel) {
					rect = SwingUtilities.convertRectangle(scenePanel.getParent(), rect, panel);
				}*/
				SwingUtil.expandRectangle(rect);
				// scroll
				panel.scrollRectToVisible(rect);
				// flash the found component
				SwingUtil.flashComponent(scenePanel);
				found = true;
				break;
			}
		}
		return found;
	}

	public static boolean doScrolling(AbstractPanel container, JPanel panel, Chapter chapter) {
		boolean found = false;
		List<MgPanelChapter> panels = findChapterPanels(container);
		for (MgPanelChapter scenePanel : panels) {
			Chapter ch = scenePanel.getChapter();
			if (ch == null) {
				continue;
			}
			if (chapter.id.equals(ch.id)) {
				Rectangle rect = scenePanel.getBounds();
				SwingUtil.expandRectangle(rect);
				// scroll and repaint
				panel.scrollRectToVisible(rect);
				// flash the found component
				SwingUtil.flashComponent(scenePanel);
				found = true;
				break;
			}
		}
		return found;
	}

	private static List<LabelPovDate> findPovDateLabels(Container cont) {
		List<Component> components = SwingUtil.findComponentsByClass(cont, LabelPovDate.class);
		List<LabelPovDate> labels = new ArrayList<>();
		components.forEach((comp) -> {
			labels.add((LabelPovDate) comp);
		});
		return labels;
	}
	public static boolean doScrolling(AbstractPanel container, JPanel panel, Pov pov, SbDate date) {
		boolean found = false;
		List<LabelPovDate> panels = findPovDateLabels(container);
		for (LabelPovDate sdPanel : panels) {
			Pov s = sdPanel.getPov();
			SbDate d = sdPanel.getDate();
			if (s == null || d == null) {
				continue;
			}
			if (pov.id.equals(s.id) && date.compareTo(d) == 0) {
				JComponent comp;
				if (container instanceof Chrono) {
					comp = (JComponent) sdPanel.getParent();
				} else if (container instanceof Work) {
					comp = (JComponent) sdPanel.getParent().getParent();
				} else break;
				Rectangle rect = comp.getBounds();
				SwingUtil.expandRectangle(rect);
				// scroll
				panel.scrollRectToVisible(rect);
				// flash the found component
				SwingUtil.flashComponent(comp);
				found = true;
				break;
			}
		}
		return found;
	}

	public static List<AbstractScenePanel> findScenePanels(Container cont) {
		if (cont instanceof Chrono) {
			return findChronoScenePanels(cont);
		}
		if (cont instanceof Work) {
			return findBookScenePanels(cont);
		}
		if (cont instanceof MgPanel) {
			return findManageScenePanels(cont);
		}
		return new ArrayList<>();
	}

	private static List<AbstractScenePanel> findBookScenePanels(Container cont) {
		List<Component> components = SwingUtil.findComponentsByClass(cont, WorkScene.class);
		List<AbstractScenePanel> panels = new ArrayList<>();
		components.forEach((comp) -> {
			panels.add((AbstractScenePanel) comp);
		});
		return panels;
	}

	private static List<AbstractScenePanel> findChronoScenePanels(Container cont) {
		List<Component> components = SwingUtil.findComponentsByClass(cont, ChronoScene.class);
		List<AbstractScenePanel> panels = new ArrayList<>();
		components.forEach((comp) -> {
			panels.add((AbstractScenePanel) comp);
		});
		return panels;
	}

	private static List<AbstractScenePanel> findManageScenePanels(Container cont) {
		List<Component> components = SwingUtil.findComponentsByClass(cont, MgPanelScene.class);
		List<AbstractScenePanel> panels = new ArrayList<>();
		components.forEach((comp) -> {
			panels.add((AbstractScenePanel) comp);
		});
		return panels;
	}

	private static List<MgPanelChapter> findChapterPanels(Container cont) {
		List<Component> components = SwingUtil.findComponentsByClass(cont, MgPanelChapter.class);
		List<MgPanelChapter> panels = new ArrayList<>();
		components.forEach((comp) -> {
			panels.add((MgPanelChapter) comp);
		});
		return panels;
	}

	public static SbView.VIEWID getView(String n) {
		for (SbView.VIEWID sbv:SbView.VIEWID.values()) {
			if (sbv.toString().equals(n)) {
				return(sbv);
			}
		}
		return(TABLE_TAG);
	}
}
