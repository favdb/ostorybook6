/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.action;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Event;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Photo;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import desk.app.App;
import desk.app.MainFrame;
import desk.app.Menu;
import desk.app.Menu.ACTION;
import desk.dialog.AboutDlg;
import desk.dialog.ChaptersCreateDlg;
import desk.dialog.CopyEntityFromDlg;
import desk.dialog.FoiDlg;
import desk.dialog.PreferencesDlg;
import desk.dialog.RenameDlg;
import desk.assistant.AssistantBookDlg;
import desk.dialog.i18n.I18NDlg;
import desk.exporter.ExportBook;
import desk.exporter.ExportDlg;
import desk.exporter.ExportOptionsDlg;
import desk.exporter.TextTransfer;
import desk.net.NetUtil;
import desk.net.Updater;
import desk.options.OptionsDlg;
import desk.spell.SpellDlg;
import desk.tools.DockUtil;
import desk.tools.swing.SwingUtil;
import desk.view.SbView;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author favdb
 */
public class MainAction {

	private final MainFrame mainFrame;

	public MainAction(MainFrame m) {
		mainFrame = m;
	}

	public void init() {
		//App.trace("MainAction.init()");
		if (mainFrame.isBlank()) {
			mainFrame.menu.setMenuForBlank();
		}
		if (mainFrame.menu.menuBar != null) {
			mainFrame.menu.reloadRecentMenu();
			mainFrame.menu.reloadPartMenu();
			mainFrame.menu.reloadWindowMenu();
		} else {
			System.err.println("General error : unable to load main actions");
		}
	}

	public void doAction(Menu.ACTION action) {
		//App.trace("MainAction.doAction(action="+action.name()+")");
		String tx = action.name().toLowerCase();
		if (tx.startsWith("menu")) {
			tx = tx.replace("menu\\.", "");
		}
		String m[] = tx.split("_");
		if (m.length < 1) {
			return;
		}
		switch (m[0]) {
			case "file":
				doActionFile(action);
				break;
			case "edit":
				doActionEdit(action);
				break;
			case "new":
				doActionNew(action);
				break;
			case "tools":
				doActionTools(action);
				break;
			case "view":
				doActionView(action);
				break;
			case "window":
				doActionWindow(action);
				break;
			case "help":
				doActionHelp(action);
				break;
		}
	}

	public void doAction(VIEWID view) {
		//App.trace("MainAction.doAction(view="+view.name()+")");
		String tx = view.name().toLowerCase();
		if (tx.startsWith("menu")) {
			tx = tx.replace("menu\\.", "");
		}
		String m[] = tx.split("_");
		if (m.length < 1) {
			return;
		}
		switch (m[0]) {
			case "table":
				doActionTable(view);
				break;
			case "view":
				doActionView(view);
				break;
			case "chart":
				doActionChart(view);
				break;
		}
	}

	public void doAction(String text) {
		//App.trace("MainAction.doAction(text=" + text + ")");
		ACTION action = Menu.getAction(text);
		if (action != null) {
			doAction(action);
			return;
		}
		VIEWID view = SbView.getVIEWID(text);
		if (view != null) {
			doAction(view);
		}
	}

	public void doActionFile(ACTION action) {
		//App.trace("MainAction.doAction(action=" + action.toString() + ")");
		switch (action) {
			case FILE_NEW:
				App.getInstance().createNewFile();
				break;
			case FILE_OPEN:
				mainFrame.setWaitingCursor();
				App.getInstance().openFile();
				mainFrame.setDefaultCursor();
				break;
			case FILE_SAVE:
				mainFrame.setWaitingCursor();
				if (mainFrame.scenarioPanel!=null && 
						mainFrame.scenarioPanel.checkModified(true)!=JOptionPane.YES_OPTION) return;
				mainFrame.xml.reformat(mainFrame.book.toXml());
				mainFrame.setTitle();
				mainFrame.setDefaultCursor();
				App.getInstance().getPreferences().recentFilesAdd(mainFrame.xml, mainFrame.book.getTitle());
				mainFrame.menu.reloadRecentMenu();
				break;
			case FILE_SAVEAS:
				mainFrame.fileSaveAs();
				break;
			case FILE_RENAME:
				mainFrame.fileRename();
				break;
			case FILE_BACKUP_NEW:
				mainFrame.backupNew(true);
				break;
			case FILE_BACKUP_REST:
				mainFrame.backupRest();
				break;
			case FILE_PROPERTIES:
				mainFrame.fileProperties();
				break;
			case FILE_ASSISTANT:
				AssistantBookDlg.show(mainFrame);
				break;
			case FILE_IMPORT:
				// for futur use to import data in other format than XML
				break;
			case FILE_EXPORT_HTML:
				ExportBook.toFile(mainFrame);
				break;
			case FILE_EXPORT_OTHER:
				ExportDlg.show(mainFrame);
				break;
			case FILE_EXPORT_OPTIONS:
				ExportOptionsDlg.show(mainFrame);
				break;
			case FILE_EXIT:
				App.btrace = false;
				mainFrame.close(true);
				break;
		}
	}

	private void doActionEdit(ACTION action) {
		switch (action) {
			case EDIT_COPY_TEXT:
				ExportBook.toClipboard(mainFrame, true);
				break;
			case EDIT_COPY_BLURB:
				TextTransfer tf = new TextTransfer();
				tf.setClipboardContents(mainFrame.book.info.getBlurb() + "\n");
				break;
			case EDIT_COPY_ENTITY:
				if (CopyEntityFromDlg.show(mainFrame)) {
					mainFrame.refreshViews();
				}
				break;
			case EDIT_PASTE:
				//TODO paste copied object
				break;
			case EDIT_EDITOR_RESET:
				SwingUtil.resetDlgPosition();
				break;
			case EDIT_PREFERENCES:
				PreferencesDlg.show(mainFrame);
				break;
			case EDIT_SPELLING:
				SpellDlg.show(mainFrame);
				break;
		}
	}

	private void doActionNew(ACTION action) {
		//App.trace("MainAction.doActionNew("+action.name()+")");
		switch (action) {
			case NEW_CATEGORY:
				mainFrame.newEntity(new Category());
				break;
			case NEW_CHAPTER:
				mainFrame.newEntity(new Chapter());
				break;
			case NEW_CHAPTERS:
				ChaptersCreateDlg.show(mainFrame);
				break;
			case NEW_EVENT:
				mainFrame.newEntity(new Event());
				break;
			case NEW_FOI:
				FoiDlg.show(mainFrame);
				break;
			case NEW_GENDER:
				mainFrame.newEntity(new Gender());
				break;
			case NEW_IDEA:
				mainFrame.newEntity(new Idea());
				break;
			case NEW_ITEM:
				mainFrame.newEntity(new Item());
				break;
			case NEW_LOCATION:
				mainFrame.newEntity(new Location());
				break;
			case NEW_MEMO:
				mainFrame.newEntity(new Memo());
				break;
			case NEW_PART:
				mainFrame.newEntity(new Part());
				break;
			case NEW_PERSON:
				mainFrame.newEntity(new Person());
				break;
			case NEW_PHOTO:
				mainFrame.newEntity(new Photo());
				break;
			case NEW_PLOT:
				mainFrame.newEntity(new Plot());
				break;
			case NEW_POV:
				mainFrame.newEntity(new Pov());
				break;
			case NEW_RELATION:
				mainFrame.newEntity(new Relation());
				break;
			case NEW_SCENE:
				mainFrame.newEntity(new Scene());
				break;
			case NEW_TAG:
				mainFrame.newEntity(new Tag());
				break;
		}
	}

	public void doActionTable(VIEWID viewName) {
		mainFrame.showView(viewName);
	}

	private void doActionTools(ACTION actionName) {
		switch (actionName) {
			case TOOLS_RENAME_CITY:
				RenameDlg.show(mainFrame, "city");
				break;
			case TOOLS_RENAME_COUNTRY:
				RenameDlg.show(mainFrame, "country");
				break;
		}
	}

	private void doActionView(VIEWID viewName) {
		if (viewName.equals(VIEWID.VIEW_OPTIONS)) {
			OptionsDlg.show(mainFrame, null);
			return;
		}
		mainFrame.showView(viewName);
	}
	
	private void doActionView(Menu.ACTION action) {
		//App.trace("MainAction.doActionView(str="+str+")");
		switch (action) {
			case VIEW_CHRONO:
				mainFrame.showView(VIEWID.VIEW_CHRONO);
				break;
			case VIEW_GALLERY:
				mainFrame.showView(VIEWID.VIEW_GALLERY);
				break;
			case VIEW_INFO:
				mainFrame.showAndFocus(VIEWID.VIEW_INFO);
				break;
			case VIEW_MANAGE:
				mainFrame.showView(VIEWID.VIEW_MANAGE);
				break;
			case VIEW_MEMORIA:
				mainFrame.showView(VIEWID.VIEW_MEMORIA);
				break;
			case VIEW_MEMOS:
				mainFrame.showView(VIEWID.VIEW_MEMO);
				break;
			case VIEW_NAVIGATION:
				mainFrame.showView(VIEWID.VIEW_NAVIGATION);
				break;
			case VIEW_OPTIONS:
				OptionsDlg.show(mainFrame, null);
				break;
			case VIEW_PLANNING:
				mainFrame.showView(VIEWID.VIEW_PLANNING);
				break;
			case VIEW_PAPERCLIP:
				mainFrame.showView(VIEWID.VIEW_PAPERCLIP);
				break;
			case VIEW_READING:
				mainFrame.showView(VIEWID.VIEW_READING);
				break;
			case VIEW_SCENARIO:
				mainFrame.showView(VIEWID.VIEW_SCENARIO);
				break;
			case VIEW_STORYBOARD:
				mainFrame.showView(VIEWID.VIEW_STORYBOARD);
				break;
			case VIEW_TREE:
				mainFrame.showAndFocus(VIEWID.VIEW_TREE);
				break;
			case VIEW_TYPIST:
				mainFrame.activateTypist();
				break;
			case VIEW_WORK:
				mainFrame.showView(VIEWID.VIEW_WORK);
				break;
		}
	}

	private void doActionChart(VIEWID viewName) {
		//App.trace("MainAction.doActionChart("+str+")");
		mainFrame.showView(viewName);
	}

	private void doActionPart(String str) {
		//nothing make by Menu.reloadPartMenu
	}

	private void doActionWindow(ACTION action) {
		switch (action) {
			case WINDOW_LAYOUT_LOAD:
				// nothing make by Menu.reloadWindowMenu
				break;
			case WINDOW_LAYOUT_SAVE:
				mainFrame.windowSaveLayoutAction();
				break;
			case WINDOW_LAYOUT_RESET:
				DockUtil.layoutSet(mainFrame, "default");
				break;
			case WINDOW_LAYOUT_REFRESH:
				mainFrame.refresh();
				break;
		}
	}

	private void doActionHelp(ACTION action) {
		//App.trace("MainAction.doActionHelp(action=" + action.toString() + ")");
		switch (action) {
			case HELP_DOC:
				NetUtil.openBrowser(NetUtil.URL_DOC);
				break;
			case HELP_FAQ:
				NetUtil.openBrowser(NetUtil.URL_FAQ);
				break;
			case HELP_HOMEPAGE:
				NetUtil.openBrowser(NetUtil.URL_HOME);
				break;
			case HELP_BUG:
				NetUtil.openBrowser(NetUtil.URL_BUG);
				break;
			case HELP_UPDATE:
				if (Updater.checkForUpdate(true)) {
					JOptionPane.showMessageDialog(mainFrame,
							I18N.getMsg("update.no_text"),
							I18N.getMsg("update.no_title"),
							JOptionPane.INFORMATION_MESSAGE);
				}
				break;
			case HELP_TRACE:
				App.getInstance().setTrace();
				mainFrame.menu.helpTrace.setSelected(App.isTrace());
				break;
			case HELP_TEST:
				break;
			case HELP_TRANSLATE:
				I18NDlg.show(mainFrame);
				break;
			case HELP_ABOUT:
				AboutDlg.show(mainFrame);
				break;
		}
	}

	public void showEntityInChrono(AbstractEntity entity) {
		mainFrame.getBookController().chronoShowEntity(entity);
	}

	public void partPrevious() {
		Part currentPart = mainFrame.getCurrentPart();
		List<Part> parts = mainFrame.book.parts;
		int index = parts.indexOf(currentPart);
		if (index == -1) {
			index = 1;
		} else if (index == 0) {
			// already first part
			return;
		}
		--index;
		partChange(parts.get(index));
	}

	public void partNext() {
		Part currentPart = mainFrame.getCurrentPart();
		List<Part> parts = mainFrame.book.parts;
		int index = parts.indexOf(currentPart);
		if (index == parts.size() - 1) {
			// already last part
			return;
		}
		++index;
		partChange(parts.get(index));
	}

	public void partChange(Part part) {
		mainFrame.setWaitingCursor();
		if (part == null) {
			part = mainFrame.book.parts.get(0);
		} else {
			Part currentPart = mainFrame.getCurrentPart();
			if (currentPart.id.equals(part.id)) {
				// same part
				return;
			}
		}
		mainFrame.setCurrentPart(part);
		mainFrame.setTitle();
		mainFrame.getBookController().changeEntity(part);
		mainFrame.setDefaultCursor();
	}

}
