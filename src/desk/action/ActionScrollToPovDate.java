/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import db.entity.Pov;
import db.entity.SbDate;
import desk.panel.AbstractPanel;
import desk.view.ViewUtil;
import db.I18N;

/**
 * @author martin
 *
 */
public class ActionScrollToPovDate implements ActionListener {

	private boolean found = false;
	private final AbstractPanel container;
	private final JPanel panel;
	private final Pov pov;
	private final SbDate date;
	private final JLabel lbWarning;

	public ActionScrollToPovDate(AbstractPanel container, JPanel panel,
			Pov pov, SbDate date, JLabel lbWarning) {
		this.container = container;
		this.panel = panel;
		this.pov = pov;
		this.date = date;
		this.lbWarning = lbWarning;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		found = ViewUtil.scrollToPovDate(container, panel, pov, date);
		if (!found) {
			lbWarning.setText(I18N.getMsg("navigation.date.not.found"));
		} else {
			lbWarning.setText(" ");
		}
	}

	public boolean isFound() {
		return found;
	}
}
