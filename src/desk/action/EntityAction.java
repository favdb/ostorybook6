/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.action;

import db.entity.AbstractEntity;
import db.entity.Chapter;
import db.entity.Location;
import db.entity.Scene;
import db.util.CheckUsed;
import desk.app.MainFrame;
import desk.dialog.ConfirmDeleteDlg;
import desk.dialog.edit.Editor;
import desk.net.NetUtil;
import desk.tools.ClipboardUtil;
import desk.view.SbView.VIEWID;
import db.I18N;
import db.entity.Book;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.AbstractAction;
import tools.IconUtil;
import static desk.view.SbView.VIEWID.VIEW_WORK;

/**
 *
 * @author favdb
 */
public class EntityAction {
	public enum ACTION {
		DELETE("delete"),
		NEW("new"),
		UPDATE("update"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop.toLowerCase());
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().toLowerCase().equals(str.toLowerCase())) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}


	public static void deleteEntity(MainFrame mainFrame, AbstractEntity entity) {
		//App.trace("EntityAction.deleteEntity(mainFrame, entity=" + entity.name + ")");
		List<AbstractEntity> entities = new ArrayList<>();
		entities.add(entity);
		deleteEntity(mainFrame, entities);
	}

	public static void deleteEntity(MainFrame mainFrame, List<AbstractEntity> entities) {
		CheckUsed du = new CheckUsed(mainFrame.book, entities);
		if (du.rc.toLowerCase().contains(I18N.getMsg("z.not.allowed"))) {
			JOptionPane.showMessageDialog(mainFrame,
					du.rc,
					I18N.getMsg("z.error"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (ConfirmDeleteDlg.show(mainFrame, entities, du)) {
			entities.forEach((entity)-> {
				switch(Book.getTYPE(entity)) {
					case PART:
						if (mainFrame.getCurrentPart().id.equals(entity.id)) {
							mainFrame.mainAction.partChange(null);
						}
						break;
					case CHAPTER:
					Chapter c=mainFrame.getLastChapter();
						if (c!=null && c.id.equals(entity.id)) {
							mainFrame.setLastChapter(null);
						}
						break;
					case SCENE:
						Scene s=mainFrame.getLastUsedScene();
						if (s.id.equals(entity.id)) {
							mainFrame.setLastUsedScene(null);
						}
						break;
				}
				mainFrame.getBookController().deleteEntity(entity);
			});
		}
	}

	public static AbstractAction delete(MainFrame mainFrame, AbstractEntity entity) {
		return new AbstractAction(I18N.getMsg("z.delete"), IconUtil.getIcon("small/delete")) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (entity == null) {
					return;
				}
				deleteEntity(mainFrame, entity);
			}
		};
	}

	public static AbstractAction create(MainFrame mainFrame, AbstractEntity entity) {
		return new AbstractAction(I18N.getMsg("z.new"), entity.getIcon()) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				Editor.show(mainFrame, entity, null);
			}
		};
	}

	public static AbstractAction clip(MainFrame mainFrame, AbstractEntity entity) {
		return new AbstractAction(I18N.getMsg("menu.edit_copy_entity_toclipboard"),
				IconUtil.getIcon("small/edit-copy")) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				ClipboardUtil.copyEntity(mainFrame, entity);
			}
		};
	}

	public static AbstractAction paste(MainFrame mainFrame, AbstractEntity entity) {
		return new AbstractAction(I18N.getMsg("menu.edit_paste"),
				IconUtil.getIcon("small/edit-paste")) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				ClipboardUtil.pasteEntity(mainFrame);
			}
		};
	}

	public static AbstractAction edit(MainFrame mainFrame, AbstractEntity entity, boolean b) {
		return new AbstractAction(I18N.getMsg("z.edit"), IconUtil.getIcon("small/edit")) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (b) {
					//save before
				}
				Editor.show(mainFrame, entity, null);
			}
		};
	}

	public static AbstractAction showIn(MainFrame mainFrame, AbstractEntity entity, VIEWID view) {
		String vue=view.toString().replace("view.","");
		String icon="small/"+vue;
		if (view.equals(VIEWID.OSM)) icon="small/map";
		
		return new AbstractAction(I18N.getMsg("show." + vue), IconUtil.getIcon(icon)) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				if (view.equals(VIEWID.OSM)) {
					if (entity instanceof Location) {
						NetUtil.openOSM((Location)entity);
						return;
					}
				}
				mainFrame.showView(view);
				switch (view) {
					case VIEW_CHRONO:
						mainFrame.getBookController().chronoShowEntity(entity);
						break;
					case VIEW_INFO:
						mainFrame.showInfo(entity);
						break;
					case VIEW_MANAGE:
						mainFrame.getBookController().manageShowEntity(entity);
						break;
					case VIEW_MEMORIA:
						mainFrame.getBookController().showInMemoria(entity);
						break;
					case OSM:
						break;
					case VIEW_WORK:
						mainFrame.getBookController().bookShowEntity(entity);
						break;
				}
			}
		};
	}

	public static AbstractAction chapterOrderByDate(MainFrame mainFrame, Chapter chapter) {
		return new AbstractAction(I18N.getMsg("chapter.sort_by_date",
				IconUtil.getIcon("small/sort"))) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				List<Scene> scenes = Scene.findByChapter(mainFrame.book.scenes, chapter);
				scenes.sort(Comparator.comparing((scene) -> scene.date.getDateTime()));
				int counter = 1;
				for (Scene scene : scenes) {
					scene.number=(counter);
					mainFrame.getBookController().updateEntity(scene);
					++counter;
				}
			}
		};
	}

	public static AbstractAction chapterResort(MainFrame mainFrame, Chapter chapter) {
		return new AbstractAction(I18N.getMsg("z.re_sort", IconUtil.getIcon("small/sort"))) {
			@Override
			public void actionPerformed(ActionEvent evt) {
				Scene.renumber(mainFrame.book,chapter,1,1);
			}
		};
	}

	public static PopupMenu editLO(MainFrame mainFrame, AbstractEntity entity) {
		//TODO editLO
		PopupMenu pop=new PopupMenu();
		return(pop);
	}

}
