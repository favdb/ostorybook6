/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.listcell;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;

import db.entity.Book;
import db.entity.SbDate;
import desk.app.App;
import desk.app.MainFrame;
import javax.swing.DefaultListCellRenderer;
import org.jdesktop.swingx.icon.EmptyIcon;

/**
 * @author martin
 *
 */
@SuppressWarnings("serial")
public class LCRDateTime extends DefaultListCellRenderer {

    private final MainFrame mainFrame;

    public LCRDateTime(MainFrame main) {
	this.mainFrame = main;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
	    int index, boolean sel, boolean focus) {
	//try {
	JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, sel, focus);
	if (value instanceof SbDate) {
	    SbDate entity = (SbDate) value;
	    label.setText(entity.format(mainFrame.bookPref.getDateShort()));
	} else if (value instanceof String) {
	    label.setText((String) value);
	    label.setIcon(new EmptyIcon());
	} else {
	    if (value == null) {
		return (label);
	    }
	    App.msg("LCRDate unkown date type is " + (value == null ? "null" : value.toString()));
	}
	return label;
	//} catch (Exception e) {
	//	return new JLabel("");
	//}
    }
}
