/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.listcell;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Idea;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;
import tools.IconUtil;

/**
 * @author favdb
 *
 */
@SuppressWarnings("serial")
public class LCREntity extends DefaultListCellRenderer {

	private final Book book;
	private final Book.TYPE type;

	public LCREntity(Book book, Book.TYPE type) {
		this.book = book;
		this.type = type;
	}

	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean sel, boolean focus) {
		JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, sel, focus);
		if (value instanceof Idea) {
			AbstractEntity entity = (AbstractEntity) value;
			label.setText(entity.name);
		} else if (value instanceof AbstractEntity) {
			AbstractEntity entity = (AbstractEntity) value;
			label.setIcon(entity.getIcon(16));
			label.setText(entity.name);
		} else if (value instanceof String) {
			label.setText((String) value);
			label.setIcon(IconUtil.getIcon("small/empty"));
		} else if (value instanceof Long) {
			AbstractEntity entity = book.getEntity(type, (Long) value);
			if (entity != null) {
				label.setText(entity.name);
				label.setIcon(entity.getIcon(16));
			}
		}
		return label;
	}
}
