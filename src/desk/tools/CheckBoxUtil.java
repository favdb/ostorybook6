/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools;

import db.entity.Category;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import desk.app.MainFrame;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;

/**
 *
 * @author favdb
 */
public class CheckBoxUtil {
	
	public static List<JCheckBox> createCategoryCheckBoxes(MainFrame mainFrame, ActionListener comp) {
		List<JCheckBox> list = new ArrayList<>();
		List<Category> categories = Category.orderBySort(mainFrame.book.categories);
		for (Category category : categories) {
			JCheckBox cb = new JCheckBox(category.name);
			cb.putClientProperty("CbCategory",category);
			cb.setOpaque(false);
			cb.addActionListener(comp);
			cb.setSelected(true);
			list.add(cb);
		}
		return list;
	}

	public static List<JCheckBox> createPersonCheckBoxes(MainFrame mainFrame, List<JCheckBox> cbl, ActionListener comp) {
		List<JCheckBox> list = new ArrayList<>();
		for (JCheckBox cb : cbl) {
			if (cb.isSelected()) {
				Category category = (Category) cb.getClientProperty("CbCategory");
				List<Person> persons = Person.findByCategory(mainFrame.book.persons, category);
				for (Person person : persons) {
					JCheckBox cbPerson = new JCheckBox(person.getFullNameAbbr());
					cbPerson.setOpaque(false);
					cbPerson.putClientProperty("CbPerson", person);
					cbPerson.addActionListener(comp);
					list.add(cbPerson);
				}
			}
		}
		return list;
	}

	public static List<JCheckBox> createItemCheckBoxes(MainFrame m, ActionListener comp) {
		List<JCheckBox> list = new ArrayList<>();
		List<Item> items = m.book.items;
		for (Item item : items) {
			JCheckBox cbItem = new JCheckBox(item.name);
			cbItem.setOpaque(false);
			cbItem.putClientProperty("CbItem", item);
			cbItem.addActionListener(comp);
			list.add(cbItem);
		}
		return list;
	}

	public static List<JCheckBox> createItemCategoryCheckBoxes(MainFrame mainFrame, ActionListener comp) {
		List<JCheckBox> list = new ArrayList<>();
		List<Category> categories = Category.findBeacon(mainFrame.book.categories,Category.BEACON.ITEM.toString());
		for (Category category : categories) {
			JCheckBox chb = new JCheckBox(category.name);
			chb.setOpaque(false);
			chb.addActionListener(comp);
			chb.setSelected(true);
			list.add(chb);
		}
		return list;
	}

	public static List<JCheckBox> createCountryCheckBoxes(MainFrame mainFrame, ActionListener comp) {
		List<JCheckBox> list = new ArrayList<>();
		List<String> countries = Location.findCountries(mainFrame.book);
		for (String country : countries) {
			JCheckBox chb = new JCheckBox(country);
			chb.setOpaque(false);
			chb.addActionListener(comp);
			chb.setSelected(true);
			list.add(chb);
		}
		return list;
	}

}
