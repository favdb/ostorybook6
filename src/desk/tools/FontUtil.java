/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.tools;

import desk.tools.swing.SwingUtil;
import java.awt.Font;
import javax.swing.JTextArea;

/**
 * @author martin
 *
 */
public class FontUtil {

	private static Font defaultFont;

	public static Font getBold() {
		return new Font(defaultFont.getName(), Font.BOLD, defaultFont.getSize());
	}

	public static Font getBold(int sz) {
		return new Font(defaultFont.getName(), Font.BOLD, sz);
	}

	public static Font getItalic() {
		return new Font(defaultFont.getName(), Font.ITALIC, defaultFont.getSize());
	}

	public static Font getDefault() {
		if (defaultFont==null) {
			JTextArea tx=new JTextArea();
			defaultFont=tx.getFont();
		}
		return defaultFont;
	}

	public static void setDefault(Font font) {
		if (font == null) {
			return;
		}
		SwingUtil.setUIFont(new javax.swing.plaf.FontUIResource(font.getName(),
				font.getStyle(), font.getSize()));
		defaultFont = font;
	}
	
	/** get Font from str 
	 * @param str contains définition like
	 *  fontName, fontSize, fontStyle
	 * @return the Font
	 * 
	 * Notes : if a parameter is missing it is replaced by the one of the
	 * defaultFont
	 * 
	 */
	public static Font getFont(String str) {
		if (str==null || str.isEmpty()) {
			return(getDefault());
		}
		String pref[]=str.split(",");
		String name=(pref.length>0?pref[0]:getDefault().getName());
		int size=(pref.length>1?Integer.parseInt(pref[1]):getDefault().getSize());
		int style=(pref.length>2?Integer.parseInt(pref[2]):getDefault().getStyle());
		return(new Font(name, style, size));
	}
	
	public static String getFontName(Font f) {
		if (f==null) {
			return(getDefault().getName());
		} else {
			return(f.getName());
		}
	}

	public static String getFontName(String str) {
		String pref[]=str.split(",");
		if (pref.length<3) return(getDefault().getFontName());
		return(pref[0]);
	}

	public static Integer getFontSize(Font f) {
		if (f==null) {
			return(getDefault().getSize());
		} else {
			return(f.getSize());
		}
	}

	public static Integer getFontSize(String str) {
		String pref[]=str.split(",");
		if (pref.length<3) return(getDefault().getSize());
		return(Integer.parseInt(pref[1]));
	}

	public static Integer getFontStyle(Font f) {
		if (f==null) {
			return(getDefault().getStyle());
		} else {
			return(f.getStyle());
		}
	}

	public static Integer getFontStyle(String str) {
		String pref[]=str.split(",");
		if (pref.length<3) return(getDefault().getStyle());
		return(Integer.parseInt(pref[2]));
	}

	public static String fontToString(Font f) {
		return(f.getFontName()+","+f.getSize()+","+f.getStyle());
	}
}
