/*
 * Copyright (C) 2017 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copyCategory of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Event;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Photo;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import desk.exporter.ExportBook;
import desk.exporter.TextTransfer;
import desk.app.MainFrame;
import db.I18N;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author favdb
 */
public class ClipboardUtil implements ClipboardOwner {
	public static boolean check(Book book, String type) {
		ClipboardUtil c=new ClipboardUtil();
		String str=c.getClipboardContents();
		if (!str.isEmpty()) {
			if (str.contains("<"+type)) {
				AbstractEntity p=AbstractEntity.xmlFromString(book, str);
				if (p!=null && p.type.toString().equals(type)) return(true);
			}
		}
		return(false);
	}

	public static boolean check(Book book, AbstractEntity type) {
		return(check(book, type.type.toString()));
	}

	public static void copyEntity(MainFrame m, AbstractEntity e) {
		StringBuilder str = new StringBuilder();
		str.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
		str.append(e.toXml());
		TextTransfer tf = new TextTransfer();
		tf.setClipboardContents(str.toString() + "\n");
	}

	public static void pasteEntity(MainFrame m) {
		ClipboardUtil c=new ClipboardUtil();
		String str=c.getClipboardContents();
		if (!str.isEmpty()) {
			AbstractEntity p=AbstractEntity.xmlFromString(m.book, str);
			if (p!=null) {
				p.id=-1L;
				p.setName(p.name+" "+I18N.getMsg("z.copied"));
				//remove dependant links
				switch(Book.getTYPE(p)) {
					case CATEGORY:
						Category cat=(Category)p;
						cat.sup=null;
						break;
					case CHAPTER:
						Chapter chapter=(Chapter)p;
						chapter.part=null;
						chapter.number=-1;
						break;
					case EVENT:
						Event event=(Event)p;
						break;
					case GENDER:
						Gender gender=(Gender)p;
						break;
					case IDEA:
						Idea idea=(Idea)p;
						break;
					case ITEM:
						Item item=(Item)p;
						item.category=(null);
						item.photos=(new ArrayList<>());
						break;
					case MEMO:
						Memo memo=(Memo)p;
						break;
					case PART:
						Part part=(Part)p;
						part.number=(-1);
						part.sup=(null);
						break;
					case PERSON:
						Person person=(Person)p;
						person.gender=(m.book.genders.get(0));
						person.category=(null);
						person.categories=(new ArrayList<>());
						person.photos=(new ArrayList<>());
						break;
					case PHOTO:
						Photo photo=(Photo)p;
						break;
					case PLOT:
						Plot plot=(Plot)p;
						plot.category=(null);
						break;
					case POV:
						Pov pov=(Pov)p;
						pov.setSort(0);
						break;
					case RELATION:
						Relation relation=(Relation)p;
						relation.startScene=(null);
						relation.endScene=(null);
						relation.items=(new ArrayList<>());
						relation.locations=(new ArrayList<>());
						relation.persons=(new ArrayList<>());
						relation.tags=(new ArrayList<>());
						break;
					case SCENE:
						Scene scene=(Scene)p;
						scene.number=0;
						scene.chapter=null;
						scene.informative=false;
						scene.items=(new ArrayList<>());
						scene.locations=(new ArrayList<>());
						scene.persons=(new ArrayList<>());
						scene.photos=(new ArrayList<>());
						scene.plots=(new ArrayList<>());
						scene.tags=(new ArrayList<>());
						scene.narrator=(null);
						break;
					case TAG:
						Tag tag=(Tag)p;
						tag.category=(null);
						break;
				}
				m.newEntity(p);
			}
		}
	}

	public static void copyText(MainFrame m) {
		ExportBook.toClipboard(m, true);
	}

	public static void copyBlurb(MainFrame m) {
		TextTransfer tf = new TextTransfer();
		tf.setClipboardContents(m.book.info.getBlurb() + "\n");
	}

	public void setClipboardContents(String str) {
		StringSelection stringSelection = new StringSelection(str);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, this);
	}

	@SuppressWarnings("null")
	public String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable contents = clipboard.getContents(null);
		boolean has = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (has) {
			try {
				result = (String) contents.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException | IOException ex) {
				System.err.println("unable to get clipboard content");
				ex.printStackTrace(System.out);
			}
		}
		return result;
	}

	@Override
	public void lostOwnership(Clipboard clpbrd, Transferable t) {
		//nothing
	}

}
