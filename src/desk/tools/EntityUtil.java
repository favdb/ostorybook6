/*
 Storybook: Open Source software for novelists and authors.
 Copyright (C) 2008 - 2015 Martin Mustun, Pete Keller

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Part;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.SbDate;
import db.entity.Scene;
import db.entity.Tag;
import desk.app.MainFrame;
import desk.action.EntityAction;
import desk.dialog.RenumScene;
import desk.view.SbView.VIEWID;
import db.I18N;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import lib.miginfocom.swing.MigLayout;
import tools.TextUtil;
import tools.html.HtmlUtil;

public class EntityUtil {

	public static List<Long> getReadOnlyIds(AbstractEntity entity) {
		ArrayList<Long> ret = new ArrayList<>();
		if (entity instanceof Category) {
			ret.add(1L);
			ret.add(2L);
		} else if (entity instanceof Gender) {
			ret.add(1L);
			ret.add(2L);
		} else if (entity instanceof Part) {
			ret.add(1L);
		} else if (entity instanceof Pov) {
			ret.add(1L);
		}
		return ret;
	}

	public static int getLastSceneNumber(MainFrame mainFrame) {
		List<Scene> lst = mainFrame.book.scenes;
		int n = 0;
		for (Scene scene : lst) {
			if ((scene.number != null) && (scene.number > n)) {
				n = scene.number;
			}
		}
		return n;
	}

	/* TODO cloneEntity
	public static AbstractEntity cloneEntity(MainFrame mainFrame, AbstractEntity entity) {
		return((AbstractEntity) BeanUtils.cloneBean(entity));
	}*/

 /*TODO copyEntity
	public static void copyEntity(MainFrame mainFrame, AbstractEntity entity) {
		AbstractEntityHandler handler = getEntityHandler(mainFrame, entity);
		AbstractEntity newEntity = handler.createNewEntity();
		ModelBook model = mainFrame.getBookModel();
		Session session = model.beginTransaction();
		session.refresh(entity);
		copyEntityProperties(mainFrame, entity, newEntity);
		markCopiedEntity(mainFrame, newEntity);

		List<Plot> plots = new ArrayList<>();
		List<Item> items = new ArrayList<>();
		List<Location> locations = new ArrayList<>();
		List<Person> persons = new ArrayList<>();
		List<Pov> povs = new ArrayList<>();
		if (entity instanceof Scene) { // correct date / relative scene
			Scene scene = (Scene) entity;
			Scene newScene = (Scene) newEntity;
			if (!scene.hasRelativeScene()) {
				newScene.removeRelativeScene();
			}
			persons = scene.getPersons();
			locations = scene.getLocations();
			povs = scene.getPovs();
		}
		List<Attribute> attributes = new ArrayList<>();
		if (entity instanceof Person) {
			Person person = (Person) entity;
			attributes = person.getAttributes();
		}
		model.commit();
		BookController ctrl = mainFrame.getBookController();
		ctrl.newEntity(newEntity);

		// re-set "stolen" bag links
		if (entity instanceof Scene) {
			Scene scene = (Scene) entity;

			List<Person> copyPersons = new ArrayList<>();
			for (Person person : persons) {
				copyPersons.add(person);
			}
			scene.setPersons(copyPersons);

			List<Location> copyLocations = new ArrayList<>();
			for (Location location : locations) {
				copyLocations.add(location);
			}
			scene.setLocations(copyLocations);

			List<Plot> copyPlots = new ArrayList<>();
			for (Plot plot : plots) {
				copyPlots.add(plot);
			}
			scene.setPlots(copyPlots);

			List<Pov> copyPovs = new ArrayList<>();
			for (Pov pov : povs) {
				copyPovs.add(pov);
			}
			scene.setPovs(copyPovs);
		}
		if (entity instanceof Person) {
			Person person = (Person) entity;
			List<Attribute> copyAttributes = new ArrayList<>();
			for (Attribute attribute : attributes) {
				copyAttributes.add(attribute);
			}
			person.setAttributes(copyAttributes);
		}
		ctrl.updateEntity(entity);

	}*/
	private static void markCopiedEntity(MainFrame mainFrame, AbstractEntity entity) {
		String copyStr = "(" + I18N.getMsg("z.copy") + ") ";
		entity.setName(copyStr + entity.name);
	}

	@SuppressWarnings("null")
	public static boolean hasHierarchyChanged(AbstractEntity oldEntity, AbstractEntity updEntity) {
		if (oldEntity == null || updEntity == null) {
			return false;
		}
		if (oldEntity instanceof Plot) {
			return false;
		}
		if (oldEntity instanceof Idea) {
			Idea old = (Idea) oldEntity;
			Idea upd = (Idea) updEntity;
			return(!old.status.equals(upd.status));
		}
		if (oldEntity instanceof Scene) {
			Scene old = (Scene) oldEntity;
			Scene upd = (Scene) updEntity;
			if (old.chapter == null && upd.chapter != null) {
				return true;
			}
			if (old.chapter != null && upd.chapter == null) {
				return true;
			}
			return old.chapter != null && upd.chapter != null
					&& (!Objects.equals(old.chapter.id, upd.chapter.id));
		}
		if (oldEntity instanceof Person) {
			Person old = (Person) oldEntity;
			Person upd = (Person) updEntity;
			if (!Objects.equals(old.category.id, upd.category.id)) {
				return true;
			}
			return !Objects.equals(old.gender.id, upd.gender.id);
		}
		if (oldEntity instanceof Relation) {
			return false;
		}
		if (oldEntity instanceof Location) {
			Location old = (Location) oldEntity;
			Location upd = (Location) updEntity;
			Location oldSite = old.sup;
			Location updSite = upd.sup;
			String oldCity = old.city;
			String updCity = upd.city;
			String oldCountry = old.country;
			String updCountry = upd.country;
			if (oldSite == null && updSite != null) {
				return true;
			}
			if (oldSite != null) {
				if (updSite == null) {
					return true;
				}
				if (!oldSite.equals(updSite)) {
					return true;
				}
			}
			if (oldCity == null && updCity != null) {
				return true;
			}
			if (oldCity != null) {
				if (updCity == null) {
					return true;
				}
				if (!oldCity.equals(updCity)) {
					return true;
				}
			}
			if (oldCountry == null && updCountry != null) {
				return true;
			}
			if (oldCountry != null) {
				if (updCountry == null) {
					return true;
				}
			} else {
				return !oldCountry.equals(updCountry);
			}
		}
		if (oldEntity instanceof Tag) {
			Tag old = (Tag) oldEntity;
			Tag upd = (Tag) updEntity;
			if (old.category == null) {
				return false;
			}
			if (old.category != null || upd.category != null) {
				return true;
			}
			return(!old.category.id.equals(upd.category.id));
		}
		if (oldEntity instanceof Item) {
			Item old = (Item) oldEntity;
			Item upd = (Item) updEntity;
			if (old.category == null) {
				return false;
			}
			if (old.category != null) {
				return true;
			}
			return(!old.category.id.equals(upd.category.id));
		}
		return false;
	}

	/**
	 * Create a popup menu for the entity
	 * @param mainFrame
	 * @param entity
	 * @return the JPopupMenu
	 */
	public static JPopupMenu createPopupMenu(MainFrame mainFrame, AbstractEntity entity) {
		JPopupMenu menu = new JPopupMenu();
		if (entity == null) {
			return null;
		}
		if (entity.isTransient()) {
			return null;
		}
		JLabel lbTitle = new JLabel("   " + entity.toString());
		lbTitle.setFont(FontUtil.getBold());
		menu.add(lbTitle);
		menu.add(new JPopupMenu.Separator());
		menu.add(EntityAction.edit(mainFrame, entity, false));
		if (entity instanceof Scene) {
			if (!mainFrame.bookPref.getXeditorExtension().isEmpty()) {
				menu.add(EntityAction.editLO(mainFrame, entity));
			}
		}
		menu.add(EntityAction.clip(mainFrame, entity));
		if (ClipboardUtil.check(mainFrame.book, entity)) {
			menu.add(EntityAction.paste(mainFrame, entity));
		}
		menu.add(EntityAction.delete(mainFrame, entity));
		menu.add(new JPopupMenu.Separator());
		if (entity instanceof Scene || entity instanceof Chapter) {
			menu.add(EntityAction.showIn(mainFrame, entity, VIEWID.VIEW_CHRONO));
			menu.add(EntityAction.showIn(mainFrame, entity, VIEWID.VIEW_WORK));
			menu.add(EntityAction.showIn(mainFrame, entity, VIEWID.VIEW_MANAGE));
		}
		menu.add(EntityAction.showIn(mainFrame, entity, VIEWID.VIEW_INFO));
		if (isAvailableInMemoria(entity)) {
			menu.add(EntityAction.showIn(mainFrame, entity, VIEWID.VIEW_MEMORIA));
		}
		menu.add(new JPopupMenu.Separator());
		if (entity instanceof Chapter) {
			menu.add(EntityAction.chapterOrderByDate(mainFrame, (Chapter) entity));
			menu.add(EntityAction.chapterResort(mainFrame, (Chapter) entity));
			menu.add(new JPopupMenu.Separator());
		}

		menu.add(EntityAction.create(mainFrame, entity));

		if (entity instanceof Location) {
			menu.add(new JPopupMenu.Separator());
			menu.add(EntityAction.showIn(mainFrame, entity, VIEWID.OSM));
		}
		if (menu.getComponents().length == 0) {
			return null;
		}
		return menu;
	}

	/**
	 * check if this entity is avalaible in memoria view
	 * @param entity
	 * @return 
	 */
	public static boolean isAvailableInMemoria(AbstractEntity entity) {
		return entity instanceof Person
				|| entity instanceof Plot
				|| entity instanceof Location
				|| entity instanceof Scene
				|| entity instanceof Tag
				|| entity instanceof Item;
	}

	/**
	 * renumber all scenes
	 * automatic numbering, is composed of 3 parts
	 * - part number between 00 and 99
	 * - chapter number between 00 and 99
	 * - scene number between 00 and 99
	 * incrementis 1
	 * 
	 * else start value 1 and increment is asked
	 * 
	 * @param mainFrame 
	 */
	public static void renumberAllScenes(MainFrame mainFrame) {
		// first ask for increment
		RenumScene dlg=new RenumScene(mainFrame);
		dlg.setVisible(true);
		if (dlg.canceled) return;
		int incBy = dlg.getInc();
		if (incBy == 0) {
			return;
		}
		if (dlg.getAuto()) {
			incBy=1;
		}
		mainFrame.setWaitingCursor();
		for (Chapter chapter:mainFrame.book.chapters) {
			renumberScenes(mainFrame,chapter,1,incBy);
		}
		mainFrame.setDefaultCursor();
	}

	/**
	 * renumber all scenes of a chapter
	 * 
	 * @param mainFrame
	 * @param chapter
	 * @param start
	 * @param inc : increment, start value is 1
	 */
	public static void renumberScenes(MainFrame mainFrame, Chapter chapter, Integer start, Integer inc) {
		List<Scene> scenes = Scene.find(mainFrame.book.scenes, chapter);
		int counter = start;
		for (Scene scene : scenes) {
			if (!scene.chapter.equals(chapter)) {
				continue;
			}
			int n=counter;
			if (inc==-1) {
				int npart=0;
				int nchap=0;
				if (scene.hasChapter()) {
					nchap=scene.chapter.number;
					if (scene.chapter.hasPart()) npart=scene.chapter.part.number;
				}
				n=((npart*100)+nchap*100)+counter;
			}
			scene.number=n;
			mainFrame.getBookController().updateEntity(scene);
			counter+=inc;
		}
	}

	public static void abandonEntityChanges(MainFrame mainFrame, AbstractEntity entity) {
	}

	/**
	 * get tooltip for this entity
	 * 
	 * @param entity
	 * @return 
	 */
	public static String getToolTip(AbstractEntity entity) {
		StringBuffer buf = new StringBuffer();
		buf.append("<html>");
		buf.append("<table width='300'>");
		buf.append("<tr><td>");
		buf.append(HtmlUtil.getTitle(entity.toString()));
		switch (Book.getTYPE(entity)) {
			case ITEM:
				toolTipAppendItem(buf, (Item) entity);
				break;
			case LOCATION:
				toolTipAppendLocation(buf, (Location) entity);
				break;
			case PERSON:
				toolTipAppendPerson(buf, (Person) entity, null);
				break;
			case PLOT:
				toolTipAppendPlot(buf, (Plot) entity);
				break;
			case POV:
				toolTipAppendPov(buf, (Pov) entity);
				break;
			case RELATION:
				toolTipAppendRelation(buf, (Relation) entity);
				break;
			case SCENE:
				toolTipAppendScene(buf, (Scene) entity);
				break;
			case TAG:
				toolTipAppendTag(buf, (Tag) entity);
				break;
		}
		buf.append("</td></tr>");
		buf.append("</table>");
		buf.append("</html>");
		return buf.toString();
	}

	/**
	 * get tool tip for Location
	 * 
	 * @param buf
	 * @param location 
	 */
	private static void toolTipAppendLocation(StringBuffer buf, Location location) {
		if (!location.city.isEmpty()) {
			buf.append(I18N.getColonMsg("location.city"));
			buf.append(" ");
			buf.append(location.city);
			buf.append("<br>");
		}
		if (!location.country.isEmpty()) {
			buf.append(I18N.getColonMsg("location.country"));
			buf.append(" ");
			buf.append(location.country);
		}
		buf.append("<p style='margin-top:5px'>");
		buf.append(TextUtil.truncate(location.getDescription()));
	}

	/**
	 * get tool tip for Plot
	 * 
	 * @param buf
	 * @param plot 
	 */
	private static void toolTipAppendPlot(StringBuffer buf, Plot plot) {
		buf.append("<b>")
				.append(I18N.getMsg("category"))
				.append("</b>: ")
				.append(plot.category.name)
				.append("<br>")
				.append(plot.getDescription());
	}

	/**
	 * get tool type for a Scene
	 * this tool tip has the 300 first characters of the texte
	 * 
	 * @param buf
	 * @param scene 
	 */
	private static void toolTipAppendScene(StringBuffer buf, Scene scene) {
		buf.append(scene.getWriting(300));
	}

	/**
	 * get tool tip for a Pov
	 * this tool type contains only the name
	 * 
	 * @param buf
	 * @param pov 
	 */
	private static void toolTipAppendPov(StringBuffer buf, Pov pov) {
		buf.append(pov.name);
	}

	/**
	 * get tool type for a Person
	 * 
	 * @param buf
	 * @param person
	 * @param date : reference date to compute the age of the Person 
	 */
	private static void toolTipAppendPerson(StringBuffer buf, Person person, SbDate date) {
		if (date != null && person.birthday != null) {
			buf.append(I18N.getColonMsg("person.age"));
			buf.append(" ");
			buf.append(person.calculateAge(date));
			if (person.isDead(date)) {
				buf.append("+");
			}
			buf.append("<br>");
		}
		buf.append(I18N.getColonMsg("gender"));
		buf.append(" ");
		buf.append(person.gender.name);
		buf.append("<br>");
		buf.append(I18N.getColonMsg("category"));
		buf.append(" ");
		buf.append(person.category.name);
		if (person.categories != null && !person.categories.isEmpty()) {
			buf.append("<br>");
			buf.append(I18N.getColonMsg("categories"));
			buf.append(" [");
			int i = 0;
			for (Category cat : person.categories) {
				if (i > 0) {
					buf.append(", ");
				}
				i++;
				buf.append(cat.name);
			}
			buf.append("]");
		}
		buf.append("<p style='margin-top:5px'>");
		buf.append(TextUtil.truncate(person.getDescription()));
	}

	/**
	 * get the tool type for a Relation
	 * 
	 * @param buf
	 * @param relation 
	 */
	private static void toolTipAppendRelation(StringBuffer buf, Relation relation) {
		buf.append(I18N.getColonMsg("persons"));
		buf.append(" ");
		List<Person> persons = relation.persons;
		if (persons != null && !persons.isEmpty()) {
			persons.forEach((p) -> {
				buf.append(p.getAbbr()).append(" ");
			});
		}
		List<Item> items = relation.items;
		if (items != null && !items.isEmpty()) {
			buf.append("<br>");
			buf.append(I18N.getColonMsg("items"));
			items.forEach((p) -> {
				buf.append(p.name).append(" ");
			});
		}
		List<Location> locations = relation.locations;
		if (locations != null && !locations.isEmpty()) {
			buf.append("<br>");
			buf.append(I18N.getColonMsg("locations"));
			locations.forEach((p) -> {
				buf.append(p.name).append(" ");
			});
		}
	}

	/**
	 * get the tool type for an Item
	 * 
	 * @param buf
	 * @param item 
	 */
	private static void toolTipAppendItem(StringBuffer buf, Item item) {
		buf.append(TextUtil.truncate(item.getDescription()));
	}

	/**
	 * get the tool tip for a Tag
	 * 
	 * @param buf
	 * @param tag 
	 */
	private static void toolTipAppendTag(StringBuffer buf, Tag tag) {
		buf.append(TextUtil.truncate(tag.getDescription()));
	}

	/**
	 * get informations about the entity to be deleted for checking
	 * 
	 * @param mainFrame
	 * @param entity
	 * @return 
	 */
	public static String getDeleteInfo(MainFrame mainFrame, AbstractEntity entity) {
		StringBuffer buf = new StringBuffer();
		boolean warnings = addDeletionInfo(mainFrame, entity, buf);
		if (warnings) {
			buf.append(HtmlUtil.getHr());
		}
		buf.append(getInfo(mainFrame, entity, true));
		return buf.toString();
	}

	/**
	 * get informations about the entity
	 * 
	 * @param mainFrame
	 * @param entity
	 * @param truncate : true for yes, else not truncate
	 * @return 
	 */
	private static String getInfo(MainFrame mainFrame, AbstractEntity entity, boolean truncate) {
		StringBuffer buf = new StringBuffer();
		addInfo(mainFrame, entity, buf, truncate);
		return buf.toString();
	}

	private static boolean addDeletionInfo(MainFrame mainFrame, AbstractEntity entity, StringBuffer buf) {
		boolean warnings = false;
		switch (Book.getTYPE(entity)) {
			case CATEGORY: {
				Category category = (Category) entity;
				List<Person> persons = Person.findByCategory(mainFrame.book.persons, category);
				if (!persons.isEmpty()) {
					buf.append("<p style='padding-top:10px'>");
					buf.append(HtmlUtil.getWarning(I18N.getMsg("category.delete.warning")));
					buf.append("</p><br>");
					buf.append(I18N.getColonMsg("persons"));
					buf.append("<br><ul>");
					persons.forEach((person) -> {
						buf.append("<li>");
						buf.append(person);
						buf.append("</li>\n");
					});
					buf.append("</ul>");
					warnings = true;
				}
			}
			break;
			case GENDER: {
				Gender gender = (Gender) entity;
				List<Person> persons = Person.findByGender(mainFrame.book.persons, gender);
				if (!persons.isEmpty()) {
					buf.append("<p style='padding-top:10px'>");
					buf.append(HtmlUtil.getWarning(I18N.getMsg("gender.delete.warning")));
					buf.append("</p><br>");
					buf.append(I18N.getColonMsg("persons"));
					buf.append("<br><ul>");
					persons.forEach((person) -> {
						buf.append("<li>");
						buf.append(person);
						buf.append("</li>\n");
					});
					buf.append("</ul>");
					warnings = true;
				}
			}
			case CHAPTER: {
				Chapter chapter = (Chapter) entity;
				List<Scene> scenes = Scene.findByChapter(mainFrame.book.scenes, chapter);
				if (!scenes.isEmpty()) {
					buf.append("<p style='margin-top:10px'>");
					buf.append(HtmlUtil.getWarning(I18N
							.getMsg("chapter.has_assigned_scenes")));
					buf.append("</p><br>");
					buf.append(I18N.getColonMsg("scenes"));
					buf.append("<br><ul>");
					scenes.forEach((scene) -> {
						buf.append("<li>");
						buf.append(scene);
						buf.append("</li>\n");
					});
					buf.append("</ul>");
					warnings = true;
				}
			}
			case SCENE: {
				Scene scene = (Scene) entity;
				List<Scene> scenes = Scene.findWithRelative(mainFrame.book.scenes, scene);
				if (!scenes.isEmpty()) {
					buf.append("<p style='padding-top:10px'>");
					buf.append(HtmlUtil.getWarning(I18N.getMsg("scene.relativedate.delete.warning")));
					buf.append("</p><br>");
					buf.append(I18N.getColonMsg("scenes"));
					buf.append("<br><ul>");
					scenes.forEach((scene2) -> {
						buf.append("<li>");
						buf.append(scene2);
						buf.append("</li>\n");
					});
					buf.append("</ul>");
					warnings = true;
				}
			}
			case PART: {
				Part part = (Part) entity;
				List<Chapter> chapters = Chapter.find(mainFrame.book.chapters, part);
				if (!chapters.isEmpty()) {
					buf.append("<p style='padding-top:10px'>");
					buf.append(HtmlUtil.getWarning(I18N.getMsg("manage.parts.delete.warning")));
					buf.append("</p><br>");
					buf.append(I18N.getColonMsg("chapters"));
					buf.append("<br><ul>");
					chapters.forEach((chapter) -> {
						buf.append("<li>");
						buf.append(chapter);
						buf.append("</li>\n");
					});
					buf.append("</ul>");
					warnings = true;
				}
			}
			case POV: {
				Pov pov = (Pov) entity;
				List<Scene> scenes = Scene.findByPov(mainFrame.book.scenes, pov);
				if (!scenes.isEmpty()) {
					buf.append("<p style='padding-top:10px'>");
					buf.append(HtmlUtil.getWarning(I18N.getMsg("manage.povs.delete.warning")));
					buf.append("</p><br>");
					buf.append(I18N.getColonMsg("scenes"));
					buf.append("<br><ul>");
					scenes.forEach((scene) -> {
						buf.append("<li>");
						buf.append(scene);
						buf.append("</li>\n");
					});
					buf.append("</ul>");
					warnings = true;
				}
			}
		}
		return warnings;
	}

	private static void addInfo(MainFrame mainFrame, AbstractEntity entity, StringBuffer buf, boolean truncate) {
		//TODO
	}

	public static void refresh(MainFrame mainFrame, AbstractEntity entity) {
	}

	public static JPanel getEntityTitlePanel(AbstractEntity entity) {
		JPanel panel = new JPanel(new MigLayout("flowx,ins 2"));
		panel.setOpaque(false);
		panel.add(getEntityIconLabel(entity));
		StringBuilder buf = new StringBuilder();
		buf.append("<html>\n").append(getEntityFullTitle(entity)).append("\n");
		panel.add(new JLabel(buf.toString()));
		return panel;
	}

	public static String getEntityFullTitle(AbstractEntity entity) {
		StringBuilder buf = new StringBuilder();
		buf.append("<span ")
				.append(getCSSTitle1())
				.append(">\n")
				.append(getEntityTitle(entity))
				.append("</span>\n");
		if (!entity.isTransient()) {
			buf.append("&nbsp;&nbsp;")
					.append("<span ")
					.append(getCSSTitle2())
					.append("/>\n")
					.append(entity.toString())
					.append("</span>");
		}
		return buf.toString();
	}

	public static JLabel getEntityIconLabel(AbstractEntity entity) {
		return new JLabel(getEntityIcon(entity));
	}

	public static Icon getEntityIcon(AbstractEntity entity) {
		return (entity.getIcon());
	}

	public static String getTypeName(AbstractEntity entity) {
		return (entity.getClass().getName().replace("db.entity.", ""));
	}

	private static String getCSSTitle1() {
		return getCSSTitle1("");
	}

	private static String getCSSTitle1(String styles) {
		return "style='font-weight:bold;font-size:12px;" + styles + "'";
	}

	private static String getCSSTitle2() {
		return getCSSTitle2("");
	}

	private static String getCSSTitle2(String styles) {
		return "style='font-weight:bold;font-size:10px;" + styles + "'";
	}

	public static String getEntityTitle(AbstractEntity entity) {
		return getEntityTitle(entity, null);
	}

	public static String getEntityTitle(AbstractEntity entity, Boolean setIsTransient) {
		if (entity == null || entity.type == null) {
			return ("");
		}
		boolean isTransient = entity.isTransient();
		if (setIsTransient != null) {
			isTransient = setIsTransient;
		}
		String type = entity.type.toString();
		if (isTransient) {
			type += ".new";
		}
		return (I18N.getMsg(type));
	}

	public static String getStats(MainFrame mainFrame, boolean toHtml) {
		StringBuilder buf = new StringBuilder();
		Book book = mainFrame.book;
		if (toHtml) {
			buf.append("<html>");
			buf.append(HtmlUtil.getHeadWithCSS(mainFrame.getFont()));
			buf.append("<body><table>");
			buf.append(HtmlUtil.getAttribute("file", mainFrame.xml.getPath()));
		} else {
			buf.append(I18N.getColonMsg("book.statistics")).append("\n");
		}
		buf.append(book.statistics(true));
		if (toHtml) {
			buf.append("</table></body></html>");
		}
		return (buf.toString());
	}

	public static String listAll(MainFrame mainFrame) {
		Book book = mainFrame.book;
		StringBuilder b = new StringBuilder();
		b.append(I18N.getMsg("entities.list"));
		b.append(listAllDetail(Book.TYPE.CATEGORY, book.categories));
		b.append(listAllDetail(Book.TYPE.CHAPTER, book.chapters));
		b.append(listAllDetail(Book.TYPE.EVENT, book.events));
		b.append(listAllDetail(Book.TYPE.GENDER, book.genders));
		b.append(listAllDetail(Book.TYPE.IDEA, book.ideas));
		b.append(listAllDetail(Book.TYPE.ITEM, book.items));
		b.append(listAllDetail(Book.TYPE.LOCATION, book.locations));
		b.append(listAllDetail(Book.TYPE.MEMO, book.memos));
		b.append(listAllDetail(Book.TYPE.PART, book.parts));
		b.append(listAllDetail(Book.TYPE.PERSON, book.persons));
		b.append(listAllDetail(Book.TYPE.PHOTO, book.photos));
		b.append(listAllDetail(Book.TYPE.PLOT, book.plots));
		b.append(listAllDetail(Book.TYPE.POV, book.povs));
		b.append(listAllDetail(Book.TYPE.RELATION, book.relations));
		b.append(listAllDetail(Book.TYPE.SCENE, book.scenes));
		b.append(listAllDetail(Book.TYPE.TAG, book.tags));
		return (b.toString());
	}

	private static String listAllDetail(Book.TYPE id, List<?> entities) {
		StringBuilder b = new StringBuilder();
		b.append("\n** ").append(I18N.getMsg(id.toString())).append("\n");
		entities.forEach((c) -> {
			b.append(String.format("- (%d) %s\n", ((AbstractEntity) c).id, ((AbstractEntity) c).name));
		});
		if (entities.isEmpty()) {
			b.append("empty\n");
		}
		return (b.toString());
	}

}
