/*
 * Copyright (C) 2018 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools;

import db.entity.AbstractEntity;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import db.I18N;
import java.io.File;
import java.io.IOException;
import org.w3c.dom.Element;
import java.io.StringReader;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author FaVdB
 */
public class TempUtil {

	public static AbstractEntity check(MainFrame mainFrame, AbstractEntity entity) {
		if (entity instanceof Scene) {
			Scene scene=(Scene)entity;
			File file=getFile(mainFrame, entity);
			if (file.exists()) {
				try {
					String saved=IOTools.readFileAsString(file.getAbsolutePath());
					if (!saved.equals(scene.toXml())) {
						int rc=JOptionPane.showConfirmDialog(null,I18N.getMsg("backup.askrestore"),
							I18N.getMsg("backup.rest"), JOptionPane.YES_NO_OPTION);
						if (rc==JOptionPane.YES_OPTION) {
							StringBuilder str = new StringBuilder();
							str.append("<?xml version='1.0'?>\n");
							str.append("<book xmlns=\"http://docbook.org/ns/docbook\" version=\"5.0\">\n");
							str.append(saved);
							str.append("</book>\n");
							Element rootNode = mainFrame.xml.rootNode;
							if (rootNode==null) return(scene);
							Element n=(Element)rootNode.getElementsByTagName("scene").item(0);
							if (n!=null) {
								scene.xmlFrom(mainFrame.xml,mainFrame.book,n);
							}
						}
					}
				} catch (IOException ex) {
					App.error("TempUtil restore failed", ex);
				}
			}
			write(mainFrame, entity);
			return(scene);
		}
		return(entity);
	}
	
	private static File getFile(MainFrame mainFrame, AbstractEntity entity) {
		Scene scene=(Scene)entity;
		String ident=scene.type.toString()+"_"+scene.id+".tmp";
		File f=new File(mainFrame.xml.getOnlyPath()+File.separator+ident);
		return(f);
	}
	
	public static void remove(MainFrame mainFrame, AbstractEntity entity) {
		if (entity instanceof Scene) {
			if (getFile(mainFrame, entity).exists()) getFile(mainFrame, entity).delete();
		}
	}
	
	public static void write(MainFrame mainFrame, AbstractEntity entity) {
		if (entity instanceof Scene) {
			Scene scene=(Scene)entity;
			IOTools.writeFileFromString(getFile(mainFrame, entity), sceneSave(scene));
		}
	}
	
	public static Element getRootNode(String str) {
		try {
			DocumentBuilder builder=DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(str)));
			Element rootNode = doc.getDocumentElement();
			return(rootNode);
		} catch (ParserConfigurationException | SAXException | IOException ex) {
			App.error("TempUtil restore failed", ex);
			return(null);
		}
	}

	public static String sceneSave(Scene scene) {
		return (scene.toXml());
	}

}
