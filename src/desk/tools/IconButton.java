/*
Storybook: Scene-based software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.tools;

import desk.tools.swing.SwingUtil;
import java.awt.Dimension;

import javax.swing.Action;
import javax.swing.JButton;

import db.I18N;
import tools.IconUtil;

@SuppressWarnings("serial")
public class IconButton extends JButton {

	private static final Dimension SIZE16X16 = new Dimension(16, 16);
	private static final Dimension SIZE20X20 = new Dimension(20, 20);
	private static final Dimension SIZE30X30 = new Dimension(32, 20);
	private static final Dimension SIZE72X72 = new Dimension(72, 72);

	public IconButton() {
		super();
	}

	public IconButton(String icon) {
		this(icon, null);
	}

	public IconButton(Action action) {
		this(null, null, action);
	}

	public IconButton(String icon, Action action) {
		this(icon, null, action);
	}

	public IconButton(String icon, String tooltip, Action action) {
		if (action != null) {
			setAction(action);
		}
		if (icon != null) {
			setIcon(IconUtil.getIcon(icon));
		}
		if (tooltip != null) {
			setToolTipText(I18N.getMsg((tooltip)));
		}
	}

	public void setIcon(String resourceIcon) {
		if (resourceIcon != null) {
			setIcon(IconUtil.getIcon(resourceIcon));
		}
	}

	public void setFlat(){
		setBorderPainted(false);
		setOpaque(false);
		setContentAreaFilled(false);
	}

	public void setPrefSize(int x) {
		SwingUtil.setForcedSize(this, new Dimension(x,x));
	}

	public void setPrefSize(int x, int y) {
		SwingUtil.setForcedSize(this, new Dimension(x,y));
	}

	public void setSize16x16() {
		SwingUtil.setForcedSize(this, SIZE16X16);
	}

	public void setSize20x20() {
		SwingUtil.setForcedSize(this, SIZE20X20);
	}

	public void setSize32x20() {
		SwingUtil.setForcedSize(this, SIZE30X30);
	}

	public void setSize72x72() {
		SwingUtil.setForcedSize(this, SIZE72X72);
	}

	public Dimension getSize16x16() {
		return SIZE16X16;
	}

	public Dimension getSize20x20() {
		return SIZE20X20;
	}

	public Dimension getSize72x72() {
		return SIZE72X72;
	}

	public void setNoBorder() {
		this.setBorder(null);
	}

	public void setControlButton() {
		this.setSize16x16();
		this.setNoBorder();
	}
}
