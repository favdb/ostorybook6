/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.tools.decorator;

import db.entity.AbstractEntity;
import db.entity.Location;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import tools.StringUtil;

/**
 * @author martin
 *
 */
public class DecoratorCbPanelLocation extends DecoratorCbPanel {
	private String oldCity = "";
	private String oldCountry = "";
	private String oldSite = "";

	public DecoratorCbPanelLocation() {
	}

	@Override
	public void decorateBeforeFirstEntity() {
		oldCity = "";
		oldCountry = "";
		oldSite = "";
	}

	@Override
	public void decorateBeforeEntity(AbstractEntity entity) {
		Location p = (Location) entity;
		String country = StringUtil.capitalize(p.country);
		String city = StringUtil.capitalize(p.city);
		String site = p.getFullName();
		if ((site.equals(city)) || (site.equals(country))) {
			site = "";
		}
		if (!oldCountry.equals(country) || !oldCity.equals(city) || !oldSite.equals(site)) {
			StringBuilder buf = new StringBuilder();
			buf.append("<html>");
			buf.append("<p style='margin-top:5px'>");
			if (!site.isEmpty()) {
			     buf.append("<b>").append(site).append("</b>&nbsp;:&nbsp;");
			}
			if (!city.isEmpty()) {
			     buf.append("<b>").append(city).append("</b>");
			}
			if (!country.isEmpty()) {
				if (!city.isEmpty()) {
					buf.append(" (");
				} else {
					buf.append("<b>");
				}
				buf.append(country);
				if (!city.isEmpty()) {
					buf.append(")");
				} else {
					buf.append("</b>");
				}
			}
			JLabel lb = new JLabel(buf.toString());
			//lb.setIcon(p.getIcon(16, 16));
			panel.add(lb, "span");
			oldCountry = country;
			oldCity = city;
			oldSite = site;
		}
	}

	@Override
	public void decorateEntity(JCheckBox cb, AbstractEntity entity) {
		if (!oldCountry.isEmpty() || !oldCity.isEmpty()) {
			JLabel lb=new JLabel("<html><p style='margin-left:5px'>&nbsp;");
			lb.setIcon(((Location)entity).getIcon());
			panel.add(lb, "split 2");
		}
		panel.add(cb);
	}

	@Override
	public void decorateAfterEntity(AbstractEntity entity) {
	}
}
