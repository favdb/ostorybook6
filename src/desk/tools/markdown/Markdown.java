/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools.markdown;

import desk.app.App;
import desk.tools.swing.SwingUtil;
import db.I18N;
import db.entity.Location;
import db.entity.Scene;
import db.entity.SceneScenario;
import desk.panel.scenario.ScenarioPanel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class Markdown extends JPanel implements ActionListener, CaretListener {

	private boolean modified;
	private String original;
	private ScenarioPanel caller;

	public enum ACTION {
		BT_LEFT("btLeft"),
		BT_RIGHT("btRight"),
		BT_BOTH("btBoth"),
		NONE("none");
		final private String text;

		private ACTION(String text) {
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

		public boolean check(String prop) {
			return text.equals(prop);
		}
	}

	public static ACTION getAction(String str) {
		for (ACTION a : ACTION.values()) {
			if (a.toString().equals(str)) {
				return (a);
			}
		}
		return (ACTION.NONE);
	}

	public enum VIEW {
		TEXT_ONLY,
		HTML_ONLY,
		ALL
	}

	private JScrollPane textScroll, htmlScroll;
	private JToolBar toolbar;
	private JButton btText, btHtml, btBoth;
	private String header = "", footer = "";
	private JTextArea text = new JTextArea(), status = new JTextArea();
	private JEditorPane html = new JEditorPane();
	private String type = "text/plain";

	public Markdown() {
		super();
		initAll();
	}

	public Markdown(String name) {
		super();
		setName(name);
		initAll();
	}

	public Markdown(String name, String type, String text) {
		super();
		setName(name);
		initAll();
		setContentType(type);
		setText(text);
	}

	public Markdown(String name, Scene scene) {
		super();
		setName(name);
		initAll();
		Location loc = null;
		if (!scene.locations.isEmpty()) {
			loc = scene.locations.get(0);
		}
		header = scene.scenario.getHeader(loc, scene.scenario);
		footer = scene.scenario.getFooter();
		setContentType("text/plain");
		setText(scene.writing);
	}

	public void setView(VIEW view) {
		switch (view) {
			case TEXT_ONLY:
				textScroll.setVisible(true);
				htmlScroll.setVisible(false);
				break;
			case HTML_ONLY:
				textScroll.setVisible(false);
				htmlScroll.setVisible(true);
				break;
			case ALL:
				textScroll.setVisible(true);
				htmlScroll.setVisible(true);
				break;
		}
	}

	public void setContentType(String type) {
		this.type = type;
		if (type.equals("text/plain")) {
			textScroll.setVisible(true);
			htmlScroll.setVisible(false);
		} else {
			textScroll.setVisible(false);
			htmlScroll.setVisible(true);
		}
	}

	public String getContentType() {
		return (type);
	}

	public void setText(String text) {
		this.original = "";
		this.text.setText(text);
		this.original = text;
		modified = false;
		setHtml();
	}

	public String getText() {
		return (this.text.getText());
	}

	public void setHtml() {
		String str = this.text.getText();
		StringBuilder buf = new StringBuilder("<html>");
		buf.append("<head>\n<style type='text/css'>\n");
		buf.append(HtmlUtil.getScenarioCSS(true));
		buf.append("</style></head>\n");
		//buf.append(String.format("<body style=\"font-family:%s;font-size:%d\">\n",
		//        getFont().getFontName(), getFont().getSize()));
		buf.append("<body>");
		if (!header.isEmpty()) {
			buf.append(header);
		}
		buf.append(toHtml(str));
		if (!footer.isEmpty()) {
			buf.append(footer);
		}
		buf.append("</body></html>");
		this.html.setText(buf.toString());
		String s = I18N.getColonMsg("z.words") + TextUtil.countWords(text.getText());
		s += ", " + I18N.getColonMsg("z.chars") + TextUtil.countChars(text.getText());
		status.setText(s);
		html.setCaretPosition(0);
	}

	public String getHtml() {
		return (html.getText());
	}

	public boolean isModified() {
		return (!original.equals(text.getText()));
	}

	public void setCaretPosition(int pos) {
		text.setCaretPosition(pos);
		try {
			html.setCaretPosition(pos);
		} catch (Exception e) {

		}
	}

	public int getCaretPosition() {
		return (text.getCaretPosition());
	}

	public Font getEditorFont() {
		return (text.getFont());
	}

	public void setEditorFont(Font font) {
		text.setFont(font);
		html.setFont(font);
	}

	public void setFocus() {
		text.requestFocus();
	}

	public void setHeader(Location location, SceneScenario scenario) {
		//App.trace("Markdown.setHeader(scene="+scene.toHtmlShortDetail()+")");
		if (scenario == null) {
			header = "";
			footer = "";
		} else {
			header = scenario.getHeader(location, scenario);
			footer = scenario.getFooter();
		}
		setHtml();
	}

	private void initAll() {
		init();
		initUi();
	}

	private void init() {
		setFont(App.getInstance().fontGetMono());
		setEditorFont(App.getInstance().fontGetMono());
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		text.addCaretListener(this);
		html.setContentType("text/html");
		html.setEditable(false);
		status.setEditable(false);
		status.setColumns(80);
		status.setBackground(this.getBackground());
	}

	private void initUi() {
		setLayout(new MigLayout("hidemode 3, ins 1,wrap", "[grow][grow]"));
		//toolbar
		initToolbar();
		add(toolbar, "north, span");
		textScroll = new JScrollPane(text);
		SwingUtil.setMaxPreferredSize(textScroll);
		htmlScroll = new JScrollPane(html);
		SwingUtil.setMaxPreferredSize(htmlScroll);
		add(textScroll, "grow");
		add(htmlScroll, "grow");
		add(status, "newline, span");
		setContentType("text/plain");
		modified = false;
	}

	private void initToolbar() {
		toolbar = new JToolBar();
		toolbar.setLayout(new MigLayout("ins 1 3 1 3"));
		toolbar.setFloatable(false);
		// show text
		btText = initButton(ACTION.BT_LEFT.toString(), "", "small/screen-left", "screen.text");
		toolbar.add(btText);
		btHtml = initButton(ACTION.BT_RIGHT.toString(), "", "small/screen-right", "screen.html");
		toolbar.add(btHtml);
		btBoth = initButton(ACTION.BT_BOTH.toString(), "", "small/screen-both", "screen.both");
		toolbar.add(btBoth);
	}

	public void hideToolbar() {
		toolbar.setVisible(false);
	}

	public void setDimension() {
		text.setColumns(80);
	}

	public JButton initButton(String name, String text, String icon, String... tooltip) {
		//App.trace("AbstractPanel.initButton(name=" + name + ", text=" + text + ", icon=" + icon + ")");
		JButton btn = new JButton();
		btn.setName(name);
		if (text != null && !text.isEmpty()) {
			btn.setText(I18N.getMsg(text));
		}
		if (icon != null && !icon.isEmpty()) {
			btn.setIcon(IconUtil.getIcon(icon));
		}
		if (tooltip.length > 0) {
			btn.setToolTipText(I18N.getMsg(tooltip[0]));
		}
		btn.addActionListener(this);
		return (btn);
	}

	public void setCallback(ScenarioPanel caller) {
		this.caller = caller;
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		if (!original.isEmpty() && !text.getText().equals(original)) {
			modified = true;
			if (caller != null) {
				caller.setModified();
			}
		} else {
			modified = false;
		}
		setHtml();
		try {
			if (e.getDot() >= 0 && text != null && html != null) {
				int i = e.getDot();
				if (html.getText().length() > i) {
					html.setCaretPosition(i);
				}
			}
		} catch (Exception exc) {
		}
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		//App.trace("Mardown.actionPerformed(evt=" + evt.toString() + ")");
		if (evt.getSource() instanceof JButton) {
			JButton btn = (JButton) evt.getSource();
			switch (getAction(btn.getName())) {
				case BT_LEFT: {
					setView(VIEW.TEXT_ONLY);
					break;
				}
				case BT_RIGHT: {
					setView(VIEW.HTML_ONLY);
					break;
				}
				case BT_BOTH: {
					setView(VIEW.ALL);
					break;
				}
			}
			revalidate();
		}
	}

	public static String toHtml(String mark) {
		if (mark == null || mark.isEmpty()) {
			return ("");
		}
		boolean isBold = false,
				isUnderline = false,
				isItalic = false,
				isDidascalie = false,
				isDialog = false,
				isTitle = false,
				isPara = false;
		int title = 0;
		String[] lines = mark.split("\\r?\\n");
		Font font = App.getInstance().fontGetMono();
		StringBuilder html = new StringBuilder();
		StringBuilder b = new StringBuilder(html);
		for (String li : lines) {
			StringBuilder x = new StringBuilder();
			String line = li.trim();
			if (isTitle) {
				x.append(String.format("</h%d>\n", title));
				title = 0;
				isTitle = false;
				isPara = false;
			} else if (line.length() == 0) {
				if (isDidascalie) {
					b.append("</p>\n");
					isDidascalie = false;
					isPara = false;
				} else if (isDialog) {
					b.append("</p>\n");
					isDialog = false;
					isPara = false;
				} else {
					if (isPara) {
						b.append("</p>\n");
						isPara = false;
					}
				}
				continue;
			}
			for (int i = 0; i < line.length(); i++) {
				char c = line.charAt(i);
				switch (c) {
					case '*':
						if (i == 0 && !isBold && !isItalic) {
							if (isPara) {
								x.append("</p>\n<p>");
								isPara = false;
							} else {
								x.append("<p>");
								isPara = true;
							}
						}
						if (line.substring(i).startsWith("***")) {
							if (isItalic && isBold) {
								x.append("</b></i>");
								isItalic = false;
							} else if (isItalic && !isBold) {
								x.append("</i><b>");
								isItalic = false;
								isBold = true;
							} else if (!isItalic && isBold) {
								x.append("</b><i>");
								isItalic = true;
								isBold = false;
							} else {
								x.append("<b><i>");
								isItalic = true;
								isBold = true;
							}
							i += 2;
							break;
						} else if (line.substring(i).startsWith("**")) {
							x.append((isBold ? "</b>" : "<b>"));
							isBold = !isBold;
							i++;
							break;
						} else {
							if (isItalic) {
								x.append("</i>");
								isItalic = false;
							} else {
								x.append("<i>");
								isItalic = true;
							}
							break;
						}
					case '_':
						if (i == 0 && !isPara) {
							x.append("<p>");
							isPara = true;
						}
						if (isUnderline) {
							x.append("</u>");
							isUnderline = false;
						} else {
							x.append("<u>");
							isUnderline = true;
						}
						break;
					case '#':
						if (isPara && !isTitle) {
							x.append("</p>\n");
							isPara = false;
						}
						int j = 0;
						for (; j < line.length(); j++) {
							if (i + j >= line.length()) {
								break;
							}
							if (line.charAt(i + j) != '#') {
								break;
							}
						}
						title = j;
						x.append(String.format("<h%d>", title));
						i += j;
						isTitle = true;
						break;
					case '(':
						if (line.substring(i).startsWith("((")) {
							x.append("(");
							i++;
						} else if (i == 0) {
							if (isPara) {
								x.append("</p>\n<p>");
							} else {
								x.append("<p>");
							}
							x.append("<i>");
							isDidascalie = true;
							isPara = true;
						} else {
							x.append("<i>");
						}
						break;
					case ')':
						if (line.substring(i).startsWith("))")) {
							x.append(")");
							i++;
						} else {
							x.append("</i>");
							if (i + 1 < line.length()) {
								if (line.charAt(i + 1) == ' ') {
									x.append("<br />");
								}
							}
						}
						break;
					case '/':
						if (line.substring(i).startsWith("//")) {
							x.append("/");
							i++;
							break;
						}
						if (i == 0) {
							if (isPara) {
								x.append("</p");
							}
							x.append("<p style=\"text-align:center;\"><b>");
							isDialog = true;
							isPara = true;
						} else if (isDialog) {
							x.append("</b><br>\n");
							isDialog = false;
							isPara = true;
						} else {
							x.append(c);
						}
						break;
					case '-':
						if (i + 1 <= line.length() && line.charAt(i + 1) == '-') {
							if (i == 0 && !isPara) {
								x.append("<p>");
								isPara = true;
							}
							x.append("&mdash");
							i++;
							break;
						}
					default:
						if (!isPara && !isTitle) {
							x.append("<p>");
							isPara = true;
						}
						x.append(c);
				}
			}
			b.append(x.toString());
		}
		int i = b.toString().lastIndexOf("<p");
		int j = b.toString().lastIndexOf("</p");
		if (j < i) {
			b.append("</p>");
		}
		return (b.toString());
	}

	public static String toMarkdown(String html) {
		if (html == null || html.isEmpty()) {
			return ("");
		}
		String r = html;
		r = r.replace("<i>", "*").replace("</i>", "*");
		r = r.replace("<b>", "**").replace("</b>", "**");
		r = r.replace("<u>", "_").replace("</u>", "_");
		r = r.replace("&mdash;", "--");
		r = HtmlUtil.htmlToText(r);
		r = r.replace("\n", "\n\n");
		return (r);
	}

}
