/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.tools.combobox;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import javax.swing.Icon;
import tools.IconUtil;

public class EntityTypeCbItem {

	public static enum Type {
		PERSON, SCENE, LOCATION, TAG, ITEM, POV, RELATION, PLOT;
	}

	private final Type type;

	public EntityTypeCbItem(Type paramType) {
		this.type = paramType;
	}

	public String getText() {
		return(type.name().toLowerCase());
	}

	public Icon getIcon() {
		return(IconUtil.getIcon("small/"+getText()));
	}

	public Type getType() {
		return this.type;
	}

	private AbstractEntity getEntity() {
		Book book=null;
		switch (type) {
			case SCENE:
				return (new Scene());
			case PERSON:
				return (new Person());
			case PLOT:
				return (new Plot());
			case LOCATION:
				return (new Location());
			case ITEM:
				return (new Item());
			case TAG:
				return (new Tag());
			case POV:
				return (new Pov());
			case RELATION:
				return(new Relation());
		}
		return null;
	}

	@Override
	public boolean equals(Object paramObject) {
		if (getClass() != paramObject.getClass()) {
			return false;
		}
		EntityTypeCbItem localEntityTypeCbItem = (EntityTypeCbItem) paramObject;
		boolean bool = true;
		bool = (bool) && (this.type.equals(localEntityTypeCbItem.type));
		return bool;
	}

	@Override
	public int hashCode() {
		int i = super.hashCode();
		i = i * 31 + (this.type != null ? this.type.hashCode() : 0);
		return i;
	}

	@Override
	public String toString() {
		return getText();
	}

}
