/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools.combobox;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Part;
import desk.app.MainFrame;
import desk.listcell.LCREntity;
import db.I18N;
import java.util.List;
import javax.swing.JComboBox;

/**
 *
 * @author favdb
 */
public class ComboUtil {
	public static boolean NEW=true, EMPTY=true, ALL=true;
	
	@SuppressWarnings("unchecked")
	public static void fillPartCombo(MainFrame mainFrame, JComboBox combo, Part entity,
			boolean isNew, boolean addEmptyItem) {
		//App.trace("EntityUtil.fillPartCombo(...)");
		combo.removeAllItems();
		int i = 0;
		if (addEmptyItem) {
			++i;
			combo.addItem("");
		}
		for (Part part : mainFrame.book.parts) {
			combo.addItem(part);
			if (entity != null) {
				if (entity.id.equals(part.id)) {
					combo.setSelectedIndex(i);
				}
			}
			++i;
		}
		combo.revalidate();
	}

	public static void fillEntity(Book book, JComboBox combo, Book.TYPE objtype) {
		ComboUtil.fillEntity(book, combo, objtype, null, null, !NEW, !EMPTY, !ALL);
	}
	
	@SuppressWarnings("unchecked")
	public static void fillEntity(Book book, JComboBox combo, Book.TYPE objtype,
			AbstractEntity toSel, AbstractEntity toHide, boolean isNew, boolean addEmpty, boolean addAll) {
		/*App.trace("ComboUtil.fillEntity(book, objtype="+objtype
				+", toSel="+(toSel==null?"null":toSel.name)
				+", toHide="+(toHide==null?"null":toHide.name)
				+", isNew="+(isNew?"true":"false")
				+", empty="+(addEmpty?"true":"false")
				+", all="+(addAll?"true":"false")
				+")");*/
		combo.removeAllItems();
		combo.setRenderer(new LCREntity(book, objtype));
		if (addEmpty) {
			combo.addItem(" ");
		}
		if (addAll) {
			combo.addItem(I18N.getMsg("z.all"));
		}
		List<AbstractEntity> entities = (List<AbstractEntity>) book.getEntities(objtype);
		entities.forEach((entity) -> {
			if (toHide==null || !entity.equals(toHide)) {
				combo.addItem(entity);
			}
		});
		if (toSel != null) {
			combo.setSelectedItem(toSel);
		}
		combo.revalidate();
	}

	@SuppressWarnings("unchecked")
	public static void fillCombo(JComboBox combo, List<String> strs, String toSel, boolean isNew, boolean addEmpty) {
		combo.removeAllItems();
		if (addEmpty) {
			combo.addItem("");
		}
		strs.forEach((str) -> {
			combo.addItem(str);
		});
		if (toSel!=null) {
			combo.setSelectedItem(toSel);
		}
	}
	
}
