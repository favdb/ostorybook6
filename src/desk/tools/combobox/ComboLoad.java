/*
 * SbApp: Open Source software for novelists and authors.
 * Original idea 2008 - 2012 Martin Mustun
 * Copyrigth (C) Favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package desk.tools.combobox;

import db.entity.AbstractEntity;
import db.entity.Category;
import db.entity.Gender;
import db.entity.Item;
import db.entity.Location;
import db.entity.Part;
import db.entity.Person;
import db.entity.Pov;
import db.entity.Scene;
import db.entity.Status;
import db.entity.Tag;
import desk.app.MainFrame;
import static desk.assistant.AssistantPanel.TILDA;
import db.I18N;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author favdb
 */
public class ComboLoad {

	@SuppressWarnings("unchecked")
	public static void loadCbStatus(JComboBox cb, Integer stat) {
		cb.removeAllItems();
		for (Status.STATUS status : Status.STATUS.values()) {
			cb.addItem(Status.getMsg(status));
		}
		if (stat!=null) cb.setSelectedIndex(stat);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbProgression(MainFrame mainFrame, JComboBox cb, String toSel) {
		if (mainFrame.assistant == null) {
			return;
		}
		cb.removeAllItems();
		cb.addItem("");//empty item
		String key = "vogler.1.01.list";
		String t = mainFrame.assistant.getMsg(key);
		if (t == null) {
			return;
		}
		if (t.contains(TILDA)) {
			String x[] = t.split(TILDA);
			String y[] = x[1].split(";");
			for (String y1 : y) {
				cb.addItem(y1);
			}
			if (toSel != null && !toSel.isEmpty()) {
				cb.setSelectedItem(toSel);
			}
		} else {
			for (int i = 1; i < 100; i++) {
				String str = mainFrame.assistant.getMsg(key + "." + String.format("%02d", i));
				if (str == null) {
					break;
				}
				JLabel lb = new JLabel(str);
				if (str.contains(":")) {
					String x[] = str.split(":");
					lb.setText(x[0]);
					lb.setToolTipText(x[1]);
				}
				cb.addItem(lb.getText());
			}
			if (toSel != null && !toSel.isEmpty()) {
				cb.setSelectedItem(toSel);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void loadLbPovs(MainFrame mainFrame, JList lb, Scene scene) {
		DefaultListModel listModel = new DefaultListModel();
		if (!"".equals(scene.name)) {
			int ix = -1, i = 0;
			for (Pov pov : mainFrame.book.povs) {
				listModel.addElement(pov.name);
				if (scene.pov.id.equals(pov.id)) {
					ix = i;
				}
				i++;
			}
			lb.setModel(listModel);
			lb.setSelectedIndex(ix);
		} else {
			lb.setModel(listModel);
		}
	}

	@SuppressWarnings("unchecked")
	public static void loadCbGenders(MainFrame mainFrame, JComboBox cb, Person person) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Gender gender : mainFrame.book.genders) {
			cb.addItem(gender.name);
			if (person.gender.id.equals(gender.id)) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadLbPersons(MainFrame mainFrame, JList lb, Scene scene) {
		DefaultListModel listModel = new DefaultListModel();
		if (!"".equals(scene.name)) {
			int i = 0;
			int[] indices = {};
			for (Person person : mainFrame.book.persons) {
				listModel.addElement(person.getFullName());
				if (scene.persons.contains(person)) {
					indices[indices.length] = i;
				}
				i++;
			}
			lb.setModel(listModel);
			lb.setSelectedIndices(indices);
		} else {
			lb.setModel(listModel);
		}
	}

	@SuppressWarnings("unchecked")
	public static void loadCbParts(MainFrame mainFrame, JComboBox cb, Part ppart, boolean addAll) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		if (addAll) {
			cb.addItem(I18N.getMsg("part.all"));
			ix = 0;
			i++;
		}
		for (Part part : mainFrame.book.parts) {
			cb.addItem(part);
			if (ppart.id != null && ppart.id.equals(part.id)) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbLocations(MainFrame mainFrame, JComboBox cb, Scene scene) {
		if (!"".equals(scene.name)) {
			int ix = -1, i = 0;
			cb.removeAllItems();
			List<Location> sceneloc = scene.locations;
			for (Location location : mainFrame.book.locations) {
				cb.addItem(location.getFullName());
				if ((sceneloc.contains(location))) {
					ix = i;
				}
				i++;
			}
			cb.setSelectedIndex(ix);
		}
	}

	@SuppressWarnings("unchecked")
	public static void loadCbCities(MainFrame mainFrame, JComboBox cb, Location location) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Location city : mainFrame.book.locations) {
			if (city.hasCity()) {
				cb.addItem(city);
				if (location.equals(city)) {
					ix = i;
				}
				i++;
			}
		}
		cb.setSelectedIndex(ix);
	}

	public static void loadCbSites(MainFrame mainFrame, JComboBox<Location> cb, Location location) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Location site : mainFrame.book.locations) {
			if (site.hasSup()) {
				cb.addItem(site);
				if (location.equals(site)) {
					ix = i;
				}
				i++;
			}
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbCountries(MainFrame mainFrame, JComboBox cb, Location location) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Location country : mainFrame.book.locations) {
			if (country.hasCountry()) {
				cb.addItem(country);
				if (location.equals(country)) {
					ix = i;
				}
				i++;
			}
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbEntities(JComboBox cb, List<?> entities, AbstractEntity toSel,
			boolean isEmpty, boolean isAll) {
		if (entities==null || entities.isEmpty() || cb==null) return;
		cb.removeAllItems();
		if (isEmpty) {
			cb.addItem("");
		}
		if (isAll) {
			cb.addItem(I18N.getMsg("z.all"));
		}
		entities.forEach((entity) -> {
			cb.addItem(entity);
		});
		if (toSel!=null) {
			cb.setSelectedItem(toSel);
		}
	}

	@SuppressWarnings("unchecked")
	public static void loadCbCategories(MainFrame mainFrame, JComboBox cb, Category cat) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Category category : mainFrame.book.categories) {
			cb.addItem(category);
			if (cat != null && cat.id.equals(category.id)) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	public static boolean isMultiLbContains(JList lb, List ls) {
		List lx = lb.getSelectedValuesList();
		if (lx.equals(ls)) {
			return (true);
		}
		return (false);
	}

	public static boolean isCbEquals(JComboBox cb, String str) {
		if (cb.getSelectedIndex() == -1) {
			if (str.equals("")) {
				return (true);
			}
		} else {
			if (str.equals("")) {
				return (false);
			}
			if (cb.getSelectedItem().equals(str)) {
				return (true);
			}
		}
		return (false);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbItems(MainFrame mainFrame, JComboBox cb, Item item) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Item u : mainFrame.book.items) {
			cb.addItem((String) u.name);
			if ((item != null) && (!item.name.isEmpty()) && (item.name.equals(u.name))) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	static void loadLbItems(MainFrame mainFrame, JList lb, Scene scene) {
		DefaultListModel listModel = new DefaultListModel();
		lb.setModel(listModel);
		int i = 0;
		int[] indices = {};
		for (Item item : mainFrame.book.items) {
			listModel.addElement(item.name);
			if (scene.items.contains(item)) {
				indices[indices.length] = i;
			}
			i++;
		}
		lb.setSelectedIndices(indices);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbTags(MainFrame mainFrame, JComboBox cb, Tag tag) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Tag u : mainFrame.book.tags) {
			cb.addItem((String) u.name);
			if ((tag != null) && (!tag.name.isEmpty()) && (tag.name.equals(u.name))) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbPersons(MainFrame mainFrame, JComboBox cb, Person person) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Person u : mainFrame.book.persons) {
			cb.addItem(u.getFullName());
			if ((person != null) && person.equals(u)) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbLocations(MainFrame mainFrame, JComboBox cb, Location location) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Location u : mainFrame.book.locations) {
			cb.addItem(u.getFullName());
			if ((location != null) && (!location.getFullName().isEmpty()) && (location.getFullName().equals(u.getFullName()))) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

	@SuppressWarnings("unchecked")
	public static void loadCbScenes(MainFrame mainFrame, JComboBox cb, Scene scene) {
		cb.removeAllItems();
		int ix = -1, i = 0;
		for (Scene u : mainFrame.book.scenes) {
			cb.addItem(u.name);
			if (scene.equals(u)) {
				ix = i;
			}
			i++;
		}
		cb.setSelectedIndex(ix);
	}

}
