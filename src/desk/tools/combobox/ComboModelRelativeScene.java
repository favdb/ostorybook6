/*
Storybook: Open Source software for novelists and authors.
Copyright (C) 2008 - 2012 Martin Mustun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package desk.tools.combobox;

import javax.swing.DefaultComboBoxModel;
import db.entity.Scene;
import desk.app.MainFrame;

public class ComboModelRelativeScene extends DefaultComboBoxModel implements ComboModelIRefreshable {

	private MainFrame mainFrame;

	public ComboModelRelativeScene() {
	}

	@Override
	@SuppressWarnings("unchecked")
	public void refresh() {
		if (mainFrame == null) {
			return;
		}
		for (Scene scene : mainFrame.book.scenes) {
			addElement(scene);
		}
	}

	@Override
	public void setSelectedItem(Object obj) {
		Scene scene;
		if (obj instanceof Long) {
			scene = mainFrame.book.getScene((Long)obj);
		} else {
			scene = (Scene) obj;
		}
		super.setSelectedItem(scene);
	}

	@Override
	public MainFrame getMainFrame() {
		return mainFrame;
	}

	@Override
	public void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}

}
