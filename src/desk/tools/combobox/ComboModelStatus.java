/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.tools.combobox;

import db.entity.Status;
import db.entity.Status.STATUS;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;

/**
 *
 * @author favdb
 */
public class ComboModelStatus extends DefaultComboBoxModel {

	public ComboModelStatus() {
	}

	@SuppressWarnings("unchecked")
	public ComboModelStatus(boolean addPseudoStates) {
		StateModel model = new StateModel(addPseudoStates);
		for (AbstractState state : model.getStates()) {
			addElement(state);
		}
	}

	public static class AbstractState {

		protected Integer number;
		protected String name;
		protected Icon icon;

		public AbstractState() {
			super();
		}

		public AbstractState(Integer num, String nam, Icon ico) {
			number = num;
			name = nam;
			icon = ico;
		}

		public Integer getNumber() {
			return number;
		}

		public String getName() {
			return name;
		}

		public Icon getIcon() {
			return icon;
		}

		public String getToolTip() {
			return toString();
		}

		@Override
		public String toString() {
			return name;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof AbstractState)) {
				return false;
			}
			AbstractState test = (AbstractState) obj;
			boolean ret = true;
			ret = ret && number.equals(test.number);
			return ret;
		}

		@Override
		public int hashCode() {
			int hash = super.hashCode();
			hash = hash * 31 + number.hashCode();
			return hash;
		}
	}

	public static class StateModel {

		protected List<AbstractState> states;

		public StateModel() {
			this(true);
		}

		public StateModel(boolean addPseudoStates) {
			super();
			states = new ArrayList<>();
			states.add(new AbstractState(STATUS.OUTLINE.ordinal(),
					Status.getMsg(STATUS.OUTLINE),
					Status.getIcon(STATUS.OUTLINE)));
			states.add(new AbstractState(STATUS.DRAFT.ordinal(),
					Status.getMsg(STATUS.DRAFT),
					Status.getIcon(STATUS.DRAFT)));
			states.add(new AbstractState(STATUS.EDIT1.ordinal(),
					Status.getMsg(STATUS.EDIT1),
					Status.getIcon(STATUS.EDIT1)));
			states.add(new AbstractState(STATUS.EDIT2.ordinal(),
					Status.getMsg(STATUS.EDIT2),
					Status.getIcon(STATUS.EDIT2)));
			states.add(new AbstractState(STATUS.DONE.ordinal(),
					Status.getMsg(STATUS.DONE),
					Status.getIcon(STATUS.DONE)));
			states.add(new AbstractState(STATUS.CANCELED.ordinal(),
					Status.getMsg(STATUS.CANCELED),
					Status.getIcon(STATUS.CANCELED)));
			if (addPseudoStates) {
				states.add(new AbstractState(STATUS.INPROGRESS.ordinal(),
						Status.getMsg(STATUS.INPROGRESS),
						Status.getIcon(STATUS.INPROGRESS)));
			}
		}

		public List<AbstractState> getStates() {
			return states;
		}
	}
}
