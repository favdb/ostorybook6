package desk.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

import desk.app.App;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Scene;
import db.I18N;
import desk.app.MainFrame;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import tools.TextUtil;
import tools.html.HtmlUtil;

public final class XeditorUtil {

	/**
	 * get book size in characters (including spaces).
	 *
	 * @param mainFrame to use
	 * @return the number of characters
	 */
	public static Long getBookSize(MainFrame mainFrame) {
		Map<Scene, Long> sizes = getScenesSize(mainFrame);
		Long ret = 0L;
		for (Long value : sizes.values()) {
			ret += value;
		}
		return ret;
	}

	/**
	 * get book size in words.
	 *
	 * @param mainFrame to use
	 * @return the number of words
	 */
	public static Long getBookWords(MainFrame mainFrame) {
		Map<Scene, Long> sizes = getScenesWords(mainFrame);
		Long ret = 0L;
		for (Long value : sizes.values()) {
			ret += value;
		}
		return ret;
	}

	public static Map<Object, Long> getElementsSize(MainFrame mainFrame) {
		Map<Object, Long> sizes = new HashMap<>();
		// Get scenes
		sizes.clear();
		List<Part> roots = mainFrame.book.parts;
		for (Part root : roots) {
			appendElementSizes(mainFrame, root, sizes);
		}
		return sizes;
	}

	private static Long appendElementSizes(MainFrame mainFrame, Part part, Map<Object, Long> sizes) {
		Long ret = 0L;
		List<Part> subparts = Part.findSubpart(mainFrame.book.parts, part);
		List<Chapter> chapters = Chapter.find(mainFrame.book.chapters, part);
		for (Part subpart : subparts) {
			ret += appendElementSizes(mainFrame, subpart, sizes);
		}
		for (Chapter chapter : chapters) {
			Long chapterSize = appendElementSizes(mainFrame, chapter, sizes);
			ret += chapterSize;
		}
		sizes.put(part, ret);
		return ret;
	}

	private static Long appendElementSizes(MainFrame mainFrame, Chapter chapter, Map<Object, Long> sizes) {
		Long ret = 0L;
		List<Scene> scenes = Scene.find(mainFrame.book.scenes, chapter);
		for (Scene scene : scenes) {
			long sceneSize;
			if (mainFrame.bookPref.isUseXeditor()) {
				String filepath = getFilePath(mainFrame, scene);
				sceneSize = getDocumentSize(filepath);
			} else {
				sceneSize = HtmlUtil.htmlToText(scene.writing).length();
			}
			sizes.put(scene, sceneSize);
			ret += sceneSize;
		}
		sizes.put(chapter, ret);
		return ret;
	}

	public static Map<Scene, Long> getScenesSize(MainFrame mainFrame) {
		return getScenesSizeOrWords(mainFrame, false);
	}

	public static Map<Scene, Long> getScenesWords(MainFrame mainFrame) {
		return getScenesSizeOrWords(mainFrame, true);
	}

	private static Map<Scene, Long> getScenesSizeOrWords(MainFrame mainFrame, boolean wordsCount) {
		Map<Scene, Long> sceneSizes = new HashMap<>();
		// Get scenes
		sceneSizes.clear();
		List<Scene> scenes = mainFrame.book.scenes;
		// Get size of scenes from LibreOffice or texte
		for (Scene scene : scenes) {
			if (scene.chapter != null) {
				if (mainFrame.bookPref.isUseXeditor()) {
					String filepath = getFilePath(mainFrame, scene);
					sceneSizes.put(scene, getDocumentWords(filepath));
				} else if (wordsCount) { // words in texte
					Long size = (long)TextUtil.countWords(HtmlUtil.htmlToText(scene.writing));
					sceneSizes.put(scene, size);
				} else {
					sceneSizes.put(scene, (long)HtmlUtil.htmlToText(scene.writing).length());
				}
			}
		}
		return sceneSizes;
	}

	public static Long getSize(MainFrame mainFrame, Object object) {
		Long ret = 0L;
		if (object instanceof Scene) {
			if (mainFrame.bookPref.isUseXeditor()) {
				String filepath = getFilePath(mainFrame, (Scene) object);
				ret = getDocumentSize(filepath);
			} else {
				Scene scene = (Scene) object;
				ret = (long)HtmlUtil.htmlToText(scene.writing).length();
			}
		} else if (object instanceof Chapter) {
			Chapter chapter = (Chapter) object;
			List<Scene> scenes = Scene.find(mainFrame.book.scenes, chapter);
			for (Scene scene : scenes) {
				ret += getSize(mainFrame, scene);
			}
		} else if (object instanceof Part) {
			Part part = (Part) object;
			List<Chapter> chapters = Chapter.find(mainFrame.book.chapters, part);
			for (Chapter chapter : chapters) {
				ret += getSize(mainFrame, chapter);
			}
		}
		return ret;
	}

	public static String getFilePath(MainFrame mainFrame, Scene scene) {
		String stored = scene.xfile;
		if ((stored != null) && (!stored.isEmpty())) {
			return stored;
		} else {
			return getDefaultFilePath(mainFrame, scene);
		}
	}

	public static String getDefaultFilePath(MainFrame mainFrame, Scene scene) {
		// Have to calculate path from information
		String path = mainFrame.xml.getPath();
		String str1 = "";
		String ext = ".odt";
		if (mainFrame.bookPref.isUseXeditor()) {
			ext = "." + mainFrame.bookPref.Editor.xExtend;
		}
		Chapter chapter = scene.chapter;
		if (chapter != null) {
			str1 += chapter.number;
		}
		if (str1.length() < 2) {
			str1 = "0" + str1;
		}
		String str2 = "" + scene.number;
		if (str2.length() < 2) {
			str2 = "0" + str2;
		}
		String str = path + File.separator
				+ I18N.getMsg("chapter") + str1 + "-"
				+ I18N.getMsg("scene") + str2 + ext;
		return (str);
	}

	/**
	 * Get character size of an ODT file.
	 *
	 * @param filePath path to the file
	 * @return the size in characters (approximated) or zero.
	 */
	public static long getDocumentSize(String filePath) {
		File f = new File(filePath);
		long s = f.length();
		return (s);
	}

	/**
	 * Get words size of an external file.
	 *
	 * @param filePath path to the file
	 * @return the size in words (approximated) or zero.
	 */
	public static Long getDocumentWords(String filePath) {
		Long wordCount = 0L;
		Path textFilePath = Paths.get(filePath);
		try {
			Stream<String> fileLines = Files.lines(textFilePath, Charset.defaultCharset());
			wordCount = fileLines.flatMap(line -> Arrays.stream(line.split(" "))).count();
		} catch (IOException ex) {
			App.error("IO error counting words in "+filePath, ex);
		}
		return (wordCount);
	}

	public static File createNewExternal(MainFrame mainFrame, String name) {
		//App.trace("XeditorUtil.createNew("+name+")");
		File file = new File(name);
		String type = "libreoffice";
		if (mainFrame.bookPref.isUseXeditor()) {
			type = "xeditor";
		}
		if (JOptionPane.showConfirmDialog(null,
				I18N.getMsg("file.notexists",name),
				I18N.getMsg("xeditor.launch"),
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			try {
				String source = mainFrame.bookPref.Editor.xTemplate;
				InputStream is = new FileInputStream(new File(source));
				Files.copy(is, file.toPath());
			} catch (IOException ex) {
				App.error("XeditorUtil.createNewExternal(" + name + ")", ex);
			}
		} else {
			return (null);
		}
		return (file);
	}

	public static void launchExternalEditor(MainFrame mainFrame, File file) throws IOException {
		String[] cmdarray = null;
		String os = System.getProperty("os.name");
		try {
			if (os.startsWith("Windows")) {
				cmdarray = new String[]{"cmd", "/c", "start", "\"\"", file.getCanonicalPath()};
			} else if (os.startsWith("Mac OS")) {
				cmdarray = new String[]{"open", file.getCanonicalPath()};
			} else if (os.startsWith("Linux")) {
				cmdarray = new String[]{"xdg-open", file.getCanonicalPath()};
			} else {
				ExceptionDlg.show("unknown way to open " + file, new InterruptedException());
				return;
			}
			int res = Runtime.getRuntime().exec(cmdarray).waitFor();
			if (res != 0) {
				ExceptionDlg.show("error (" + res + ") executing " + Arrays.asList(cmdarray), new InterruptedException());
			}
		} catch (InterruptedException e) {
			ExceptionDlg.show("interrupted waiting for " + Arrays.asList(cmdarray), e);
		}
	}
	public static void launchExternalEditor(MainFrame mainFrame, Scene scene) throws IOException {
		//App.trace("XeditorUtil.launchExternalEditor(" + "mainFrame," + scene.getFullTitle() + ")");
		String name;
		if ((scene.xfile == null) || (scene.xfile.isEmpty())) {
			name = getDefaultFilePath(mainFrame, scene);
		} else {
			name = scene.xfile;
		}
		File f = new File(name);
		if (!f.exists()) {
			f = createNewExternal(mainFrame, name);
			if (f == null) {
				return;
			}
			scene.xfile=(name);
			mainFrame.getBookController().updateEntity(scene);
		}
		String[] cmdarray = null;
		String os = System.getProperty("os.name");
		try {
			if (os.startsWith("Windows")) {
				cmdarray = new String[]{"cmd", "/c", "start", "\"\"", f.getCanonicalPath()};
			} else if (os.startsWith("Mac OS")) {
				cmdarray = new String[]{"open", f.getCanonicalPath()};
			} else if (os.startsWith("Linux")) {
				cmdarray = new String[]{"xdg-open", f.getCanonicalPath()};
			} else {
				ExceptionDlg.show("unknown way to open " + f, new InterruptedException());
				return;
			}
			int res = Runtime.getRuntime().exec(cmdarray).waitFor();
			if (res != 0) {
				ExceptionDlg.show("error (" + res + ") executing " + Arrays.asList(cmdarray), new InterruptedException());
			}
		} catch (InterruptedException e) {
			ExceptionDlg.show("interrupted waiting for " + Arrays.asList(cmdarray), e);
		}
	}
}
