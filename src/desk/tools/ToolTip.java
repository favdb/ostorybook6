/*
 * Copyright (C) 2019 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools;

import db.entity.AbstractEntity;
import db.entity.Item;
import db.entity.Location;
import db.entity.Person;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.SbDate;
import db.entity.Scene;
import db.entity.Tag;
import db.I18N;
import db.entity.Book;
import java.util.List;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class ToolTip {

	public static String getToolTip(AbstractEntity entity) {
		return getToolTip(entity, null);
	}

	public static String getToolTip(AbstractEntity entity, SbDate date) {
		StringBuilder buf = new StringBuilder();
		buf.append("<html>");
		buf.append("<table width='300'>");
		buf.append("<tr><td>");
		buf.append(HtmlUtil.getTitle(entity.toString()));
		if (entity instanceof Pov) {
			buf.append(toolTipAppendPov((Pov) entity));
		} else if (entity instanceof Person) {
			buf.append(toolTipAppendPerson((Person) entity, date));
		} else if (entity instanceof Scene) {
			buf.append(toolTipAppendScene((Scene) entity));
		} else if (entity instanceof Relation) {
			buf.append(toolTipAppendRelation((Relation) entity));
		} else if (entity instanceof Location) {
			buf.append(toolTipAppendLocation((Location) entity));
		} else if (entity instanceof Plot) {
			buf.append(toolTipAppendPlot((Plot) entity));
		} else if (entity instanceof Item) {
			buf.append(toolTipAppendItem((Item) entity));
		} else if (entity instanceof Tag) {
			buf.append(toolTipAppendTag((Tag) entity));
		}
		buf.append("</td></tr>");
		buf.append("</table>");
		buf.append("</html>");
		return(buf.toString());
	}

	private static String toolTipAppendLocation(Location location) {
		StringBuilder buf=new StringBuilder();
		if (!location.city.isEmpty()) {
			buf.append(I18N.getColonMsg("location.city"));
			buf.append(" ");
			buf.append(location.city);
			buf.append("<br>");
		}
		if (!location.country.isEmpty()) {
			buf.append(I18N.getColonMsg("location.country"));
			buf.append(" ");
			buf.append(location.country);
		}
		buf.append("<p style='margin-top:5px'>");
		buf.append(TextUtil.truncate(location.getDescription()));
		return(buf.toString());
	}

	private static String toolTipAppendPlot(Plot plot) {
		StringBuilder buf=new StringBuilder();
		buf.append("<b>").append(I18N.getMsg("category")).append("</b>: ").append(plot.category.name).append("<br>");
		buf.append(plot.getDescription());
		return(buf.toString());
	}

	private static String toolTipAppendScene(Scene scene) {
		return(scene.getWriting(600));
	}

	private static String toolTipAppendPov(Pov pov) {
		return(pov.name);
	}

	public static String toolTipAppendPerson(Person person, SbDate date) {
		StringBuilder buf=new StringBuilder();
		if (date != null && person.birthday != null) {
			if (person.isDead(date)) {
				buf.append("+");
			} else {
				buf.append(I18N.getColonMsg("person.age")).append(" ").append(person.calculateAge(date));
			}
			buf.append("<br>");
		}
		buf.append(I18N.getColonMsg("gender")).append(" ").append(person.gender).append("<br>");
		if (person.hasCategory()) {
			buf.append(I18N.getColonMsg("category")).append(" ").append(person.category).append("<br>");
		}
		if (person.categories!=null && !person.categories.isEmpty()) {
			buf.append(I18N.getColonMsg("categories"));
			person.categories.forEach((c)-> {
				buf.append(c.name);
			});
			buf.append("<br>");
		}
		buf.append(TextUtil.truncate(person.getDescription())).append("<br>");
		return(buf.toString());
	}

	private static String toolTipAppendRelation(Relation relation) {
		StringBuilder buf=new StringBuilder();
		buf.append(I18N.getColonMsg("persons"));
		buf.append(" ");
		List<Person> persons = relation.persons;
		if (persons != null && !persons.isEmpty()) {
			buf.append(Book.getNames(persons));
		}
		List<Item> items = relation.items;
		if (items != null && !items.isEmpty()) {
			buf.append("<br>");
			buf.append(I18N.getColonMsg("items"));
			buf.append(Book.getNames(items));
		}
		List<Location> locations = relation.locations;
		if (locations != null && !locations.isEmpty()) {
			buf.append("<br>");
			buf.append(I18N.getColonMsg("locations"));
			buf.append(Book.getNames(locations));
		}
		return(buf.toString());
	}

	private static String toolTipAppendTag(Tag tag) {
		StringBuilder buf=new StringBuilder();
		buf.append(TextUtil.truncate(tag.getDescription()));
		return(buf.toString());
	}

	private static String toolTipAppendItem(Item item) {
		StringBuilder buf=new StringBuilder();
		buf.append(TextUtil.truncate(item.getDescription()));
		return(buf.toString());
	}

}
