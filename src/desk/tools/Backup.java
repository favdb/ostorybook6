/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.tools;

import desk.app.pref.AppPref;
import desk.app.MainFrame;

/**
 *
 * @author favdb
 */
public class Backup {
	private final MainFrame mainFrame;
	String dir="";
	Boolean open=false;
	Boolean close=false;
	Boolean increment=false;
	
	public Backup(MainFrame m) {
		mainFrame=m;
		String str=mainFrame.getPref().getString(AppPref.Key.BACKUP,"");
		if (!str.isEmpty()) {
			String p[]=str.split(",");
			if (p.length<4) {
				return;
			}
			dir=p[0];
			open=p[1].equals("true");
			close=p[2].equals("true");
			increment=p[3].equals("true");
		}
	}

	public String getDir() {
		return(dir);
	}
	
	public void setDir(String s) {
		dir=s;
	}

	public boolean getOpen() {
		return(open);
	}

	public void setOpen(boolean s) {
		open=s;
	}

	public boolean getClose() {
		return(close);
	}
	
	public void setClose(boolean s) {
		close=s;
	}

	public boolean getIncrement() {
		return(increment);
	}
	
	public void setIncrement(boolean s) {
		increment=s;
	}

	@Override
	public String toString() {
		return(dir+","+open.toString()+","+close.toString()+","+increment.toString());
	}

	public void saveParam() {
		mainFrame.getPref().setString(AppPref.Key.BACKUP,toString());
	}

}
