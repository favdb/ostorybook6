package desk.tools.swing.label;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.plaf.basic.BasicLabelUI;

public class VerticalLabelUI extends BasicLabelUI {

	static {
		labelUI = new VerticalLabelUI(false);
	}

	protected boolean clockwise;

	public VerticalLabelUI(boolean clockwise) {
		super();
		this.clockwise = clockwise;
	}

	@Override
	public Dimension getPreferredSize(JComponent c) {
		Dimension dim = super.getPreferredSize(c);
		return new Dimension(dim.height, dim.width);
	}

	private static final Rectangle PAINTICONR = new Rectangle();
	private static final Rectangle PAINTTEXTR = new Rectangle();
	private static final Rectangle PAINTVIEWR = new Rectangle();
	private static Insets paintViewInsets = new Insets(0, 0, 0, 0);

	@Override
	public void paint(Graphics g, JComponent c) {

		JLabel label = (JLabel) c;
		String text = label.getText();
		Icon icon = (label.isEnabled()) ? label.getIcon() : label.getDisabledIcon();

		if ((icon == null) && (text == null)) {
			return;
		}

		FontMetrics fm = g.getFontMetrics();
		paintViewInsets = c.getInsets(paintViewInsets);

		PAINTVIEWR.x = paintViewInsets.left;
		PAINTVIEWR.y = paintViewInsets.top;

		// Use inverted height & width
		PAINTVIEWR.height = c.getWidth() - (paintViewInsets.left + paintViewInsets.right);
		PAINTVIEWR.width = c.getHeight() - (paintViewInsets.top + paintViewInsets.bottom);

		PAINTICONR.x = PAINTICONR.y = PAINTICONR.width = PAINTICONR.height = 0;
		PAINTTEXTR.x = PAINTTEXTR.y = PAINTTEXTR.width = PAINTTEXTR.height = 0;

		String clippedText
				= layoutCL(label, fm, text, icon, PAINTVIEWR, PAINTICONR, PAINTTEXTR);

		Graphics2D g2 = (Graphics2D) g;
		AffineTransform tr = g2.getTransform();
		if (clockwise) {
			g2.rotate(Math.PI / 2);
			g2.translate(0, -c.getWidth());
		} else {
			g2.rotate(-Math.PI / 2);
			g2.translate(-c.getHeight(), 0);
		}

		if (icon != null) {
			icon.paintIcon(c, g, PAINTICONR.x, PAINTICONR.y);
		}

		if (text != null) {
			int textX = PAINTTEXTR.x;
			int textY = PAINTTEXTR.y + fm.getAscent();

			if (label.isEnabled()) {
				paintEnabledText(label, g, clippedText, textX, textY);
			} else {
				paintDisabledText(label, g, clippedText, textX, textY);
			}
		}

		g2.setTransform(tr);
	}
}
