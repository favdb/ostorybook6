/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.tools.swing;

import db.entity.AbstractEntity;

import tools.IconUtil;

import desk.app.App;
import desk.app.pref.AppPref;
import desk.app.MainFrame;
import desk.dialog.AbstractDialog;
import db.I18N;
import db.entity.Book;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.text.JTextComponent;
import lib.shef.ui.text.html.WysiwygHTMLEditorKit;
import resources.MainResources;

/**
 *
 * @author favdb
 */
public class SwingUtil {
	public final static String DEFAULT_LAF="cross";

	/**
	 * Create a standard JButton
	 *
	 * @param name : name of the button, may be empty
	 * @param text : text of the button, may be empty
	 * @param icon : icon of the button, may be empty
	 * @param tips : tooltip of the button, may be empty
	 * @param mnemo : mnemonic for the button, may be empty
	 * @param action : action when button is pressed, may be null if no action
	 * @return a JButton
	 */
	public static JButton initButton(String name, String text, String icon,
			String tips, String mnemo, AbstractAction action) {
		JButton bt = initButton(text, icon, tips);
		if (!name.isEmpty()) {
			bt.setName(name);
		}
		if (action != null) {
			bt.setAction(action);
		}
		if (!mnemo.isEmpty()) {
			bt.setMnemonic(mnemo.charAt(0));
		}
		return (bt);
	}

	/**
	 * Create a standard JButton
	 * 
	 * @param name
	 * @param text
	 * @param icon
	 * @param tips
	 * @return 
	 */
	public static JButton initButton(String name, String text, String icon, String tips) {
		JButton bt = initButton(text, icon, tips);
		if (!name.isEmpty()) {
			bt.setName(name);
		}
		return (bt);
	}

	/**
	 * Create a standard basic JButton
	 *
	 * @param text : text of the button, may be empty
	 * @param icon : icon of the button, may be empty
	 * @param tips : tooltip of the button, may be empty
	 * @return a JButton
	 */
	public static JButton initButton(String text, String icon, String tips) {
		JButton bt = new JButton();
		if (!text.isEmpty()) {
			bt.setText(I18N.getMsg(text));
		}
		if (!icon.isEmpty()) {
			bt.setIcon(IconUtil.getIcon(MainResources.class, icon));
		}
		if (!tips.isEmpty()) {
			bt.setToolTipText(I18N.getMsg(tips));
		}
		bt.setFocusable(false);
		bt.setMargin(new Insets(0, 0, 0, 0));
		return (bt);
	}

	public static void setLookAndFeel() {
		try {
			// get saved look and feel
			setLookAndFeel(App.getInstance().getPreferences().getString(AppPref.Key.LAF));
		} catch (Exception e) {
			setLookAndFeel(DEFAULT_LAF);
		}
	}

	public static void setLookAndFeel(String lookAndFeel) {
		try {
			@SuppressWarnings("UnusedAssignment")
			String lafClassName = UIManager.getCrossPlatformLookAndFeelClassName();
			switch (lookAndFeel) {
				case "cross":
					lafClassName = UIManager.getCrossPlatformLookAndFeelClassName();
					break;
				default:
					lafClassName = UIManager.getCrossPlatformLookAndFeelClassName();
					break;
			}
			App.getInstance().getPreferences().setString(AppPref.Key.LAF, lookAndFeel);
			UIManager.setLookAndFeel(lafClassName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
		}
	}

	public static String fontToHtml(JTextPane ta) {
		Font font = ta.getFont();
		String r = "style=\""
				+ "font-family: " + font.getFontName() + ";"
				+ "font-size: " + font.getSize() + "pt;"
				+ "\"";
		return (r);
	}

	public static Border getBorderDefault() {
		return getBorderDefault(1);
	}

	public static Border getBorderDefault(int thickness) {
		return BorderFactory.createLineBorder(Color.black, thickness);
	}

	public static Border getBorderRed() {
		return BorderFactory.createLineBorder(Color.red);
	}

	public static Border getBorderBlue() {
		return getBorderBlue(1);
	}

	public static Border getBorderBlue(int thickness) {
		return BorderFactory.createLineBorder(Color.blue, thickness);
	}

	public static Border getBorderGray() {
		return BorderFactory.createLineBorder(Color.gray);
	}

	public static Border getBorderLightGray() {
		return BorderFactory.createLineBorder(Color.lightGray);
	}

	public static Border getBorderEtched() {
		return BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
	}

	public static Border getBorder(Color clr) {
		return BorderFactory.createLineBorder(clr, 1);
	}

	public static Border getBorder(Color clr, int thickness) {
		return BorderFactory.createLineBorder(clr, thickness);
	}

	public static Border getBorderDot() {
		return new DotBorder();
	}

	public static void enableContainerChildren(Container container, boolean enable) {
		if (container != null) {
			for (Component comp : container.getComponents()) {
				try {
					comp.setEnabled(enable);
					((JComponent) comp).setOpaque(enable);
					if (comp instanceof Container) {
						enableContainerChildren((Container) comp, enable);
					}
				} catch (ClassCastException e) {
					// ignore component
				}
			}
		}
	}

	public static Dimension getDlgPosition(MainFrame m, AbstractEntity entity) {
		Dimension d = new Dimension(0, 0);
		AppPref pref = m.getPref();
		String keyName = "Editor_" + entity.type.toString();
		String x = pref.getString(keyName, "");
		if (x.isEmpty()) {
			return (d);
		}
		String z[] = x.split(",");
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		if (z.length > 4 && gs.length != Integer.parseInt(z[4])) {
			return (new Dimension(0, 0));
		}
		d = new Dimension();
		d.height = Integer.parseInt(z[0]);
		d.width = Integer.parseInt(z[1]);
		return (d);
	}

	public static Dimension getDlgSize(MainFrame m, AbstractEntity entity) {
		Dimension d = null;
		AppPref pref = m.getPref();
		String keyName = "Editor_" + entity.type.toString();
		String x = pref.getString(keyName, "");
		if (x.isEmpty()) {
			return (d);
		}
		String z[] = x.split(",");
		if (z.length < 4) {
			return (d);
		}
		d = new Dimension();
		d.height = Integer.parseInt(z[3]);
		d.width = Integer.parseInt(z[2]);
		return (d);
	}

	public static void saveDlgPosition(AbstractDialog dlg, AbstractEntity entity) {
		int X = dlg.getX();
		int Y = dlg.getY();
		int H = dlg.getHeight();
		int W = dlg.getWidth();
		String key = "Editor_" + entity.type.toString();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		String pos = X + "," + Y + "," + H + "," + W + "," + gs.length;
		AppPref pref = dlg.getMainFrame().getPref();
		if (!pos.equals(pref.getString(key, ""))) {
			pref.setString(key, pos);
		}
	}

	public static Dimension getScreenSize() {
		return (Toolkit.getDefaultToolkit().getScreenSize());
	}

	public static Color getTableSelectionBackgroundColor() {
		return UIManager.getColor("Table.selectionBackground");
	}

	public static Color getTableBackgroundColor() {
		return getTableBackgroundColor(false);
	}

	public static Color getTableBackgroundColor(boolean colored) {
		if (colored) {
			return new Color(0xf4f4f4);
		}
		return UIManager.getColor("Table.background");
	}

	public static Color getTableHeaderColor() {
		return UIManager.getColor("TableHeader.background");
	}

	public static JSlider createSafeSlider(int orientation, int min, int max,
			int value) {
		if (value < min) {
			value = min;
		} else if (value > max) {
			value = max;
		}
		return new JSlider(orientation, min, max, value);
	}

	public static class DotBorder implements Border {

		@Override
		public Insets getBorderInsets(Component c) {
			return new Insets(3, 3, 6, 6);
		}

		@Override
		public boolean isBorderOpaque() {
			return true;
		}

		@Override
		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.gray);
			int w = 1;
			//		float[] dash = { 1, 3 };
			float[] dash = {1, 2};
			float dash_phase = 1;
			g2.setStroke(new BasicStroke(w, BasicStroke.CAP_SQUARE,
					BasicStroke.JOIN_MITER, 10, dash, dash_phase));
			g2.drawRect(x, y, width - 2, height - 2);
		}
	}

	public static void setUIFont(javax.swing.plaf.FontUIResource f) {
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value instanceof javax.swing.plaf.FontUIResource) {
				UIManager.put(key, f);
			}
		}
	}

	public static void setMaxPreferredSize(JComponent comp) {
		comp.setPreferredSize(new Dimension(8192, 8192));
	}

	public static void setMaxWidth(JComponent comp, int width) {
		comp.setMaximumSize(new Dimension(width, Short.MAX_VALUE));
	}

	public static List<Component> findComponentsByClass(Container rootComponent, Class<? extends Component> cname) {
		List<Component> res=new ArrayList<>();
		if (rootComponent instanceof Container) {
			Component[] components = ((Container) rootComponent).getComponents();
			for (Component comp : components) {
				if (cname.isInstance(comp)) {
					res.add(comp);
				}
				if (comp instanceof Container) {
					res.addAll(findComponentsByClass((Container) comp, cname));
				}
			}
		}
		return res;
	}

	public static void setForcedSize(Component comp, Dimension dim) {
		comp.setMinimumSize(dim);
		comp.setPreferredSize(dim);
		comp.setMaximumSize(dim);
	}

	public static KeyStroke getKeyStrokeInsert() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0, false);
	}

	public static KeyStroke getKeyStrokeEnter() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false);
	}

	public static KeyStroke getKeyStrokeCopy() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
	}

	public static KeyStroke getKeyStrokeDelete() {
		return KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, false);
	}

	public static void forceRevalidate(Component comp) {
		comp.invalidate();
		comp.validate();
		comp.repaint();
	}

	public static void setWaitingCursor(Component comp) {
		comp.setCursor(new Cursor(Cursor.WAIT_CURSOR));
	}

	public static void setDefaultCursor(Component comp) {
		comp.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	public static void resetDlgPosition() {
		AppPref pref = App.getInstance().getPreferences();
		for (Book.TYPE k : Book.TYPE.values()) {
			String keyName = "Editor_" + k.toString();
			pref.removeString(keyName);
		}
	}

	public static String getNiceFontName(Font font) {
		if (font == null) {
			return "";
		}
		StringBuilder buf = new StringBuilder();
		buf.append(font.getName());
		buf.append(", ");
		switch (font.getStyle()) {
			case Font.BOLD:
				buf.append("bold");
				break;
			case Font.ITALIC:
				buf.append("italic");
				break;
			case Font.PLAIN:
				buf.append("plain");
				break;
		}
		buf.append(", ");
		buf.append(font.getSize());
		return buf.toString();
	}

	public static void addEnterAction(JComponent comp, Action action) {
		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), action);
		comp.getActionMap().put(action, action);
	}

	public static void addEscAction(JComponent comp, Action action) {
		comp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), action);
		comp.getActionMap().put(action, action);
	}

	public static Color getBackgroundColor() {
		return Color.white;
	}

	public static void setUnitIncrement(JScrollPane scroller) {
		scroller.getVerticalScrollBar().setUnitIncrement(20);
		scroller.getHorizontalScrollBar().setUnitIncrement(20);
	}

	public static void expandRectangle(Rectangle rect) {
		Point p = rect.getLocation();
		p.translate(-5, -5);
		rect.setLocation(p);
		rect.grow(10, 10);
	}

	private static Boolean flashIsRunning = false;

	@SuppressWarnings("SynchronizeOnNonFinalField")
	public static void flashComponent(JComponent comp) {
		synchronized (flashIsRunning) {
			if (flashIsRunning) {
				return;
			}
			flashIsRunning = true;
			Color bg = comp.getBackground();
			FlashThread flash = new FlashThread(comp);
			SwingUtilities.invokeLater(flash);
			FlashThread flash2 = new FlashThread(comp, bg);
			Timer timer = new Timer(200, flash2);
			timer.setRepeats(true);
			timer.start();
		}
	}

	@SuppressWarnings("SynchronizeOnNonFinalField")
	public static void flashEnded() {
		synchronized (flashIsRunning) {
			flashIsRunning = false;
		}
	}

	public static void addCtrlEnterAction(JComponent comp, AbstractAction action) {
		InputMap inputMap = comp.getInputMap();
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.CTRL_DOWN_MASK), action);
	}

	public static JTextComponent createTextComponent() {
		JTextComponent tc;
		tc = new JEditorPane();
		JEditorPane ep = (JEditorPane) tc;
		ep.setEditorKitForContentType("text/html", new WysiwygHTMLEditorKit());
		ep.setContentType("text/html");
		return tc;
	}

	public static void setAccelerator(JMenuItem menuItem, int key, int mask) {
		menuItem.setAccelerator(KeyStroke.getKeyStroke(key, mask));
	}
	
	public static void setDefaultScroller(JScrollPane scroller) {
		setUnitIncrement(scroller);
		setMaxPreferredSize(scroller);
	}

}
