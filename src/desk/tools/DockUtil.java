/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.tools;

import desk.app.App;
import desk.app.MainFrame;
import desk.app.pref.AppPref;
import desk.model.Ctrl;
import desk.panel.AbstractPanel;
import desk.view.SbView;
import desk.view.SbView.VIEWID;
import desk.view.SbViewFactory;
import db.I18N;
import java.awt.event.ActionEvent;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import lib.infonode.docking.DockingWindow;
import lib.infonode.docking.RootWindow;
import lib.infonode.docking.TabWindow;
import lib.infonode.docking.View;
import lib.infonode.docking.WindowBar;
import lib.infonode.docking.properties.TabWindowProperties;
import lib.infonode.docking.util.StringViewMap;
import tools.Hexa;
import tools.Hexa.DecoderException;

/**
 *
 * @author favdb
 */
public class DockUtil {

	public static JMenu layoutGetMenu(MainFrame mainFrame) {
		String [] list={
			"default",
			"only_work",
			"only_chrono",
			"only_manage",
			"only_reading",
			"persons_locations",
			"screenplay",
			"theaterplay",
			"storyboard"
		};
		JMenu menu = new JMenu(I18N.getMsg("docking.predefined"));
		for (String key : list) {
			JMenuItem m = new JMenuItem();
			m.setName(key);
			m.setText(I18N.getMsg("docking.layout." + key));
			m.addActionListener((ActionEvent evt) -> {
				layoutLoadFile(mainFrame, key);
			});
			menu.add(m);
		}
		return (menu);
	}

	public static void setRespectMinimumSize(MainFrame mainFrame) {
		SbView view = mainFrame.getView(VIEWID.VIEW_TREE);
		if (view==null) return;
		DockingWindow win = view.getWindowParent();
		if (win instanceof TabWindow) {
			TabWindowProperties props = new TabWindowProperties();
			props.setRespectChildWindowMinimumSize(true);
			((TabWindow) win).getTabWindowProperties().addSuperObject(props);
		} else if (win instanceof WindowBar) {
			// can't be set for a WindowBar
		}
	}

	/**
	 * Get the current layout a a hexidecimal string
	 *
	 * @param mainFrame
	 * @return 
	**/
	public static String layoutGet(MainFrame mainFrame) {
		try {
			RootWindow rootWindow = mainFrame.getRootWindow();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try (ObjectOutputStream out = new ObjectOutputStream(bos)) {
				rootWindow.write(out, false);
			}
			byte[] ba = bos.toByteArray();
			String hexString=Hexa.encodeHexString(ba);
			//if (App.btrace) {
				//System.out.println("value=" + hexString);
			//}
			//App.trace("DockUtil.layoutGet: layout size="+hexString.length());
			return(hexString);
		} catch (IOException e) {
			System.err.println("*** DockUtil.layoutToString(" + mainFrame.getName()
				+ ") \nException :" + e.getMessage());
		}
		return("");
	}

	/**
	 * Set the layout from a hexadecimal string
	 * 
	 * @param mainFrame
	 * @param hex 
	 */
	public static void layoutSet(MainFrame mainFrame, String hex) {
		//App.trace("DockUtil.layoutSet: layout size="+hex.length());
		try {
			mainFrame.setWaitingCursor();
			byte[] ba = Hexa.decodeHex(hex.toCharArray());
			try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(ba))) {
				RootWindow rootWindow = mainFrame.getRootWindow();
				try {
					rootWindow.read(in, true);
				} catch (NullPointerException | IOException e) {
					ExceptionDlg.show(I18N.getMsg("docking.err.layout_reading"),e);
				}
			}
			setRespectMinimumSize(mainFrame);
			unloadHiddenViews(mainFrame);
			mainFrame.setDefaultCursor();
		} catch (DecoderException | IOException e) {
		}
	}

	public static void unloadHiddenViews(MainFrame mainFrame) {
		//App.trace("DockUtil.unloadHiddenViews(mainFrame)");
		SbViewFactory viewFactory = mainFrame.getViewFactory();
		StringViewMap viewMap = viewFactory.getViewMap();
		Ctrl ctrl = mainFrame.getBookController();
		int c = viewMap.getViewCount();
		for (int i = 0; i < c; ++i) {
			View view = viewMap.getViewAtIndex(i);
			if (view instanceof SbView) {
				SbView sbView = (SbView) view;
				if (sbView.isWindowShowing()) { // window is showing
					if (!sbView.isLoaded()) {
						viewFactory.loadView(sbView);
						ctrl.attachView((AbstractPanel) view.getComponent());
					}
				} else // window is not showing
				 if (sbView.isLoaded()) {
						ctrl.detachView((AbstractPanel) view.getComponent());
						viewFactory.unloadView(sbView);
					}
			}
		}
	}
	
	public static void layoutSave(MainFrame mainFrame, String name) {
		try {
			RootWindow rootWindow = mainFrame.getRootWindow();
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			try (ObjectOutputStream out = new ObjectOutputStream(bos)) {
				rootWindow.write(out, false);
			}
			byte[] ba = bos.toByteArray();
			File f = new File(App.getPrefDir().getAbsolutePath() + File.separator + name + ".layout");
			if (name.equals("last")) {
				f=new File(mainFrame.xml.getOnlyPath()+File.separator+".last"+".layout");
			}
			FileOutputStream fos = new FileOutputStream(f.getAbsolutePath());
			fos.write(bos.toByteArray());
			fos.close();
			List<String> list = App.getInstance().getPreferences().getList(AppPref.Key.LAYOUT_LIST);
			list.add(name);
			App.getInstance().getPreferences().setList(AppPref.Key.LAYOUT_LIST, list);
			App.getInstance().getPreferences().setString(AppPref.Key.LAYOUT_LAST, name);
			App.getInstance().reloadMenuBars();
			App.getInstance().reloadStatusBars();
		} catch (IOException e) {
			System.err.println("*** DockUtil.saveLoayout(" + mainFrame.getName()
				+ "," + name + ") Exception :" + e.getMessage());
		}
	}

	public static boolean layoutLoad(MainFrame mainFrame, String name) {
		//App.trace("DockUtil.layoutLoad(mainFrame,name="+name+")");
		if (name.isEmpty()) {
			System.err.println("DockUtil.layoutLoad error: name is empty");
			return(false);
		}
		try {
			String xf=App.getPrefDir().getAbsolutePath()+File.separator+name;
			if (name.equals("last")) {
				xf=mainFrame.xml.getOnlyPath()+File.separator+".last"+".layout";
			}
			Path path = Paths.get(xf);
			if (name.isEmpty()) {
				path = Paths.get(App.getPrefDir().getAbsolutePath() + File.separator + "default.layout");
			}
			if (!path.toFile().exists()) {
				System.err.println("no layout to load from "+xf);
				return(false);
			}
			byte[] ba = Files.readAllBytes(path);
			try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(ba))) {
				RootWindow rootWindow = mainFrame.getRootWindow();
				rootWindow.read(in, true);
			}
			mainFrame.setWaitingCursor();
			setRespectMinimumSize(mainFrame);
			unloadHiddenViews(mainFrame);
			mainFrame.setDefaultCursor();
			return true;
		} catch (IOException ex) {
			System.err.println("*** DockUtil.loadLoayout(" + mainFrame.getName()
				+ "," + (name.isEmpty()?"empty name":name) + ") Exception :" + ex.getMessage());
		}
		return false;
	}

	public static void layoutLoadFile(MainFrame mainFrame, String name) {
		//App.trace("DockUtil.LoadLayoutFile(mainframe," + name + ")");
		String x = "/resources/layout/" + name.replace("docking.layout.", "") + ".layout";
		if (name.isEmpty()) {
			App.msg("DockUtil.layoutLoadFile warning: name is empty, loading default layout");
			x="/resources/layout/default.layout";
		}
		//App.trace("resource is " + x+"\n");
		try {
			InputStream stream = App.class.getResourceAsStream(x);
			BufferedInputStream buf = new BufferedInputStream(stream);
			byte[] b=new byte[stream.available()];
			int r=buf.read(b);
			if (b!=null && r>0) {
				try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b))) {
					RootWindow rootWindow = mainFrame.getRootWindow();
					rootWindow.read(in, true);
				}
				mainFrame.setWaitingCursor();
				setRespectMinimumSize(mainFrame);
				unloadHiddenViews(mainFrame);
				mainFrame.setDefaultCursor();
			}
		} catch (IOException ex) {
			System.err.println("unable to load docking resource : "+x);
		}
	}

}
