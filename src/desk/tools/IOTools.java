/*
 Storybook: Open Source software for novelists and authors.
 Copyright (C) 2008 - 2012 Martin Mustun

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.tools;

import db.entity.AbstractEntity;
import db.entity.Chapter;
import db.entity.Scene;
import desk.app.App;
import desk.app.MainFrame;
import db.I18N;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import java.nio.file.Files;
import javax.swing.JOptionPane;

/**
 * @author martin
 *
 */
public class IOTools {

	public static String getEntityFileNameForExport(MainFrame paramMainFrame, String paramString, AbstractEntity paramAbstractEntity) {
		String str1 = "";
		try {
			String str2 = paramMainFrame.xml.name;
			if (paramAbstractEntity == null) {
				str1 = str2 + " (" + I18N.getMsg("book.info") + ")";
			} else {
				str1 = str2 + " (" + paramString + ") - " + paramAbstractEntity.toString();
			}
			str1 = cleanupFilename(str1);
			str1 = str1.replaceAll("\\[", "(");
			str1 = str1.replaceAll("\\]", ")");
			str1 = str1.substring(0, 50);
		} catch (Exception localException) {
		}
		return str1;
	}

	public static String cleanupFilename(String paramString) {
		String str = paramString.replaceAll("[\\/:*?\"<>|]", "");
		str = str.replaceAll("\\\\", "");
		return str;
	}

	public static String readFileAsString(String filePath) throws java.io.IOException {
		byte[] buffer = new byte[(int) new File(filePath).length()];
		BufferedInputStream f = null;
		try {
			f = new BufferedInputStream(new FileInputStream(filePath));
			f.read(buffer);
		} finally {
			if (f != null) {
				try {
					f.close();
				} catch (IOException e) {
					App.error("IOTools.readFileAsString(" + filePath + ")", e);
				}
			}
		}
		return new String(buffer);
	}

	public static void writeFileFromString(File file, String str) {
		try {
			file.createNewFile();
			try (BufferedWriter f = new BufferedWriter(new FileWriter(file))) {
				f.write(str, 0, str.length());
			}
		} catch (IOException e) {
			App.error("IOTools.writeFileFromString(" + file.getAbsolutePath() + ",buffer)", e);
		}
	}

	public static InputStream stringToInputStream(String str) {
		return new ByteArrayInputStream(str.getBytes());
	}
	
	public static String convertToRrelativePath(String str, String path) {
		String rc=str.replace("file://"+path+File.pathSeparator, "");
		return(rc);
	}
	
	public static File selectDirectory(JDialog parent,String f) {
		JFileChooser chooser = new JFileChooser(f);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setDialogTitle(I18N.getMsg("z.directory.select"));
		chooser.setApproveButtonText(I18N.getMsg("z.directory.select"));
		int i = chooser.showOpenDialog(parent);
		if (i != 0) return(null);
		return chooser.getSelectedFile();
	}

	public static File selectFile(JDialog parent, String filename, String ext, String filter) {
		JFileChooser chooser = new JFileChooser(filename);
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileFilter fileFilter=new FileFilter(ext,filter);
		chooser.addChoosableFileFilter(fileFilter);
		chooser.setFileFilter(fileFilter);
		chooser.setSelectedFile(new File(filename));
		int i = chooser.showOpenDialog(parent);
		if (i != 0) return(null);
		return(chooser.getSelectedFile());
	}

	public static void launch(MainFrame mainFrame,Scene scene) {
		App.trace("IOUtil.launchExternalEditor("+ "mainFrame," + scene.getFullTitle() +")");
		String name;
		String type = "libreoffice";
		if (mainFrame.bookPref.Editor.xUse) {
			type = "xeditor";
		}
		if ((scene.xfile== null) || (scene.xfile.isEmpty())) {
			name = IOTools.getDefaultFilePath(mainFrame, scene);
		} else {
			name = scene.xfile;
		}
		File file = new File(name);
		if (!file.exists()) {
			file = createNewXfile(mainFrame, name);
			if (file == null) {
				return;
			}
			scene.xfile=(name);
			mainFrame.getBookController().updateEntity(scene);
		}
		try {
			Runtime.getRuntime().exec(file.getAbsolutePath());
		} catch (IOException e) {
			App.error("IOTools.launch(" + mainFrame + scene.getFullTitle() + ")", e);
		}
	}

	public static String getFilePath(MainFrame mainFrame, Scene scene) {
		String stored = scene.xfile;
		if ((stored != null) && (!stored.isEmpty())) {
			return stored;
		} else {
			return getDefaultFilePath(mainFrame, scene);
		}
	}

	public static String getDefaultFilePath(MainFrame mainFrame, Scene scene) {
		// Have to calculate path from information
		String path = mainFrame.xml.getPath();
		String str1 = "";
		String ext = ".odt";
		if (mainFrame.bookPref.Editor.xUse) {
			ext = "." + mainFrame.bookPref.Editor.xExtend;
		}
		Chapter chapter = scene.chapter;
		if (chapter != null) {
			str1 += chapter.number;
		}
		if (str1.length() < 2) {
			str1 = "0" + str1;
		}
		String str2 = "" + scene.number;
		if (str2.length() < 2) {
			str2 = "0" + str2;
		}
		String str = path + File.separator
			+ I18N.getMsg("chapter") + str1 + "-"
			+ I18N.getMsg("scene") + str2 + ext;
		App.trace("Scene external file=" + str);
		return (str);
	}

	public static File createNewXfile(MainFrame mainFrame, String name) {
		//System.out.println("IOUtil.createNew("+name+")");
		File file = new File(name);
		String type = "libreoffice";
		if (mainFrame.bookPref.Editor.xUse) {
			type = "xeditor";
		}
		if (JOptionPane.showConfirmDialog(null,
			I18N.getMsg("file.to_create",name),
			I18N.getMsg("xeditor.launch"),
			JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			try {
				String source = "resources/template/Empty.odt";
				if (!mainFrame.bookPref.Editor.xTemplate.isEmpty()) {
					source = mainFrame.bookPref.Editor.xTemplate;
				}
				InputStream is;
				if (mainFrame.bookPref.Editor.xUse) {
					is = new FileInputStream(new File(source));
				} else {
					is = mainFrame.getClass().getClassLoader().getResourceAsStream(source);
				}
				Files.copy(is, file.toPath());
			} catch (IOException ex) {
				App.error("IOTools.createNewXfile(" + name + ")", ex);
			}
		} else {
			return (null);
		}
		return (file);
	}

}
