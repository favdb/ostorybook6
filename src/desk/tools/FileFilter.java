/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.tools;

import java.io.File;

/**
 *
 * @author favdb
 */
public class FileFilter extends javax.swing.filechooser.FileFilter {
	public static String DB = ".osbk",
		HTML[] = {".htm", ".html"},
		PNG = ".png",
		IMG[] = {".png", ".jpg", ".jpeg", ".gif"},
		ODT = ".odt",
		XML = ".xml";

	private String extension;
    private String[] extensions;
	private final String description;
	
	public FileFilter(String ext, String desc) {
		extension=ext;
		description=desc;
	}

	public FileFilter(String[] extensions, String description) {
        this.extensions = extensions;
        this.description = description;
    }
	
	@Override
    public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		if (extension==null) {
			for (String x:extensions) {
				if (file.getName().endsWith(x)) return(true);
			}
			return(false);
		}
        return file.getName().endsWith(extension);
    }
	@Override
    public String getDescription() {
        return description;
    }
}
