/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package desk.tools;

import db.I18N;
import db.entity.AbstractEntity;
import db.entity.Book;
import desk.app.MainFrame;
import desk.dialog.edit.AbstractEditorPanel;
import desk.dialog.edit.CheckBoxPanel;
import desk.dialog.edit.Editor;
import desk.dialog.edit.EditorPlainText;
import desk.tools.combobox.ComboUtil;
import desk.tools.swing.SwingUtil;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import lib.miginfocom.swing.MigLayout;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class Ui {
	public static Dimension MINIMUM_SIZE = new Dimension(440, 120);
	public static Dimension MAXIMUM_SIZE = new Dimension(800, 600);
	public static Dimension PREF_SIZE = new Dimension(750, 560);
	public static boolean
			MANDATORY = true,
			NEW = true,
			ALL = true,
			EMPTY = true,
			HIDE = true,
			INFO = true,
			NEWTAB=true,
			GROW=true,
			TIME=true,
			TOP=true,
			WRAP=true;
	
	public static void addField(JPanel panel, String title, String top,
			JComponent comp, boolean grow, boolean mandatory, JTabbedPane tab) {
		if (tab!=null) {
			tab.add(comp,I18N.getMsg(title));
		} else {
			JLabel lb = new JLabel(I18N.getColonMsg(title));
			if (mandatory) {
				lb.setFont(FontUtil.getBold());
			}
			String c=top;
			if (!top.contains("wrap")) {
				if (!top.isEmpty()) c+=",right";
				else c="right";
			}
			String strgrow="";
			if (top.contains("wrap")) {
				strgrow="wrap,span,";
			}
			if (grow) {
				strgrow="grow";
			}
			try {
				panel.add(lb,c);
				panel.add(comp,strgrow);
			} catch(Exception e) {
			}
		}
	}
	
	public static void addLabel(JPanel panel, String title, boolean isTop, boolean mandatory) {
		String strtop="";
		if (isTop) {
			strtop="top,";
		}
		JLabel lb = new JLabel(I18N.getColonMsg(title));
		if (mandatory) {
			lb.setFont(FontUtil.getBold());
		}
		panel.add(lb,strtop+"right");
	}

	public static JTextField initTextField(JPanel p, String title, int len, String val, boolean isMandatory, boolean isInfo) {
		JTextField tf = new JTextField();
		tf.setName(title);
		tf.setText(val);
		boolean grow = false;
		if (len > 0) {
			tf.setColumns(len);
		} else {
			grow=true;
		}
		if (isInfo) {
			tf.setEditable(false);
		}
		addField(p,title,"",tf,grow,isMandatory,null);
		return (tf);
	}

	public static JTextField initTextField(JPanel p, String title, int len, Integer val,
			boolean isMandatory, boolean isInfo) {
		if (val==null) val=0;
		return (initTextField(p, title, len, val.toString(), isMandatory, isInfo));
	}

	public static JEditorPane initEditorPane(JPanel p, String title, String val, int len, boolean isMandatory) {
		JLabel lb = new JLabel(I18N.getColonMsg(title));
		if (isMandatory) {
			lb.setFont(FontUtil.getBold());
		}
		p.add(lb,"top");
		JEditorPane ta = new JEditorPane();
		//SwingUtil.setForcedSize(ta, MAXIMUM_SIZE);
		ta.setName(title);
		ta.setText(val);
		p.add(ta,"growx");
		return (ta);
	}
	
	public static JTextArea initTextArea(JPanel panel, String title, String val, int len,
			boolean mandatory, boolean info, JTabbedPane tab) {
		JTextArea ta=new JTextArea();
		ta.setName(title);
		if (val!=null) ta.setText(val);
		if (info) ta.setEditable(false);
		ta.setLineWrap(true);
        ta.setWrapStyleWord(true);
		if (len>0) ta.setRows(len);
		JScrollPane scroller = new JScrollPane(ta);
        //scroller.setMinimumSize(MINIMUM_SIZE);
		if (tab!=null) {
			//scroller.setMaximumSize(new Dimension(640,110));
		}
		addField(panel,title,"top",scroller,GROW,mandatory, tab);
		return(ta);
	}

	public static EditorPlainText initTextEditor(JPanel panel, String title, String val, int len,
			boolean mandatory, JTabbedPane tab) {
		EditorPlainText ta = new EditorPlainText(val,len);
		ta.setName(title);
		ta.setText(val);
		ta.setPreferredSize(new Dimension(800,600));
		JScrollPane scroller = new JScrollPane(ta);
		scroller.setPreferredSize(new Dimension(800,600));
		addField(panel,title,"top",ta,GROW,mandatory,tab);
		return (ta);
	}

	public static JComboBox initComboBox(JPanel panel, MainFrame mainFrame, String title,
			Book.TYPE objtype, AbstractEntity toSel, AbstractEntity toHide,
			boolean mandatory, boolean addNew, boolean addEmpty) {
		JComboBox combo = new JComboBox();
		//combo.setPreferredSize(new Dimension(250, 26));
		combo.setName(title);
		ComboUtil.fillEntity(mainFrame.book, combo, objtype, toSel, toHide, addNew, addEmpty,!ALL);
		if (addNew) {
			JPanel p=new JPanel(new MigLayout("inset 0"));
			JButton bt = new JButton(IconUtil.getIcon("small/new"));
			SwingUtil.setForcedSize(bt, new Dimension(22, 22));
			bt.addActionListener((ActionEvent e) -> {
				AbstractEntity newEntity=mainFrame.book.getEntityNew(objtype);
				boolean rc=Editor.show(mainFrame, newEntity, null);
				if (rc!=true) {
					mainFrame.book.entityNew(newEntity);
					ComboUtil.fillEntity(mainFrame.book, combo, objtype, newEntity, toHide, addNew, addEmpty,ALL);
				}
			});
			p.add(combo);p.add(bt,"shrink");
			addLabel(panel,title,!TOP,mandatory);
			panel.add(p,"al left");
		} else {
			addField(panel,title,"",combo,!GROW,mandatory,null);
		}
		return (combo);
	}

	public static JCheckBox initCheckBox(JPanel panel, String title, boolean check, boolean isMandatory, String... top) {
		JCheckBox cb = new JCheckBox();
		cb.setName(title);
		cb.setText(I18N.getMsg(title));
		cb.setSelected(check);
		if (isMandatory) {
			cb.setFont(FontUtil.getBold());
		}
		if (top.length>0) panel.add(cb,top[0]);
		else panel.add(cb);
		return (cb);
	}

	public static JComboBox initAutoCB(JPanel panel, MainFrame mainFrame, String title,
			Book.TYPE objtype, AbstractEntity toSel, boolean addNew) {
		return(initAutoCB(panel,mainFrame, title,objtype,toSel,null,!MANDATORY,addNew,EMPTY));
	}
	
	@SuppressWarnings("unchecked")
	public static JComboBox initAutoCB(JPanel panel, String title,
			List<String> list, String toSel,
			boolean mandatory, boolean addEmpty) {
		JComboBox combo=new JComboBox();
		combo.setName(title);
		combo.setMinimumSize(new Dimension(150,20));
		combo.setEditable(true);
		if (addEmpty) {
			combo.addItem("");
		}
		list.forEach((str)-> {
			combo.addItem(str);
		});
		if (toSel != null) {
			combo.setSelectedItem(toSel);
		}
		addField(panel,title,"",combo,!GROW,mandatory,null);
		return (combo);
	}

	public static JComboBox initAutoCB(JPanel panel, MainFrame mainFrame, String title, Book.TYPE objtype,
			AbstractEntity toSel, AbstractEntity toHide,
			boolean mandatory, boolean isNew, boolean addEmpty) {
		JComboBox combo=new JComboBox();
		combo.setName(title);
		combo.setMinimumSize(new Dimension(150,20));
		ComboUtil.fillEntity(mainFrame.book, combo, objtype, toSel, toHide, isNew, addEmpty, !ALL);
		IconButton bt = new IconButton("small/new",title+".new",null);
		bt.setPrefSize(24);
		bt.addActionListener((ActionEvent e) -> {
			AbstractEntity newEntity=mainFrame.book.getEntityNew(objtype);
			boolean rc=Editor.show(mainFrame, newEntity, null);
			if (rc!=true) {
				ComboUtil.fillEntity(mainFrame.book, combo, objtype, newEntity, toHide, isNew, addEmpty, !ALL);
			}
		});
		bt.setVisible(isNew);
		JPanel p=new JPanel(new MigLayout("ins 0"));
		p.add(combo);
		p.add(bt);
		addField(panel,title,"",p,!Ui.GROW,mandatory,null);
		return (combo);
	}

	@SuppressWarnings("unchecked")
	public static CheckBoxPanel initListBox(JPanel panel, MainFrame mainFrame, String title,
			AbstractEntity toSel, List<?> ls, boolean mandatory, JTabbedPane tab) {
		JPanel p=new JPanel(new MigLayout());
		p.setBorder(BorderFactory.createTitledBorder(I18N.getColonMsg(title)));
		String stitle=title;
		String s[]=title.split("\\.");
		if (s.length>1) stitle=s[1];
		CheckBoxPanel cb = new CheckBoxPanel(mainFrame, Book.getTYPE(s[0]), toSel);
		cb.setName(title);
		if (ls!=null) {
			cb.selectEntities((List<AbstractEntity>) ls);
		}
		JScrollPane scroller = new JScrollPane(cb);
		//SwingUtil.setUnitIncrement(scroller);
		if (tab!=null) {
	        scroller.setMinimumSize(new Dimension(220,350));
			scroller.setMaximumSize(new Dimension(250,350));
		} else {
	        scroller.setMinimumSize(new Dimension(210,210));
		}
		p.add(scroller, "grow, wrap");
		
		JButton btEdit = SwingUtil.initButton("z.edit", "small/edit", "z.edit");
		btEdit.setName("BtEdit" + title);
		btEdit.addActionListener((ActionEvent arg0) -> {
			((AbstractEditorPanel)panel).actionEntity(btEdit.getName(), cb);
		});
		p.add(btEdit, "split 2,right");
		
		JButton btAdd = SwingUtil.initButton("z.new", "small/plus", "z.new");
		btAdd.setName("BtAdd" + title);
		btAdd.setIcon(IconUtil.getIcon("small/plus"));
		btAdd.addActionListener((ActionEvent arg0) -> {
			((AbstractEditorPanel)panel).actionEntity(btAdd.getName(), cb);
		});
		p.add(btAdd);
		
		if (tab!=null) addField(panel,stitle,"",p,!GROW,mandatory,tab);
		else panel.add(p,"grow");
		return (cb);
	}

}
