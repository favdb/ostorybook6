/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.assistant;

import db.entity.BookAssistant;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import lib.miginfocom.swing.MigLayout;
import desk.app.App;

import db.I18N;
import desk.app.MainFrame;
import desk.dialog.AbstractDialog;
import desk.tools.swing.SwingUtil;
import tools.IconUtil;

/**
 *
 * @author FaVdB
 */
public class AssistantBookDlg extends AbstractDialog {
	int step=0;
	List<JPanel> panel;
	private JPanel mainPanel;
	private JButton btNext, btPrior, btCancel;
	
	private JLabel lbStep;
	private JTextArea taStep;
	private JLabel lbPolti;
	private JTextArea taPolti;
	private AssistantPanel panelStep;
	private AssistantPanel panelPolti;
	
	public AssistantBookDlg(MainFrame m) {
		super(m);
		initAll();
	}
	
	public static boolean show(MainFrame m) {
		AssistantBookDlg dlg=new AssistantBookDlg(m);
		dlg.setVisible(true);
		return(!dlg.canceled);
	}

	@Override
	public void init() {
		panel=new ArrayList<>();
		lbStep=new JLabel();
		taStep=initTextArea("");
		lbPolti=new JLabel();
		taPolti=initTextArea("");
	}
	
	private JTextArea initTextArea(String str) {
		JTextArea ta=new JTextArea();
		ta.setText(str);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setCaretPosition(0);
		return(ta);
	}
	
	@Override
	public void initUi() {
		super.initUi();
		setTitle(I18N.getMsg("assistant"));
		setLayout((new MigLayout("", "[][]")));

		JTabbedPane tbPane=new JTabbedPane();

		mainPanel=new JPanel(new MigLayout());
		initSubPanel();
		loadData(mainFrame.book.info.assistant);
		add(mainPanel,"span");

		btNext=getButton("export.nav.next","small/nav-next");
		btNext.addActionListener((ActionEvent evt) -> {
			stepNext();
		});

		btPrior=getButton("export.nav.previous","small/nav-previous");
		btPrior.addActionListener((ActionEvent evt) -> {
			stepPrior();
		});

		btCancel = getButton("cancel");
		btCancel.addActionListener((ActionEvent evt) -> {
			dispose();
		});

		add(btCancel);
		add(btPrior,"split 2, right");
		add(btNext);
		refreshPanel();
		this.setModal(true);
	}
	
	private void stepNext() {
		if (step==panel.size()-1) {
			apply();
			return;
		}
		JPanel p=panel.get(step);
		String pname=p.getName();
		String str,x;
		switch(pname) {
			case "step":
				str=((AssistantPanel)p).applySettings();
				if (str.isEmpty()) return;
				x=str.substring(str.indexOf("]")+1,str.lastIndexOf("["));
				lbStep.setText("<html>"+x+"</html>");
				break;
			case "stepnotes":
				break;
			case "polti":
				str=((AssistantPanel)p).applySettings();
				if (str.isEmpty()) return;
				x=str.substring(str.indexOf("]")+1,str.lastIndexOf("["));
				lbPolti.setText("<html>"+x+"</html>");
				break;
			case "poltinotes":
				break;
		}
		step++;
		refreshPanel();
	}

	private void stepPrior() {
		if (step>0) {
			step--;
			refreshPanel();
		}
	}

	private void apply() {
		BookAssistant ass=mainFrame.book.info.assistant;
		ass.setStep(lbStep.getText());
		ass.setStepNotes(taStep.getText());
		ass.setPolti(lbPolti.getText());
		ass.setPoltiNotes(taPolti.getText());
		dispose();
	}
	
	private void loadData(BookAssistant ass) {
		lbStep.setText(ass.getStep());
		taStep.setText(ass.getStepNotes());
		lbPolti.setText(ass.getPolti());
		taPolti.setText(ass.getPoltiNotes());
	}
	
	private void refreshPanel() {
		if (mainPanel.getComponentCount()>0) mainPanel.remove(0);
		JPanel p=panel.get(step);
		mainPanel.add(p,"span, growx");
		if (step==panel.size()-1) {
			btNext.setText(I18N.getMsg("z.end"));
			btNext.setIcon(IconUtil.getIcon("small/ok"));
		} else {
			btNext.setText(I18N.getMsg("export.nav.next"));
			btNext.setIcon(IconUtil.getIcon("small/nav-next"));
		}
		btPrior.setEnabled((step>0));
		this.revalidate();
		setResizable(true);
		pack();
		setLocationRelativeTo(mainFrame);
		setResizable(false);
	}

	private void initSubPanel() {
		App.trace("AssistantDlg.initSubpanel()");
		Dimension dimMed = new Dimension(640, 240);
		Dimension dimMax = new Dimension(800, 600);
		
		panelStep = new AssistantPanel(mainFrame.assistant,mainFrame.assistant.getMsg("book.1.tab"),"book.1");
		panelStep.setName("step");
		panelStep.setChoice(mainFrame.book.info.assistant.getStep());
		panelStep.setMinimumSize(dimMed);
		panel.add(panelStep);
		
		JPanel pStepNotes = new JPanel((new MigLayout("wrap", "[][grow]")));
		pStepNotes.setName("taStep");
		JScrollPane scroller2 = new JScrollPane(taStep);
		SwingUtil.setMaxPreferredSize(scroller2);
		pStepNotes.add(lbStep,"span");
		pStepNotes.add(new JLabel(I18N.getMsg("notes")),"top");
		pStepNotes.add(scroller2, "grow");
		pStepNotes.setPreferredSize(dimMed);
		pStepNotes.setMinimumSize(dimMed);
		panel.add(pStepNotes);

		panelPolti = new AssistantPanel(mainFrame.assistant,mainFrame.assistant.getMsg("polti.1.tab"),"polti.1");
		panelPolti.setName("polti");
		panelPolti.setChoice(mainFrame.book.info.assistant.getPolti());
		panelPolti.setMinimumSize(dimMed);
		panelPolti.setMaximumSize(dimMax);
		panel.add(panelPolti);
		
		JPanel pPoltiNotes = new JPanel((new MigLayout("wrap", "[][grow]")));
		pPoltiNotes.setName("taPolti");
		JScrollPane scroller3 = new JScrollPane(taPolti);
		SwingUtil.setMaxPreferredSize(scroller3);
		pPoltiNotes.add(lbPolti,"span");
		pPoltiNotes.add(new JLabel(I18N.getMsg("notes")),"top");
		pPoltiNotes.add(scroller3, "grow");
		pPoltiNotes.setPreferredSize(dimMed);
		pPoltiNotes.setMinimumSize(dimMed);
		panel.add(pPoltiNotes);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
	
}
