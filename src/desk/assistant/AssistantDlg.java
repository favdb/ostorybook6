/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.assistant;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import lib.miginfocom.swing.MigLayout;

import db.I18N;
import db.entity.AbstractEntity;
import db.entity.Chapter;
import db.entity.Scene;
import desk.app.MainFrame;
import desk.dialog.AbstractDialog;
import desk.dialog.edit.Editor;
import desk.tools.swing.SwingUtil;
import tools.IconUtil;

/**
 * Dialogue sous la forme d'une fenêtre avec onglet - les items de niveau 1 sont les titres des
 * onglets, exemple: book.01.tab - le numero de l'item Les items de niveau 2 sont soit: - un nombre
 * : ce qui implique un sous-élément - un type : list, text, checkbox, radiobox
 *
 * @author FaVdB
 */
public class AssistantDlg extends AbstractDialog {

	private final String element;
	private final AbstractEntity entity;
	private int nombre;
	private Assistant assistant;
	private List<AssistantPanel> listPanel;
	private boolean cleared = false;

	public AssistantDlg(MainFrame m, String n) {
		super(m);
		element = n.toLowerCase();
		this.entity = null;
		initAll();
	}

	@SuppressWarnings("null")
	private AssistantDlg(MainFrame m, AbstractEntity entity) {
		super(m);
		this.entity = entity;
		if ((entity instanceof Chapter) || (entity instanceof Scene)) {
			element = "polti";
		} else {
			element = entity.type.toString();
		}
		initAll();
	}

	public static String show(MainFrame m, String b, String n) {
		//App.trace("AssistantDlg.show(MainFrame...)\nb=" + b + "\nn=" + n);
		AssistantDlg dlg = new AssistantDlg(m, b);
		dlg.setChoice(n);
		dlg.setVisible(true);
		if (dlg.canceled) {
			return (n);
		}
		String str = dlg.applySettings();
		if (!str.isEmpty()) {
			return (/*Assistant.setTag(*/str/*,b)*/);
		}
		return (n);
	}

	public static String show(MainFrame m, AbstractEntity entity, String n) {
		//App.trace("AssistantDlg.show(EntityEditor...)\nentity=" + entity.getClass().getSimpleName() + "\nn=" + n);
		AssistantDlg dlg = new AssistantDlg(m, entity);
		dlg.setChoice(n);
		dlg.setVisible(true);
		if (dlg.canceled) {
			return (n);
		}
		if (dlg.cleared) {
			return ("");
		}
		String str = dlg.applySettings();
		if (!str.isEmpty()) {
			return (str);
		}
		return (n);
	}

	public static String show(Editor parent, AbstractEntity entity, String origin) {
		//App.trace("AssistantDlg.show(EntityEditor...)\nentity=" + entity.getClass().getSimpleName() + "\nn=" + origin);
		AssistantDlg dlg = new AssistantDlg(parent.getMainFrame(), entity);
		dlg.setChoice(origin);
		dlg.setVisible(true);
		if (dlg.canceled) {
			return (origin);
		}
		if (dlg.cleared) {
			return ("");
		}
		String str = dlg.applySettings();
		if (!str.isEmpty()) {
			return (str);
		}
		return (origin);
	}

	@Override
	public void init() {
		assistant = mainFrame.assistant;
		this.setModal(true);
		listPanel = new ArrayList<>();
	}

	@Override
	public void initUi() {
		this.setTitle(I18N.getMsg("assistant.title"));
		this.setMaximumSize(new Dimension(600, 500));
		setLayout(new MigLayout("fillx"));
		JTabbedPane tabbed = new JTabbedPane();
		for (nombre = 1; nombre < 10; nombre++) {
			String key = element + "." + String.format("%d", nombre);
			String title = assistant.getMsg(key + ".tab");
			if (title == null) {
				break;
			}
			AssistantPanel panel = new AssistantPanel(assistant, title, key);
			SwingUtil.setMaxWidth(panel, WIDTH);
			JScrollPane scroll = new JScrollPane(panel);
			scroll.setViewportView(panel);
			tabbed.add(panel.tabTitle.trim(), scroll);
			listPanel.add(panel);
		}
		this.add(tabbed, "span, growx, wrap");
		this.add(getClearedButton(), "span,split,right");
		this.add(buttonCancel(), "span,split,right");
		this.add(buttonOk(), "right");
		this.pack();
		this.setLocationRelativeTo(null);
	}

	protected JButton getClearedButton() {
		AbstractAction act = getClearedAction();
		JButton bt = new JButton(act);
		bt.setText(I18N.getMsg("z.clear"));
		bt.setIcon(IconUtil.getIcon("small/clear"));
		SwingUtil.addEscAction(bt, act);
		return bt;
	}

	protected AbstractAction getClearedAction() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cleared = true;
				dispose();
			}
		};
	}

	@Override
	protected AbstractAction actionOk() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				canceled = false;
				dispose();
			}
		};
	}

	private String applySettings() {
		StringBuilder res = new StringBuilder();
		listPanel.forEach((panel)-> {
			String x = panel.applySettings();
			if (!x.isEmpty()) {
				res.append(Assistant.tagStart(panel.tabTitle))
						.append("\n")
						.append(x)
						.append(Assistant.tagEnd(panel.tabTitle))
						.append("\n");
			}
		});
		return (res.toString());
	}

	private void setChoice(String n) {
		listPanel.forEach((panel)-> {
			panel.setChoice(n);
		});
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
