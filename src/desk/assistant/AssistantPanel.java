/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.assistant;

import desk.app.App;
import desk.tools.swing.SwingUtil;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import lib.miginfocom.swing.MigLayout;
import tools.html.HtmlUtil;


/**
 *
 * @author FaVdB
 */
public class AssistantPanel extends JPanel {

	public String tabTitle;
	private Assistant assistant;
	public static final String TILDA = "~";
	private List<Object> itemList;
	
	@SuppressWarnings("LeakingThisInConstructor")
	public AssistantPanel(Assistant ass, String title, String key) {
		assistant=ass;
		this.setLayout(new MigLayout());
		itemList = new ArrayList<>();
		tabTitle = title.trim();
		if (title.contains(";")) {
			String x[] = title.split(";");
			this.add(subtitle(x[1].trim()), "span,wrap");
			tabTitle = x[0];
		}
		for (int j = 1; j < 100; j++) {
			if (setLine(this, key, j) == false) {
				break;
			}
		}
	}
	
	private JLabel subtitle(String s) {
		JLabel lb = new JLabel(s);
		lb.setFont(new Font("Noto Sans", Font.ITALIC, 12));
		lb.setName("label");
		return (lb);
	}

	private boolean setLine(JPanel panel, String key, int j) {
		String line = key + "." + String.format("%02d", j);
		if (setRadioButton(panel, line) == true) {
			return (true);
		} else if (setCheckBox(panel, line) == true) {
			return (true);
		} else if (setList(panel, line) == true) {
			return (true);
		} else if (setMList(panel, line) == true) {
			return (true);
		} else if (setTextField(panel, line) == true) {
			return (true);
		} else if (setTextArea(panel, line) == true) {
			return (true);
		} else if (setInteger(panel, line) == true) {
			return (true);
		} else if (setEdit(panel, line) == true) {
			return (true);
		}
		return (false);
	}

	private boolean setRadioButton(JPanel panel, String s) {
		String key=s + ".radio";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		ButtonGroup group = new ButtonGroup();
		if (t.contains(TILDA)) {
			String x[] = t.split(TILDA);
			String y[] = x[1].split(";");
			itemList.add(new JLabel(x[0].trim()));
			panel.add(new JLabel(x[0].trim()));
			String z = "split " + (y.length + 1);
			for (String y1 : y) {
				String txt=HtmlUtil.textToHTML(y1.trim());
				JRadioButton rb = new JRadioButton("<html><p>"+txt+"</p></html>");
				rb.setName(key);
				group.add(rb);
				itemList.add(rb);
				panel.add(rb, z);
				z = "";
			}
			panel.add(new JLabel(" "), "wrap");
		} else {
			JLabel lbTitle = new JLabel(t.trim());
			itemList.add(lbTitle);
			JPanel p = new JPanel(new MigLayout());
			p.add(lbTitle, "span, wrap");
			p.setBorder(BorderFactory.createEmptyBorder());
			for (int i = 1; i < 100; i++) {
				key=s + ".radio" + "." + String.format("%02d", i);
				String str = assistant.getMsg(s + ".radio" + "." + String.format("%02d", i));
				if (str == null) {
					break;
				}
				String title = str;
				String tool = "";
				if (str.contains(";")) {
					String x[] = str.split(";");
					title = x[0];
					tool = x[1];
				}
				JRadioButton rb = new JRadioButton();
				rb.setText("<html><p>"+getLabelText(title.trim())+"</p></html>");
				rb.setName(key);
				rb.setVerticalTextPosition(SwingConstants.TOP);
				if (!tool.isEmpty()) {
					rb.setToolTipText(tool.trim());
				}
				group.add(rb);
				itemList.add(rb);
				p.add(rb,"span, grow, wrap");
			}
			JScrollPane scroll = new JScrollPane(p);
			scroll.setBorder(BorderFactory.createEmptyBorder());
			panel.add(scroll, "span,wrap");
		}
		return (true);
	}

	private boolean setCheckBox(JPanel panel, String s) {
		String key=s + ".check";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		ButtonGroup group = new ButtonGroup();
		if (t.contains(TILDA)) {
			String x[] = t.split(TILDA);
			String y[] = x[1].split(";");
			JLabel lbTitle = new JLabel(x[0].trim());
			itemList.add(lbTitle);
			panel.add(lbTitle, "split " + (y.length + 1));
			for (String y1 : y) {
				JCheckBox rb = new JCheckBox(y1.trim());
				rb.setName(key);
				group.add(rb);
				panel.add(rb);
			}
		} else {
			JLabel lbTitle = new JLabel(t.trim());
			itemList.add(lbTitle);
			panel.add(lbTitle, "span,wrap");
			for (int i = 1; i < 10; i++) {
				key=s + ".radio" + "." + String.format("%02d", i);
				String str = assistant.getMsg(key);
				if (str == null) {
					break;
				}
				String x[] = str.split(";");
				JCheckBox rb = new JCheckBox(x[0].trim());
				rb.setName(key);
				rb.setToolTipText(x[1].trim());
				group.add(rb);
				itemList.add(rb);
				panel.add(rb, "wrap");
			}
		}
		panel.add(new JLabel(" "), "wrap");
		return (true);
	}

	@SuppressWarnings("unchecked")
	private boolean setList(JPanel panel, String s) {
		String key= s + ".list";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		if (t.contains(TILDA)) {
			String[] x = t.split(TILDA);
			JLabel lbTitle = new JLabel(x[0].trim());
			itemList.add(lbTitle);
			panel.add(lbTitle);
			String[] y = x[1].split(";");
			JList list = new JList(y);
			list.setName(key);
			itemList.add(list);
			JScrollPane scroll = new JScrollPane(list);
			scroll.setViewportView(list);
			panel.add(scroll, "wrap");
		} else {
			JLabel lbTitle = new JLabel(t.trim());
			itemList.add(lbTitle);
			panel.add(lbTitle, "span,wrap");
			List<String> ls = new ArrayList<>();
			for (int i = 1; i < 10; i++) {
				String str = assistant.getMsg(key);
				if (str == null) {
					break;
				}
				ls.add(str.trim());
			}
			JList list = new JList(ls.toArray());
			list.setName(key);
			itemList.add(list);
			panel.add(new JScrollPane(list), "span,wrap");
		}
		return (true);
	}

	@SuppressWarnings("unchecked")
	private boolean setMList(JPanel panel, String tag) {
		String key=tag + ".mlist";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		if (t.contains(TILDA)) {
			String x[] = t.split(TILDA);
			JLabel lbTitle = new JLabel(x[0].trim());
			itemList.add(lbTitle);
			panel.add(lbTitle);
			String y[] = x[1].split(";");
			JList list = new JList(y);
			list.setName(key);
			list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			itemList.add(list);
			panel.add(new JScrollPane(list), "wrap");
		} else {
			JLabel lbTitle = new JLabel(t.trim());
			itemList.add(lbTitle);
			panel.add(lbTitle, "span,wrap");
			List<String> ls = new ArrayList<>();
			for (int i = 1; i < 10; i++) {
				String str = assistant.getMsg(tag + ".mlist" + "." + String.format("%02d", i));
				if (str == null) {
					break;
				}
				ls.add(str.trim());
			}
			JList list = new JList(ls.toArray());
			list.setName(key);
			list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			itemList.add(list);
			panel.add(new JScrollPane(list), "wrap");
		}
		return (true);
	}

	private boolean setTextField(JPanel panel, String s) {
		String key=s + ".text";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		JLabel tfTitle = new JLabel(t.trim());
		JTextField tf = new JTextField();
		tf.setName(key);
		tf.setColumns(32);
		tf.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				int len = tf.getText().length();
				if (len >= 64) {
					evt.consume();
				}
			}
		});
		if (t.contains(";")) {
			String[] x = t.split(";");
			tfTitle = new JLabel(x[0].trim());
			tf.setToolTipText(x[1].trim());
		}
		panel.add(tfTitle);
		itemList.add(tfTitle);
		panel.add(tf, "wrap");
		itemList.add(tf);
		return (true);
	}

	private boolean setTextArea(JPanel panel, String s) {
		String key=s + ".textarea";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		JLabel tfTitle = new JLabel(t.trim());
		JTextArea tf = new JTextArea();
		tf.setName(key);
		tf.setColumns(64);
		tf.setLineWrap(true);
		tf.setRows(5);
		JTextField tfx=new JTextField();
		tf.setBorder(tfx.getBorder());
		tf.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				int len = tf.getText().length();
				if (len >= 64) {
					evt.consume();
				}
			}
		});
		if (t.contains(";")) {
			String[] x = t.split(";");
			tfTitle = new JLabel(x[0].trim());
			tf.setToolTipText(x[1].trim());
		}
		panel.add(tfTitle,"wrap");
		itemList.add(tfTitle);
		panel.add(tf, "span,wrap");
		itemList.add(tf);
		return (true);
	}

	private boolean setInteger(JPanel panel, String s) {
		String key=s + ".int";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		JTextField tf = new JTextField();
		tf.setName(key);
		JLabel tfTitle = new JLabel(t.trim());
		tf.setColumns(5);
		tf.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				char c = evt.getKeyChar();
				if (c >= '0' && c <= '9') {
				} else {
					evt.consume();
				}
			}
		});
		tf.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				int len = tf.getText().length();
				if (len >= 6) {
					evt.consume();
				}
			}
		});
		if (t.contains(";")) {
			String[] x = t.split(";");
			tfTitle = new JLabel(x[0].trim());
			tf.setToolTipText(x[1].trim());
		}
		panel.add(tfTitle);
		itemList.add(tfTitle);
		panel.add(tf, "wrap");
		itemList.add(tf);
		return (true);
	}

	private boolean setEdit(JPanel panel, String s) {
		String key=s + ".edit";
		String t = assistant.getMsg(key);
		if (t == null) {
			return (false);
		}
		String tool = "";
		String title = t;
		if (t.contains(";")) {
			String[] x = t.split(";");
			title = x[0];
			tool = x[1];
		}
		JLabel lb = new JLabel(title.trim());
		lb.setFont(new Font("Noto Sans", Font.ITALIC, 12));
		if (!tool.isEmpty()) {
			lb.setToolTipText(tool.trim());
		}
		panel.add(lb, "wrap");
		JTextArea ta = new JTextArea(5, 64);
		ta.setName(key);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setBorder(SwingUtil.getBorderEtched());
		if (!tool.isEmpty()) {
			ta.setToolTipText(tool);
		}
		itemList.add(ta);
		JScrollPane scroller = new JScrollPane(ta);
		panel.add(scroller, "wrap");
		return (true);
	}

	public String applySettings() {
		StringBuilder res = new StringBuilder();
		for (Object comp : itemList) {
			res.append(getTextField(comp));
			res.append(getTextArea(comp));
			res.append(getRadioButton(comp));
			res.append(getCheckBox(comp));
			res.append(getList(comp));
		}
		return (res.toString());
	}

	private String getTextField(Object comp) {
		if (comp instanceof JTextField) {
			JTextField tf = (JTextField) comp;
			if (!tf.getText().isEmpty()) {
				return ("["+ tf.getName() + "]" + tf.getText() + "[/"+ tf.getName()+"]\n");
			}
		}
		return ("");
	}

	private String getTextArea(Object comp) {
		if (comp instanceof JTextArea) {
			JTextArea tf = (JTextArea) comp;
			if (!tf.getText().isEmpty()) {
				return ("["+ tf.getName() + "]" + HtmlUtil.htmlToText(tf.getText()) + "[/"+ tf.getName()+"]\n");
			}
		}
		return ("");
	}

	private String getRadioButton(Object comp) {
		if (comp instanceof JRadioButton) {
			JRadioButton rb = ((JRadioButton) comp);
			if (rb.isSelected()) {
				return ("["+rb.getName()+"]"+ HtmlUtil.htmlToText(rb.getText()) + "[/"+rb.getName()+"]\n");
			}
		}
		return ("");
	}

	private String getCheckBox(Object comp) {
		if (comp instanceof JCheckBox) {
			JCheckBox rb = ((JCheckBox) comp);
			if (rb.isSelected()) {
				return("["+rb.getName()+"]"+ HtmlUtil.htmlToText(rb.getText()) + "[/"+rb.getName()+"]\n");
			}
		}
		return ("");
	}

	private String getList(Object comp) {
		if (comp instanceof JList) {
			String r = "";
			JList ls=((JList) comp);
			@SuppressWarnings("unchecked")
			List<String> values = ls.getSelectedValuesList();
			if (values.isEmpty()) {
				return ("");
			}
			for (String v : values) {
				if (!"".equals(r)) {
					r += ", ";
				}
				r += v;
			}
			return ("["+ls.getName()+"]"+ r + "[/"+ls.getName()+"]\n");
		}
		return ("");
	}

	private String getLabelText(String str) {
		String txt=HtmlUtil.textToHTML(str.trim().replace(":", ":\n")).replace(":", ":<i>")+"</i>";
		return(txt);
	}
	
	private static String whichTag(String tag, String value) {
		//App.trace("AssistantPanel.whichTag("+tag+","+value+")");
		return(Assistant.tagStart(tag)+HtmlUtil.htmlToText(value)+Assistant.tagEnd(tag));
	}

	public void setChoice(String text) {
		//App.trace("AssistantPanel.setchoice("+text+")");
		String search=text;
		for (Object comp : itemList) {
			if (comp instanceof JRadioButton) {
				JRadioButton rb = ((JRadioButton) comp);
				if (search.contains(whichTag(rb.getName(),rb.getText())) ||
					whichTag(rb.getName(), rb.getText()).contains(text)) {
					rb.setSelected(true);
					//break;
				}
			}
			else if (comp instanceof JCheckBox) {
				JCheckBox rb = ((JCheckBox) comp);
				if (search.contains(whichTag(rb.getName(),rb.getText()))) {
					rb.setSelected(true);
					//break;
				}
			}
			else if (comp instanceof JList) {
				ListModel list = ((JList)comp).getModel();
				for(int i = 0; i < list.getSize(); i++){
					Object obj = list.getElementAt(i);
					if (search.contains((String)obj)) {
						((JList)comp).setSelectedValue(obj, true);
						break;
					}
				}
			}
			else if (comp instanceof JTextField) {
				JTextField rb=(JTextField)comp;
				String value=findValue(rb.getName(), search);
				rb.setText(value);
			}
			else if (comp instanceof JTextArea) {
				JTextArea rb=(JTextArea)comp;
				String value=findValue(rb.getName(), search);
				rb.setText(value);
			}
		}
	}
	
	private String findValue(String name, String search) {
		if (!search.contains(name)) return ("");
		int dd=search.indexOf(name), df=search.indexOf("/"+name);
		if (dd==-1 || df==-1) return("");
		return(search.substring(dd+name.length()+1,df-1));
	}
}
