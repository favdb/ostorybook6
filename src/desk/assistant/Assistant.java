/*
 Storybook: Scene-based software for novelists and authors.
 Copyright (C) 2008 - 2011 Martin Mustun

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package desk.assistant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import desk.app.App;

import desk.app.Language;
import desk.app.pref.AppPref;
import desk.app.MainFrame;
import tools.html.HtmlUtil;

public class Assistant {

    private static ResourceBundle messageBundle = null;
    private static Properties propMsg;
    private static String propFileName;
    private static int msgType = 0;//0=not init, 1=internal, 2=file

    public Assistant(MainFrame m, String fileName) {
        init(Language.LANGUAGE.valueOf(m.getPref().getLanguage()).getLocale(), fileName);
    }

    public boolean isOK() {
        return (msgType != 0);
    }

    public final void init(Locale locale, String fileName) {
        ResourceBundle.clearCache();
        messageBundle = ResourceBundle.getBundle("desk.assistant.Assistant", Locale.getDefault());
        String nf = App.getPrefDir().getAbsolutePath()
                + File.separator + "Assistant"
                + File.separator + "Assistant.properties";
        if (fileName != null && !"internal".equals(fileName)) {
            initFile(fileName);
        } else {
            initFile(nf);
        }
        initProperties();
        if (propFileName != null) {
            //App.trace("Assistant from file: " + propFileName);
        } else {
            //App.trace("Assistant from bundled file");
        }
    }

    public static void initFile(String nf) {
        File file = new File(nf);
        if (file.exists()) {
            propFileName = nf;
            msgType = 2;
        } else {
            propFileName = null;
            msgType = 1;
        }
    }

    // an external file inside the install sub-directory assistant
    public static final void initProperties() {
        if (propMsg == null) {
            try {
                propMsg = new Properties();
                InputStream input;
                // messages localized
                if (propFileName == null) {
                    return;
                }
                File file = new File(propFileName);
                if (!file.exists()) {
                    propMsg = null;
                    return;
                }
                input = new FileInputStream(propFileName);
                propMsg.load(input);
                input.close();
                msgType = 2;
            } catch (IOException ex) {
                System.out.println("Assistant resources missing");
                propMsg = null;
            }
        }
    }

    public static String getMsgFile(String key) {
        try {
            return (propMsg.getProperty(key));
        } catch (Exception ex) {
            return (null);
        }
    }

    public static String getMsgInt(String key) {
        try {
            return (messageBundle.getString(key));
        } catch (Exception ex) {
            return (null);
        }
    }

    public String getMsg(String key) {
        String r = null;
        if (msgType == 0) {
            msgType = 0;
        }
        switch (msgType) {
            case 1:
                r = getMsgInt(key);
                break;
            case 2:
                r = getMsgFile(key);
                break;
        }
        return (r);
    }

    public static boolean isTag(String str, String tag) {
        return (str.contains("[" + tag + "]"));
    }

    public static String tagStart(String tag) {
        return ("[" + tag + "]");
    }

    public static String tagEnd(String tag) {
        return ("[/" + tag + "]");
    }

    public static String getTagValue(String str, String tag) {
        String tagstart = tagStart(tag), tagend = tagEnd(tag);
        int start = str.indexOf(tagstart);
        if (start < 0) {
            return ("");
        }
        int end = str.indexOf(tagend);
        if (end < 0) {
            return ("");
        }
        String rc = str.substring(start + tagstart.length(), end);
        while (rc.startsWith("\n")) {
            rc = rc.replaceFirst("\n", "");
        }
        return (rc);
    }

    public static String setTag(String str, String tag) {
        //App.trace("Assistant.setTag(...)\nstr=" + str + "\ntag=" + tag);
        return (tagStart(tag) + str + tagEnd(tag) + "\n");
    }

    public static String removeTag(String notes, String tag) {
        //App.trace("Assitant.removeTag(...)\nnotes=" + notes + "\ntag=" + tag);
        if (notes.length() == 0) {
            return ("");
        }
        if (!isTag(notes, tag)) {
            return (notes);
        }
        int dtag = notes.indexOf(tagStart(tag));
        if (dtag == -1) {
            return (notes);
        }
        int ftag = notes.indexOf(tagEnd(tag));
        String z = notes.substring(dtag, ftag + tagEnd(tag).length());
        String s = notes.replace(z, "");
        return (s);
    }

    public static String getSubTag(String str, String step) {
        String x = getTagValue(str, step);
        String y[] = x.split("\\]");
        if (y.length < 2) {
            return ("");
        }
        String z[] = y[1].split("\\[");
        if (z.length < 2) {
            return ("");
        }
        return (z[0].trim());
    }

    public static String getTagName(String str) {
        if (!str.contains("[")) {
            return ("");
        }
        String y[] = str.split("\\]");
        String z[] = y[1].split("\\[");
        return (z[0].trim());
    }

    public static String toHtml(Assistant ass, String instr) {
        if (instr.isEmpty()) {
            return ("");
        }
        StringBuilder b = new StringBuilder("<html>\n");
        String in[] = instr.split("\n");
        int lev = 0;
        if (in.length > 0) {
            for (String e : in) {
                String tag = e.substring(1, e.indexOf("]"));
                if (tag.contains("/")) {
                    b.append("</ul>\n");
                    lev--;
                } else if (!tag.contains(".")) {
                    b.append(HtmlUtil.getIndent(lev));
                    b.append("<b>").append(tag).append("</b>\n<ul>\n");
                    lev++;
                } else {
                    String title = ass.getMsg(tag);
                    int l = title.indexOf("~");
                    if (l == -1) {
                        l = title.indexOf(";");
                    }
                    if (l == -1) {
                        l = title.length();
                    }
                    int dd = e.indexOf("]") + 1, df = e.lastIndexOf("[");
                    String value = title;
                    if (df > 0) {
                        value = e.substring(dd, df);
                    }
                    if (tag.contains("radio") || tag.contains("checkbox")
                            || tag.contains("list") || tag.contains("mlist")) {
                        if (tag.substring(tag.length() - 1).matches("-?\\d+(\\.\\d+)?")) {
                            b.append("<li>").append(title.substring(0, l)).append("</li>\n");
                        } else {
                            b.append("<li>").append(title.substring(0, l)).append("=").append(value).append("</li>\n");
                        }
                    } else {
                        b.append("<li>").append(title.substring(0, l)).append("=").append(value).append("</li>\n");
                    }
                }
            }
        }
        while (lev > 0) {
            b.append("</ul");
            lev--;
        }
        b.append("</html>\n");
        return (b.toString());
    }

}
