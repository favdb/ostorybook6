/*
 * Copyright (C) 2018 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.InputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import resources.MainResources;

/**
 *
 * @author favdb
 */
public class IconUtil {

	public static ImageIcon resizeIcon(ImageIcon icon, int i) {
		Image newimg = icon.getImage().getScaledInstance(i, i,  java.awt.Image.SCALE_SMOOTH ) ;
		return(new ImageIcon(newimg));
	}
	
	public static ImageIcon resizeIcon(ImageIcon icon, int height, int width) {
		Dimension size=new Dimension(height,width);
		return(resizeIcon(icon,size));
	}
	
	public static ImageIcon resizeIcon(ImageIcon icon, Dimension size) {
		ImageIcon rs = icon;
		if ((icon.getIconHeight() != size.height) || (icon.getIconWidth() != size.width)) {
			int iw = icon.getIconHeight();
			int ih = icon.getIconWidth();
			int ph = size.height;
			int pw = size.width;
			double scale;
			if (2.0 * pw / iw < 2.0 * ph / ih) {
				scale = 1.0 * pw / iw;
			} else {
				scale = 1.0 * ph / ih;
			}
			int scaledw = (int) (iw * scale);
			int scaledh = (int) (ih * scale);
			Image imageicon = icon.getImage().getScaledInstance(scaledw, scaledh, Image.SCALE_DEFAULT);
			rs = new ImageIcon(imageicon);
		}
		return (rs);
	}

	public static Icon getIcon(Class<?> c, String key) {
		Class<?> x=MainResources.class;
		String path="icons/"+key.toLowerCase()+".png";
		if (c==null) {
			x = IconUtil.class;
			path="small/unknown.png";
		}
		ImageIcon icon = createImageIcon(x, path);
		if (icon==null) {
			System.err.println("icon "+path+" not found");
			icon = createImageIcon(x, "icons/small/unknown.png");
		}
		return (icon);
	}

	public static Icon getIcon(String path) {
		return(getIcon(MainResources.class,path));
	}
	
	public static Icon getIcon(Class<?> c, String key, Dimension dim) {
		Class<?> x=MainResources.class;
		if (c==null) {
			x = IconUtil.class;
		}
		java.net.URL url = x.getResource("icons/"+key.toLowerCase()+".png");
		ImageIcon icon = new ImageIcon(url);
		if (dim!=null) {
			return resizeIcon(icon,dim);
		}
		return(icon);
	}

	public static Icon getIcon(String path, String key, Dimension dim) {
		String k=path+key.toLowerCase()+".png";
		File f=new File(k);
		if (!f.exists()) {
			String m=IconUtil.class.getCanonicalName().replace(IconUtil.class.getEnclosingClass().getName(), "");
			k=m+"/unknown.png";
			System.err.println("unknown icon=\""+k+"\"");
		}
		ImageIcon icon = new ImageIcon(k);
		if (dim!=null) {
			return resizeIcon(icon,dim);
		}
		return (Icon) (icon.getImage());
	}

	public static ImageIcon createImageIcon(Class<?> c, String path) {
		java.net.URL imgURL = MainResources.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find icon: " + path);
			return null;
		}
	}

	public static ImageIcon createImageIcon(String path) {
		return(createImageIcon(MainResources.class, "icons/"+path.toLowerCase()));
	}
	
	public static Image getIconImage(Class<?> c,String resourceKey) {
		ImageIcon icon = (ImageIcon) IconUtil.getIcon(c,resourceKey);
		return icon.getImage();
	}

	public static InputStream getResourceAsStream(String img) {
		return(getResourceAsStream(MainResources.class,img));
	}

	public static InputStream getResourceAsStream(Class<?> c,String img) {
		return(c.getResourceAsStream("icons/" + img.toLowerCase() + ".png"));
	}

	public static Image getIconImage(String resourceKey) {
		ImageIcon icon = (ImageIcon) IconUtil.getIcon(resourceKey);
		return icon.getImage();
	}

	public static Icon getIconExternal(String filename) {
		File f=new File(filename);
		if (!f.exists()) return(null);
		ImageIcon iconext = new ImageIcon(filename);
		return (iconext);
	}

	public static Icon getIconExternal(String filename, Dimension size) {
		ImageIcon ic=(ImageIcon)getIconExternal(filename);
		if (ic==null) return(null);
		Image img = ic.getImage();
		Image newimg = img.getScaledInstance(size.height, size.width,  java.awt.Image.SCALE_SMOOTH ) ;
		return(new ImageIcon(newimg));
	}

	public static Icon getIconExternal(String filename, int i) {
		ImageIcon ic=(ImageIcon)getIconExternal(filename);
		if (ic==null) return(null);
		Image img = ic.getImage();
		Image newimg = img.getScaledInstance(i, i,  java.awt.Image.SCALE_SMOOTH ) ;
		return(new ImageIcon(newimg));
	}

	public enum StateIcon {
		ERROR("small/error"),
		OK("small/ok"),
		WARNING("small/warning"),
		INFO("small/info");
		final private Icon icon;
		private StateIcon(String iconKey) { this.icon = IconUtil.getIcon(null,iconKey); }
		public Icon getIcon() { return icon; };
	}
	
}
