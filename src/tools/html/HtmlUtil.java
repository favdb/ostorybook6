package tools.html;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JMenu;

import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLWriter;
import db.entity.AbstractEntity;
import db.entity.SbDate;
import db.I18N;
import desk.app.App;
import java.awt.Dimension;
import java.awt.Point;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import org.jsoup.Jsoup;
import resources.MainResources;
import tools.TextUtil;
import tools.ColorUtil;
import tools.StringUtil;

public class HtmlUtil {

	private static final String REPJAVA[][] = {
		{"\"", "\\\""},
		{"\\", "\\\\"},
		{"\b", "\\b"},
		{"\n", "\\n"},
		{"\t", "\\t"},
		{"\f", "\\f"},
		{"\r", "\\r"}
	};
	private static final String REPHTML[][] = {
		{"\"", "&quot;"},
		{"&", "&amp;"},
		{"<", "&lt;"},
		{">", "&gt;"}
	};

	public static String getCleanHtml(String html) {
		StringBuffer buf = new StringBuffer();
		appendCleanHtml(buf, html);
		return buf.toString();
	}

	public static void appendCleanHtml(StringBuffer buf, String html) {
		// remove new lines
		html = html.replace("\n", "");
		html = html.replaceAll("<div>\\s*</div>", "<p></p>");
		Pattern p = Pattern.compile("body>(.*)</body");
		Matcher m = p.matcher(html);
		if (m.find() == true) {
			html = m.group(1);
		}
		html = html.trim();
		// not in any tag at all, so build one
		boolean addPara = false;
		if (!html.startsWith("<")) {
			addPara = true;
			buf.append("<p>");
		}
		buf.append(html);
		if (addPara) {
			buf.append("</p>");
		}
	}

	public static String getContent(HTMLDocument doc) {
		try {
			StringWriter writer = new StringWriter();
			HTMLWriter htmlWriter = new HtmlBodyWriter(writer, doc);
			htmlWriter.write();
			return writer.toString();
		} catch (IOException | BadLocationException e) {
		}
		return "";
	}

	public static String findHref(String html) {
		Pattern pattern = Pattern
				.compile("(((file|http|https)://)|(www\\.))+(([a-zA-Z0-9\\._-]+\\.[a-zA-Z]{2,6})|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(/[a-zA-Z0-9\\&amp;%_\\./-~-]*)?");
		Matcher matcher = pattern.matcher(html);
		while (matcher.find()) {
			return matcher.group();
		}
		return null;
	}

	public static String wrapIntoTable(String html) {
		return wrapIntoTable(html, 200);
	}

	public static String wrapIntoTable(String html, int width) {
		String ret = "<html>"
				+ "<table width='" + width + "'><tr><td>"
				+ html
				+ "<td></tr><table>";
		return ret;
	}

	public static int getChars(String html) {
		//return the text size without any tag and any char code
		if (html == null) {
			return (0);
		}
		String x = htmlToText(html).replaceAll("&#.*;", "?");
		return (x.length());
	}

	public static String removeTags(String html) {
		String text = Jsoup.parse(html).text();
		while (text.contains("  ")) {
			text = text.replace("  ", " ");
		}
		return (text);
	}

	public static String htmlToText(String html) {
		final StringBuilder sb = new StringBuilder();
		HTMLEditorKit.ParserCallback parserCallback = new HTMLEditorKit.ParserCallback() {
			public boolean readyForNewline;

			@Override
			public void handleText(final char[] data, final int pos) {
				String s = new String(data);
				sb.append(s.trim());
				readyForNewline = true;
			}

			@Override
			public void handleStartTag(final HTML.Tag t, final MutableAttributeSet a, final int pos) {
				if (readyForNewline && (t == HTML.Tag.DIV || t == HTML.Tag.BR || t == HTML.Tag.P)) {
					sb.append("\n");
					readyForNewline = false;
				}
			}

			@Override
			public void handleSimpleTag(final HTML.Tag t, final MutableAttributeSet a, final int pos) {
				handleStartTag(t, a, pos);
			}
		};
		try {
			new ParserDelegator().parse(new StringReader(html), parserCallback, false);
		} catch (IOException ex) {
			Logger.getLogger(HtmlUtil.class.getName()).log(Level.SEVERE, null, ex);
		}
		return sb.toString();
	}

	public static String textToHTML(String text) {
		if (text == null) {
			return null;
		}
		int length = text.length();
		boolean prevSlashR = false;
		String out = "";
		for (int i = 0; i < length; i++) {
			char ch = text.charAt(i);
			switch (ch) {
				case '\r':
					if (prevSlashR) {
						out += "<br>";
					}
					prevSlashR = true;
					break;
				case '\n':
					prevSlashR = false;
					out += "<br>";
					break;
				case '"':
					if (prevSlashR) {
						out += "<br>";
						prevSlashR = false;
					}
					out += "&quot;";
					break;
				case '<':
					if (prevSlashR) {
						out += "<br>";
						prevSlashR = false;
					}
					out += "&lt;";
					break;
				case '>':
					if (prevSlashR) {
						out += "<br>";
						prevSlashR = false;
					}
					out += "&gt;";
					break;
				case '&':
					if (prevSlashR) {
						out += "<br>";
						prevSlashR = false;
					}
					out += "&amp;";
					break;
				default:
					if (prevSlashR) {
						out += "<br>";
						prevSlashR = false;
					}
					out += ch;
					break;
			}
		}
		return (out);
	}

	public static String getRow2Cols(StringBuffer text1, String text2) {
		return getRow2Cols(text1.toString(), text2);
	}

	public static String getRow2Cols(StringBuffer text1, StringBuffer text2) {
		return getRow2Cols(text1.toString(), text2.toString());
	}

	public static String getRow2Cols(String text1, StringBuffer text2) {
		return getRow2Cols(text1, text2.toString());
	}

	public static String getRow2Cols(String text1, String text2) {
		return "<tr><td>" + text1 + "</td><td>" + text2 + "</td></tr>";
	}

	public static String getTitle(String title) {
		String buf = "<div style='padding-top:2px;padding-bottom:2px;"
				+ "padding-left:4px;padding-right:4px;"
				+ "margin-bottom:2px;'><b>" + title + "</b></div>";
		return (buf);
	}

	public static String getHTMLName(Color color) {
		return "#" + ColorUtil.getHexName(color);
	}

	public static String getColorSpan(Color clr) {
		String htmlClr = (clr == null ? "white" : ColorUtil.getHexName(clr));
		String buf = "<span style='";
		if (clr != null) {
			buf += "background-color:" + htmlClr + ";";
		}
		buf += "'>";
		buf += "&nbsp; &nbsp; &nbsp; &nbsp; </span>";
		return (buf);
	}

	public static String getColoredTitle(Color clr, String title) {
		String htmlClr = (clr == null ? "white" : ColorUtil.getHexName(clr));
		String buf = "<div style='padding-top:2px;padding-bottom:2px;"
				+ "padding-left:4px;padding-right:4px;margin-bottom:2px;";
		if (clr != null) {
			buf += "background-color:" + htmlClr + ";";
		}
		if (clr != null && ColorUtil.isDark(clr)) {
			buf += "color:white;";
		}
		buf += "'><b>" + title + "</b></div>";
		return (buf);
	}

	public static String getHeadWithCSS() {
		JMenu e = new JMenu();
		return getHeadWithCSS(e.getFont());
	}

	public static String getHeadWithCSS(Font font) {
		String buf = "<head>\n";
		// body
		buf += getCSSDefault(font);
		buf += "</head>\n";
		return (buf);
	}

	public static String getCSS(String nf, Font font) {
		StringBuilder b = new StringBuilder();
		b.append("<style type='text/css'>\n");
		if (nf != null && !nf.isEmpty()) {
			File f = new File(nf);
			if (f.exists()) {
				try {
					InputStream ips = new FileInputStream(f);
					InputStreamReader ipsr = new InputStreamReader(ips);
					BufferedReader br;
					br = new BufferedReader(ipsr);
					String ligne;
					while ((ligne = br.readLine()) != null) {
						b.append(ligne).append("\n");
					}
					br.close();
				} catch (IOException e) {
					System.err.println("ExportHtml.getHtmlHead() error reading CSS");
				}
			}
		} else {
			b.append(HtmlUtil.getCSSDefault(font));
		}
		b.append("</style>\n");
		return (b.toString());
	}

	public static String getCSSDefault() {
		Graphics g = new BufferedImage(300, 300, BufferedImage.TYPE_INT_RGB).getGraphics();
		Font font = new Font(g.getFont().toString(), 0, 12);
		g.dispose();
		return (getCSSDefault(font));
	}

	public static String getCSSDefault(Font font) {
		String buf = "<style type=\"text/css\">\n", tab = "    ";
		// body
		buf += tab + "body {"
				+ "font-family:" + font.getFamily() + ";"
				+ "font-size:" + font.getSize() + ";"
				+ "padding-left:2px;"
				+ "padding-right:2px;"
				+ "}\n";
		//h1
		buf += tab + "h1 {"
				+ "font-size:140%;"
				+ "text-align:center;"
				+ "margin-top:10px;"
				+ "margin-bottom:1px;"
				+ "}\n";
		//h2
		buf += tab + "h2 {"
				+ "font-size:130%;"
				+ "margin-top:10px;"
				+ "margin-bottom:1px;"
				+ "}\n";
		buf += tab + "h3 {"
				+ "font-size:120%;"
				+ "margin-top:6px;"
				+ "margin-bottom:3px;"
				+ "}\n";
		buf += tab + "h4 {"
				+ "font-size:110%;"
				+ "margin-top:6px;"
				+ "margin-bottom:3px;"
				+ "}\n";
		buf += tab + "h5 {"
				+ "font-size:100%;"
				+ "margin-top:6px;"
				+ "margin-bottom:3px;"
				+ "}\n";
		buf += tab + "h6 {"
				+ "font-size:100%;"
				+ "margin-top:6px;"
				+ "margin-bottom:3px;"
				+ "}\n";
		//p
		buf += tab + "p {"
				+ "margin-top:2px;"
				+ "}\n";
		buf += tab + "#navbar {"
				+ "background-color: lightgrey;"
				+ "text-align:right;"
				+ "}\n";
		//ul
		buf += tab + "ul {"
				+ "margin-top:2px;"
				+ "margin-left:15px;"
				+ "margin-bottom:2px;"
				+ "}\n";
		// ordered list
		buf += tab + "ol {"
				+ "margin-top:2px;"
				+ "margin-left:15px;"
				+ "margin-bottom:2px;"
				+ "}\n";
		// table
		buf += tab + "table tr {"
				+ "margin:0px;"
				+ "padding:0px;"
				+ "}\n";
		buf += tab + "td {"
				+ "margin-right:1px;"
				+ "padding:1px;"
				+ "}\n";
		buf += "</style>\n";
		return (buf);
	}

	/**
	 * get CSS for scenario
	 * @param force: get standard CSS and force font-family, size and margin-top and bottom
	 * left and right margin will be 1cm. If not forced, then get the CSS ressource and 
	 * force font-family and  font size to the default font mono
	 * 
	 * @return String containing the CSS code 
	 */
	public static String getScenarioCSS(boolean force) {
		String buf = "";
		Font font=App.getInstance().fontGetMono();
		if (force) {
			buf = "body {"
					+ "font-family: "+font.getFontName()+";"
					+ "font-size: "+font.getSize()+";"
					+ "}"
					+ "p {"
					+ "margin-left: 0.5cm;\n"
					+ "margin-right: 0.5cm;\n"
					+ "margin-top: "+font.getSize()/2+"px;"
					+ "margin-bottom: "+font.getSize()/2+"px;"
					+ "}";
			return(buf);
		}
		File file = new File(MainResources.class.getResource("css/scenario.css").getFile());
		try {
			buf = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException ex) {
		}
		String str = buf.replace("monospace", font.getFontName());
		str=str.replace("font-size: 12px", "font-size: "+Integer.toString(font.getSize())+"px");
		str=str.replace("8px", Integer.toString(font.getSize()/2)+"px");
		return (str);
	}

	public static String getScenarioCSS(String cssFile) {
		String buf = "";
		File file = new File(cssFile);
		try {
			buf = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException ex) {
		}
		return (buf);
	}

	public static String getBold(String str) {
		return "<b>" + str + "</b>";
	}

	public static String getWarning(String str) {
		return "<font color='red'><b>" + str + "</b></font>";
	}

	public static String getHr() {
		return "<hr style='margin:10px' />";
	}

	public static String appendFormatedWarning(String buf, String warning) {
		String str = warning.replaceAll("(\r\n|\r|\n|\n\r)", "<br>");
		if (str.isEmpty()) {
			return (buf);
		}
		return ("<div style='color:red'>" + str + "</div>");
	}

	public static String appendFormatedDescr(String buf, String descr, boolean shorten) {
		String str = descr.replaceAll("(\r\n|\r|\n|\n\r)", "<br>");
		if (str.isEmpty()) {
			return (buf);
		}
		String ret = buf;
		if (shorten) {
			ret += "<div style='width:300px'>" + TextUtil.truncate(str, 300);
		} else {
			ret += "<div>" + str;
		}
		return (ret + "</div>");
	}

	public static String appendFormatedNotes(String buf, String notes, boolean shorten) {
		String str = notes.replaceAll("(\r\n|\r|\n|\n\r)", "<br>");
		if (str.isEmpty()) {
			return (buf);
		}
		String ret = "<hr style='margin:5px'/>";
		if (shorten) {
			ret += "<div style='width:300px'>" + TextUtil.truncate(str, 300);
		} else {
			ret += "<div>" + str;
		}
		return (ret + "</div>");
	}

	public static String appendFormatedMetaInfos(String buf, String metaInfos) {
		String str = metaInfos.replaceAll("(\r\n|\r|\n|\n\r)", "<br>");
		if (str.isEmpty()) {
			return (buf);
		}
		return ("<hr style='margin:5px'/>" + "<div>" + str + "</div>");
	}

	public static String getHtag(String lib) {
		String[] x = lib.split("\\.");
		int n = 2 + x.length;
		if (n > 6) {
			n = 6;
		}
		String t = lib;
		if (t.startsWith(".")) {
			t = lib.substring(1);
		}
		return ("<h" + n + ">" + t + "</h" + n + ">\n");
	}

	public static String getIndent(int lev) {
		String r = "";
		for (int i = 0; i < lev; i++) {
			r += "&nbsp;";
		}
		return (r);
	}

	public static String getTitle(String title, Integer lev) {
		return ("<h" + lev.toString() + ">" + I18N.getMsg(title) + " :</h" + lev.toString() + ">\n");
	}

	public static String getAttribute(String key, Long value, boolean... p) {
		String b = getIndent(4) + I18N.getMsg(key) + "=" + (value != null ? value.toString() : "0");
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, Integer value, boolean... p) {
		String b = getIndent(4) + I18N.getMsg(key) + "=" + (value != null ? value.toString() : "0");
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, SbDate value, boolean... p) {
		String b = "";
		if (value != null) {
			b = ((SbDate) value).format("yyyMMdd HHmmSS");
		}
		b = getIndent(4) + I18N.getMsg(key) + "=\"" + b + "\"";
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, AbstractEntity value, boolean... p) {
		String b = "???";
		try {
			b = getIndent(4) + I18N.getMsg(key) + "=\"" + (value != null ? "\"" + value.name + "\"" : "") + "\"";
			if (p.length > 0) {
				b = "<p>" + b + "</p>";
			} else {
				b = b + "<br>";
			}
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, String value, boolean... p) {
		String b = getIndent(4) + I18N.getMsg(key) + "=\"" + (value != null && !value.isEmpty() ? value : "") + "\"";
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, boolean value, boolean... p) {
		String b = getIndent(4) + I18N.getMsg(key) + "=" + (value == true ? "true" : "false");
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, Dimension value, boolean... p) {
		String b = getIndent(4) + I18N.getMsg(key) + "=";
		b += String.format("%d,%d", value.height, value.width);
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, Point value, boolean... p) {
		String b = getIndent(4) + I18N.getMsg(key) + "=";
		b += String.format("%d,%d", value.x, value.y);
		if (p.length > 0) {
			b = "<p>" + b + "</p>";
		} else {
			b = b + "<br>";
		}
		return (b + "\n");
	}

	public static String escapeHtml(String str) {
		return (StringUtil.escape(REPHTML, str));
	}

	public static String getRow2Cols(String string, int size) {
		return (getRow2Cols(string, Integer.toString(size)));
	}

}
