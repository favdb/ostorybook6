/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import db.I18N;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 *
 * @author favdb
 */
public class FileTool {
	public static void copyFile(File infile, File outfile) throws IOException {
		copyFile(infile, outfile, false);
	}

	public static void copyFile(final File srcFile, final File destFile,
			final boolean preserveFileDate) throws IOException {
		if (srcFile == null) {
			throw new NullPointerException(I18N.getMsg("file.must_not_be_null"));
		}
		if (destFile == null) {
			throw new NullPointerException(I18N.getMsg("file.must_not_be_null"));
		}
		if (!srcFile.exists()) {
			throw new FileNotFoundException(I18N.getMsg("file.not_exists", srcFile));
		}
		if (srcFile.isDirectory()) {
			throw new IOException(I18N.getMsg("file.is_directory", srcFile));
		}
		if (srcFile.getCanonicalPath().equals(destFile.getCanonicalPath())) {
			throw new IOException(I18N.getMsg("file.same", srcFile));
		}
		final File parentFile = destFile.getParentFile();
		if (parentFile != null) {
			if (!parentFile.mkdirs() && !parentFile.isDirectory()) {
				throw new IOException(I18N.getMsg("file.directory_not_created", parentFile));
			}
		}
		if (destFile.exists() && destFile.canWrite() == false) {
			throw new IOException(I18N.getMsg("file.readonly", destFile));
		}
		if (destFile.exists() && destFile.isDirectory()) {
			throw new IOException(I18N.getMsg("file.directory_not_allowed", destFile));
		}
		try {
			FileOutputStream fos;
			try (FileInputStream fis = new FileInputStream(srcFile)) {
				fos = new FileOutputStream(destFile);
				FileChannel input = fis.getChannel();
				FileChannel output = fos.getChannel();
				final long size = input.size(); // T O D O See IO-386
				long pos = 0;
				long count;
				while (pos < size) {
					final long remain = size - pos;
					count = remain > (1024 * 1024) ? (1024 * 1024) : remain;
					final long bytesCopied = output.transferFrom(input, pos, count);
					if (bytesCopied == 0) { // IO-385 - can happen if file is truncated after caching the size
						break; // ensure we don't loop forever
					}
					pos += bytesCopied;
				}
			}
			fos.close();
		} catch (IOException ex) {
		}

		final long srcLen = srcFile.length(); // T O D O See IO-386
		final long dstLen = destFile.length(); // T O D O See IO-386
		if (srcLen != dstLen) {
			throw new IOException(I18N.getMsg("file.failed_to_copy", srcFile));
		}
		if (preserveFileDate) {
			destFile.setLastModified(srcFile.lastModified());
		}
	}

	public static String getOnlyName(String name) {
		String str=name.substring(name.lastIndexOf(File.separator));
		return (str);
	}
	
	public static String getOnlyPath(String name) {
		String str=name.substring(0, name.lastIndexOf(File.separator));
		return (str);
	}
	
}
