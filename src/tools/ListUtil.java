/*
 * Copyright (C) 2017 FaVdB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author FaVdB
 */
public class ListUtil {

	public static String join(List array) {
		return(join(array,","));
	}
	
	public static String join(List array, String separator) {
		if (array == null) {
			return null;
		}
		if (separator == null) {
			separator = ",";
		}
		final StringBuilder buf = new StringBuilder();
		for (int i = 0; i < array.size(); i++) {
			if (i > 0) {
				buf.append(separator);
			}
			if (array.get(i) != null) {
				buf.append(array.get(i));
			}
		}
		return buf.toString();
	}

	@SuppressWarnings("unchecked")
	public static List setUnique(List array) {
		List list = new ArrayList();
		if (array != null) {
			for (Object o : array.toArray()) {
				if (!list.contains(o)) {
					list.add(o);
				}
			}
		}
		return (list);
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	public static List removeNullAndDuplicates(List list) {
		list.removeAll(Collections.singletonList(null));
		Set set = new LinkedHashSet(list);
		return new ArrayList(set);
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] addAll(final T[] array1, final T... array2) {
		if (array1 == null) {
			return clone(array2);
		} else if (array2 == null) {
			return clone(array1);
		}
		final Class<?> type1 = array1.getClass().getComponentType();
		@SuppressWarnings("unchecked") // OK, because array is of type T
		final T[] joinedArray = (T[]) Array.newInstance(type1, array1.length + array2.length);
		System.arraycopy(array1, 0, joinedArray, 0, array1.length);
		try {
			System.arraycopy(array2, 0, joinedArray, array1.length, array2.length);
		} catch (final ArrayStoreException ase) {
			// Check if problem was due to incompatible types
			/*
             * We do this here, rather than before the copy because:
             * - it would be a wasted check most of the time
             * - safer, in case check turns out to be too strict
			 */
			final Class<?> type2 = array2.getClass().getComponentType();
			if (!type1.isAssignableFrom(type2)) {
				throw new IllegalArgumentException("Cannot store "
						+ type2.getName()
						+ " in an array of "
						+ type1.getName(), ase);
			}
			throw ase; // No, so rethrow original
		}
		return joinedArray;
	}

	public static <T> T[] clone(final T[] array) {
		if (array == null) {
			return null;
		}
		return array.clone();
	}

	public static String fromLongs(List<Long> longs) {
		if (longs==null || longs.isEmpty()) {
			return ("");
		}
		List<String> l = new ArrayList<>();
		for (Long e : longs) {
			if (e != null) {
				l.add(e.toString());
			}
		}
		return (String.join(",", l));
	}

	public static List<Long> toLongs(String str) {
		List<Long> l = new ArrayList<>();
		if (!str.isEmpty()) {
			String x[] = str.split(",");
			for (String z : x) {
				l.add(Long.parseLong(z));
			}
		}
		return (l);
	}

}
