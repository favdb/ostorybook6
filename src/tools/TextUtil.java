/*
 * Copyright (C) 2018 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tools;

import db.DB;
import db.entity.AbstractEntity;
import db.entity.SbDate;
import db.I18N;
import java.awt.Dimension;
import java.awt.Point;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class TextUtil {

	public static int countChars(String text) {
		String str=HtmlUtil.removeTags(text);
		return(str.length());
	}
	
	public static int countWords(String text) {
		if (text == null || text.isEmpty()) {
			return (0);
		}
		String[] words = HtmlUtil.removeTags(text).split(" ");
		int count = words.length;
		for (String word : words) {
			if (word.length() == 0) {
				count--;
			}
			if (word.length() == 1) {
				if (".,;/§!%$£=+})°]@^\\_`|[({'#\"~&³*œ".contains(word)) {
					count--;
				}
			}
		}
		return count;
	}

	public static String truncate(String text) {
		return truncate(text, 200);
	}

	public static String truncate(String str, int length) {
		if (str == null) {
			return "";
		}
		if (str.length() > length) {
			String s=str.substring(0, length);
			if (s.endsWith(" ")) s=str.substring(0, length-1);
			return s + "…";
		}
		return str;
	}

	public static String ellipsize(String str, int max) {
		if (str == null) {
			return "";
		}
		if (str.length() > max) {
			String str2 = str.substring(0, max - 3);
			if (!str2.contains(" ")) {
				return (str2 + "...");
			}
			return str2.substring(0, str2.lastIndexOf(" ")) + "...";
		}
		return str;
	}

	public static boolean isNumber(String str) {
		char[] t = str.toCharArray();
		for (char c : t) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	public static String getAttribute(String key, Long value) {
		String b = I18N.getMsg(key) + "=" + (value != null ? value.toString() : "0");
		return (b + "\n");
	}

	public static String getAttribute(String key, Integer value) {
		String b = I18N.getMsg(key) + "=" + (value != null ? value.toString() : "0");
		return (b + "\n");
	}

	public static String getAttribute(String key, SbDate value) {
		String b = "";
		if (value != null) {
			b = ((SbDate) value).format(DB.getInstance().getFormatDateTime());
		}
		b = I18N.getMsg(key) + "=\"" + b + "\"";
		return (b + "\n");
	}

	public static String getAttribute(String key, AbstractEntity value) {
		String b = "???";
		try {
			b = I18N.getMsg(key) + "=\"" + (value != null ? "\"" + value.name + "\"" : "") + "\"";
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
		return (b + "\n");
	}

	public static String getAttribute(String key, String value) {
		String b = I18N.getMsg(key) + "=\"" + (value != null && !value.isEmpty() ? value : "") + "\"";
		return (b + "\n");
	}

	public static String getAttribute(String key, boolean value) {
		String b = I18N.getMsg(key) + "=" + (value == true ? "true" : "false");
		return (b + "\n");
	}

	public static String getAttribute(String key, Dimension value) {
		String b = I18N.getMsg(key) + "=";
		b += String.format("%d,%d", value.height, value.width);
		return (b + "\n");
	}

	public static String getAttribute(String key, Point value) {
		String b = I18N.getMsg(key) + "=";
		b += String.format("%d,%d", value.x, value.y);
		return (b + "\n");
	}

}
