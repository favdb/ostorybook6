/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.util;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Event;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Photo;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author favdb
 */
public class EntityTool {
	
	public static Scene findFirstScene(Book book, Chapter chapter) {
		Scene scene = null;
		for (Scene s : book.scenes) {
			if (s.chapter.id.equals(chapter.id)) {
				if (scene != null && scene.number > s.number) {
					scene = s;
				}
			}
		}
		return (scene);
	}

	public static List<Scene> listToExport(Book book) {
		List<Scene> list = new ArrayList<>();
		book.scenes.forEach((scene)-> {
			if (scene.hasChapter()) {
				list.add(scene);
			}
		});
		list.sort((Scene t1, Scene t2) -> t1.getFullNumber().compareTo(t2.getFullNumber()));
		return (list);
	}

	public static AbstractEntity copy(Book book, AbstractEntity entity) {
		AbstractEntity ent = null;
		int n;
		switch (Book.getTYPE(entity)) {
			case CATEGORY:
				n = book.categories.size();
				book.categories.add(Category.copy(book, (Category) entity, book.last_id++));
				ent = book.categories.get(n);
				break;
			case CHAPTER:
				n = book.chapters.size();
				book.chapters.add(Chapter.copy(book, (Chapter) entity, book.last_id++));
				ent = book.chapters.get(n);
				break;
			case EVENT:
				n = book.events.size();
				book.events.add(Event.copy(book, (Event) entity, book.last_id++));
				ent = book.events.get(n);
				break;
			case GENDER:
				n = book.genders.size();
				book.genders.add(Gender.copy(book, (Gender) entity, book.last_id++));
				ent = book.genders.get(n);
				break;
			case IDEA:
				n = book.ideas.size();
				book.ideas.add(Idea.copy(book, (Idea) entity, book.last_id++));
				ent = book.ideas.get(n);
				break;
			case ITEM:
				n = book.items.size();
				book.items.add(Item.copy(book, (Item) entity, book.last_id++));
				ent = book.items.get(n);
				break;
			case LOCATION:
				n = book.locations.size();
				book.locations.add(Location.copy(book, (Location) entity, book.last_id++));
				ent = book.locations.get(n);
				break;
			case MEMO:
				n = book.memos.size();
				book.memos.add(Memo.copy(book, (Memo) entity, book.last_id++));
				ent = book.memos.get(n);
				break;
			case PART:
				n = book.parts.size();
				book.parts.add(Part.copy(book, (Part) entity, book.last_id++));
				ent = book.parts.get(n);
				break;
			case PERSON:
				n = book.persons.size();
				book.persons.add(Person.copy(book, (Person) entity, book.last_id++));
				ent = book.persons.get(n);
				break;
			case PHOTO:
				n = book.photos.size();
				book.photos.add(Photo.copy(book, (Photo) entity, book.last_id++));
				ent = book.photos.get(n);
				break;
			case PLOT:
				n = book.plots.size();
				book.plots.add(Plot.copy(book, (Plot) entity, book.last_id++));
				ent = book.plots.get(n);
				break;
			case POV:
				n = book.povs.size();
				book.povs.add(Pov.copy(book, (Pov) entity, book.last_id++));
				ent = book.povs.get(n);
				break;
			case RELATION:
				n = book.relations.size();
				book.relations.add(Relation.copy(book, (Relation) entity, book.last_id++));
				ent = book.relations.get(n);
				break;
			case SCENE:
				n = book.scenes.size();
				book.scenes.add(Scene.copy(book, (Scene) entity, book.last_id++));
				ent = book.scenes.get(n);
				break;
			case TAG:
				n = book.tags.size();
				book.tags.add(Tag.copy(book, (Tag) entity, book.last_id++));
				ent = book.tags.get(n);
				break;
		}
		return (ent);
	}

	public static AbstractEntity findName(Book book, Book.TYPE tobj, String name) {
		switch (tobj) {
			case CATEGORY:
				for (Category c : book.categories) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case CHAPTER:
				for (Chapter c : book.chapters) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case EVENT:
				for (Event c : book.events) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case GENDER:
				for (Gender c : book.genders) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case IDEA:
				for (Idea c : book.ideas) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case ITEM:
				for (Item c : book.items) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case LOCATION:
				for (Location c : book.locations) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case MEMO:
				for (Memo c : book.memos) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case PART:
				for (Part c : book.parts) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case PERSON:
				for (Person c : book.persons) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case PHOTO:
				for (Photo c : book.photos) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case PLOT:
				for (Plot c : book.plots) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case POV:
				for (Pov c : book.povs) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case RELATION:
				for (Relation c : book.relations) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case SCENE:
				for (Scene c : book.scenes) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			case TAG:
				for (Tag c : book.tags) {
					if (c.name.equals(name)) {
						return (c);
					}
				}
				break;
			default:
				break;
		}
		return (null);
	}

	public static AbstractEntity findEntity(Book book, Book.TYPE vname, Long id) {
		//App.trace("BookUtil.findEntity(book,vname=" + vname + ", id=" + id + ")");
		switch (vname) {
			case CATEGORY:
				for (Category e : book.categories) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case CHAPTER:
				for (Chapter e : book.chapters) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case EVENT:
				for (Event e : book.events) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case GENDER:
				for (Gender e : book.genders) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case IDEA:
				for (Idea e : book.ideas) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case ITEM:
				for (Item e : book.items) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case LOCATION:
				for (Location e : book.locations) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case MEMO:
				for (Memo e : book.memos) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case PART:
				for (Part e : book.parts) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case PERSON:
				for (Person e : book.persons) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case PHOTO:
				for (Photo e : book.photos) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case PLOT:
				for (Plot e : book.plots) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case POV:
				for (Pov e : book.povs) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case RELATION:
				for (Relation e : book.relations) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case SCENE:
				for (Scene e : book.scenes) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
				break;
			case TAG:
				for (Tag e : book.tags) {
					if (e.id.equals(id)) {
						return (e);
					}
				}
		}
		return (null);
	}

	public static String findEntity(String str) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new InputSource(new StringReader(str)));
		} catch (IOException | ParserConfigurationException | SAXException e) {
			e.printStackTrace(System.err);
			return (null);
		}
		String r = doc.getFirstChild().getNodeName().toLowerCase();
		return (r);
	}

}
