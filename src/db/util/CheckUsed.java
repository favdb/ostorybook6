/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.util;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Event;
import db.entity.Gender;
import db.entity.Idea;
import db.entity.Item;
import db.entity.Location;
import db.entity.Memo;
import db.entity.Part;
import db.entity.Person;
import db.entity.Photo;
import db.entity.Plot;
import db.entity.Pov;
import db.entity.Relation;
import db.entity.Scene;
import db.entity.Tag;
import db.I18N;
import java.util.List;

/**
 *
 * @author favdb
 */
public class CheckUsed {

	public String rc = "";
	private final Book book;

	public CheckUsed(Book book, AbstractEntity entity) {
		this.book=book;
		usedBy(entity);
	}

	public CheckUsed(Book book, List<AbstractEntity> entities) {
		this.book=book;
		entities.forEach((entity) -> {
			usedBy(entity);
		});
	}
	
	public final void usedBy(AbstractEntity e) {
		switch (Book.getTYPE(e)) {
			case CATEGORY:
				displayUsedCategory((Category) e);
				break;
			case CHAPTER:
				displayUsedChapter((Chapter) e);
				break;
			case GENDER:
				displayUsedGender((Gender) e);
				break;
			case EVENT:
				displayUsedEvent((Event) e);
				break;
			case IDEA:
				displayUsedidea((Idea) e);
				break;
			case ITEM:
				displayUsedItem((Item) e);
				break;
			case LOCATION:
				displayUsedLocation((Location) e);
				break;
			case MEMO:
				displayUsedMemo((Memo) e);
				break;
			case PART:
				displayUsedPart((Part) e);
				break;
			case PERSON:
				displayUsedPerson((Person) e);
				break;
			case PHOTO:
				displayUsedPhoto((Photo) e);
				break;
			case PLOT:
				displayUsedPlot((Plot) e);
				break;
			case POV:
				displayUsedPov((Pov) e);
				break;
			case RELATION:
				displayUsedRelation((Relation) e);
				break;
			case SCENE:
				displayUsedScene((Scene) e);
				break;
			case TAG:
				displayUsedTag((Tag) e);
				break;
		}
	}

	private String stringResult(AbstractEntity e) {
		return (I18N.getMsg(e.type.toString()) + " : " + e.name + "<br>\n");
	}

	private void result(AbstractEntity e, String r) {
		if (!r.isEmpty()) {
			rc = "<p>" + I18N.getMsg(e.type.toString()) + " " + I18N.getMsg("entity.used_in") + ":<br>\n" + r + "</p>";
		} else {
			rc = "";
		}
	}
	
	private void displayUsedCategory(Category e) {
		StringBuilder r = new StringBuilder();
		if (e.name.equals(I18N.getMsg("category.char.central"))
				|| e.name.equals(I18N.getMsg("category.char.secondary"))) {
			rc += I18N.getMsg("z.error.delete.notallowed", e.type.toString())+"\n";
			return;
		}
		book.categories.forEach((p)-> {
			if (p.hasSup() && p.sup.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.events.forEach((p)-> {
			if (p.hasCategory() && p.category.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.ideas.forEach((p)-> {
			if (p.hasCategory() && p.category.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.items.forEach((p)-> {
			if (p.hasCategory() && p.category.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.persons.forEach((p)-> {
			if (p.hasCategory() && p.category.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.plots.forEach((p)-> {
			if (p.hasCategory() && p.category.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.tags.forEach((p)-> {
			if (p.hasCategory() && p.category.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedChapter(Chapter e) {
		StringBuilder r = new StringBuilder();
		book.scenes.forEach((p)-> {
			if (p.hasChapter() && p.chapter.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedGender(Gender e) {
		StringBuilder r = new StringBuilder();
		if (e.name.equals(I18N.getMsg("gender.male"))
				|| e.name.equals(I18N.getMsg("gender.female"))) {
			rc += I18N.getMsg("z.error.delete.notallowed", e.type.toString())+"\n";
			return;
		}
		book.persons.forEach((p)-> {
			if (p.gender!=null && p.gender.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedEvent(Event e) {
		//nothing
		String r = "";
		result(e, r);
	}

	private void displayUsedidea(Idea e) {
		//nothing
		String r = "";
		result(e, r);
	}

	private void displayUsedItem(Item e) {
		StringBuilder r = new StringBuilder();
		book.relations.forEach((p)-> {
			if (p.items!=null && p.items.contains(e)) {
				r.append(stringResult(p));
				
			}
		});
		book.scenes.forEach((p)-> {
			if (p.items!=null && p.items.contains(e)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedLocation(Location e) {
		StringBuilder r = new StringBuilder();
		book.locations.forEach((p)-> {
			if (p.sup!=null && p.sup.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.scenes.forEach((p)-> {
			if (p.locations!=null && p.locations.contains(e)) {
				r.append(stringResult(p));
			}
		});
		book.relations.forEach((p)-> {
			if (p.locations!=null && p.locations.contains(e)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedMemo(Memo e) {
		//nothing
		String r = "";
		result(e, r);
	}

	private void displayUsedPart(Part e) {
		StringBuilder r = new StringBuilder();
		book.parts.forEach((p)-> {
			if (p.hasSup() && p.sup.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.chapters.forEach((p)-> {
			if (p.hasPart() && p.part.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedPerson(Person e) {
		StringBuilder r = new StringBuilder();
		book.relations.forEach((p)-> {
			if (p.persons!=null && p.persons.contains(e)) {
				r.append(stringResult(p));
			}
		});
		book.scenes.forEach((p)-> {
			if (p.persons!=null && p.persons.contains(e)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedPhoto(Photo e) {
		StringBuilder r = new StringBuilder();
		book.scenes.forEach((p)-> {
			if (p.photos!=null && p.photos.contains(e)) {
				r.append(stringResult(p));
			}
		});
		book.locations.forEach((p)-> {
			if (p.photos!=null && p.photos.contains(e)) {
				r.append(stringResult(p));
			}
		});
		book.persons.forEach((p)-> {
			if (p.photos!=null && p.photos.contains(e)) {
				r.append(stringResult(p));
			}
		});
		book.items.forEach((p)-> {
			if (p.photos!=null && p.photos.contains(e)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedPlot(Plot e) {
		StringBuilder r = new StringBuilder();
		book.scenes.forEach((p)-> {
			if (p.plots!=null && p.plots.contains(e)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedPov(Pov e) {
		StringBuilder r = new StringBuilder();
		book.scenes.forEach((p)-> {
			if (p.hasPov() && p.pov.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedRelation(Relation e) {
		//nothing
		String r = "";
		result(e, r);
	}

	private void displayUsedScene(Scene e) {
		StringBuilder r = new StringBuilder();
		book.relations.forEach((p)-> {
			if (p.startScene!=null && p.endScene!=null &&
					p.startScene.id.equals(e.id) || p.endScene.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		book.scenes.forEach((p)-> {
			if (p.relativeScene!=null && p.relativeScene.id.equals(e.id)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

	private void displayUsedTag(Tag e) {
		StringBuilder r = new StringBuilder();
		book.relations.forEach((p)-> {
			if (p.tags!=null && p.tags.contains(e)) {
				r.append(stringResult(p));
			}
		});
		book.scenes.forEach((p)-> {
			if (p.tags!=null && p.tags.contains(e)) {
				r.append(stringResult(p));
			}
		});
		result(e, r.toString());
	}

}
