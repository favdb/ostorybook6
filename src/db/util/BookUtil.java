/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.util;

import db.entity.AbstractEntity;
import db.entity.Book;
import db.entity.Category;
import db.entity.Chapter;
import db.entity.Part;
import db.entity.Person;
import db.entity.Pov;
import db.entity.SbDate;
import db.entity.Scene;
import java.util.ArrayList;
import java.util.List;

/**
 * Basic tools for Book
 */
public class BookUtil {
	
	public static List<SbDate> findDistinctDates(Book book) {
		List<SbDate> list = new ArrayList<>();
		book.chapters.forEach((chapter) -> {
			findDistinctDates(book, chapter).forEach((d) -> {
				list.add(d);
			});
		});
		return (list);
	}

	public static List<SbDate> findDistinctDates(Book book, Chapter chapter) {
		List<SbDate> list = new ArrayList<>();
		book.scenes.forEach((s) -> {
			if (s.hasChapter() && s.chapter.id.equals(chapter.id)) {
				if (s.hasDate()) {
					list.add(s.date);
				}
			}
		});
		return (list);
	}

	@SuppressWarnings("unchecked")
	public static SbDate findFirstDate(Book book) {
		SbDate r = null;
		for (Scene s : book.scenes) {
			if (s.date != null) {
				if (r == null || (s.hasDate() && s.date.before(r))) {
					r = s.date;
				}
			}
		}
		for (Person s : book.persons) {
			if (s.birthday != null) {
				if (r == null || (s.hasBirthday() && s.birthday.before(r))) {
					r = s.birthday;
				}
				if (r == null || (s.hasDayofDeath() && s.dayofdeath.before(r))) {
					r = s.dayofdeath;
				}
			}
		}
		return (r);
	}

	public static SbDate findLastDate(Book book) {
		SbDate r = null;
		for (Scene s : book.scenes) {
			if (r == null || (s.hasDate() && s.date.after(r))) {
				r = s.date;
			}
		}
		for (Person s : book.persons) {
			if (s.birthday != null) {
				if (r == null || (s.hasBirthday() && s.birthday.after(r))) {
					r = s.birthday;
				}
				if (r == null || (s.hasDayofDeath() && s.dayofdeath.after(r))) {
					r = s.dayofdeath;
				}
			}
		}
		return (r);
	}

	public static List<AbstractEntity> findAll(Book book) {
		List<AbstractEntity> list = new ArrayList<>();
		list.addAll(book.categories);
		list.addAll(book.chapters);
		list.addAll(book.events);
		list.addAll(book.genders);
		list.addAll(book.ideas);
		list.addAll(book.items);
		list.addAll(book.locations);
		list.addAll(book.memos);
		list.addAll(book.parts);
		list.addAll(book.persons);
		list.addAll(book.plots);
		list.addAll(book.povs);
		list.addAll(book.photos);
		list.addAll(book.relations);
		list.addAll(book.scenes);
		list.addAll(book.tags);
		return (list);
	}

	/**
	 * checkIfNumberExists usable only for new entity creation
	 *
	 * @param book : the book to search
	 * @param objtype : name for the object type
	 * @param elem : element of the data to cha=eck
	 * @param n : the number to check
	 * @return true if the number allready exists
	 *
	 */
	public static boolean checkIfNumberExists(Book book, String objtype, String elem, int n) {
		switch (Book.getTYPE(objtype)) {
			case CATEGORY:
				for (Category c : book.categories) {
					if (elem.equals("sort")) {
						if (c.sort == n) {
							return (true);
						}
					}
				}
				break;
			case CHAPTER:
				for (Chapter c : book.chapters) {
					if (elem.equals("number")) {
						if (c.number == n) {
							return (true);
						}
					}
				}
				break;
			case PART:
				for (Part c : book.parts) {
					if (elem.equals("number")) {
						if (c.number == n) {
							return (true);
						}
					}
				}
				break;
			case POV:
				for (Pov c : book.povs) {
					if (elem.equals("number")) {
						if (c.getSort() == n) {
							return (true);
						}
					}
				}
				break;
			case SCENE:
				for (Scene c : book.scenes) {
					if (elem.equals("number")) {
						if (c.number == n) {
							return (true);
						}
					}
				}
				break;
		}
		return (false);
	}

}
