/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.DB;
import db.XmlDB;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Event extends AbstractEntity implements Comparable<Event> {

	public SbDate date;
	public Category category;

	public static Event copy(Book book,Event old, Long id) {
		Event n=new Event();
		n.name=old.name;
		n.category=old.category;
		n.date=old.date;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"event.date","category",
			"z.description","z.notes"/*,"Assistant"*/};
		return(c);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(date);
		cols.add(category);
		cols.add(desc);
		cols.add(notes);
		return(cols);
	}

	public Event() {
		super(Book.TYPE.EVENT);
	}

	public Event(String title, String notes, SbDate moment, Category category) {
		super(Book.TYPE.EVENT);
		this.name=title;
		this.notes=notes;
		this.date = moment;
		this.category = category;
	}

	Event(Book book,Node node) {
		super(Book.TYPE.EVENT,node);
		xmlFrom(book, node);
	}

	public boolean hasDate() {
		return date != null;
	}

	public boolean hasCategory() {
		return(category!=null);
	}
	
	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Event test = (Event) obj;
		boolean ret = true;
		ret = ret && category.equals(test.category);
		ret = ret && date.equals(test.date);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		if (name != null) {
			hash = hash * 31 + name.hashCode();
		}
		if (getNotes() != null) {
			hash = hash * 31 + getNotes().hashCode();
		}
		if (date != null) {
			hash = hash * 31 + date.hashCode();
		}
		return hash;
	}

	@Override
	public int compareTo(Event ch) {
		if (date == null) {
			return name.compareTo(ch.name);
		} else {
			return date.compareTo(ch.date);
		}
	}

	@Override
	public String toString() {
		if (isTransient()) {
			return "";
		}
		return date.format(DB.getInstance().getFormatDateTime()) + " - " + name;
	}

	@Override
	public String toCsv(String quoteStart, String quoteEnd, String separator) {
		StringBuilder b = new StringBuilder();
		b.append(quoteStart).append(getClean(date)).append(quoteEnd).append(separator);
		b.append(quoteStart).append(getClean(category)).append(quoteEnd).append(separator);
		return (super.toCsv(b.toString(), quoteStart, quoteEnd, separator));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("category",getClean(category)));
		b.append(toHtmlInfo("event.date",date));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"category", getClean(category)));
		b.append(toXmlSetAttribute(0,"date", getClean(date)));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Event> events) {
		StringBuilder b=new StringBuilder();
		events.forEach((c)-> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	@Override
	public void xmlFrom(Book book, Node node) {
		super.xmlFrom(book, node);
		date=new SbDate(xmlGetString(node,"date"));
		category=book.getCategory(xmlGetLong(node,"category"));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"date",date);
		xml.attributeSetValue(n,"category",category);
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Event> events) {
		events.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}
	
	public static List<Event> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(new Event().type.toString());
		List<Event> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Event c=new Event(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}
	
}
