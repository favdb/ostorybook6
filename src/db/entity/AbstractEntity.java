/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.DB;
import db.XmlDB;
import static db.XmlDB.*;
import db.I18N;
import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import tools.IconUtil;
import tools.ListUtil;
import tools.StringUtil;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 * AbstractEntity class
 *
 * @author favdb
 */
public class AbstractEntity {

	public enum DataType {
		TXT, TEXTAREA, INT, DATE, COLOR, ICON, IMAGE, BOOL, NONE
	}

	public static String[] getColumns() {
		String c[] = {
			"z.id", "z.creation", "z.maj", "z.name",
			"z.description", "z.notes", "assistant"};
		return (c);
	}

	public Long id = -1L;
	public Book.TYPE type=Book.TYPE.NONE;
	public String name = "";
	public String desc = "";
	public String notes = "";
	public String assistant = "";
	public SbDate creation;
	public SbDate maj;

	public AbstractEntity() {
		super();
	}
	
	public AbstractEntity(Book.TYPE type) {
		super();
		this.type=type;
		this.name = I18N.getMsg(DB.class, "z.notitle");
		SbDate today = SbDate.getToDay();
		this.creation = today;
		this.maj = today;
		this.desc = "";
		this.notes = "";
		this.assistant = "";
	}

	public AbstractEntity(Book.TYPE type, Node node) {
		super();
		this.type=type;
		id = attributeGetLong(node, "id");
		name = attributeGetString(node, "name");
		creation = (new SbDate(attributeGetString(node, "creation")));
		maj = (new SbDate(attributeGetString(node, "maj")));
		desc = (childContentGet(node, "description"));
		notes = (childContentGet(node, "notes"));
		assistant = (childContentGet(node, "assistant"));
	}

	public AbstractEntity(Book.TYPE type, Long id, String name) {
		this(type);
		this.id=id;
		this.name=name;
	}

	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return (cols);
	}

	public Book.TYPE getType() {
		return (type);
	}

	public void setCreation() {
		creation = SbDate.getToDay();
	}

	public void setCreation(SbDate c) {
		creation = c;
	}

	public String getName(int len) {
		return (name == null ? "" : TextUtil.truncate(name, len));
	}

	public String getFullTitle() {
		StringBuilder buf = new StringBuilder();
		buf.append("<h1>").append(type.toString()).append("</h1>");
		return buf.toString();
	}

	public String getAbbr() {
		return (TextUtil.truncate(name, 10));
	}

	public void setName(String x) {
		name = x;
	}

	public boolean isTransient() {
		return id.intValue() == -1L;
	}

	public String getDescription() {
		return (desc);
	}

	public void setDescription(String x) {
		desc = x;
	}

	public String getNotes() {
		return (notes);
	}

	public boolean hasNotes() {
		return (!HtmlUtil.htmlToText(this.notes).equals(""));
	}

	public void setNotes(String x) {
		notes = x;
	}

	public String getAssistant() {
		return (assistant);
	}

	public void setAssistant(String str) {
		this.assistant = str;
	}

	public String get() {
		return "";
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 31 + (name != null ? name.hashCode() : 0);
		hash = hash * 31 + (type != null ? type.hashCode() : 0);
		hash = hash * 31 + (creation != null ? creation.hashCode() : 0);
		hash = hash * 31 + (maj != null ? maj.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AbstractEntity other = (AbstractEntity) obj;
		if (!Objects.equals(this.type, other.type)) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		return Objects.equals(this.hashCode(), other.hashCode());
	}

	public static boolean equalsObjectNullValue(Object o1, Object o2) {
		if (o1 != null && o2 == null) {
			return false;
		}
		if (o1 == null && o2 != null) {
			return false;
		}
		if (o1 != null) {
			if (o2 == null) {
				return false;
			}
			return o1.equals(o2);
		}
		return true;
	}

	public static boolean equalsStringNullValue(String s1, String s2) {
		if (s1 != null && s2 == null) {
			return false;
		}
		if (s1 == null && s2 != null) {
			return false;
		}
		if (s1 != null) {
			if (s2 == null) {
				return false;
			}
			String st1 = HtmlUtil.htmlToText(s1);
			String st2 = HtmlUtil.htmlToText(s2);
			return st1.equals(st2);
		}
		return true;
	}

	public static boolean equalsIntegerNullValue(Integer n1, Integer n2) {
		if (n1 != null && n2 == null) {
			return false;
		}
		if (n1 == null && n2 != null) {
			return false;
		}
		if (n1 != null) {
			if (n2 == null) {
				return false;
			}
			return n1.equals(n2);
		}
		return true;
	}

	public static boolean equalsLongNullValue(Long l1, Long l2) {
		if (l1 != null && l2 == null) {
			return false;
		}
		if (l1 == null && l2 != null) {
			return false;
		}
		if (l1 != null) {
			if (l2 == null) {
				return false;
			}
			return l1.equals(l2);
		}
		return true;
	}

	public static boolean equalsDateNullValue(Date d1, Date d2) {
		if (d1 != null && d2 == null) {
			return false;
		}
		if (d1 == null && d2 != null) {
			return false;
		}
		if (d1 != null) {
			if (d2 == null) {
				return false;
			}
			return (d1.compareTo(d2) == 0);
		}
		return true;
	}

	public static boolean equalsListNullValue(
			List<? extends AbstractEntity> li1,
			List<? extends AbstractEntity> li2) {
		if (li1 == null && li2 == null) {
			return true;
		}
		if (li1 == null || li2 == null) {
			return false;
		}
		if (li1.isEmpty() && li2.isEmpty()) {
			return true;
		}
		if (li1.size() != li2.size()) {
			return false;
		}
		List<Long> ids1 = new ArrayList<>();
		li1.forEach((e) -> {
			ids1.add(e.id);
		});
		List<Long> ids2 = new ArrayList<>();
		li2.forEach((e) -> {
			ids2.add(e.id);
		});
		ids1.removeAll(ids2);
		return ids1.isEmpty();
	}

	public static int getListHashCode(List<?> list) {
		int hash = 31;
		for (Object o : list) {
			if (o instanceof AbstractEntity) {
				AbstractEntity e = (AbstractEntity) o;
				hash = hash * 31 + (e.id != null ? e.id.hashCode() : 0);
			}
			if (o instanceof Long) {
				Long e = (Long) o;
				hash = hash * 31 + (e != null ? e.hashCode() : 0);
			}
		}
		return hash;
	}

	public static boolean equalsBooleanNullValue(Boolean b1, Boolean b2) {
		if (b1 != null && b2 == null) {
			return false;
		}
		if (b1 == null && b2 != null) {
			return false;
		}
		if (b1 != null) {
			if (b2 == null) {
				return false;
			}
			return b1.equals(b2);
		}
		return true;
	}

	public Icon getIcon() {
		return (IconUtil.getIcon("small/" + this.type.toString()));
	}

	public Icon getIcon(String size) {
		return (IconUtil.getIcon(size + "/" + this.type.toString()));
	}

	public Icon getIcon(int h) {
		ImageIcon ic = (ImageIcon) IconUtil.getIcon("small/" + this.type.toString());
		return (IconUtil.resizeIcon(ic, new Dimension(h, h)));
	}

	public Icon getIcon(int h, int l) {
		ImageIcon ic = (ImageIcon) IconUtil.getIcon("small/" + this.type.toString());
		return (IconUtil.resizeIcon(ic, new Dimension(h, l)));
	}

	public String toCsv(String quoteStart, String quoteEnd, String separator) {
		StringBuilder b = new StringBuilder(toCsvHeader(quoteStart, quoteEnd, separator));
		b.append(toCsvFooter(quoteStart, quoteEnd, separator));
		return (b.toString());
	}

	public String toCsv(String value, String start, String end, String sep) {
		return (String.format("%s%s%s%s", start, value, end, sep));
	}

	public String toCsvHeader(String start, String end, String sep) {
		StringBuilder b = new StringBuilder();
		b.append(toCsv(id.toString(), start, end, sep));
		b.append(toCsv(name, start, end, sep));
		b.append(toCsv(creation.getDateTime(), start, end, sep));
		b.append(toCsv(maj.getDateTime(), start, end, sep));
		b.append("\n");
		return (b.toString());
	}

	public String toCsvFooter(String start, String end, String sep) {
		StringBuilder b = new StringBuilder();
		b.append(toCsv(desc, start, end, sep));
		b.append(toCsv(notes, start, end, sep));
		b.append(toCsv(assistant, start, end, sep));
		b.append("\n");
		return (b.toString());
	}

	public String toCsvInfo(String val, String qs, String qe, String se) {
		return (qs + val + qs + se);
	}

	public String toCsvInfo(boolean val, String qs, String qe, String se) {
		String s = (val?I18N.getMsg("z.true"):I18N.getMsg("z.false"));
		return(toCsvInfo(s,qs,qe,se));
	}

	public String toCsvInfo(Integer val, String qs, String qe, String se) {
		if (val==null) return(toCsvInfo("",qs,qe,se));
		return(toCsvInfo(val.toString(),qs,qe,se));
	}

	public String toCsvInfo(SbDate val, String qs, String qe, String se) {
		if (val==null) return(toCsvInfo("",qs,qe,se));
		return(toCsvInfo(val.toString(),qs,qe,se));
	}

	public String toCsvInfo(AbstractEntity val, String qs, String qe, String se) {
		if (val==null) return(toCsvInfo("",qs,qe,se));
		return(toCsvInfo(val.name,qs,qe,se));
	}

	public String toHtml() {
		return (toCsv("<td>", "</td>", "\n"));
	}
	
	public String toHtmlShort() {
		StringBuilder b = new StringBuilder();
		b.append("<h2>").append(I18N.getMsg(type.toString())).append("</h2>");
		b.append(toHtmlInfo("z.name", name));
		b.append(toHtmlDetailFooter());
		return (b.toString());
	}

	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlDetailHeader());
		b.append(toHtmlDetailFooter());
		return (b.toString());
	}

	public String toHtmlDetail(String str) {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlDetailHeader());
		b.append(str);
		b.append(toHtmlDetailFooter());
		return (b.toString());
	}

	public String toHtmlDetailHeader() {
		StringBuilder b = new StringBuilder();
		b.append("<h2>").append(I18N.getMsg(type.toString())).append("</h2>");
		b.append(toHtmlInfo("z.id", id.toString()));
		b.append(toHtmlInfo("z.name", name));
		b.append(toHtmlInfo("z.date.creation", creation.getDateTime()));
		b.append(toHtmlInfo("z.date.maj", maj.getDateTime()));
		return (b.toString());
	}

	public String toHtmlDetailFooter() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlInfo("z.description", desc));
		b.append(toHtmlInfo("z.notes", notes));
		b.append(toHtmlInfo("assistant", assistant));
		return (b.toString());
	}

	public String toHtmlInfo(String key, boolean value) {
		return ("<b>" + I18N.getMsg(DB.class, key) + "</b> = \"" + (value ? "true" : "false") + "\"<br>\n");
	}

	public String toHtmlInfo(String key, String value) {
		return ("<b>" + I18N.getMsg(DB.class, key) + "</b> = \"" + (value == null ? "" : value) + "\"<br>\n");
	}

	public String toHtmlInfo(String key, List<Long> value) {
		return ("<b>" + I18N.getMsg(DB.class, key) + "</b> = \"" + ListUtil.fromLongs(value) + "\"<br>\n");
	}

	public String toHtmlInfo(String key, AbstractEntity value) {
		return ("<b>" + I18N.getMsg(DB.class, key) + "</b> = \"" + (value == null ? "" : value.name) + "\"<br>\n");
	}

	public String toHtmlInfo(String key, Integer value) {
		if (value == null) {
			return ("");
		}
		return ("<b>" + I18N.getMsg(DB.class, key) + "</b> = " + value + "<br>\n");
	}

	public String toHtmlInfo(String key1, Integer v1, String key2, Integer v2) {
		if (v1 == null) {
			return ("");
		}
		return ("<b>" + I18N.getMsg(DB.class, key1) + "</b> = " + v1 + ", "
				+ "<b>" + I18N.getMsg(DB.class, key2) + "</b> = " + v2 + "<br>\n");
	}

	public String toHtmlInfo(String key, SbDate value) {
		String b = "";
		if (value != null) {
			b = value.format("yyyy-MM-dd HH:mm:SS");
		}
		return ("<b>" + I18N.getMsg(DB.class, key) + "</b> = \"" + b + "\"<br>\n");
	}

	public String toText() {
		return (toCsv("", "", "\t"));
	}

	@Override
	public String toString() {
		return (name);
	}

	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlHeader()).append(toXmlFooter());
		return (b.toString());
	}

	public String toXmlHeader() {
		StringBuilder s = new StringBuilder();
		s.append(toXmlTab(1)).append("<").append(type.toString());
		s.append(toXmlSetAttribute(0, "id", id.toString()));
		s.append(toXmlSetAttribute(0, "creation", creation.toStringInteger()));
		s.append(toXmlSetAttribute(0, "maj", maj.toStringInteger())).append("\n");
		s.append(toXmlSetAttribute(3, "name", name)).append("\n");
		return (s.toString());
	}

	public String toXmlFooter() {
		StringBuilder s = new StringBuilder();
		s.append(" >\n");
		s.append(toXmlEnd());
		return (s.toString());
	}

	public String toXmlEnd() {
		StringBuilder s = new StringBuilder();
		if (!desc.isEmpty()) {
			s.append(toXmlChild(2, "description", desc));
		}
		if (!notes.isEmpty()) {
			s.append(toXmlChild(2, "notes", notes));
		}
		if (!assistant.isEmpty()) {
			s.append(toXmlChild(2, "assistant", assistant));
		}
		s.append("    </").append(type.toString()).append(">\n");
		return (s.toString());
	}

	public static String toXmlChild(int tab, String key, String value) {
		if (value == null || value.trim().isEmpty()) {
			return ("");
		}
		StringBuilder b = new StringBuilder();
		b.append(toXmlTab(tab)).append("<").append(key).append(">");
		b.append(value);
		b.append("</").append(key).append(">\n");
		return (b.toString());
	}

	public static String toXmlChildData(int tab, String key, String value) {
		if (value == null || value.isEmpty()) {
			return ("");
		}
		StringBuilder b = new StringBuilder();
		b.append(toXmlTab(tab)).append("<").append(key.toLowerCase()).append(">");
		if (!value.replace("<p>", "").replace("</p>", "").replace("\n", "").trim().isEmpty()) {
			b.append("<![CDATA[").append(value).append("]]>");
		}
		b.append("</").append(key.toLowerCase()).append(">\n");
		return (b.toString());
	}

	public static String toXmlSetAttribute(int tab, String key, String value) {
		if (value == null || value.isEmpty()) {
			return(toXmlTab(tab) + key.toLowerCase() + "=\"\"");
		}
		return(toXmlTab(tab) + key.toLowerCase() + "=\"" + value.replace("\"", "''") + "\"");
	}

	public static String toXmlSetAttribute(int tab, String key, AbstractEntity value) {
		if (value == null) {
			return(toXmlSetAttribute(tab, key, ""));
		}
		return(toXmlSetAttribute(tab, key, value.id));
	}

	public static String toXmlSetAttribute(int tab, String key, SbDate value) {
		if (value==null) {
			return(toXmlSetAttribute(tab,key, ""));
		}
		return(toXmlSetAttribute(tab,key, value.toString()));
	}

	public static String toXmlSetAttribute(int tab, String key, Long value) {
		if (value==null) {
			return(toXmlSetAttribute(tab,key, ""));
		}
		return(toXmlSetAttribute(tab,key, value.toString()));
	}

	public static String toXmlSetAttribute(int tab, String key, Integer value) {
		if (value==null) {
			return(toXmlSetAttribute(tab,key, ""));
		}
		return(toXmlSetAttribute(tab,key, value.toString()));
	}

	public static String toXmlSetAttribute(int tab, String key, Boolean value) {
		if (value==null) {
			return(toXmlSetAttribute(tab,key, "0"));
		}
		return(toXmlSetAttribute(tab, key, (value?"1":"0")));
	}

	public static String toXmlSetAttribute(int tab, String key, List<?> value) {
		if (value == null) {
			return(toXmlSetAttribute(tab, key, ""));
		}
		List<String> sx = new ArrayList<>();
		value.forEach((l) -> {
			if (l!=null) {
				sx.add(((AbstractEntity)l).id.toString());
			}
		});
		String xvalue = String.join(",", sx);
		return(toXmlSetAttribute(tab,key, xvalue));
	}

	public static String toXmlTab(int i) {
		if (i == 0) {
			return (" ");
		}
		String b = "";
		for (int k = 0; k < i; k++) {
			b += "    ";
		}
		return (b);
	}

	public static boolean xmlGetBoolean(Node node, String key) {
		Element e = (Element) node;
		String s=e.getAttribute(key.toLowerCase()).trim();
		return ("1".equals(s)||"true".equals(s));
	}

	public static String xmlGetString(Node node, String key) {
		Element e = (Element) node;
		return (e.getAttribute(key.toLowerCase()).trim());
	}

	public static Long xmlGetLong(Node node, String key) {
		String x = xmlGetString(node, key).trim();
		if (x.isEmpty() || "null".equals(x) || !StringUtil.isLong(x)) {
			return (-1L);
		}
		Long r = -1L;
		try {
			r = Long.parseLong(x);
		} catch (NumberFormatException ex) {

		}
		return (r);
	}

	public static SbDate xmlGetDate(Node node, String key) {
		String x = xmlGetString(node, key).trim();
		if (x.isEmpty() || "null".equals(x) || !x.isEmpty()) {
			return(null);
		}
		return(new SbDate(x));
	}

	public static List<Long> xmlGetListLong(Node node, String key) {
		String x = xmlGetString(node, key).trim();
		List<Long> list = new ArrayList<>();
		if (x.isEmpty() || "null".equals(x)) {
			return (list);
		}
		String str[] = x.split(",");
		for (String s : str) {
			try {
				list.add(Long.parseLong(s));
			} catch (NumberFormatException ex) {

			}
		}
		return (list);
	}

	public static Integer xmlGetInteger(Node node, String key) {
		String x = xmlGetString(node, key).trim();
		if (x.isEmpty() || "null".equals(x)) {
			return (null);
		}
		return (Integer.parseInt(x));
	}

	public static String xmlGetText(Node node, String key) {
		NodeList t = node.getChildNodes();
		if (t.getLength() > 0) {
			for (int i = 0; i < t.getLength(); i++) {
				if (t.item(i).getNodeName().equals(key.toLowerCase())) {
					return (t.item(i).getTextContent().trim());
				}
			}
		}
		return ("");
	}

	public static List<String> xmlGetList(Node node, String key) {
		NodeList t = node.getChildNodes();
		List<String> list = new ArrayList<>();
		if (t.getLength() > 0) {
			for (int i = 0; i < t.getLength(); i++) {
				if (t.item(i).getNodeName().equals(key.toLowerCase())) {
					list.add(t.item(i).getTextContent().trim());
				}
			}
		}
		return (list);
	}

	public boolean xmlEntityCreate(XmlDB xml) {
		Node n = xml.findNode(getType().toString(), id);
		if (n != null) {
			return (xmlEntityUpdate(xml));
		}
		Element node = xml.document.createElement(getType().toString());
		xmlTo(xml, node);
		xml.setModified();
		return (true);
	}

	public boolean xmlEntityUpdate(XmlDB xml) {
		Node node = xml.findNode(getType().toString(), id);
		if (node == null) {
			System.err.println("AbstractEntity.xmlEntityUpdate unable to update node for "
					+ getType().toString() + ", id=" + id);
			return (false);
		}
		xmlTo(xml, node);
		xml.setModified();
		return (true);
	}

	public boolean xmlEntityDelete(XmlDB xml) {
		Node n = xml.findNode(getType().toString(), id);
		if (n == null) {
			return (false);
		}
		xml.rootNode.removeChild(n);
		xml.setModified();
		return (true);
	}

	public void xmlFrom(XmlDB xml, Book book, Node node) {
		xmlFrom(book, node);
	}

	public void xmlFrom(Book book, Node node) {
		id=(attributeGetLong(node, "id"));
		name=(attributeGetString(node, "name"));
		creation=(new SbDate(attributeGetString(node, "creation")));
		maj=(new SbDate(attributeGetString(node, "maj")));
		desc=(childContentGet(node, "description"));
		notes=(childContentGet(node, "notes"));
		assistant=(childContentGet(node, "assistant"));
	}

	public Node xmlTo(XmlDB xml, Node node) {
		Node n = xml.findNode(type.toString(), id);
		if (n == null) {
			n = xml.document.createElement(type.toString());
			node.appendChild(n);
		}
		xmlTo(xml.document, n);
		return (n);
	}

	public Node xmlFindNode(XmlDB xml) {
		Node n = xml.findNode(type.toString(), name);
		if (n == null) {
			n = xml.document.createElement(type.toString());
			xml.rootNode.appendChild(n);
		}
		return (n);
	}

	public void xmlTo(Document d, Node n) {
		attributeSet(n, "id", id);
		attributeSet(n, "name", name);
		attributeSet(n, "creation", creation.getDateTime());
		attributeSet(n, "maj", maj.getDateTime());
		childContentSet(d, n, "description", desc);
		childContentSet(d, n, "notes", notes);
		childContentSet(d, n, "assistant", assistant);
	}

	public String xmlToString(Node node) {
		StringWriter str = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(str));
		} catch (TransformerException ex) {
			System.out.println("xmlToString Transformer Exception");
		}
		return str.toString();
	}

	public static AbstractEntity xmlFromString(Book book, String str) {
		//App.trace(("xmlFromString(book="+book.getTitle()+", str="+str));
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new InputSource(new StringReader(str)));
		} catch (IOException | ParserConfigurationException | SAXException e) {
			return (null);
		}
		AbstractEntity p = null;
		Node n = doc.getFirstChild();
		switch (Book.getTYPE(n.getNodeName().toLowerCase())) {
			case CATEGORY:
				p = new Category(book, n);
				break;
			case CHAPTER:
				p = new Chapter(book, n);
				break;
			case EVENT:
				p = new Event(book, n);
				break;
			case GENDER:
				p = new Gender(book, n);
				break;
			case IDEA:
				p = new Idea(book, n);
				break;
			case ITEM:
				p = new Item(book, n);
				break;
			case LOCATION:
				p = new Location(book, n);
				break;
			case MEMO:
				p = new Memo(book, n);
				break;
			case PART:
				p = new Part(book, n);
				break;
			case PERSON:
				p = new Person(book, n);
				break;
			case PLOT:
				p = new Plot(book, n);
				break;
			case POV:
				p = new Pov(book, n);
				break;
			case RELATION:
				p = new Relation(book, n);
				break;
			case SCENE:
				p = new Scene(book, n);
				break;
			case TAG:
				p = new Tag(book, n);
				break;
		}
		return p;
	}

	public static String getClean(boolean d) {
		return (d == true ? "1" : "0");
	}

	public static String getClean(Dimension d) {
		if (d == null) {
			return ("");
		}
		return (d.width + "," + d.height);
	}

	public static String getClean(Point d) {
		if (d == null) {
			return ("");
		}
		return (d.x + "," + d.y);
	}

	public static String getClean(SbDate d) {
		return (d != null ? d.toStringInteger() : "");
	}

	public static String getClean(Date date) {
		return (date != null ? date.toString() : "");
	}

	public static String getClean(Integer d) {
		return (d != null ? d.toString() : "");
	}

	public static String getClean(Long d) {
		return (d != null ? d.toString() : "");
	}

	public static String getClean(String str) {
		return (str != null ? HtmlUtil.escapeHtml(str) : "");
	}

	public static String getClean(AbstractEntity entity) {
		return (entity != null ? entity.name : "");
	}

	public static String getCleanId(AbstractEntity entity) {
		return (entity != null ? entity.id.toString() : "");
	}

	public static AbstractEntity findName(List<?> list, String name) {
		for (Object e : list) {
			if (((AbstractEntity) e).name.equals(name)) {
				return ((AbstractEntity) e);
			}
		}
		return (null);
	}

	@SuppressWarnings("unchecked")
	public String getListId(List<?> entities) {
		if (entities.isEmpty()) {
			return ("");
		}
		List<String> l = new ArrayList<>();
		entities.forEach((e) -> {
			if (e != null) {
				l.add(((AbstractEntity) e).id.toString());
			}
		});
		return (String.join(",", l));
	}

}
