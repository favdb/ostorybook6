/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * (c) 2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import java.awt.Dimension;
import java.awt.Point;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class AbsEntityFunction {
	public static String getClean(boolean d) {
		return (d == true ? "1" : "0");
	}

	public static String getClean(Dimension d) {
		return (d == null ? "" : (d.width + "," + d.height));
	}

	public static String getClean(Point d) {
		return (d == null ? "" : (d.x + "," + d.y));
	}

	public static String getClean(SbDate d) {
		return (d != null ? d.toStringInteger() : "");
	}

	public static String getClean(Integer d) {
		return (d != null ? d.toString() : "");
	}

	public static String getClean(Long d) {
		return (d != null ? d.toString() : "");
	}

	public static String getClean(String str) {
		return (str != null ? HtmlUtil.escapeHtml(str) : "");
	}

	public static String getClean(AbstractEntity entity) {
		return (entity != null ? entity.name : "");
	}

	public static String getCleanId(AbstractEntity entity) {
		return (entity != null ? entity.id.toString() : "");
	}

	
}
