/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.IconUtil;
import tools.StringUtil;

/**
 */
public class Tag extends AbstractEntity implements Comparable<Tag> {

	public Category category;
	public String iconFile="";

	public static Tag copy(Book book,Tag old, Long id) {
		Tag n=new Tag();
		n.id=id;
		n.name=old.name;
		n.category=old.category;
		n.iconFile=old.iconFile;
		n.desc=old.desc;
		n.notes=old.notes;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"category","z.icon_file",
			"z.description","z.notes","assistant"};
		return(c);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(category);
		cols.add(iconFile);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Tag() {
		super(Book.TYPE.TAG);
	}

	public Tag(Long id, String name, Category value) {
		super(Book.TYPE.TAG,id, name);
		this.category = value;
	}

	public Tag(Book book, Node node) {
		super(Book.TYPE.TAG);
		xmlFrom(book, node);
	}

	public boolean hasCategory() {
		return(category!=null);
	}

	@Override
	public Icon getIcon() {
		if (iconFile!=null && !iconFile.isEmpty()) {
			return(IconUtil.getIconExternal(iconFile));
		}
		return(super.getIcon());
	}
	
	@Override
	public Icon getIcon(String size) {
		if (iconFile!=null && !iconFile.isEmpty()) {
			return(IconUtil.getIconExternal(iconFile));
		}
		return(super.getIcon(size));
	}
	
	@Override
	public Icon getIcon(int size) {
		if (iconFile!=null && !iconFile.isEmpty()) {
			return(IconUtil.getIconExternal(iconFile,size));
		}
		return(super.getIcon(size));
	}
		
	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(category,qs,qe,se));
		b.append(toCsvInfo(iconFile,qs,qe,se));
		return(super.toCsv(b.toString(), qs, qe, se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("category",category));
		b.append(toHtmlInfo("z.icon_file",iconFile));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"category", getClean(category)));
		b.append(toXmlSetAttribute(0,"iconfile",getClean(iconFile)));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Tag> list) {
		StringBuilder b=new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	public boolean equals(Tag tag) {
		if (!super.equals(tag)) {
			return false;
		}
		boolean ret = true;
		ret = ret && equalsStringNullValue(name, tag.name);
		ret = ret && equalsObjectNullValue(category, tag.category);
		ret = ret && equalsStringNullValue(desc, tag.desc);
		ret = ret && equalsStringNullValue(notes, tag.notes);
		return ret;
	}

	@Override
	public int compareTo(Tag o) {
		return(name.compareTo(o.name));
	}
	
	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book,node);
		category=(book.getCategory(xmlGetLong(node,"category")));
		iconFile=(xmlGetString(node,"iconfile"));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"category",category);
		xml.attributeSetValue(n,"iconfile",iconFile);
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Tag> tags) {
		tags.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}
	
	public static List<Tag> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.TAG.toString());
		List<Tag> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Tag c=new Tag(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

}
