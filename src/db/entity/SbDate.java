/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.entity.SbCalendar.MONTH;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import tools.StringUtil;
import tools.TextUtil;

/**
 *
 * @author FaVdB
 */
public class SbDate {

	public int year=0,
			month=0,
			day=0,
			hour=0,
			minute=0,
			second=0;
	public SbCalendar calendar=new SbCalendar();

	public SbDate() {
	}

	public SbDate(int y, int m, int d) {
		setDate(y, m, d);
	}

	public SbDate(int y, int m, int d, int hh, int mm, int ss) {
		setDate(y, m, d);
		setTime(hh, mm, ss);
	}

	public SbDate(String d) {
		if (!d.isEmpty()) {
			if (TextUtil.isNumber(d)) {
				setDateTimeInteger(d);
			} else setDateTime(d);
		}
	}
	
	public SbDate(Date d) {
		Calendar c = new java.util.GregorianCalendar();
		c.setTime(d);
		setDate(c.get(Calendar.YEAR),c.get(Calendar.MONTH)+1,c.get(Calendar.DAY_OF_MONTH));
		setTime(c.get(Calendar.HOUR),c.get(Calendar.MINUTE),c.get(Calendar.SECOND));
	}

	public SbDate(long time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = format.format(new Date());
		setDateTime(date);
	}
	
	public boolean isZero() {
		if (year+month+day+hour+minute+second==0) {
			return(true);
		}
		if ((year==1899) && (month==12) && (day==31) &&
				hour+minute+second==0) {
			return(true);
		}
		return(false);
	}

	public void setDate(int y, int m, int d) {
		setYear(y);
		setMonth(m);
		setDay(d);
	}

	public void setDate(String str) {
		if (str.isEmpty()) {
			return;
		}
		String x[] = str.split("-");
		if (str.contains("/")) {
			x=str.split("/");
		}
		if (TextUtil.isNumber(x[0])) year = Integer.parseInt(x[0]);
		if (x.length>1 && TextUtil.isNumber(x[1])) month = Integer.parseInt(x[1]);
		if (x.length>2 && TextUtil.isNumber(x[2])) day = Integer.parseInt(x[2]);
	}

	public void setTime(int hh, int mm, int ss) {
		setHour(hh);
		setMinute(mm);
		setSecond(ss);
	}

	public void setTime(String str) {
		if (str.isEmpty()) return;
		String x[] = str.split(":");
		if (x.length<1) return;
		hour = 0;
		minute = 0;
		second = 0;
		if (TextUtil.isNumber(x[0])) hour = Integer.parseInt(x[0]);
		if (x.length > 1) {
			if (TextUtil.isNumber(x[1])) minute = Integer.parseInt(x[1]);
		}
		if (x.length > 2) {
			if (TextUtil.isNumber(x[2])) second = Integer.parseInt(x[2]);
		}
	}

	public void setDateTime(String str) {
		String x[] = str.split(" ");
		if (str.contains("T")) {
			x=str.split("T");
		}
		if (x.length>0) setDate(x[0]);
		if (x.length>1) setTime(x[1]);
	}

	public void setDateTimeInteger(String str) {
		Long d=Long.parseLong(str);
		if (d==0) return;
		Long r=d%100L;second=r.intValue();d=d/100;
		r=d%100L;minute=r.intValue();d=d/100;
		r=d%100L;hour=r.intValue();d=d/100;
		r=d%100L;day=r.intValue();d=d/100;
		r=d%100L;month=r.intValue();r=d/100;year=r.intValue();
	}

	public void setYear(int y) {
		year = y;
	}

	public void setMonth(int m) {
		if (m < 1) {
			month = 1;
		} else {
			month = m;
		}
	}

	public void setDay(int d) {
		if (d < 1) {
			day = 1;
		} else {
			day = d;
		}
	}

	public void setHour(int hh) {
		if (hh < 0) {
			hour = 0;
		} else {
			hour = hh;
		}
	}

	public void setMinute(int mm) {
		if (mm < 0) {
			minute = 0;
		} else {
			minute = mm;
		}
	}

	public void setSecond(int ss) {
		if (ss < 0) {
			second = 0;
		} else {
			second = ss;
		}
	}
	
	public void setCalendar(SbCalendar c) {
		calendar=c;
	}

	public String getDate() {
		return String.format("%04d-%02d-%02d", year, month, day);
	}

	public String getTime() {
		return String.format("%02d:%02d:%02d", hour, minute, second);
	}

	public String getDateTime() {
		if (year+month+day+hour+minute+second==0) {
			return("");
		}
		return getDate() + " " + getTime();
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}
	
	public SbCalendar getCalendar() {
		if (calendar==null) {
			calendar=new SbCalendar();
		}
		return(calendar);
	}
	
	public static String today() {
		Date today = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String tx=dateFormat.format(today);
		return(tx);
	}

	public static SbDate getToDay() {
		return(new SbDate(today()));
	}
	
	public Boolean after(SbDate startDate) {
		String date=getDateTime();
		String debut=startDate.getDateTime();
		return(debut.compareTo(date)>0);
	}

	public Boolean before(SbDate startDate) {
		String date=getDateTime();
		String debut=startDate.getDateTime();
		return(debut.compareTo(date)<0);
	}

	public Boolean equals(SbDate startDate) {
		String date=getDateTime();
		String debut=startDate.getDateTime();
		return(debut.equals(date));
	}

	public int compareTo(SbDate startDate) {
		String date=getDateTime();
		String debut=startDate.getDateTime();
		return(debut.compareTo(date));
	}

	public String age(SbCalendar cal, SbDate todate) {
		Integer d1=toDays(cal);
		Integer d2=todate.toDays(cal);
		Integer r=(d1>d2?d1-d2:d2-d1);
		return(cal.getDuration(r));
	}
	
	public Integer toDays() {
		return(toDays(this.calendar));
	}
	
	public Integer toDays(SbCalendar cal) {
		Integer ret=0;
		if (year>0) {
			ret=(year-1)*cal.daysByYear();
		}
		for (int m=1;m<month;m++) {
			ret+=((MONTH)cal.months.get(m)).days;
		}
		ret+=day;
		return(ret);
	}
	
	public Integer toSeconds() {
		if (calendar==null) {
			calendar=new SbCalendar();
		}
		Integer ret=(((hour*calendar.minutes)+minute)*calendar.seconds)+second;
		return(ret);
	}
	
	/*TODO uniformiser la fonction add en se basant sur la norme ISO8601 concernant la durée
	Exemple:
	P1Y pour un an
	P25M pour 25 mois
	PT42H pour 42 heures
	PT12M pour 12 minutes
	*/

    public static Date addDateDays(final Date date, final int amount) {
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, amount);
        return c.getTime();
    }

	public static String addDays(String date, Integer rel, SbCalendar cal) {
		SbDate d=new SbDate(date);
		d.day+=rel;
		d=SbCalendar.compute(cal, d);
		return(d.getDateTime());
	}
	
	public static Integer daysBetween(SbCalendar cal,SbDate d1, SbDate d2) {
		Integer ret=0;
		if (d1==null || d2==null) {
			return(ret);
		}
		return(ret);
	}
	
	public static Long secondsBetween(SbCalendar cal, SbDate d1, SbDate d2) {
		if (d1==null || d2==null) {
			return(0L);
		}
		Long s1=Integer.toUnsignedLong(d1.toSeconds());
		Long s2=Integer.toUnsignedLong(d2.toSeconds());
		return(s1-s2);
	}
	
	public SbDate getZeroTime() {
		SbDate d=new SbDate(year,month,day,0,0,0);
		return(d);
	}

	public static Date getZeroTimeDate() {
		Calendar cal = Calendar.getInstance();
		clearTime(cal);
		return cal.getTime();
	}

	public static Calendar clearTime(Calendar cal) {
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}

	public static Date addTimeFromDate(Date date, Date time) {
		Calendar calTime = Calendar.getInstance();
		calTime.setTime(time);
		int h = calTime.get(Calendar.HOUR_OF_DAY);
		int m = calTime.get(Calendar.MINUTE);
		int s = calTime.get(Calendar.SECOND);
		Date r = setTime(date, h, m, s);
		return(r);
	}

    public static Date setTime(Date date, int hours, int minutes, int seconds) {
        Date d=set(date, Calendar.HOUR_OF_DAY, hours);
		d=set(d, Calendar.MINUTE, minutes);
        return set(d, Calendar.SECOND, seconds);
    }

    private static Date set(final Date date, final int calendarField, final int amount) {
		if (date == null) {
			throw new IllegalArgumentException(String.format("Date must not be null"));
		}
        // getInstance() returns a new object, so this method is thread safe.
        final Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(calendarField, amount);
        return c.getTime();
    }

	@SuppressWarnings("deprecation")
	public Date toDate() {
		return(new Date(year-1900,month-1,day,hour,minute));
	}

	public static String getNiceDates(List<SbDate> dates) {
		List<String> dateList = new ArrayList<>();
		dates.forEach((date)-> {
			dateList.add(date.getDate());
		});
		return StringUtil.join(dateList, ", ");
	}

	public String getAge(SbCalendar c,SbDate d) {
		String r="";
		Long l=secondsBetween(c,this,d);
		return(c.getDuration(l));
	}
	
	public long toLong() {
		String str=String.format("%d%02d%02d%02d%d02%02d", year,month,day,hour,minute,second);
		long t=Long.parseLong(str);
		return(t);
	}
	
	public String toStringInteger() {
		return(String.format("%d%02d%02d%02d%02d%02d", year,month,day,hour,minute,second));
	}
	
	/* format may be a string containing
	* y for year without fixed minimum size
	* y...y year withe fixed minimum size
	* m for month
	* mm for month with two digits
	* d for day
	* dd day with two digits
	* H for hour
	* HH hour with two digits
	* M for minute
	* MM minutes with two digits
	* S for second
	* SS seconds with two digits
	* exemple :
	* yMdHmS = 190218081225 -> 19-2-18 8:12:25
	* d/MM/y H:m:S = 18/2/19 8:12:25
	* yyyy/MM/dd HH:mm:SS = 2019/02/18 08:12:25 
	*/
	public String format(String fmt) {
		if (fmt==null || fmt.isEmpty()) {
			return("");
		}
		StringBuilder b=new StringBuilder();
		int j,nb;
		for (int i=0;i<fmt.length();i++) {
			nb=1;
			switch(fmt.charAt(i)) {
				case 'y':
					nb=0;
					while(fmt.charAt(i)=='y') {
						nb++;
						i++;
						if (i>=fmt.length()) break;
					}
					b.append(String.format("%0"+nb+"d", year));
					i--;
					break;
				case 'M':
					nb=0;
					while(fmt.charAt(i)=='M') {
						nb++;
						i++;
						if (i>=fmt.length()) break;
					}
					i--;
					switch (nb) {
						case 1:
							b.append(String.format("%d", month));
							break;
						case 2:
							b.append(String.format("%02d", month));
							break;
						case 3:
							b.append(getMonthAbbr(calendar,month));
							break;
						case 4:
							b.append(getMontName(calendar,month));
							break;
						default:
							b.append(String.format("%02d", month));
							break;
					}
					break;

				case 'd':
					nb=0;
					while(fmt.charAt(i)=='d') {
						nb++;
						i++;
						if (i>=fmt.length()) break;
					}
					i--;
					switch (nb) {
						case 1:
							b.append(String.format("%d", day));
							break;
						case 2:
							b.append(String.format("%02d", day));
							break;
						case 3:
							b.append(getDayAbbr(calendar,day));
							break;
						case 4:
							b.append(getDayName(calendar,day));
							break;
						default:
							b.append(String.format("%02d", day));
							break;
					}
					break;
				case 'H':
				case 'h':
					if ((i<fmt.length()-1) && ((fmt.charAt(i+1)=='H')||(fmt.charAt(i+1)=='h'))) {
						i++;
						nb=2;
					}
					b.append(String.format("%0"+nb+"d", hour));
					break;
				case 'm':
					if ((i<fmt.length()-1) && (fmt.charAt(i+1)=='m')) {
						i++;
						nb++;
					}
					b.append(String.format("%0"+nb+"d", minute));
					break;
				case 's':
				case 'S':
					if ((i<fmt.length()-1) && ((fmt.charAt(i+1)=='s')||(fmt.charAt(i+1)=='S'))) {
						i++;
						nb=2;
					}
					b.append(String.format("%0"+nb+"d", second));
					break;
				default:
					b.append(fmt.charAt(i));
					break;
			}
		}
		return(b.toString());
	}

	public SbDate addMinutes(int i) {
		SbDate d=new SbDate(year,month,day,hour,minute,second);
		d.minute+=i;
		if (d.minute>this.calendar.minutes) {
			d.hour++;
			d.minute-=calendar.minutes;
			if (d.hour>calendar.hours) {
				d.day++;
				d.hour-=calendar.hours;
				//TODO compute day
			}
		}
		return(d);
	}

	private String getMonthAbbr(SbCalendar calendar, int month) {
		if (calendar==null || calendar.months.isEmpty()) return("");
		return(calendar.getMonthAbbr(month-1));
	}

	private String getMontName(SbCalendar calendar, int month) {
		if (calendar==null || calendar.months.isEmpty()) return("");
		return(calendar.getMonthName(month-1));
	}

	private String getDayAbbr(SbCalendar calendar, int day) {
		if (calendar==null || calendar.days.isEmpty()) return("");
		return(calendar.getDayAbbr(day));
	}

	private String getDayName(SbCalendar calendar, int day) {
		if (calendar==null || calendar.days.isEmpty()) return("");
		return(calendar.getDayName(day));
	}

}
