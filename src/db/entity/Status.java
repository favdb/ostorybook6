/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.I18N;
import javax.swing.Icon;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class Status {

	public enum STATUS {
		DUMMY, OUTLINE, DRAFT, EDIT1, EDIT2, DONE, INPROGRESS, CANCELED;

		@Override
		public String toString() {
			return name().toLowerCase();
		}
	}

	public Status() {
	}

	public static String getMsg(STATUS state) {
		return(I18N.getMsg("status."+state.toString()));
	}

	public static String getMsg(Integer state) {
		STATUS n[]=STATUS.values();
		return (I18N.getMsg("status."+n[state].toString()));
	}

	public static Icon getIcon(STATUS state) {
		return (IconUtil.getIcon("small/status_"+state.toString()));
	}

	public static Icon getIcon(String state) {
		return (IconUtil.getIcon("small/status_"+state.toLowerCase()));
	}

	public static Icon getIcon(Integer state) {
		STATUS n[]=STATUS.values();
		Icon ic=IconUtil.getIcon("small/unknown");
		try {
			ic=IconUtil.getIcon("small/status_"+n[state].toString());
		} catch (Exception ex) {
			System.err.println("Failed to get icon for status number "+state);
		}
		return (ic);
	}

	public static STATUS getStatus(String s) {
		for (STATUS state:STATUS.values()) {
			if (I18N.getMsg("status."+state.toString()).equals(s)) {
				return(state);
			}
		}
		return(STATUS.DUMMY);
	}
	
	public static String getStatusName(int n) {
		return(STATUS.values()[n].toString());
	}
}
