/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author favdb
 */
public class Part extends AbstractEntity {

	public Integer number=-1;
	public Part sup=null;
    public SbDate objective;
    public SbDate done;
    public Integer size=0;

	public static Part copy(Book book,Part old, Long id) {
		Part n=new Part();
		n.name=old.name;
		n.number=old.number;
		n.sup=old.sup;
		n.objective=old.objective;
		n.done=old.done;
		n.size=old.size;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static Integer getNextNumber(List<Part> parts) {
		Integer ret=0;
		for (Part p:parts) {
			if (p.number>ret) {
				ret=p.number;
			}
		}
		return(ret+1);
	}

	public static List<Part> findSubpart(List<Part> parts, Part part) {
		// find subpart
		List<Part>list=new ArrayList<>();
		parts.forEach((p)-> {
			if (p.hasSup() && p.sup.equals(part)) {
				list.add(p);
			}
		});
		return(list);
	}

	public static Part findNumber(List<Part> parts, int n) {
		for (Part p:parts) {
			if (p.number==n) {
				return(p);
			}
		}
		return(null);
	}

	public static String[] getColumns() {
		String c[]={"Id","Creation","Maj","Name",
			"Number","Sup","Objective","Size","Done",
			"Description","Notes","Assistant"};
		return(c);
	}

	public static List<Part> findRoots(List<Part> parts) {
		List<Part> list=new ArrayList<>();
		parts.forEach((p)-> {
			if (p.sup==null) {
				list.add(p);
			}
		});
		return(list);
	}

	public static List<Part> getParts(List<Part> parts, Part part) {
		List<Part> list=new ArrayList<>();
		parts.forEach((p)-> {
			if (p.sup!=null) {
				list.add(p);
			}
		});
		return(list);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(number);
		if (sup!=null) cols.add(sup);
		else cols.add("");
		if (size!=null && size>0) {
			cols.add(objective);
			cols.add(size);
			cols.add(done);
		} else {
			cols.add("");
			cols.add("");
			cols.add("");
		}
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Part() {
		super(Book.TYPE.PART);
	}

	public Part(Long id, String name) {
		super(Book.TYPE.PART, id, name);
	}

	public Part(Long id, String name, Integer number) {
		super(Book.TYPE.PART,id,name);
		this.number = number;
	}

	public Part(Long id,Integer number, String name, Part superpart,
			String creation, SbDate obj, SbDate done, Integer size) {
		super(Book.TYPE.PART,id,name);
		this.number = number;
		this.sup = superpart;
		this.objective = obj;
		this.done = done;
		this.size = size;
	}
	
	public Part(Book book, Node n) {
		super(Book.TYPE.PART);
		xmlFrom(book, n);
	}

	public String getNumberName() {
		return(this.number + ": " + this.name);
	}

    public boolean hasSup() {
        return(sup != null);
    }

    public boolean isPartOfPart(Part ancestor) {
    	return this.id.equals(ancestor.id);
    }

    public boolean hasObjective() {
        return objective != null;
    }

    public boolean hasSize() {
        return size != 0;
    }

	public boolean hasDone() {
        return(done != null);
    }

	@Override
	public String toString() {
		return(number + " " + name);
	}

	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(number,qs,qe,se));
		b.append(toCsvInfo(sup,qs,qe,se));
		b.append(toCsvInfo(creation,qs,qe,se));
		b.append(toCsvInfo(objective,qs,qe,se));
		b.append(toCsvInfo(done,qs,qe,se));
		b.append(toCsvInfo(size,qs,qe,se));
		return(super.toCsv(b.toString(),qs,qe,se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("z.number",number));
		b.append(toHtmlInfo("part.sup",sup));
		b.append(toHtmlInfo("objective.size",size));
		b.append(toHtmlInfo("objective.date",objective));
		b.append(toHtmlInfo("objective.done",done));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"number", number));
		b.append(toXmlSetAttribute(0,"sup", sup)).append("\n");
		if (size > 0 || hasObjective() || hasDone()) {
			b.append(toXmlSetAttribute(0,"size",size));
			b.append(toXmlSetAttribute(3,"objective",objective));
			b.append(toXmlSetAttribute(0,"done",done));
		}
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Part> parts) {
		StringBuilder b=new StringBuilder();
		parts.forEach((p)-> {
			b.append(p.toXml());
		});
		return(b.toString());
	}
	
	@Override
	public void xmlFrom(Book book, Node node) {
		super.xmlFrom(book, node);
		number=(xmlGetInteger(node,"number"));
		sup=(book.getPart(xmlGetLong(node,"sup")));
		size=(xmlGetInteger(node,"size"));
		objective=xmlGetDate(node,"objective");
		done=xmlGetDate(node,"done");
	}

	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"number",number);
		xml.attributeSetValue(n,"sup",sup);
		xml.attributeSetValue(n,"objective",objective);
		xml.attributeSetValue(n,"size",size);
		xml.attributeSetValue(n,"done",done);
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Part> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Part test = (Part) obj;
		boolean ret = true;
		ret = ret && equalsIntegerNullValue(number, test.number);
		ret = ret && equalsObjectNullValue(sup, test.sup);
		return(ret);
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (number != null ? number.hashCode() : 0);
		return(hash);
	}

	public static List<Part> load(XmlDB xml, Book book) {
		List<Part> list=new ArrayList<>();
		NodeList nodes=xml.rootNode.getElementsByTagName("part");
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Part c=new Part(book, nodes.item(i));
			list.add(c);
		}
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Node n=nodes.item(i);
			Long s=xmlGetLong(n,"sup");
			if (s!=null && s!=-1L) {
				for (Part p:list) {
					if (p.id.equals(s)) {
						list.get(i).sup=p;
						break;
					}
				}
			}
		}
		return(list);
	}

	public static Part findFirstPart(List<Part> parts) {
		Part part = parts.get(0);
		for (Part p : parts) {
			if (part==null) {
				part=p;
			} else if (p.number < part.number) {
				part = p;
			}
		}
		return (part);
	}
	
	public int getChars(Book book) {
		int r=0;
		for (Chapter chapter:book.chapters) {
			if (chapter.id.equals(id)) r+=chapter.getChars(book);
		}
		return(r);
	}

	public int getWords(Book book) {
		int r=0;
		for (Chapter chapter:book.chapters) {
			if (chapter.id.equals(id)) r+=chapter.getWords(book);
		}
		return(r);
	}

}
