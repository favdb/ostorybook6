/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.DB;
import db.XmlDB;
import db.I18N;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class Chapter extends AbstractEntity implements Comparable<Chapter> {

	public Integer number = 0;
	public Part part=null;
	public SbDate objective=null;
	public Integer size=0;
	public SbDate done=null;
	public Integer nbScenes=0;

	public static Chapter copy(Book book, Chapter old, Long id) {
		Chapter n = new Chapter();
		n.name=old.name;
		n.number=old.number;
		n.part=old.part;
		n.objective=old.objective;
		n.done=old.done;
		n.size=old.size;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return (n);
	}

	public static Integer getNextNumber(List<Chapter> chapters) {
		Integer ret = 0;
		for (Chapter c : chapters) {
			if (c.number > ret) {
				ret = c.number;
			}
		}
		return (ret + 1);
	}

	public static Chapter findNumber(List<Chapter> chapters, int n) {
		for (Chapter c : chapters) {
			if (c.number == n) {
				return (c);
			}
		}
		return (null);
	}

	public static String[] getColumns() {
		String c[] = {"z.id", "z.creation", "z.maj", "z.name",
			"z.number", "part", "chapter.nb_scenes", "z.objective.date", "z.objective.size", "z.objective.done",
			"z.description", "z.notes", "assistant"};
		return (c);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(number);
		cols.add(part);
		cols.add(nbScenes);
		if (size > 0) {
			cols.add(objective);
			cols.add(size);
			cols.add(done);
		} else {
			cols.add("");
			cols.add("");
			cols.add("");
		}
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return (cols);
	}

	public Chapter() {
		super(Book.TYPE.CHAPTER);
	}

	public Chapter(Part part, Long id, String name, Integer n) {
		super(Book.TYPE.CHAPTER, id, name);
		this.part=part;
		this.size = 0;
		this.number = n;
	}

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public Chapter(Book book, Node node) {
		super(Book.TYPE.CHAPTER);
		xmlFrom(book, node);
	}
	
	public boolean hasPart() {
		return(part != null);
	}

	public String getNumberRoman() {
		return (StringUtil.intToRoman(number));
	}

	public boolean hasObjective() {
		return (objective != null);
	}

	public boolean hasDone() {
		return (done != null);
	}

	@Override
	public String toString() {
		if (number == null) {
			return ("#?: " + name + (hasNotes() ? "*" : ""));
		}
		return (number + ": " + name + (hasNotes() ? "*" : ""));
	}

	@Override
	public String toCsv(String qs, String qe, String se) {
		StringBuilder b = new StringBuilder();
		if (!hasPart()) {
			b.append(qs).append(I18N.getMsg(DB.class, "z.empty")).append(qe).append(se);
		} else {
			b.append(qs).append(part.toString()).append(qe).append(se);
		}
		b.append(qs).append(number.toString()).append(qe).append(se);
		b.append(qs).append(getClean(size)).append(qe).append(se);
		b.append(qs).append(getClean(objective)).append(qe).append(se);
		b.append(qs).append(getClean(done)).append(qe).append(se);
		return (super.toCsv(b.toString(), qs, qe, se));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlInfo("z.number", number.toString()));
		b.append(toHtmlInfo("part", part));
		b.append(toHtmlInfo("objective.size", size));
		b.append(toHtmlInfo("objective.date", objective));
		b.append(toHtmlInfo("objective.done", done));
		return (super.toHtmlDetail(b.toString()));
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3, "number", getClean(number)));
		b.append(toXmlSetAttribute(0, "part", part)).append("\n");
		if ((size!=null && size > 0) || hasObjective() || hasDone()) {
			b.append(toXmlSetAttribute(3, "size", size));
			b.append(toXmlSetAttribute(0, "objective", objective));
			b.append(toXmlSetAttribute(0, "done", done));
		}
		b.append(toXmlFooter());
		return (b.toString());
	}

	public static String toXml(List<Chapter> list) {
		StringBuilder b = new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return (b.toString());
	}

	@Override
	public int compareTo(Chapter ch) {
		return number.compareTo(ch.number);
	}

	@Override
	public void xmlFrom(Book book, Node node) {
		super.xmlFrom(book, node);
		number=xmlGetInteger(node, "number");
		part=book.getPart(xmlGetLong(node, "part"));
		size=xmlGetInteger(node, "size");
		if (xmlGetString(node, "obective").isEmpty()) {
			objective=null;
		} else {
			objective=new SbDate(xmlGetString(node, "objective"));
		}
		if (xmlGetString(node, "done").isEmpty()) {
			done=null;
		} else {
			done=new SbDate(xmlGetString(node, "done"));
		}
	}

	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = super.xmlTo(xml, node);
		xml.attributeSetValue(n, "number", number);
		xml.attributeSetValue(n, "part", part);
		xml.attributeSetValue(n, "size", size);
		xml.attributeSetValue(n, "objective", objective);
		xml.attributeSetValue(n, "done", done);
		return (n);
	}

	static void xmlTo(XmlDB xml, List<Chapter> entities) {
		entities.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static Chapter find(List<Chapter> list, String x) {
		if (!x.isEmpty()) {
			if (StringUtil.isLong(x)) {
				find(list, Long.parseLong(x));
			}
		}
		return (null);
	}

	public static Chapter find(List<Chapter> list, Long id) {
		for (Chapter elem : list) {
			if (elem.id.equals(id)) {
				return (elem);
			}
		}
		return (null);
	}

	public static List<Chapter> load(XmlDB xml, Book book) {
		NodeList nodes = xml.rootNode.getElementsByTagName(new Chapter().type.toString());
		List<Chapter> list = new ArrayList<>();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getParentNode() != xml.rootNode) {
				continue;
			}
			Chapter c = new Chapter(book, nodes.item(i));
			list.add(c);
		}
		if (list.isEmpty()) {
			//App.trace("no Chapter to load");
		}
		return (list);
	}

	public static List<Chapter> find(List<Chapter> chapters, Part part) {
		List<Chapter> list = new ArrayList<>();
		chapters.forEach((c) -> {
			if (c.hasPart() && c.part.equals(part)) {
				list.add(c);
			}
		});
		return (list);
	}

	public static List<Chapter> orderByNumber(List<Chapter> chapters) {
		List<Chapter> list = new ArrayList<>();
		chapters.forEach((c) -> {
			list.add(c);
		});
		list.sort((Chapter m1, Chapter m2) -> m1.number - m2.number);
		return (list);
	}

	public static List<Chapter> orderByNumber(List<Chapter> chapters, Part part) {
		List<Chapter> list = new ArrayList<>();
		chapters.forEach((c) -> {
			if (c.hasPart() && c.part.equals(part)) {
				list.add(c);
			}
		});
		list.sort((Chapter m1, Chapter m2) -> m1.number - m2.number);
		return (list);
	}
	
	public int getChars(Book book) {
		int r=0;
		for (Scene scene:book.listScenes(this)) {
			r+=scene.getChars();
		}
		return(r);
	}

	public int getWords(Book book) {
		int r=0;
		for (Scene scene:book.listScenes(this)) {
			r+=scene.getWords();
		}
		return(r);
	}

}
