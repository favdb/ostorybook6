/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.DB;
import static db.entity.AbstractEntity.getClean;
import db.XmlDB;
import static db.XmlDB.*;
import db.I18N;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import tools.StringUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class Book {

	public enum TYPE {
		BOOK,
		CATEGORY, CATEGORY_EVENT, CATEGORY_IDEA, CATEGORY_ITEM,
		CATEGORY_PERSON, CATEGORY_PLOT, CATEGORY_TAG, 
		CHAPTER, EVENT, GENDER, IDEA, ITEM,
		LOCATION, MEMO, PART, PERSON, PHOTO, PLOT, POV,
		RELATION, SCENE, SCENARIO, TAG,
		CATEGORIES, CHAPTERS, EVENTS, GENDERS, IDEAS, ITEMS,
		LOCATIONS, MEMOS, PARTS, PERSONS, PHOTOS, PLOTS, POVS,
		RELATIONS, SCENES, TAGS,
		ALL, NONE;

		private TYPE() {
		}

		@Override
		public String toString() {
			return this.name().toLowerCase();
		}

		public boolean compare(String str) {
			return this.name().toLowerCase().equals(str.toLowerCase());
		}
	}

	public static TYPE getTYPE(AbstractEntity entity) {
		if (entity == null) {
			return (TYPE.NONE);
		}
		return (entity.type);
	}

	public static TYPE getTYPE(String str) {
		if (str != null && !str.isEmpty()) {
			for (TYPE id : TYPE.values()) {
				if (id.compare(str.toLowerCase())) {
					return (id);
				}
			}
		}
		return (TYPE.NONE);
	}

	public Long last_id = 0L;
	public BookInfo info = new BookInfo();
	public List<Category> categories = new ArrayList<>();
	public List<Chapter> chapters = new ArrayList<>();
	public List<Event> events = new ArrayList<>();
	public List<Gender> genders = new ArrayList<>();
	public List<Idea> ideas = new ArrayList<>();
	public List<Item> items = new ArrayList<>();
	public List<Location> locations = new ArrayList<>();
	public List<Memo> memos = new ArrayList<>();
	public List<Part> parts = new ArrayList<>();
	public List<Person> persons = new ArrayList<>();
	public List<Photo> photos = new ArrayList<>();
	public List<Plot> plots = new ArrayList<>();
	public List<Pov> povs = new ArrayList<>();
	public List<Relation> relations = new ArrayList<>();
	public List<Scene> scenes = new ArrayList<>();
	public List<Tag> tags = new ArrayList<>();

	/**
	 * Creation d'un Book vide
	 */
	public Book() {
		last_id = 0L;
	}

	/**
	 * Création d'un Book vide avec initialisation du titre
	 *
	 * @param title
	 */
	public Book(String title) {
		last_id = 0L;
		info.setTitle(title);
	}

	/**
	 * Création d'un book avec initialisation à partir d'un XML
	 *
	 * @param xml
	 */
	public Book(XmlDB xml) {
		xmlFrom(xml);
	}

	/**
	 * getEntityIndex permet de récupérer l'index d'une entité au seine de la liste des entités de la même classe
	 *
	 * @param entity
	 * @return
	 */
	public Integer getEntityIndex(AbstractEntity entity) {
		if (entity == null) {
			DB.trace("Book.getEntityIndex has null entity argument");
			return (-1);
		}
		List<?> list = getEntities(entity.type);
		for (int i = 0; i < list.size(); i++) {
			if (((AbstractEntity) list.get(i)).id.equals(entity.id)) {
				return (i);
			}
		}
		return (-1);
	}

	public void entityNew(AbstractEntity entity) {
		if (entity == null) {
			return;
		}
		if (entity.id < 0L) {
			entity.id = ++last_id;
		}
		switch (getTYPE(entity)) {
			case CATEGORY:
				categories.add((Category) entity);
				break;
			case CHAPTER:
				chapters.add((Chapter) entity);
				break;
			case EVENT:
				events.add((Event) entity);
				break;
			case GENDER:
				genders.add((Gender) entity);
				break;
			case IDEA:
				ideas.add((Idea) entity);
				break;
			case ITEM:
				items.add((Item) entity);
				break;
			case LOCATION:
				locations.add((Location) entity);
				break;
			case MEMO:
				memos.add((Memo) entity);
				break;
			case PART:
				parts.add((Part) entity);
				break;
			case PERSON:
				persons.add((Person) entity);
				break;
			case PHOTO:
				photos.add((Photo) entity);
				break;
			case PLOT:
				plots.add((Plot) entity);
				break;
			case POV:
				povs.add((Pov) entity);
				break;
			case RELATION:
				relations.add((Relation) entity);
				break;
			case SCENE:
				scenes.add((Scene) entity);
				break;
			case TAG:
				tags.add((Tag) entity);
				break;
		}
	}

	public void entityUpdate(AbstractEntity entity) {
		if (entity == null) {
			return;
		}
		Integer idx = getEntityIndex(entity);
		if (idx < 0) {
			System.err.println("Book.entityUpdate: entity index not found");
			return;
		}
		switch (Book.getTYPE(entity)) {
			case CATEGORY:
				categories.set(idx, (Category) entity);
				break;
			case CHAPTER:
				chapters.set(idx, (Chapter) entity);
				break;
			case EVENT:
				events.set(idx, (Event) entity);
				break;
			case GENDER:
				genders.set(idx, (Gender) entity);
				break;
			case IDEA:
				ideas.set(idx, (Idea) entity);
				break;
			case ITEM:
				items.set(idx, (Item) entity);
				break;
			case LOCATION:
				locations.set(idx, (Location) entity);
				break;
			case MEMO:
				memos.set(idx, (Memo) entity);
				break;
			case PART:
				parts.set(idx, (Part) entity);
				break;
			case PERSON:
				persons.set(idx, (Person) entity);
				break;
			case PHOTO:
				photos.set(idx, (Photo) entity);
				break;
			case PLOT:
				plots.set(idx, (Plot) entity);
				break;
			case POV:
				povs.set(idx, (Pov) entity);
				break;
			case RELATION:
				relations.set(idx, (Relation) entity);
				break;
			case SCENE:
				scenes.set(idx, (Scene) entity);
				break;
			case TAG:
				tags.set(idx, (Tag) entity);
				break;
			default:
				break;
		}
	}

	public void entityDelete(AbstractEntity entity) {
		if (entity == null) {
			return;
		}
		boolean rc = false;
		switch (Book.getTYPE(entity)) {
			case CATEGORY:
				rc = categories.remove((Category) entity);
				break;
			case CHAPTER:
				rc = chapters.remove((Chapter) entity);
				break;
			case EVENT:
				rc = events.remove((Event) entity);
				break;
			case GENDER:
				rc = genders.remove((Gender) entity);
				break;
			case IDEA:
				rc = ideas.remove((Idea) entity);
				break;
			case ITEM:
				rc = items.remove((Item) entity);
				break;
			case LOCATION:
				rc = locations.remove((Location) entity);
				break;
			case MEMO:
				rc = memos.remove((Memo) entity);
				break;
			case PART:
				rc = parts.remove((Part) entity);
				break;
			case PERSON:
				rc = persons.remove((Person) entity);
				break;
			case PHOTO:
				rc = photos.remove((Photo) entity);
				break;
			case PLOT:
				rc = plots.remove((Plot) entity);
				break;
			case POV:
				rc = povs.remove((Pov) entity);
				break;
			case RELATION:
				rc = relations.remove((Relation) entity);
				break;
			case SCENE:
				rc = scenes.remove((Scene) entity);
				break;
			case TAG:
				rc = tags.remove((Tag) entity);
				break;
		}
		DB.trace("Book delete entity " + entity.name + " " + (rc ? "ok" : "not found"));
	}

	public AbstractEntity getEntity(Long id) {
		for (TYPE t : TYPE.values()) {
			AbstractEntity entity = getEntity(t.toString(), id);
			if (entity != null) {
				return (entity);
			}
		}
		return (null);
	}

	public AbstractEntity getEntity(Book.TYPE type, String name) {
		List<?> entities=getEntities(type);
		for (Object entity:entities) {
			if (((AbstractEntity)entity).name.equals(name)) {
				return((AbstractEntity)entity);
			}
		}
		return(null);
	}
	
	public AbstractEntity getEntity(Book.TYPE type, Long id) {
		switch (type) {
			case CATEGORY:
				return (getEntity(categories, id));
			case CHAPTER:
				return (getEntity(chapters, id));
			case EVENT:
				return (getEntity(events, id));
			case GENDER:
				return (getEntity(genders, id));
			case IDEA:
				return (getEntity(ideas, id));
			case ITEM:
				return (getEntity(items, id));
			case LOCATION:
				return (getEntity(locations, id));
			case MEMO:
				return (getEntity(memos, id));
			case PART:
				return (getEntity(parts, id));
			case PERSON:
				return (getEntity(persons, id));
			case PHOTO:
				return (getEntity(photos, id));
			case PLOT:
				return (getEntity(plots, id));
			case POV:
				return (getEntity(povs, id));
			case RELATION:
				return (getEntity(relations, id));
			case SCENE:
				return (getEntity(scenes, id));
			case TAG:
				return (getEntity(tags, id));
		}
		return (null);
	}

	public AbstractEntity getEntity(String objtype, Long id) {
		String t = objtype;
		if (t.startsWith("table.")) {
			t = objtype.replaceAll("table.", "");
		}
		TYPE type = getTYPE(t);
		return (getEntity(type, id));
	}

	private AbstractEntity getEntity(List<?> list, Long nid) {
		if (list == null || list.isEmpty() || nid == null || nid < 0L) {
			return (null);
		}
		for (Object c : list) {
			AbstractEntity a = (AbstractEntity) c;
			if (a.id.equals(nid)) {
				return (a);
			}
		}
		return (null);
	}

	public List<?> getEntities(Book.TYPE type) {
		List<AbstractEntity> list = new ArrayList<>();
		switch (type) {
			case CATEGORY:
				return (categories);
			case CHAPTER:
				return (chapters);
			case EVENT:
				return (events);
			case GENDER:
				return (genders);
			case IDEA:
				return (ideas);
			case ITEM:
				return (items);
			case LOCATION:
				return (locations);
			case MEMO:
				return (memos);
			case PART:
				return (parts);
			case PERSON:
				return (persons);
			case PLOT:
				return (plots);
			case POV:
				return (povs);
			case RELATION:
				return (relations);
			case SCENE:
				return (scenes);
			case TAG:
				return (tags);
			default:
				if (type.toString().startsWith("category_")) {
					String cattype = type.toString().replace("category_", "");
					List<Category> cats = new ArrayList<>();
					categories.forEach((cat) -> {
						if (cat.beacon.equals(cattype)) {
							cats.add(cat);
						}
					});
					return (cats);
				}
		}
		return (list);
	}

	public List<?> getEntities(TYPE type, List<Long> ids) {
		List<AbstractEntity> list = new ArrayList<>();
		switch (type) {
			case CATEGORY:
				return (getCategories(ids));
			case CHAPTER:
				return (getChapters(ids));
			case EVENT:
				return (getEvents(ids));
			case GENDER:
				return (getGenders(ids));
			case IDEA:
				return (getIdeas(ids));
			case ITEM:
				return (getItems(ids));
			case LOCATION:
				return (getLocations(ids));
			case MEMO:
				return (getMemos(ids));
			case PART:
				return (getParts(ids));
			case PERSON:
				return (getPersons(ids));
			case PLOT:
				return (getPlots(ids));
			case POV:
				return (getPovs(ids));
			case RELATION:
				return (getRelations(ids));
			case SCENE:
				return (getScenes(ids));
			case TAG:
				return (getTags(ids));
		}
		return (list);
	}

	public AbstractEntity getEntityNew(Book.TYPE type) {
		AbstractEntity abs = new AbstractEntity();
		switch (type) {
			case CATEGORY:
				abs = new Category();
				break;
			case CHAPTER:
				abs = new Chapter();
				break;
			case EVENT:
				abs = new Event();
				break;
			case GENDER:
				abs = new Gender();
				break;
			case IDEA:
				abs = new Idea();
				break;
			case ITEM:
				abs = new Item();
				break;
			case LOCATION:
				abs = new Location();
				break;
			case MEMO:
				abs = new Memo();
				break;
			case PART:
				abs = new Part();
				break;
			case PERSON:
				abs = new Person();
				break;
			case PHOTO:
				abs = new Photo();
				break;
			case PLOT:
				abs = new Plot();
				break;
			case POV:
				abs = new Pov();
				break;
			case RELATION:
				abs = new Relation();
				break;
			case SCENE:
				abs = new Scene();
				break;
			case TAG:
				abs = new Tag();
				break;
		}
		abs.id=-1L;
		return (abs);
	}

	public Category getCategory(Long nid) {
		return ((Category) getEntity(categories, nid));
	}

	public List<Category> getCategories(List<Long> ids) {
		List<Category> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Category) getEntity(categories, id));
			});
		}
		return (list);
	}

	public Chapter getChapter(Long nid) {
		return ((Chapter) getEntity(chapters, nid));
	}

	public List<Chapter> getChapters(List<Long> ids) {
		List<Chapter> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Chapter) getEntity(chapters, id));
			});
		}
		return (list);
	}

	public Event getEvent(Long nid) {
		return ((Event) getEntity(events, nid));
	}

	public List<Event> getEvents(List<Long> ids) {
		List<Event> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Event) getEntity(events, id));
			});
		}
		return (list);
	}

	public Gender getGender(Long nid) {
		return ((Gender) getEntity(genders, nid));
	}

	public List<Gender> getGenders(List<Long> ids) {
		List<Gender> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Gender) getEntity(genders, id));
			});
		}
		return (list);
	}

	public Idea getIdea(Long nid) {
		return ((Idea) getEntity(ideas, nid));
	}

	public List<Idea> getIdeas(List<Long> ids) {
		List<Idea> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Idea) getEntity(ideas, id));
			});
		}
		return (list);
	}

	public Item getItem(Long nid) {
		return ((Item) getEntity(items, nid));
	}

	public List<Item> getItems(List<Long> ids) {
		List<Item> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Item) getEntity(items, id));
			});
		}
		return (list);
	}

	public Location getLocation(Long nid) {
		return ((Location) getEntity(locations, nid));
	}

	public List<Location> getLocations(List<Long> ids) {
		List<Location> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Location) getEntity(locations, id));
			});
		}
		return (list);
	}

	public Memo getMemo(Long nid) {
		return ((Memo) getEntity(memos, nid));
	}

	public List<Memo> getMemos(List<Long> ids) {
		List<Memo> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Memo) getEntity(memos, id));
			});
		}
		return (list);
	}

	public Part getPart(Long nid) {
		return ((Part) getEntity(parts, nid));
	}

	public List<Part> getParts(List<Long> ids) {
		List<Part> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Part) getEntity(parts, id));
			});
		}
		return (list);
	}

	public Person getPerson(Long nid) {
		return ((Person) getEntity(persons, nid));
	}

	public List<Person> getPersons(List<Long> ids) {
		List<Person> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Person) getEntity(persons, id));
			});
		}
		return (list);
	}

	public Photo getPhoto(Long nid) {
		return ((Photo) getEntity(photos, nid));
	}

	public List<Photo> getPhotos(List<Long> ids) {
		List<Photo> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Photo) getEntity(photos, id));
			});
		}
		return (list);
	}

	public Plot getPlot(Long nid) {
		return ((Plot) getEntity(plots, nid));
	}

	public List<Plot> getPlots(List<Long> ids) {
		List<Plot> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Plot) getEntity(plots, id));
			});
		}
		return (list);
	}

	public Pov getPov(Long nid) {
		if (nid == -1L) {
			return (povs.get(0));
		}
		return ((Pov) getEntity(povs, nid));
	}

	public List<Pov> getPovs(List<Long> ids) {
		List<Pov> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Pov) getEntity(povs, id));
			});
		}
		return (list);
	}

	public Relation getRelation(Long nid) {
		return ((Relation) getEntity(relations, nid));
	}

	public List<Relation> getRelations(List<Long> ids) {
		List<Relation> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Relation) getEntity(relations, id));
			});
		}
		return (list);
	}

	public Scene getScene(Long nid) {
		return ((Scene) getEntity(scenes, nid));
	}

	public List<Scene> getScenes(List<Long> ids) {
		List<Scene> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Scene) getEntity(scenes, id));
			});
		}
		return (list);
	}

	public Tag getTag(Long nid) {
		return ((Tag) getEntity(tags, nid));
	}

	public List<Tag> getTags(List<Long> ids) {
		List<Tag> list = new ArrayList<>();
		if (ids != null && !ids.isEmpty()) {
			ids.forEach((id) -> {
				list.add((Tag) getEntity(tags, id));
			});
		}
		return (list);
	}

	public Category getSecondaryCharacter() {
		return (categories.get(1));
	}

	public List<Scene> listScenes(Chapter chapter) {
		List<Scene> list = new ArrayList<>();
		scenes.forEach((c) -> {
			if (c.hasChapter() && c.chapter.equals(chapter)) {
				list.add(c);
			}
		});
		return (list);
	}

	public Long nextId() {
		last_id++;
		return (last_id);
	}

	public void init(String title) {
		info = new BookInfo();
		info.setTitle(title);
		initCategories();
		initGenders();
		initPovs();
		initParts();
		initChapters(parts.get(0), 1);
		initScenes(chapters.get(0), povs.get(0), 1);
	}

	private void initCategories() {
		int i = 1;
		categories.add(new Category(nextId(), "person", I18N.getMsg("category.char.central"), 1, null));
		categories.add(new Category(nextId(), "person", I18N.getMsg("category.char.secondary"), 2, null));
		//App.trace(categories.size() + " Categories initialized");
	}

	private void initChapters(Part part, int number) {
		int n = chapters.size();
		for (int z = 1; z <= number; z++) {
			chapters.add(new Chapter(part, nextId(),
					I18N.getMsg(DB.class, "chapter") + " " + StringUtil.intToRoman(n + z), n + z));
		}
		//App.trace(number + " Chapter(s) initialized");
	}

	private void initGenders() {
		genders.add(new Gender(nextId(), I18N.getMsg(DB.class, "gender.male"), 1));
		genders.add(new Gender(nextId(), I18N.getMsg(DB.class, "gender.female"), 2));
		//App.trace(genders.size() + " Genders initialized");
	}

	private void initParts() {
		parts.add(new Part(nextId(), I18N.getMsg(DB.class, "part.init")));
		//App.trace(parts.size() + " Part initialized");
	}

	private void initPovs() {
		povs.add(new Pov(nextId(), I18N.getMsg(DB.class, "pov.init")));
		//App.trace(povs.size() + " Pov initialized");
	}

	private void initScenes(Chapter chapter, Pov pov, int number) {
		int n = scenes.size();
		for (int z = 1; z <= number; z++) {
			scenes.add(new Scene(nextId(),
					I18N.getMsg("scene") + " " + (n + z), chapter, pov, n + z));
		}
		//App.trace(scenes.size() + " Scene(s) initialized");
	}

	public boolean isScenario() {
		return (info.scenario);
	}

	public void xmlFrom(XmlDB xml) {
		//App.trace("Book.xmlFrom(" + xml.name + ")");
		last_id = attributeGetLong(xml.rootNode, "last_id", last_id);
		info = BookInfo.load(xml);
		categories = Category.load(xml, this);
		genders = Gender.load(xml, this);
		events = Event.load(xml, this);
		ideas = Idea.load(xml, this);
		items = Item.load(xml, this);
		locations = Location.load(xml, this);
		memos = Memo.load(xml, this);
		persons = Person.load(xml, this);
		tags = Tag.load(xml, this);
		photos = Photo.load(xml, this);
		plots = Plot.load(xml, this);
		povs = Pov.load(xml, this);
		parts = Part.load(xml, this);
		chapters = Chapter.load(xml, this);
		scenes = Scene.load(xml, this);
		relations = Relation.load(xml, this);
		if (last_id == -1L) {
			last_id = getLastId();
		}
	}

	private long getLastId() {
		long l = -1L;
		for (Category c : categories) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Event c : events) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Gender c : genders) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Idea c : ideas) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Memo c : memos) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Person c : persons) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Location c : locations) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Item c : items) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Tag c : tags) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Pov c : povs) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Plot c : plots) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Part c : parts) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Chapter c : chapters) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Scene c : scenes) {
			if (c.id > l) {
				l = c.id;
			}
		}
		for (Relation c : relations) {
			if (c.id > l) {
				l = c.id;
			}
		}
		return (l);
	}

	public String toHtml() {
		StringBuilder buf = new StringBuilder();
		buf.append("<p><b>\n");
		buf.append(I18N.getColonMsg("book.title"));
		buf.append("</b></p><p>\n");
		buf.append(info.getTitle());
		buf.append("</p><p style='padding-top:10px'><b>\n");
		buf.append(I18N.getColonMsg("boo.subtitle"));
		buf.append("</b></p><p>\n");
		buf.append(info.getSubtitle());
		buf.append("</p><p style='padding-top:10px'><b>\n");
		buf.append(I18N.getColonMsg("book.author"));
		buf.append("</b></p><p>\n");
		buf.append(info.getAuthor());
		buf.append("</p><p style='padding-top:10px'><b>\n");
		buf.append(I18N.getColonMsg("book.copyright"));
		buf.append("</b></p><p>\n");
		buf.append(info.getCopyright());
		buf.append("</p><p style='padding-top:10px'><b>\n");
		buf.append(I18N.getColonMsg("book.blurb"));
		buf.append("</b></p><p>\n");
		buf.append(info.getBlurb());
		buf.append("</p><p style='padding-top:10px'><b>\n");
		buf.append(I18N.getColonMsg("z.notes"));
		buf.append("</b></p><p>\n");
		buf.append(info.getNotes());
		buf.append("<p>\n");
		return (buf.toString());
	}

	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder(HtmlUtil.getTitle("book", 1));
		b.append(HtmlUtil.getAttribute("book.last_id", last_id.toString()));
		b.append(info.toHtmlDetail());
		b.append(statistics(true));
		return (b.toString());
	}

	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append("<book last_id=\"").append(getClean(last_id)).append("\">\n");
		b.append(info.toXml());
		b.append(Category.toXml(categories));
		b.append(Chapter.toXml(chapters));
		b.append(Event.toXml(events));
		b.append(Gender.toXml(genders));
		b.append(Idea.toXml(ideas));
		b.append(Item.toXml(items));
		b.append(Location.toXml(locations));
		b.append(Memo.toXml(memos));
		b.append(Part.toXml(parts));
		b.append(Person.toXml(persons));
		b.append(Photo.toXml(photos));
		b.append(Plot.toXml(plots));
		b.append(Pov.toXml(povs));
		b.append(Relation.toXml(relations));
		b.append(Scene.toXml(scenes));
		b.append(Tag.toXml(tags));
		b.append("</book>\n");
		return (b.toString());
	}

	public String statistics(boolean html) {
		StringBuilder b = new StringBuilder((html ? HtmlUtil.getTitle("book.statistics", 3) : ""));
		b.append((html ? "<p>\n" : ""));
		b.append(HtmlUtil.getAttribute("categories", categories.size()));
		b.append(HtmlUtil.getAttribute("genders", genders.size()));
		b.append(HtmlUtil.getAttribute("ideas", ideas.size()));
		b.append(HtmlUtil.getAttribute("memos", memos.size()));
		b.append(HtmlUtil.getAttribute("povs", povs.size()));
		b.append(HtmlUtil.getAttribute("photos", photos.size()));
		b.append(HtmlUtil.getAttribute("persons", persons.size()));
		b.append(HtmlUtil.getAttribute("locations", locations.size()));
		b.append(HtmlUtil.getAttribute("plots", plots.size()));
		b.append(HtmlUtil.getAttribute("items", items.size()));
		b.append(HtmlUtil.getAttribute("parts", parts.size()));
		b.append(HtmlUtil.getAttribute("chapters", chapters.size()));
		b.append(HtmlUtil.getAttribute("scenes", scenes.size()));
		b.append(HtmlUtil.getAttribute("relations", relations.size()));
		Integer n = categories.size() + genders.size()
				+ ideas.size() + memos.size() + povs.size() + persons.size()
				+ locations.size() + items.size() + plots.size() + photos.size() + parts.size()
				+ chapters.size() + scenes.size() + relations.size();
		b.append(I18N.getMsg("book.total_objects", n.toString()));
		b.append((html ? "</p>" : "")).append("\n");
		return (b.toString());
	}

	public void xmlTo(XmlDB xml) {
		//App.trace("Book.xmlTo(" + xml.name + ")");
		xml.attributeSetValue(xml.rootNode, "last_id", last_id);
		info.xmlTo(xml);
		Category.xmlTo(xml, categories);
		Chapter.xmlTo(xml, chapters);
		Gender.xmlTo(xml, genders);
		Idea.xmlTo(xml, ideas);
		Item.xmlTo(xml, items);
		Location.xmlTo(xml, locations);
		Memo.xmlTo(xml, memos);
		Part.xmlTo(xml, parts);
		Person.xmlTo(xml, persons);
		Photo.xmlTo(xml, photos);
		Plot.xmlTo(xml, plots);
		Pov.xmlTo(xml, povs);
		Relation.xmlTo(xml, relations);
		Scene.xmlTo(xml, scenes);
		Tag.xmlTo(xml, tags);
	}

	public Map<Object, Integer> getElementsSize() {
		Map<Object, Integer> sizes = new HashMap<>();
		// Get scenes
		sizes.clear();
		parts.forEach((part) -> {
			appendElementSizes(part, sizes);
		});
		return sizes;
	}

	private int appendElementSizes(Part part, Map<Object, Integer> sizes) {
		int ret = 0;
		List<Chapter> partChapters = Chapter.find(chapters, part);
		ret = partChapters.stream().map((chapter)
				-> appendElementSizes(chapter, sizes)).map((chapterSize)
				-> chapterSize).reduce(ret, Integer::sum);
		sizes.put(part, ret);
		return ret;
	}

	private int appendElementSizes(Chapter chapter, Map<Object, Integer> sizes) {
		int ret = chapter.getChars(this);
		sizes.put(chapter, ret);
		return ret;
	}

	public String getTitle() {
		return (info.getTitle());
	}

	public int getNbScenesInChapter(Chapter chapter) {
		return (Scene.find(scenes, chapter).size());
	}

	public int getNbChaptersInPart(Part part) {
		return (Chapter.find(chapters, part).size());
	}

	public int getChars() {
		int r = 0;
		for (Part part : parts) {
			r += part.getChars(this);
		}
		return (r);
	}

	public int getWords() {
		int r = 0;
		for (Part part : parts) {
			r += part.getWords(this);
		}
		return (r);
	}

	public boolean isPlanning() {
		for (Part part : parts) {
			if (part.size>0) {
				return (true);
			}
		}
		for (Chapter chapter : chapters) {
			if (chapter.size>0) {
				return (true);
			}
		}
		return (false);
	}

	public static String getIds(List<?> entities) {
		List<String> list=new ArrayList<>();
		for (Object entity:entities) {
			list.add(((AbstractEntity)entity).id.toString());
		}
		return(String.join(",", list));
	}
	
	public static String getNames(List<?> entities) {
		List<String> list=new ArrayList<>();
		for (Object entity:entities) {
			list.add(((AbstractEntity)entity).name);
		}
		return(String.join(",", list));
	}

}
