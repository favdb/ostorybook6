/*
 * Copyright (C) 2018 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.ListUtil;

/**
 *
 * @author favdb
 */
public class Location extends AbstractEntity implements Comparable<Location> {

	public String address="", city="", country="", gps="";
	public Integer altitude=0;
	public Location sup=null;
	public List<Photo> photos=new ArrayList<>();

	public static Location copy(Book book,Location old, Long id) {
		Location n=new Location();
		n.name=old.name;
		n.address=old.address;
		n.city=old.city;
		n.country=old.country;
		n.altitude=old.altitude;
		n.gps=old.gps;
		n.sup=old.sup;
		n.photos=old.photos;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.date.creation","z.date.maj","z.name",
			"location.address","location.city","location.country",
			"location.altitude","location.gps","z.sup","photos",
			"z.description","z.notes","assistant"};
		return(c);
	}

	public static List<Location> findByCountries(List<Location> locations, List<String> countries) {
		List<Location> list=new ArrayList<>();
		locations.stream().filter((l) -> (countries.contains(l.country))).forEachOrdered((l) -> {
			list.add(l);
		});
		return(list);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(address);
		cols.add(city);
		cols.add(country);
		cols.add(altitude);
		cols.add(gps);
		cols.add(sup);
		cols.add(photos);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Location() {
		super(Book.TYPE.LOCATION);
	}

	public Location(Book book, Node node) {
		super(Book.TYPE.LOCATION);
		xmlFrom(book, node);
	}

	@SuppressWarnings("OverridableMethodCallInConstructor")
	public Location(Long id, String name,
				String address,
				String city,
				String country,
				Integer altitude,
				Location sup) {
		super(Book.TYPE.LOCATION,id,name);
		this.address=(address);
		this.city=(city);
		this.country=(country);
		this.altitude=(altitude);
		this.sup=(sup);
	}

	public boolean hasCity() {
		return !((this.city == null) || (this.city.isEmpty()));
	}

	public boolean hasCountry() {
		return !((this.country == null) || (this.country.isEmpty()));
	}

	public String getCountryCity() {
		if (hasCountry()) {
			return this.country + ", " + this.city;
		}
		return this.city;
	}

	public boolean hasSup() {
		return(sup != null);
	}

	@Override
	public String toCsv(String qs, String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(address,qs,qe,se));
		b.append(toCsvInfo(city,qs,qe,se));
		b.append(toCsvInfo(country,qs,qe,se));
		b.append(toCsvInfo(altitude.toString(),qs,qe,se));
		b.append(toCsvInfo(gps,qs,qe,se));
		b.append(toCsvInfo(getCleanId(sup),qs,qe,se));
		b.append(Book.getIds(photos));
		return(super.toCsv(b.toString(),qs, qe, se));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("location.address",address));
		b.append(toHtmlInfo("location.city",city));
		b.append(toHtmlInfo("location.country",country));
		b.append(toHtmlInfo("location.altitude",altitude));
		b.append(toHtmlInfo("location.gps",gps));
		b.append(toHtmlInfo("z.sup",sup));
		b.append(toHtmlInfo("photos",Book.getNames(photos)));
		return(super.toHtmlDetail(b.toString()));
	}

	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"address", address));
		b.append(toXmlSetAttribute(0,"city",city));
		b.append(toXmlSetAttribute(0,"country",country)).append("\n");
		b.append(toXmlSetAttribute(3,"sup", sup));
		b.append(toXmlSetAttribute(0,"altitude",altitude));
		b.append(toXmlSetAttribute(0,"gps",gps)).append("\n");
		b.append(toXmlSetAttribute(3,"photos",photos));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Location> list) {
		StringBuilder b=new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	@Override
	@SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Location test = (Location) obj;
		boolean ret = true;
		ret = ret && equalsStringNullValue(address, test.address);
		ret = ret && equalsStringNullValue(city, test.city);
		ret = ret && equalsStringNullValue(country, test.country);
		ret = ret && (sup == null) || (test.sup == null) || (sup.equals(test.sup));
		ret = ret && equalsIntegerNullValue(altitude, test.altitude);
		ret = ret && equalsStringNullValue(gps, test.gps);
		ret = ret && equalsObjectNullValue(photos, test.photos);
		ret = ret && equalsStringNullValue(desc, test.desc);
		ret = ret && equalsStringNullValue(getNotes(), test.notes);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (address != null ? address.hashCode() : 0);
		hash = hash * 31 + (city != null ? city.hashCode() : 0);
		hash = hash * 31 + (country != null ? country.hashCode() : 0);
		hash = hash * 31 + (altitude != null ? altitude.hashCode() : 0);
		hash = hash * 31 + (gps != null ? gps.hashCode() : 0);
		hash = hash * 31 + (photos != null ? getListHashCode(photos) : 0);
		return hash;
	}

	@Override
	public int compareTo(Location o) {
		if (hasSup() && (!o.hasSup())) {
			return 1;
		}
		if (!hasSup() && (o.hasSup())) {
			return -1;
		}
		if (hasSup() && (o.hasSup()) && (!(sup.equals(o.sup)))) {
			return(sup.compareTo(o.sup));
		}
		if (country == null && o.country == null) {
			return name.compareTo(o.name);
		}
		if (country != null && o.country == null) {
			return -1;
		}
		if (o.country != null && country == null) {
			return -1;
		}
		int cmp = country.compareTo(o.country);
		if (cmp == 0) {
			if (city == null && o.city == null) {
				return 0;
			}
			if (city != null && o.city == null) {
				return -1;
			}
			if (o.city != null && city == null) {
				return -1;
			}
			int cmp2 = city.compareTo(o.city);
			if (cmp2 == 0) {
				return name.compareTo(o.name);
			}
			return cmp2;
		}
		return cmp;
	}

	@Override
	public final void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		Long l=attributeGetLong(node,"sup");
		sup=(book.getLocation(l));
		address=(attributeGetString(node,"address"));
		city=(attributeGetString(node,"city"));
		country=(attributeGetString(node,"country"));
		altitude=(attributeGetInteger(node,"altitude"));
		gps=(attributeGetString(node,"gps"));
		photos=(book.getPhotos(attributeGetLongs(node,"photos")));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"sup",sup);
		xml.attributeSetValue(n,"address",address);
		xml.attributeSetValue(n,"city",city);
		xml.attributeSetValue(n,"country",country);
		xml.attributeSetValue(n,"altitude",altitude);
		xml.attributeSetValue(n,"gps",gps);
		xml.attributeSetValue(n,"photos",Book.getIds(photos));
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Location> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static List<Location> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.LOCATION.toString());
		List<Location> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Location c=new Location(book, nodes.item(i));
			list.add(c);
		}
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Node n=nodes.item(i);
			Long s=xmlGetLong(n,"sup");
			if (s!=null && s!=-1L) {
				for (Location l:list) {
					if (l.id.equals(s)) {
						list.get(i).sup=l;
					}
				}
			}
		}
		return(list);
	}

	@SuppressWarnings("unchecked")
	public static List<String> findCountries(Book book) {
		List<String> list=new ArrayList<>();
		list.add("nocountry");
		book.locations.forEach((l)-> {
			if (l.hasCountry()) {
				list.add(l.country);
			}
		});
		Collections.sort(list, (String t, String t1) -> (t.compareTo(t1)));
		return(ListUtil.setUnique(list));
	}

	public static List<Location> findByCountry(List<Location> locations, String country) {
		List<Location> list=new ArrayList<>();
		locations.forEach((l)-> {
			if (country.equals(l.country)) {
				list.add(l);
			} else {
				if (country.equals("nocountry") && l.country.isEmpty()) {
					list.add(l);
				}
			}
		});
		return(list);
	}

	@SuppressWarnings("unchecked")
	public static List<String> findCities(Book book) {
		List<String> list=new ArrayList<>();
		list.add("nocity");
		book.locations.forEach((l)-> {
			list.add(l.city);
		});
		return(ListUtil.setUnique(list));
	}

	public static List<Location> findByCity(List<Location> locations, String city) {
		List<Location> list=new ArrayList<>();
		locations.forEach((l)-> {
			if (l.city.equals(city)) {
				list.add(l);
			} else if (city.equals("nocity") && l.city.isEmpty()) {
				list.add(l);
			}
		});
		return(list);
	}

	@SuppressWarnings("unchecked")
	public static List<String> findCitiesByCountry(List<Location> locations, String country) {
		List<String> cities=new ArrayList<>();
		locations.forEach((l)-> {
			if (l.country.equals(country)) {
				cities.add(l.city);
			} else if (country.equals("nocountry") && l.country.isEmpty()) {
				cities.add(l.city);
			}
		});
		Collections.sort(cities, (String t1, String t2)->t1.compareTo(t2));
		return(ListUtil.setUnique(cities));
	}

	public static List<Location> findByCountryCity(List<Location> locations, String pays, String ville) {
		List<Location>list=new ArrayList<>();
		locations.forEach((l)-> {
			if (l.country.equals(pays) && l.city.equals(ville)) {
				list.add(l);
			} else if (pays.equals("nocountry") && l.country.isEmpty()) {
				if (l.city.equals(ville) || (ville.equals("nocity") && l.city.isEmpty())) {
					list.add(l);
				}
			}
		});
		return(list);
	}

	@SuppressWarnings("unchecked")
	public static List<Location> findByChapter(List<Scene> scenes, Chapter chapter) {
		List<Location> list = new ArrayList<>();
		if (scenes!=null && !scenes.isEmpty()) {
			for (Scene s:scenes) {
				if (s.chapter.equals(chapter)) {
					list.add(s.getFirstLocation());
					list.addAll(s.locations);
				}
			}
		}
		return (ListUtil.setUnique(list));
	}

	public String getFullName() {
		StringBuilder r=new StringBuilder();
		if (sup!=null) {
			r.append(sup.name).append(",");
		}
		if (!country.isEmpty()) {
			r.append(country).append(",");
		}
		if (!city.isEmpty()) {
			r.append(city).append(",");
		}
		r.append(name);
		return(r.toString());
	}
	
	public static List<Location> copyByName(List<Location> locations) {
		List<Location> list = new ArrayList<>();
		list.addAll(locations);
		list.sort((Location p2, Location p1) -> p1.getFullName().compareTo(p2.getFullName()));
		return (list);
	}

}
