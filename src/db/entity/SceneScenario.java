/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import db.I18N;
import org.w3c.dom.Node;

/**
 *
 * @author favdb
 */
public class SceneScenario extends AbstractEntity {
	public String pitch="";
	public Integer
			moment=0,
				//0=NONE, 1=scenario.moment.01=DAY,02=AT DAWN,
				//03=MORNING,04=MIDDAY,05=AFTERNOON
				//06=SUNSET,07=NIGHT,08=EVENING,09=LATER
			loc=0,
				//00=NONE,01=INT.,02=EXT.
			start=0,
				//00=NONE,01=CUT,02=FADE IN
			end=0
				//00=NONE,01=CUT,02=FADE OUT,03=DISSOLVE TO
			;
	
	public SceneScenario() {
		super(Book.TYPE.SCENARIO);
	}
	
	public boolean hasInfo() {
		if ((pitch==null || pitch.isEmpty()) &&
				moment==0 &&
				loc==0 &&
				end==0) {
			return(false);
		}
		return(true);
	}
	
	public void setPitch(String pitch) {
		this.pitch=pitch;
	}
	
	public String getPitch() {
		return(pitch);
	}
	
	public void setMoment(Integer moment) {
		this.moment=moment;
	}
	
	public Integer getMoment() {
		return(moment);
	}
	
	public void setLoc(Integer loc) {
		this.loc=loc;
	}
	
	public Integer getLoc() {
		return(loc);
	}
	
	public void setStart(Integer start) {
		this.start=start;
	}
	
	public Integer getStart() {
		return(start);
	}
	
	public void setEnd(Integer end) {
		this.end=end;
	}
	
	public Integer getEnd() {
		return(end);
	}
	
	@Override
	public String toHtmlDetail() {
		//App.trace("SCENARIO.toHtmlDetail()");
		StringBuilder b = new StringBuilder();
		b.append("<h3>").append(I18N.getMsg("scenario")).append("</h3>\n");
		b.append(toHtmlInfo("scenario.pitch", pitch));
		b.append(toHtmlInfo("scenario.moment",
				I18N.getMsg(String.format("%s.%02d", "scenario.moment",moment))));
		b.append(toHtmlInfo("scenario.loc",
				I18N.getMsg(String.format("%s.%02d", "scenario.loc",loc))));
		b.append(toHtmlInfo("scenario.start",
				I18N.getMsg(String.format("%s.%02d", "scenario.in",start))));
		b.append(toHtmlInfo("scenario.end",
				I18N.getMsg(String.format("%s.%02d", "scenario.out",end))));
		b.append("<p></p>");
		return(b.toString());
	}
	
	public String getHeader(Location location, SceneScenario scenario) {
		//App.trace("SceneScenario.getHeader()");
		StringBuilder b = new StringBuilder();
		b.append("<p>");
		b.append(I18N.getMsg(String.format("%s.%02d", "scenario.moment",moment+1))).append(" - ");
		b.append(I18N.getMsg(String.format("%s.%02d", "scenario.loc",loc)));
		if (location!=null) b.append(" ").append(location.name);
		b.append(" - ");
		b.append(I18N.getMsg(String.format("%s.%02d", "scenario.in",start+1)));
		b.append("</p>");
		return(b.toString());
	}
	
	public String getFooter() {
		StringBuilder b=new StringBuilder();
		b.append("<p style=\"text-align:right;\">");
		b.append(I18N.getMsg(String.format("%s.%02d", "scenario.out",end+1)));
		b.append("</p>");
		return(b.toString());
	}
	
	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlTab(3)).append("<").append("scenario ");
		b.append(toXmlSetAttribute(0, "pitch", getClean(getPitch())));
		b.append(toXmlSetAttribute(0, "moment", getClean(getMoment())));
		b.append(toXmlSetAttribute(0, "loc", getClean(getLoc())));
		b.append(toXmlSetAttribute(0, "start", getClean(getStart())));
		b.append(toXmlSetAttribute(0, "end", getClean(getEnd())));
		b.append("/>\n");
		return(b.toString());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SceneScenario)) {
			return(true);
		}
		if (!super.equals(obj)) {
			return false;
		}
		SceneScenario test = (SceneScenario) obj;

		if (id != null ? !id.equals(test.id) : test.id != null) {
			return false;
		}
		boolean ret = true;
		ret = ret && equalsStringNullValue(pitch, test.getPitch());
		ret = ret && equalsIntegerNullValue(moment, test.getMoment());
		ret = ret && equalsIntegerNullValue(loc, test.getLoc());
		ret = ret && equalsIntegerNullValue(start, test.getStart());
		ret = ret && equalsIntegerNullValue(end, test.getEnd());
		return(ret);
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (pitch != null ? pitch.hashCode() : 0);
		hash = hash * 31 + (moment != null ? moment.hashCode() : 0);
		hash = hash * 31 + (loc != null ? loc.hashCode() : 0);
		hash = hash * 31 + (start != null ? start.hashCode() : 0);
		hash = hash * 31 + (end != null ? end.hashCode() : 0);
		return(hash);
	}
	
	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		//App.trace("SCENARIO.xmlFrom for scene:"+name);
		Node n=XmlDB.childGetNode(node, "scenario");
		if (n!=null) {
			setPitch(attributeGetString(n, "pitch"));
			setMoment(attributeGetInteger(n, "moment"));
			setLoc(attributeGetInteger(n, "loc"));
			setStart(attributeGetInteger(n, "start"));
			setEnd(attributeGetInteger(n, "end"));
		}
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = XmlDB.childGetNode(node, "scenario");
		xml.attributeSetValue(n, "pitch", pitch);
		xml.attributeSetValue(n, "moment", moment);
		xml.attributeSetValue(n, "loc", loc);
		xml.attributeSetValue(n, "start", start);
		xml.attributeSetValue(n, "end", end);
		return (n);
	}
}
