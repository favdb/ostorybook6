/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.Icon;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.IconUtil;
import tools.ListUtil;

/**
 *
 * @author favdb
 */
public class Item extends AbstractEntity implements Comparable<Item> {
	public Category category=null;
	public String beacon="";
	public String iconFile="";
	public List<Photo> photos;

	public static Item copy(Book book,Item old, Long id) {
		Item n=new Item();
		n.name=old.name;
		n.category=old.category;
		n.beacon=old.beacon;
		n.iconFile=old.iconFile;
		n.photos=old.photos;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"category", "category.beacon", "z.icon_file", "photos",
			"z.description","z.notes","assistant"};
		return(c);
	}

	public static List<Item> orderByCategory(List<Item> items) {
		if (items==null || items.isEmpty()) return(items);
		items.sort((Item s2, Item s1) -> s1.category.compareTo(s2.category));
		return (items);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(category);
		cols.add(beacon);
		cols.add(iconFile);
		cols.add(photos);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Item() {
		super(Book.TYPE.ITEM);
	}

	public Item(Long id, String name, Category value) {
		super(Book.TYPE.ITEM,id, name);
		this.category = value;
	}

	public Item(Book book, Node node) {
		super(Book.TYPE.ITEM);
		xmlFrom(book, node);
	}

	public boolean hasCategory() {
		return(category!=null);
	}

	@Override
	public Icon getIcon() {
		if (iconFile!=null && !iconFile.isEmpty()) {
			return(IconUtil.getIconExternal(iconFile));
		}
		return(super.getIcon());
	}
	
	@Override
	public Icon getIcon(String size) {
		if (iconFile!=null && !iconFile.isEmpty()) {
			return(IconUtil.getIconExternal(iconFile));
		}
		return(super.getIcon(size));
	}
	
	@Override
	public Icon getIcon(int size) {
		if (iconFile!=null && !iconFile.isEmpty()) {
			return(IconUtil.getIconExternal(iconFile,size));
		}
		return(super.getIcon(size));
	}
	
	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(category.toString(),qs,qe,se));
		b.append(toCsvInfo(beacon,qs,qe,se));
		b.append(toCsvInfo(iconFile,qs,qe,se));
		b.append(Book.getNames(photos));
		return(super.toCsv(b.toString(), qs, qe, se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("category",category.toString()));
		b.append(toHtmlInfo("beacon",beacon));
		b.append(toHtmlInfo("icon.file",iconFile));
		b.append(toHtmlInfo("photos",Book.getNames(photos)));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"category", category));
		b.append(toXmlSetAttribute(0,"beacon", beacon)).append("\n");
		b.append(toXmlSetAttribute(3,"iconfile",iconFile));
		b.append(toXmlSetAttribute(0,"photos",photos));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Item> items) {
		StringBuilder b=new StringBuilder();
		items.forEach((c)-> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	public boolean equals(Item item) {
		if (!super.equals(item)) {
			return false;
		}
		boolean ret = true;
		ret = ret && equalsObjectNullValue(category, item.category);
		ret = ret && equalsStringNullValue(beacon, item.beacon);
		ret = ret && equalsObjectNullValue(photos, item.photos);
		return ret;
	}

	@Override
	public int compareTo(Item o) {
		return name.compareTo(o.name);
	}
	
	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		Long l=attributeGetLong(node,"category");
		category=(book.getCategory(l));
		beacon=(attributeGetString(node,"beacon"));
		iconFile=(attributeGetString(node,"iconfile"));
		photos=(book.getPhotos(attributeGetLongs(node,"photos")));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"category",category);
		xml.attributeSetValue(n,"beacon",beacon);
		xml.attributeSetValue(n,"iconfile",iconFile);
		xml.attributeSetValue(n,"photos",Book.getIds(photos));
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Item> items) {
		items.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}
	
	public static List<Item> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.ITEM.toString());
		List<Item> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Item c=new Item(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

	public static List<Item> copyByCategory(List<Item> source) {
		List<Item> list = source.stream().collect(Collectors.toList());
		list.sort((Item p2, Item p1) -> {
			if (p1.category.equals(p2.category)) {
				return p1.name.compareTo(p2.name);
			}
			return p1.category.compareTo(p2.category);
		});
		return (list);
	}

	public static List<Item> copyByName(List<Item> source) {
		List<Item> list = source.stream().collect(Collectors.toList());
		list.sort((Item p2, Item p1) -> p1.name.compareTo(p2.name));
		return (list);
	}

	@SuppressWarnings("unchecked")
	public List<String> findCategories(List<Item> items) {
		List<String> list=new ArrayList<>();
		items.forEach((l)-> {
			if (l.category!=null) {
				list.add(l.category.name);
			}
		});
		return(ListUtil.setUnique(list));
	}

	@SuppressWarnings("IncompatibleEquals")
	public List<Item> findByCategory(List<Item> locations, String category) {
		List<Item> list=new ArrayList<>();
		locations.forEach((l)-> {
			if (l.category!=null && l.category.name.equals(category)) {
				list.add(l);
			}
		});
		return(list);
	}

}
