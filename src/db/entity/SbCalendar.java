/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.I18N;
import db.XmlDB;
import static db.entity.AbstractEntity.equalsObjectNullValue;
import static db.entity.AbstractEntity.toXmlSetAttribute;
import static db.entity.AbstractEntity.toXmlTab;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import tools.html.HtmlUtil;

/**
 *
 * @author FaVdB
 */
public class SbCalendar {
	public List<MONTH> months=new ArrayList<>();
	public List<DAY> days=new ArrayList<>();
	public int hours=24, minutes=60, seconds=60, first_day=5;
	
	public SbCalendar() {
		//App.trace("SbCalendar()");
		setMonths(I18N.getMsg("calendar.months"));
		setDays(I18N.getMsg("calendar.days"));
	}

	public SbCalendar(String general, String libMonths, String libDays) {
		//App.trace("SbCalendar(general="+general+", months="+libMonths+", libDays="+libDays+")");
		setCalendar(general);
		setMonths(libMonths);
		setDays(libDays);
	}
	
	public boolean isCalendar() {
		if (this.equals(new SbCalendar())) return(false);
		else return(true);
	}
	
	public boolean equals(SbCalendar c) {
		boolean ret = true;
		ret = ret && equalsObjectNullValue(getMonths(), c.getMonths());
		ret = ret && equalsObjectNullValue(getDays(), c.getDays());
		ret = ret && equalsObjectNullValue(secondsByDay(), c.secondsByDay());
		ret = ret && equalsObjectNullValue(getFirstDay(), c.getFirstDay());
		return(ret);
	}
	
	public static SbDate compute(SbCalendar cal, SbDate d) {
		SbDate dc=new SbDate(d.getDateTime());
		if (dc.getSecond()>cal.seconds) {
			dc.setMinute(dc.getMinute()+1);
			dc.setSecond(dc.getSecond()-cal.seconds);
		}
		if (dc.getMinute()>cal.minutes) {
			dc.setHour(dc.getHour()+1);
			dc.setMinute(dc.getMinute()-cal.minutes);
		}
		if (dc.getHour()>cal.hours) {
			dc.setDay(dc.getDay()+1);
			dc.setHour(dc.getHour()-cal.hours);
		}
		if (dc.getDay()>cal.getMonthNum(dc.getMonth())) {
			dc.setMonth(dc.getMonth()+1);
			dc.setDay(dc.getDay()-cal.getMonthNum(dc.getMonth()));
		}
		if (dc.getMonth()>cal.months.size()) {
			dc.setYear(dc.getYear()+1);
			dc.setMonth(dc.getMonth()-cal.months.size());
		}
		return(dc);
	}
	
	public void setMonths(String ms) {
		//App.trace("SbCalendar.Months(ms="+ms+")");
		if (ms.isEmpty()) return;
		months=new ArrayList<>();
		String m[]=ms.split(";");
		for (String m1 : m) {
			MONTH lm = new MONTH(m1);
			months.add(lm);
		}
	}

	public void setDays(String ms) {
		//App.trace("SbCalendar.setDays(ms="+ms+")");
		if (ms.isEmpty()) return;
		days=new ArrayList<>();
		String m[]=ms.split(";");
		for (String m1 : m) {
			DAY lm = new DAY(m1);
			days.add(lm);
		}
	}
	
	public void setFirstDay(int f) {
		//App.trace("SbCalendar.setFirstDay(ms="+f+")");
		this.first_day=f;
	}
	
	public int getFirstDay() {
		return(first_day);
	}
	
	public void setCalendar(String calendar) {
		//App.trace("SbCalendar.setCalendar(calendar="+calendar+")");
		if (calendar.isEmpty()) return;
		String m[]=calendar.split(",");
		if (m.length>0) hours=Integer.parseInt(m[0]);
		if (m.length>1) minutes=Integer.parseInt(m[1]);
		if (m.length>2) seconds=Integer.parseInt(m[2]);
		if (m.length>3) first_day=Integer.parseInt(m[3]);
	}
	
	public String getCalendar() {
		return(hours+","+minutes+","+seconds+","+first_day);
	}
	
	public String getMonthName(int m) {
		if (m<0 || months.size()<m) return("");
		return(months.get(m).name);
	}

	public String getMonthAbbr(int m) {
		if (m<0 || months.size()<m) return("");
		return(months.get(m).abbr);
	}

	public int getMonthNum(int m) {
		if (m<0 || months.size()<m) return(0);
		return(months.get(m).days);
	}

	public String getDayName(int m) {
		if (days.size()<m) return("");
		return(days.get(m).name);
	}

	public String getDayAbbr(int m) {
		if (days.size()<m) return("");
		return(days.get(m).abbr);
	}
	
	public int getHours() {
		return(hours);
	}

	public int getMinutes() {
		return(minutes);
	}

	public int getSeconds() {
		return(seconds);
	}
	
	public void setHours(int hours) {
		this.hours=hours;
	}

	public void setMinutes(int minutes) {
		this.minutes=minutes;
	}

	public void setSeconds(int seconds) {
		this.seconds=seconds;
	}
	
	public String getMonths() {
		StringBuilder r=new StringBuilder();
		months.forEach((m) -> {
			r.append(m.name).append(",").append(m.abbr).append(",").append(m.days).append(";");
		});
		return(r.toString().substring(0, r.toString().length()-1));
	}

	public String getDays() {
		StringBuilder r=new StringBuilder();
		days.forEach((m) -> {
			r.append(m.name).append(",").append(m.abbr).append(",").append(";");
		});
		return(r.toString().substring(0, r.toString().length()-1));
	}
	
	public Integer daysByYear() {
		Integer ret=0;
		ret = months.stream().map((m) -> m.days).reduce(ret, Integer::sum);
		return(ret);
	}
	
	public Integer secondsByDay() {
		Integer ret=0;
		for (int hh=0;hh<hours;hh++) {
			for (int mm=0;mm<minutes;mm++) {
				ret+=seconds;
			}
		}
		return(ret);
	}

	public String getDuration(Integer r) {
		Integer aa=r/daysByYear();
		Integer mm=0;
		Integer jj=0;
		Integer hh=0;
		Integer mn=0;
		if (r>(aa*daysByYear())) {
			r-=aa*daysByYear();
		}
		for (MONTH m:months) {
			if (r>m.days) {
				mm++;
				r-=m.days;
			} else {
				break;
			}
		}
		if (r>((hours*minutes)*seconds)) {
			hh=r/((hours*minutes)*seconds);
			r-=(hh*((hours*minutes)*seconds));
		}
		if (r>(minutes*seconds)) {
			mn=r/(minutes*seconds);
			r-=(mm*(minutes*seconds));
		}
		return(String.format("%dy %dm %dd %dH %dM %ds", aa,mm,jj,hh,mn,r));
	}

	public String getDuration(Long r) {
		Long aa=r/daysByYear();
		Long mm=0L;
		Long jj=0L;
		Long hh=0L;
		Long mn=0L;
		if (r>(aa*daysByYear())) {
			r-=aa*daysByYear();
		}
		for (MONTH m:months) {
			if (r>m.days) {
				mm++;
				r-=m.days;
			} else {
				break;
			}
		}
		if (r>((hours*minutes)*seconds)) {
			hh=r/((hours*minutes)*seconds);
			r-=(hh*((hours*minutes)*seconds));
		}
		if (r>(minutes*seconds)) {
			mn=r/(minutes*seconds);
			r-=(mm*(minutes*seconds));
		}
		return(String.format("%dy %dm %dd %dH %dM %ds", aa,mm,jj,hh,mn,r));
	}

	public static class MONTH {
		public String name="";
		public String abbr="";
		public int days=0;

		public MONTH() {
		}

		private MONTH(String x) {
			String y[]=x.split(",");
			if (y.length>0) name=y[0];
			if (y.length>1) abbr=y[1];
			if (y.length>2) days=Integer.parseInt(y[2]);
		}
	}
	
	public static class DAY {
		public String name="";
		public String abbr="";

		public DAY(String x) {
			String y[]=x.split(",");
			if (y.length>0) name=y[0];
			if (y.length>1) abbr=y[1];
		}
	}
	
	@Override
	public String toString() {
		String s="";
		s = months.stream().map((m) -> m.name+","+m.abbr+","+m.days+";").reduce(s, String::concat);
		s+="|";
		s += days.stream().map((d) -> d.name+","+d.abbr+";").reduce(s, String::concat);
		s+="|";
		s+=hours+","+minutes+","+seconds;
		s+="|";
		s+=first_day;
		return(s);
	}
	
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append("<calendar \n");
		b.append(HtmlUtil.getAttribute("calendar.months", getMonths()));
		b.append(HtmlUtil.getAttribute("calendar.days", getDays()));
		b.append(HtmlUtil.getAttribute("calendar.calendar", getCalendar()));
		return(b.toString());
	}
	
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlTab(3)).append("<calendar \n");
		b.append(toXmlSetAttribute(3,"months", getMonths()));
		b.append(toXmlSetAttribute(0,"days", getDays()));
		b.append(toXmlSetAttribute(0,"calendar",getCalendar()));
		return(b.toString());
	}
	
	public static SbCalendar xmlFrom(XmlDB xml, Node node) {
		SbCalendar c=new SbCalendar();
		c.setMonths(XmlDB.attributeGetString(node,"months",c.getMonths()));
		c.setDays(XmlDB.attributeGetString(node,"days",c.getDays()));
		c.setCalendar(XmlDB.attributeGetString(node,"calendar",c.getCalendar()));
		return(c);
	}
	
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = xml.findNode("calendar");
		xml.attributeSetValue(n,"months",getMonths());
		xml.attributeSetValue(n,"days",getDays());
		xml.attributeSetValue(n,"calendar",getCalendar());
		return(n);
	}

}
