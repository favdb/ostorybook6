/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.StringUtil;

/**
 *
 * @author favdb
 */
public class Memo extends AbstractEntity {

	public static Memo copy(Book book,Memo old, Long id) {
		Memo n=new Memo();
		n.name=old.name;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"Id","Creation","Maj","Name",
			"Description","Notes","Assistant"};
		return(c);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Memo() {
		super(Book.TYPE.MEMO);
	}

	public Memo(Book book, Node node) {
		super(Book.TYPE.MEMO);
		xmlFrom(book, node);
	}

	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Memo> list) {
		StringBuilder b=new StringBuilder();
		list.forEach((c)-> {
			b.append(c.toXml());
		});
		return(b.toString());
	}

	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = super.xmlTo(xml, node);
		return (n);
	}

	public static void xmlTo(XmlDB xml, List<Memo> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static List<Memo> load(Book book,Node node) {
		NodeList nodes=node.getOwnerDocument().getElementsByTagName(Book.TYPE.MEMO.toString());
		List<Memo> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			Memo c=new Memo(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

	public static Memo find(List<Memo> list, String x) {
		if (!x.isEmpty()) {
			if (StringUtil.isLong(x)) {
				return(find(list,Long.parseLong(x)));
			}
		}
		return(null);
	}

	public static Memo find(List<Memo> list, Long id) {
		for (Memo elem:list) {
			if (elem.id.equals(id)) {
				return(elem);
			}
		}
		return(null);
	}

	public static List<Memo> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.MEMO.toString());
		List<Memo> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Memo c=new Memo(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

}
