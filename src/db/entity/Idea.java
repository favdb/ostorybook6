/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author favdb
 */
public class Idea extends AbstractEntity {

	public Integer status=0;
	public Category category = null;
	public String theme="";

	public static Idea copy(Book book, Idea old, Long id) {
		Idea n = new Idea();
		n.name=old.name;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return (n);
	}

	public static String[] getColumns() {
		String c[] = {"z.id", "z.creation", "z.maj", "z.name",
			"status", "category", "idea.theme",
			"z.description", "z.notes", "assistant"};
		return (c);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(Status.getMsg(status));
		cols.add(category);
		cols.add(theme);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return (cols);
	}

	public Idea() {
		super(Book.TYPE.IDEA);
	}

	public Idea(Long id, String name) {
		super(Book.TYPE.IDEA, id, name);
	}

	public Idea(Book book, Node node) {
		super(Book.TYPE.IDEA);
		xmlFrom(book, node);
	}

	public boolean hasCategory() {
		return(category!=null);
	}

	@Override
	public String toCsv(String qs, String qe, String se) {
		StringBuilder b = new StringBuilder();
		b.append(qs).append(Status.getMsg(status)).append(qe).append(se);
		b.append(qs).append(getCleanId(category)).append(qe).append(se);
		b.append(qs).append(theme).append(qe).append(se);
		return (toCsv(b.toString(), qs, qe, se));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlInfo("status", Status.getMsg(status)));
		b.append(toHtmlInfo("category", category));
		b.append(toHtmlInfo("theme", theme));
		return (super.toHtmlDetail(b.toString()));
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3, "status", getClean(status))).append("\n");
		b.append(toXmlSetAttribute(3, "category", getCleanId(category)));
		b.append(toXmlSetAttribute(0, "theme", getClean(theme)));
		b.append(toXmlFooter());
		return (b.toString());
	}

	public static String toXml(List<Idea> list) {
		StringBuilder b = new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return (b.toString());
	}

	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		status=attributeGetInteger(node, "status");
		category=book.getCategory(attributeGetLong(node, "category"));
		theme=attributeGetString(node, "theme");
	}

	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = super.xmlTo(xml, node);
		xml.attributeSetValue(n, "status", status);
		xml.attributeSetValue(n, "category", category);
		xml.attributeSetValue(n, "theme", theme);
		return (n);
	}

	public static void xmlTo(XmlDB xml, List<Idea> entities) {
		entities.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static List<Idea> findByTheme(List<Idea> ideas, String theme) {
		List<Idea> list = new ArrayList<>();
		if (theme != null && !theme.isEmpty()) {
			list.forEach((elem)-> {
				if (elem.theme.equals(theme)) {
					list.add(elem);
				}
			});
		}
		return (list);
	}

	public static List<Idea> findByStatus(List<Idea> ideas, Status.STATUS state) {
		return (findByStatus(ideas, state.ordinal()));
	}

	public static List<Idea> findByStatus(List<Idea> ideas, int x) {
		List<Idea> list = new ArrayList<>();
		ideas.forEach((idea)-> {
			if (idea.status == x) {
				list.add(idea);
			}
		});
		return (list);
	}

	public static List<Idea> load(XmlDB xml, Book book) {
		NodeList nodes = xml.rootNode.getElementsByTagName(Book.TYPE.IDEA.toString());
		List<Idea> list = new ArrayList<>();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getParentNode() != xml.rootNode) {
				continue;
			}
			Idea c = new Idea(book, nodes.item(i));
			list.add(c);
		}
		return (list);
	}

}
