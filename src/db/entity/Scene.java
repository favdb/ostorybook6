/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.DB;
import db.XmlDB;
import static db.XmlDB.*;
import db.I18N;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.ListUtil;
import tools.TextUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class Scene extends AbstractEntity implements Comparable<Scene> {
    public Boolean informative = false;
    public Integer number = 0;
    public Chapter chapter = null;
    public Pov pov = null;
    public SbDate date = null;
    public Integer relativeDate = 0;
    public Scene relativeScene = null;
    public Integer status = 0;
    public String writing = "";
    public String xfile = "";
    public Person narrator = null;
    public List<Person> persons = new ArrayList<>();
	public List<Item> items = new ArrayList<>();
	public List<Tag> tags = new ArrayList<>();
	public List<Location> locations = new ArrayList<>();
	public List<Plot> plots = new ArrayList<>();
	public List<Photo> photos = new ArrayList<>();
    public String stage = "";
    public SceneScenario scenario=new SceneScenario();


    public static List<Scene> findByPlot(List<Scene> scenes, Plot plot) {
        List<Scene> list = new ArrayList<>();
        if (plot != null) {
            scenes.forEach((scene) -> {
                List<Plot> pids = scene.plots;
                for (Plot pid : pids) {
                    if (pid.id.equals(plot.id)) {
                        list.add(scene);
                    }
                }
            });
        }
        return (list);
    }

    public static int getSceneLastNumber(List<Scene> scenes) {
        int r = 0;
        for (Scene s : scenes) {
            if (s.number > r) {
                r = s.number;
            }
        }
        return (r);
    }

    public static Scene copy(Book book, Scene o, Long id) {
        Scene n = new Scene();
        n.name = o.name;
        n.informative = o.informative;
        n.number = o.number;
        n.chapter = o.chapter;
        n.pov = o.pov;
        n.date = o.date;
        n.relativeScene = o.relativeScene;
        n.relativeDate = o.relativeDate;
        n.status = o.status;
        n.writing = o.writing;
        n.xfile = o.xfile;
        n.narrator = o.narrator;
        n.persons = o.persons;
        n.items = o.items;
        n.tags = o.tags;
        n.locations = o.locations;
        n.plots = o.plots;
        n.photos = o.photos;
        n.stage = o.stage;
        n.scenario = o.scenario;
        n.desc = o.desc;
        n.notes = o.notes;
        n.assistant = o.assistant;
		n.id=id;
        return (n);
    }

    public static String[] getColumns() {
        String c[] = {"z.id", "z.creation", "z.maj", "z.name",
            "z.number", "scene.informative", "chapter", "pov", "scene.narrator", "scene.step",
            "z.date", "scene.relative.date", "scene.relative.scene", "status", "scene.texte",
            "scene.xfile",
            "persons", "locations", "items", "tags", "plots", "photos",
            "z.description", "z.notes", "assistant"};
        return (c);
    }

    public static List<Scene> getForStatus(List<Scene> scenes, int st) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.status == st) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> getForPov(Book book, List<Scene> scenes, Pov pov) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.pov.id.equals(pov.id)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> getForPerson(Book book, List<Scene> scenes, Person person) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.persons.contains(person)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> getForPlot(Book book, List<Scene> scenes, Plot plot) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.plots.contains(plot)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> getForPart(Book book, List<Scene> scenes, Part part) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.hasChapter()) {
                if (s.chapter.hasPart()) {
                    if (s.chapter.hasPart() && s.chapter.part.id.equals(part.id)) {
                        list.add(s);
                    }
                }
            }
        });
        return (list);
    }

    public static int countByItem(List<Scene> scenes, Item item) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.items.contains(item)) {
                list.add(s);
            }
        });
        return (list.size());
    }

    public static int countByLocation(List<Scene> scenes, Location location) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.locations.contains(location)) {
                list.add(s);
            }
        });
        return (list.size());
    }

    public static int countByPerson(List<Scene> scenes, Person person) {
        int n=0;
        for (Scene s:scenes) {
            if (s.persons.contains(person)) {
                n++;
            }
        }
        return(n);
    }

    @SuppressWarnings("unchecked")
    public static List<SbDate> findDistinctDates(List<Scene> scenes) {
        List<SbDate> dates = new ArrayList<>();
        for (Scene s : scenes) {
            if (s.hasDate()) {
                dates.add(s.date);
            }
        }
        dates = ListUtil.setUnique(dates);
        return (dates);
    }

    public static List<SbDate> findDistinctDates(Book book, List<Scene> scenes, Part part) {
        if (part == null) {
            return (findDistinctDates(scenes));
        }
        @SuppressWarnings("unchecked")
        List<Scene> list = find(book, scenes, part);
        return (findDistinctDates(list));
    }

    public static long countByPersonLocationDate(List<Scene> scenes, Person person, Location location, SbDate date) {
        long nb = 0L;
        for (Scene s : scenes) {
			if (s.persons==null || s.persons.isEmpty()) continue;
			if (!s.persons.contains(person)) continue;
			if (s.locations==null || s.locations.isEmpty()) continue;
			if (!s.locations.contains(location)) continue;
            if (s.hasDate() && s.date.equals(date)) {
                nb++;
            }
        }
        return(nb);
    }

    public static List<Scene> findByChapter(List<Scene> scenes, Chapter chapter) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.hasChapter() && s.chapter.id.equals(chapter.id)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByState(List<Scene> scenes, int i) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.status == i) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByPovAndDate(List<Scene> scenes, Pov pov, SbDate date) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.pov.id.equals(pov.id) && (s.hasDate() && s.date.equals(date))) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByLocation(List<Scene> scenes, Location location) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.locations.contains(location)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByPerson(List<Scene> scenes, Person person) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.persons.contains(person)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByPov(List<Scene> scenes, Pov pov) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.pov.equals(pov)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByItem(List<Scene> scenes, Item entity) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.items.contains(entity)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findWithRelative(List<Scene> scenes, Scene entity) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.hasRelativeScene() && s.relativeScene.equals(entity.id)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> orderByDate(List<Scene> scenes, Chapter chapter) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.chapter.id.equals(chapter.id)) {
                list.add(s);
            }
        });
        return (orderByDate(list));
    }

    public static List<Scene> orderByDate(List<Scene> list) {
        list.sort((Scene s2, Scene s1) -> s1.date.compareTo(s2.date));
        return (list);
    }

    public static List<Scene> orderByNumber(List<Scene> list) {
        list.sort((Scene s2, Scene s1) -> s2.getFullNumber().compareTo(s1.getFullNumber()));
        return (list);
    }

    @Override
    public List<Object> getRow() {
        //App.trace("Scene.getRow()");
        List<Object> cols = new ArrayList<>();
        cols.add(id);
        cols.add(creation);
        cols.add(maj);
        cols.add(name);
        cols.add(number);
        cols.add(informative);
        cols.add(chapter);
        cols.add(status);
        cols.add(getWords());
        cols.add(pov);
        cols.add(narrator);
        cols.add(date);
        cols.add(relativeDate);
        cols.add(relativeScene);
        cols.add(writing);
        cols.add(xfile);
        cols.add(persons);
        cols.add(locations);
        cols.add(items);
        cols.add(tags);
        cols.add(plots);
        cols.add(photos);
        cols.add(desc);
        cols.add(notes);
        cols.add(assistant);
        return (cols);
    }

    public Scene() {
        super(Book.TYPE.SCENE);
        scenario = new SceneScenario();
    }

    public Scene(Long id, String name) {
        super(Book.TYPE.SCENE, id, name);
        scenario = new SceneScenario();
    }

    public Scene(Long id, String name, Chapter chapter, Pov pov, Integer number) {
        this(id, name);
        chapter=(chapter);
        pov=(pov);
        this.number = number;
    }

    public Scene(Book book, Node node) {
        this();
        xmlFrom(book, node);
    }

    public boolean hasChapter() {
        return(chapter != null);
    }

    public boolean hasPov() {
        return(pov != null);
    }

    public String getNumberStr() {
        return(String.format("%02d", number));
    }

    public String getFullNumber() {
        StringBuilder buf = new StringBuilder();
        if (hasChapter()) {
			if (chapter.hasPart()) {
				buf.append(chapter.part.number).append(".");
			} else {
				buf.append("0.");
			}
            buf.append(chapter.getNumberRoman()).append(".");
        } else {
            buf.append("0.0.");
        }
        if (hasNumber()) {
            buf.append(String.format("%02d", number));
        } else {
            buf.append("00");
        }
        return buf.toString();
    }

    public boolean hasNumber() {
        return (number != 0);
    }

    public boolean hasDate() {
        return (date != null);
    }

    public void resetDate() {
        date = null;
    }

    public void setRelative(Integer diff, Scene s) {
        if (diff==0 || s == null) {
			resetRelative();
        } else {
			this.relativeDate = diff;
			this.relativeScene = s;
		}
    }

    public boolean hasRelativeScene() {
        return(this.relativeScene != null);
    }

    public void resetRelative() {
        relativeScene = null;
        relativeDate = 0;
    }

	public Location getFirstLocation() {
        Location loc = null;
        if (locations != null && !locations.isEmpty()) {
            loc = locations.get(0);
        }
        return (loc);
    }

    @Override
    public String toCsv(String qs, String qe, String se) {
        StringBuilder b = new StringBuilder();
        b.append(toCsvInfo(number, qs, qe, se));
        b.append(toCsvInfo(informative, qs, qe, se));
        b.append(toCsvInfo(date.getDateTime(), qs, qe, se));
        b.append(toCsvInfo(Status.getMsg(status), qs, qe, se));
        b.append(toCsvInfo(chapter.toString(), qs, qe, se));
        b.append(toCsvInfo(pov.toString(), qs, qe, se));
        b.append(toCsvInfo(stage, qs, qe, se));
        b.append(toCsvInfo(Book.getIds(persons), qs, qe, se));
        b.append(toCsvInfo(Book.getIds(locations), qs, qe, se));
        b.append(toCsvInfo(Book.getIds(items), qs, qe, se));
        b.append(toCsvInfo(Book.getIds(tags), qs, qe, se));
        b.append(toCsvInfo(Book.getIds(plots), qs, qe, se));
        b.append(toCsvInfo(Book.getIds(photos), qs, qe, se));
        b.append(toCsvInfo(relativeDate, qs, qe, se));
        b.append(toCsvInfo(relativeScene.toString(), qs, qe, se));
        b.append(toCsvInfo(narrator.toString(), qs, qe, se));
        b.append(toCsvInfo(xfile, qs, qe, se));
        b.append(toCsvInfo(writing, qs, qe, se));
        return (super.toCsv(b.toString(), qs, qe, se));
    }

    @Override
    public String toHtmlDetail() {
        //App.trace("Scene.toHtmlDetail()");
        StringBuilder b = new StringBuilder();
        b.append(toHtmlInfo("z.number", number));
        b.append(toHtmlInfo("scene.informative", informative));
        if (date != null) {
            b.append(toHtmlInfo("z.date", date.format(DB.getInstance().getFormatDateTime())));
        } else {
            b.append(toHtmlInfo("z.date", ""));
		}
        b.append(toHtmlInfo("status", Status.getMsg(status)));
        b.append(toHtmlInfo("chapter", chapter));
        b.append(toHtmlInfo("pov", pov));
        b.append(toHtmlInfo("scene.narrator", narrator));
        b.append(toHtmlInfo("scene.stage", stage));
        b.append(scenario.toHtmlDetail());
        b.append(toHtmlInfo("persons", Book.getNames(persons)));
        b.append(toHtmlInfo("locations", Book.getNames(locations)));
        b.append(toHtmlInfo("items", Book.getNames(items)));
        b.append(toHtmlInfo("tags", Book.getNames(tags)));
        b.append(toHtmlInfo("plots", Book.getNames(plots)));
        b.append(toHtmlInfo("photos", Book.getNames(photos)));
        b.append(toHtmlInfo("scene.relative.date", relativeDate));
        b.append(toHtmlInfo("scene.relative.scene", relativeScene));
        b.append(toHtmlInfo("scene.xfile", xfile));
        b.append(toHtmlInfo("scene.texte", writing));
        return (super.toHtmlDetail(b.toString()));
    }

    public String toHtmlShortDetail() {
        //App.trace("Scene.toHtmlDetail()");
        StringBuilder b = new StringBuilder();
        b.append(toHtmlInfo("z.number", number));
        b.append(toHtmlInfo("scene.informative", informative));
        if (date != null) {
            b.append(toHtmlInfo("z.date", date.format(DB.getInstance().getFormatDateTime())));
        }
        b.append(toHtmlInfo("status", Status.getMsg(status)));
        b.append(toHtmlInfo("chapter", chapter));
        b.append(toHtmlInfo("pov", pov));
        b.append(toHtmlInfo("scene.narrator", narrator));
        b.append(toHtmlInfo("scene.stage", stage));
        b.append(toHtmlInfo("persons", Book.getNames(persons)));
        b.append(toHtmlInfo("locations", Book.getNames(locations)));
        b.append(toHtmlInfo("items", Book.getNames(items)));
        b.append(toHtmlInfo("tags", Book.getNames(tags)));
        b.append(toHtmlInfo("plots", Book.getNames(plots)));
        b.append(toHtmlInfo("photos", Book.getNames(photos)));
        b.append(toHtmlInfo("scene.relative.date", relativeDate));
        b.append(toHtmlInfo("scene.relative.scene", relativeScene));
        b.append(toHtmlInfo("scene.xfile", xfile));
        b.append(scenario.toHtmlDetail());
        b.append(toHtmlInfo("z.words", TextUtil.countWords(writing), "z.chars", HtmlUtil.getChars(writing)));
        return (b.toString());
    }

    @Override
    public String toXml() {
        StringBuilder b = new StringBuilder();
        b.append(toXmlHeader());
        b.append(toXmlSetAttribute(3, "number", number));
        b.append(toXmlSetAttribute(0, "informative", informative));
        b.append(toXmlSetAttribute(0, "date", date));
        b.append(toXmlSetAttribute(0, "status", status)).append("\n");
        b.append(toXmlSetAttribute(3, "chapter", chapter));
        b.append(toXmlSetAttribute(0, "pov", pov));
        b.append(toXmlSetAttribute(0, "stage", stage)).append("\n");
        b.append(toXmlSetAttribute(3, "persons", persons)).append("\n");
        b.append(toXmlSetAttribute(3, "locations", locations)).append("\n");
        b.append(toXmlSetAttribute(3, "items", items)).append("\n");
        b.append(toXmlSetAttribute(3, "tags", tags)).append("\n");
        b.append(toXmlSetAttribute(3, "plots", plots)).append("\n");
        b.append(toXmlSetAttribute(3, "photos", photos)).append("\n");
        b.append(toXmlSetAttribute(3, "relative.date", relativeDate));
        b.append(toXmlSetAttribute(0, "relative.scene", relativeScene)).append("\n");
        b.append(toXmlSetAttribute(3, "narrator", narrator)).append("\n");
        b.append(toXmlSetAttribute(3, "xfile", xfile)).append(" >\n");
        if (scenario != null && scenario.hasInfo()) {
            b.append(scenario.toXml());
        }
        if (!writing.isEmpty()) {
            b.append(toXmlChildData(2, "texte", writing));
        }
        b.append(toXmlEnd());
        return (b.toString());
    }

    public static String toXml(List<Scene> list) {
        StringBuilder b = new StringBuilder();
        list.forEach((s) -> {
            b.append(s.toXml());
        });
        return (b.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        Scene test = (Scene) obj;

        if (id != null ? !id.equals(test.id) : test.id != null) {
            return false;
        }
        boolean ret = true;
        ret = ret && equalsIntegerNullValue(number, test.number);
        ret = ret && equalsObjectNullValue(informative, test.informative);
        ret = ret && equalsObjectNullValue(status, test.status);
        ret = ret && equalsObjectNullValue(chapter, test.chapter);
        ret = ret && equalsObjectNullValue(pov, test.pov);
        if (date != null) {
            ret = ret && equalsObjectNullValue(date, test.date);
        } else if (relativeScene != null) {
            ret = ret && equalsObjectNullValue(relativeScene, test.relativeScene);
            ret = ret && equalsIntegerNullValue(relativeDate, test.relativeDate);
        }
        ret = ret && equalsObjectNullValue(narrator, test.narrator);
        ret = ret && equalsObjectNullValue(scenario, test.scenario);
        ret = ret && equalsObjectNullValue(persons, test.persons);
        ret = ret && equalsObjectNullValue(items, test.items);
        ret = ret && equalsObjectNullValue(tags, test.tags);
        ret = ret && equalsObjectNullValue(locations, test.locations);
        ret = ret && equalsObjectNullValue(plots, test.plots);
        ret = ret && equalsStringNullValue(desc, test.desc);
        ret = ret && equalsStringNullValue(notes, test.notes);
        return ret;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = hash * 31 + (number != null ? number.hashCode() : 0);
        hash = hash * 31 + (status != null ? status.hashCode() : 0);
        hash = hash * 31 + (chapter != null ? chapter.hashCode() : 0);
        hash = hash * 31 + (pov != null ? pov.hashCode() : 0);
        hash = hash * 31 + (date != null ? date.hashCode() : 0);
        hash = hash * 31 + (relativeScene != null ? relativeScene.hashCode() : 0);
        hash = hash * 31 + (relativeDate != null ? relativeDate.hashCode() : 0);
        hash = hash * 31 + (writing != null ? writing.hashCode() : 0);
        hash = hash * 31 + (informative != null ? informative.hashCode() : 0);
        hash = hash * 31 + (narrator != null ? narrator.hashCode() : 0);
        hash = hash * 31 + (items != null ? getListHashCode(items) : 0);
        hash = hash * 31 + (locations != null ? getListHashCode(locations) : 0);
        hash = hash * 31 + (persons != null ? getListHashCode(persons) : 0);
        hash = hash * 31 + (plots != null ? getListHashCode(plots) : 0);
        hash = hash * 31 + (tags != null ? getListHashCode(tags) : 0);
        return hash;
    }

    @Override
    public int compareTo(Scene o) {
        int cmp = chapter.compareTo(o.chapter);
        if (cmp == 0) {
            return number.compareTo(o.number);
        }
        return cmp;
    }

    @Override
    public void xmlFrom(XmlDB xml, Book book, Node node) {
        super.xmlFrom(xml, book, node);
        //App.trace("Scene.xmlFrom for scene:"+name);
        number=(attributeGetInteger(node, "number"));
        informative=(attributeGetBoolean(node, "informative"));
        date=(attributeGetDate(node, "date"));
        status=(attributeGetInteger(node, "status"));
        chapter=(book.getChapter(attributeGetLong(node, "chapter")));
        stage=(attributeGetString(node, "stage"));
        pov=(book.getPov(attributeGetLong(node, "pov")));
        scenario.xmlFrom(xml, book, node);
        persons=(book.getPersons(attributeGetLongs(node, "persons")));
        locations=(book.getLocations(attributeGetLongs(node, "locations")));
        items=(book.getItems(attributeGetLongs(node, "items")));
		tags=(book.getTags(attributeGetLongs(node, "tags")));
        plots=(book.getPlots(attributeGetLongs(node, "plots")));
        photos=(book.getPhotos(attributeGetLongs(node, "photos")));
        relativeDate=(attributeGetInteger(node, "relativedate", relativeDate));
        relativeScene=(book.getScene(attributeGetLong(node, "relativescene")));
        narrator=(book.getPerson(attributeGetLong(node, "narrator")));
        xfile=(attributeGetString(node, "xfile"));
        writing=(childContentGet(node, "texte"));
    }

    @Override
    public Node xmlTo(XmlDB xml, Node node) {
        Node n = super.xmlTo(xml, node);
        xml.attributeSetValue(n, "number", number);
        xml.attributeSetValue(n, "informative", informative);
        xml.attributeSetValue(n, "date", date);
        xml.attributeSetValue(n, "status", status);
        xml.attributeSetValue(n, "chapter", chapter);
        xml.attributeSetValue(n, "pov", pov);
        xml.attributeSetValue(n, "stage", stage);
        if (scenario != null) {
            scenario.xmlTo(xml, n);
        }
        xml.attributeSetValue(n, "persons", Book.getIds(persons));
        xml.attributeSetValue(n, "locations", Book.getIds(locations));
        xml.attributeSetValue(n, "items", Book.getIds(items));
        xml.attributeSetValue(n, "tags", Book.getIds(tags));
        xml.attributeSetValue(n, "plots", Book.getIds(plots));
        xml.attributeSetValue(n, "photos", Book.getIds(photos));
        xml.attributeSetValue(n, "relativedate", relativeDate);
        xml.attributeSetValue(n, "relativescene", relativeScene);
        xml.attributeSetValue(n, "narrator", narrator);
        xml.attributeSetValue(n, "xfile", xfile);
        XmlDB.childContentSet(xml.document, n, "texte", writing);
        return (n);
    }

    public static List<Scene> load(XmlDB xml, Book book) {
        NodeList nodes = xml.rootNode.getElementsByTagName(Book.TYPE.SCENE.toString());
        List<Scene> list = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            if (nodes.item(i).getParentNode() != xml.rootNode) {
                continue;
            }
            Scene c = new Scene(book, nodes.item(i));
            list.add(c);
        }
        return (list);
    }

    public static void xmlTo(XmlDB xml, List<Scene> entities) {
        entities.forEach((c) -> {
            c.xmlTo(xml, xml.rootNode);
        });
    }

    @SuppressWarnings("null")
    public static List<Scene> find(List<Scene> scenes, Chapter chapter) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((scene) -> {
            if (chapter == null) {
                if (!scene.hasChapter()) {
                    list.add(scene);
                }
            } else {
                if (scene.hasChapter() && scene.chapter.id.equals(chapter.id)) {
                    list.add(scene);
                }
            }
        });
        return (list);
    }

    public static List find(Book book, List<Scene> scenes, Part part) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.hasChapter()) {
                Chapter c = s.chapter;
                if (c.hasPart() && c.part.id.equals(part.id)) {
                    list.add(s);
                }
            }
        });
        return (list);
    }

    public static List<Scene> findByNumber(List<Scene> scenes, Chapter chapter) {
        return (Scene.orderByNumber(find(scenes, chapter)));
    }

    public static List<Scene> findByStatus(List<Scene> scenes, int status) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.status == status) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<SbDate> findDistinctDatesByPov(List<Scene> scenes, Pov pov) {
        List<SbDate> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.pov != null && s.pov.equals(pov)) {
                if (s.hasDate()) {
                    list.add(s.date);
                }
            }
        });
        ListUtil.setUnique(list);
        return (list);
    }

    public Integer getWords() {
        return (TextUtil.countWords(writing));
    }

    public Integer getChars() {
        return (HtmlUtil.getChars(writing));
    }

    public String getChapterSceneNo(boolean colon) {
        @SuppressWarnings("null")
        String str = String.format("%02d.%02d ",
                (hasChapter() ? chapter.number : 0),
                (hasNumber() ? number : 0));
        return(str);
    }

    @Override
    public String getFullTitle() {
        return (getChapterSceneNo(false) + " " + name);
    }

    public String getTitleText(boolean truncate, int length) {
        if (name == null || name.isEmpty()) {
            return(Scene.this.getWriting(length));
        }
        return name + ": " + Scene.this.getWriting(length);
    }

    public String getChapterSceneToolTip() {
        if (!hasChapter()) {
            return "";
        }
        StringBuilder buf = new StringBuilder("<html>");
        buf.append(I18N.getColonMsg("chapter")).append(chapter.toString());
        buf.append("<br>");
        if (chapter.hasPart()) {
            buf.append(I18N.getColonMsg("part"));
            buf.append(" ");
			if (chapter.hasPart()) {
				buf.append(chapter.part.toString());
			}
			else {
				buf.append(I18N.getMsg("z.none"));
			}
            buf.append("<br>");
        }
        return buf.toString();
    }

    public int getFullNo() {
        return(Integer.getInteger(getFullNumber()));
    }

    public static Scene findNumber(List<Scene> scenes, int n) {
        for (Scene s : scenes) {
            if (s.number.equals(n)) {
                return (s);
            }
        }
        return (null);
    }

    public static Integer getNextNumber(List<Scene> scenes, Chapter c) {
        Integer r = 0;
        for (Scene p : scenes) {
            if (p.hasChapter() && p.chapter.equals(c) && p.number > r) {
                r = p.number;
            }
        }
        return (r + 1);
    }

    public static List<Scene> findByDate(List<Scene> scenes, SbDate date) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            if (s.date.equals(date)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static List<Scene> findByPhoto(List<Scene> scenes, Photo entity) {
        List<Scene> list = new ArrayList<>();
        scenes.forEach((s) -> {
            List<Photo> photos = s.photos;
            if (photos.contains(entity)) {
                list.add(s);
            }
        });
        return (list);
    }

    public static SbDate getFirstDate(List<Scene> scenes) {
        SbDate d = SbDate.getToDay();
        for (Scene scene : scenes) {
            if (scene.hasDate() && scene.date.before(d)) {
                d = scene.date;
            }
        }
        return (d);
    }

    public static void renumber(Book book, Chapter chapter, int start, int inc) {
        int n = start;
        for (Scene s : book.scenes) {
            if (s.hasChapter() && s.chapter.id.equals(chapter.id)) {
                s.number=(n);
				n=n+inc;
            }
        }
    }

	public String getWriting(int length) {
		return(TextUtil.truncate(writing, length));
	}

}
