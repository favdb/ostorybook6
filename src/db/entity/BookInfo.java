/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import static db.entity.AbstractEntity.toXmlChild;
import static db.entity.AbstractEntity.toXmlSetAttribute;
import db.XmlDB;
import static db.XmlDB.*;
import org.w3c.dom.Node;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class BookInfo {
	private String title="", subtitle="", author="", copyright="", blurb="", notes="";
	private SbDate creation, maj;
	private boolean poltiUse=false, stepUse=false, calendarUse=false;
	public boolean scenario=false;
	public SbCalendar calendar=new SbCalendar();
	public BookAssistant assistant = new BookAssistant();
	
	public BookInfo() {
		init();
	}
	
	private void init() {
		setTitle("New Book");
		setCreation();
		setMaj();
		notes="";
	}
	
	public void init(String str) {
		setTitle(str);
		setCreation();
		setMaj();
	}
	
	public static BookInfo load(XmlDB xml) {
		Node n=xml.findNode("info");
		if (n==null) {
			return(null);
		}
		return(xmlFrom(xml,n));
	}

	public void setTitle(String str) {
		title=str;
	}
	
	public String getTitle() {
		return(title);
	}

	public void setSubtitle(String str) {
		subtitle=str;
	}
	
	public String getSubtitle() {
		return(subtitle);
	}

	public void setAuthor(String str) {
		author=str;
	}
	
	public String getAuthor() {
		return(author);
	}

	public void setCopyright(String str) {
		copyright=str;
	}
	
	public String getCopyright() {
		return(copyright);
	}

	public void setBlurb(String str) {
		blurb=str;
	}
	
	public String getBlurb() {
		return(blurb);
	}

	public void setNotes(String str) {
		notes=str;
	}
	
	public String getNotes() {
		return(notes);
	}

	public void setCreation() {
		creation=SbDate.getToDay();
	}
	
	public void setCreation(SbDate str) {
		creation=str;
	}
	
	public SbDate getCreation() {
		return(creation);
	}

	public void setMaj() {
		maj=SbDate.getToDay();
	}
	
	public void setMaj(SbDate str) {
		maj=str;
	}
	
	public SbDate getMaj() {
		return(maj);
	}

	public SbCalendar getCalendar() {
		return (calendar);
	}

	public void setCalendar(SbCalendar b) {
		calendar = b;
	}

	public void setPoltiUse(boolean x) {
		poltiUse=x;
	}
	
	public boolean getPoltiUse() {
		return(poltiUse);
	}
	
	public boolean isUsePolti() {
		return(poltiUse);
	}
	
	public void setStepUse(boolean x) {
		stepUse=x;
	}
	
	public boolean getStepUse() {
		return(stepUse);
	}
	
	public boolean isUseStep() {
		return(stepUse);
	}
	
	public void setAssistant(BookAssistant x) {
		assistant=x;
	}
	
	public BookAssistant getAssistant() {
		return(assistant);
	}
	
	public void setScenario() {
		this.scenario=true;
	}
	
	public void setScenario(boolean b) {
		this.scenario=b;
	}
	
	public boolean isScenario() {
		return(scenario);
	}
	
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder(HtmlUtil.getTitle("book.info",2));
		//b.append("<p>\n");
		b.append(HtmlUtil.getAttribute("z.date.creation", creation.getDateTime()));
		b.append(HtmlUtil.getAttribute("z.date.maj", maj.getDateTime()));
		b.append(HtmlUtil.getAttribute("book.title", title));
		b.append(HtmlUtil.getAttribute("book.subtitle", subtitle));
		b.append(HtmlUtil.getAttribute("book.author", author));
		b.append(HtmlUtil.getAttribute("book.copyright", copyright));
		b.append(HtmlUtil.getAttribute("book.scenario",scenario));
		if (calendar.isCalendar()) b.append(calendar.toHtmlDetail());
		b.append(HtmlUtil.getAttribute("assistant.step_use",isUseStep()));
		b.append(HtmlUtil.getAttribute("assistant.step",assistant.getStep()));
		b.append(HtmlUtil.getAttribute("z.notes",assistant.getStepNotes()));
		b.append(HtmlUtil.getAttribute("assistant.polti_use",isUsePolti()));
		b.append(HtmlUtil.getAttribute("assistant.polti",assistant.getPolti()));
		b.append(HtmlUtil.getAttribute("z.notes",assistant.getPoltiNotes()));
		//b.append("</p>\n");
		return(b.toString());
	}
	
	public String toXml() {
		StringBuilder b=new StringBuilder("    <info \n");
		b.append(toXmlSetAttribute(2,"creation", creation.toStringInteger())).append("\n");
		b.append(toXmlSetAttribute(2,"maj", maj.toStringInteger())).append("\n");
		b.append(toXmlSetAttribute(2,"stepUse", stepUse)).append("\n");
		b.append(toXmlSetAttribute(2,"poltiUse", poltiUse)).append("\n");
		b.append(toXmlSetAttribute(2,"title", title)).append("\n");
		b.append(toXmlSetAttribute(2,"subtitle", subtitle)).append("\n");
		b.append(toXmlSetAttribute(2,"author", author)).append("\n");
		b.append(toXmlSetAttribute(2,"scenario", scenario)).append("\n");
		if (calendar!=null) b.append(toXmlSetAttribute(2,"calendarUse", calendar.isCalendar())).append("\n");
		else b.append(toXmlSetAttribute(2,"calendarUse", false)).append("\n");
		b.append("        >\n");
		if (calendar!=null && calendar.isCalendar()) b.append(calendar.toXml());
		b.append(assistant.toXml());
		b.append(toXmlChild(2,"blurb", blurb));
		b.append(toXmlChild(2,"notes", notes));
		b.append("    </info>\n");
		return(b.toString());
	}

	public void xmlTo(XmlDB xml) {
		Node n=childGetNode(xml.rootNode,"info");
		xml.attributeSetValue(n, "creation", creation.toStringInteger());
		xml.attributeSetValue(n, "maj", maj.toStringInteger());
		xml.attributeSetValue(n, "title", title);
		xml.attributeSetValue(n, "subtitle", subtitle);
		xml.attributeSetValue(n, "author", author);
		xml.attributeSetValue(n, "copyright", copyright);
		xml.attributeSetValue(n, "calendarUse", calendar.isCalendar());
		if (calendar.isCalendar()) calendar.xmlTo(xml, n);
		else xml.removeNode("calendar");
		xml.attributeSetValue(n, "scenario", scenario);
		xml.attributeSetValue(n, "stepUse", stepUse);
		xml.attributeSetValue(n, "poltiUse", poltiUse);
		assistant.xmlTo(xml,n);
		xml.childContentSet(xml, n, "blurb", blurb);
		xml.childContentSet(xml, n, "notes", notes);
	}
	
	public static BookInfo xmlFrom(XmlDB xml, Node node) {
		BookInfo p=new BookInfo();
		p.setCreation(new SbDate(attributeGetString(node,"creation")));
		p.setMaj(new SbDate(attributeGetString(node,"maj")));
		p.setTitle(attributeGetString(node,"title"));
		p.setSubtitle(attributeGetString(node,"subtitle"));
		p.setAuthor(attributeGetString(node,"author"));
		p.setCopyright(attributeGetString(node,"copyright"));
		p.setScenario(attributeGetBoolean(node,"scenario"));
		p.setCalendar(SbCalendar.xmlFrom(xml, node));
		p.setStepUse(attributeGetBoolean(node,"stepUse"));
		p.setPoltiUse(attributeGetBoolean(node,"poltiUse"));
		p.setAssistant(BookAssistant.fromXml(xml,node));
		p.setBlurb(childContentGet(node,"blurb"));
		p.setNotes(childContentGet(node,"notes"));
		return(p);
	}

}
