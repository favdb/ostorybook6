/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class Gender extends AbstractEntity {

	public Integer sort=0,
			childhood=6,
			adolescence=12,
			adulthood=18,
			retirement=65;
	public String iconFile="";

	public static Gender copy(Book book, Gender old, Long id) {
		Gender n = new Gender();
		n.name=old.name;
		n.sort=old.sort;
		n.childhood=old.childhood;
		n.adolescence=old.adolescence;
		n.adulthood=old.adulthood;
		n.retirement=old.retirement;
		n.iconFile=old.iconFile;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return (n);
	}

	public static String[] getColumns() {
		String c[] = {"z.id", "z.creation", "z.maj", "z.name",
			"z.sort", "gender.childhood", "gender.adolescence", "gender.adultdhood", "gender.retirement", "z.icon_file",
			"z.description", "z.notes", "assistant"};
		return (c);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(sort);
		cols.add(childhood);
		cols.add(adolescence);
		cols.add(adulthood);
		cols.add(retirement);
		cols.add(iconFile);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return (cols);
	}

	public Gender() {
		super(Book.TYPE.GENDER);
	}

	public Gender(String name, Integer sort, Integer childhood, Integer adolescence,
			Integer adulthood, Integer retirement) {
		super(Book.TYPE.GENDER);
		this.name = name;
		this.sort = sort;
		this.childhood = childhood;
		this.adolescence = adolescence;
		this.adulthood = adulthood;
		this.retirement = retirement;
	}

	public Gender(Long id, String name, Integer sort) {
		super(Book.TYPE.GENDER, id, name);
		this.sort = sort;
	}

	public Gender(Book book, Node n) {
		super(Book.TYPE.GENDER);
		xmlFrom(book, n);
	}

	public boolean isMale() {
		return (id == 1L);
	}

	public boolean isFemale() {
		return (id == 2L);
	}

	@Override
	public Icon getIcon(String size) {
		int dim = 16;
		if (size.equals("medium")) {
			dim = 32;
		} else if (size.equals("large")) {
			dim = 64;
		}
		Icon ic = IconUtil.getIcon(size + "/person");
		if (iconFile != null && !iconFile.isEmpty()) {
			ic = IconUtil.getIconExternal(iconFile, new Dimension(dim, dim));
		} else if (isMale()) {
			ic = IconUtil.getIcon(size + "/man");
		} else if (isFemale()) {
			ic = IconUtil.getIcon(size + "/woman");
		} else if (iconFile != null) {
			ic = new ImageIcon(iconFile);
		}
		return (ic);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String toCsv(String qs, String qe, String se) {
		StringBuilder b = new StringBuilder();
		b.append(toCsvInfo(getClean(sort), qs, qe, se));
		b.append(toCsvInfo(getClean(childhood), qs, qe, se));
		b.append(toCsvInfo(getClean(adolescence), qs, qe, se));
		b.append(toCsvInfo(getClean(adulthood), qs, qe, se));
		b.append(toCsvInfo(getClean(retirement), qs, qe, se));
		b.append(toCsvInfo(iconFile, qs, qe, se));
		return (super.toCsv(b.toString(), qs, qe, se));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlInfo("z.sort", sort));
		b.append(toHtmlInfo("gender.childhood", childhood));
		b.append(toHtmlInfo("gender.adolescence", adolescence));
		b.append(toHtmlInfo("gender.adulthood", adulthood));
		b.append(toHtmlInfo("gender.retirement", retirement));
		b.append(toHtmlInfo("icon.file", iconFile));
		return (super.toHtmlDetail(b.toString()));
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3, "sort", getClean(sort))).append("\n");
		b.append(toXmlSetAttribute(3, "childhood", getClean(childhood)));
		b.append(toXmlSetAttribute(0, "adolescence", getClean(adolescence)));
		b.append(toXmlSetAttribute(0, "adulthood", getClean(adulthood)));
		b.append(toXmlSetAttribute(0, "retirement", getClean(retirement))).append("\n");
		b.append(toXmlSetAttribute(3, "iconfile", getClean(iconFile)));
		b.append(toXmlFooter());
		return (b.toString());
	}

	public static String toXml(List<Gender> list) {
		StringBuilder b = new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return (b.toString());
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Gender test = (Gender) obj;
		boolean ret = true;
		ret = ret && equalsStringNullValue(name, test.name);
		ret = ret && equalsIntegerNullValue(childhood, test.childhood);
		ret = ret && equalsIntegerNullValue(adolescence, test.adolescence);
		ret = ret && equalsIntegerNullValue(adulthood, test.adulthood);
		ret = ret && equalsIntegerNullValue(retirement, test.retirement);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (childhood != null ? childhood.hashCode() : 0);
		hash = hash * 31 + (adolescence != null ? adolescence.hashCode() : 0);
		hash = hash * 31 + (adulthood != null ? adulthood.hashCode() : 0);
		hash = hash * 31 + (retirement != null ? retirement.hashCode() : 0);
		return hash;
	}

	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		sort=xmlGetInteger(node, "sort");
		childhood=(xmlGetInteger(node, "childhood"));
		adolescence=(xmlGetInteger(node, "adolescence"));
		adulthood=(xmlGetInteger(node, "adulthood"));
		retirement=(xmlGetInteger(node, "retirement"));
		iconFile=(xmlGetString(node, "iconfile"));
	}

	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = super.xmlTo(xml, node);
		xml.attributeSetValue(n, "sort", sort);
		xml.attributeSetValue(n, "childhood", childhood);
		xml.attributeSetValue(n, "adolescence", adolescence);
		xml.attributeSetValue(n, "adulthood", adulthood);
		xml.attributeSetValue(n, "retirement", retirement);
		xml.attributeSetValue(n, "iconfile", iconFile);
		return (n);
	}

	public static void xmlTo(XmlDB xml, List<Gender> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static List<Gender> load(XmlDB xml, Book book) {
		List<Gender> list = new ArrayList<>();
		NodeList nodes = xml.rootNode.getElementsByTagName("gender");
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getParentNode() != xml.rootNode) {
				continue;
			}
			Gender c = new Gender(book, nodes.item(i));
			list.add(c);
		}
		return (list);
	}

	public static Integer getNextSort(List<Gender> genders) {
		Integer ret = 0;
		for (Gender c : genders) {
			if (c.sort > ret) {
				ret = c.sort;
			}
		}
		return ret + 1;
	}

}
