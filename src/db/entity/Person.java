/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.IconUtil;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class Person extends AbstractEntity implements Comparable<Person> {

	public Gender gender=null;
	public String firstname = "",
			lastname = "",
			abbreviation = "",
			occupation = "";
	public SbDate birthday=null, dayofdeath=null;
	public Integer color=0;
	public Category category=null;
	public List<Category> categories=new ArrayList<>();
	public List<Photo> photos=new ArrayList<>();

	public static Person copy(Book book,Person old, Long id) {
		Person n = new Person();
		n.name=old.name;
		n.gender=old.gender;
		n.firstname=old.firstname;
		n.lastname=old.lastname;
		n.abbreviation=old.abbreviation;
		n.birthday=old.birthday;
		n.dayofdeath=old.dayofdeath;
		n.occupation=old.occupation;
		n.color=old.color;
		n.category=old.category;
		n.categories=old.categories;
		n.photos=old.photos;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return (n);
	}

	public static String[] getColumns() {
		String c[] = {"z.id", "z.creation", "z.maj", "z.name",
			"gender", "person.firstname", "person.lastname", "z.abbreviation",
			"gender", "category", "categories",
			"person.birthday", "person.dayofdeath", "person.occupation",
			"z.color", "photos",
			"z.description", "z.notes", "assistant"};
		return (c);
	}

	public static List<Person> findByGender(List<Person> persons, Gender gender) {
		List<Person> list = new ArrayList<>();
		persons.stream().filter((p) -> (p.gender!=null && p.gender.id.equals(gender.id))).forEachOrdered((p) -> {
			list.add(p);
		});
		return (list);
	}

	public static List<Person> findByCategory(List<Person> persons, Category category) {
		List<Person> list = new ArrayList<>();
		persons.stream().filter((p) -> (p.categories.contains(category))).forEachOrdered((p) -> {
			list.add(p);
		});
		return (list);
	}

	public static List<Person> findByCategories(List<Person> persons, List<Category> categories) {
		List<Person> list = new ArrayList<>();
		for (Person p : persons) {
			for (Category c:categories) {
				if (p.categories.contains(c)) {
					list.add(p);
					break;
				}
			}
		}
		return (list);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(firstname);
		cols.add(lastname);
		cols.add(abbreviation);
		cols.add(gender);
		cols.add(category);
		cols.add(categories);
		cols.add(birthday);
		cols.add(dayofdeath);
		cols.add(occupation);
		cols.add(color);
		cols.add(photos);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return (cols);
	}

	public Person() {
		super(Book.TYPE.PERSON);
	}

	public Person(Book book, Node node) {
		super(Book.TYPE.PERSON);
		xmlFrom(book, node);
	}

	public Person(Gender gender, String firstname, String lastname,
			String abbreviation, String birthday, String dayofdeath,
			String occupation, String description, Integer color, String notes,
			List<Category> categories) {
		super(Book.TYPE.PERSON);
		this.gender = gender;
		this.firstname = firstname;
		this.lastname = lastname;
		this.abbreviation = abbreviation;
		this.birthday = new SbDate(birthday);
		this.dayofdeath = new SbDate(dayofdeath);
		this.occupation = occupation;
		this.desc = description;
		this.color = color;
		this.notes = notes;
		this.categories = categories;
		this.assistant = "";
	}

	public String getFullName() {
		return(firstname + " " + lastname);
	}

	public String getFullNameAbbr() {
		return(firstname + " " + lastname + " [" + abbreviation + "]");
	}

	public boolean hasBirthday() {
		return(birthday!=null);
	}
	
	public boolean hasDayofDeath() {
		return(this.dayofdeath!=null);
	}
	
	public Color getJColor() {
		if (color == null) {
			return null;
		}
		return(new Color(color));
	}

	public String getHTMLColor() {
		return(HtmlUtil.getHTMLName(getJColor()));
	}

	public void setJColor(Color color) {
		if (color == null) {
			this.color = null;
			return;
		}
		this.color = color.getRGB();
	}

	public boolean hasCategory() {
		return (category!=null);
	}

	@Override
	public String getAbbr() {
		return(abbreviation);
	}

	public Icon getIconPerson(int size) {
		switch(size) {
			case 16: return(getIconPerson("small"));
			case 32: return(getIconPerson("medium"));
			case 64: return(getIconPerson("large"));
		}
		return(getIcon(size));
	}
	
	public Icon getIconPerson(String size) {
		Icon icon=IconUtil.getIcon(size+"/person");
		int dim=16;
		if (size.equals("medium")) dim=32;
		if (size.equals("large")) dim=64;
		if (gender.iconFile!=null && !gender.iconFile.isEmpty()) {
			icon=IconUtil.resizeIcon((ImageIcon) getIcon(), new Dimension(dim,dim));
		}
		else if (gender.isMale()) icon=IconUtil.getIcon(size+"/man");
		else if (gender.isFemale()) icon=IconUtil.getIcon(size+"/woman");
		return(icon);
	}

	@Override
	public Icon getIcon() {
		if (gender != null) {
			return(getIconPerson("small"));
		}
		return(super.getIcon());
	}

	@Override
	public Icon getIcon(int size) {
		switch(size) {
			case 16: return(getIconPerson("small"));
			case 32: return(getIconPerson("medium"));
			case 64: return(getIconPerson("large"));
		}
		return(super.getIcon(size));
	}

	public Boolean isDead(SbDate now) {
		if (dayofdeath == null || now == null) {
			return false;
		}
		return (now.after(dayofdeath));
	}

	@Override
	public String toString() {
		if (isTransient()) {
			return "";
		}
		return(getFullNameAbbr() + (this.hasNotes() ? "*" : ""));
	}

	@Override
	public String toCsv(String qs, String qe, String se) {
		StringBuilder b = new StringBuilder();
		b.append(toCsvInfo(firstname, qs, qe, se));
		b.append(toCsvInfo(lastname, qs, qe, se));
		b.append(toCsvInfo(abbreviation, qs, qe, se));
		b.append(toCsvInfo(gender, qs, qe, se));
		b.append(toCsvInfo(category, qs, qe, se));
		b.append(Book.getIds(categories));
		b.append(toCsvInfo(birthday, qs, qe, se));
		b.append(toCsvInfo(dayofdeath, qs, qe, se));
		b.append(toCsvInfo(occupation, qs, qe, se));
		b.append(toCsvInfo(color, qs, qe, se));
		b.append(toCsvInfo(Book.getIds(photos), qs, qe, se));
		return (super.toCsv(b.toString(), qs, qe, se));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlInfo("person.firstname", firstname));
		b.append(toHtmlInfo("person.lastname", lastname));
		b.append(toHtmlInfo("z.abbreviation", abbreviation));
		b.append(toHtmlInfo("gender", gender));
		b.append(toHtmlInfo("category", category));
		b.append(toHtmlInfo("categories", Book.getNames(categories)));
		b.append(toHtmlInfo("person.birthday", birthday));
		b.append(toHtmlInfo("person.deathday", dayofdeath));
		b.append(toHtmlInfo("person.occupation", occupation));
		b.append(toHtmlInfo("z.color", color));
		b.append(toHtmlInfo("photos", Book.getNames(photos)));
		return (super.toHtmlDetail(b.toString()));
	}

	@Override
	public String toXml() {
		StringBuilder b = new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3, "firstname", firstname));
		b.append(toXmlSetAttribute(0, "lastname", lastname));
		b.append(toXmlSetAttribute(0, "abbreviation", abbreviation));
		b.append(toXmlSetAttribute(0, "gender", gender)).append("\n");
		b.append(toXmlSetAttribute(3, "category", category));
		b.append(toXmlSetAttribute(0, "categories", categories)).append("\n");
		b.append(toXmlSetAttribute(3, "birthday", birthday));
		b.append(toXmlSetAttribute(0, "dayofdeath", dayofdeath));
		b.append(toXmlSetAttribute(0, "occupation", occupation)).append("\n");
		b.append(toXmlSetAttribute(3, "color", color)).append("\n");
		b.append(toXmlSetAttribute(3, "photos", photos));
		b.append(toXmlFooter());
		return (b.toString());
	}

	public static String toXml(List<Person> list) {
		StringBuilder b = new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return (b.toString());
	}

	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Person test = (Person) obj;
		boolean ret = true;
		ret = ret && equalsStringNullValue(abbreviation, test.abbreviation);
		ret = ret && equalsStringNullValue(firstname, test.firstname);
		ret = ret && equalsStringNullValue(lastname, test.lastname);
		ret = ret && equalsObjectNullValue(gender, test.gender);
		ret = ret && equalsObjectNullValue(category, test.category);
		ret = ret && equalsObjectNullValue(categories, test.categories);
		ret = ret && equalsObjectNullValue(birthday, test.birthday);
		ret = ret && equalsObjectNullValue(dayofdeath, test.dayofdeath);
		ret = ret && equalsIntegerNullValue(color, test.color);
		ret = ret && equalsObjectNullValue(photos, test.photos);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (abbreviation != null ? abbreviation.hashCode() : 0);
		hash = hash * 31 + (firstname != null ? firstname.hashCode() : 0);
		hash = hash * 31 + (lastname != null ? lastname.hashCode() : 0);
		hash = hash * 31 + (gender != null ? gender.hashCode() : 0);
		hash = hash * 31 + (birthday != null ? birthday.hashCode() : 0);
		hash = hash * 31 + (dayofdeath != null ? dayofdeath.hashCode() : 0);
		hash = hash * 31 + (color != null ? color.hashCode() : 0);
		hash = hash * 31 + (category != null ? category.hashCode() : 0);
		hash = hash * 31 + (categories != null ? getListHashCode(categories) : 0);
		hash = hash * 31 + (photos != null ? getListHashCode(photos) : 0);
		hash = hash * 31 + (desc != null ? desc.hashCode() : 0);
		hash = hash * 31 + (notes != null ? notes.hashCode() : 0);
		hash = hash * 31 + (assistant != null ? assistant.hashCode() : 0);
		return hash;
	}

	@Override
	public int compareTo(Person o) {
		return getFullName().compareTo(o.getFullName());
	}

	@Override
	public final void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		firstname=(attributeGetString(node, "firstname"));
		lastname=(attributeGetString(node, "lastname"));
		abbreviation=(attributeGetString(node, "abbreviation"));
		gender=(book.getGender(attributeGetLong(node, "gender")));
		category=(book.getCategory(attributeGetLong(node, "category")));
		categories=(book.getCategories(attributeGetLongs(node, "categories")));
		birthday=(new SbDate(attributeGetString(node, "birthday")));
		dayofdeath=(new SbDate(attributeGetString(node, "dayofdeath")));
		occupation=(attributeGetString(node, "occupation"));
		color=(attributeGetInteger(node, "color"));
		photos=(book.getPhotos(attributeGetLongs(node, "photos")));
	}

	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = super.xmlTo(xml, node);
		xml.attributeSetValue(n, "firstname", firstname);
		xml.attributeSetValue(n, "lastname", lastname);
		xml.attributeSetValue(n, "abbreviation", abbreviation);
		xml.attributeSetValue(n, "gender", gender);
		xml.attributeSetValue(n, "category", category);
		xml.attributeSetValue(n, "categories", Book.getIds(categories));
		xml.attributeSetValue(n, "birthday", birthday);
		xml.attributeSetValue(n, "lastname", dayofdeath);
		xml.attributeSetValue(n, "occupation", occupation);
		xml.attributeSetValue(n, "color", color);
		xml.attributeSetValue(n, "photos", Book.getIds(photos));
		return (n);
	}

	public static void xmlTo(XmlDB xml, List<Person> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static Person findByFullName(List<Person> list, String x) {
		if (!x.isEmpty()) {
			for (Person p:list) {
				if (p.getFullName().equals(x)) {
					return(p);
				}
			}
		}
		return(null);
	}

	public static List<Person> load(XmlDB xml, Book book) {
		NodeList nodes = xml.rootNode.getElementsByTagName(Book.TYPE.PERSON.toString());
		List<Person> list = new ArrayList<>();
		for (int i = 0; i < nodes.getLength(); i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Person c = new Person(book, nodes.item(i));
			list.add(c);
		}
		return (list);
	}

	public static List<Person> copy(List<Person> source) {
		List<Person> list2 = source.stream().collect(Collectors.toList());
		return (list2);
	}

	public static List<Person> copyByCategory(List<Person> source) {
		List<Person> list = source.stream().collect(Collectors.toList());
		list.sort((Person p2, Person p1) -> {
			if (p1.category.equals(p2.category)) {
				return p1.getFullName().compareTo(p2.getFullName());
			}
			return p1.category.compareTo(p2.category);
		});
		return (list);
	}

	public static List<Person> copyByName(List<Person> source) {
		List<Person> list = source.stream().collect(Collectors.toList());
		list.sort((Person p2, Person p1) -> p1.getFullName().compareTo(p2.getFullName()));
		return (list);
	}

	public String calculateAge(SbDate date) {
		SbDate d = SbDate.getToDay();
		Long l = Integer.toUnsignedLong(date.toSeconds()) - Integer.toUnsignedLong(SbDate.getToDay().toSeconds());
		return (date.getCalendar().getDuration(l));
	}

}
