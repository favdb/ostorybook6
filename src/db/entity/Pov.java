/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.ColorIcon;
import tools.html.HtmlUtil;

/**
 *
 * @author favdb
 */
public class Pov extends AbstractEntity implements Comparable<Pov> {

	private String abbr="";
	private Integer color=0;
	private Integer sort=0;

	public static Pov copy(Book book,Pov old, Long id) {
		Pov n=new Pov();
		n.name=old.name;
		n.sort=old.sort;
		n.abbr=old.abbr;
		n.color=old.color;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static Integer getNextSort(List<Pov> povs) {
		Integer r=0;
		for (Pov p:povs) {
			if (p.getSort()>r) {
				r=p.getSort();
			}
		}
		return(r+1);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"z.abbreviation","z.color","z.sort",
			"z.description","z.notes","assistant"};
		return(c);
	}

	public static List<Pov> sort(List<Pov> povs) {
		List<Pov> list=povs;
		list.sort((Pov m1, Pov m2) -> m2.getSort() - m1.getSort());
		return(list);
	}

	public static Pov findSort(List<Pov> povs, int n) {
		if (povs!=null) {
			for (Pov pov:povs) {
				if (pov.getSort().equals(n)) return(pov);
			}
		}
		return(null);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(abbr);
		cols.add(getJColor());
		cols.add(sort);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Pov() {
		super(Book.TYPE.POV);
	}


	public Pov(Long id, String name) {
		super(Book.TYPE.POV,id,name);
	}
	
	public Pov(Book book, Node node) {
		super(Book.TYPE.POV);
		xmlFrom(book, node);
	}

	public Pov(Long id, String name, String abbreviation, Integer color, Integer sort, String notes) {
		super(Book.TYPE.POV,id,name);
		this.abbr = abbreviation;
		this.color = color;
		this.sort = sort;
		this.notes = notes;
	}

	public String getAbbreviation() {
		return this.abbr;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbr = abbreviation;
	}

	public Integer getColor() {
		return this.color;
	}

	public Color getJColor() {
		if (color == null) {
			return null;
		}
		return new Color(color);
	}

	public String getHTMLColor() {
		return HtmlUtil.getHTMLName(getJColor());
	}

	public ColorIcon getColorIcon() {
		return new ColorIcon(getJColor(), 10, 20);
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public void setJColor(Color color) {
		if (color == null) {
			this.color = null;
			return;
		}
		this.color = color.getRGB();
	}

	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(getAbbreviation(),qs,qe,se));
		b.append(toCsvInfo(getClean(getColor()),qs,qe,se));
		b.append(toCsvInfo(getClean(getSort()),qs,qe,se));
		return(super.toCsv(b.toString(),qs, qe, se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("z.abbreviation", abbr));
		b.append(toHtmlInfo("z.color", color));
		b.append(toHtmlInfo("z.sort", sort));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"abbreviation", getClean(getAbbreviation())));
		b.append(toXmlSetAttribute(0,"color", getClean(getColor())));
		b.append(toXmlSetAttribute(0,"sort",getClean(getSort())));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Pov> list) {
		StringBuilder b=new StringBuilder();
		list.forEach((c)-> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Pov test = (Pov) obj;
		boolean ret = true;
		ret = ret && equalsStringNullValue(name, test.name);
		ret = ret && equalsStringNullValue(abbr, test.getAbbreviation());
		ret = ret && equalsIntegerNullValue(color, test.getColor());
		ret = ret && equalsIntegerNullValue(sort, test.getSort());
		ret = ret && equalsStringNullValue(notes, test.getNotes());
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (name != null ? name.hashCode() : 0);
		hash = hash * 31 + (abbr != null ? abbr.hashCode() : 0);
		hash = hash * 31 + (color != null ? color.hashCode() : 0);
		hash = hash * 31 + (sort != null ? sort.hashCode() : 0);
		hash = hash * 31 + (notes != null ? notes.hashCode() : 0);
		return hash;
	}

	@Override
	public int compareTo(Pov o) {
		if (sort == null && o == null) {
			return 0;
		}
		if (sort != null && o.getSort() == null) {
			return -1;
		}
		if (o.getSort() != null && sort == null) {
			return -1;
		}
		return sort.compareTo(o.getSort());
	}

	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		setAbbreviation(attributeGetString(node,"abbreviation"));
		setColor(attributeGetInteger(node,"color"));
		setSort(attributeGetInteger(node,"sort"));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"abbreviation",abbr);
		xml.attributeSetValue(n,"color",color);
		xml.attributeSetValue(n,"sort",sort);
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Pov> entities) {
		entities.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}
	
	public static List<Pov> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.POV.toString());
		List<Pov> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Pov c=new Pov(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

}
