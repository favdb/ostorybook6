/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * (c) 2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.I18N;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class AbsEntity extends AbsEntityFunction {
	public Long id=-1L;
	public Book.TYPE type=Book.TYPE.NONE;
	public SbDate creation=null, maj=null;
	public String name=I18N.getMsg("z.notitle"),
			desc="", notes="", assistant="";
	public enum DataType {
		TXT, TEXTAREA, INT, DATE, COLOR, ICON, IMAGE, BOOL, NONE
	}
	
	public AbsEntity(Book.TYPE type) {
		this.type=type;
	}
	
	public AbsEntity(Book.TYPE type, Long id, String name) {
		this(type);
		this.id=id;
		this.name=name;
	}
	
	public Icon getIcon() {
		return (IconUtil.getIcon(this.type.toString()));
	}

	public Icon getIcon(int h) {
		ImageIcon ic = (ImageIcon) IconUtil.getIcon(this.type.toString());
		return (IconUtil.resizeIcon(ic, new Dimension(h, h)));
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 31 + (name != null ? name.hashCode() : 0);
		hash = hash * 31 + (type != null ? type.hashCode() : 0);
		hash = hash * 31 + (creation != null ? creation.hashCode() : 0);
		hash = hash * 31 + (maj != null ? maj.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		boolean b=true;
		if (this == obj) {
			return(b);
		}
		if (obj == null || getClass() != obj.getClass()) {
			return(false);
		}
		final AbsEntity other = (AbsEntity) obj;
		String str=toCsv(), sobj=other.toCsv();
		return(str.equals(sobj));
	}

	public String toCsv() {
		StringBuilder b = new StringBuilder(toCsvHeader());
		b.append(toCsvFooter());
		return (b.toString());
	}
	
	public String toCsvValue(String value) {
		if (value==null) {
			return("\"\",");
		} else {
			return("\""+value+"\",");
		}
	}

	public String toCsvHeader() {
		StringBuilder b = new StringBuilder();
		b.append(toCsvValue(getClean(id)));
		b.append(toCsvValue(getClean(name)));
		b.append(toCsvValue(getClean(creation)));
		b.append(toCsvValue(getClean(maj)));
		b.append("\n");
		return (b.toString());
	}

	public String toCsvFooter() {
		StringBuilder b = new StringBuilder();
		b.append(toCsvValue(getClean(desc)));
		b.append(toCsvValue(getClean(notes)));
		b.append(toCsvValue(getClean(assistant)));
		b.append("\n");
		return (b.toString());
	}

	public String toHtml() {
		StringBuilder b = new StringBuilder(toHtmlHeader());
		b.append(toHtmlFooter());
		return (b.toString());
	}
	
	public String toHtmlValue(String key, String value) {
		StringBuilder b=new StringBuilder();
		b.append("<b>").append(I18N.getMsg(key)).append("</b>=");
		if (value==null) {
			b.append("\"\"");
		} else {
			b.append("\"").append(value).append("\"");
		}
		b.append("<br>\n");
		return(b.toString());
	}

	public String toHtmlHeader() {
		StringBuilder b = new StringBuilder();
		b.append("<h2>").append(this.type.toString()).append("</h2>\n");
		b.append(toHtmlValue("z.id",getClean(id)));
		b.append(toHtmlValue("z.name",getClean(name)));
		b.append(toHtmlValue("z.creation",getClean(creation)));
		b.append(toHtmlValue("z.maj",getClean(maj)));
		return (b.toString());
	}

	public String toHtmlFooter() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlValue("z.desc",getClean(desc)));
		b.append(toHtmlValue("z.notes",getClean(notes)));
		b.append(toHtmlValue("assistant",getClean(assistant)));
		return (b.toString());
	}
	
	private String toXmlIndent(int indent) {
		StringBuilder b=new StringBuilder();
		if (indent>0) {
			for (int i=0;i<indent;i++) b.append("    ");
		}
		return(b.toString());
	}
	public String toXml(int indent, String... value) {
		StringBuilder b = new StringBuilder(toXmlHeader(indent));
		if (value.length>0) {
			b.append(value[0]);
		}
		b.append(toXmlFooter(indent));
		return (b.toString());
	}
	
	public String toXmlValue(int indent, String key, String value) {
		StringBuilder b=new StringBuilder();
		if (indent>0) {
			b.append("\n").append(toXmlIndent(indent));
		}
		b.append(key).append("=");
		if (value==null) {
			b.append("\"\"");
		} else {
			b.append("\"").append(value).append("\"");
		}
		b.append(" ");
		return(b.toString());
	}

	public String toXmlChildValue(int indent, String key, String value) {
		StringBuilder b=new StringBuilder();
		b.append(toXmlIndent(indent)).append("<").append(key).append(">\n");
		if (value!=null) {
			String nohtml=value;
			if (nohtml.contains("<")) {
				nohtml="<![CDATA["+value+"]]>";
			}
			b.append(nohtml).append("\n");
		}
		b.append("</").append(key).append(">\n");
		return(b.toString());
	}

	public String toXmlHeader(int indent, String... v) {
		StringBuilder b = new StringBuilder(toXmlIndent(indent));
		b.append("<").append(this.type.toString()).append(" ");
		b.append(toXmlValue(0,"z.id",getClean(id)));
		b.append(toXmlValue(0,"z.name",getClean(name)));
		b.append(toXmlValue(0,"z.creation",getClean(creation)));
		b.append(toXmlValue(0,"z.maj",getClean(maj)));
		if (v.length>0) {
			b.append(v);
		}
		b.append(">\n");
		return (b.toString());
	}

	public String toXmlFooter(int indent) {
		StringBuilder b = new StringBuilder(toXmlIndent(indent));
		b.append(toXmlChildValue(indent+1,"z.desc",getClean(desc)));
		b.append(toXmlChildValue(indent+1,"z.notes",getClean(notes)));
		b.append(toXmlChildValue(indent+1,"assistant",getClean(assistant)));
		b.append(toXmlIndent(indent)).append("</").append(type.toString()).append(">\n");
		return (b.toString());
	}
	
	@Override
	public String toString() {
		return(getClean(name));
	}

}
