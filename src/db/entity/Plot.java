/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author favdb
 */
public class Plot extends AbstractEntity {

	public Category category=null;
	public String beacon="";

	public static Plot copy(Book book,Plot old, Long id) {
		Plot n=new Plot();
		n.name=old.name;
		n.category=old.category;
		n.beacon=old.beacon;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"category", "plot.beacon",
			"z.description","z.notes","assistant"};
		return(c);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(category);
		cols.add(beacon);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Plot() {
		super(Book.TYPE.PLOT);
	}

	public Plot(Long id, String name) {
		super(Book.TYPE.PLOT,id,name);
	}

	public Plot(Book book, Node node) {
		super(Book.TYPE.PLOT);
		xmlFrom(book, node);
	}

	public boolean hasCategory() {
		return(category!=null);
	}

	@Override
	public String toCsv(String qs,String qe, String se) {
		String b=toCsvInfo(category.name,qs,qe,se);
		b+=toCsvInfo(beacon,qs,qe,se);
		return(super.toCsv(b,qs, qe, se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b = new StringBuilder();
		b.append(toHtmlInfo("category",category));
		b.append(toHtmlInfo("category.beacon",beacon));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"category", category));
		b.append(toXmlSetAttribute(0,"beacon",beacon));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Plot> list) {
		StringBuilder b=new StringBuilder();
		list.forEach((c) -> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Plot test = (Plot) obj;
		boolean ret = true;
		ret = ret && equalsObjectNullValue(category, test.category);
		ret = ret && equalsObjectNullValue(beacon, test.beacon);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (category != null ? category.hashCode() : 0);
		hash = hash * 31 + (beacon != null ? beacon.hashCode() : 0);
		return hash;
	}

	@Override
	public final void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		category=(book.getCategory(xmlGetLong(node,"category")));
		beacon=(xmlGetString(node,"beacon"));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"category",category);
		xml.attributeSetValue(n,"beacon",beacon);
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Plot> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static List<Plot> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.PLOT.toString());
		List<Plot> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Plot c=new Plot(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

	public static List<Plot> copyByName(List<Plot> plots) {
		List<Plot> list = plots.stream().collect(Collectors.toList());
		list.sort((Plot p2, Plot p1) -> p1.name.compareTo(p2.name));
		return (list);
	}

}
