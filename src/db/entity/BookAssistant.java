/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import static db.entity.AbstractEntity.toXmlChild;
import db.XmlDB;
import org.w3c.dom.Node;
import static db.XmlDB.childGetNode;

/**
 *
 * @author favdb
 */
public class BookAssistant {

	private AssistantData step, polti;
	
	public BookAssistant() {
		step=new AssistantData();
		polti=new AssistantData();
	}
	
	public void setStep(String s) {
		step.choice=s;
	}
	
	public String getStep() {
		return(step.choice);
	}
		
	public void setStepNotes(String s) {
		step.notes=s;
	}
	
	public String getStepNotes() {
		return(step.notes);
	}
		
	public void setStep(String s, String n) {
		step.choice=s;
		step.notes=n;
	}
	
	public void setPolti(String s) {
		polti.choice=s;
	}
	
	public String getPolti() {
		return(polti.choice);
	}
		
	public void setPoltiNotes(String s) {
		polti.notes=s;
	}
	
	public String getPoltiNotes() {
		return(polti.notes);
	}
		
	public void setPolti(String s,String n) {
		polti.choice=s;
		polti.notes=n;
	}
	
	public boolean isEmpty() {
		return(step.choice.isEmpty() && step.notes.isEmpty() &&
				polti.choice.isEmpty() && polti.notes.isEmpty());
	}
	
	public void xmlTo(XmlDB xml, Node node) {
		Node nodeAss=childGetNode(node,"assistant");
		if (nodeAss!=null) {
			Node child=childGetNode(nodeAss,"step");
			if (child!=null) {
				step.xmlTo(xml,child);
			}
			child=childGetNode(nodeAss,"polti");
			if (child!=null) {
				polti.xmlTo(xml,child);
			}
		}
	}
	
	public void xmlFrom(XmlDB xml,Node node) {
		Node n=childGetNode(node,"step");
		step.xmlFrom(xml,node);
		polti.xmlFrom(xml,node);
	}

	void xmlTo(XmlDB xml) {
		Node node=childGetNode(xml.rootNode,"assistant");
		Node n=childGetNode(node,"step");
		step.xmlTo(xml, n);
		polti.xmlTo(xml, n);
	}
	
	@Override
	public String toString() {
		String s="";
		s+="step=\""+step.choice+"\"/n";
		s+="stepNotes=\""+step.notes+"\"/n";
		s+="polti=\""+polti.choice+"\"/n";
		s+="poltiNotes=\""+polti.notes+"\"/n";
		return(s);
	}

	public String toXml() {
		String sstr=step.toXml();
		String pstr=polti.toXml();
		String astr="";
		if (!sstr.isEmpty()) astr="\n"+sstr;
		if (!pstr.isEmpty()) astr="\n"+pstr;
		return(toXmlChild(2,"assistant",astr));
	}
	
	public static BookAssistant fromXml(XmlDB xml, Node node) {
		BookAssistant ass=new BookAssistant();
		Node n=xml.findNode(node, "assistant");
		if (n!=null) {
			Node child=xml.findNode(n, "step");
			if (child!=null) {
				ass.step=AssistantData.fromXml(xml,child);
			}
			child=xml.findNode(n, "polti");
			if (child!=null) {
				ass.polti=AssistantData.fromXml(xml, child);
			}
		}
		return(ass);
	}
	
}
