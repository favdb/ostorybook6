/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.*;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author favdb
 */
public class Relation extends AbstractEntity implements Comparable<Relation> {

	public Scene startScene=null;
	public Scene endScene=null;
	public List<Person> persons=new ArrayList<>();
	public List<Location> locations=new ArrayList<>();
	public List<Item> items=new ArrayList<>();
	public List<Tag> tags=new ArrayList<>();

	public static Relation copy(Book book,Relation old, Long id) {
		Relation n=new Relation();
		n.name=old.name;
		n.startScene=old.startScene;
		n.endScene=old.endScene;
		n.persons=old.persons;
		n.locations=old.locations;
		n.items=old.items;
		n.tags=old.tags;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"relation.start","relation.end","persons","locations","items","tags",
			"z.description","z.notes","assistant"};
		return(c);
	}

	public static List<Relation> findByLocation(List<Relation> relations, Location location) {
		List<Relation> list = new ArrayList<>();
		relations.stream().filter((s) -> (s.locations.contains(location))).forEachOrdered((s) -> {
			list.add(s);
		});
		return (list);
	}

	public static List<Relation> findByPerson(List<Relation> relations, Person person) {
		List<Relation> list = new ArrayList<>();
		relations.stream().filter((s) -> (s.persons.contains(person))).forEachOrdered((s) -> {
			list.add(s);
		});
		return (list);
	}
	
	public static List<Relation> findByTag(List<Relation> relations, Tag entity) {
		List<Relation> list = new ArrayList<>();
		relations.stream().filter((s) -> (s.tags.contains(entity))).forEachOrdered((s) -> {
			list.add(s);
		});
		return (list);
	}

	public static List<Relation> findByItem(List<Relation> relations, Item entity) {
		List<Relation> list = new ArrayList<>();
		relations.stream().filter((s) -> (s.items.contains(entity))).forEachOrdered((s) -> {
			list.add(s);
		});
		return (list);
	}

	public static List<Relation> findByScene(List<Relation> relations, Scene entity) {
		List<Relation> list = new ArrayList<>();
		relations.stream().filter((r) -> ((r.hasStartScene() && r.startScene.equals(entity))
				|| (r.hasEndScene() && r.endScene.equals(entity)))).forEachOrdered((r) -> {
					list.add(r);
		});
		return (list);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(startScene);
		cols.add(endScene);
		cols.add(persons);
		cols.add(locations);
		cols.add(items);
		cols.add(tags);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Relation() {
		super(Book.TYPE.RELATION);
	}

	public Relation(Long id, String name) {
		super(Book.TYPE.RELATION,id,name);
	}

	public Relation(Book book, Node node) {
		super(Book.TYPE.RELATION);
		xmlFrom(book, node);
	}

	public boolean hasStartScene() {
		return(this.startScene != null);
	}

	public boolean hasEndScene() {
		return(endScene != null);
	}

	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(startScene,qs,qe,se));
		b.append(toCsvInfo(endScene,qs,qe,se));
		b.append(toCsvInfo(Book.getIds(persons),qs,qe,se));
		b.append(toCsvInfo(Book.getIds(locations),qs,qe,se));
		b.append(toCsvInfo(Book.getIds(items),qs,qe,se));
		b.append(toCsvInfo(Book.getIds(tags),qs,qe,se));
		return(super.toCsv(b.toString(),qs, qe, se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("relation.start", startScene));
		b.append(toHtmlInfo("relation.end", endScene));
		b.append(toHtmlInfo("persons", Book.getNames(persons)));
		b.append(toHtmlInfo("locations", Book.getNames(locations)));
		b.append(toHtmlInfo("items", Book.getNames(items)));
		b.append(toHtmlInfo("tags", Book.getNames(tags)));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"startscene", startScene));
		b.append(toXmlSetAttribute(0,"endscene", endScene)).append("\n");
		b.append(toXmlSetAttribute(3,"persons",persons));
		b.append(toXmlSetAttribute(0,"locations",locations));
		b.append(toXmlSetAttribute(0,"items",items));
		b.append(toXmlSetAttribute(0,"tags",tags));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Relation> list) {
		StringBuilder b=new StringBuilder();
		list.forEach((c)-> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Relation test = (Relation) obj;
		boolean ret = true;
		ret = ret && equalsObjectNullValue(startScene, test.startScene);
		ret = ret && equalsObjectNullValue(endScene, test.endScene);
		ret = ret && equalsObjectNullValue(persons, test.persons);
		ret = ret && equalsObjectNullValue(locations, test.locations);
		ret = ret && equalsObjectNullValue(items, test.items);
		ret = ret && equalsObjectNullValue(tags, test.tags);
		ret = ret && equalsStringNullValue(notes, test.notes);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (name != null ? name.hashCode() : 0);
		hash = hash * 31 + (startScene != null ? startScene.hashCode() : 0);
		hash = hash * 31 + (endScene != null ? endScene.hashCode() : 0);
		hash = hash * 31 + (persons != null ? persons.hashCode() : 0);
		hash = hash * 31 + (locations != null ? locations.hashCode() : 0);
		hash = hash * 31 + (items != null ? items.hashCode() : 0);
		hash = hash * 31 + (tags != null ? tags.hashCode() : 0);
		hash = hash * 31 + (notes != null ? notes.hashCode() : 0);
		return hash;
	}

	@Override
	public int compareTo(Relation o) {
		return 0;
	}

	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		startScene=(book.getScene(attributeGetLong(node,"startscene")));
		endScene=(book.getScene(attributeGetLong(node,"endscene")));
		persons=(book.getPersons(attributeGetLongs(node,"persons")));
		locations=(book.getLocations(attributeGetLongs(node,"locations")));
		items=(book.getItems(attributeGetLongs(node,"items")));
		persons=(book.getPersons(attributeGetLongs(node,"persons")));
		tags=(book.getTags(attributeGetLongs(node,"tags")));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"startscene",startScene);
		xml.attributeSetValue(n,"endscene",endScene);
		xml.attributeSetValue(n,"persons",Book.getIds(persons));
		xml.attributeSetValue(n,"locations",Book.getIds(locations));
		xml.attributeSetValue(n,"items",Book.getIds(items));
		xml.attributeSetValue(n,"tags",Book.getIds(tags));
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Relation> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}
	
	public static List<Relation> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.RELATION.toString());
		List<Relation> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Relation c=new Relation(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

	public boolean hasPeriod() {
		return(!(getPeriod()==null));
	}

	public SbPeriod getPeriod() {
		if (startScene==null || endScene==null) return(null);
		if (!startScene.hasDate() || !endScene.hasDate()) return(null);
		return(new SbPeriod(startScene.date, endScene.date));
	}

}
