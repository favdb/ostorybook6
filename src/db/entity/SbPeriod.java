/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.I18N;

/**
 * Class pour gérer les périodes
 * 
 * @author favdb
 */
public class SbPeriod {
	public SbDate startDate;
	public SbDate endDate;

	/**
	 * Période :
	 * 
	 * @param start: date de début
	 * @param end : date de fin
	 */
	public SbPeriod(SbDate start, SbDate end) {
		this.startDate = start;
		this.endDate = end;
	}

	/**
	 * Calcul si la période en arguement recouvre la période
	 * @param periode: période à vérifier
	 * 
	 * @return : true si vrai
	 */
	public boolean isOverlapping(SbPeriod periode) {
		return this.getStartDate().compareTo(periode.getEndDate()) < 0
				&& this.getEndDate().compareTo(periode.getStartDate()) > 0;
	}

	/**
	 * Calcul si la date en argument est incluse dan la période
	 * 
	 * @param date
	 * @return : true si vrai
	 */
	public boolean isInside(SbDate date) {
		if (date.compareTo(startDate) == 0) {
			return true;
		}
		if (date.compareTo(endDate) == 0) {
			return true;
		}
		return date.after(startDate) && date.before(endDate);
	}

	/**
	 * Calcul si la périoe en arguement est égale à la période
	 * @param periode
	 * @return 
	 */
	@Override
	public boolean equals(Object periode) {
		if (!(periode instanceof SbPeriod)) {
			return false;
		}
		SbPeriod p = (SbPeriod) periode;
		return this.getStartDate().compareTo(p.getStartDate()) == 0
				&& this.getEndDate().compareTo(p.getEndDate()) == 0;
	}

	/**
	 * Calcul du Hash code de la période
	 * 
	 * @return le Hash 
	 */
	@Override
	public int hashCode() {
		int hash = 1;
		hash = hash * 31 + getStartDate().hashCode();
		hash = hash * 31 + getEndDate().hashCode();
		return hash;
	}

	/**
	 * retourne la date de début
	 * 
	 * @return 
	 */
	public SbDate getStartDate() {
		return this.startDate;
	}

	/**
	 * retourne la date de fin
	 * 
	 * @return 
	 */
	public SbDate getEndDate() {
		return this.endDate;
	}

	/**
	 * calcul si la date de début et la date de fin sont renseignées
	 * 
	 * @return 
	 */
	public boolean isValid() {
		return !(startDate == null || endDate == null);
	}

	/**
	 * Transformation en String de la période
	 * 
	 * @return 
	 */
	@Override
	public String toString() {
		if (!isValid()) {
			return I18N.getMsg("period.invalid");
		}
		String startStr = startDate.getDateTime();
		if (startDate.equals(endDate)) {
			return startStr;
		}
		String endStr = endDate.getDateTime();
		return startStr + " - " + endStr;
	}
	
}
