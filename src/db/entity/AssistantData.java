/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import static db.XmlDB.attributeGetString;
import static db.entity.AbstractEntity.toXmlChild;
import org.w3c.dom.Node;

/**
 *
 * @author favdb
 */
class AssistantData {

	public String choice="";
	public String notes="";
	
	public AssistantData() {
	}
	
	public void setChoice(String choice) {
		this.choice=choice;
	}
	
	public String getChoice() {
		return(choice);
	}
	
	public void setNotes(String notes) {
		this.notes=notes;
	}
	
	public String getNotes() {
		return(notes);
	}

	public void xmlTo(XmlDB xml, Node child) {
		xml.childContentSet(xml, child, "choice", choice);
		xml.childContentSet(xml, child, "notes", notes);
	}

	public void xmlFrom(XmlDB xml, Node node) {
		AssistantData d=new AssistantData();
		d.setChoice(attributeGetString(node,"choice"));
		d.setNotes(attributeGetString(node,"notes"));
	}

	public String toXml() {
		String r="";
		r+=toXmlChild(4,"choice",choice);
		r+=toXmlChild(4,"notes",notes);
		return(r);
	}
	
	public static AssistantData fromXml(XmlDB xml, Node child) {
		AssistantData d=new AssistantData();
		d.choice=XmlDB.childContentGet(child,"choice");
		d.notes=XmlDB.childContentGet(child,"notes");
		return(d);
	}

}
