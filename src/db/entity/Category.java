/*
 * oStorybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import db.I18N;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author favdb
 */
public class Category extends AbstractEntity {
	public String beacon="";
	public Integer sort=0;
	public Category sup=null;

	public enum BEACON {
		EVENT, IDEA, ITEM, PERSON, PLOT, TAG, NONE;
		private BEACON() {
		}
		@Override
		public String toString() {
			return this.name().toLowerCase();
		}
	}
	
	public static BEACON getBEACON(String str) {
		for (BEACON t:BEACON.values()) {
			if (str.equals(t.toString())) return(t);
		}
		return(BEACON.NONE);
	}
		
	public Category() {
		super(Book.TYPE.CATEGORY);
		sort=0;
	}

	public Category(Long id, String subtype, String name, Integer sort, Category sup) {
		super(Book.TYPE.CATEGORY,id,name);
		this.beacon=subtype;
		this.sort=sort;
		this.sup=sup;
	}

	public Category(Book book, Node n) {
		super(Book.TYPE.CATEGORY);
		xmlFrom(book, n);
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public static int getSortNext(List<Category> list) {
		Integer ret=0;
		for (Category c:list) {
			if (c.sort>ret) {
				ret=c.sort;
			}
		}
		return(ret + 1);
	}

    public boolean hasSup() {
        return(sup != null);
    }

	public static Category copy(Book book, Category old, Long id) {
		Category n=new Category();
		n.name=old.name;
		n.beacon=old.beacon;
		n.sort=old.sort;
		n.sup=old.sup;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static List<Category> findBeacon(List<Category> categories, String beacon) {
		List<Category> list=new ArrayList<>();
		categories.forEach((c)-> {
			if (c.beacon.equals(beacon)) {
				list.add(c);
			}
		});
		return(list);
	}

	public static Category findSort(List<Category> categories, int n) {
		for (Category c:categories) {
			if (c.sort==n) {
				return(c);
			}
		}
		return(null);
	}

	public static List<Category> findSup(List<Category> categories, Category entity) {
		List<Category> list=new ArrayList<>();
		categories.forEach((c)-> {
			if (c.hasSup() && c.sup.id.equals(entity.id)) {
				list.add(c);
			}
		});
		return(list);
	}

	public static  String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"category.beacon","z.sort","z.sup",
			"z.description","z.notes","assistant"};
		return(c);
	}

	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(beacon);
		cols.add(sort);
		cols.add(hasSup()?sup.name:"");
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(beacon,qs,qe,se));
		b.append(toCsvInfo(sort.toString(),qs,qe,se));
		b.append(toCsvInfo(getClean(sup),qs,qe,se));
		return(toCsv(b.toString(),qs,qe,se));
	}
	
	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("category.beacon",I18N.getMsg(beacon)));
		b.append(toHtmlInfo("z.sort",sort));
		if (hasSup()) b.append(toHtmlInfo("z.sup",sup.toString()));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"beacon", getClean(beacon)));
		b.append(toXmlSetAttribute(0,"sort", getClean(sort)));
		b.append(toXmlSetAttribute(0,"sup", sup));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Category> categories) {
		StringBuilder buff=new StringBuilder();
		categories.forEach((c)-> {
			buff.append(c.toXml());
		});
		return(buff.toString());
	}
	
	@Override
	public void xmlFrom(Book book, Node node) {
		super.xmlFrom(book,node);
		beacon=(xmlGetString(node,"beacon"));
		sort=(xmlGetInteger(node,"sort"));
		sup=(book.getCategory(xmlGetLong(node,"sup")));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n = super.xmlTo(xml, node);
		xml.attributeSetValue(n, "sort", Integer.toString(sort));
		xml.attributeSetValue(n, "beacon", beacon);
		xml.attributeSetValue(n, "sup", sup);
		return (n);
	}

	public static void xmlTo(XmlDB xml, List<Category> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Category)) {
			return(false);
		}
		if (!super.equals(obj)) {
			return false;
		}
		Category test = (Category) obj;
		boolean ret = true;
		ret = ret && sort.equals(test.sort);
		ret = ret && beacon.equals(test.beacon);
		ret = ret && name.equals(test.name);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + sort.hashCode();
		hash = hash * 31 + name.hashCode();
		return hash;
	}

	public int compareTo(Category ca) {
		if (hasSup() && (ca.hasSup()) && (!(sup.equals(ca.sup)))) {
			return(sup.compareTo(ca.sup));
		}
		return(sort.compareTo(ca.sort));
	}

	public static List<Category> load(XmlDB xml, Book book) {
		List<Category> list=new ArrayList<>();
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.CATEGORY.toString());
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Category c=new Category(book, nodes.item(i));
			list.add(c);
		}
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Node n=nodes.item(i);
			Long s=xmlGetLong(n,"sup");
			if (s!=null && s!=-1L) {
				for (Category c:list) {
					if (c.id.equals(s)) {
						list.get(i).sup=c;
					}
				}
			}
		}
		return(list);
	}

	public static List<Category> orderBySort(List<Category> categories) {
		categories.sort((Category s2, Category s1) -> s1.sort.compareTo(s2.sort));
		return (categories);
	}

}
