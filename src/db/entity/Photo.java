/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db.entity;

import db.XmlDB;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Icon;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import tools.IconUtil;

/**
 *
 * @author favdb
 */
public class Photo extends AbstractEntity {
	
	public String file="";
	
	public static Photo copy(Book book,Photo old, Long id) {
		Photo n=new Photo();
		n.name=old.name;
		n.file=old.file;
		n.desc=old.desc;
		n.notes=old.notes;
		n.id=id;
		return(n);
	}

	public static String[] getColumns() {
		String c[]={"z.id","z.creation","z.maj","z.name",
			"file",
			"z.description","z.notes","assistant"};
		return(c);
	}
	
	@Override
	public List<Object> getRow() {
		List<Object> cols = new ArrayList<>();
		cols.add(id);
		cols.add(creation);
		cols.add(maj);
		cols.add(name);
		cols.add(file);
		cols.add(desc);
		cols.add(notes);
		cols.add(assistant);
		return(cols);
	}

	public Photo() {
		super(Book.TYPE.PHOTO);
	}
	
	public Photo(Long id, String file) {
		super(Book.TYPE.PHOTO);
		this.id=id;
		this.file=file;
	}
	
	public Photo(Book book, Node node) {
		super(Book.TYPE.PHOTO);
		xmlFrom(book, node);
	}
	
	public Icon getImage(String path) {
		if (file==null || file.isEmpty()) {
			return(null);
		}
		return(IconUtil.getIconExternal(path+file, 16));
	}
	
	public Icon getImage(String path, int i) {
		if (file==null || file.isEmpty()) {
			return(null);
		}
		return(IconUtil.getIconExternal(path+file, i));
	}
		
	@Override
	public String toCsv(String qs,String qe, String se) {
		StringBuilder b=new StringBuilder();
		b.append(toCsvInfo(file,qs,qe,se));
		return(super.toCsv(b.toString(),qs, qe, se));
	}

	@Override
	public String toHtmlDetail() {
		StringBuilder b=new StringBuilder();
		b.append(toHtmlInfo("file",file));
		return(super.toHtmlDetail(b.toString()));
	}
	
	@Override
	public String toXml() {
		StringBuilder b=new StringBuilder();
		b.append(toXmlHeader());
		b.append(toXmlSetAttribute(3,"file", file));
		b.append(toXmlFooter());
		return(b.toString());
	}
	
	public static String toXml(List<Photo> photos) {
		StringBuilder b=new StringBuilder();
		photos.forEach((c)-> {
			b.append(c.toXml());
		});
		return(b.toString());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		Photo test = (Photo) obj;
		boolean ret = true;
		ret = ret && equalsStringNullValue(file, test.file);
		return ret;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		hash = hash * 31 + (file != null ? file.hashCode() : 0);
		return hash;
	}

	@Override
	public void xmlFrom(XmlDB xml, Book book, Node node) {
		super.xmlFrom(xml, book, node);
		file=(xmlGetString(node,"file"));
	}
	
	@Override
	public Node xmlTo(XmlDB xml, Node node) {
		Node n=super.xmlTo(xml,node);
		xml.attributeSetValue(n,"file",file);
		return(n);
	}

	public static void xmlTo(XmlDB xml, List<Photo> list) {
		list.forEach((c) -> {
			c.xmlTo(xml, xml.rootNode);
		});
	}

	public static List<Photo> load(XmlDB xml, Book book) {
		NodeList nodes=xml.rootNode.getElementsByTagName(Book.TYPE.PHOTO.toString());
		List<Photo> list=new ArrayList<>();
		for (int i=0;i<nodes.getLength();i++) {
			if (nodes.item(i).getParentNode()!=xml.rootNode) continue;
			Photo c=new Photo(book, nodes.item(i));
			list.add(c);
		}
		return(list);
	}

}
