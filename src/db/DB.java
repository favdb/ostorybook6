/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db;

import db.entity.Book;

/** Basic DB
 * usuable for test and date time format
 *
 * @author favdb
 */
public class DB {
	private static boolean dev=false, trace=false;
	private static DB instance;
	private FORMAT formatDate=new FORMAT("yyyy-MM-dd HH:mm:SS");
	public static String DB_VERSION = "6.0", DB_FILE_EXT = ".osbk";


	/**
	 * Main only for testing DB
	 * 
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		System.out.println("Book test Xml creation");
		Book book=new Book();
		book.init("test création");
		XmlDB xml=XmlDB.create("testXml", book, true);
		book.xmlFrom(xml);
		System.out.println("Book xml content");
		System.out.println(book.toXml());
	}

	/** for non static usage
	 * 
	 * @return an instance to a DB
	 */
	public static DB getInstance() {
		if (instance==null) {
			instance=new DB();
		}
		return(instance);
	}
	
	/**
	 * Sending error exception message to console
	 * 
	 * @param string : the message text
	 * @param ex : the exception
	 */
	public static void error(String string, Exception ex) {
		System.err.println(string);
		ex.printStackTrace(System.err);
	}

	/**
	 * Sending message to console
	 * 
	 * @param string : the message text to print
	 */
	public static void trace(String string) {
		if (trace) System.err.println(string);
	}

	/**
	 * Setting the dev mode
	 * 
	 */
	public void setDev() {
		System.out.println("Setting dev mode");
		dev=true;
	}
	
	public void resetDev() {
		System.out.println("Resetting dev mode");
		dev=false;
	}
	
	/**
	 * test if in dev mode
	 * 
	 * @return true if in dev mode
	 */
	public boolean isDev() {
		return(dev);
	}
	
	/**
	 * entering in trace mode
	 */
	public void setTrace() {
		System.out.println("DB enter trace mode");
		trace=true;
	}
	
	/**
	 * ending trace mode
	 */
	public void resetTrace() {
		System.out.println("DB exit trace mode");
		trace=false;
	}
	
	/**
	 * check if in trace mode
	 * @return true if in trace mode
	 */
	public boolean isTrace() {
		return(trace);
	}
	
	/**
	 * Setting the Date and time format
	 * @param date : like yyyy-MM-dd
	 * @param time : like HH:mm:SS
	 */
	public void setFormatDateTime(String date, String time) {
		formatDate=new FORMAT(date,time);
	}

	/**
	 * Getting the Date and time format
	 * @return the corresponding string
	 */
	public String getFormatDateTime() {
		return(formatDate.getDateTime());
	}
	
	/**
	 * Setting the Date format
	 */
	public void setFormatDate(String str) {
		formatDate.setDate(str);
	}
	
	public String getFormatDate() {
		return(formatDate.getDate());
	}
	
	/**
	 * Setting the Time format
	 * @param time : like HH:mm:SS
	 */
	public void setFormatTime(String time) {
		formatDate.setTime(time);
	}
	
	public String getFormatTime() {
		return(formatDate.getTime());
	}

	/**
	 * The Format class describe the date and time string format
	 */
	private static class FORMAT {
		String date;
		String time;

		public FORMAT(String date, String time) {
			this.date=date;
			this.time=time;
		}
		public FORMAT(String format) {
			String s[]=format.split(" ");
			this.date=s[0];
			this.time=s[1];
		}
		
		public String getDate() {return(date);}
		public void setDate(String str) {date=str;}
		public String getTime() {return(time);}
		public void setTime(String str) {time=str;}
		public String getDateTime() {return(date+" "+time);}
		public void setDateTime(String date, String time) { this.date=date; this.time=time;}
	}
	
}
