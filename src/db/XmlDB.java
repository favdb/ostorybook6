/*
 * oStorybook: Libre software for novelists and authors.
 * Original idea Martin Mustun
 * Copyright (C) 2013 - 2020 The oStorybook Team
 * 
 * This program is libre software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db;

import static db.entity.AbstractEntity.getClean;
import db.entity.Book;
import db.entity.SbDate;
import db.entity.AbstractEntity;
import java.awt.Dimension;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tools.FileTool;
import tools.ListUtil;
import tools.StringUtil;

/**
 *
 * @author FaVdB
 */
public class XmlDB {
	
	public static String getTitle(String val) {
		XmlDB xml = new XmlDB(val);
		if (xml.open()==false) return(null);
		Node n=xml.findNode("info");
		String t=attributeGetString(n,"title");
		xml.close();
		return(t);
		/*Book book = new Book();
		book.xmlFrom(xml);
		xml.close();
		return (book.info.getTitle());*/
	}
	
	public String name;
	private final File file;
	private boolean opened = false;
	private boolean modified = false;
	public Document document;
	public Element rootNode;
	public DocumentBuilder documentBuilder;
	private static boolean btrace = false;
	
	public XmlDB(String path) {
		name = path;
		file = new File(path);
	}
	
	public XmlDB(File f) {
		name = f.getAbsolutePath();
		file = f;
	}
	
	private static void error(String msg, Exception ex) {
		System.err.println(msg);
		ex.printStackTrace(System.err);
	}
	
	private void init(Book book) {
		if (document == null) {
			try {
				documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				document = documentBuilder.newDocument();
			} catch (ParserConfigurationException ex) {
				error("xml.err.create", ex);
			}
		}
		rootNode = document.createElement("book");
		document.appendChild(rootNode);
		if (book != null) {
			book.xmlFrom(this);
		}
	}
	
	public boolean isOK() {
		boolean rc = true;
		File f = new File(name);
		// file doesn't exist
		if (!f.exists()) {
			String txt = I18N.getMsg(DB.class, "file.notexists", f.getPath());
			JOptionPane.showMessageDialog(null, txt,
					I18N.getMsg(DB.class, "z.warning"),
					JOptionPane.ERROR_MESSAGE);
			rc = false;
		}
		// file is read-only
		if (!f.canWrite()) {
			String txt = I18N.getMsg(DB.class, "xml.readonly", f.getPath());
			JOptionPane.showMessageDialog(null, txt,
					I18N.getMsg(DB.class, "z.warning"),
					JOptionPane.ERROR_MESSAGE);
			rc = false;
		}
		return (rc);
	}
	
	public boolean isOpened() {
		return (opened);
	}
	
	public static XmlDB create(String fileName, Book book, boolean b) {
		//App.trace("XmlDB.create(fileName="+fileName+", title="+title+", b="+(b?"true":false));
		XmlDB.btrace = b;
		if (fileName == null || fileName.isEmpty()) {
			return (null);
		}
		String fname = fileName;
		if (!fileName.endsWith(".osbk")) {
			fname = fileName + ".osbk";
		}
		XmlDB xml = new XmlDB(fname);
		xml.init(null);
		book.xmlTo(xml);
		xml.save(true);
		xml.close();
		return (xml);
	}
	
	public static boolean rename(String source, String dest) {
		//App.trace("XmlDB.rename(source="+source+", dest="+dest);
		if (source == null || source.isEmpty()
				|| dest == null || dest.isEmpty()) {
			return (false);
		}
		File inf = new File(source);
		File ouf = new File(dest);
		if (inf.exists() && !ouf.exists()) {
			inf.renameTo(ouf);
		}
		return (true);
	}
	
	public static boolean delete(String source) {
		//App.trace("XmlDB.delete(source="+source);
		if (source == null || source.isEmpty()) {
			return (false);
		}
		File inf = new File(source);
		return (inf.delete());
	}
	
	public static boolean copy(String source, String dest) {
		if (source == null || source.isEmpty()
				|| dest == null || dest.isEmpty()) {
			return (false);
		}
		File inf = new File(source);
		if (inf.exists()) {
			InputStream is;
			OutputStream os;
			try {
				is = new FileInputStream(source);
				os = new FileOutputStream(dest);
				byte[] buffer = new byte[1024];
				int length;
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
				is.close();
				os.close();
			} catch (FileNotFoundException ex) {
				error("file " + source + " not found", ex);
				return (false);
			} catch (IOException ex) {
				error("unable to copy file " + source + " to " + dest, ex);
				return (false);
			}
		}
		return (true);
	}
	
	public boolean open() {
		//App.trace("Xml.open()"+" open file "+name);
		if (isOpened()) return(true);
		documentBuilder = null;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			error("DocumentBuilder error: ", ex);
			return (false);
		}
		document = readDom();
		if (document == null) {
			rootNode = null;
			return (false);
		}
		rootNode = document.getDocumentElement();
		opened = true;
		resetModified();
		return (true);
	}
	
	public void close() {
		if (opened) {
			opened = false;
			resetModified();
			document = null;
			documentBuilder = null;
		}
	}
	
	public Document readDom() {
		//App.trace("Xml.readDom()");
		Document rc = null;
		try {
			rc = documentBuilder.parse(new File(name));
		} catch (IOException | SAXException e) {
			error("XmlDB.readDom() Parsing error for " + name, e);
		}
		return (rc);
	}
	
	public Node findNode(String typeobj) {
		NodeList nodes = rootNode.getElementsByTagName(typeobj);
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!n.getParentNode().equals(rootNode)) {
				continue;
			}
			if (n.getNodeName().equals(typeobj)) {
				return (n);
			}
		}
		return (null);
	}
	
	public Node findNode(Node node, String name) {
		NodeList nodes = node.getChildNodes();
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!n.getParentNode().equals(node)) {
				continue;
			}
			if (n.getNodeName().equals(name)) {
				return (n);
			}
		}
		return (null);
	}
	
	public Node findNode(String tobj, String name) {
		NodeList nodes = document.getElementsByTagName(tobj);
		try {
			if (nodes.getLength() == 0) {
				return (null);
			}
		} catch (NullPointerException ex) {
			return (null);
		}
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			if (!n.getParentNode().equals(rootNode)) {
				continue;
			}
			if (n.getNodeName().equals(tobj)) {
				String x = attributeGetString(n, "name");
				if (x.equals(name)) {
					return (n);
				}
			}
		}
		return (null);
	}
	
	public Node findNode(String tobj, Long id) {
		NodeList nodes = rootNode.getElementsByTagName(tobj);
		for (int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			long x = XmlDB.attributeGetLong(n, "id");
			if (!n.getParentNode().equals(rootNode)) {
				continue;
			}
			if (n.getNodeName().equals(tobj)) {
				if (x == id) {
					return (n);
				}
			}
		}
		return (null);
	}
	
	public boolean reformat(String str) {
		close();
		try {
			FileWriter fw;
			fw = new FileWriter(name,false);
			try (BufferedWriter bw = new BufferedWriter(fw)) {
				bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n");
				bw.write(str);
				bw.flush();
				bw.close();
			}
		} catch (IOException ex) {
			error("XmlDB.reformat(str="+str+") error", ex);
			return(false);
		}
		open();
		return (true);
	}

	/** enregistrement du fichier XML en cours
	 * 
	 * @param force : vrai si l'enregistrement est à faire même si le fichier n'a pas été modifié,
	 * sinon si le fichier n'a pas été modifié on ne fait rien.
	 * 
	 * @return vrai si l'opération s'est correctement déroulée.
	 */
	public boolean save(boolean force) {
		DB.trace("XmlDB.save(force="+(force?"true":"false")+")");
		if (!force && !isModified()) {
			return(true);
		}
		try {
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new File(name));
			transformer.transform(source, result);
		} catch (TransformerConfigurationException ex) {
			error("Xml.save() exception", ex);
			return (false);
		} catch (TransformerException ex) {
			error("Xml.save() exception", ex);
			return (false);
		}
		resetModified();
		return (true);
	}
	
	/** marquer le fichier XML comme modifié
	 * 
	 */
	public void setModified() {
		modified = true;
	}
	
	/** réinitialisation de l'indicateur de modification du fichier XML
	 * 
	 */
	public void resetModified() {
		modified = false;
	}
	
	/** le fichier XML a-t-il été modifié
	 * 
	 * @return 
	 */
	public boolean isModified() {
		return (modified);
	}
	
	/** attribuer une valeur String à un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param val : valeur
	 */
	public static void attributeSet(Node node, String attribute, String val) {
		if (node == null || attribute == null || attribute.isEmpty()) {
			return;
		}
		Element e = (Element) node;
		if (e.getAttribute(attribute) != null) {
			if (val != null && !val.isEmpty()) {
				e.setAttribute(attribute, val);
			} else {
				e.removeAttribute(attribute);
			}
			return;
		}
		if (val == null || val.isEmpty()) {
			return;
		}
		Node nv = node.getAttributes().getNamedItem(attribute);
		if (nv != null) {
			nv.setNodeValue(val);
		}
	}
	
	/** attribuer une valeur Long à un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param val : valeur
	 */
	public static void attributeSet(Node node, String attribute, Long val) {
		if (val != null) {
			attributeSet(node, attribute, Long.toString(val));
		} else {
			attributeRemove(node, attribute);
		}
	}
	
	/** attribuer une valeur Integer à un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param val : valeur
	 */
	public static void attributeSet(Node node, String attribute, Integer val) {
		if (val != null) {
			attributeSet(node, attribute, val.toString());
		} else {
			attributeRemove(node, attribute);
		}
	}
	
	/** attribuer une valeur Boolean à un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param val : valeur
	 */
	public static void attributeSet(Node node, String attribute, Boolean val) {
		if (val != null) {
			attributeSet(node, attribute, val.toString());
		} else {
			attributeRemove(node, attribute);
		}
	}
	
	/** attribuer une valeur AbstractEntity dont on prend le nom à un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param val : valeur
	 * 
	 * note: si val est null alors l'attribut est supprimé
	 */
	public static void attributeSet(Node node, String attribute, AbstractEntity val) {
		if (val == null) {
			attributeSet(node, attribute, "");
		} else {
			attributeSet(node, attribute, val.name);
		}
	}
	
	/** attribuer une valeur SbDate, sous forme d'un String à un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param val : valeur
	 * 
	 * note: si val est null alors l'attribut est supprimé
	 */
	public static void attributeSet(Node node, String attribute, SbDate val) {
		if (val != null) {
			attributeSet(node, attribute, val.getDateTime());
		} else {
			attributeRemove(node, attribute);
		}
	}
	
	/** suppression d'un attribut s'il existe
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 */
	public static void attributeRemove(Node node, String attribute) {
		Element e = (Element) node;
		if (e == null) {
			return;
		}
		if (e.getAttribute(attribute) != null) {
			e.removeAttribute(attribute);
		}
	}
	
	/** récupération des données d'un noeud enfant
	 * @param node	: noeud parent
	 * @param n : nom du noeud enfant
	 * @return : une chaine de caractère
	**/
	public static String childContentGet(Node node, String n) {
		NodeList t = node.getChildNodes();
		if (t.getLength() > 0) {
			for (int i = 0; i < t.getLength(); i++) {
				if (t.item(i).getNodeName().equals(n)) {
					return (t.item(i).getTextContent().trim());
				}
			}
		}
		return ("");
	}
	
	/** enregistrement des données d'un noeud enfant
	 * règles : 
	 *  - si la valeur est vide ou null et que le noeud enfant existe il est supprimé
	 *  - sinon on ne fait rien
	 *  - si le noeud enfant n'existe pas il est créé
	 * @param doc : document XML
	 * @param node : noeud parent
	 * @param key : nom du neoud enfant, forcé en lowercase
	 * @param val : valeur à enregistrer
	 */
	public static void childContentSet(Document doc, Node node, String key, String val) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(key.toLowerCase())) {
				if (val == null || val.isEmpty()) {
					node.removeChild(n);
				} else {
					n.setNodeValue(val);
				}
				return;
			}
		}
		if (val == null || val.isEmpty()) {
			return;
		}
		Element e = doc.createElement(key.toLowerCase());
		e.setNodeValue(val);
		node.appendChild(e);
	}
	
	/** récupération d'une valeur booléenne d'un attribut
	 * 
	 * @param node
	 * @param attribut
	 * @return la valeur booléenne
	 * si le noeud n'est pas trouvé retourne faux
	 */
	public static boolean attributeGetBoolean(Node node, String attribut) {
		Element e = (Element) node;
		if (e == null) {
			return (false);
		}
		boolean b="1".equals(e.getAttribute(attribut.toLowerCase()).trim()) ||
				"true".equals(e.getAttribute(attribut.toLowerCase()).trim());
		return (b);
	}
	
	/** récupération d'une valeur SbDate d'un attribut
	 * 
	 * @param node
	 * @param attribut
	 * @return la valeur SbDate
	 * si le noeud n'est pas trouvé retourne null
	 */
	public static SbDate attributeGetDate(Node node, String attribut) {
		Element e = (Element) node;
		if (e == null) {
			return(null);
		}
		String val = ((Element)node).getAttribute(attribut);
		return (new SbDate(val));
	}
	
	/** récupération d'une valeur Integer d'un attribut ou la valeur par défaut en cas d'absence
	 * 
	 * @param node : noeud
	 * @param attribute : attribut
	 * @param def : valeur par défaut en cas d'absence du noeud, facultatif
	 * @return la valeur Integer ou la valeur par défaut en cas d'absence
	 */
	public static Integer attributeGetInteger(Node node, String attribute, int... def) {
		String x = attributeGetString(node, attribute);
		if (x.isEmpty() || "null".equals(x)) {
			if (def!=null && def.length>0) {
				return(def[0]);
			}
			return (0);
		}
		return (Integer.parseInt(x));
	}

	/** retourne une liste de String
	 * 
	 * @param node : noeud
	 * @param attribute : nom de l'attribut
	 * @return 
	 */
	public static List<String> attributeGetList(Node node, String attribute) {
		String str = attributeGetString(node, attribute);
		List<String> list = new ArrayList<>(Arrays.asList(str.split(",")));
		return (list);
	}
	
	/** retourne une valeur Long d'un attribut avec une valeur par défaut en cas d'absence
	 * 
	 * @param node : noeud
	 * @param attribute : nom de l'attribut
	 * @param def : valeur par défaut en cas d'absence, facultatif
	 * @return la valeur Long de l'attributou la valeur par défaut
	 */
	public static Long attributeGetLong(Node node, String attribute, long... def) {
		String x = attributeGetString(node, attribute);
		if (x.isEmpty() || "null".equals(x) || !StringUtil.isLong(x)) {
			if (def!=null && def.length>0) {
				return (def[0]);
			} return(-1L);
		}
		return (Long.parseLong(x));
	}
	
	/** récupération d'une liste de valeurs Long d'un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : string list attribute
	 * @return la liste des valeurs, ou une liste vide en cas d'absence du noeud
	 */
	public static List<Long> attributeGetLongs(Node node, String attribute) {
		String x = attributeGetString(node, attribute);
		//App.trace("XmlDB.attributeGetLongs(node="+node.getNodeName()+",attribute="+attribute+"::"+x+")");
		if (x.isEmpty() || "null".equals(x) || !StringUtil.isLongs(x)) {
			return(new ArrayList<>());
		}
		return (ListUtil.toLongs(x));
	}
	
	/** retourne le String d'un attribut
	 * 
	 * @param node : noeud
	 * @param attribut : attribut
	 * @param def : valeur par défaut en cas d'absence du noeud, facultatif
	 * @return 
	 */
	public static String attributeGetString(Node node, String attribut, String... def) {
		String val="";
		if (def!=null && def.length>0) val=def[0];
		if (node != null) {
			val = ((Element)node).getAttribute(attribut);
		}
		return(val.trim());
	}
	
	/** enregistrement de la valeur d'un attribut
	 * 
	 * @param node : noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur à enregistrer
	 * 
	 * Note : si le noeud n'existe pas il est créé
	 */
	public void attributeSetValue(Node node, String attribute, String val) {
		Node n = node.getAttributes().getNamedItem(attribute);
		String cleanval = getClean(val);
		if (n != null) {
			n.setNodeValue(cleanval);
		} else {
			Element e = (Element) node;
			e.setAttribute(attribute, cleanval);
		}
	}
	
	/** enregistrement de la valeur Long d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 */
	public void attributeSetValue(Node node, String attribute, Long val) {
		String cleanval = getClean(val);
		attributeSetValue(node, attribute, cleanval);
	}
	
	/** enregistrement d'une liste de valeurs Long d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : liste de valeurs Long
	 */
	public void attributeSetValue(Node node, String attribute, List<Long> val) {
		String cleanval = ListUtil.fromLongs(val);
		attributeSetValue(node, attribute, cleanval);
	}
	
	/** enregistrement d'une valeur int d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 */
	public void attributeSetValue(Node node, String attribute, int val) {
		String cleanval = getClean(val);
		attributeSetValue(node, attribute, cleanval);
	}
	
	/** enregistrement d'une valeur boolean d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 */
	public void attributeSetValue(Node node, String attribute, boolean val) {
		attributeSetValue(node, attribute, (val ? "1" : "0"));
	}
	
	/** enregistrement de la valeur Dimension d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 */
	public void attributeSetValue(Node node, String attribute, Dimension val) {
		attributeSetValue(node, attribute, String.format("%d,%d", val.height, val.width));
	}
	
	/** enregistrement de la valeur Point d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 */
	public void attributeSetValue(Node node, String attribute, Point val) {
		attributeSetValue(node, attribute, String.format("%d,%d", val.x, val.y));
	}
	
	/** enregistrement de la valeur de l'Id d'une entité d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 * 
	 * Note : si l'entité est nulle la valeur enregistrée sera une chaine vide
	 */
	public void attributeSetValue(Node node, String attribute, AbstractEntity val) {
		if (val == null) {
			attributeSetValue(node, attribute, "");
		} else {
			attributeSetValue(node, attribute, val.id.toString());
		}
	}
	
	/** enregistrement de la valeur d'une date d'un attribut
	 * 
	 * @param node : nom du noeud
	 * @param attribute : nom de l'attribut
	 * @param val : valeur
	 */
	public void attributeSetValue(Node node, String attribute, SbDate val) {
		if (val == null) {
			attributeSetValue(node, attribute, "");
		} else {
			attributeSetValue(node, attribute, val.getDateTime());
		}
	}
	
	/** enregistrement de la valeur dans un noeud enfant
	 * 
	 * @param xml : XmlDB file
	 * @param node : nom du noeud parent
	 * @param child : nom du noeud enfant
	 * @param val : valeur, si val est vide et que le noeud enfant existe il sera supprimé
	 */
	public void childContentSet(XmlDB xml, Node node, String child, String val) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(child)) {
				if (val==null || val.isEmpty()) node.removeChild(n);
				else n.setNodeValue(val);
				return;
			}
		}
		Element e = xml.document.createElement(child);
		e.setNodeValue(val);
		node.appendChild(e);
	}
	
	/** récupération d'une valeur d'un noeud enfant
	 * 
	 * @param node : noeud parent
	 * @param child : nom du noeud enfant
	 * @return : le noeud enfant
	 * 
	 * Note : si le noeud enfant n'existe pas il est créé
	 */
	public static Node childGetNode(Node node, String child) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(child)) {
				return (n);
			}
		}
		Node n = node.getOwnerDocument().createElement(child);
		node.appendChild(n);
		return (n);
	}
	
	/** récupération d'une valeur d'un noeud enfant existant
	 * 
	 * @param node : nom du noeud parent
	 * @param child : nom du noeud enfant
	 * @return : le noeud enfant
	 * 
	 * Note : si le noeud enfant n'existe pas retourne null
	 */
	public static Node childGetExisting(Node node, String child) {
		NodeList childs = node.getChildNodes();
		for (int i = 0; i < childs.getLength(); i++) {
			Node n = childs.item(i);
			if (n.getNodeName().equals(child)) {
				return (n);
			}
		}
		return (null);
	}
	
	/** retourn l'objet File du fichier XML ouvert
	 * 
	 * @return 
	 */
	public File getFile() {
		return (file);
	}
	
	/** retourne le chemin d'accès au fichier XML ouvert
	 * 
	 * @return 
	 */
	public String getPath() {
		return (file.getPath());
	}
	
	public String getOnlyPath() {
		String str=file.getPath();
		str=str.substring(0, str.lastIndexOf(File.separator));
		return (str);
	}
	
	/** conversion en chaîne de caractère de la date du jour
	 * 
	 * @return 
	 */
	public static String getDateString() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		return (formatter.format(new Date()));
	}
	
	/** faire une sauvegarde du fichier en cours
	 * 
	 * @param toFilename : nom du fichier cible
	 * @param incremental : sauvegarde incrémentale ou non
	 */
	public void doBackup(String toFilename, boolean incremental) {
		String backup = toFilename;
		if (incremental) {
			backup += "_" + getDateString();
		}
		backup += ".backup";
		File toFile = new File(backup);
		try {
			FileTool.copyFile(getFile(), toFile);
		} catch (IOException ex) {
			DB.error("Error XmlDB.doBackup to " + backup, ex);
		}
		System.out.println("Backup file to " + backup);
	}
	
	/** faire la restauration d'un fichier sauvegardé
	 * 
	 * @param file 
	 */
	public void doRestore(File file) {
		try {
			FileTool.copyFile(file, getFile());
		} catch (IOException ex) {
			DB.error("Error XmlDB.doRestore from " + file.getAbsolutePath(), ex);
		}
	}

	/** suppression d'un noeud correspondant à une entité
	 * 
	 * @param entity 
	 */
	public void removeNode(AbstractEntity entity) {
		Node node=findNode(entity.type.toString(), entity.id);
		if (node!=null) {
			rootNode.removeChild(node);
		}
		setModified();
	}
	
	public void removeNode(String str) {
		Node node=findNode(str);
		if (node!=null) {
			rootNode.removeChild(node);
		}
		setModified();
	}
	
}
