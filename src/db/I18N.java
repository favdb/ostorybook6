/*
 * Storybook: Open Source software for novelists and authors.
 * Original idea 2008-2012 Martin Mustun
 * Copyright 2013-2020 the oStorybook Team
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package db;

import db.DB;
import db.entity.SbDate;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.UIManager;

/**
 *
 * @author favdb
 */
public class I18N {
	public final static String TIME_FORMAT = "HH:mm:ss";
	private static String msgType;//0=not init, i=internal, x=external, f=file
	private static ResourceBundle messageBundle = null;
	private static Properties propMsg;
	private static Properties propFile;
	private static String propMsgName;
	private static String propFileName;

	public static final void initMessages(Locale locale) {
		msgType="0";
		initMsgInternal(locale);
		initMsgProp();
		if (propFileName!=null) initMsgFile();
	}
	
	public static void setFileMessages(String nf) {
		System.out.println("setFileMessages="+nf);
		File file=new File(nf);
		if (file.exists()) {
			propFileName=nf;
			msgType="f";
		} else {
			propFileName=null;
			msgType="i";
			initMsgProp();
		}
	}
	
	// an external properties file somewhere
	public static final void initMsgFile() {
		propMsg = null;
		if (propFileName==null) {
			return;
		}
		propMsg = new Properties();
		InputStream input;
		try {
			input = new FileInputStream(propFileName);
			propMsg.load(input);
			input.close();
		} catch (IOException ex) {
			System.err.println("messages file error ");
		}
	}

	// an external file inside the install sub-directory resources/i18n
	public static final Properties initMsgProp() {
		if (propMsg == null) {
			try {
				propMsg = new Properties();
				InputStream input;
				// messages localized
				String dir=System.getProperty("user.dir")+File.separator+"msg";
				propMsgName=dir+File.separator+"messages_"+Locale.getDefault().getLanguage()+".properties";
				File file=new File(propMsgName);
				if (!file.exists()) {
					propMsgName=dir+File.separator+"messages.properties";
					file=new File(propMsgName);
					if (!file.exists()) {
						propMsgName="";
						propMsg=null;
						return(null);
					}
				}
				input = new FileInputStream(propMsgName);
				propMsg.load(input);
				input.close();
				msgType="x";
				return (propMsg);
			} catch (IOException ex) {
				System.out.println("external resources missing");
				propMsg=null;
				propMsgName="";
				return (null);
			}
		}
		return(propMsg);
	}

	public static final Properties getExternalResources() {
		if (propMsg == null) {
			initMsgFile();
		}
		return propMsg;
	}

	public static String getMsgExternal(Class<?> c,String key) {
		try {
			String r=propMsg.getProperty(key);
			if (r==null) {
				r=getMsgInternal(c,key);
			}
			return(r);
		} catch (Exception ex) {
			return(getMsgInternal(c,key));
		}
	}

	public static String getMsgInternal(Class<?> c, String key) {
		ResourceBundle rb = getResourceBundle(c);
		try {
			return(rb.getString(key));
		} catch (Exception ex) {
			return '!' + key + '!';
		}
	}

	public static final void initMsgInternal(Locale locale) {
		ResourceBundle.clearCache();
		messageBundle = null;
		Locale.setDefault(locale);
		UIManager.getDefaults().setDefaultLocale(locale);
	}
	
	public static final ResourceBundle getResourceBundle(Class<?> c) {
		if (messageBundle == null) {
			String p="msg.messages";
			messageBundle = ResourceBundle.getBundle(p, Locale.getDefault());
		}
		return messageBundle;
	}

	public static char getMnemonic(String key) {
		String s = getMsg(key + ".mnemonic");
		if (s != null && s.length() > 0) {
			return s.charAt(0);
		}
		return '!';
	}

	public static String getMsg(Class<?> c, String key) {
		String r;
		if (msgType==null) msgType="0";
		switch(msgType) {
			case "f":
			case "x":
				r=getMsgExternal(c,key);
				break;
			default:
				r=getMsgInternal(c,key);
				break;
		}
		return(r);
	}
	
	public static String getMsg(Class<?> c,String key, Object[] args) {
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(Locale.getDefault());
		String pattern = getMsg(c,key);
		formatter.applyPattern(pattern);
		return formatter.format(args);
	}

	public static String getMsg(Class<?> c,String key, Object arg) {
		Object[] args = new Object[]{arg};
		return getMsg(c,key, args);
	}

	public static String getMsg(String key) {
		return(I18N.getMsg(I18N.class, key));
	}

	public static String getMsg(String key, Object [] args) {
		return(I18N.getMsg(I18N.class, key,args));
	}
	
	public static String getMsg(String key, Object arg) {
		return(I18N.getMsg(I18N.class, key,arg));
	}
	
	public static String getLanguage(String key) {
		//App.trace("I18N.getLanguage("+key+")");
		ResourceBundle rb = ResourceBundle.getBundle("msg.language", Locale.getDefault());
		try {
			return rb.getString(key);
		} catch (Exception ex) {
			System.err.println("language exception:"+ex.getLocalizedMessage()+"\nlanguage="+key);
			return '!' + key + '!';
		}
	}

	public static String getCountryLanguage(Locale locale) {
		return locale.getLanguage() + "_" + locale.getCountry();
	}

	public static String getColonMsg(String key) {
		return(getMsg(key)+" : ");
	}

	public static String getDateTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return (formatter.format(date));
	}

	public static String getDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return (formatter.format(date));
	}

	public static String getDate(SbDate date) {
		return(date.format(DB.getInstance().getFormatDate()));
	}
	
	public static String getDateTime(SbDate date) {
		return(date.format(DB.getInstance().getFormatDateTime()));
	}

}
